/*
 * SeisCompress.h
 *
 *  Created on: Dec 11, 2012
 *      Author: angel
 */

#ifndef SEISCOMPRESS_H_
#define SEISCOMPRESS_H_

#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include "byteBuffer.h"
#define EPS 2.2204e+30
#define ABSF(x) ((x) < 0 ? -(x) : (x))

int _endian;
int _type;
int _nwindows;
int _numsamp;
int _numsamp2;
int _recordLength;
int _recordLengthShort;
int _recordLengthFloat;
int _scalarLengthShort;
int _ibufLengthFloat;
short *_ibuf;
float *_scalars;
byteBuffer _buf;
byteBuffer _sbuf;
byteBuffer _fbuf;
int _tracePosition;

#define NATIVE 0
#define SCALAR16 16
#define LENWIN16 100

void SeisCompress_Init(int type, int numsamp, int endianness);
void SeisCompress_Finalize(void);
int recordLength(int type, int numsamp);
int position(int tracePosition);
void packTrace(float *trc);
void packFrame(int ntrc, float **frm);
void packTrace16( float *trc );
void unpackTrace( float *trc );
void unpackTrace16( float *trc );
void unpackTrace16Ex( float *trc);

#endif /* SEISCOMPRESS_H_ */
