/*
 * SeisCompress.c
 *
 *  Created on: Dec 5, 2012
 *      Author: Fuad
 */

#include "SeisCompress.h"
#include <endian.h>

void SeisCompress_Init(int type, int numsamp, int endianness)
{
	byteBuffer buf=NULL;

	if (type == SCALAR16)
		_type = SCALAR16;
	else
		_type = NATIVE;

	_endian = endianness;

	if (_type == SCALAR16)
	{
		_nwindows = (numsamp + LENWIN16 - 1) / LENWIN16;
		_numsamp = numsamp;
		_numsamp2 = ((numsamp % 2 == 0) ? numsamp : numsamp + 1);
		_recordLength = 4 * _nwindows + 2 * _numsamp;
		_buf = mallocByteBuffer(4 * numsamp);
		_scalars = malloc(_nwindows * sizeof(float));
		_ibuf = malloc(_numsamp2 * sizeof(short));
		//_sbuf = mallocByteBuffer(4 * numsamp);
		_sbuf = _buf;
		//memcpy(_sbuf, buf, sizeof(byteBuffer) + 4 * numsamp);
		//_fbuf = mallocByteBuffer(4 * numsamp);
		_fbuf = _buf;
		//memcpy(_fbuf, buf, sizeof(byteBuffer) + 4 * numsamp);
		_recordLengthFloat = _recordLength / 4;
		_recordLengthShort = _recordLength / 2;
		_scalarLengthShort = 2 * _nwindows;
		_ibufLengthFloat = _numsamp / 2;
	}
	else if (_type == NATIVE)
	{
		_numsamp = numsamp;
		_recordLength = 4 * _numsamp;
		_recordLengthFloat = _numsamp;
		_buf = buf;
		memcpy(&_fbuf, &buf, sizeof(uint8_t));
	}

	_tracePosition = 0;
}

void SeisCompress_Finalize(void)
{
	free(_buf);
	free(_scalars);
	free(_ibuf);
}

int recordLength(int type, int numsamp)
{
	int len;

	if (type == SCALAR16)
		len = 2 * (numsamp + 1) + 4 * (numsamp + LENWIN16 - 1) / LENWIN16;
	else
		len = 4 * numsamp;

	return (len);
}

int position(int tracePosition)
{
    _tracePosition = tracePosition;
    _buf->position = _tracePosition * _recordLength;
    if ( _type == SCALAR16 )
      _sbuf->position = _tracePosition * _recordLengthShort;
    _fbuf->position = _tracePosition * _recordLengthFloat;
    return (_tracePosition);
}

//void unpackFrame( int ntrc, float trc[20][20] )
//{
//    for ( int j = 0; j < ntrc; j++ ) {
//      position( j );
//      if (_type == SCALAR16)
//        unpackTrace16( trc[j] );
//      else
//        unpackTrace( trc[j] );
//    }
//}

void unpackFrame( int ntrc, float trc[20][20] )
{
	int j;

    for (j = 0; j < ntrc; j++ ) {
      position( j );
      if (_type == SCALAR16)
        unpackTrace16( trc[j] );
      else
        unpackTrace( trc[j] );
    }
}

//void packFrame( int ntrc, float frm[20][20] )
//{
//    for ( int j = 0; j < ntrc; j++ ) {
//      position( j );
//      if ( _type == SCALAR16 )
//        packTrace16( frm[j] );
//      else
//        packTrace( frm[j] );
//    }
//}

/* (ntrc, ns) -> frm */
void packFrame(int ntrc, float **frm)
{
	int j;

    for (j = 0; j < ntrc; j++ ) {
      position( j );
      //if ( _type == SCALAR16 )
      //  packTrace16( frm[j] );
      //else
        packTrace( frm[j] );
    }
}

void packTrace(float *trc)
{
	//_fbuf.put( trc, 0, _numsamp );
	int i;

	for (i = 0; i < _numsamp; i++)
	{
		BytePutFloat(_fbuf, *(trc + i));
	}
}

void packTrace16( float *trc ) {
   float val;
   int i1 = 0;
   int i2 = 0;
   int i, k;

   // Loop over windows
   for (k = 0; k < _nwindows; k++ )
   {

	   // Sample range for this window
	   i1 = i2;
	   i2 = i1 + LENWIN16;
	   if ( i2 > _numsamp )
	   {
		   i2 = _numsamp;

		   // Find max abs value in window
	   }
	   float amax = 0.0f;
	   for (i = i1; i < i2; i++ )
	   {
		   if( (isnan(trc[i])) || (isinf(trc[i])))
			   trc[i] = 0;

//		   if(trc[i]< -1e+10)
//			   trc[i] = -1e-4;
//
		   if(ABSF(trc[i])> EPS)
			   trc[i] = 0;

		   val = ( ( trc[i] < 0.0f ) ? -trc[i] : trc[i] );
		   if ( val > amax ) {
			   amax = val;
		   }
	   }

	   //printf("amax=%10.10e  ", amax);
//	   if(amax>2.2e+6)
//	   		   amax = 2.2e-4;
	   // Set scale factor
	   if ( amax > 0.0 ) {
		   amax = 32766.0f / amax;


		 //  printf("amax=%10.10e  \n", amax);


		   // Loop and scale to 16 bit range
	   }

	   for (i = i1; i < i2; i++ ) {
		   _ibuf[i] = ( short ) ( 0.5f + amax * trc[i] );
	   }
	   _scalars[k] = amax;
   }
   // Pack scalars into into buffer
   //_fbuf.put( _scalars );
   for (i = 0; i < _nwindows; i++)
   {
	   //BytePutFloat(_fbuf, _scalars[i]);
	   BytePutFloatEx(_fbuf, _scalars[i], _endian);
   }

   // Update position of "Short" view buffer to reflect scalars that were added
   //_sbuf->position += _scalarLengthShort; //_sbuf.position( _sbuf.position() + _scalarLengthShort );
   // Pack 16bit sample values into buffer
   //printf("scalarLengthShort = %d\n", _scalarLengthShort);
   //_sbuf.put( _ibuf );
   for (i = 0; i < _numsamp; i++)
   {
	   //uint32data = htobe32(uint32data);
	   //_ibuf[i] = htobe16(_ibuf[i]);
	   //BytePutShort(_sbuf, _ibuf[i]);
	   BytePutShortEx(_sbuf, _ibuf[i], _endian);;
   }
   //_fbuf->position += _ibufLengthFloat;
   // Update position of "Float" view buffer to reflect 16bit samples
   //_fbuf.position( _fbuf.position() + _ibufLengthFloat );
}

void unpackTrace( float *trc )
{
   // _fbuf.get( trc, 0, _numsamp );
	int i;
	int pos;

	pos = 0;
	for (i = 0; i < _numsamp; i++)
	{
		trc[i] = ByteGetFloat(_fbuf, pos);
		pos += sizeof(float);
	}
}

void unpackTrace16( float *trc )
{
    float amax;
    int i, k;
    int i1 = 0;
    int i2 = 0;

// Unpack scalars from buffer
    //_fbuf.get( _scalars );
    for (i = 0; i < _nwindows; i++)
    {
    	_scalars[i] = ByteGetFloat(_fbuf, i * sizeof(float));
//    	printf("_scalars[%d] = %f\n", i, _scalars[i]);
    }

// Update position of "Short" view buffer
    //_sbuf.position( _sbuf.position() + _scalarLengthShort );
    _sbuf->position += _scalarLengthShort;
// Unpack SCALAR16 sample values from buffer
    //_sbuf.get( _ibuf );
    for (i = 0; i < _numsamp2; i++)
    {
    	_ibuf[i] = ByteGetShort(_sbuf, i * sizeof(short));
//    	printf("ibuf[%d] = %x\n", i, _ibuf[i]);
    }

// Loop over windows
    for ( k = 0; k < _nwindows; k++ ) {

// Sample range for this window
      i1 = i2;
      i2 = i1 + LENWIN16;
      if ( i2 > _numsamp ) {
        i2 = _numsamp;

// Set inverse scale factor
      }
      if ( _scalars[k] > 0.0 ) {
        amax = 1.0f / _scalars[k];
      }
      else {
        amax = 0.0f;

// Loop and scale back to float
      }
      for ( i = i1; i < i2; i++ ) {
        trc[i] = amax * ( _ibuf[i] );

      }
    }
}

void unpackTrace16Ex( float *trc )
{
    float amax;
    int i, k;
    int i1 = 0;
    int i2 = 0;

    int pos = 0;

// Unpack scalars from buffer
    //_fbuf.get( _scalars );
    for (i = 0; i < _nwindows; i++)
    {
    	_scalars[i] = ByteGetFloat(_fbuf, i * sizeof(float));
    }

    pos = _nwindows * sizeof(float);
// Update position of "Short" view buffer
    //_sbuf.position( _sbuf.position() + _scalarLengthShort );
    _sbuf->position += _scalarLengthShort;
// Unpack SCALAR16 sample values from buffer
    //_sbuf.get( _ibuf );
    for (i = 0; i < _numsamp; i++)
    {
    	_ibuf[i] = ByteGetShort(_sbuf, pos);
    	pos += sizeof(short);
    }

// Loop over windows
    for ( k = 0; k < _nwindows; k++ ) {

// Sample range for this window
      i1 = i2;
      i2 = i1 + LENWIN16;
      if ( i2 > _numsamp ) {
        i2 = _numsamp;

// Set inverse scale factor
      }
      if ( _scalars[k] > 0.0 ) {
        amax = 1.0f / _scalars[k];
      }
      else {
        amax = 0.0f;

// Loop and scale back to float
      }
      for ( i = i1; i < i2; i++ ) {
        trc[i] = amax * ( _ibuf[i] );
      }
    }
}

int numWindows( int numsamp )
{
    return ( ( numsamp + LENWIN16 - 1 ) / LENWIN16 );
}
