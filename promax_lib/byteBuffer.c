/*
 * byteBuffer.c
 *
 *  Created on: Dec 6, 2012
 *
 */

#include "byteBuffer.h"

void printBytes(uint8_t *buff, size_t len, char *name)
{
	int i;
	printf("Dumping %d bytes from %s\n", (int) len, name);
	for(i=0; i<len; i++) {
		if(i > 0 && !(i%8)) putchar(' ');
		if(i > 0 && !(i%64)) putchar('\n');
		printf("%02x", buff[i]);
	}
	putchar('\n');
}

void printByteBuffer(byteBuffer bb, char *name)
{
    printBytes(bb->bytes, bb->len, name);
}

byteBuffer mallocByteBuffer(size_t len)
{
	byteBuffer retval;
	if((retval = (byteBuffer) malloc(sizeof(byteBufferStruct) + len + 1)) == NULL)
		return NULL;
	retval->position = 0;
    retval->len = len;
    retval->bytes = (uint8_t *) (retval + 1) ; /* just past the byteBuffer in malloc'ed space */
    memset(retval->bytes, 0, len*sizeof(uint8_t));
    return retval;
}

/* utility function to convert hex character representation to their nibble (4 bit) values */
static uint8_t nibbleFromChar(char c)
{
	if(c >= '0' && c <= '9') return c - '0';
	if(c >= 'a' && c <= 'f') return c - 'a' + 10;
	if(c >= 'A' && c <= 'F') return c - 'A' + 10;
	return 255;
}

/* Convert a string of characters representing a hex buffer into a series of bytes of that real value */
byteBuffer hexStringToBytes(char *inhex)
{
	byteBuffer retval;
	uint8_t *p;
	int len, i;

	len = strlen(inhex) / 2;
	if((retval = mallocByteBuffer(len)) == NULL) return NULL;

	for(i=0, p = (uint8_t *) inhex; i<len; i++) {
		retval->bytes[i] = (nibbleFromChar(*p) << 4) | nibbleFromChar(*(p+1));
		p += 2;
	}
    retval->bytes[len] = 0;
	return retval;
}

byteBuffer bytesToBytes(void *bytes, size_t len)
{
    byteBuffer retval = mallocByteBuffer(len);
    memcpy(retval->bytes, bytes, len);
    return retval;
}

int bytesAreEqual(byteBuffer b1, byteBuffer b2)
{
    if(b1->len != b2->len) return 0;
    return (memcmp(b1->bytes, b2->bytes, b1->len) == 0);
}

static char byteMap[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
static int byteMapLen = sizeof(byteMap);

/* Utility function to convert nibbles (4 bit values) into a hex character representation */
static char nibbleToChar(uint8_t nibble)
{
	if(nibble < byteMapLen) return byteMap[nibble];
	return '*';
}

/* Convert a buffer of binary values into a hex string representation */
char *bytesToHexString(byteBuffer bb)
{
	char *retval;
	int i;

	retval = malloc(bb->len*2 + 1);
	for(i=0; i<bb->len; i++) {
		retval[i*2] = nibbleToChar(bb->bytes[i] >> 4);
		retval[i*2+1] = nibbleToChar(bb->bytes[i] & 0x0f);
	}
    retval[bb->len*2] = 0;
	return retval;
}

void BytePut(byteBuffer bb, uint8_t *src, size_t size)
{
	int pos;

	pos = (*bb).position;
	(*bb).position = pos + size;
	memcpy(bb->bytes + pos, &src, size * sizeof(uint8_t));
}

void BytePutShort(byteBuffer bb, short src)
{
	int pos;
	int shortblen = sizeof(short);

	pos = bb->position;
	memcpy(bb->bytes + pos, &src, shortblen);
	bb->position = pos + shortblen;
}

void BytePutShortEx(byteBuffer bb, short src, int endian)
{
	int pos;
	int shortblen = sizeof(short);
	uint16_t shortbuffer;

	pos = bb->position;
	memcpy(&shortbuffer, &src, shortblen);
	shortbuffer = (endian == BIG_ENDIAN) ? htobe16(shortbuffer) : htole16(shortbuffer);
	memcpy(bb->bytes + pos, &shortbuffer, shortblen);
	bb->position = pos + shortblen;
}

void BytePutInt(byteBuffer bb, int src)
{
	int pos;
	int intblen = sizeof(int);

	pos = bb->position;
	memcpy(bb->bytes + pos, &src, intblen);
	bb->position = pos + intblen;
}

void BytePutIntEx(byteBuffer bb, int src, int endian)
{
	int pos;
	int intblen = sizeof(int);
	uint32_t intbuffer;

	pos = bb->position;
	memcpy(&intbuffer, &src, intblen);
	intbuffer = (endian == BIG_ENDIAN) ? htobe32(intbuffer) : htole32(intbuffer);
	memcpy(bb->bytes + pos, &intbuffer, intblen);
	bb->position = pos + intblen;
}

void BytePutFloat(byteBuffer bb, float src)
{
	int pos;
	int floatblen = sizeof(float);

	pos = bb->position;
	memcpy(bb->bytes + pos, &src, floatblen);
	bb->position = pos + floatblen;
}

void BytePutFloatEx(byteBuffer bb, float src, int endian)
{
	int pos;
	int intblen = sizeof(uint32_t);
	uint32_t intbuffer;

	pos = bb->position;
	memcpy(&intbuffer, &src, sizeof(float));
	intbuffer = (endian == BIG_ENDIAN) ? htobe32(intbuffer) : htole32(intbuffer);
	memcpy(bb->bytes + pos, &intbuffer, intblen);
	bb->position = pos + intblen;
}

void BytePutDoubleEx(byteBuffer bb, double src, int endian)
{
	int pos;
	int intblen = sizeof(uint64_t);
	uint64_t longbuffer;

	pos = bb->position;
	memcpy(&longbuffer, &src, sizeof(double));
	longbuffer = (endian == BIG_ENDIAN) ? htobe64(longbuffer) : htole32(longbuffer);
	memcpy(bb->bytes + pos, &longbuffer, intblen);
	bb->position = pos + intblen;
}

short ByteGetShort(byteBuffer bb, int pos)
{
	short shortvalue;

	memcpy(&shortvalue, bb->bytes + pos, sizeof(short));

	return shortvalue;
}

int ByteGetInt(byteBuffer bb, int pos)
{
	int intvalue;

	memcpy(&intvalue, bb->bytes + pos, sizeof(int));

	return intvalue;
}

float ByteGetFloat(byteBuffer bb, int pos)
{
	float floatvalue;

	memcpy(&floatvalue, bb->bytes + pos, sizeof(float));

	return floatvalue;
}

double ByteGetDouble(byteBuffer bb, int pos)
{
	double doublevalue;

	memcpy(&doublevalue, bb->bytes + pos, sizeof(double));

	return doublevalue;
}
