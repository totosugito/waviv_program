/*
 * segy_promax.c
 *
 *  Created on: Dec 26, 2012
 *      Author: fuad
 */

#include "segy_promax.h"

PromaxTraceHeader HEADER_INIT =
{
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, P_EOH, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, P_EOH, P_EOH
};

float* ReadFloat16Data(FILE *fid, int nsp);

void writeTraceToDataset(segy intrace, int traceno, int ntrc,
		int seqno, int endens, int nsp, int dt, FILE *fohdr, FILE *fotrc)
{
	PromaxTraceHeader phdr;
	uint32_t uint32buf;

	readSuTraceHeader(&phdr, intrace, nsp, dt);
	writePromaxTraceHeaderEx(phdr, traceno, seqno, endens, fohdr);

	position(0);
	packTrace16(intrace.data);
	uint32buf = _nwindows;
	uint32buf = htobe32(uint32buf);
	fwrite(&uint32buf, sizeof(uint32_t), 1, fotrc);
	uint32buf = LENWIN16;
	uint32buf = htobe32(uint32buf);
	fwrite(&uint32buf, sizeof(uint32_t), 1, fotrc);
	uint32buf = _numsamp % LENWIN16;
	uint32buf = htobe32(uint32buf);
	fwrite(&uint32buf, sizeof(uint32_t), 1, fotrc);
	fwrite(_buf->bytes, 1, _recordLength, fotrc);
}

void writeTraceToDataset64(segy intrace, int traceno, int ntrc,
		int seqno, int endens, int nsp, int dt, FILE *fohdr, FILE *fotrc)
{
	PromaxTraceHeader phdr;
	uint32_t uint32buf;

	readSuTraceHeader(&phdr, intrace, nsp, dt);
	writePromaxTraceHeader64Ex(phdr, traceno, seqno, endens, fohdr);

	position(0);
	packTrace16(intrace.data);
	uint32buf = _nwindows;
	uint32buf = htobe32(uint32buf);
	fwrite(&uint32buf, sizeof(uint32_t), 1, fotrc);
	uint32buf = LENWIN16;
	uint32buf = htobe32(uint32buf);
	fwrite(&uint32buf, sizeof(uint32_t), 1, fotrc);
	uint32buf = _numsamp % LENWIN16;
	uint32buf = htobe32(uint32buf);
	fwrite(&uint32buf, sizeof(uint32_t), 1, fotrc);
	fwrite(_buf->bytes, 1, _recordLength, fotrc);
}

void writeSegyToFloat16(char *segyFileName, char *outputFileName, int endian)
{
	/* BEGIN VARIABLE DECLARATION */
	FILE *fid;
	FILE *fod;
	char ebcdic[3200];

	int nsegy;
	int nsp;
	int format;
	int ntrc;

	int i;
	uint32_t uint32data;
	/* END VARIABLE DECLARATION */

	/* BEGIN VARIABLE INITIALIZATION */
	fid = fopen(segyFileName, "r");
	readEbcdicHeader(fid, ebcdic);
	readBinaryHeader(fid, endian, &bh, &nsegy);

	ntrc = getNumberOfTraceSegyFile(fid, nsegy);
	nsp = bh.hns;
	format = bh.format;

	fod = fopen(outputFileName, "w");

	SeisCompress_Init(SCALAR16, nsp, BIG_ENDIAN);
	position(0);
	/* END VARIABLE INITIALIZATION */

	/* MAIN */
	for (i = 0; i < ntrc; i++)
	{
		readTraceSegy(fid, endian, nsp, format, nsegy, &tr);

		position(0);

		packTrace16(tr.data);

		// Write float16 header
		uint32data = _nwindows;
		uint32data = htobe32(uint32data);
		fwrite(&uint32data, sizeof(uint32_t), 1, fod);

		uint32data = LENWIN16;
		uint32data = htobe32(uint32data);
		fwrite(&uint32data, sizeof(uint32_t), 1, fod);

		uint32data = _numsamp % LENWIN16;
		uint32data = htobe32(uint32data);
		fwrite(&uint32data, sizeof(uint32_t), 1, fod);

		// Write float16 data
		fwrite(_buf->bytes, 1, _recordLength, fod);
	}

	/* FINALIZATION */
	SeisCompress_Finalize();
	fclose(fid);
	fclose(fod);
}

void writeSegyToFloat16Ex(char *segyFileName, char *outputFileName, int endian)
{
	/* BEGIN VARIABLE DECLARATION */
	FILE *fid;
	FILE *fod;
	char ebcdic[3200];

	int nsegy;
	int nsp, dt;
	int format;
	int ntrc;

	PromaxTraceHeader phdr;

	int i;
	uint32_t uint32data;
	/* END VARIABLE DECLARATION */

	/* BEGIN VARIABLE INITIALIZATION */
	fid = fopen(segyFileName, "r");
	readEbcdicHeader(fid, ebcdic);
	readBinaryHeader(fid, endian, &bh, &nsegy);

	ntrc = getNumberOfTraceSegyFile(fid, nsegy);
	nsp = bh.hns;
	dt = bh.hdt;
	format = bh.format;

	fod = fopen(outputFileName, "w");

	SeisCompress_Init(SCALAR16, nsp, BIG_ENDIAN);
	position(0);
	/* END VARIABLE INITIALIZATION */

	/* MAIN */
	for (i = 0; i < ntrc; i++)
	{
		readTraceSegy(fid, endian, nsp, format, nsegy, &tr);

		position(0);

		//void readSegyHeader(PromaxTraceHeader *desthdr, segy srchdr, int nsp, int dt)
		readSegyTraceHeader(&phdr, tr, nsp, dt);
		packTrace16(tr.data);

		// Write float16 header
		uint32data = _nwindows;
		uint32data = htobe32(uint32data);
		fwrite(&uint32data, sizeof(uint32_t), 1, fod);

		uint32data = LENWIN16;
		uint32data = htobe32(uint32data);
		fwrite(&uint32data, sizeof(uint32_t), 1, fod);

		uint32data = _numsamp % LENWIN16;
		uint32data = htobe32(uint32data);
		fwrite(&uint32data, sizeof(uint32_t), 1, fod);

		// Write float16 data
		fwrite(_buf->bytes, 1, _recordLength, fod);
	}

	/* FINALIZATION */
	SeisCompress_Finalize();
	fclose(fid);
	fclose(fod);
}

/*
 * -- How to Use --
 * ReadFloat16Header(fid, &nsp, &nwindows);
 * rewind(fid);
 * SeisCompress_Init(SCALAR16, nsp, BIG_ENDIAN);
 * getTrace16(fid, &tr);
 * SeisCompress_Finalize();
 */
void getTrace16(FILE *fid, segy *trace)
{
	int nsp, nwindows;
	float *data;

	position(0);
	ReadFloat16Header(fid, &nsp, &nwindows);
	data = ReadFloat16Data(fid, nsp);

	memcpy(trace->data, data, nsp * sizeof(float));

	free(data);
}

void ReadFloat16(char *traceFileName)
{
	/* BEGIN VARIABLE DECLARATION */
	FILE *fid;

	int nwindows;
	int nsp;
	float *data;

	/* END VARIABLE DECLARATION */

	/* BEGIN VARIABLE INITIALIZATION */
	fid = fopen(traceFileName, "r");
	/* END VARIABLE INITIALIZATION */

	/* MAIN */
	ReadFloat16Header(fid, &nsp, &nwindows);
	rewind(fid);

	SeisCompress_Init(SCALAR16, nsp, BIG_ENDIAN);

	position(0);
	ReadFloat16Header(fid, &nsp, &nwindows);
	data = ReadFloat16Data(fid, nsp);

	//for (i = 0; i < nsp; i++)
	//	printf("%d: %f\n", i, data[i]);

	free(data);

	SeisCompress_Finalize();

	/* FINALIZATION */
	//SeisCompress_Finalize();
	fclose(fid);
}

void ReadFloat16Header(FILE *fid, int *nsp, int *nwindows)
{
	uint32_t uint32_buf;
	int inwindows = 0;
	int lenwin16 = 0;
	int residue = 0;

	if (fread(&uint32_buf, sizeof(uint32_t), 1 , fid)) inwindows = htobe32(uint32_buf);

	if (fread(&uint32_buf, sizeof(uint32_t), 1, fid))
	{
		lenwin16 = htobe32(uint32_buf);
		if (lenwin16 != LENWIN16) fprintf(stderr, "Warning: data length not equal to %d\n", LENWIN16);
	}

	if (fread(&uint32_buf, sizeof(uint32_t), 1, fid)) residue = htobe32(uint32_buf);

	/*
	scalars = calloc(inwindows, sizeof(float));
	for (i = 0; i < inwindows; i++)
	{
		if (fread(&uint32_buf, 1, sizeof(uint32_t), fid)) uint32_buf = htobe32(uint32_buf);
		memcpy(scalars + i, &uint32_buf, sizeof(uint32_t));
	}

	for (i = 0; i < 5; i++)
	{
		fread(&uint32_buf, 1, sizeof(uint32_t), fid);
	}

	_scalars = scalars;
	*/

	(*nsp) = (inwindows - 1) * lenwin16 + residue;
	(*nwindows) = inwindows;
}

float* ReadFloat16Data(FILE *fid, int nsp)
{
	int i = 0;
	int result;
	short short_buf;
	float float_buf;
	float *traces = (float*) calloc(nsp, sizeof(float));

	for (i = 0; i < _nwindows; i++)
	{
		result = fread(&float_buf, sizeof(uint32_t), 1, fid);
		/*
		uint32_buf = htobe32(uint32_buf);
		memcpy(&float_buf, &uint32_buf, sizeof(float));
		BytePutFloat(_buf, float_buf);
		*/
		BytePutFloatEx(_buf, float_buf, BIG_ENDIAN);
		//printf("%f\n", ByteGetFloat(_buf, i * sizeof(float)));
	}

	for (i = 0; i < _numsamp; i++)
	{
		result = fread(&short_buf, sizeof(short), 1, fid);
		//BytePutShort(_buf, short_buf);
		BytePutShortEx(_buf, short_buf, BIG_ENDIAN);
	}

	unpackTrace16Ex(traces);

//	for (i = 0; i < _numsamp; i++)
//	{
//		printf("%d: %f\n", i, traces[i]);
//	}

	return traces;
}

int getNumberOfTraceFloat16File(FILE *inpf, int nsp, int nwindows)
{
	off64_t begPos, endPos;  /*size of file input*/
	off64_t bytetr;  /*size of trace data and data */
	int retseek;
	int ntrc;              /*output number of traces*/
	int nsegy = 3 * sizeof(float) + nwindows * sizeof(float) + nsp * sizeof(short);

	ntrc = 0;
	begPos = ftello64(inpf);                /*get beginning of file pointer location*/

	retseek = fseeko64(inpf, 0 , SEEK_END); /* go to end of file */
	if(retseek) segy_lib_err("getNumOfTrace", "error seek command.");

	endPos = ftello64(inpf);                /* get size of file */

	//bytetr = endPos - 3600;              /* remove ascii and binary trace */
	bytetr = endPos;

	if((bytetr % nsegy)==0)            /* get number of traces*/
		ntrc = bytetr/nsegy;
	else
		ntrc = 0;

	retseek = fseeko64(inpf, begPos , SEEK_SET);
	if(retseek) segy_lib_err("getNumOfTrace", "error set location to beginning pointer.");

	return (ntrc);
}

void readSegyTraceHeader(PromaxTraceHeader *desthdr, segy srchdr, int nsp, int dt)
{
	PromaxTraceHeader hdr = HEADER_INIT;

	float scalercoord = 1;
	float scalerelev = 1;

	hdr.SEQNO = 0;
//	//
	hdr.TRCID = srchdr.trid;
	hdr.MTSTART = srchdr.muts;
	hdr.MTEND = srchdr.mute;
	hdr.TIMELENGTH1 = (nsp - 1) * dt / 1000;
	hdr.TIMELENGTH2 = (nsp - 1) * dt / 1000;
	hdr.TSA = srchdr.tstat;
	hdr.NHST = srchdr.nhs;
	hdr.FFID = srchdr.fldr;
//	int TRCFLD;			// byte: 85 - 88 //
	//hdr.TRCFLD; /* not verified */
	hdr.SP = srchdr.ep;
	hdr.CDP = srchdr.cdp;
	hdr.DSREG1 = srchdr.offset;
	hdr.DSREG2 = srchdr.offset;

	scalerelev = (srchdr.scalel < 0) ? (float)-1 / srchdr.scalel : (float)srchdr.scalel;
	if (srchdr.scalel == 0) scalerelev = 1;

	hdr.RGE = srchdr.gelev * scalerelev;
	hdr.SES = srchdr.selev * scalerelev;
	hdr.SDBS = srchdr.sdepth * scalerelev;
	hdr.WDS = srchdr.swdep * scalerelev;
	hdr.WGD = srchdr.gwdep * scalerelev;

	scalercoord = (srchdr.scalco < 0) ? (float)-1 / srchdr.scalco : (float)srchdr.scalco;
	if (srchdr.scalco == 1) scalercoord = 1;

	hdr.SRCX = srchdr.sx * scalercoord;
	hdr.SRCXD = srchdr.sx * scalercoord;
	hdr.SRCY = srchdr.sy * scalercoord;
	hdr.SRCYD = srchdr.sy * scalercoord;
	hdr.GRPX = srchdr.gx * scalercoord;
	hdr.GRPXD = srchdr.gx * scalercoord;
	hdr.GRPY = srchdr.gy * scalercoord;
	hdr.GRPYD = srchdr.gy * scalercoord;
	hdr.UTSRC = srchdr.sut * scalercoord;

	hdr.SECSCOR = srchdr.sstat;
	hdr.GRPSCOR = srchdr.gstat;

	/* EXTENDED BEGIN */
//	hdr.ILINE = srchdr.d2;
//	hdr.XLINE = srchdr.f2;
//	hdr.CDPX = srchdr.d1;
//	hdr.CDPXD = srchdr.d1;
//	hdr.CDPY = srchdr.f1;
//	hdr.CDPYD = srchdr.f1;
//	hdr.SPNO = srchdr.ungpow;
	/* EXTENDED END */

	/* EXTENDED BEGIN */
	hdr.ILINE = 0;
	hdr.XLINE = 0;
	hdr.CDPX = 0;
	hdr.CDPXD = 0;
	hdr.CDPY = 0;
	hdr.CDPYD = 0;
	hdr.SPNO = 0;
	/* EXTENDED END */

	hdr.DELRECT = srchdr.delrt;

	(*desthdr) = hdr;
}

void readSuTraceHeader(PromaxTraceHeader *desthdr, segy srchdr, int nsp, int dt)
{
	PromaxTraceHeader hdr = HEADER_INIT;

	float scalercoord = 1;
	float scalerelev = 1;

	hdr.SEQNO = 0;
//	//
	hdr.TRCID = srchdr.trid;
	hdr.MTSTART = srchdr.muts;
	hdr.MTEND = srchdr.mute;
	hdr.TIMELENGTH1 = (nsp - 1) * dt / 1000;
	hdr.TIMELENGTH2 = (nsp - 1) * dt / 1000;
	hdr.TSA = srchdr.tstat;
	hdr.NHST = srchdr.nhs;
	hdr.FFID = srchdr.fldr;
//	int TRCFLD;			// byte: 85 - 88 //
	//hdr.TRCFLD; /* not verified */
	hdr.SP = srchdr.ep;
	hdr.CDP = srchdr.cdp;
	hdr.DSREG1 = srchdr.offset;
	hdr.DSREG2 = srchdr.offset;

	scalerelev = (srchdr.scalel < 0) ? (float)-1 / srchdr.scalel : (float)srchdr.scalel;
	if (srchdr.scalel == 0) scalerelev = 1;

	hdr.RGE = srchdr.gelev * scalerelev;
	hdr.SES = srchdr.selev * scalerelev;
	hdr.SDBS = srchdr.sdepth * scalerelev;
	hdr.WDS = srchdr.swdep * scalerelev;
	hdr.WGD = srchdr.gwdep * scalerelev;

	scalercoord = (srchdr.scalco < 0) ? (float)-1 / srchdr.scalco : (float)srchdr.scalco;
	if (srchdr.scalco == 1) scalercoord = 1;

	hdr.SRCX = srchdr.sx * scalercoord;
	hdr.SRCXD = srchdr.sx * scalercoord;
	hdr.SRCY = srchdr.sy * scalercoord;
	hdr.SRCYD = srchdr.sy * scalercoord;
	hdr.GRPX = srchdr.gx * scalercoord;
	hdr.GRPXD = srchdr.gx * scalercoord;
	hdr.GRPY = srchdr.gy * scalercoord;
	hdr.GRPYD = srchdr.gy * scalercoord;
	hdr.UTSRC = srchdr.sut * scalercoord;

	hdr.SECSCOR = srchdr.sstat;
	hdr.GRPSCOR = srchdr.gstat;

	/* EXTENDED BEGIN */
	hdr.ILINE = srchdr.d2;
	hdr.XLINE = srchdr.f2;
	hdr.CDPX = srchdr.d1;

	hdr.CDPXD = srchdr.d1;
	hdr.CDPY = srchdr.f1;
	hdr.CDPYD = srchdr.f1;
	hdr.SPNO = srchdr.ungpow;
	/* EXTENDED END */

	hdr.DELRECT = srchdr.delrt;

	(*desthdr) = hdr;
}

void beS(short *dest, short src)
{
	uint16_t buf;

	memcpy(&buf, &src, sizeof(uint16_t));
	buf = htobe16(buf);
	memcpy(dest, &buf, sizeof(short));
}

void beI(int *dest, int src)
{
	uint32_t buf;

	memcpy(&buf, &src, sizeof(uint32_t));
	buf = htobe32(buf);
	memcpy(dest, &buf, sizeof(int));
}

void beF(float *dest, float src)
{
	uint32_t buf;

	memcpy(&buf, &src, sizeof(uint32_t));
	buf = htobe32(buf);
	memcpy(dest, &buf, sizeof(float));
}

void beD(double *dest, double src)
{
	uint64_t buf;

	memcpy(&buf, &src, sizeof(uint64_t));
	buf = htobe64(buf);
	memcpy(dest, &buf, sizeof(double));
}

void bethS(short *dest, short src)
{
	uint16_t buf;

	memcpy(&buf, &src, sizeof(uint16_t));
	buf = be16toh(buf);
	memcpy(dest, &buf, sizeof(short));
}

void bethI(int *dest, int src)
{
	uint32_t buf;

	memcpy(&buf, &src, sizeof(uint32_t));
	buf = be32toh(buf);
	memcpy(dest, &buf, sizeof(int));
}

void bethF(float *dest, float src)
{
	uint32_t buf;

	memcpy(&buf, &src, sizeof(uint32_t));
	buf = be32toh(buf);
	memcpy(dest, &buf, sizeof(float));
}

void bethD(double *dest, double src)
{
	uint64_t buf;

	memcpy(&buf, &src, sizeof(uint64_t));
	buf = be64toh(buf);
	memcpy(dest, &buf, sizeof(double));
}

void writePromaxTraceHeader(PromaxTraceHeader hdr, FILE *fod)
{
	PromaxTraceHeader bufhdr = HEADER_INIT;

	beI(&bufhdr.CDP, hdr.CDP);
	beF(&bufhdr.CDPX, hdr.CDPX);
	beD(&bufhdr.CDPXD, hdr.CDPXD);
	beF(&bufhdr.CDPY, hdr.CDPY);
	beD(&bufhdr.CDPYD, hdr.CDPYD);
	beF(&bufhdr.DELRECT, hdr.DELRECT);
	beF(&bufhdr.DSREG1, hdr.DSREG1);
	beF(&bufhdr.DSREG2, hdr.DSREG2);
	beI(&bufhdr.FFID, hdr.FFID);
	//beI(&bufhdr.FLAG, hdr.FLAG);
	beF(&bufhdr.GRPSCOR, hdr.GRPSCOR);
	beF(&bufhdr.GRPX, hdr.GRPX);
	beD(&bufhdr.GRPXD, hdr.GRPXD);
	beF(&bufhdr.GRPY, hdr.GRPY);
	beD(&bufhdr.GRPYD, hdr.GRPYD);
	beI(&bufhdr.ILINE, hdr.ILINE);
	beF(&bufhdr.MTEND, hdr.MTEND);
	beF(&bufhdr.MTSTART, hdr.MTSTART);
	beF(&bufhdr.NHST, hdr.NHST);
	beF(&bufhdr.RGE, hdr.RGE);
	beF(&bufhdr.SDBS, hdr.SDBS);
	beF(&bufhdr.SECSCOR, hdr.SECSCOR);
	beF(&bufhdr.SES, hdr.SES);
	beI(&bufhdr.SP, hdr.SP);
	beI(&bufhdr.SPNO, hdr.SPNO);
	beF(&bufhdr.SRCX, hdr.SRCX);
	beD(&bufhdr.SRCXD, hdr.SRCXD);
	beF(&bufhdr.SRCY, hdr.SRCY);
	beD(&bufhdr.SRCYD, hdr.SRCYD);
	beF(&bufhdr.TIMELENGTH1, hdr.TIMELENGTH1);
	beF(&bufhdr.TIMELENGTH2, hdr.TIMELENGTH2);
	//beI(&bufhdr.TRACENO, hdr.TRACENO);
	beI(&bufhdr.TRCFLD, hdr.TRCFLD);
	beS(&bufhdr.TRCID, hdr.TRCID);
	beF(&bufhdr.TSA, hdr.TSA);
	beF(&bufhdr.UTSRC, hdr.UTSRC);
	beF(&bufhdr.WDS, hdr.WDS);
	beF(&bufhdr.WGD, hdr.WGD);
	beI(&bufhdr.XLINE, hdr.XLINE);

	fwrite(&bufhdr, sizeof(PromaxTraceHeader), 1, fod);
};

void writePromaxTraceHeaderEx(PromaxTraceHeader hdr, int traceno, int seqno, int endens, FILE *fod)
{
	PromaxTraceHeader bufhdr = HEADER_INIT;

	beI(&bufhdr.END_ENS1, endens);
	beI(&bufhdr.SEQNO, seqno);
	beI(&bufhdr.END_ENS2, endens);
	beI(&bufhdr.TRACENO, traceno); //beI(&bufhdr.TRACENO, hdr.TRACENO);

	beS(&bufhdr.TRCID, hdr.TRCID);
	beF(&bufhdr.MTSTART, hdr.MTSTART);
	beF(&bufhdr.MTEND, hdr.MTEND);
	beF(&bufhdr.TIMELENGTH1, hdr.TIMELENGTH1);
	beF(&bufhdr.TIMELENGTH2, hdr.TIMELENGTH2);
	beF(&bufhdr.TSA, hdr.TSA);
	beF(&bufhdr.NHST, hdr.NHST);
	beI(&bufhdr.FFID, hdr.FFID);
	beI(&bufhdr.TRCFLD, hdr.TRCFLD);
	beI(&bufhdr.SP, hdr.SP);

	beI(&bufhdr.CDP, hdr.CDP);
	beF(&bufhdr.DSREG1, hdr.DSREG1);
	beF(&bufhdr.DSREG2, hdr.DSREG2);
	beF(&bufhdr.RGE, hdr.RGE);
	beF(&bufhdr.SES, hdr.SES);
	beF(&bufhdr.SDBS, hdr.SDBS);
	beF(&bufhdr.WDS, hdr.WDS);
	beF(&bufhdr.WGD, hdr.WGD);
	beF(&bufhdr.SRCX, hdr.SRCX);
	beD(&bufhdr.SRCXD, hdr.SRCXD);
	beF(&bufhdr.SRCY, hdr.SRCY);
	beD(&bufhdr.SRCYD, hdr.SRCYD);
	beF(&bufhdr.GRPX, hdr.GRPX);
	beD(&bufhdr.GRPXD, hdr.GRPXD);
	beF(&bufhdr.GRPY, hdr.GRPY);
	beD(&bufhdr.GRPYD, hdr.GRPYD);
	beF(&bufhdr.UTSRC, hdr.UTSRC);
	beF(&bufhdr.SECSCOR, hdr.SECSCOR);
	beF(&bufhdr.GRPSCOR, hdr.GRPSCOR);
	beI(&bufhdr.ILINE, hdr.ILINE);
	beI(&bufhdr.XLINE, hdr.XLINE);
	beF(&bufhdr.CDPX, hdr.CDPX);
	beD(&bufhdr.CDPXD, hdr.CDPXD);
	beF(&bufhdr.CDPY, hdr.CDPY);
	beD(&bufhdr.CDPYD, hdr.CDPYD);
	beI(&bufhdr.SPNO, hdr.SPNO);
	beF(&bufhdr.DELRECT, hdr.DELRECT);

//	printf("size = %d\n", sizeof(PromaxTraceHeader), 1, fod);
};

void writePromaxTraceHeader64Ex(PromaxTraceHeader hdr, int traceno, int seqno, int endens, FILE *fod)
{
	PromaxTraceHeader bufhdr = HEADER_INIT;

	byteBuffer bytebuf;

	int dummyint = 0;
	short dummyshort = 0;

	bytebuf = mallocByteBuffer(HDRBYTES);

	beI(&bufhdr.END_ENS1, endens);
	beI(&bufhdr.SEQNO, seqno);
	beI(&bufhdr.END_ENS2, endens);
	beI(&bufhdr.TRACENO, traceno); //beI(&bufhdr.TRACENO, hdr.TRACENO);

	BytePutInt(bytebuf, dummyint);
	BytePutIntEx(bytebuf, endens, BIG_ENDIAN);
	BytePutIntEx(bytebuf, seqno, BIG_ENDIAN);
	BytePutIntEx(bytebuf, endens, BIG_ENDIAN);
	BytePutInt(bytebuf, dummyint);
	BytePutIntEx(bytebuf, traceno, BIG_ENDIAN);
	BytePutShort(bytebuf, dummyshort);
//	fprintf(stderr, "POS0 = %i \n", bytebuf->position);

	beS(&bufhdr.TRCID, hdr.TRCID);
	beF(&bufhdr.MTSTART, hdr.MTSTART);
	beF(&bufhdr.MTEND, hdr.MTEND);
	beF(&bufhdr.TIMELENGTH1, hdr.TIMELENGTH1);
	beF(&bufhdr.TIMELENGTH2, hdr.TIMELENGTH2);
	beF(&bufhdr.TSA, hdr.TSA);
	beF(&bufhdr.NHST, hdr.NHST);
	beI(&bufhdr.FFID, hdr.FFID);
	beI(&bufhdr.TRCFLD, hdr.TRCFLD);
	beI(&bufhdr.SP, hdr.SP);

	BytePutShortEx(bytebuf, hdr.TRCID, BIG_ENDIAN);
	BytePutFloatEx(bytebuf, hdr.MTSTART, BIG_ENDIAN);
	BytePutFloatEx(bytebuf, hdr.MTEND, BIG_ENDIAN);
	BytePutFloatEx(bytebuf, hdr.TIMELENGTH1, BIG_ENDIAN);
	BytePutFloatEx(bytebuf, hdr.TIMELENGTH2, BIG_ENDIAN);
	BytePutInt(bytebuf, dummyint);
	BytePutFloatEx(bytebuf, hdr.TSA, BIG_ENDIAN);
	BytePutInt(bytebuf, dummyint);
	BytePutInt(bytebuf, dummyint);
	BytePutFloatEx(bytebuf, hdr.NHST, BIG_ENDIAN);
	BytePutInt(bytebuf, dummyint);
	BytePutInt(bytebuf, dummyint);
	BytePutInt(bytebuf, dummyint);
	BytePutInt(bytebuf, dummyint);
	BytePutIntEx(bytebuf, hdr.FFID, BIG_ENDIAN);
	BytePutIntEx(bytebuf, hdr.TRCFLD, BIG_ENDIAN);
	BytePutIntEx(bytebuf, hdr.SP, BIG_ENDIAN);
	BytePutInt(bytebuf, hdr.FLAG);
//	fprintf(stderr, "POS1 = %i \n", bytebuf->position);

	beI(&bufhdr.CDP, hdr.CDP);
	beF(&bufhdr.DSREG1, hdr.DSREG1);
	beF(&bufhdr.DSREG2, hdr.DSREG2);
	beF(&bufhdr.RGE, hdr.RGE);
	beF(&bufhdr.SES, hdr.SES);
	beF(&bufhdr.SDBS, hdr.SDBS);
	beF(&bufhdr.WDS, hdr.WDS);
	beF(&bufhdr.WGD, hdr.WGD);
	beF(&bufhdr.SRCX, hdr.SRCX);
	beD(&bufhdr.SRCXD, hdr.SRCXD);
	beF(&bufhdr.SRCY, hdr.SRCY);
	beD(&bufhdr.SRCYD, hdr.SRCYD);
	beF(&bufhdr.GRPX, hdr.GRPX);
	beD(&bufhdr.GRPXD, hdr.GRPXD);
	beF(&bufhdr.GRPY, hdr.GRPY);
	beD(&bufhdr.GRPYD, hdr.GRPYD);
	beF(&bufhdr.UTSRC, hdr.UTSRC);
	beF(&bufhdr.SECSCOR, hdr.SECSCOR);
	beF(&bufhdr.GRPSCOR, hdr.GRPSCOR);
	beI(&bufhdr.ILINE, hdr.ILINE);
	beI(&bufhdr.XLINE, hdr.XLINE);
	beF(&bufhdr.CDPX, hdr.CDPX);
	beD(&bufhdr.CDPXD, hdr.CDPXD);
	beF(&bufhdr.CDPY, hdr.CDPY);
	beD(&bufhdr.CDPYD, hdr.CDPYD);
	beI(&bufhdr.SPNO, hdr.SPNO);
	beF(&bufhdr.DELRECT, hdr.DELRECT);

	BytePutIntEx(bytebuf, hdr.CDP, BIG_ENDIAN);
	BytePutFloatEx(bytebuf, hdr.DSREG1, BIG_ENDIAN);
	BytePutFloatEx(bytebuf, hdr.DSREG2, BIG_ENDIAN);
	BytePutFloatEx(bytebuf, hdr.RGE, BIG_ENDIAN);
	BytePutFloatEx(bytebuf, hdr.SES, BIG_ENDIAN);
	BytePutFloatEx(bytebuf, hdr.SDBS, BIG_ENDIAN);
	BytePutFloatEx(bytebuf, hdr.WDS, BIG_ENDIAN);
	BytePutFloatEx(bytebuf, hdr.WGD, BIG_ENDIAN);
	BytePutFloatEx(bytebuf, hdr.SRCX, BIG_ENDIAN);
	BytePutDoubleEx(bytebuf, hdr.SRCXD, BIG_ENDIAN);
	BytePutFloatEx(bytebuf, hdr.SRCY, BIG_ENDIAN);
	BytePutDoubleEx(bytebuf, hdr.SRCYD, BIG_ENDIAN);
	BytePutFloatEx(bytebuf, hdr.GRPX, BIG_ENDIAN);
	BytePutDoubleEx(bytebuf, hdr.GRPXD, BIG_ENDIAN);
	BytePutFloatEx(bytebuf, hdr.GRPY, BIG_ENDIAN);
	BytePutDoubleEx(bytebuf, hdr.GRPYD, BIG_ENDIAN);
	BytePutFloatEx(bytebuf, hdr.UTSRC, BIG_ENDIAN);
	BytePutFloatEx(bytebuf, hdr.SECSCOR, BIG_ENDIAN);
	BytePutFloatEx(bytebuf, hdr.GRPSCOR, BIG_ENDIAN);
	BytePutIntEx(bytebuf, hdr.ILINE, BIG_ENDIAN);
	BytePutIntEx(bytebuf, hdr.XLINE, BIG_ENDIAN);
	BytePutFloatEx(bytebuf, hdr.CDPX, BIG_ENDIAN);
	BytePutDoubleEx(bytebuf, hdr.CDPXD, BIG_ENDIAN);
	BytePutFloatEx(bytebuf, hdr.CDPY, BIG_ENDIAN);
	BytePutDoubleEx(bytebuf, hdr.CDPYD, BIG_ENDIAN);
	BytePutIntEx(bytebuf, hdr.SPNO, BIG_ENDIAN);
	BytePutFloatEx(bytebuf, hdr.DELRECT, BIG_ENDIAN);
//	fprintf(stderr, "POS2 = %i \n\n", bytebuf->position);
	fwrite(bytebuf->bytes, HDRBYTES, 1, fod);

	free(bytebuf);
};

PromaxTraceHeader ReadPromaxHeader(segy *strace, PromaxTraceHeader hdr, unsigned short nsp)
{
	PromaxTraceHeader bufhdr = HEADER_INIT;

	bethI(&bufhdr.CDP, hdr.CDP);
	bethF(&bufhdr.CDPX, hdr.CDPX);
	bethD(&bufhdr.CDPXD, hdr.CDPXD);
	bethF(&bufhdr.CDPY, hdr.CDPY);
	bethD(&bufhdr.CDPYD, hdr.CDPYD);
	bethF(&bufhdr.DELRECT, hdr.DELRECT);
	bethF(&bufhdr.DSREG1, hdr.DSREG1);
	bethF(&bufhdr.DSREG2, hdr.DSREG2);
	bethI(&bufhdr.END_ENS1, hdr.END_ENS1);
	bethI(&bufhdr.END_ENS2, hdr.END_ENS2);
	bethI(&bufhdr.FFID, hdr.FFID);
	bethI(&bufhdr.FLAG, hdr.FLAG);
	bethF(&bufhdr.GRPSCOR, hdr.GRPSCOR);
	bethF(&bufhdr.GRPX, hdr.GRPX);
	bethD(&bufhdr.GRPXD, hdr.GRPXD);
	bethF(&bufhdr.GRPY, hdr.GRPY);
	bethD(&bufhdr.GRPYD, hdr.GRPYD);
	bethI(&bufhdr.ILINE, hdr.ILINE);
	bethF(&bufhdr.MTEND, hdr.MTEND);
	bethF(&bufhdr.MTSTART, hdr.MTSTART);
	bethF(&bufhdr.NHST, hdr.NHST);
	bethF(&bufhdr.RGE, hdr.RGE);
	bethF(&bufhdr.SDBS, hdr.SDBS);
	bethF(&bufhdr.SECSCOR, hdr.SECSCOR);
	bethI(&bufhdr.SEQNO, hdr.SEQNO);
	beF(&bufhdr.SES, hdr.SES);
	beI(&bufhdr.SP, hdr.SP);
	beI(&bufhdr.SPNO, hdr.SPNO);
	beF(&bufhdr.SRCX, hdr.SRCX);
	beD(&bufhdr.SRCXD, hdr.SRCXD);
	beF(&bufhdr.SRCY, hdr.SRCY);
	beD(&bufhdr.SRCYD, hdr.SRCYD);
	beF(&bufhdr.TIMELENGTH1, hdr.TIMELENGTH1);
	beF(&bufhdr.TIMELENGTH2, hdr.TIMELENGTH2);
	beI(&bufhdr.TRACENO, hdr.TRACENO); //beI(&bufhdr.TRACENO, hdr.TRACENO);
	beI(&bufhdr.TRCFLD, hdr.TRCFLD);
	beS(&bufhdr.TRCID, hdr.TRCID);
	beF(&bufhdr.TSA, hdr.TSA);
	beF(&bufhdr.UTSRC, hdr.UTSRC);
	beF(&bufhdr.WDS, hdr.WDS);
	beF(&bufhdr.WGD, hdr.WGD);
	beI(&bufhdr.XLINE, hdr.XLINE);

	//printf("CDP = %d\n", bufhdr.CDP);

	strace->tracl = bufhdr.TRACENO;
	strace->tracf = bufhdr.SEQNO;

	strace->trid = bufhdr.TRCID;
	strace->muts = bufhdr.MTSTART;
	strace->mute = bufhdr.MTEND;
	strace->tstat = bufhdr.TSA;
	strace->nhs = bufhdr.TSA;
	strace->fldr = bufhdr.FFID;
	strace->ep = bufhdr.SP;
	strace->cdp = bufhdr.CDP;
	strace->offset = bufhdr.DSREG1;
	strace->scalel = 1;
	strace->gelev = bufhdr.RGE;
	strace->selev = bufhdr.SES;
	strace->sdepth = bufhdr.SDBS;
	strace->swdep = bufhdr.WDS;
	strace->gwdep = bufhdr.WGD;
	strace->scalco = 1;
	strace->sx = bufhdr.SRCX;
	strace->sy = bufhdr.SRCY;
	strace->gx = bufhdr.GRPX;
	strace->gy = bufhdr.GRPY;
	strace->sut = bufhdr.UTSRC;
	strace->sstat = bufhdr.SECSCOR;
	strace->gstat = bufhdr.GRPSCOR;

	strace->ns = nsp;
	strace->dt = bufhdr.TIMELENGTH1 * 1000 / (nsp - 1);

	/* EXTENDED */
	strace->d2 = bufhdr.ILINE;
	strace->f2 = bufhdr.XLINE;
	strace->d1 = bufhdr.CDPX;
	strace->f1 = bufhdr.CDPY;
	strace->ungpow = bufhdr.SPNO;

	/* -------- */

	strace->delrt = bufhdr.DELRECT;

	return bufhdr;
}

char* getWorkingDirectory()
{
	char *buffer;
	char path[PATH_MAX];
	int pathlen;
	char *workdir = malloc(PATH_MAX * sizeof(char));
	char *result;

	buffer=(char*) calloc(100,1);
	sprintf(buffer, "pwd");

	FILE *fp = popen(buffer, "r");
	result = fgets(path, PATH_MAX, fp);
	pathlen = strlen(path);
	path[pathlen - 1] = '\0';

	strcpy(workdir, path);

	pclose(fp);

	free(buffer);
	free(result);
	return workdir;
}

void Encode4ByteString(char *str, int pos)
{
	char buf[4];
	int i;

	for (i = 0; i < 4; i++)
		buf[(4 - 1) - i] = str[pos + i];

	for (i = 0; i < 4; i++)
		str[pos + i] = buf[i];
}

/*
 * encode = 0 -> false
 * encode = 1 -> true
 */
int getDatasetDirectory(char *workdir, char *inpfilename)
{
	int wdlen;

	int bytelen;
	int i;

	const int ASCII_SPACE = 32;

	workdir = getWorkingDirectory();
	strcat(workdir, inpfilename);

	wdlen = strlen(workdir);
	bytelen = ceil((float)wdlen / 4);

//	printf("wdlen = %d\n", wdlen);

	for (i = 0; i < bytelen * 4 - wdlen; i++)
		workdir[i + wdlen + 1] = ASCII_SPACE;

//	if (encode)
//	{
//		for (i = 0; i < bytelen; i++)
//		{
//			Encode4ByteString(workdir, i * 4);
//			//printf("%x %x %x %x\n", workdir[i * 4], workdir[i * 4 + 1], workdir[i * 4 + 2], workdir[i * 4 + 3]);
//		}
//	}

	return bytelen;
}

char *EncodeToDataset(int *pathlen, char *path, char *unixname)
{
	char *dsfn = (char *) calloc(PATH_MAX, sizeof(char)); // dataset filename source
	int wdlen;
	int bytelen;

	const int ASCII_SPACE = 32;

	int i;

	strcpy(dsfn, path);
	strcat(dsfn, "/");
	strcat(dsfn, unixname);
	strcat(dsfn, "/TRC1");

	wdlen = strlen(dsfn);
	bytelen = ceil(wdlen / 4.0);

	for (i = 0; i < bytelen * 4 - wdlen; i++)
		dsfn[i + wdlen + 1] = ASCII_SPACE;

	for (i = 0; i < bytelen; i++)
	{
		Encode4ByteString(dsfn, i * 4);
//		printf("%c:%x %c:%x %c:%x %c:%x\n",
//				dsfn[i * 4], dsfn[i * 4],
//				dsfn[i * 4 + 1], dsfn[i * 4 + 1],
//				dsfn[i * 4 + 2], dsfn[i * 4 + 2],
//				dsfn[i * 4 + 3], dsfn[i * 4 + 3]);
	}

	(*pathlen) = bytelen * 4;

	return dsfn;
}

char *loadDatasetIndex(int *cindsize, FILE *cindfile)
{
	char *cindbuf;
	int indsize = 0;
	int result;

	fseeko64(cindfile, 0, SEEK_END);
	indsize = ftello64(cindfile);
//	printf("indsize = %d\n", indsize);
	result = fseeko64(cindfile, 0, SEEK_SET);

	cindbuf = (char *)calloc(indsize, 1);
	result = fread(cindbuf, 1, indsize, cindfile);

	(*cindsize) = indsize;

	return cindbuf;
}

int getIndexPathPosition(char *cindbuf, int cindsize, char *edatasetname)
{
	int dsnamelen = strlen(edatasetname);
	char *dsnamebuf = calloc(dsnamelen, 1);
	int dsnamepos = 0;

	int posinit = 5000;
	int i;

	for (i = posinit; i < cindsize - dsnamelen; i++)
	{
		memcpy(dsnamebuf, cindbuf + i, dsnamelen);

//		printf("%c %c %c %c\n", dsnamebuf[0], dsnamebuf[1], dsnamebuf[2], dsnamebuf[3]);
//		system("pause");

		if (strcmp(dsnamebuf, edatasetname) == 0)
		{
			dsnamepos = i;
			break;
		}
	}

	return dsnamepos;
}

void changeIndexDatasetName(char *cindbuf, int dsnamepos, int dssizein, int dssizeout, char *edatasetname)
{
	int i;
	int res;
	int lastpos = 0;

	memcpy(cindbuf + dsnamepos, edatasetname, dssizeout);

	res = dssizein - dssizeout;

	if (res > 0)
	{
		lastpos = dsnamepos + dssizeout;
		for (i = 0; i < res; i++)
		{
			cindbuf[lastpos + i] = 32;
		}
	}
}

void ReadPromaxDatasetHeader(PromaxTraceHeader *promaxheader, FILE *headerinpfile)
{
	byteBuffer bytebuf;
	PromaxTraceHeader bufhdr;
	int result;

	int pos = 0;

	bytebuf = mallocByteBuffer(HDRBYTES);

	result = fread(bytebuf->bytes, 1, HDRBYTES, headerinpfile);
														pos += sizeof(int); //skip
	bufhdr.END_ENS1 = ByteGetInt(bytebuf, pos);			pos += sizeof(int);
	bufhdr.SEQNO = ByteGetInt(bytebuf, pos);			pos += sizeof(int);
	bufhdr.END_ENS2 = ByteGetInt(bytebuf, pos);			pos += sizeof(int);
														pos += sizeof(int); //skip
	bufhdr.TRACENO = ByteGetInt(bytebuf, pos);			pos += sizeof(int);
														pos += sizeof(short); //skip

	bufhdr.TRCID = ByteGetShort(bytebuf, pos);			pos += sizeof(short);
	bufhdr.MTSTART = ByteGetFloat(bytebuf, pos);		pos += sizeof(float);
	bufhdr.MTEND = ByteGetFloat(bytebuf, pos);			pos += sizeof(float);
	bufhdr.TIMELENGTH1 = ByteGetFloat(bytebuf, pos);	pos += sizeof(float);
	bufhdr.TIMELENGTH2 = ByteGetFloat(bytebuf, pos);	pos += sizeof(float);
														pos += sizeof(int); //skip
	bufhdr.TSA = ByteGetFloat(bytebuf, pos);			pos += sizeof(float);
														pos += sizeof(int); //skip
														pos += sizeof(int); //skip
	bufhdr.NHST = ByteGetFloat(bytebuf, pos);			pos += sizeof(float);
														pos += sizeof(int); //skip
														pos += sizeof(int); //skip
														pos += sizeof(int); //skip
														pos += sizeof(int); //skip
	bufhdr.FFID = ByteGetInt(bytebuf, pos);				pos += sizeof(int);
	bufhdr.TRCFLD = ByteGetInt(bytebuf, pos);			pos += sizeof(int);
	bufhdr.SP = ByteGetInt(bytebuf, pos);				pos += sizeof(int);
	bufhdr.FLAG = ByteGetInt(bytebuf, pos);				pos += sizeof(int);

	bufhdr.CDP = ByteGetInt(bytebuf, pos);				pos += sizeof(int);
	bufhdr.DSREG1 = ByteGetFloat(bytebuf, pos);			pos += sizeof(float);
	bufhdr.DSREG2 = ByteGetFloat(bytebuf, pos);			pos += sizeof(float);
	bufhdr.RGE = ByteGetFloat(bytebuf, pos);			pos += sizeof(float);
	bufhdr.SES = ByteGetFloat(bytebuf, pos);			pos += sizeof(float);
	bufhdr.SDBS = ByteGetFloat(bytebuf, pos);			pos += sizeof(float);
	bufhdr.WDS = ByteGetFloat(bytebuf, pos);			pos += sizeof(float);
	bufhdr.WGD = ByteGetFloat(bytebuf, pos);			pos += sizeof(float);
	bufhdr.SRCX = ByteGetFloat(bytebuf, pos);			pos += sizeof(float);
	bufhdr.SRCXD = ByteGetDouble(bytebuf, pos);			pos += sizeof(double);
	bufhdr.SRCY = ByteGetFloat(bytebuf, pos);			pos += sizeof(float);
	bufhdr.SRCYD = ByteGetDouble(bytebuf, pos);			pos += sizeof(double);

	bufhdr.GRPX = ByteGetFloat(bytebuf, pos);			pos += sizeof(float);
	bufhdr.GRPXD = ByteGetDouble(bytebuf, pos);			pos += sizeof(double);
	bufhdr.GRPY = ByteGetFloat(bytebuf, pos);			pos += sizeof(float);
	bufhdr.GRPYD = ByteGetDouble(bytebuf, pos);			pos += sizeof(double);

	bufhdr.UTSRC = ByteGetFloat(bytebuf, pos);			pos += sizeof(float);
	bufhdr.SECSCOR = ByteGetFloat(bytebuf, pos);		pos += sizeof(float);
	bufhdr.GRPSCOR = ByteGetFloat(bytebuf, pos);		pos += sizeof(float);
	bufhdr.ILINE = ByteGetInt(bytebuf, pos);			pos += sizeof(int);
	bufhdr.XLINE = ByteGetInt(bytebuf, pos);			pos += sizeof(int);
	bufhdr.CDPX = ByteGetFloat(bytebuf, pos);			pos += sizeof(float);
	bufhdr.CDPXD = ByteGetDouble(bytebuf, pos);			pos += sizeof(double);
	bufhdr.CDPY = ByteGetFloat(bytebuf, pos);			pos += sizeof(float);
	bufhdr.CDPYD = ByteGetDouble(bytebuf, pos);			pos += sizeof(double);
	bufhdr.SPNO = ByteGetInt(bytebuf, pos);				pos += sizeof(int);
														pos += sizeof(int); //skip
	bufhdr.DELRECT = ByteGetFloat(bytebuf, pos);		pos += sizeof(float);
														pos += sizeof(int); //skip
														pos += sizeof(int); //skip

	//set dummy value

	bufhdr.dummy1_1 = 0;
	bufhdr.dummy2_2 = 0;
	bufhdr.dummy2_4 = 0;
	bufhdr.dummy3_1 = 0;
	bufhdr.dummy4_1 = 0;
	bufhdr.dummy4_2 = 0;
	bufhdr.dummy5_1 = 0;
	bufhdr.dummy5_2 = 0;
	bufhdr.dummy5_3 = 0;
	bufhdr.dummy5_4 = 0;
	bufhdr.dummy6_1 = 0;
	bufhdr.dummy6_2 = 0;
	bufhdr.dummy6_3 = 0;
	(*promaxheader) = bufhdr;

	free(bytebuf);
}

