/*
 * segy_promax.h
 *
 *  Created on: Dec 26, 2012
 *      Author: fuad
 */

#ifndef SEGY_PROMAX_H_
#define SEGY_PROMAX_H_

#include "../src_lib/segy_lib.h"
#include "SeisCompress.h"
#include "byteBuffer.h"

#define P_EOH -1630961793

int sp_lastffid;
int sp_seqno;

typedef struct PromaxTraceHeader
{
	//
	int dummy1_1;		// byte# 1-4
	int END_ENS1;		// byte# 5-8

	int SEQNO;			// byte# 9-12 PROMAX

	//
	int END_ENS2;		// byte# 13-16
	int dummy2_2;		// byte# 17-20
	int TRACENO;		// byte# 21-24 PROMAX
	short dummy2_4;		// byte# 25-26


	short TRCID;		// byte# 27-28
	float MTSTART;		// byte# 29-32
	float MTEND;		// byte# 33-36
	float TIMELENGTH1;	// byte# 37-40
	float TIMELENGTH2;	// byte# 41-44

	int dummy3_1;		// byte# 45-48

	float TSA;			// byte# 49-52

	//
	int dummy4_1;		// byte# 53-56
	int dummy4_2;		// byte# 57-60

	float NHST;			// byte# 61-64

	//
	int dummy5_1;		// byte# 65-68
	int dummy5_2;		// byte# 69-72
	int dummy5_3;		// byte# 73-76
	int dummy5_4;		// byte# 77-80

	int FFID;			// byte# 81-84
	int TRCFLD;			// byte# 85-88 //
	int SP;				// byte# 89-92

	int FLAG;			// byte# 93-96 PROMAX

	int CDP;			// byte# 97-100
	float DSREG1;		// byte# 101-104
	float DSREG2;		// byte# 105-108
	float RGE;			// byte# 109-112
	float SES;			// byte# 113-116
	float SDBS;			// byte# 117-120
	float WDS;			// byte# 121-124
	float WGD;			// byte# 125-128
	float SRCX;			// byte# 129-132
	double SRCXD;		// byte# 133-140
	float SRCY;			// byte# 141-144
	double SRCYD;		// byte# 145-152
	float GRPX;			// byte# 153-156
	double GRPXD;		// byte# 157-164
	float GRPY;			// byte# 165-168
	double GRPYD;		// byte# 169-176
	float UTSRC;		// byte# 177-180
	float SECSCOR;		// byte# 181-184
	float GRPSCOR;		// byte# 185-188
	int ILINE;			// byte# 189-192
	int XLINE;			// byte# 193-196
	float CDPX;			// byte# 197-200
	double CDPXD;		// byte# 201-208
	float CDPY;			// byte# 209-212
	double CDPYD;		// byte# 213-220
	int SPNO;			// byte# 221-224

	//
	int dummy6_1;		// byte# 225-228

	float DELRECT;		// byte# 229-232

	int dummy6_2;		// byte# 233-236
	int dummy6_3;		// byte# 237-240

}PromaxTraceHeader;

void writeTraceToDataset(segy intrace, int traceno, int ntrc,
		int seqno, int endens, int nsp, int dt, FILE *fohdr, FILE *fotrc);
void writeTraceToDataset64(segy intrace, int traceno, int ntrc,
		int seqno, int endens, int nsp, int dt, FILE *fohdr, FILE *fotrc);
void readSuTraceHeader(PromaxTraceHeader *desthdr, segy srchdr, int nsp, int dt);
void writeSegyToFloat16(char *segyFileName, char *outputFileName, int endian);
void ReadFloat16(char *traceFileName);
void ReadFloat16Header(FILE *fid, int *nsp, int *nwindows);
void getTrace16(FILE *fid, segy *trace);
int getNumberOfTraceFloat16File(FILE *inpf, int nsp, int nwindows);
void readSegyTraceHeader(PromaxTraceHeader *desthdr, segy srchdr, int nsp, int dt);
void writePromaxTraceHeader(PromaxTraceHeader hdr, FILE *fod);
void writePromaxTraceHeaderEx(PromaxTraceHeader hdr, int traceno, int seqno, int endens, FILE *fod);
void writePromaxTraceHeader64Ex(PromaxTraceHeader hdr, int traceno, int seqno, int endens, FILE *fod);
PromaxTraceHeader ReadPromaxHeader(segy *strace, PromaxTraceHeader hdr, unsigned short nsp);
char* getWorkingDirectory();
char *EncodeToDataset(int *pathlen, char *path, char *unixname);
char *loadDatasetIndex(int *cindsize, FILE *cindfile);
int getIndexPathPosition(char *cindbuf, int cindsize, char *edatasetname);
void changeIndexDatasetName(char *cindbuf, int dsnamepos, int dssizein, int dssizeout, char *edatasetname);
void ReadPromaxDatasetHeader(PromaxTraceHeader *promaxheader, FILE *headerinpfile);
#endif /* SEGY_PROMAX_H_ */
