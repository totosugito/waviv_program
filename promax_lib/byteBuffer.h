/*
 * byteBuffer.h
 *
 *  Created on: Dec 6, 2012
 *
 */

#ifndef BYTEBUFFER_H_
#define BYTEBUFFER_H_

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <endian.h>
#include "swapbytes_module.h"

typedef struct byte_buf {
    size_t  len;
    int position;
    uint8_t  *bytes;
} byteBufferStruct, *byteBuffer;

void printByteBuffer(byteBuffer bb, char *name);

void printBytes(uint8_t *buff, size_t len, char *name);

byteBuffer mallocByteBuffer(size_t len);

byteBuffer hexStringToBytes(char *inhex);

byteBuffer bytesToBytes(void *bytes, size_t len);

int bytesAreEqual(byteBuffer b1, byteBuffer b2);

char *bytesToHexString(byteBuffer bytes);

void BytePut(byteBuffer bb, uint8_t *src, size_t size);
void BytePutShort(byteBuffer bb, short src);
void BytePutShortEx(byteBuffer bb, short src, int endian);
void BytePutInt(byteBuffer bb, int src);
void BytePutIntEx(byteBuffer bb, int src, int endian);
void BytePutFloat(byteBuffer bb, float src);
void BytePutFloatEx(byteBuffer bb, float src, int endian);
void BytePutDoubleEx(byteBuffer bb, double src, int endian);
short ByteGetShort(byteBuffer bb, int pos);
int ByteGetInt(byteBuffer bb, int pos);
float ByteGetFloat(byteBuffer bb, int pos);
double ByteGetDouble(byteBuffer bb, int pos);
#endif /* BYTEBUFFER_H_ */
