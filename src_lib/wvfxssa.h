/*
 * wvfxssa.h
 *
 *  Created on: Apr 3, 2013
 *      Author: toto
 */

#ifndef WVFXSSA_H_
#define WVFXSSA_H_


#include <fftw3.h>
#include <lapacke.h>
#include "waviv_common.h"

#define min(a,b) ((a)>(b)?(b):(a))
void run_cadzow_v3(float **fdata, int ntrc, int nsp,
		int rank, int spatspan, int tempspan, float fftpad, float epsilon,
		int pocs, int maxiter, int interp, int old, int dealiased);
void svd(double** data, int row, int col, double** S, double** U, double** VT);
void svd_inv(double** data, int m, int n, double** S, double** U, double** VT);
double** rankApproximation(double** data,int row,int col, int rank);
double** getrank(double** u,double** s,double** v, int row, int col, int rank);
void ffttemporal(double** inputtx, double** outputreal, double** outputimag, int ntraces, int ns,float pad);
void iffttemporal(double** outputtx, double** inputreal, double** inputimag, int ntraces, int ns,float pad);
void cadzow2d(double** data,double** outdata, int row, int col, int nbeg, int nend,int rank,int maxiter);
void cadzow2dint(double** data,double** outdata,int* OBS,int row, int col, int nbeg, int nend,int rank, int pocs, int maxiter);
double** weightmaskspat(int row, int col);
double** weightmasktemp(int row, int col);
void reinsert(double* sv1,double* sv0, double* sobs, int* OBS,double a,int n);
void reinsert_pocs(double* sv1,double* sv0, double* sobs, int* OBS,double a,int n);
void reinsertamp(double* sv0, double* sobs,double a,int n);
void gulunay(double** data,int row,int col,float epsilon,float pad);
void suamp(dcomplex** input, double** output,int row,int col);
void norm2max2dc(dcomplex** data, int row,int col);
void savearray2dc(char* outfilename,dcomplex** array,int row,int col);
void strecth(double** data, int row, int col, double nMax,double nMin);
void cadzow2dold(double** data,double** outdata, int row, int col, int nbeg, int nend,int rank);
void reinsert2d(double** sv0, double** sobs, int* OBS,double a,int nrow, int ncol);
double** span(double** data, int row,int col,int span);
double** rmspan(double** data, int row,int col,int span);
void dealiasedcadzow2dint(double** data,double** dataprecond,double** outdata,int* OBS,int row, int col, int nbeg, int nend,int rank, int maxiter);
double** getprecond(double** data,int row,int col, int rank);
void preparepreconditioner(double** datain, double** realout, double** imagout, int row,int col);
#endif /* WVFXSSA_H_ */
