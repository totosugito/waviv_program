/*
 * wave_3dlib.h
 *
 *  Created on: Dec 28, 2011
 *      Author: toto
 */

#ifndef WAVE_3DLIB_H_
#define WAVE_3DLIB_H_

#include "segy_lib.h"
#include "iolibrary.h"

typedef struct pos3ddata{
	int xpos;
	int ypos;
	int iline;
	int xline;
	int trace;
	float sx;
	float sy;
}pos3ddata;

//typedef struct cdp3Header{
//	int iline;	//inline number
//	int cdp;	//[cdp trmin_pos trmax_pos]
//	int trmin;
//	int trmax;
//}cdp3Header;

typedef struct cadzow3dHeader
{
	int ncdp;
	int *cdp;
	int *trmin;
	int *trmax;
}cadzow3dHeader;

// ----------------- data type for filter gather in offset ------------------------
typedef struct hdrgauss{
	int cdp;
	int iinline;
	int ixline;

	float offset;
	float sx;
	float sy;
}hdrgauss;

typedef struct hdrinfo{
	int cdpmin;
	int cdpmax;
	int inlinemin;
	int inlinemax;
	int xlinemin;
	int xlinemax;

	float offsetmin;
	float offsetmax;
}hdrinfo;
//-------------------------------------------------------------------------------
//void cadzow3d_header_create(char *segyInput, char *cadzow3dHeaderInput,
//		int endian, int idxinline, int idxcdp);

pos3ddata **pos3ddata_create(int ny, int nx);
void pos3ddata_free(pos3ddata **data);
void setValuePositionInXline(pos3ddata **data,
		int min_inline, int min_xline,
		int curr_inline, int curr_xline,
		int scalco,
		float sx, float sy,
		int itrace);
void pos3ddata_print(pos3ddata **data, int inpos1, int inpos2, int xpos1, int xpos2);
void pos3ddata_setindex(pos3ddata **data, int row, int col);
void pos3ddata_write(pos3ddata **data, int row, int col, char *cfout,
		int min_iline, int max_iline, int min_xline, int max_xline);
pos3ddata **pos3ddata_read(char *cfinp, int *irow, int *icol,
		int *imin_iline, int *imax_iline, int *imin_xline, int *imax_xline);
void pos3ddata_windowing(pos3ddata **data, int pos1i, int pos2i, int pos1x, int pos2x, int space,
		pos3ddata **windowpos);

pos3ddata **pos3ddata_getradiusWithSpace(pos3ddata **data, int idxxline, int idxinline,
		int space, int lenInline, int lenXline,
		int nignore_neighbor,
		int *nneighbor, int *centerPos);
pos3ddata **pos3ddata_getradiusWithRadius(pos3ddata **data, int idxxline, int idxinline,
		int radius, float distance,
		int lenInline, int lenXline,
		int nignore_neighbor,
		int *nneighbor, int *centerPos, int verbose);

int getPos1(int idx, int space);
int getPos2(int idx, int space, int idxmax);

float *gaussianKernel(float sigma, int rkernel);
float normgaussian1d(float r,float sigma);
float gaussian1d(float amp, float r,float sigma);
#endif /* WAVE_3DLIB_H_ */
