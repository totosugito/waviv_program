/*
 * binaryLib.h
 *
 *  Created on: Feb 8, 2013
 *      Author: toto
 */

#ifndef BINARYLIB_H_
#define BINARYLIB_H_
#include <stdio.h>
#include <stdbool.h>
#include <time.h>

#include <par.h>
#define COLUMN_GROUPH 4
#define MAX_NAME_LEN 400
#define MAX_COMMAND 4096

FILE *openFile(char *cfile, char *otype );
int **readBinaryGrouphFile(char *cbinfile, int *nsp, int *ntrc, float *dt, int *ngrouph);
void writeBinaryGrouphFile(int **grouph, int irow, int icol, char *cname, int nsp, float dt);

float **readBinaryTracesFile(char *ctraces, int nsp, int ntrc);
void writeBinaryTracesFile(char *ctraces, float **traces, int nsp, int ntrc);
void writeBinaryTracesFileTes(char *ctraces, float **traces, int nsp, int ntrc);

void printGrouphData(int **data, int nrow, int ncol);
bool checkNewShot(int *ithdr, int keydata, int tmpkey);


char *cTrFile;
char *cHdrFile;
char *cGrouphFile;
char *createAutoFileNameWithDir(char *dirloc);
char *createAutoFileName();
void createAutomaticTemporaryFile();
void removeTemporaryFile();
void freeTemporaryFile();
#endif /* BINARYLIB_H_ */
