/*
 * iolibrary.c
 *
 *  Created on: Jan 30, 2012
 *      Author: toto
 */
#include "iolibrary.h"

char *createSlicingFileName(char *ctmpfile, int tidx1, int tidx2)
{
	char *strfilename;

	strfilename = (char*) calloc(100, sizeof(char));
	sprintf(strfilename, "%s.tslice_%s_%s", ctmpfile, number2string(tidx1), number2string(tidx2));

	return(strfilename);
}

char *number2string(int number)
{
	char *buff;

	buff = (char*) calloc(10, sizeof(char));
	if(number>=0 && number<10)
		sprintf(buff, "000%i", number);
	else if(number>=10 && number<100)
		sprintf(buff, "00%i", number);
	else if(number>=100 && number<1000)
		sprintf(buff, "0%i", number);
	else
		sprintf(buff, "%i", number);

	return(buff);
}

char *tmp_mergeSliceFileName(char *ctmpfile)
{
	char *buff;

	buff = (char*) calloc(100, sizeof(char));
	sprintf(buff, "%s_mergefile.bin", ctmpfile);

	return(buff);
}

void saveBinaryFile(char *outfile, float **data, int nrow, int ncol)
{
	FILE *fod=NULL;

	fod = fopen(outfile, "w");
	fwrite(data[0], sizeof(float), nrow*ncol, fod);
	fclose(fod);
}
