/*
 * cedSmoothingLib.c
 *
 *  Created on: Dec 18, 2012
 *      Author: toto
 */

#include "cedSmoothingLib.h"
#include "../src_lib/iolibrary.h"

float *gDerivative(int order, float *x, float *Gs, float scale, int nx)
{
	float *r=NULL;
	int i;

	r = (float*) calloc(nx, sizeof(float));
	if(order==0){
		memcpy(r, Gs, nx*sizeof(float));
	}
	else if(order==1) {
		for(i=0; i<nx; i++)
			r[i] = -x[i] / powf(scale, 2.0) * Gs[i];
	}
	else if(order==2) {
		for(i=0; i<nx; i++)
			r[i] = (powf(x[i], 2.0) - powf(scale, 2.0) / powf(scale, 4.0)) * Gs[i];
	}
	else {
		fprintf(stderr, "only derivatives up to second order are supported");
		exit(0);
	}

	return r;
}

float **convSepBrd(float **X, int irow, int icol,
		float *w1, int nw1, float *w2, int nw2, int *nrow, int *ncol)
{
	int i, j;
	int *iind1, *jind1;
	int niind, njind;
	float **fwb, **g;

	float M, N;	//size of input array
	int K, L;
	float tmp1;
//	int nRow, nCol;

	// get row and column size
	N = (float) irow;
	M = (float) icol;

	K = (nw1 - 1)/2;
	L = (nw2 - 1)/2;

	niind = N + 2.0*K;
	njind = M + 2.0*K;
	iind1 = (int*) calloc(niind, sizeof(int));
	jind1 = (int*) calloc(njind, sizeof(int));

	for(i=0; i<niind; i++)
	{
		tmp1 = i+1-K;
		if(tmp1<1)	iind1[i] = 1.0;
		else if(tmp1>N)	iind1[i] = N;
		else	iind1[i] = tmp1;
	}

	for(i=0; i<njind; i++)
	{
		tmp1 = i+1-L;
		if(tmp1<1)	jind1[i] = 1.0;
		else if(tmp1>M)	jind1[i] = M;
		else	jind1[i] = tmp1;
	}

	fwb = alloc2float(njind, niind);
	for(i=0; i<niind; i++)
	{
		for(j=0; j<njind; j++)
		{
			fwb[i][j] = X[iind1[i]-1][jind1[j]-1];
//			if(isnan(fwb[i][j]))
//				err("fwb is nan ");
//			if(isinf(fwb[i][j]))
//				err("fwb is inf ");
		}
	}

	g = conv2f(w1, nw1, w2, nw2, fwb, niind, njind, 2, nrow, ncol);

//	for(i=0; i<*nrow; i++)
//	{
//		for(j=0; j<*ncol; j++)
//		{
//			if(isnan(g[i][j]))
//				err("g is nan ");
//		}
//	}
	//	print1i("iind", iind1, niind);
	//	print1i("jind", jind1, njind);
//	print2f("X", X, irow, icol);
//	print2f("fwb", fwb, niind, njind);
//	(*nrow) = nRow;
//	(*ncol) = nCol;
	free1int(iind1);
	free1int(jind1);
	free2float(fwb);
	return(g);
}

float **gD(float **X, int irow, int icol, float scale, int ox, int oy,
		int *grow, int *gcol)
{
	float sumGs;
	float *x, *Gs;
	float *Gsx, *Gsy;
	float **g;

	int i;
	int K, nK;

	// Gaussian (Derivative) Convolution
	K = (int) (ceil(3.0 * scale )); //span
	nK = 2*K + 1;
	Gs = (float*) calloc(nK, sizeof(float));
	x = (float*) calloc(nK, sizeof(float));

	sumGs = 0.0;
	for(i=0; i<nK; i++)
	{
		x[i] = (float) (-K+i);
		Gs[i] = expf(- powf(x[i], 2.0) / (2.0*powf(scale, 2.0)));
		sumGs += Gs[i];
	}
//	print1f("X", x, nK);

	for(i=0; i<nK; i++)
	{
		Gs[i] = Gs[i] / sumGs;
	}

	Gsx = gDerivative(ox, x, Gs, scale, nK);
	Gsy = gDerivative(oy, x, Gs, scale, nK);
	g = convSepBrd(X, irow, icol, Gsx, nK, Gsy, nK, grow, gcol);


//	print1f("Gs", Gs, nK);
//	print1f("Gsx", Gsx, nK);
//	print1f("Gsy", Gsy, nK);
	free(x);
	free(Gs);
	free(Gsx);
	free(Gsy);
	return (g);
}

void runCED(float **traces, int ntrc, int nsp, float k,
		float obsscale, float intscale, float stepsize, int nosteps,
		int verbose)
{
	int itr;
	int i, j;
	int xyrow, xycol;
	float eps;

	float **Rx=NULL, **Ry=NULL;
	float **Rx2=NULL, **Ry2=NULL, **Rxy=NULL;
	float **s11=NULL, **s12=NULL, **s22=NULL;
	float **d11=NULL, **d12=NULL, **d22=NULL;
	float alpha, el1, el2, c1, c2;
	float **dtnldStep;

	if(ntrc<3)
		return;

	eps = 2.2204e-16;
	Rx2 = alloc2float(nsp, ntrc);
	Ry2 = alloc2float(nsp, ntrc);
	Rxy = alloc2float(nsp, ntrc);
	d11 = alloc2float(nsp, ntrc);
	d12 = alloc2float(nsp, ntrc);
	d22 = alloc2float(nsp, ntrc);

	for(itr=0; itr<nosteps; itr++) //loop over steps
	{
		if(verbose)
			fprintf(stderr, "Run Steps [ %i / %i ] \n", itr+1, nosteps);

		//konvolusi 2D input dengan gaussian derivative kernel horizontal
		Rx = gD( traces, ntrc, nsp, obsscale, 1, 0, &xyrow, &xycol);

		//konvolusi 2D input dengan gaussian derivative kernel vertikal
		Ry = gD( traces, ntrc, nsp, obsscale, 0, 1, &xyrow, &xycol);

		if(xyrow!=ntrc || xycol!=nsp)
		{
			fprintf(stderr, "xyrow!=ntrc || xycol!=nsp !\n");
			exit(0);
		}

		for(i=0; i<ntrc; i++)
		{
			for(j=0; j<nsp; j++)
			{
				Rx2[i][j] = Rx[i][j]*Rx[i][j];
				Ry2[i][j] = Ry[i][j]*Ry[i][j];
				Rxy[i][j] = Rx[i][j]*Ry[i][j];
			}
		}

		//konvolusi 2D input dengan gaussian derivative kernel horizontal pangkat 2
		s11 = gD(Rx2, ntrc, nsp, intscale, 0, 0, &xyrow, &xycol);

		//konvolusi 2D input dengan gaussian derivative kernel perkalian
		s12 = gD(Rxy, ntrc, nsp, intscale, 0, 0, &xyrow, &xycol);

		//konvolusi 2D input dengan gaussian derivative kernel vertikal pangkat 2
		s22 = gD(Ry2, ntrc, nsp, intscale, 0, 0, &xyrow, &xycol);

		for(i=0; i<ntrc; i++)
		{
			for(j=0; j<nsp; j++)
			{
				alpha = sqrt( pow(s11[i][j]-s22[i][j], 2.0) + (4.0*pow(s12[i][j], 2.0)) );

				el1 = 0.5 * (s11[i][j] + s22[i][j] - alpha ); //eigen1
				el2 = 0.5 * (s11[i][j] + s22[i][j] + alpha ); //eigen2
				c1 = MAX(0.01, 1-exp( -pow((el1-el2),2.0) / pow(k,2.0) ));
				c2 = 0.01;
//				if(i==0 && j==0)
//					fprintf(stderr, "alpha=%e; el1=%e; el2=%e; c1=%e; c2=%e; s11=%e; s22=%e; s12=%e;\n",
//							alpha, el1, el2, c1, c2, s11[0][0], s22[0][0],s12[0][0]);
				d11[i][j] = 0.5 * (c1+c2+(c2-c1)*(s11[i][j]-s22[i][j])/(alpha+eps)); //tensor
				d12[i][j] = (c2-c1)*s12[i][j]/(alpha+eps); //tensor
				d22[i][j] = 0.5 * (c1+c2-(c2-c1)*(s11[i][j]-s22[i][j])/(alpha+eps)); //tensor
			}
		}

		dtnldStep = tnldStep(traces, ntrc, nsp, d11, d12, d22);
		for(i=0; i<ntrc; i++)
		{
			for(j=0; j<nsp; j++)
			{
				if(isnan(dtnldStep[i][j]) || isinf(dtnldStep[i][j])) //trap nan and inv data
					dtnldStep[i][j] = 0.0;
				traces[i][j] = traces[i][j] + stepsize*dtnldStep[i][j];
			}
		}

		free2float(Rx);
		free2float(Ry);
		free2float(s11);
		free2float(s12);
		free2float(s22);
		free2float(dtnldStep);
	}


	free2float(Rx2);
	free2float(Ry2);
	free2float(Rxy);
	free2float(d11);
	free2float(d12);
	free2float(d22);
}

int *getTranslateImageIndex(int iLen, int dij)
{
	int *result;
	int i;

	result = alloc1int(iLen);
	for(i=0; i<iLen; i++)
	{
		if(dij>0)
		{
			result[i] = dij+i;
			if(result[i]>=iLen)
				result[i] = iLen-1;
		}
		else if(dij<0)
		{
			result[i] = dij+i;
			if(result[i]<0)
				result[i] = 0;
		}
		else
			result[i] = i;
	}
	return(result);
}

float **translateImage(float **f, int irow, int icol, int di, int dj)
{
	float **result=NULL;
	int *iind, *jind;
	int i,j;

	result = alloc2float(icol, irow);
	iind = getTranslateImageIndex(irow, di);
	jind = getTranslateImageIndex(icol, dj);

	for(i=0; i<irow; i++)
		for(j=0; j<icol; j++)
			result[i][j] = f[iind[i]][jind[j]];

	free1int(iind);
	free1int(jind);
	return(result);
}

float **tnldStep(float **L, int irow, int icol, float **a, float **b, float **c )
{
	int i, j;
	float **result=NULL;
	float **Lpc=NULL, **Lpp=NULL, **Lcp=NULL, **Lmp=NULL;
	float **Lmc=NULL, **Lmm=NULL, **Lcm=NULL, **Lpm=NULL;
	float **amc=NULL, **apc=NULL, **bmc=NULL, **bcm=NULL;
	float **bpc=NULL, **bcp=NULL, **ccp=NULL, **ccm=NULL;

    Lpc = translateImage( L, irow, icol, 1, 0 );
    Lpp = translateImage( L, irow, icol, 1, 1 );
    Lcp = translateImage( L, irow, icol, 0, 1 );
    Lmp = translateImage( L, irow, icol, -1, 1 );

    Lmc = translateImage( L, irow, icol, -1, 0 );
    Lmm = translateImage( L, irow, icol, -1, -1 );
    Lcm = translateImage( L, irow, icol, 0, -1 );
    Lpm = translateImage( L, irow, icol, 1, -1 );

    amc = translateImage( a, irow, icol, -1, 0 );
    apc = translateImage( a, irow, icol, +1, 0 );
    bmc = translateImage( b, irow, icol, -1, 0 );
    bcm = translateImage( b, irow, icol, 0, -1 );

    bpc = translateImage( b, irow, icol, +1, 0 );
    bcp = translateImage( b, irow, icol, 0, +1 );
    ccp = translateImage( c, irow, icol, 0, +1 );
    ccm = translateImage( c, irow, icol, 0, -1 );

	result = alloc2float(icol, irow);
	for(i=0; i<irow; i++)
	{
		for(j=0; j<icol; j++)
		{
			result[i][j] = -0.25 * (bmc[i][j]+bcp[i][j]) * Lmp[i][j] +
		             0.5 * (ccp[i][j]+c[i][j])   * Lcp[i][j] +
		             0.25 * (bpc[i][j]+bcp[i][j]) * Lpp[i][j] +
		             0.5 * (amc[i][j]+a[i][j])   * Lmc[i][j] -
		             0.5 * (amc[i][j]+2*a[i][j]+apc[i][j]+ccm[i][j]+2*c[i][j]+ccp[i][j]) * L[i][j] +
		             0.5 * (apc[i][j]+a[i][j])   * Lpc[i][j] +
		             0.25 * (bmc[i][j]+bcm[i][j]) * Lmm[i][j] +
		             0.5 * (ccm[i][j]+c[i][j])   * Lcm[i][j] -
		             0.25 * (bpc[i][j]+bcm[i][j]) * Lpm[i][j];
		}
	}

	free2float(Lpc);
	free2float(Lpp);
	free2float(Lcp);
	free2float(Lmp);
	free2float(Lmc);
	free2float(Lmm);
	free2float(Lcm);
	free2float(Lpm);
	free2float(amc);
	free2float(apc);
	free2float(bmc);
	free2float(bcm);
	free2float(bpc);
	free2float(bcp);
	free2float(ccp);
	free2float(ccm);

	return(result);
}
