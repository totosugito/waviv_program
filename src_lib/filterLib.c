/*
 * filterLib.c
 *
 *  Created on: Apr 13, 2012
 *      Author: toto
 */
#include "filterLib.h"

void processEigenFilter(float **data, int rank, int nx, int nz, int extract)
{
	int ix, iz, rk;
	int row, col;
	double **ddata=NULL;
	double **u;
	double **v, *s;
	double *U1, *V1;

	ddata = alloc2double(nz, nx);
	u = alloc2double(nx, nx);
	v = alloc2double(nz, nz);
	s = alloc1double(nz);
	U1 = alloc1double(nx);
	V1 = alloc1double(nz);

	/* convert data to double */
	for(ix=0; ix<nx; ix++)
		for(iz=0; iz<nz; iz++)
			ddata[ix][iz] = (double) (data[ix][iz]);

	svdlapackd(ddata, nx, nz, u, s, v);

	if(extract>0)
	{
		memset((void*)data[0], 0, nx*nz*sizeof(float));
		for (row=0; row<nx; row++)
			U1[row] = u[row][rank];

		for (col=0; col<nz; col++)
			V1[col] = v[col][rank];

		for(row=0;row<nx;row++)
			for(col=0;col<nz;col++)
				data[row][col] = (float) (V1[col]*U1[row]*s[rank]);
	}
	else
	{
		memset((void*)data[0], 0, nx*nz*sizeof(float));
        for(rk=0;rk<=rank;rk++)
        {
    		for (row=0; row<nx; row++) U1[row] = u[row][rk];
    		for (col=0; col<nz; col++) V1[col] = v[col][rk];

            for(row=0;row<nx;row++)
                for(col=0;col<nz;col++)
                    data[row][col] += (float) (V1[col]*U1[row]*s[rk]);
        }
	}

	/* free allocated memory */
	free2double(ddata);
	free2double(u);
	free2double(v);
	free1double(s);
	free1double(U1);
	free1double(V1);

}

double **getrank(double** data, int row, int col, int rank)
{
	double **u, *s, **v, **su;
	int i,j;

	u = alloc2double(row, row);
	s = alloc1double(row);
	v = alloc2double(col, col);

	//svd(data, row, col, s, u, v);
	svdlapackd(data, row, col, u, s, v);

	su = alloc2double(col, row);

	for(i=0;i<row;i++)
		for(j=0;j<col;j++)
			su[i][j]=v[rank][j] * u[i][rank] * s[rank];

	free2double(u);
	free1double(s);
	free2double(v);
	return su;
}

double **rankApproximation(double **data,int row,int col, int rank)
{
	double **out, **eigenimage;
	int iter, i, j;

	out = alloc2double(col, row);
	memset(out[0], 0, row*col*sizeof(double));

	for(iter=0; iter<rank; iter++)
	{
		eigenimage = getrank(data, row, col, iter);
		for(i=0;i<row;i++)
		{
			for(j=0; j<col; j++)
				out[i][j] += eigenimage[i][j];
		}
		free2double(eigenimage);
	}
	return out;
}

void rankApproximationf(float **data,int row,int col, int rank)
{
	double eigenimage, **tmpdata;
	double **u, *s, **v;
	int i,j,k;

	tmpdata = alloc2double(col, row);
	for (i=0; i<row; i++)
		for(j=0; j<col; j++)
			tmpdata[i][j] = (double) (data[i][j]);

	u = alloc2double(row, row);
	s = alloc1double(row);
	v = alloc2double(col, col);

	svdlapackd(tmpdata, row, col, u, s, v);

	memset(data[0], 0, row*col*sizeof(float));
	for(i=0; i<row; i++)
	{
		for (j=0; j<col; j++)
		{
			eigenimage = 0.0;
			for(k=0; k<rank; k++)
				eigenimage += v[k][j] * u[i][k] * s[k];

			data[i][j] = (float) (eigenimage);
		}
	}

	free2double(u);
	free1double(s);
	free2double(v);
	free2double(tmpdata);
}

void rankApproximationd(double **data,int row,int col, int rank)
{
	double eigenimage;
	double **u, *s, **vt;
	int i,j,k;

	u = alloc2double(row, row);
	s = alloc1double(row);
	vt = alloc2double(col, col);

	svdlapackd(data, row, col, u, s, vt);

	memset(data[0], 0, row*col*sizeof(float));
	for(i=0; i<row; i++)
	{
		for (j=0; j<col; j++)
		{
			eigenimage = 0.0;
			for(k=0; k<rank; k++)
			{
				eigenimage += vt[k][j] * u[i][k] * s[k];
			}
			data[i][j] = eigenimage;
		}
	}

//	printf("\nU\n");
//	for (i=0; i<row; i++)
//	{
//		for (j=0; j<row; j++)
//			printf("%3.3f   ", u[i][j]);
//		printf("\n");
//	}
//	printf("\nS\n");
//	for (j=0; j<row; j++)
//		printf("%3.3f   ", s[j]);
//	printf("\n");
//
//	printf("\nV\n");
//	for (i=0; i<col; i++)
//	{
//		for (j=0; j<col; j++)
//			printf("%3.3f   ", v[i][j]);
//		printf("\n");
//	}

	free2double(u);
	free1double(s);
	free2double(vt);
}

