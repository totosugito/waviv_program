/*
 * shiftingLib.h
 *
 *  Created on: Apr 13, 2012
 *      Author: toto
 */

#ifndef SHIFTINGLIB_H_
#define SHIFTINGLIB_H_
#include <stdlib.h>
#include <math.h>
#ifdef SEISUNIX
#include "segy_lib.h"
#else
#include <cpromax.h>
#endif
#include "printLib.h"

float **returnDipSteer(float **data_ori, float **dipsteer_data, int ntrc, int ns,
		int *shifting_pos, int nshifting, int midshifting);
float **runDipSteer(float **data, int ntrc, int ns, int nshifting, int midshifting, int *shiftingPos);
float *checkShifting(float *data, float *dataref, int ndata,
		int nshifting, int midshifting, int *ishifting);
float *getShiftingDataAtPosition(float *data, int ndata, int i);
float *shiftingData(float *data, int ndata, int i, int midshifting, int *dpos);
float *shiftingReferenceData(float *dataref, int ndata, int i, int dpos, int midshifting,
		int *nrefdata);
float coefcorrdata(float *x, float *y, int ndata);
int getMaximumPosition(float *data, int ndata);
float *stackingData(float **data, int ntrc, int ns);
#endif /* SHIFTINGLIB_H_ */
