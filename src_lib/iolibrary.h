/*
 * iolibrary.h
 *
 *  Created on: Jan 30, 2012
 *      Author: toto
 */

#ifndef IOLIBRARY_H_
#define IOLIBRARY_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
char *createSlicingFileName(char *ctmpfile, int tidx1, int tidx2);
char *number2string(int number);
char *tmp_mergeSliceFileName(char *ctmpfile);
void saveBinaryFile(char *outfile, float **data, int nrow, int ncol);
//int uchar2int(unsigned char *data, int ipos, int nbyte, int endian);
#endif /* IOLIBRARY_H_ */
