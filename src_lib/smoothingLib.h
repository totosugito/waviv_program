/*
 * smoothingLib.h
 *
 *  Created on: Jan 18, 2012
 *      Author: toto
 */

#ifndef SMOOTHINGLIB_H_
#define SMOOTHINGLIB_H_

#include "par.h"

void GaussianSmoothY(float **input, float **output, int rows, int cols, float sigma, float intscale);
void GaussianSmoothX(float **input, float **output, int rows, int cols, float sigma, float intscale);
void GaussianDerivativeY(float **input, float **output, int rows, int cols, float sigma, float intscale);
void GaussianDerivativeX(float **input, float **output, int rows, int cols, float sigma, float intscale);

void ConstructDiffusionTensor(float **S11, float **S22, float **S12,
		float **D11, float **D22, float **D12,
		float k, int row, int col);

void gaussiankernelv(float *kernel, int ksize, float sigma);
void gaussianderivative1kernelv(float *kernel, int ksize, float sigma);
void convolve(float* function, int flength, float* kernel, int klength);
void convolven1(float** input, int n1, int n2, float* kernel, int klength);
void convolven2(float** input, int n1, int n2, float* kernel, int klength);
void getcol(float** input, int colselect, float* coldata, int nrow);
void setcol(float** input, int colselect, float* coldata, int nrow);
void timesarray(float** data1,float** data2, float** result, int row, int col);
void constructcedtensor(float** S11, float** S22, float** S12,
		float** D11, float** D22, float** D12,
		float k, int row, int col);
void WeickertCED(float** data, float** a, float** c, float** b,
		float dt, int row, int col);

void wvstrucsmooth(float **data, int ny, int nx,
		float sigma, float sigmaderiv, float k, float dts,
		int klength, int nstep);

void k1k2filter(float **data, int nx1, int nx2, int nx1fft, int nx2fft,
		float nord, float alpha, int nK1, int nK2, int nxm, int nzm,
		float cf1, float cf2, float onfft);
void circular_filter(int nx, int nz, int xc, int zc, float a1, float b1,
		float nord, float alpha, float **filt);
float **computeAmplitudeSpectrum(complex **ct, int nrow, int ncol, int nx1fft);
float **smooth2(float **v0, int row, int col, float r1, float r2, int *win, float rw);

void smooth2SeisUn(float **v, int nrow, int ncol,
		float rw, float r1, float r2);
#endif /* SMOOTHINGLIB_H_ */
