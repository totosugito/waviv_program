/*
 * fxemd_segy.h
 *
 *  Created on: Feb 18, 2010
 *      Author: toto
 */

#ifndef SEGY_LIB_H_
#define SEGY_LIB_H_

#include "su.h"
#include "segy.h"
#include "tapesegy.h"
#include "tapebhdr.h"
#include "bheader.h"
#include "header.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>

#define roundfunc(x) ((int)((x)>0.0?(x)+0.5:(x)-0.5))

char **alloc2char(int col, int row);
void free2char(char **A);

void segy_lib_err(char *namefunc, char *report);

/* Reading SEGY Subroutine prototypes */
void readEbcdicHeader(FILE *inpf, char cebcdic[]);
void readBinaryHeader(FILE *inpf, int endian, bhed *bh, int *nsegy);
void readTraceSegy(FILE *inpf, int endian, int ns, int format, int nsegy, segy *trace);
void readSegy(FILE *inpf, int endian, int ns, int format, int nsegy, segy *tr,
		char *header, float *data);

void writeSegyTraceData(FILE *fod, float **traces, char **headers,
		int ntrc, int nsp, int endian, int nsegy);
void writeSuTraceData(FILE *fod, float **traces, char **headers,
		int ntrc, int nsp);

/* Writing SEGY Subroutine prototypes */
void writeEbcdicHeader(FILE *outf, char cebcdic[]);
void writeBinaryHeader(FILE *outf, int endian, bhed *binhead);
void writeTraceSegy(FILE *outf, int endian, segy *trace, int nsegy, int ns);
void writeSegy(FILE *outf, int endian, segy *trace, int nsegy, int ns,
		char *header, float *data);

/*Other Subroutine */
int getNumberOfTraceSegyFile(FILE *inpf, int nsegy); /*get number of trace */
int getNumberOfTraceSuFile(FILE *ifile);

/*binary header conversion */
void tapebhed_to_bhed(const tapebhed *tapebhptr, bhed *bhptr);
void bhed_to_tapebhed(const bhed *bhptr, tapebhed *tapebhptr);

/*trace conversion */
void tapesegy_to_segy(const tapesegy *tapetrptr, segy *trptr);
void segy_to_tapesegy(const segy *trptr, tapesegy *tapetrptr, size_t nsegy);

/* data format conversion */
void ibm_to_float(int from[], int to[], int n, int endian);
void long_to_float(long from[], float to[], int n, int endian);
void short_to_float(short from[], float to[], int n, int endian);
void integer1_to_float(signed char from[], float to[], int n);
void float_to_ibm(int from[], int to[], int n, int endian);


//void swaphval(segy *tp, int index);

/*message*/
void warning_message(char *str);
void gotoTraceSegyPosition(FILE *fin, int loc, int nsegy);
void gotoTraceSuPosition(FILE *fin, int loc, int nsegy);

/* Globals */
tapesegy tapetr;
tapebhed tapebh;
segy tr;
bhed bh;

int computeIntHeaderAtByte(FILE *fin, int pos, int nbyte, int endian, int tsigned);
float computeIBMFloatHeaderAtByte(FILE *fin, int pos, int nbyte, int endian);
void nextTrace(FILE *fin, int nsegy);

int char2int(char *data, int pos, int npos, int endian);
int uchar2int(unsigned char *data, int pos, int npos, int endian);
float uchar2float(unsigned char *data, int pos, int npos, int endian);
float uchar2ibm(unsigned char *data, int pos, int npos, int endian);
float int2float(int data);
float ibm2float(int ifrom);

void int2uchar(int ivalue, unsigned char *data, int pos, int npos, int endian);
void int2char(int ivalue, char *data, int pos, int npos, int endian);
#endif /* SEGY_LIB_H_ */
