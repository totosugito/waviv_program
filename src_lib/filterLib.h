/*
 * filterLib.h
 *
 *  Created on: Apr 13, 2012
 *      Author: toto
 */

#ifndef FILTERLIB_H_
#define FILTERLIB_H_
#include "svdlapack.h"


void processEigenFilter(float **data, int rank, int nx, int nz, int extract);
double **getrank(double** data, int row, int col, int rank);
double **rankApproximation(double **data,int row,int col, int rank);
void rankApproximationf(float **data,int row,int col, int rank);
void rankApproximationd(double **data,int row,int col, int rank);
#endif /* FILTERLIB_H_ */
