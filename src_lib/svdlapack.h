/*
 * svdlapack.h
 *
 *  Created on: Jul 19, 2011
 *      Author: toto
 */

#ifndef SVDLAPACK_H_
#define SVDLAPACK_H_

#ifdef SEISUNIX
#include "segy_lib.h"
#else
#include <cpromax.h>
#endif


void svdlapackd(double **data, int irow, int icol,
		double **U, double *S, double **VT);

/* DGESVD prototype */
extern void dgesvd_( char* jobu, char* jobvt, int* m, int* n, double* a,
		int* lda, double* s, double* u, int* ldu, double* vt, int* ldvt,
		double* work, int* lwork, int* info );

#endif /* SVDLAPACK_H_ */
