/*
 * fftwLib.h
 *
 *  Created on: Jul 10, 2012
 *      Author: toto
 */

#ifndef FFTWLIB_H_
#define FFTWLIB_H_

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <fftw3.h>

//fft and ifft for float complex number
fftwf_complex *fftwf_data(fftwf_complex *input, int ndata, int nfft);
fftwf_complex *ifftwf_data(fftwf_complex *fdata, int ndata, int nfft);

//fft and ifft for double complex number
fftw_complex *fftwd_data(fftw_complex *input, int ndata, int nfft);
fftw_complex *ifftwd_data(fftw_complex *fdata, int ndata, int nfft);

void fftw1f(float *input, int ndata, int nf, int nfft, float *dreal, float *dimag);
void ifftw1f(float *input, int ndata, int nfft, float *dreal, float *dimag);

void fftw2f(float **input, int nrow, int ncol, int nfft,
		float **dreal, float **dimag, bool type);
void ifftw2f(float **input, int nrow, int ncol, int nfft,
		float **dreal, float **dimag, bool type);

#endif /* FFTWLIB_H_ */
