/*
 * wave_3dlib.c
 *
 *  Created on: Dec 28, 2011
 *      Author: toto
 */
#include "wave_3dlib.h"

//void cadzow3d_header_create(char *segyInput, char *cadzow3dHeaderInput,
//		int endian, int idxinline, int idxcdp)
//{
//	FILE *fid=NULL;
//	char cebcid[3200];
//	unsigned char *buffer;
//	bhed bh;
//	//cdp3Header cdphead, *totCdpHead;
//	int nsegy, ntrc;
//	int i;
//
//	int tmpInline, currInline, ninline;
//	int tmpCdp, currCdp, ncdp, firstcdp;
//	int trcdp, maxtrcdp;
//	bool bget1cdp;
//	int result;
//	//int **cdpinfo=NULL;
//
//	fid = fopen(segyInput, "r");
//	if(fid==NULL){
//		err("cadzow3d_header_create : error opening file %s \n", segyInput);
//		exit(0);
//	}
//	//read file 1 header information
//	readEbcdicHeader(fid, cebcid); /* read ebcdic header */
//	readBinaryHeader(fid, endian, &bh, &nsegy); /*read binary header */
//	ntrc = getNumberOfTraceSegyFile(fid, nsegy);
//	buffer = (unsigned char*) calloc(nsegy, sizeof(unsigned char));
//	//totCdpHead = (cdp3Header*) calloc(ntrc, sizeof(cdp3Header));
//
//	//cdpinfo = alloc2int(3, 3000); //allocate cdp info
//
//	//read the first of trace
//	result = fread(buffer, sizeof(unsigned char), nsegy, fid);
//	currInline = uchar2int(buffer, idxinline-1, 4, endian);	//read inline
//	currCdp = uchar2int(buffer, idxcdp-1, 4, endian); 		//read cdp
//	firstcdp = currCdp;
//	ninline = 1;
//	ncdp = 1;
//	trcdp = 1;
//	maxtrcdp = trcdp;
//	bget1cdp = false;
//	for(i=1; i<ntrc; i++)
//	{
//		result = fread(buffer, sizeof(unsigned char), nsegy, fid);
//		tmpInline = uchar2int(buffer, idxinline-1, 4, endian);	//read inline
//		tmpCdp = uchar2int(buffer, idxcdp-1, 4, endian); 		//read cdp
//
//		if(tmpInline!=currInline)
//		{
//			printf("Current Inline = %i \n", currInline);
//			currInline = tmpInline;
//			ninline++;
//		}
//
//		if(tmpCdp!=currCdp)
//		{
//			printf("Current CDP = %i [%i] \n", currCdp, trcdp);
//
//			if(bget1cdp==false)	{ //create tag if get the first cdp
//				bget1cdp = true;
//				maxtrcdp = trcdp;
//			}
//			else
//			{
//				if(maxtrcdp != trcdp)
//				{
//					printf("The number of trace in cdp is not equal \n");
//					printf("Maximum Number of Trace in First CDP(%i) = %i \n",
//							firstcdp, maxtrcdp);
//					printf("Maximum Number of Trace in Current CDP(%i) = %i \n",
//							currCdp, maxtrcdp);
//				}
//			}
//
//			trcdp = 1;
//			currCdp = tmpCdp;
//			ncdp++;
//		}
//		trcdp++;
//	}
//	printf("Current CDP = %i [%i] \n", currCdp, trcdp);
//
//	fclose(fid);
//	free(buffer);
//}

void setValuePositionInXline(pos3ddata **data,
		int min_inline, int min_xline,
		int curr_inline, int curr_xline,
		int scalco,
		float sx, float sy,
		int itrace)
{
	int ipos, xpos;

	ipos = curr_inline - min_inline;
	xpos = curr_xline - min_xline;

	data[ipos][xpos].xpos = ipos;
	data[ipos][xpos].ypos = xpos;
	data[ipos][xpos].iline = curr_inline;
	data[ipos][xpos].xline = curr_xline;
	if(scalco>0)
	{
		data[ipos][xpos].sx = sx*scalco;
		data[ipos][xpos].sy = sy*scalco;
	}
	else
	{
		data[ipos][xpos].sx = -sx/scalco;
		data[ipos][xpos].sy = -sy/scalco;
	}
	data[ipos][xpos].trace = itrace;

//	printf("in=%i x=%i tr=%i sx=%f sy=%f scalco=%i \n", curr_inline, curr_xline, itrace,
//			data[ipos][xpos].sx, data[ipos][xpos].sy, scalco);
}

pos3ddata **pos3ddata_create(int row, int col)
{
	int i;
	pos3ddata **data=NULL;

	/* allocate pointers to rows */
	data = (pos3ddata **) malloc((row*sizeof(pos3ddata*)));
	if (!data) {
		fprintf(stderr, "pos3ddata_create : error allocation row");
		exit(0);
	}

	/* allocate rows and set pointers to them */
	data[0]=(pos3ddata*) malloc((row*col)*sizeof(pos3ddata));
	if (!data[0]) {
		fprintf(stderr, "pos3ddata_create : error allocation column");
		exit(0);
	}

	for(i=1; i<row; i++)
		data[i]=data[i-1] + col;

	/* return pointer to array of pointers to rows */
	return data;
}

void pos3ddata_free(pos3ddata **data)
{
	free (data[0]);
	free (data);
}

void pos3ddata_print(pos3ddata **data, int inpos1, int inpos2, int xpos1, int xpos2)
{
	char *buffer;
	int nbyte, i, j;

	nbyte = 200;
	buffer = (char*) calloc(nbyte, sizeof(char));

	for(j=xpos1; j<=xpos2; j++)
	{
		for(i=inpos1; i<=inpos2; i++)
		{
			fprintf(stderr,"[%i,%i,%i,%i,%i]   ", data[j][i].ypos, data[j][i].xpos, data[j][i].xline, data[j][i].iline, data[j][i].trace);
		}
		fprintf(stderr,"\n");
	}
}

void pos3ddata_setindex(pos3ddata **data, int row, int col)
{
	int i,j;

	for(j=0; j<row; j++)
	{
		for(i=0; i<col; i++)
		{
			data[j][i].xpos = i;
			data[j][i].ypos = j;
			data[j][i].iline = 0;
			data[j][i].xline = 0;
			data[j][i].trace = 0;
		}
	}
}

void pos3ddata_write(pos3ddata **data, int row, int col, char *cfout,
		int min_iline, int max_iline, int min_xline, int max_xline)
{
	FILE *Fout=NULL;
	int i;

	Fout = fopen(cfout, "w");
	if(Fout==NULL) {
		fprintf(stderr, "error opening file %s", cfout);
		exit(0);
	}

	//save ny(inline), nx(xline)
	fwrite(&row, sizeof(int), 1, Fout);
	fwrite(&col, sizeof(int), 1, Fout);

	//save minimum inline, maximum inline
	fwrite(&min_iline, sizeof(int), 1, Fout);
	fwrite(&max_iline, sizeof(int), 1, Fout);

	//save minimum xline, maximum xline
	fwrite(&min_xline, sizeof(int), 1, Fout);
	fwrite(&max_xline, sizeof(int), 1, Fout);

	for(i=0; i<row; i++)
	{
		fwrite(data[i], sizeof(pos3ddata), col, Fout);
	}

	fclose(Fout);
}

pos3ddata **pos3ddata_read(char *cfinp, int *irow, int *icol,
		int *imin_iline, int *imax_iline, int *imin_xline, int *imax_xline)
{
	FILE *Fout=NULL;
	int i;
	int row, col;
	int min_iline, max_iline;
	int min_xline, max_xline;
	pos3ddata **posInXline=NULL;
	size_t result;

	Fout = fopen(cfinp, "r");
	if(Fout==NULL) {
		fprintf(stderr, "error opening file %s", cfinp);
		exit(0);
	}

	//read ny(inline), nx(xline)
	result = fread(&row, sizeof(int), 1, Fout);
	result = fread(&col, sizeof(int), 1, Fout);

	//read minimum inline, maximum inline
	result = fread(&min_iline, sizeof(int), 1, Fout);
	result = fread(&max_iline, sizeof(int), 1, Fout);

	//read minimum xline, maximum xline
	result = fread(&min_xline, sizeof(int), 1, Fout);
	result = fread(&max_xline, sizeof(int), 1, Fout);

	posInXline = pos3ddata_create(row, col);
	if(!posInXline)
	{
		fprintf(stderr, "error allocate 3d data position");
		exit(0);
	}

	//read data xline and inline position
	for(i=0; i<row; i++)
	{
		result = fread(posInXline[i], sizeof(pos3ddata), col, Fout);
	}

	fclose(Fout);

	(*irow) = row;
	(*icol) = col;
	(*imin_iline) = min_iline;
	(*imax_iline) = max_iline;
	(*imin_xline) = min_xline;
	(*imax_xline) = max_xline;
	return(posInXline);
}

/* windowing data from position inline pos1i to pos2i
 *                          and xline  pos1x to pos2x
 */
void pos3ddata_windowing(pos3ddata **data, int pos1i, int pos2i, int pos1x, int pos2x, int space,
		pos3ddata **windowpos)
{
	int i, j;
	int nwindow = space*space+1;

	memset(windowpos[0], '\0', nwindow*sizeof(pos3ddata));
//	fprintf(stderr, "---- %i %i %i %i ----\n",pos1x, pos2x, pos1i, pos2i);
	for(i=0; i<=pos2i-pos1i; i++)
	{
		for(j=0; j<=pos2x-pos1x; j++)
		{
			windowpos[j][i].xpos = data[pos1x+j][pos1i+i].xpos;
			windowpos[j][i].ypos = data[pos1x+j][pos1i+i].ypos;
			windowpos[j][i].iline = data[pos1x+j][pos1i+i].iline;
			windowpos[j][i].xline = data[pos1x+j][pos1i+i].xline;
			windowpos[j][i].sx = data[pos1x+j][pos1i+i].sx;
			windowpos[j][i].sy = data[pos1x+j][pos1i+i].sy;
			windowpos[j][i].trace = data[pos1x+j][pos1i+i].trace;
		}
	}
}


pos3ddata **pos3ddata_getradiusWithSpace(pos3ddata **data, int idxxline, int idxinline,
		int space, int lenInline, int lenXline,
		int nignore_neighbor,
		int *nneighbor, int *centerPos)
{
	int i, j, idx;
	int pos1i, pos2i, pos1x, pos2x;
	pos3ddata **windowpos=NULL;

	pos1i = getPos1(idxinline, space);		//minimum inline location
	pos2i = getPos2(idxinline, space, lenInline);	//maximum inline location
	pos1x = getPos1(idxxline, space);		//minimum xline location
	pos2x = getPos2(idxxline, space, lenXline);	//maximum xline location

	idx = 0;
	for(i=0; i<=pos2i-pos1i; i++)
	{
		for(j=0; j<=pos2x-pos1x; j++)
		{
			if(data[pos1x+j][pos1i+i].iline>0 && data[pos1x+j][pos1i+i].xline>0)
			{
				idx++;
			}
		}
	}

	if(idx<nignore_neighbor) //tidak usah diambil jika jumlah neighbor tidak mencukupi
	{
		(*nneighbor) = 0;
		return(windowpos);
	}
	else
	{
		windowpos = pos3ddata_create(1, idx);
		idx = 0;
		for(i=0; i<=pos2i-pos1i; i++)
		{
			for(j=0; j<=pos2x-pos1x; j++)
			{
				if(data[pos1x+j][pos1i+i].iline>0 && data[pos1x+j][pos1i+i].xline>0)
				{
					if(pos1x+j==idxxline && pos1i+i==idxinline)
						(*centerPos) = idx;

					windowpos[0][idx].xpos = data[pos1x+j][pos1i+i].xpos;
					windowpos[0][idx].ypos = data[pos1x+j][pos1i+i].ypos;
					windowpos[0][idx].iline = data[pos1x+j][pos1i+i].iline;
					windowpos[0][idx].xline = data[pos1x+j][pos1i+i].xline;
					windowpos[0][idx].sx = data[pos1x+j][pos1i+i].sx;
					windowpos[0][idx].sy = data[pos1x+j][pos1i+i].sy;
					windowpos[0][idx].trace = data[pos1x+j][pos1i+i].trace;
					idx++;
				}
			}
		}

		(*nneighbor) = idx;
		return(windowpos);
	}
}

pos3ddata **pos3ddata_getradiusWithRadius(pos3ddata **data, int idxinline, int idxxline,
		int radius, float distance,
		int lenInline, int lenXline,
		int nignore_neighbor,
		int *nneighbor, int *centerPos, int verbose)
{
	int i, j, idx;
	int pos1i, pos2i, pos1x, pos2x;
	pos3ddata **windowpos=NULL;
	float refsx, refsy;
	float cur_sx, cur_sy;
	double cur_distance;
	double tmp1, tmp2;

	refsx = data[idxinline][idxxline].sx;
	refsy = data[idxinline][idxxline].sy;

	pos1i = getPos1(idxinline, radius);		//minimum inline location
	pos2i = getPos2(idxinline, radius, lenInline);	//maximum inline location
	pos1x = getPos1(idxxline, radius);		//minimum xline location
	pos2x = getPos2(idxxline, radius, lenXline);	//maximum xline location
	idx = 0;

	if(verbose)
		printf("%i  : ", data[idxinline][idxxline].trace);
	for(i=0; i<=pos2i-pos1i; i++)
	{
		for(j=0; j<=pos2x-pos1x; j++)
		{
			if(data[pos1i+i][pos1x+j].iline>0 && data[pos1i+i][pos1x+j].xline>0)
			{
				cur_sx = data[pos1i+i][pos1x+j].sx;
				cur_sy = data[pos1i+i][pos1x+j].sy;
				tmp1 = (double)(cur_sx-refsx);
				tmp2 = (double)(cur_sy-refsy);
				cur_distance = sqrt(pow(tmp1,2.0) + pow(tmp2,2.0));

				if(cur_distance<=distance)
				{
					if(verbose)
						printf(" %i, ", data[pos1i+i][pos1x+j].trace);
					idx++;
				}
			}
		}
	}
	if(verbose)
		printf("loop nwindowing = %i traces\n", idx);

//	fprintf(stderr,"%i %i \n", idx, nignore_neighbor);
	if(idx<nignore_neighbor) //tidak usah diambil jika jumlah neighbor tidak mencukupi
	{
		(*nneighbor) = 0;
		return(windowpos);
	}
	else
	{
		windowpos = pos3ddata_create(1, idx);
		idx = 0;
		for(i=0; i<=pos2i-pos1i; i++)
		{
			for(j=0; j<=pos2x-pos1x; j++)
			{
				if(data[pos1i+i][pos1x+j].iline>0 && data[pos1i+i][pos1x+j].xline>0)
				{
					if(pos1i+i==idxinline && pos1x+j==idxxline)
						(*centerPos) = idx;

					cur_sx = data[pos1i+i][pos1x+j].sx;
					cur_sy = data[pos1i+i][pos1x+j].sy;
					tmp1 = (double)(cur_sx-refsx);
					tmp2 = (double)(cur_sy-refsy);
					cur_distance = sqrt(pow(tmp1,2.0) + pow(tmp2,2.0));
					//cur_distance = sqrt(pow(cur_sx-refsx,2.0) + pow(cur_sy-refsy, 2.0));
					if(cur_distance<=distance)
					{
						windowpos[0][idx].xpos = data[pos1i+i][pos1x+j].xpos;
						windowpos[0][idx].ypos = data[pos1i+i][pos1x+j].ypos;
						windowpos[0][idx].iline = data[pos1i+i][pos1x+j].iline;
						windowpos[0][idx].xline = data[pos1i+i][pos1x+j].xline;
						windowpos[0][idx].sx = data[pos1i+i][pos1x+j].sx;
						windowpos[0][idx].sy = data[pos1i+i][pos1x+j].sy;
						windowpos[0][idx].trace = data[pos1i+i][pos1x+j].trace;
						idx++;
					}
				}
			}
		}

		(*nneighbor) = idx;
		return(windowpos);
	}

}

int getPos1(int idx, int space)
{
	if(idx-space<0)
		return(0);
	else
		return(idx-space);
}

int getPos2(int idx, int space, int idxmax)
{
	if(idx+space>=idxmax)
		return(idxmax-1);
	else
		return(idx+space);
}

float *gaussianKernel(float sigma, int rkernel)
{
	float *kernelgauss=NULL;
	int i, span;
	double con, gd;

	kernelgauss = (float*) calloc(rkernel, sizeof(float));
	span = (rkernel-1)/2;

	con = sqrt(2.0 * 3.141592653589793) * sigma;
	for(i=0; i<span; i++)
	{
		gd = exp((-1.0*i*i)/(2.0*sigma*sigma));
		kernelgauss[span+i+1] = (float) (gd/con);
		kernelgauss[span-i-1] = (float) (gd/con);
	}
	i=0;
	gd = exp((-1.0*i*i)/(2.0*sigma*sigma));
	kernelgauss[span] = (float) (gd/con);

	return(kernelgauss);
}


float normgaussian1d(float r,float sigma)
{
	float con = sqrt(2*3.14159)*sigma;
	return exp((-1.0*r*r)/(2.0*sigma*sigma))/con;
}

float gaussian1d(float amp, float r,float sigma)
{
	return amp * exp((-1.0*r*r)/(2.0*sigma*sigma));
}
