/*
 * smoothingLib.c
 *
 *  Created on: Jan 18, 2012
 *      Author: toto
 */

#include "smoothingLib.h"

/**
 *
 * @author Dythia Prayudhatama
 */
/*
 * Operator1D.c
 *
 *  Created on: Jul 12, 2010
 *      Author: toto (modified)
 */


void GaussianSmoothY(float **input, float **output,
		int rows, int cols, float sigma, float intscale)
{
	int K;
	int test;
	int xlen;			//len of array x
	int span;
	int extrow;
	int extcol;
	int i, j, cc;				//index
	int indexConv;
	int UpperSpan;
	int LowerSpan;
	//float norm;		//sum of array kernel
	float ConvReslt;

	float *x = NULL;
	float *kernel=NULL;
	float **process=NULL;
	float *D=NULL;

	// Prepare 1D array for Gaussian Kernel
	K = ceil(intscale*sigma);
	test=K;

	xlen = (K*2)+1;
	x = ealloc1float(xlen);
	K=K*(-1);
	for(i=0; i<xlen;i++)
	{
		x[i]=K;
		K=K+1;
	}

	// Construct 1D Gaussian Kernel
	kernel = ealloc1float(xlen);
	for(i=0; i<xlen; i++)
		kernel[i]=exp((-1) *(pow(x[i], 2.0))/(2*(pow(sigma, 2.0))));

	/*
	norm = sum1_float(kernel, xlen);
	for(i=0; i<xlen; i++)
	{
		kernel[i]=kernel[i]/norm;
	}
	 */

	if(test!=K)	K=test;

	span = (xlen-1) / 2;

	extrow=rows+(span*2);
	extcol=cols+(span*2);

	process = ealloc2float(extcol, extrow);
	memset(process[0], 0, extcol*extrow*sizeof(float));

	for(i=span; i<extrow-span; i++)
		for(j=span; j<extcol-span; j++)
			process[i][j]=input[i-span][j-span];

	D = ealloc1float(xlen);
	for(i=span; i<extcol-span;i++)	 // Collum Shifter
	{
		for(j=span; j<extrow-span;j++)	 // Row Shifter
		{
			indexConv=0;
			ConvReslt=0;
			UpperSpan=j-span;
			LowerSpan=j+span;

			for(cc=UpperSpan; cc<LowerSpan; cc++)
			{
				D[indexConv]=process[cc][i]*kernel[indexConv];
				ConvReslt=ConvReslt+D[indexConv];
				indexConv++;
			}

			output[j-span][i-span]=ConvReslt;
			memset(D, 0 , xlen*sizeof(float));
		}
	}

	free1float(D);
	free1float(kernel);
	free1float(x);
	free2float(process);
}

void GaussianSmoothX(float **input, float **output,
		int rows, int cols, float sigma, float intscale)
{
	int i, j, cc;
	int K;
	int test;
	int span;
	int extrow;
	int extcol;
	int indexConv2;
	int LeftSpan;
	int RightSpan;
	int xlen=0;
	//float norm;
	float ConvReslt2;
	float *D2=NULL;
	float *x=NULL;
	float *kernel=NULL;
	float **process=NULL;

	// Prepare 1D array for Gaussian Kernel
	K = ceil(intscale*sigma);

	test=K;
	xlen = (K*2)+1;
	x = ealloc1float(xlen);

	K=K*(-1);
	for(i=0; i<xlen; i++)
	{
		x[i]=K;
		K=K+1;
	}

	// Construct 1D Gaussian Kernel
	kernel = ealloc1float(xlen);
	for(i=0; i<xlen; i++)
		kernel[i]=exp((-1) *(pow(x[i], 2.0))/(2*(pow(sigma, 2.0))));

	/*
	norm = sum1_float(kernel, xlen);
	for(i=0; i<xlen; i++)
	{
		kernel[i]=kernel[i]/norm;
	}
	 */

	if(test!=K) K=test;

	span = (xlen-1) / 2;
	extrow=rows+(span*2);
	extcol=cols+(span*2);

	process = ealloc2float(extcol, extrow);
	memset(process[0], 0, extcol*extrow*sizeof(float));

	for(i=span; i<extrow-span; i++)
	{
		for(j=span; j<extcol-span; j++)
		{
			process[i][j]=input[i-span][j-span];
		}
	}

	// Convolve in X-Direction
	D2 = ealloc1float(xlen);
	for(i=span; i<extrow-span;i++)		 // Row Shifter
	{
		for(j=span; j<extcol-span;j++)	 // Col Shifter
		{
			indexConv2=0;
			ConvReslt2=0;
			LeftSpan=j-span;
			RightSpan=j+span;

			for(cc=LeftSpan; cc<RightSpan; cc++)
			{
				D2[indexConv2]=process[i][cc]*kernel[indexConv2];
				ConvReslt2=ConvReslt2+D2[indexConv2];
				indexConv2++;
			}
			output[i-span][j-span]=ConvReslt2;
			memset(D2, 0 , xlen*sizeof(float));
		}
	}

	free1float(D2);
	free1float(kernel);
	free1float(x);
	free2float(process);
}

void GaussianDerivativeY(float **input, float **output,
		int rows, int cols, float sigma, float intscale)
{
	int i, j, cc;
	int K;
	int test;
	int span;
	int extrow;
	int extcol;
	int indexConv;
	int UpperSpan;
	int LowerSpan;
	int xlen=0;
	//float norm;
	float ConvReslt;
	float *D=NULL;
	float *x=NULL;
	float *kernel=NULL;
	float **process=NULL;

	// Prepare 1D array for Gaussian Kernel
	K = ceil(intscale*sigma);
	test=K;
	xlen = (K*2)+1;
	x = ealloc1float(xlen);

	K=K*(-1);
	for(i=0; i<xlen;i++)
	{
		x[i]=K;
		K=K+1;
	}

	// Construct 1D Gaussian Kernel
	kernel = ealloc1float(xlen);
	for(i=0; i<xlen; i++)
		kernel[i]=exp((-1) *(pow(x[i], 2))/(2*(pow(sigma, 2))));

	/*
	norm = sum1_float(kernel, xlen);
	for(i=0; i<xlen; i++)
	{
		kernel[i]=kernel[i]/norm;
	}
	// Normalized Gaussian Convolution Kernel has Created
	 */

	// Calculate Gaussian Derivative Kernel
	for(i=0; i<xlen; i++)
		kernel[i]=kernel[i] * (((-1) * x[i]) / (sigma*sigma));

	if(test!=K)	K=test;

	span = (xlen-1) / 2;
	extrow=rows+(span*2);
	extcol=cols+(span*2);

	process = ealloc2float(extcol, extrow);
	memset(process[0], 0, extcol*extrow*sizeof(float));

	for(i=span; i<extrow-span; i++)
		for(j=span; j<extcol-span; j++)
			process[i][j]=input[i-span][j-span];

	D = ealloc1float(xlen);
	for(i=span; i<extcol-span;i++)	 // Collum Shifter
	{
		for(j=span; j<extrow-span;j++)	// Row Shifter
		{
			indexConv=0;
			ConvReslt=0;
			UpperSpan=j-span;
			LowerSpan=j+span;

			for(cc=UpperSpan; cc<LowerSpan; cc++)
			{
				D[indexConv]=process[cc][i]*kernel[indexConv];
				ConvReslt=ConvReslt+D[indexConv];
				indexConv++;
			}

			output[j-span][i-span]=ConvReslt;
			memset(D, 0, xlen*sizeof(float));
		}
	}

	free1float(D);
	free1float(kernel);
	free1float(x);
	free2float(process);
}

void GaussianDerivativeX(float **input, float **output,
		int rows, int cols, float sigma, float intscale)
{
	int i, j, cc;
	int K;
	int test;
	int span;
	int extrow;
	int extcol;
	int indexConv2;
	int LeftSpan;
	int RightSpan;
	int xlen=0;
	//float norm;
	float ConvReslt2;
	float *D2=NULL;
	float *x=NULL;
	float *kernel=NULL;
	float **process=NULL;

	// Prepare 1D array for Gaussian Kernel
	K = ceil(intscale*sigma);

	test=K;
	xlen = (K*2)+1;
	x = ealloc1float(xlen);

	K=K*(-1);
	for(i=0; i<xlen;i++)
	{
		x[i]=K;
		K=K+1;
	}

	// Construct 1D Gaussian Kernel
	kernel = ealloc1float(xlen);
	for(i=0; i<xlen; i++)
		kernel[i]=exp((-1) *(pow(x[i], 2))/(2*(pow(sigma, 2))));

	/*
	norm = sum1_float(kernel, xlen);
	for(i=0; i<xlen; i++)
	{
		kernel[i]=kernel[i]/norm;
	}
	// Normalized Gaussian Convolution Kernel has Created
	 */

	// Calculate Gaussian Derivative Kernel
	for(i=0; i<xlen; i++)
		kernel[i]=kernel[i] * (((-1) * x[i]) / (sigma*sigma));

	if(test!=K) K=test;

	span = (xlen-1) / 2;
	extrow=rows+(span*2);
	extcol=cols+(span*2);

	process = ealloc2float(extcol, extrow);
	memset(process[0], 0, extcol*extrow*sizeof(float));

	for(i=span; i<extrow-span; i++)
	{
		for(j=span; j<extcol-span; j++)
		{
			process[i][j]=input[i-span][j-span];
		}
	}

	// Convolve in X-Direction
	D2 = ealloc1float(xlen);
	for(i=span; i<extrow-span;i++)	 // Row Shifter
	{
		for (j=span; j<extcol-span;j++)	 // Col Shifter
		{
			indexConv2=0;
			ConvReslt2=0;
			LeftSpan=j-span;
			RightSpan=j+span;

			for(cc=LeftSpan; cc<RightSpan; cc++)
			{
				D2[indexConv2]=process[i][cc]*kernel[indexConv2];
				ConvReslt2=ConvReslt2+D2[indexConv2];
				indexConv2++;
			}
			output[i-span][j-span]=ConvReslt2;
			memset(D2, 0, xlen*sizeof(float));
		}
	}

	free1float(D2);
	free1float(kernel);
	free1float(x);
	free2float(process);
}

void ConstructDiffusionTensor(float **S11, float **S22, float **S12,
		float **D11, float **D22, float **D12,
		float k, int row, int col)
{
	float c1,c2=0.001,alpha,eigen1,eigen2;
	float temp;
	int i, j;

	for(i=0; i<row; i++)
	{
		for(j=0; j<col; j++)
		{
			alpha = sqrt( pow((S11[i][j]-S22[i][j]),2) + 4* pow(S12[i][j], 2) );
			eigen1 = (0.5) * (S11[i][j]+S22[i][j]-alpha);
			eigen2 = (0.5) * (S11[i][j]+S22[i][j]+alpha);
			temp = (-1) * pow((eigen1-eigen2),2);
			c1 = MAX(c2, 1 - exp(temp/(k*k)));
			D11[i][j]=(0.5) * (c1+c2 + ((c2-c1) * (S11[i][j]-S22[i][j]) / alpha) );
			D12[i][j]= ((c2-c1) * S12[i][j]) / alpha;
			D22[i][j]=(0.5) * (c1+c2 - ((c2-c1) * (S11[i][j]-S22[i][j]) / alpha) );
		}
	}
}

void gaussiankernelv(float *kernel, int ksize, float sigma)
{
	int x, index, span;
	float con;

	span = (ksize-1) /2;
	con = sqrt(2*3.14159)*sigma;
	index = 0;
	for(x=span*-1; x<=span; x++)
	{
		kernel[index]  = exp((-1.0*(float)x*(float)x)/(2.0*sigma*sigma))/con;
		index++;
	}
}

void gaussianderivative1kernelv(float *kernel, int ksize, float sigma)
{
	int x, index, span;
	float con;

	span = (ksize-1) /2;
	con = sqrt(2*3.14159)*sigma;
	index = 0;
	for(x=span*-1; x<=span; x++)
	{
		kernel[index]  = exp((-1.0*(float)x*(float)x)/(2.0*sigma*sigma))/con;
		kernel[index] *= ( (-1.0)* (float)x / (sigma*sigma));
		index++;
	}
}

void convolve(float* function, int flength, float* kernel, int klength)
{
	int span, plength;
	float *panel, *D, ConvReslt;
	int i, upperspan, lowerspan, indexConv, cc;

	span=(klength-1)/2;
	plength = (flength+(2*span));

	panel = alloc1float(plength);
	memcpy(panel+span, function, sizeof(float)*flength);

	for(i=0; i<span; i++)
		panel[i] = function[0];
	for(i=plength-span; i<plength; i++)
		panel[i] = function[flength-1];

	D = alloc1float(klength);
	for(i=span; i<(plength-span);i++)
	{
		memset(D,'\0',klength);
		indexConv = 0;
		ConvReslt = 0;
		upperspan = i-span;
		lowerspan = i+span;
		for(cc=upperspan; cc<=lowerspan; cc++)
		{
			D[indexConv] = (panel[cc] * kernel[indexConv]);
			ConvReslt = ConvReslt+D[indexConv];
			indexConv++;
		}
		function[i-span]=ConvReslt;
	}
	free(D);
	free(panel);
}

void convolven1(float** input, int n1, int n2, float* kernel, int klength)
{
	int row;

	for(row=0; row<n1; row++)
		convolve(input[row], n2, kernel, klength);
}

void convolven2(float** input, int n1, int n2, float* kernel, int klength)
{
	float *panel;
	int col;

	panel = alloc1float(n1);
	for(col=0; col<n2; col++)
	{
		getcol(input, col, panel, n1);
		convolve(panel, n1, kernel, klength);
		setcol(input, col, panel, n1);
	}

	free1float(panel);
}

void getcol(float** input, int colselect, float* coldata, int nrow)
{
	int i;

	for(i=0; i<nrow; i++)
		coldata[i] = input[i][colselect];
}

void setcol(float** input, int colselect, float* coldata, int nrow)
{
	int i;

	for(i=0; i<nrow; i++)
		input[i][colselect] = coldata[i];
}

void timesarray(float** data1,float** data2, float** result, int row, int col)
{
	int i,j;

	for(i=0; i<row; i++)
		for(j=0; j<col; j++)
			result[i][j] = data1[i][j]*data2[i][j];
}

void constructcedtensor(float** S11, float** S22, float** S12,
		float** D11, float** D22, float** D12,
		float k, int row, int col)
{
	int i,j;
	float c1,c2=0.001f,alpha,eigen1,eigen2;
	float temp;

	for( i=0; i<row; i++)
	{
		for(j=0; j<col; j++)
		{
			alpha = (float) sqrt( pow((S11[i][j]-S22[i][j]), 2.0) + 4* pow(S12[i][j], 2.0) );
			if(alpha==0)	alpha=1;	/*remove bug nan value */
			eigen1 = (float) ((0.5) * (S11[i][j] + S22[i][j] - alpha));
			eigen2 = (float) ((0.5) * (S11[i][j] + S22[i][j] + alpha));
			temp = (float) ((-1) * pow(eigen1 - eigen2, 2.0));
			c1 = (float) MAX(c2, 1 - exp(temp/(k*k)));
			D11[i][j]=(float) ((0.5) * (c1 + c2 + ((c2 - c1) * (S11[i][j] - S22[i][j]) / alpha)));
			D12[i][j]= ((c2-c1) * S12[i][j]) / alpha;
			D22[i][j]=(float) ((0.5) * (c1 + c2 - ((c2 - c1) * (S11[i][j] - S22[i][j]) / alpha)));
		}
	}
}

void WeickertCED(float** data, float** a, float** c, float** b,
		float dt, int row, int col)
{

	int i,j;
	float** panel;
	float K00,K01,K02;
	float K10,K11,K12;
	float K20,K21,K22;

	panel = ealloc2float(col, row);
	for(i=0;i<row;i++)
		for(j=0;j<col;j++)
			panel[i][j]=data[i][j];

	for(i=1; i<row-1; i++)
	{
		for(j=1; j<col-1; j++)
		{
			K00 = (-1) * ( (b[i-1][j] + b[i][j+1]) / 4) * panel[i-1][j+1] ;
			K01 = ((c[i][j+1]+c[i][j]) / 2) * panel[i][j+1];
			K02 = ((b[i+1][j]+b[i][j+1]) / 4) * panel[i+1][j+1];
			K10 = ((a[i-1][j] + a[i][j]) /2 ) * panel[i-1][j];
			K11 = (a[i-1][j]) + (2 * a[i][j]) + (a[i+1][j]) + (c[i-1][j]) + (2* c[i][j]) + (c[i+1][j]);
			K11 = (K11 /2.0f)*(-1)*panel[i][j];
			K12 = ((a[i+1][j] + a[i][j]) / 2) * panel[i+1][j];
			K20 = ((b[i-1][j] + b[i][j-1]) /4) * panel[i-1][j-1];
			K21 = ((c[i][j-1] + c[i][j]) / 2) * panel[i][j-1];
			K22 = (-1) * ((b[i+1][j] + b[i][j-1]) /4) * panel[i+1][j-1];
			data[i][j] = panel[i][j] + (dt * (K00 + K01 + K02 +
					K10 + K11 + K12 +
					K20 + K21 + K22));
		}
	}

	free2float(panel);
}

/*
 * nx --> number of column
 * ny --> number of row
 * */
void wvstrucsmooth(float **data, int ny, int nx,
		float sigma, float sigmaderiv, float k, float dts,
		int klength, int nstep)
{

	float *kernelg, *kernelgderiv;
	float **datax, **datay, **dataxy;
	float **D11, **D12, **D22;
	int i,j;

	/* initialization variable */
	kernelg = alloc1float(klength);
	kernelgderiv = alloc1float(klength);
	datay = alloc2float(nx, ny);
	datax = alloc2float(nx, ny);
	dataxy = alloc2float(nx, ny);
	D11 = alloc2float(nx, ny);
	D22 = alloc2float(nx, ny);
	D12 = alloc2float(nx, ny);

	/* copy input data */
	memcpy(datax[0], data[0], ny*nx*sizeof(float));
	memcpy(datay[0], data[0], ny*nx*sizeof(float));

	gaussiankernelv(kernelg, klength, sigma);
	gaussianderivative1kernelv(kernelgderiv, klength, sigmaderiv);

	convolven2(datay, ny, nx, kernelgderiv, klength);
	convolven1(datax, ny, nx, kernelgderiv, klength);
	timesarray(datay, datax, dataxy, ny, nx);

	for(i=0; i<ny; i++)
	{
		for(j=0; j<nx; j++)
		{
			datax[i][j] *= datax[i][j];
			datay[i][j] *= datay[i][j];
			dataxy[i][j] = ABS(dataxy[i][j]);
		}
	}

	convolven2(datay, ny, nx, kernelg, klength);
	convolven1(datay, ny, nx, kernelg, klength);

	convolven2(datax, ny, nx, kernelg, klength);
	convolven1(datax, ny, nx, kernelg, klength);

	convolven2(dataxy, ny, nx, kernelg, klength);
	convolven1(dataxy, ny, nx, kernelg, klength);

	constructcedtensor(datay, datax, dataxy, D11, D22, D12, k, ny, nx);
	for(i=0; i<nstep; i++)
	{
		//fprintf(stderr, "Running steps %i / %i \n", i+1, nstep);
		WeickertCED(data, D11, D22, D12, dts, ny, nx);
	}

	free1float(kernelg);
	free1float(kernelgderiv);
	free2float(datax);
	free2float(datay);
	free2float(dataxy);
	free2float(D11);
	free2float(D22);
	free2float(D12);
}

void k1k2filter(float **data, int nx1, int nx2, int nx1fft, int nx2fft,
		float nord, float alpha, int nK1, int nK2, int nxm, int nzm,
		float cf1, float cf2, float onfft)
{
	complex **ct, **ct_temp;	/* complex FFT workspace		*/
	float **absf2, **rt;		/* float FFT workspace			*/
	int ix1, ix2, ik1, ik2;

	/* Allocate space */
	rt = alloc2float(nx1fft, nx2fft);
	ct = alloc2complex(nK1,nx2fft);
	ct_temp = alloc2complex(nx1fft,nx2fft);
	absf2 = alloc2float(nx1fft, nx2fft);

	/* Zero all arrays */
	memset((void *) rt[0], 0, nx1fft*nx2fft*FSIZE);
	memset((void *) absf2[0], 0, nx1fft*nx2fft*FSIZE);
	memset((void *) ct[0], 0, nK1*nx2fft*sizeof(complex));
	memset((void *) ct_temp[0], 0, nx1fft*nx2fft*sizeof(complex));

	/* Load traces into fft arrays and close tmpfile */
	for (ix2=0; ix2<nx2; ++ix2)
	{
		if (ISODD(ix2))
		{
			for (ix1=0; ix1<nx1; ++ix1)
				rt[ix2][ix1] = -data[ix2][ix1];
		}
		else
			memcpy(rt[ix2], data[ix2], nx1*sizeof(float));
	}

	pfa2rc(-1,1,nx1fft,nx2,rt[0],ct[0]);	/* Fourier transform dimension 1 */
	pfa2cc(-1,2,nK1,nx2fft,ct[0]);			/* Fourier transform dimension 2 */


	for (ik2=0; ik2<nx2fft; ++ik2)
	{
		for (ik1=0; ik1<nK1; ++ik1)			/* do upper half of K-plane */
			ct_temp[ik2][ik1] = ct[nx2fft-1-ik2][nK1-1-ik1];

		for (ik1=nK1; ik1<nx1fft; ++ik1)	/* build lower half plane from upper plane assuming symmetry */
		{
			if (ik2<nx2fft-1)
				ct_temp[ik2][ik1] = ct[ik2+1][ik1-nK1];
			else
				ct_temp[ik2][ik1] = ct[0][ik1-nK1];
		}
	}


	circular_filter(nx2fft, nx1fft, nK2, nK1, cf2, cf1, nord, alpha, absf2);

	for (ik2=0; ik2<nx2fft; ++ik2)			/* filter */
	{
		for (ik1=0; ik1<nx1fft; ++ik1)		/* do upper half of K-plane */
			ct_temp[ik2][ik1] = crmul(ct_temp[ik2][ik1], absf2[ik2][ik1] ) ;
	}

	for (ik2=0; ik2<nx2fft; ++ik2)			/* return ct_temp to ct */
	{
		for (ik1=0; ik1<nK1; ++ik1)			/* do upper half of K-plane */
			ct[ik2][ik1] = ct_temp[nx2fft-1-ik2][nK1-1-ik1];
	}

	pfa2cc(1,2,nK1,nx2fft,ct[0]);			/* Inverse Fourier transform dimension 2 */
	pfa2cr(1,1,nx1fft,nx2,ct[0],rt[0]);		/* Inverse Fourier transform dimension 1 */

	for (ix2=0; ix2 < nx2; ++ix2)
	{
		if (ISODD(ix2))
		{
			for (ix1=0; ix1<nx1; ++ix1)
				data[ix2][ix1] = -rt[ix2][ix1]*onfft;
		}
		else
		{
			for (ix1=0; ix1<nx1; ++ix1)
				data[ix2][ix1] = rt[ix2][ix1]*onfft;
		}

		if (nxm>0)
		{
			if ( ix2<nxm-1 || ix2>nx2-nxm )
				for (ix1=0; ix1<nx1; ++ix1)
					data[ix2][ix1] = 0.0;
		}

		if (nzm>0)
		{
			for (ix1=0; ix1<nzm; ++ix1)
			{
				data[ix2][ix1]       = 0.0;
				data[ix2][nx1-ix1-1] = 0.0;
			}
		}
	}

	free2float(absf2);
	free2float(rt);
	free2complex(ct_temp);
	free2complex(ct);
}

void circular_filter(int nx, int nz, int xc, int zc, float a1, float b1,
		float nord, float alpha, float **filt)
{

	int ix,iz;
	float	x,z,xa,za,x2,z2;
	float	tmp,tmp1;

	float max_tmp,max_tmp1;
	float min_tmp,min_tmp1;
	float max_x2, max_z2;
	float min_x2, min_z2;

	float min_x, min_z;
	float max_x, max_z;
	float min_xa, min_za;
	float max_xa, max_za;



	for (ix=0; ix<nx; ix++) {
		for (iz=0; iz<nz; iz++ ) {

			x = (float)(ix - xc)/(float)nx;
			z = (float)(iz - zc)/(float)nz;

			xa = x*cos(alpha)  - z*sin(-alpha);
			za = x*sin(-alpha) + z*cos(alpha);

			x2 = (xa*xa)/(a1*a1);
			z2 = (za*za)/(b1*b1);

			tmp = x2+z2;
			tmp1 = 1 + pow(tmp,nord);
			filt[ix][iz] = 1.0/tmp1;


			if (ix==0 && iz==00) {
				max_tmp = tmp;
				max_tmp1 = tmp1;
				max_x2 = x2;
				max_z2 = z2;
				max_xa = xa;
				max_za = za;
				max_x  = x;
				max_z  = z;

				min_tmp = tmp;
				min_tmp1 = tmp1;
				min_x2 = x2;
				min_z2 = z2;
				min_xa = xa;
				min_za = za;
				min_x  = x;
				min_z  = z;


			} else 	{
				if (max_tmp<tmp) max_tmp = tmp;
				if (max_tmp1<tmp1) max_tmp1 = tmp1;
				if (max_x2<x2) max_x2 = x2;
				if (max_z2<z2) max_z2 = z2;
				if (max_xa<xa) max_xa = xa;
				if (max_za<za) max_za = za;
				if (max_x<x) max_x = x;
				if (max_z<z) max_z = z;

				if (min_tmp>tmp) min_tmp = tmp;
				if (min_tmp1>tmp1) min_tmp1 = tmp1;
				if (min_x2>x2) min_x2 = x2;
				if (min_z2>z2) min_z2 = z2;
				if (min_xa>xa) min_xa = xa;
				if (min_za>za) min_za = za;
				if (min_x>x) min_x = x;
				if (min_z>z) min_z = z;
			}
		}
	}

	//	warn(" max_tmp1 = %f  min_tmp1 = %f",max_tmp1,min_tmp1);
	//	warn(" max_tmp  = %f  min_tmp  = %f",max_tmp,min_tmp);
	//	warn(" max_x2   = %f  min_x2   = %f",max_x2,min_x2);
	//	warn(" max_z2   = %f  min_z2   = %f",max_z2,min_z2);
	//	warn(" max_xa   = %f  min_xa   = %f",max_xa,min_xa);
	//	warn(" max_za   = %f  min_za   = %f",max_za,min_za);
	//	warn(" max_x    = %f  min_x   = %f",max_x,min_x);
	//	warn(" max_z    = %f  min_z   = %f",max_z,min_z);

}

float **computeAmplitudeSpectrum(complex **ct, int nrow, int ncol, int nx1fft)
{
	int ik1, ik2;
	int nK2, nK1;
	float **AmpSpectrum=NULL;
	nK2 = nrow;
	nK1 = ncol;

	AmpSpectrum = alloc2float(nx1fft, nrow);
	//	AmpSpectrum = alloc2float(ncol, nrow);

	/* Compute and output amplitude spectrum */
	for (ik2=0; ik2<nK2; ++ik2)
	{
		/* do upper half of K-plane */
		for (ik1=0; ik1<nK1; ++ik1)
			AmpSpectrum[ik2][ik1] = rcabs(ct[nK2-1-ik2][nK1-1-ik1]);

		/* build lower half plane from upper plane assuming symmetry */
		for (ik1=nK1; ik1<nx1fft; ++ik1)
		{
			if (ik2<nK2-1)
				AmpSpectrum[ik2][ik1] = rcabs(ct[ik2+1][ik1-nK1]);
			else
				AmpSpectrum[ik2][ik1] = rcabs(ct[0][ik1-nK1]);
		}
	}

	return(AmpSpectrum);
}

float **smooth2(float **v0, int row, int col, float r1, float r2, int *win, float rw)
{
	int n1, n2;
	int nmax;	/* max of n1 and n2 */
	int ix, iz;	/* counters */
	float **v=NULL;	/* array of output velocities */
	float **w=NULL;	/* intermediate array */
	float *d=NULL, *e=NULL;	/* input arrays for subroutine tripd */
	float *f=NULL;	/* intermediate array */

	n1 = col;
	n2 = row;

	/* scale the smoothing parameter */
	r1 = r1*r1*0.25;
	r2 = r2*r2*0.25;

	/* allocate space */
	nmax = (n1<n2)?n2:n1;
	v = alloc2float(n1,n2);
	w = alloc2float(n1,n2);
	d = alloc1float(nmax);
	e = alloc1float(nmax);
	f = alloc1float(nmax);

	/* save the original velocity */
	for(ix=0; ix<n2; ++ix)
		for(iz=0; iz<n1; ++iz)
			v[ix][iz]=v0[ix][iz];

	rw = rw*rw*0.25;

	/* define the window function */
	for(ix=0; ix<n2; ++ix)
		for(iz=0; iz<n1; ++iz)
			w[ix][iz] = 0;
	for(ix=win[2]; ix<win[3]; ++ix)
		for(iz=win[0]; iz<win[1]; ++iz)
			w[ix][iz] = 1;

	if(win[0]>0 || win[1]<n1 || win[2]>0 || win[3]<n2)
	{
		/*	smooth the window function */
		for(iz=0; iz<n1; ++iz)
		{
			for(ix=0; ix<n2; ++ix)
			{
				d[ix] = 1.0+2.0*rw;
				e[ix] = -rw;
				f[ix] = w[ix][iz];
			}
			d[0] -= rw;
			d[n2-1] -= rw;
			tripd(d,e,f,n2);
			for(ix=0; ix<n2; ++ix)
				w[ix][iz] = f[ix];
		}
		for(ix=0; ix<n2; ++ix)
		{
			for(iz=0; iz<n1; ++iz)
			{
				d[iz] = 1.0+2.0*rw;
				e[iz] = -rw;
				f[iz] = w[ix][iz];
			}
			d[0] -= rw;
			d[n1-1] -= rw;
			tripd(d,e,f,n1);
			for(iz=0; iz<n1; ++iz)
				w[ix][iz] = f[iz];
		}
	}

	/*      solving for the smoothing velocity */
	for(iz=0; iz<n1; ++iz)
	{
		for(ix=0; ix<n2-1; ++ix)
		{
			d[ix] = 1.0+r2*(w[ix][iz]+w[ix+1][iz]);
			e[ix] = -r2*w[ix+1][iz];
			f[ix] = v[ix][iz];
		}
		d[0] -= r2*w[0][iz];
		d[n2-1] = 1.0+r2*w[n2-1][iz];
		f[n2-1] = v[n2-1][iz];
		tripd(d,e,f,n2);
		for(ix=0; ix<n2; ++ix)
			v[ix][iz] = f[ix];
	}
	for(ix=0; ix<n2; ++ix)
	{
		for(iz=0; iz<n1-2; ++iz)
		{
			d[iz] = 1.0+r1*(w[ix][iz+1]+w[ix][iz+2]);
			e[iz] = -r1*w[ix][iz+2];
			f[iz] = v[ix][iz+1];
		}
		f[0] += r1*w[ix][1]*v[ix][0];
		d[n1-2] = 1.0+r1*w[ix][n1-1];
		f[n1-2] = v[ix][n1-1];
		tripd(d,e,f,n1-1);
		for(iz=0; iz<n1-1; ++iz)
			v[ix][iz+1] = f[iz];
	}

	free2float(w);
	free1float(d);
	free1float(e);
	free1float(f);

	return(v);
}

void smooth2SeisUn(float **v, int nrow, int ncol,
		float rw, float r1, float r2)
{
	//float rw;	/* smoothing parameter for window */
	//float r1;	/* smoothing parameter for x1 direction */
	//float r2;	/* smoothing parameter for x2 direction */

	int ix, iz;
	int n1;		/* number of points in x1 (fast) dimension */
	int n2;		/* number of points in x2 (slow) dimension */
	int nmax;	/* max of n1 and n2 */
	int *win=NULL;	/* 1d array defining the corners of smoothing window */
	float **w=NULL;	/* intermediate array */
	float *f=NULL;	/* intermediate array */
	float *d=NULL, *e=NULL;	/* input arrays for subroutine tripd */

	n1 = ncol;
	n2 = nrow;

	/* scale the smoothing parameter */
	r1 = r1*r1*0.25;
	r2 = r2*r2*0.25;

	/* allocate space */
	nmax = (n1<n2)?n2:n1;
	win = alloc1int(4);
	w = alloc2float(n1,n2);
	f = alloc1float(nmax);
	d = alloc1float(nmax);
	e = alloc1float(nmax);

	win[0] = 0;
	win[1] = n1;
	win[2] = 0;
	win[3] = n2;
	rw = rw*rw*0.25;

	/* define the window function */
	for(ix=0; ix<n2; ++ix)
		for(iz=0; iz<n1; ++iz)
			w[ix][iz] = 0;

	for(ix=win[2]; ix<win[3]; ++ix)
		for(iz=win[0]; iz<win[1]; ++iz)
			w[ix][iz] = 1;

	if(win[0]>0 || win[1]<n1 || win[2]>0 || win[3]<n2)
	{
		/*	smooth the window function */
		for(iz=0; iz<n1; ++iz)
		{
			for(ix=0; ix<n2; ++ix)
			{
				d[ix] = 1.0+2.0*rw;
				e[ix] = -rw;
				f[ix] = w[ix][iz];
			}
			d[0] -= rw;
			d[n2-1] -= rw;
			tripd(d,e,f,n2);
			for(ix=0; ix<n2; ++ix)
				w[ix][iz] = f[ix];
		}

		for(ix=0; ix<n2; ++ix)
		{
			for(iz=0; iz<n1; ++iz)
			{
				d[iz] = 1.0+2.0*rw;
				e[iz] = -rw;
				f[iz] = w[ix][iz];
			}
			d[0] -= rw;
			d[n1-1] -= rw;
			tripd(d,e,f,n1);
			for(iz=0; iz<n1; ++iz)
				w[ix][iz] = f[iz];
		}
	}

	/*      solving for the smoothing velocity */
	for(iz=0; iz<n1; ++iz)
	{
		for(ix=0; ix<n2-1; ++ix)
		{
			d[ix] = 1.0+r2*(w[ix][iz]+w[ix+1][iz]);
			e[ix] = -r2*w[ix+1][iz];
			f[ix] = v[ix][iz];
		}
		d[0] -= r2*w[0][iz];
		d[n2-1] = 1.0+r2*w[n2-1][iz];
		f[n2-1] = v[n2-1][iz];
		tripd(d,e,f,n2);
		for(ix=0; ix<n2; ++ix)
			v[ix][iz] = f[ix];
	}
	for(ix=0; ix<n2; ++ix)
	{
		for(iz=0; iz<n1-2; ++iz)
		{
			d[iz] = 1.0+r1*(w[ix][iz+1]+w[ix][iz+2]);
			e[iz] = -r1*w[ix][iz+2];
			f[iz] = v[ix][iz+1];
		}
		f[0] += r1*w[ix][1]*v[ix][0];
		d[n1-2] = 1.0+r1*w[ix][n1-1];
		f[n1-2] = v[ix][n1-1];
		tripd(d,e,f,n1-1);
		for(iz=0; iz<n1-1; ++iz)
			v[ix][iz+1] = f[iz];
	}
}
