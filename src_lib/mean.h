/*
 * mean.h
 *
 *  Created on: Nov 19, 2012
 *      Author: toto
 */

#ifndef MEAN_H_
#define MEAN_H_

#include <math.h>
#include <stdlib.h>
float mean(float *a, int n);
/*< get a mean value >*/


float alphamean(float *temp, int nfw, float alpha);
/*< get a alpha-trimmed-mean value >*/


float rangemean(float *temp, int nfw, float pclip);
/*< get a modified-trimmed-mean value >*/

float sdeviation(float *temp, int nfw);
/*< calculate a standard deviation >*/

float medianfilter(float *temp,int nfw);
/*< get a median value >*/
#endif /* MEAN_H_ */
