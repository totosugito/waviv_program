/*
 * cadzowLib.h
 *
 *  Created on: Apr 23, 2012
 *      Author: toto
 */

#ifndef CADZOWLIB_H_
#define CADZOWLIB_H_

#ifdef SEISUNIX
#include "segy_lib.h"
#else
#include <cpromax.h>
#endif


#include "svdlapack.h"
#include "filterLib.h"
#include "fftLib.h"

typedef struct hankel1d
{
	int m,l;//m:row l:col
	double **data;
} hankel1d;

typedef struct hankel2d
{
    int s; //subhankel
    int m;
    int l;
    int ss;
    int mm;
    int ll;
    int hm;
    int hl;
	double** data;
} hankel2d;

void runCadzow1D(float **data, int rank, int nx, int nz, int nf, int nfft, float d1,
		int tmin, int tmax);
void robustRunCadzow1D(float **data, int rank, int nx, int nz, int nf, int nfft, float d1,
		int tmin, int tmax, float e, int maxiter);
void reweight(double *T, double *S, double e, int n);
//cadzow using wosa windowing method
void runCadzow1D_wosa(float **data, int rank, int nx, int nz, float dt,
		int use, int span, int usetrace, int spantrace, float sigmax,
		float sigmay);
void runCadzow1D_wosa_rev1(float **data, int rank, int nx, int nz, float dt,
		int use, int span, int usetrace, int spantrace, float sigmax,
		float sigmay);
void robustRunCadzow1D_wosa_rev1(float **data, int rank, int nx, int nz, float dt,
		int use, int span, int usetrace, int spantrace, float sigmax,
		float sigmay, float e, int maxiter);

void runCadzow2D(float **data, int ntrc, int nsp, float dt, int rank,
		int nsort1, int nsort2, float ffbeg, float ffend, int fillzero);
void processDataCadzow2d(double **data, int nf, int ntrc, int rank, int nsort1, int nsort2,
		int fbeg, int fend, int fillzero);

hankel1d *hankel1d_alloc(int n);
hankel2d *hankel2d_alloc(int row, int col);
void freehankel1d(hankel1d* hankel);
void freehankel2d(hankel2d* hankel);

hankel1d *createhankel1d(double* data, int n);
hankel1d *createhankel1d_modif(double* data, int n);
hankel2d *createhankel2d(double** in, int rowin, int colin);
hankel2d *createhankel2d_modif(double** in, int rowin, int colin);

double **rankApproximation_v2(double** data,int row,int col, int rank);
double **getrank_v21(double** u,double* s,double** v, int row, int col, int rank);
double **getrank_v2(double** u,double** s,double** v, int row, int col, int rank);
double **inverseHankel2D(hankel2d* hankel);
void inverseHankel1D(hankel1d *hankel, double *sumcol);
double *average(double** data, int m, int l);
void stack(double* d1,double* d2,int l);
void divarr(double *d1,double div, int l);
void savedata(double **data, int nx, int ny);

void getfreqslice(double **data, int rowdata, int coldata, int fidx, double *fslice);

//WOSA
void gaussian2dmask(float **data, int nx, int ny,float sigmax, float sigmay);
void getsubmatrix(float **data, float **window, int cx, int cy, int span,int spantrace);
void mask(float **input, float **mask, int nrow, int ncol);
//void get1dsubmatrix(int* dest, int* source,int beg, int end);
void addsubmatrix(float **data, float **window, int cx, int cy, int span,int spantrace);
#endif /* CADZOWLIB_H_ */
