/*
 * fftLib.c
 *
 *  Created on: May 4, 2012
 *      Author: toto
 */
#include "fftLib.h"
#define CFL 0.7
#define LOOKFAC	2	/* Look ahead factor for npfaro	  */
#define PFA_MAX	720720	/* Largest allowed nfft	          */
#define SF_PI (3.14159265358979323846264338328)


void swap(float *x,float *y)
{
   int temp;
   temp = *x;
   *x = *y;
   *y = temp;
}

//http://cprogramminglanguage.net/c-bubble-sort-source-code.aspx
void bublesort(float list[], int n)
{
   int i,j;
   for(i=0;i<(n-1);i++)
      for(j=0;j<(n-(i+1));j++)
             if(list[j] > list[j+1])
                    swap(&list[j],&list[j+1]);
}

float getMedian(float list[], int n)
{
	float out;
	int pos1=0;
	int pos2=0;

	bublesort(list, n);
	if(n%2==0)
	{
		pos1 = round(n/2);
		out = list[pos1];
	}
	else
	{
		pos1 = round(n/2);
		pos2 = pos1+1;
		out = (list[pos1] + list[pos2])/2;
	}
	return(out);
}

/*
  * double d1;		sample interval in Hz
  * int nfft;		transform length
  * int nf;			number of frequencies
  */
void getNfft(int nz, float dt, int *nf, int *nfft, float *d1)
{
	/* ------------  add. get nf, d1(df) and nfft for fftomega process --------------------- */
	float d11;
	int nf1, nfft1;

	nfft1 = npfaro(nz, LOOKFAC *nz);
	if (nfft1 >= PFA_MAX)
	{
		fprintf(stderr, "Padded nsp=%d--too big", nfft1);
		exit(0);
	}

	nf1 = nfft1/2 + 1;

	d11 = 1.0/(nfft1*dt);

	(*nf) = nf1;
	(*nfft) = nfft1;
	(*d1) = d11;
}

void fftProcess(float **data, int nsp, int nx, int nf, int nfft,
		float **dreal, float **dimag)
{
	int ifq, itr, it;
	complex *ct;		/* complex transformed trace (window)  	*/
	float *tidataw;       	/* real trace in time window - input   	*/

	/* space allocation */
	ct = alloc1complex(nfft);
	tidataw = alloc1float(nfft);

	for(itr=0; itr<nx; itr++)  //loop over trace
	{
		memcpy(tidataw, data[itr], nsp*sizeof(float));

		memset((void *) (tidataw + nsp), 0, (nfft-nsp)*FSIZE);
		memset((void *) ct, 0, nfft*sizeof(complex));

		/* FFT from t to f */
		for (it=0;it<nfft;it++)
			tidataw[it]=(it%2 ? -tidataw[it] : tidataw[it]);
		pfarc(1, nfft, tidataw, ct);

		/* Store values */
		for (ifq = 0; ifq < nf; ifq++) {
			dreal[itr][ifq] = (float) ct[nf-1-ifq].r;
			dimag[itr][ifq] = (float) ct[nf-1-ifq].i;
		}
	}

	/* free allocated memory */
	free1float(tidataw);
	free1complex(ct);
}

void ifftProcess(float **data, int nsp, int nx, int nf, int nfft,
		float **dreal, float **dimag)
{
	int it, ifq, itr, itt;
	complex *ct;		/* complex transformed trace (window)  	*/
	complex *freqv;		/* frequency vector		      	*/
	float *todataw;       	/* real trace in time window - output   */

	/* space allocation */
	ct = alloc1complex(nfft);
	freqv = alloc1complex(nfft);
	todataw = alloc1float(nfft);

	for(itr=0; itr<nx; itr++)  //loop over trace
	{
		/* select data */
		for (ifq=0,itt=nf-1;ifq<nf;ifq++,itt--){
			freqv[ifq].r = (float) dreal[itr][itt];
			freqv[ifq].i = (float) dimag[itr][itt];
		}

		memset((void *) (freqv+nf), 0, (nfft-nf)*sizeof(complex));
		memset((void *) todataw, 0, nfft*FSIZE);

		/* FFT back from f to t and scaling */
		pfacr(-1, nfft, freqv, todataw);
		for (it=0;it<MIN(nsp,nfft);it++)
			data[itr][it] = (it%2 ? (-todataw[it]/nfft) : (todataw[it]/nfft));
	}

	/* free allocated memory */
	free1float(todataw);
	free1complex(freqv);
	free1complex(ct);
}

void ifftProcess1(float *data, double d1, int nsp, int nx, int nf, int nfft,
		float *dreal, float *dimag)
{
	int it, ifq;
	complex *ct;		/* complex transformed trace (window)  	*/
	complex *freqv;		/* frequency vector		      	*/
	float *todataw;       	/* real trace in time window - output   */

	/* space allocation */
	ct = alloc1complex(nfft);
	freqv = alloc1complex(nfft);
	todataw = alloc1float(nfft);

	/* select data */
	for (ifq=0,it=nf-1;ifq<nf;ifq++,it--){
		freqv[ifq].r = (float) dreal[it];
		freqv[ifq].i = (float) dimag[it];
	}

	memset((void *) (freqv+nf), 0, (nfft-nf)*sizeof(complex));
	memset((void *) todataw, 0, nfft*FSIZE);

	/* FFT back from f to t and scaling */
	pfacr(-1, nfft, freqv, todataw);
	for (it=0;it<MIN(nsp,nfft);it++)
		data[it] = (it%2 ? (-todataw[it]/nfft) : (todataw[it]/nfft));

	/* free allocated memory */
	free1float(todataw);
	free1complex(freqv);
	free1complex(ct);
}

void fftProcessd(float **data, int nsp, int nx, int nf, int nfft,
		double **dreal, double **dimag)
{
	int ifq, itr, it;
	complex *ct;		/* complex transformed trace (window)  	*/
	float *tidataw;       	/* real trace in time window - input   	*/

	/* space allocation */
	ct = alloc1complex(nfft);
	tidataw = alloc1float(nfft);

	for(itr=0; itr<nx; itr++)  //loop over trace
	{
		memcpy(tidataw, data[itr], nsp*sizeof(float));

		memset((void *) (tidataw + nsp), 0, (nfft-nsp)*FSIZE);
		memset((void *) ct, 0, nfft*sizeof(complex));

		/* FFT from t to f */
		for (it=0;it<nfft;it++)
			tidataw[it]=(it%2 ? -tidataw[it] : tidataw[it]);

		pfarc(1, nfft, tidataw, ct);

		/* Store values */
		for (ifq = 0; ifq < nf; ifq++) {
			dreal[itr][ifq] = (double) ct[nf-1-ifq].r;
			dimag[itr][ifq] = (double) ct[nf-1-ifq].i;
		}
	}

	/* free allocated memory */
	free1float(tidataw);
	free1complex(ct);
}

void ifftProcessd(float **data, int nsp, int nx, int nf, int nfft,
		double **dreal, double **dimag)
{
	int it, ifq, itr, itt;
	complex *ct;		/* complex transformed trace (window)  	*/
	complex *freqv;		/* frequency vector		      	*/
	float *todataw;       	/* real trace in time window - output   */

	/* space allocation */
	ct = alloc1complex(nfft);
	freqv = alloc1complex(nfft);
	todataw = alloc1float(nfft);

	for(itr=0; itr<nx; itr++)  //loop over trace
	{
		/* select data */
		for (ifq=0,itt=nf-1;ifq<nf;ifq++,itt--){
			freqv[ifq].r = (float) dreal[itr][itt];
			freqv[ifq].i = (float) dimag[itr][itt];
		}

		memset((void *) (freqv+nf), 0, (nfft-nf)*sizeof(complex));
		memset((void *) todataw, 0, nfft*FSIZE);

		/* FFT back from f to t and scaling */
		pfacr(-1, nfft, freqv, todataw);
		for (it=0;it<MIN(nsp,nfft);it++)
			data[itr][it] = (it%2 ? (-todataw[it]/nfft) : (todataw[it]/nfft));
	}

	/* free allocated memory */
	free1float(todataw);
	free1complex(freqv);
	free1complex(ct);
}

/* ------------------------------------------------------------------------------------
 * ----------------------------------UPDATE CODE--------------------------------------------------
 * ------------------------------------------------------------------------------------ */
void fft1_1ff(float *data, int nsp, int nf, int nfft,
		float *dreal, float *dimag)
{
	int ifq;
	complex *ct;		/* complex transformed trace (window)  	*/
	float *tidataw;       	/* real trace in time window - input   	*/

	/* space allocation */
	ct = alloc1complex(nfft);
	tidataw = alloc1float(nfft);

	memcpy(tidataw, data, nsp*sizeof(float));

	memset((void *) (tidataw + nsp), 0, (nfft-nsp)*FSIZE);
	memset((void *) ct, 0, nfft*sizeof(complex));

	for (ifq=0; ifq<nfft; ifq++)
		tidataw[ifq]=(ifq%2 ? -tidataw[ifq] : tidataw[ifq]);

	/* FFT from t to f */
	pfarc(1, nfft, tidataw, ct);

	/* Store values */
	for (ifq = 0; ifq < nf; ifq++) {
		dreal[ifq] = (float) ct[nf-1-ifq].r;
		dimag[ifq] = (float) ct[nf-1-ifq].i;
	}

	/* free allocated memory */
	free1float(tidataw);
	free1complex(ct);
}

void ifft1_1ff(float *data, int nsp, int nf, int nfft,
		float *dreal, float *dimag)
{
	int ifq, itt;
	complex *ct;		/* complex transformed trace (window)  	*/
	complex *freqv;		/* frequency vector		      	*/
	float *todataw;       	/* real trace in time window - output   */

	/* space allocation */
	ct = alloc1complex(nfft);
	freqv = alloc1complex(nfft);
	todataw = alloc1float(nfft);

	/* select data */
	for (ifq=0,itt=nf-1;ifq<nf;ifq++,itt--){
		freqv[ifq].r = (float) dreal[itt];
		freqv[ifq].i = (float) dimag[itt];
	}

	memset((void *) (freqv+nf), 0, (nfft-nf)*sizeof(complex));
	memset((void *) todataw, 0, nfft*FSIZE);

	/* FFT back from f to t and scaling */
	pfacr(-1, nfft, freqv, todataw);
	for (ifq=0; ifq<MIN(nsp,nfft); ifq++)
		data[ifq] = (ifq%2 ? (-todataw[ifq]/nfft) : (todataw[ifq]/nfft));

	/* free allocated memory */
	free1float(todataw);
	free1complex(freqv);
	free1complex(ct);
}

/*
 * type = 1 --> fft from row i ... n
 * type = 2 --> fft from column j ... n
 */
void fft1_2ff(float **data, int nsp, int nx, int nf, int nfft,
		float **dreal, float **dimag, int type)
{
	int ifq, itr, j;
	complex *ct;		/* complex transformed trace (window)  	*/
	float *tidataw;       	/* real trace in time window - input   	*/
	int floop, ndata;

	/* space allocation */
	ct = alloc1complex(nfft);
	tidataw = alloc1float(nfft);

	if(type==1){
		floop = nx;
		ndata = nsp;
	}
	else {
		floop = nsp;
		ndata = nx;
	}

	for(itr=0; itr<floop; itr++)  //loop over trace
	{
		if(type==1)
			memcpy(tidataw, data[itr], ndata*sizeof(float));
		else{
			for(j=0; j<ndata; j++)
				tidataw[j] = data[j][itr];
		}

		//memset((void *) (tidataw + ndata), 0, (nfft-ndata)*FSIZE);
		memset((void *) ct, 0, nfft*sizeof(complex));

		for (ifq=0; ifq<nfft; ifq++)
			tidataw[ifq]=(ifq%2 ? -tidataw[ifq] : tidataw[ifq]);

		pfarc(1, nfft, tidataw, ct);			/* FFT from t to f */

		/* Store values */

		for (ifq = 0; ifq < nf; ifq++)
		{
			if(type==1)
			{
				dreal[itr][ifq] = (float) ct[nf-1-ifq].r;
				dimag[itr][ifq] = (float) ct[nf-1-ifq].i;
			}
			else
			{
				dreal[ifq][itr] = (float) ct[nf-1-ifq].r;
				dimag[ifq][itr] = (float) ct[nf-1-ifq].i;
			}
		}
	}

	/* free allocated memory */
	free1float(tidataw);
	free1complex(ct);
}

/*
 * type = 1 --> fft from row i ... n
 * type = 2 --> fft from column j ... n
 */
void ifft1_2ff(float **data, int nsp, int nx, int nf, int nfft,
		float **dreal, float **dimag, int type)
{
	int ifq, itr, itt;
	complex *ct;		/* complex transformed trace (window)  	*/
	complex *freqv;		/* frequency vector		      	*/
	float *todataw;       	/* real trace in time window - output   */
	int floop, ndata;

	/* space allocation */
	ct = alloc1complex(nfft);
	freqv = alloc1complex(nfft);
	todataw = alloc1float(nfft);

	if(type==1){
		floop = nx;
		ndata = nsp;
	}
	else {
		floop = nsp;
		ndata = nx;
	}

	for(itr=0; itr<floop; itr++)  //loop over trace
	{
		/* select data */
		for (ifq=0,itt=nf-1;ifq<nf;ifq++,itt--)
		{
			if(type==1)
			{
				freqv[ifq].r = (float) dreal[itr][itt];
				freqv[ifq].i = (float) dimag[itr][itt];
			}
			else
			{
				freqv[ifq].r = (float) dreal[itt][itr];
				freqv[ifq].i = (float) dimag[itt][itr];
			}
		}

		memset((void *) (freqv+nf), 0, (nfft-nf)*sizeof(complex));
		memset((void *) todataw, 0, nfft*FSIZE);

		/* FFT back from f to t and scaling */
		pfacr(-1, nfft, freqv, todataw);

		for (ifq=0; ifq<MIN(ndata, nfft); ifq++)
		{
			if(type==1)
				data[itr][ifq] = (ifq%2 ? (-todataw[ifq]/nfft) : (todataw[ifq]/nfft));
			else
				data[ifq][itr] = (ifq%2 ? (-todataw[ifq]/nfft) : (todataw[ifq]/nfft));
		}
	}

	/* free allocated memory */
	free1float(todataw);
	free1complex(freqv);
	free1complex(ct);
}
