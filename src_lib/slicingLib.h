/*
 * slicingLib.h
 *
 *  Created on: Jan 27, 2012
 *      Author: toto
 */

#ifndef SLICINGLIB_H_
#define SLICINGLIB_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "par.h"
#include "wave_3dlib.h"
#include "iolibrary.h"
#include "segy_lib.h"

void createTimeSlicing(FILE *segyinp, char *ctmpfile, int ntrc, int tmax,
		int nsp, int nsegy, int format, int endian,
		int nslice, int tslice, int **tslicearray, int vblock);

char *createSlicingFileName(char *ctmpfile, int tidx1, int tidx2);
void savingSlicingFile(char *ctmpfile, float **dataslice, int islice, int tslice,
		int ntrc, int **tslicearray);
void readSlicingFile(float **data, char *tmpfileslice, int islice, int nslice, int ntrc,
		int **tslicearray);
void removeSlicingFile(char *tmpfileslice);
void mergeSlicingFile(char *cfin, float **data,
		int deltat, int ntrc, int islice, int nslice,
		int lendata, int **tslicearray, int vblock);
void mergeSlicingFile_v2(char *cfin, float **data,
		int deltat, int ntrc, int islice, int nslice,
		int lendata, int **tslicearray, int vblock);

void timeslice_to_Data2d(float *data,
		float **data2d, int winline, int wxline, pos3ddata **posdata);
void Data2d_to_timeslice(float *data,
		float **data2d, int winline, int wxline, pos3ddata **posdata);
#endif /* SLICINGLIB_H_ */
