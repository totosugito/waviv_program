#include "waviv_common.h"
void savearray2d(char* outfilename,double** array,int row,int col){
	FILE* outfileptr = efopen(outfilename,"w");
	float* outfloat = (float*) calloc(col,sizeof(float));
	int i,j;
	for(i=0;i<row;i++){
		for(j=0;j<col;j++)outfloat[j]=(float)array[i][j];
		efwrite(outfloat,1,col*sizeof(float),outfileptr);
	}
	free(outfloat);
	fclose(outfileptr);
}

void savearray1f(char* outfilename,float* array,int col){
	FILE* outfileptr = efopen(outfilename,"r");
	efwrite(array,1,col*sizeof(float),outfileptr);
	fclose(outfileptr);
}

void getcol(float** input, int colselect, float* coldata, int nrow){
	int i;
	for(i=0;i<nrow;i++){
		coldata[i]=input[i][colselect];
	}
}

void setcol(float** input, int colselect, float* coldata, int nrow){
	int i;
	for(i=0;i<nrow;i++){
		input[i][colselect]=coldata[i];
	}
}

void getrow(float** input, int rowselect, float* rowdata, int ncol){
	memcpy(rowdata,input[rowselect],ncol*sizeof(float));
}

void setrow(float** input, int rowselect, float* rowdata, int ncol){
	memcpy(input[rowselect],rowdata,ncol*sizeof(float));
}

void getcold(double** input, int colselect, double* coldata, int nrow){
	int i;
	for(i=0;i<nrow;i++){
		coldata[i]=input[i][colselect];
	}
}

void setcold(double** input, int colselect, double* coldata, int nrow){
	int i;
	for(i=0;i<nrow;i++){
		input[i][colselect]=coldata[i];
	}
}

void getrowd(double** input, int rowselect, double* rowdata, int ncol){
	memcpy(rowdata,input[rowselect],ncol*sizeof(double));
}

void setrowd(double** input, int rowselect, double* rowdata, int ncol){
	memcpy(input[rowselect],rowdata,ncol*sizeof(double));
}

float** ftranspose(float** data, int row, int col){
	float** output = (float**) ealloc2float(row,col);

	int i,j;	
	for(i=0;i<row;i++){
		for(j=0;j<col;j++){
			output[j][i]=data[i][j];
		}
	}
	return output;
}

double** dtranspose(double** data, int row, int col){
	double** output = (double**) ealloc2double(row,col);

	int i,j;	
	for(i=0;i<row;i++){
		for(j=0;j<col;j++){
			output[j][i]=data[i][j];
		}
	}
	return output;
}

double* todouble1d(float* data, int n){
	double* out = (double*) calloc(n,sizeof(double));
	int i;	
	for(i=0;i<n;i++){
		out[i]=(double) data[i];
	}
	return out;
}

float* tofloat1d(double* data, int n){
	float* out = (float*) calloc(n,sizeof(float));
	int i;	
	for(i=0;i<n;i++){
		out[i]=(float) data[i];
	}
//        float a = (float) 3/  2.0;
	return out;
}

double** todouble2d(float** data, int row, int col){
	double** output = (double**) ealloc2double(col,row);
	int i,j;
	for(i=0;i<row;i++){
		for(j=0;j<col;j++){
			output[i][j]=(double)data[i][j];
		}
	}
	return output;
}

float** tofloat2d(double** data, int row, int col){
	float** output = (float**) ealloc2float(col,row);
	int i,j;
	for(i=0;i<row;i++){
		for(j=0;j<col;j++){
			output[i][j] = (float)data[i][j];
		}
	}
	return output;
}

void fill1i(int* data,int val, int len){
	int i;
	for(i=0;i<len;i++){
			data[i]=val;		
	}
}

void fill1f(float* data,float val, int len){
	int i;
	for(i=0;i<len;i++){
			data[i]=val;		
	}
}

void fill1d(double* data,double val, int len){
	int i;
	for(i=0;i<len;i++){
			data[i]=val;		
	}
}

void fill2d(double** data,double val, int row,int col){
	int i,j;
	for(i=0;i<row;i++){
		for(j=0;j<col;j++){
			data[i][j]=val;		
		}
	}
}

void taper1dlin(float* data, int ns, int s){
	float m = 1.0/s;
	int i;
	float tap=0.0;
	for(i=0;i<s;i++){
		data[i]*=tap;
		tap+=m;
	}
	tap=1.0;
	for(i=ns-s-1;i<ns;i++){
		data[i]*=tap;
		tap-=m;
	}
}

hankel1d* createhankel1d(double* data, int n){
	hankel1d* hankel = malloc(sizeof(hankel1d)); 
	hankel->m = (n+1)/2; // m=row
	hankel->l = n-hankel->m+1; // l=row
	hankel->data = (double**) ealloc2double(hankel->l,hankel->m);

	int idx,row,col;
	for(row=0;row<hankel->m;row++){
		idx=row;
		for(col=0;col<hankel->l;col++){
			hankel->data[row][col]=data[idx];
			idx++;
		}
	}
	return hankel;
}

hankel1d* createhankelM1d(double* data, int n, int m){
	hankel1d* hankel = malloc(sizeof(hankel1d)); 
	hankel->m = m;
	hankel->l = n-hankel->m+1;
	hankel->data = (double**) ealloc2double(hankel->l,hankel->m);

	int idx,row,col;
	for(row=0;row<hankel->m;row++){
		idx=row;
		for(col=0;col<hankel->l;col++){
			hankel->data[row][col]=data[idx];
			idx++;
		}
	}
	return hankel;
}

hankel2d* createhankel2d(double** in, int rowin, int colin){
	hankel2d* hankel = malloc(sizeof(hankel2d));
	hankel->s = colin;
    hankel->m = (hankel->s+1)/2;
    hankel->l = (hankel->s-hankel->m+1);
        
    hankel->ss = rowin;
    hankel->mm = (hankel->ss+1)/2;
    hankel->ll = hankel->ss-hankel->mm+1;
     
    hankel->hm = hankel->m*hankel->mm;
    hankel->hl = hankel->l*hankel->ll;
	hankel->data = ealloc2double(hankel->hl,hankel->hm);
	
	int beg=0;
	int row,col,r,c,i,j;
	
    for(row=0;row<hankel->mm;row++){
        for(col=0;col<hankel->ll;col++){
			hankel1d* sub = createhankel1d(in[beg+col],colin);
            r=(row*hankel->m);  
            for(i=0;i<hankel->m;i++){
                c=(col*hankel->l);
                for(j=0;j<hankel->l;j++){
                    hankel->data[r][c]=sub->data[i][j];
                    c++;
                }
                r++;
            }
			freehankel1d(sub);
        }
        beg++;
    }
	return hankel;
}

double* inverseHankel(hankel1d* hankel){
	int m = hankel->m;
	int l = hankel->l;
	double* val = (double*) calloc((l*m),sizeof(double));
	double* avg = (double*) calloc((l+m-1),sizeof(double)); 

	int dx=0;
	int i,j;
	for(i=0;i<m;i++){
		for(j=0;j<l;j++){
			val[dx]=hankel->data[i][j];
			dx++;
		}
	}

	for(i=0;i<m;i++){
		double cnt=0;
		for(j=0;j<i+1;j++){
			avg[i]+=val[i+(j*(l-1))];
			cnt++;
		}
		avg[i]/=cnt;
	}

	if(m==l){
		int cnt  = l-1;
		int mdex = 2;
		for(i=m;i<l+m-1;i++){
			int dex = (mdex*l)-1;
			int mnt = 0;
			for(j=0;j<cnt;j++){
				mnt++;
				avg[i]+=val[dex];
				dex+=(l-1);
			}
			avg[i]/=cnt;
			cnt--;
			mdex++;
		}

	}else{
		int cnt  = m;
		int mdex = 1;
		for(i=m;i<l+m-1;i++){
			int dex = (mdex*l)-1;
			int mnt = 0;
			for(j=0;j<cnt;j++){
				mnt++;
				avg[i]+=val[dex];
				dex+=(l-1);
			}
			avg[i]/=cnt;
			cnt--;
			mdex++;
		}
	}
	free(val);
    return avg;
}

double* inverseHankelM1d(hankel1d* hankel){
	int m = hankel->m;
	int l = hankel->l;
	double* val = (double*) calloc((l*m),sizeof(double));
	double* avg = (double*) calloc((l+m-1),sizeof(double)); 

	int dx=0;
	int i,j;
	for(i=0;i<m;i++){
		for(j=0;j<l;j++){
			val[dx]=hankel->data[i][j];
			dx++;
		}
	}

	for(i=0;i<m;i++){
		double cnt=0;
		for(j=0;j<i+1;j++){
			avg[i]+=val[i+(j*(l-1))];
			cnt++;
		}
		avg[i]/=cnt;
	}

	if(m==l){
		int cnt  = l-1;
		int mdex = 2;
		for(i=m;i<l+m-1;i++){
			int dex = (mdex*l)-1;
			for(j=0;j<cnt;j++){
				avg[i]+=val[dex];
				dex+=(l-1);
			}
			avg[i]/=cnt;
			cnt--;
			mdex++;
		}

	}else{
		int cnt  = m;
		int mdex = 1;
		for(i=l-1;i<l+m-1;i++){
			int dex = (mdex*l)-1;

			for(j=0;j<cnt;j++){

				avg[i]+=val[dex];
				dex+=(l-1);
			}
			avg[i]/=cnt;
			cnt--;
			mdex++;
		}
		cnt  = m;
		int dex;
		for(i=m;i<l-1;i++){
			avg[i]=0.0;
			dex=i;
			for(j=0;j<cnt;j++){
				avg[i]+=val[dex+(j*(l-1))];
			}
			avg[i]/=cnt;
			dex++;
		}
	}
	free(val);
    return avg;
}

double** inverseHankel2D(hankel2d* hankel){
    int ssdex=0;
	int row,col,i,j;
	int mm=hankel->mm;
	int ll=hankel->ll;
	int m=hankel->m;
	int l=hankel->l;
	int s=hankel->s;
	int ss=hankel->ss;
	
	double** list = ealloc2double(l+m-1,mm*ll);
	
	int indexlist=0;
    for( row=0;row<mm;row++){
        for( col=0;col<ll;col++){
            double** sub = ealloc2double(l,m);
            int r=(row*m);
            for( i=0;i<m;i++){
                int c=(col*l);
                for( j=0;j<l;j++){
                    sub[i][j]=hankel->data[r][c];
                    c++;                        
                }
                r++;
            }
			double* av = average(sub,m,l);
			
			memcpy(list[indexlist],av,(m+l-1)*sizeof(double));
			indexlist++;
            ssdex++;
			free1double(av);
			free2double(sub);
        }
    }
	
    double** inv = ealloc2double(s,ss);
	
	for( i=0;i<mm;i++){
        double* a = calloc(s,sizeof(double));
        double cnt=0;
		
        for( j=0;j<i+1l;j++){
            stack(a,list[(i+(j*(ll-1)))],s);
            cnt++;
        }
		
        divarr(a,cnt,s);
		memcpy(inv[i],a,s*sizeof(double));
		free1double(a);
    }
        
    if(mm==ll){
        int cnt  = ll-1;
        int mdex = 2;
        for( i=mm;i<mm+ll-1;i++){
            int dex = (mdex*ll)-1;
            double* a = calloc(s,sizeof(double));
            for( j=0;j<cnt;j++){
                stack(a,list[dex],s);
                dex+=(ll-1);
            }
            divarr(a,cnt,s);
            memcpy(inv[i],a,s*sizeof(double));
            cnt--;
            mdex++;
			free1double(a);
        }
    }else{
        int cnt  = mm;
        int mdex = 1;
        for( i=mm;i<ll+mm-1;i++){
            int dex = (mdex*ll)-1;
            double* a = calloc(s,sizeof(double));
            for( j=0;j<cnt;j++){
                stack(a,list[dex],s);
                dex+=(ll-1);
            }
            divarr(a,cnt,s);
            memcpy(inv[i],a,s*sizeof(double));
            cnt--;
            mdex++;
			free1double(a);
        }
    }
    return inv;
}
	
double* average(double** data, int m, int l){
	if(l<m){
		printf("collum < row : this procedure are not intended for this condition\n");
	}
	double* val = (double*) calloc((l*m),sizeof(double));
	double* avg = (double*) calloc((l+m-1),sizeof(double)); 

	int dx=0;
	int i,j;
	for(i=0;i<m;i++){
		for(j=0;j<l;j++){
			val[dx]=data[i][j];
			dx++;
		}
	}

	double cnt=0;
	for(i=0;i<m;i++){
		cnt=0;
		for(j=0;j<i+1;j++){
			avg[i]+=val[i+(j*(l-1))];
			cnt++;
		}
		avg[i]/=cnt;
	}
	
	if(m==l){
		cnt  = l-1;
		int mdex = 2;
		for(i=m;i<l+m-1;i++){
			int dex = (mdex*l)-1;
			int mnt = 0;
			for(j=0;j<cnt;j++){
				mnt++;
				avg[i]+=val[dex];
				dex+=(l-1);
			}
			avg[i]/=cnt;
			cnt--;
			mdex++;
		}

	}else{
		cnt  = m;
		int mdex = 1;
		for(i=m;i<l+m-1;i++){
			int dex = (mdex*l)-1;
			int mnt = 0;
			for(j=0;j<cnt;j++){
				mnt++;
				avg[i]+=val[dex];
				dex+=(l-1);
			}
			avg[i]/=cnt;
			cnt--;
			mdex++;
		}
	}
    return avg;
}

void stack(double* d1,double* d2,int l){
	int i;
    for(i=0;i<l;i++){
        d1[i]+=d2[i];
    }
}

void divarr(double* d1,double div, int l){
    int i;
	for(i=0;i<l;i++){
        d1[i]/=div;
    }
} 

void freehankel1d(hankel1d* hankel){
	free2double(hankel->data);
	free(hankel);
}

void writearray(FILE* outfileptr,float** data, int n1, int n2){
	int row,col;	
	for(row=0;row<n1;row++){
		for(col=0;col<n2;col++){
			efwrite(&data[row][col],1,sizeof(float),outfileptr);	
		}   
	}
}

void convolve1d(float* function, int flength, float* kernel, int klength)
{
	int span=(klength-1)/2;
	int plength = (flength+(2*span));
	float* panel = (float*) calloc( plength , sizeof(float));
	memcpy(&panel[span],&function[0],sizeof(float)*flength);
	int i;
	for(i=0;i<span;i++){panel[i]=function[0];}
	for(i=plength-span;i<plength;i++){panel[i]=function[flength-1];}

	int indexConv;
	float ConvReslt;
	int cc;
	float* D = (float*) calloc(klength,sizeof(float));
	for(i=span; i<(plength-span);i++){
	    fill1f(D,0.0,klength);
		indexConv=0;
		ConvReslt=0;
		for(cc=i-span; cc<=i+span; cc++){
                ConvReslt+=(panel[cc] * kernel[indexConv]);
				indexConv++;
		}
		function[i-span]=ConvReslt;
	}
	free(D);
	free(panel);
}

void convolven1(float** input, int n1, int n2, float* kernel, int klength){
	int row;
	for(row=0;row<n1;row++){
		convolve1d(input[row],n2,kernel,klength);
	}
}

void convolven2(float** input, int n1, int n2, float* kernel, int klength){
	float* panel = (float*) calloc(n1,sizeof(float));
	int col;
	for(col=0;col<n2;col++){
		getcol(input,col,panel,n1);
		convolve1d(panel,n1,kernel,klength);
		setcol(input,col,panel,n1);
	}
	free(panel);
}

float* gaussianderivative1kernel(int ksize,float sigma){
	if(ksize%2==0){printf("%i\n", ksize%2);exit(EXIT_FAILURE);}
	int span = (ksize-1) /2;
	float con = sqrt(2*3.14159)*sigma;
	float* kernel = (float*) malloc(ksize*sizeof(float));
	int x,index=0;
	for(x=span*-1; x<=span; x++){
		kernel[index]  = exp((-1.0*(float)x*(float)x)/(2.0*sigma*sigma))/con;
		kernel[index] *= ( (-1.0)* (float)x / (sigma*sigma));	
		index++;
	}
	return kernel;
}

float* morletkernel(int ksize,float sigma){
	if(ksize%2==0){printf("%i\n", ksize%2);exit(EXIT_FAILURE);}
	int span = (ksize-1) /2;
	float con = 2.0 / (sqrt(3*sigma)*1.331335083);
	float* kernel = (float*) malloc(ksize*sizeof(float));
	int x,index=0;

	for(x=span*-1; x<=span; x++){
		float xf = (float)x;
		kernel[index]  = exp((-1.0*xf*xf)/(2.0*sigma*sigma))*con;
		kernel[index] *= ( 1 - ( (xf*xf)/(sigma*sigma) ) );	
		index++;
	}
	return kernel;
}

float* gaussiankernel(int ksize,float sigma){
	if(ksize%2==0){printf("%i\n", ksize%2);exit(EXIT_FAILURE);}
	int span = (ksize-1) /2;
	float con = sqrt(2*3.14159)*sigma;
	float* kernel = (float*) malloc(ksize*sizeof(float));
	int x,index=0;
	for(x=span*-1; x<=span; x++){
		kernel[index]  = exp((-1.0*(float)x*(float)x)/(2.0*sigma*sigma))/con;	
		index++;
	}
	return kernel;
}

float* meankernel(int ksize){
	if(ksize%2==0){printf("%i\n", ksize%2);exit(EXIT_FAILURE);}
//	int span = (ksize-1) /2;
	float* kernel = (float*) malloc(ksize*sizeof(float));
	int x;	
	float sum=0;
	for(x=0; x<ksize; x++){
		kernel[x]=1.0/(float)ksize;
		sum+=kernel[x];
	}
	return kernel;
}

float* trianglekernel(int ksize){
	if(ksize%2==0){printf("%i\n", ksize%2);exit(EXIT_FAILURE);}
	ksize+=2;
	int span = (ksize-1) /2;
	float* kernelprep = (float*) malloc(ksize*sizeof(float));
	int x,ct=span+1;	
	float dval=1.0/(float)span;
	float cval=1.0-dval;
	for(x=0;x<ct;x++)kernelprep[x]=x*dval;
	for(x=ct;x<ksize;x++){
		kernelprep[x]=cval;
		cval-=dval;
	}
	ksize-=2;
	float* kernel = (float*) malloc(ksize*sizeof(float));
	for(x=0;x<ksize;x++)kernel[x]=kernelprep[x+1];
	free(kernelprep);
	return kernel;
}

void printarray1d(double* data,  int col){
	int j;
	for(j=0;j<col;j++){
		printf("%f ",data[j]);
	}
	printf("\n");
}

void printarray1i(int* data,  int col){
	int j;
	for(j=0;j<col;j++){
		printf("%i ",data[j]);
	}
	printf("\n");
}

void printarray1f(float* data,  int col){
	int j;
	for(j=0;j<col;j++){
		printf("%f ",data[j]);
	}
	printf("\n");
}

void printarray(double** data, int row, int col){
	int i,j;
	for(i=0;i<row;i++){
		for(j=0;j<col;j++){
			printf("%2.2f ",data[i][j]);
		}
		printf("\n");
	}
}

unsigned long fsize(FILE *fp){
    unsigned long prev=ftell(fp);
    fseek(fp, 0L, SEEK_END);
    unsigned long sz=ftell(fp);
    fseek(fp,prev,SEEK_SET); //go back to where we were
    return sz;
}

void timesarray(float** data1,float** data2, float** result, int row, int col){
	int i,j;
	for(i=0; i<row; i++){
		for(j=0; j<col; j++){
			result[i][j]=data1[i][j]*data2[i][j];
		}
	}
}

void powarray(float** data, int row, int col){
	int i,j;
	for(i=0; i<row; i++){
		for(j=0; j<col; j++){
			data[i][j]*=data[i][j];
		}
	}
}

void absarray(float** data, int row, int col){
	int i,j;
	for(i=0; i<row; i++){
		for(j=0; j<col; j++){
			data[i][j]=abs(data[i][j]);
		}
	}
}

double Max2d(double** Array, int row,int col){
	double max = Array[0][0];
	int i,j;
    for (i = 0; i < row; i++){
		for (j = 0; j < col; j++){
        	if (Array[i][j] > max)
        	    max = Array[i][j];
		}
    }
    return max;
}

double Maxd(double* Array, int row){
	double max = Array[0];
	int i;
    for (i = 0; i < row; i++){
        	if (Array[i]> max)
        	    max = Array[i];
    }
    return max;
}

float Maxf(float* Array, int row){
	float max = Array[0];
	int i;
    for (i = 0; i < row; i++){
        	if (Array[i]> max)
        	    max = Array[i];
    }
    return max;
}

void constructcedtensor(float** S11, float** S22, float** S12,float** D11, float** D22, float** D12,float k, int row, int col){
	int i,j;
	float c1,c2=0.01f,alpha,eigen1,eigen2;
	float temp;
	for( i=0; i<row; i++){
		for(j=0; j<col; j++){
			alpha = (float) sqrt( pow((S11[i][j]-S22[i][j]),2) + 4* pow(S12[i][j], 2) );
			eigen1 = (float) ((0.5) * (S11[i][j] + S22[i][j] - alpha));
			eigen2 = (float) ((0.5) * (S11[i][j] + S22[i][j] + alpha));
			temp = (float) ((-1) * pow(eigen1 - eigen2, 2));
			if(c2> (1 - exp(temp/(k*k))) ){
				c1=c2;
			}else{
				c1=(1 - exp(temp/(k*k)));
			}
			D11[i][j]=(float) ((0.5) * (c1 + c2 + ((c2 - c1) * (S11[i][j] - S22[i][j]) / alpha)));
			D12[i][j]= ((c2-c1) * S12[i][j]) / alpha;
			D22[i][j]=(float) ((0.5) * (c1 + c2 - ((c2 - c1) * (S11[i][j] - S22[i][j]) / alpha)));
		}
	}
}

void HaleCED(float** input,float** D11,float** D22, float** D12,
                 float alf,int cgiter, int row, int col){
	int iter=0;
	float** S = (float**) ealloc2float(col,row);
	float**  R = (float**) ealloc2float(col,row);
	float**  P = (float**) ealloc2float(col,row);
	float**  APJ = (float**) ealloc2float(col,row);
	float**  res = (float**) ealloc2float(col,row);
	float**  BTB = (float**) ealloc2float(col,row);

	int i2,i1;	
	float r00=0,r01=0,r10=0,r11=0;
	for(i2=1;i2<row;i2++){
		for(i1=1;i1<col;i1++){
			r00=input[i2  ][i1  ];
			r01=input[i2  ][i1-1];
			r10=input[i2-1][i1  ];
			r11=input[i2-1][i1-1];
			BTB[i2][i1]=0.25f*(r00+r01+r10+r11);
			S[i2][i1]=0;
			R[i2][i1]=BTB[i2][i1];
		}
	}

	dot_hale(D11,D22,D12,R,S,alf,row,col);
	clearnan2(R, row, col);
	clearnan2(S, row, col);
	int i,j;
	for(i=0;i<row;i++){
		for(j=0;j<col;j++){
			res[i][j]=(input[i][j])-S[i][j];
			P[i][j]=res[i][j];
		}
	}

	float rjrj=0;
	float rj1rj1=0;
	float alpha=0;
	float apjpj=0;
	float beta=0;
        
	int ii,jj;
	for(i=0;i<cgiter;i++){
		iter++;

		rjrj = herpr(res,res,row,col);

		dot_hale(D11,D22,D12,P,APJ,alf,row,col);
        clearnan2(APJ, row, col);
		clearnan2(P, row, col);
		apjpj = herpr(APJ,P,row,col);
		alpha = rjrj / apjpj;
                
		for( ii=0;ii<row;ii++){
			for(jj=0;jj<col;jj++){
				R[ii][jj] += alpha*P[ii][jj];
				res[ii][jj]=res[ii][jj] - (alpha*APJ[ii][jj]);
			}
		}

		rj1rj1 = herpr(res,res,row,col);
		beta = rj1rj1/rjrj;

		for(ii=0;ii<row;ii++){
			for( jj=0;jj<col;jj++){
				P[ii][jj]= res[ii][jj]+ (beta*P[ii][jj]);
				}
			}
    }

	for(ii=0;ii<row;ii++){
		memcpy(input[ii],R[ii],col*sizeof(float));
	}

	free2float(S);
	free2float(R);
	free2float(P);
	free2float(APJ);
	free2float(res);
	free2float(BTB);
}

void dot_hale(float** D11,float** D22,float** D12, float** in, float** out, float alpha, int row, int col){
	float r00=0.0,r01=0.0,r10=0.0,r11=0.0;
	float e11,e12,e22;
	float rs,ra,rb,r1,r2,s1,s2,sa,sb;
	int i2,i1;
	for( i2=1;i2<row;++i2){
		for( i1=1;i1<col;++i1){
			e11 = alpha*D11[i2][i1];
			e12 = alpha*D12[i2][i1];
			e22 = alpha*D22[i2][i1];
			r00=in[i2  ][i1  ];
			r01=in[i2  ][i1-1];
			r10=in[i2-1][i1  ];
			r11=in[i2-1][i1-1];
			rs=(r00+r01+r10+r11)/4.0f;
			ra=r00-r11;
			rb=r01-r10;
			r1=ra-rb;
			r2=ra+rb;
			s1=(e11*r1)+(e12*r2);
			s2=(e12*r1)+(e22*r2);
			sa=s1+s2;
			sb=s1-s2;
			out[i2  ][i1  ]+=sa+rs;
			out[i2  ][i1-1]-=sb-rs;
			out[i2-1][i1  ]+=sb+rs;
			out[i2-1][i1-1]-=sa-rs;
		}
	}
}

float herpr(float** X, float** Y, int row, int col){
	float val=0;
	int i,j;
	for(i=0;i<row;i++){
		for(j=0;j<col;j++){
			val+=(X[i][j]*Y[i][j]);
		}
	}
	return val;
}

void clearnan2(float** data, int row, int col){
	int kk=0,ll=0;
	for(kk=0;kk<row;kk++){
		for(ll=0;ll<col;ll++){
			if(data[kk][ll]!=data[kk][ll])data[kk][ll]=0.0;
		}
	}
}

void clearnand2(double** data, int row, int col){
	int kk=0,ll=0;
	for(kk=0;kk<row;kk++){
		for(ll=0;ll<col;ll++){
			if(data[kk][ll]!=data[kk][ll])data[kk][ll]=0.0;
		}
	}
}

void clearnan1d(double* data, int row){
	int kk=0;
	for(kk=0;kk<row;kk++){
			if(data[kk]!=data[kk])data[kk]=0.0;
	}
}

void WeickertCED(float** data,float** a,float** c, float** b,float dt, int row, int col){
	int i,j;
    float** panel = (float**) ealloc2float(col,row);
	memcpy(panel[0],data[0],sizeof(float)*col*row);

    float K00,K01,K02;
    float K10,K11,K12;
    float K20,K21,K22;
		
    for(i=1; i<row-1; i++){
        for(j=1; j<col-1; j++){
            K00 = (-1) * ( (b[i-1][j] + b[i][j+1]) / 4) * panel[i-1][j+1] ;
            K01 = ((c[i][j+1]+c[i][j]) / 2) * panel[i][j+1];
            K02 = ((b[i+1][j]+b[i][j+1]) / 4) * panel[i+1][j+1];
            K10 = ((a[i-1][j] + a[i][j]) /2 ) * panel[i-1][j];
            K11 = (a[i-1][j]) + (2 * a[i][j]) + (a[i+1][j]) + (c[i-1][j]) + (2* c[i][j]) + (c[i+1][j]);
            K11 = (K11 /2.0f)*(-1)*panel[i][j];
            K12 = ((a[i+1][j] + a[i][j]) / 2) * panel[i+1][j];
            K20 = ((b[i-1][j] + b[i][j-1]) /4) * panel[i-1][j-1];
            K21 = ((c[i][j-1] + c[i][j]) / 2) * panel[i][j-1];
            K22 = (-1) * ((b[i+1][j] + b[i][j-1]) /4) * panel[i+1][j-1];
            data[i][j] = panel[i][j] + (dt * (K00 + K01 + K02 +
                                                     K10 + K11 + K12 +
                                                     K20 + K21 + K22));
        }
    }

	free2float(panel);
}

void cg(float** A, float* X, float* B, int row, int col,int cgiter){
	int i;
	float* R = (float*) malloc(row*sizeof(float));
	float* P = (float*) malloc(row*sizeof(float));
	float* APJ = (float*) malloc(row*sizeof(float));

	dot_vec(A,X,R,row,col);
	for(i=0;i<row;i++){
		R[i] = B[i] - R[i];
		P[i] = R[i];	
	}
	int iter;
	for(iter=0;iter<cgiter;iter++){
		float rjrj = vec_dot_vec(R,R,row);
		dot_vec(A, P,APJ,  row, col);
		float apjpj = vec_dot_vec(P,APJ,row);
		float alpha = rjrj/apjpj;
		for(i=0;i<row;i++){
			X[i]+=alpha*P[i];
			R[i]-=alpha*APJ[i];
		}
		float rjirji= vec_dot_vec(R,R,row);
		float beta = rjirji/rjrj;
		for(i=0;i<row;i++){
			P[i]=R[i]+ (beta*P[i]);
		}
	}
	free(R);
	free(P);
	free(APJ);
}

void dot_vec(float** A, float* X,float* B, int row, int col){
	float val=0;
	int i,j;
	for(i=0;i<row;i++){
		val=0;
		for(j=0;j<col;j++){
			val+=(A[i][j]*X[j]);
		}
		B[i]=val;
	}
}

float vec_dot_vec(float* a,float* b,int row){
	int i;
	float sum=0;
	for(i=0;i<row;i++){
		sum+=(a[i]*b[i]);
	}
	return sum;
}

void osfmax(float* output,float* function, int flength, int klength){
	int span=(klength-1)/2;
	int plength = (flength+(2*span));
	float* panel = (float*) malloc( plength * sizeof(float));
	memcpy(&panel[span],&function[0],sizeof(float)*flength);
	int i;
	for(i=0;i<span;i++){panel[i]=function[0];}
	for(i=plength-span;i<plength;i++){panel[i]=function[flength-1];}
	int upperspan;
	int lowerspan;
	int indexConv;
	int cc;
	float* D = (float*) malloc(sizeof(float)*klength);
	for(i=span; i<(plength-span);i++){
		memset(D,'\0',klength);
		indexConv=0;
		upperspan=i-span;
		lowerspan=i+span;
		for(cc=upperspan; cc<=lowerspan; cc++){
			D[indexConv]=(panel[cc]);
			indexConv++;
		}
		qksort(klength,D);
		output[i-span]=D[klength-1];
	}
	free(D);
	free(panel);
}

void osfmin(float* output,float* function, int flength, int klength){
	int span=(klength-1)/2;
	int plength = (flength+(2*span));
	float* panel = (float*) malloc( plength * sizeof(float));
	memcpy(&panel[span],&function[0],sizeof(float)*flength);
	int i;
	for(i=0;i<span;i++){panel[i]=function[0];}
	for(i=plength-span;i<plength;i++){panel[i]=function[flength-1];}
	int upperspan;
	int lowerspan;
	int indexConv;
	int cc;
	float* D = (float*) malloc(sizeof(float)*klength);
	for(i=span; i<(plength-span);i++){
		memset(D,'\0',klength);
		indexConv=0;
		upperspan=i-span;
		lowerspan=i+span;
		for(cc=upperspan; cc<=lowerspan; cc++){
			D[indexConv]=(panel[cc]);
			indexConv++;
		}
		qksort(klength,D);
		output[i-span]=D[0];
	}
	free(D);
	free(panel);
}

void normalizedmaxd(double* data,int row){
	double norm = Maxd(data,row);
	int i;
	for(i=0;i<row;i++){
		data[i]/=norm;
	}
}

void normalizedmaxf(float* data,int row){
	float norm = Maxf(data,row);
	int i;
	for(i=0;i<row;i++){
		data[i]/=norm;
	}
}

void getsubmatrix(double** data, double** window, int ccol, int crow, int collspan,int rowspan){
	int i,j;
	int ndex=0,tdex=0;
	for(i=ccol-collspan;i<=ccol+collspan;i++){
		tdex=0;
		for(j=crow-rowspan;j<=crow+rowspan;j++){
			window[tdex][ndex]=data[j][i];
			tdex++;
		}
		ndex++;
	}
}

void setsubmatrix(double** data, double** window, int ccol, int crow, int collspan,int rowspan){
	int i,j;
	int ndex=0,tdex=0;
	for(i=ccol-collspan;i<=ccol+collspan;i++){
		tdex=0;
		for(j=crow-rowspan;j<=crow+rowspan;j++){
			data[j][i]=window[tdex][ndex];
			tdex++;
		}
		ndex++;
	}
}

void setsubmatrixuse(double** data, double** window, int ccol, int crow, 
					int colspan,int coluse,int rowspan, int rowuse){
	int i,j;
	int rowdex=(rowspan-rowuse);
	int coldex=(colspan-coluse);
	for(i=ccol-coluse;i<=ccol+coluse;i++){
		rowdex=(rowspan-rowuse);
		for(j=crow-rowuse;j<=crow+rowuse;j++){
			data[j][i]=window[rowdex][coldex];
			rowdex++;
		}
		coldex++;
	}
}

void addsubmatrix(double** data, double** window, int ccol, int crow, int colspan,int rowspan){
	int i,j;
	int ndex=0,tdex=0;
	for(i=ccol-colspan;i<=ccol+colspan;i++){
		tdex=0;
		for(j=crow-rowspan;j<=crow+rowspan;j++){
			data[j][i]+=window[tdex][ndex];
			tdex++;
		}
		ndex++;
	}
}

void mask(double** data,double** mask, int row,int col){
	int i,j;
	for(i=0;i<row;i++)
		for(j=0;j<col;j++)data[i][j]*=mask[i][j];
}

void get1dsubmatrix(int* dest, int* source,int beg, int end){
	int i,dex=0;
	for(i=beg;i<=end;i++){
		dest[dex]=source[i];
		dex++;
	}
}

int Max(int* Array, int array_size){
    int max = Array[0];
	int i;
    for (i = 1; i < array_size; i++)
    {
        if (Array[i] > max)
            max = Array[i];
    }
    return max;
}

int Min(int* Array, int array_size){
    int min = Array[0];
	int i;
    for (i = 1; i < array_size; i++)
    {
        if (Array[i] < min){
            min = Array[i];
		}
    }
    return min;
}

int Mindex(int* Array, int array_size){
    int min = Array[0];
	int mindex=0;
	int i;
    for (i = 1; i < array_size; i++)
    {
        if (Array[i] < min){
            min = Array[i];
			mindex=i;
		}
    }
    return mindex;
}

int Maxdex(int* Array, int array_size){
    int max = Array[0];
	int maxdex=0;
	int i;
    for (i = 1; i < array_size; i++)
    {
        if (Array[i] > max){
            max = Array[i];
			maxdex=i;
		}
    }
    return maxdex;
}

double Max2dd(double** Array, int row,int col){
	double max = Array[0][0];
	int i,j;
    for (i = 0; i < row; i++){
		for (j = 0; j < col; j++){
        	if (Array[i][j] > max)
        	    max = Array[i][j];
		}
    }
    return max;
}

double Min2dd(double** Array, int row,int col){
	double min = Array[0][0];
	int i,j;
    for (i = 0; i < row; i++){
		for (j = 0; j < col; j++){
        	if (Array[i][j] < min)
        	    min = Array[i][j];
		}
    }
    return min;
}

void normtrace(float* data, int ns){
	double std=0;
	double mean=0;
	int t;
	for(t=0;t<ns;t++){
		mean+=data[t];
	}
	mean/=(float)ns;

	for(t=0;t<ns;t++){
		std+=(data[t]-mean)*(data[t]-mean);
	}
	std/=ns;
	std=sqrt(std);

	for(t=0;t<ns;t++){
		data[t]=(data[t]-mean)/std;
	}
}

void normtraced(double* data, int ns){
	double std=0;
	double mean=0;
	int t;
	for(t=0;t<ns;t++){
		mean+=data[t];
	}
	mean/=(double)ns;

	for(t=0;t<ns;t++){
		std+=(data[t]-mean)*(data[t]-mean);
	}
	std/=ns;
	std=sqrt(std);

	for(t=0;t<ns;t++){
		data[t]=(data[t]-mean)/std;
	}
}

void strecth(double** data, int row, int col, double nMax,double nMin){
	double max =  Max2dd(data,  row, col);
	double min =  Min2dd(data,  row, col);
	double coeff = (nMax-nMin)/ (max-min);
	int i,j;
	for(i=0;i<row;i++)
		for(j=0;j<col;j++)
			data[i][j] = ( (data[i][j] - min) * coeff ) + nMin;
}

void matmul(double** A, double** X, double** AX, int rowa, int cola,int rowx,int colx){
	if(cola!=rowx){exit(EXIT_FAILURE);}	
	int i,j;
	int d;

	for(i=0;i<rowa;i++){
		for(j=0;j<colx;j++){
			AX[i][j]=0;
			for(d=0;d<cola;d++){
				AX[i][j]+=A[i][d]*X[d][j];			
			}
		}
	}
}

void copy2d(double** out,double** in, int row,int col){
	int i,j;
	for(i=0;i<row;i++)
		for(j=0;j<col;j++)
			out[i][j]=in[i][j];

}

int* downsampling1di(int* data, int col){
	int* output = (int*) calloc(col/2,sizeof(int));
	int i;
	for(i=0;i<col/2;i++)
		output[i]=data[i*2];
	return output;
}

double** downsamplingtraces(double** data, int row,int col){
	double** output = (double**) ealloc2double(col,row/2);

	int i;
	for(i=0;i<row/2;i++)
		memcpy(output[i],data[i*2],col*sizeof(double));
	return output;
}
