/*
 * svdlapack.c
 *
 *  Created on: Jul 19, 2011
 *      Author: toto
 */
#include "svdlapack.h"

void svdlapackd(double **data, int irow, int icol,
		double **U, double *S, double **VT)
{
	/* Locals  */
	int info, lwork;

	double *tmpa, *tmpu, *tmpv;
	double wkopt;
	double* work;
	int i, j, ioff;

	tmpa = alloc1double(irow*icol);
	tmpu = alloc1double(irow*irow);
	tmpv = alloc1double(icol*icol);

	ioff = 0;
	for(i=0; i<icol; i++)
	{
		for(j=0; j<irow; j++)
		{
			tmpa[ioff] = data[j][i];
			ioff = ioff+1;
		}
	}

	/* Query and allocate the optimal workspace */
	lwork = -1;
	dgesvd_( "All", "All", &irow, &icol, tmpa, &irow, S, tmpu, &irow, tmpv, &icol, &wkopt, &lwork,
			&info );

	lwork = (int)wkopt;
	work = (double*)malloc( lwork*sizeof(double) );

	/* Compute SVD */
	dgesvd_( "All", "All", &irow, &icol, tmpa, &irow, S, tmpu, &irow, tmpv, &icol, work, &lwork,
			&info );

	/* Check for convergence */
	if( info > 0 ) {
		printf( "The algorithm computing SVD failed to converge.\n" );
		exit( 1 );
	}

	/* copy from tmpu to u */
	ioff = 0;
	for(i=0; i<irow; i++)
	{
		for(j=0; j<irow; j++)
		{
			U[j][i] = tmpu[ioff];
			ioff = ioff+1;
		}
	}

	ioff = 0;
	for(i=0; i<icol; i++)
	{
		for(j=0; j<icol; j++)
		{
			VT[j][i] = tmpv[ioff];		//V transpose
			ioff = ioff+1;
		}
	}

	free( (void*)work );
	free1double(tmpa);
	free1double(tmpu);
	free1double(tmpv);
}

