/*
 * tesWindowing.h
 *
 *  Created on: Jun 21, 2012
 *      Author: toto
 */

#ifndef TESWINDOWING_H_
#define TESWINDOWING_H_
#ifdef SEISUNIX
#include "segy_lib.h"
#else
#include <cpromax.h>
#include <stdbool.h>
#endif

int nWindowingCount(int beg_pos, int end_pos, int spansize, int spanfilter);
void printWindowingPosArray(int **posN, int ndata);
void fillWindowingArray(int **posN, int nwindow,
		int beg_pos, int end_pos, int spansize, int spanfilter);
int **createWindowingArray(int beg_pos, int end_pos,
		int spansize, int spanfilter, int *iwindow);
bool getpos1pos2(int nsp, int icenter, int nspan, int nsfilt,
		int lastspan1, int lastifilt1,
		int *sp1, int *sp2, int *ss1, int *ss2);
void addDatai(int **data, int posX1, int posX2, int posY1, int posY2);
void addDataf(float **data, float **procData,
		int posX1, int posX2, int posY1, int posY2);
void addDataf1(float **A, int a1, int a2,
		float **B, int b11, int b12, int b21, int b22);
float **getData(float **data, int ntrc, int nsp,
		int posX1, int posX2, int posY1, int posY2,
		int *nx, int *ny);
#endif /* TESWINDOWING_H_ */
