/*
 * swt.c
 *
 *  Created on: Dec 14, 2012
 *      Author: toto
 */


#include "swt.h"

float *run_swt_v1(float *trace, int nt,
		int _filter_id, int _level_scale, float _treshold, int _ntreshold,
		int _leakage1, int _leakage2, int _inv_level/*, int *newNt*/)
{
	float *db;
	int ndb, wndb, indb;
	float *Lo_D, *Hi_D, *Lo_R, *Hi_R;
	float **LoD, **HiD, **LoR, **HiR;
	float **swa, **swd, **swdth, **swdcfs;
	float *result;
	int p;

	float *ca;
	int Nca; //new length of trace
	int l;
	float fpower;

	db = get_daubechies(_filter_id, &ndb);

	p = 0;
	wndb = (int) (powf(ndb, _level_scale+1));
	fpower = powf(2.0, _level_scale);
	l=(int) (ceil(nt/fpower) * fpower - nt);
	Nca = nt + l;

	//allocate array
	result = alloc1float(Nca);
	Lo_D = alloc1float(ndb);
	Hi_D = alloc1float(ndb);
	Lo_R = alloc1float(ndb);
	Hi_R = alloc1float(ndb);
	ca = alloc1float(Nca);

	LoD = alloc2float(wndb, _level_scale+1);
	HiD = alloc2float(wndb, _level_scale+1);
	LoR = alloc2float(wndb, _level_scale+1);
	HiR = alloc2float(wndb, _level_scale+1);
	swa = alloc2float(Nca, _level_scale);
	swd = alloc2float(Nca, _level_scale);
	swdth = alloc2float(Nca, _level_scale);
	swdcfs = alloc2float(Nca, _level_scale);

	//set memory to NULL
	memset(result, 0, Nca*sizeof(float));
	memset(Lo_D, 0, ndb*sizeof(float));
	memset(Hi_D, 0, ndb*sizeof(float));
	memset(Lo_R, 0, ndb*sizeof(float));
	memset(Hi_R, 0, ndb*sizeof(float));
	memset(ca, 0, Nca*sizeof(float));

	memset(LoD[0], 0, wndb*(_level_scale+1)*sizeof(float));
	memset(HiD[0], 0, wndb*(_level_scale+1)*sizeof(float));
	memset(LoR[0], 0, wndb*(_level_scale+1)*sizeof(float));
	memset(HiR[0], 0, wndb*(_level_scale+1)*sizeof(float));
	memset(swa[0], 0, Nca*(_level_scale)*sizeof(float));
	memset(swd[0], 0, Nca*(_level_scale)*sizeof(float));
	memset(swdth[0], 0, Nca*(_level_scale)*sizeof(float));
	memset(swdcfs[0], 0, Nca*(_level_scale)*sizeof(float));

	orthfilt_f(db, ndb, p, Lo_D, Hi_D, Lo_R, Hi_R);

//	if((_ntreshold+1)>=_level_scale){
//		fprintf(stderr, "threshold_data : ntreshold[%i] > (_level_scale-1) [%i] \n",
//				_ntreshold, _level_scale-1);
//		exit(0);
//	}

	//copy data
	indb = (int) (powf(ndb, 1));
	memcpy(LoD[0], Lo_D, ndb*sizeof(float));
	memcpy(HiD[0], Hi_D, ndb*sizeof(float));
	memcpy(LoR[0], Lo_R, ndb*sizeof(float));
	memcpy(HiR[0], Hi_R, ndb*sizeof(float));
	memcpy(ca, trace, nt*sizeof(float));

	forward_swt(ca, Nca, _level_scale, ndb, LoD, HiD, LoR, HiR, swa, swd);
	threshold_data(swd, _level_scale, Nca, _treshold, _ntreshold, swdth);

	inverse_swt_coeff(swd, swa, swdth, LoR, HiR,
			_level_scale, Nca, _leakage1, _leakage2, ndb, swdcfs);
	inverse_swt(swa, swdcfs, LoR, HiR,
			_level_scale, Nca, _inv_level, ndb, result);

	//	(*newNt) = Nca;
	free1float(db);
	free1float(Lo_D);
	free1float(Hi_D);
	free1float(Lo_R);
	free1float(Hi_R);
	free1float(ca);

	free2float(LoD);
	free2float(HiD);
	free2float(LoR);
	free2float(HiR);
	free2float(swa);
	free2float(swd);
	free2float(swdth);
	free2float(swdcfs);
	return(result);
}

float *run_swt_v2(float *trace, int nt,
		int _filter_id, int _level_scale, float _treshold,
		int _treshold_start, int _ntreshold,
		int _inv_level/*, int *newNt*/)
{
	float *db;
	int ndb, wndb, indb;
	float *Lo_D, *Hi_D, *Lo_R, *Hi_R;
	float **LoD, **HiD, **LoR, **HiR;
	float **swa, **swd, **swdth;
	float *result;
	int p;

	float *ca;
	int Nca; //new length of trace
	int l;
	float fpower;

	db = get_daubechies(_filter_id, &ndb);

	p = 0;
	wndb = (int) (powf(ndb, _level_scale+1));
	fpower = powf(2.0, _level_scale);
	l=(int) (ceil(nt/fpower) * fpower - nt);
	Nca = nt + l;

	//allocate array
	result = alloc1float(Nca);
	Lo_D = alloc1float(ndb);
	Hi_D = alloc1float(ndb);
	Lo_R = alloc1float(ndb);
	Hi_R = alloc1float(ndb);
	ca = alloc1float(Nca);

	LoD = alloc2float(wndb, _level_scale+1);
	HiD = alloc2float(wndb, _level_scale+1);
	LoR = alloc2float(wndb, _level_scale+1);
	HiR = alloc2float(wndb, _level_scale+1);
	swa = alloc2float(Nca, _level_scale);
	swd = alloc2float(Nca, _level_scale);
	swdth = alloc2float(Nca, _level_scale);

	//set memory to NULL
	memset(result, 0, Nca*sizeof(float));
	memset(Lo_D, 0, ndb*sizeof(float));
	memset(Hi_D, 0, ndb*sizeof(float));
	memset(Lo_R, 0, ndb*sizeof(float));
	memset(Hi_R, 0, ndb*sizeof(float));
	memset(ca, 0, Nca*sizeof(float));

	memset(LoD[0], 0, wndb*(_level_scale+1)*sizeof(float));
	memset(HiD[0], 0, wndb*(_level_scale+1)*sizeof(float));
	memset(LoR[0], 0, wndb*(_level_scale+1)*sizeof(float));
	memset(HiR[0], 0, wndb*(_level_scale+1)*sizeof(float));
	memset(swa[0], 0, Nca*(_level_scale)*sizeof(float));
	memset(swd[0], 0, Nca*(_level_scale)*sizeof(float));
	memset(swdth[0], 0, Nca*(_level_scale)*sizeof(float));

	orthfilt_f(db, ndb, p, Lo_D, Hi_D, Lo_R, Hi_R);

//	if((_ntreshold+1)>=_level_scale){
//		fprintf(stderr, "threshold_data : ntreshold[%i] > (_level_scale) [%i] \n",
//				_ntreshold, _level_scale-1);
//		exit(0);
//	}

	//copy data
	indb = (int) (powf(ndb, 1));
	memcpy(LoD[0], Lo_D, ndb*sizeof(float));
	memcpy(HiD[0], Hi_D, ndb*sizeof(float));
	memcpy(LoR[0], Lo_R, ndb*sizeof(float));
	memcpy(HiR[0], Hi_R, ndb*sizeof(float));
	memcpy(ca, trace, nt*sizeof(float));

	forward_swt(ca, Nca, _level_scale, ndb, LoD, HiD, LoR, HiR, swa, swd);
	threshold_data_v2(swd, _level_scale, Nca, _treshold, _treshold_start,
			_ntreshold, swdth);

	inverse_swt(swa, swdth, LoR, HiR,
			_level_scale, Nca, _inv_level, ndb, result);

	//	(*newNt) = Nca;
	free1float(db);
	free1float(Lo_D);
	free1float(Hi_D);
	free1float(Lo_R);
	free1float(Hi_R);
	free1float(ca);

	free2float(LoD);
	free2float(HiD);
	free2float(LoR);
	free2float(HiR);
	free2float(swa);
	free2float(swd);
	free2float(swdth);
//	free2float(swdcfs);
	return(result);
}

void forward_swt(float *ca, int Nca, int _level_scale, int ndb,
		float **LoD, float **HiD, float **LoR, float **HiR,
		float **swa, float **swd)
{
	int i, j;
	int indb;
	int mk, lk, dcc;
	float *Lo_D, *Hi_D, *Lo_R, *Hi_R;
	float *cd;
	float *ccd, *cca;
	int Ncc, nu;

	cd = alloc1float(Nca);
	for (i=0; i<_level_scale; i++)
	{
		indb = (int) (powf(ndb, i+1));
		mk = (int) ((indb-2)/2.0);
		lk = (int) (powf(2.0, i));

		//allocate array
		Lo_D = alloc1float(indb);
		Hi_D = alloc1float(indb);
		Lo_R = alloc1float(indb);
		Hi_R = alloc1float(indb);
		memcpy(Lo_D, LoD[i], indb*sizeof(float));
		memcpy(Hi_D, HiD[i], indb*sizeof(float));
		memcpy(Lo_R, LoR[i], indb*sizeof(float));
		memcpy(Hi_R, HiR[i], indb*sizeof(float));

		ccd = convf(ca, Hi_D, Nca, indb, &Ncc);
		cca = convf(ca, Lo_D, Nca, indb, &Ncc);
		dcc = Ncc-mk-1;
		for(j=0; j<lk; j++)
		{
			cca[dcc-j] = cca[dcc-j] + cca[lk-j-1];
			ccd[dcc-j] = ccd[dcc-j] + ccd[lk-j-1];
		}

		memcpy(ca, cca+lk, Nca*sizeof(float));
		memcpy(cd, ccd+lk, Nca*sizeof(float));
		memcpy(swa[i], ca, Nca*sizeof(float));
		memcpy(swd[i], cd, Nca*sizeof(float));

		upsample_f2(Lo_R, indb, 2, LoR[i+1], &nu);
		upsample_f2(Hi_R, indb, 2, HiR[i+1], &nu);
		upsample_f2(Lo_D, indb, 2, LoD[i+1], &nu);
		upsample_f2(Hi_D, indb, 2, HiD[i+1], &nu);

		free1float(Lo_D);
		free1float(Hi_D);
		free1float(Lo_R);
		free1float(Hi_R);
		free1float(ccd);
		free1float(cca);
	}
	free1float(cd);
}

void threshold_data(float **swd, int nrow, int ncol,
		float _treshold, int _ntreshold, float **swdth)
{
	int i, j;
	float thr;
	float mmax;

	memcpy(swdth[0], swd[0], nrow*ncol*sizeof(float));
	for (i=0; i<_ntreshold; i++)
	{
		//get maximum absolute data
		mmax = ABS(swd[i][0]);
		for(j=0; j<ncol; j++)
			if(mmax<ABS(swd[i][j]))
				mmax = ABS(swd[i][j]);

		//compute treshold data
		thr = _treshold*mmax;
		for(j=0; j<ncol; j++)
		{
			if(ABS(swd[i+1][j]) <= thr)
				swdth[i+1][j] = 0.0;
		}
	}
}


void threshold_data_v2(float **swd, int nrow, int ncol,
		float _treshold, int _treshold_start, int _ntreshold, float **swdth)
{
	int i, j;
	float thr;
	float mmax;

	memcpy(swdth[0], swd[0], nrow*ncol*sizeof(float));
	for (i=_treshold_start-1; i<_ntreshold; i++)
	{
		//get maximum absolute data
		mmax = ABS(swd[i][0]);
		for(j=0; j<ncol; j++)
			if(mmax<ABS(swd[i][j]))
				mmax = ABS(swd[i][j]);

		//compute treshold data
		thr = _treshold*mmax;
		for(j=0; j<ncol; j++)
		{
			if(ABS(swd[i+1][j]) <= thr)
				swdth[i+1][j] = 0.0;
		}
	}
}

//threshold_real for leakswt
void threshold_data_v3(float **swd, int nrow, int ncol,
		float _treshold, int _ntreshold, float **swdth)
{
	int i, j;
	float thr;
	float mmax;

	memcpy(swdth[0], swd[0], nrow*ncol*sizeof(float));
	for (i=_ntreshold-1; i>=1; i--)
	{
		//get maximum absolute data
		mmax = ABS(swd[i-1][0]);
		for(j=0; j<ncol; j++)
			if(mmax<ABS(swd[i-1][j]))
				mmax = ABS(swd[i-1][j]);

		//compute treshold data
		thr = _treshold*mmax;
		for(j=0; j<ncol; j++)
		{
			if(ABS(swd[i][j]) <= thr)
				swdth[i][j] = 0.0;
		}
	}
}


void inverse_swt_coeff(float **swd, float **swa, float **swdth,
		float **LoR, float **HiR,
		int nrow, int ncol,
		int leakage1, int leakage2, int ndb,
		float **swdcfs)
{
	int i, j;
	int indb;
	float *Hi_R;
	float *tmp_swdcfs, *tmp_swa;
	float *id, *ia;
	int Niad, mk, lk, diad;

	memcpy(swdcfs[0], swd[0], nrow*ncol*sizeof(float));
	tmp_swdcfs = alloc1float(ncol);
	tmp_swa = alloc1float(ncol);
	for(i=leakage2-1; i>=leakage1-1; i--)
	{
		indb = (int) (powf(ndb, i+1));
		Hi_R = alloc1float(indb);
		memcpy(Hi_R, HiR[i], indb*sizeof(float));
		memcpy(tmp_swdcfs, swdcfs[i], ncol*sizeof(float));
		memcpy(tmp_swa, swa[i], ncol*sizeof(float));

		id = convf(tmp_swdcfs, Hi_R, ncol, indb, &Niad);
		ia = convf(tmp_swa, Hi_R, ncol, indb, &Niad);
		mk = (int)((indb-2)/2.0);
		lk = (int) (pow(2.0, i));
		diad = Niad-mk-lk;

		for(j=0; j<lk; j++)
		{
			id[j] = id[diad+j] + id[j];
			ia[j] = ia[diad+j] + ia[j];
		}

		for (j=0; j<diad; j++)
			if(swdth[i-1][j]==0.0)
				swdcfs[i-1][j] = swd[i-1][j] + id[j];

		free1float(Hi_R);
		free1float(id);
		free1float(ia);
	}
	free1float(tmp_swdcfs);
	free1float(tmp_swa);
}

//inverse_swt_coeff for leakswt
void inverse_swt_coeff_leakswt(float **swd, float **swa, float **swdth,
		float **LoD, float **HiD,
		int nrow, int ncol,
		int leakage1, int leakage2, int ndb,
		float **swdcfs, float _scale)
{
	int i, j;
	int indb;
	float *Lo_D;
	float *tmp_swdcfs, *tmp_swa;
	float *id, *ia;
	int Niad, mk, lk, diad;
	int dcc;

	memcpy(swdcfs[0], swd[0], nrow*ncol*sizeof(float));
	tmp_swdcfs = alloc1float(ncol);
	tmp_swa = alloc1float(ncol);
	for(i=leakage1-1; i<=leakage2-2; i++)
	{
		indb = (int) (powf(ndb, i+1));
		Lo_D = alloc1float(indb);
		memcpy(Lo_D, LoD[i], indb*sizeof(float));
		memcpy(tmp_swdcfs, swdcfs[i], ncol*sizeof(float));
		memcpy(tmp_swa, swa[i], ncol*sizeof(float));

		id = convf(tmp_swdcfs, Lo_D, ncol, indb, &Niad);
		ia = convf(tmp_swa, Lo_D, ncol, indb, &Niad);
		mk = (int)((indb-2)/2.0);
		lk = (int) (pow(2.0, i));
		diad = Niad-mk-lk;

//		for(j=0; j<lk; j++)
//		{
//			id[j] = id[diad+j] + id[j];
//			ia[j] = ia[diad+j] + ia[j];
//		}
		dcc = Niad-mk-1;
		for(j=0; j<lk; j++)
		{
			ia[dcc-j] = ia[dcc-j] + ia[lk-j-1];
			id[dcc-j] = id[dcc-j] + id[lk-j-1];
		}

//		memcpy(ia, cca+lk, Nca*sizeof(float));
//		memcpy(id, ccd+lk, Nca*sizeof(float));
		for(j=0; j<Niad; j++)
		{
			ia[j] = ia[j+lk];
			id[j] = id[j+lk];
		}

		for (j=0; j<diad; j++)
			if(swdth[i+1][j]==0.0)
				swdcfs[i+1][j] = swd[i+1][j] + _scale*id[j];

		free1float(Lo_D);
		free1float(id);
		free1float(ia);
	}
	free1float(tmp_swdcfs);
	free1float(tmp_swa);
}

void inverse_swt_leakswt(float **swa, float **swd, float **swdcfs, float **LoR, float **HiR,
		int nrow, int ncol, int _inv_level, int ndb, float *trace, int leakage1)
{
	float *ca;
	float *Lo_R, *Hi_R;
	float *tmp_swdcfs, *ia, *id;
	int i, j;
	int indb, Niad, mk, lk, diad;

	ca = alloc1float(ncol);
	tmp_swdcfs = alloc1float(ncol);
	memcpy(ca, swa[_inv_level-1], ncol*sizeof(float));

	for(i=0; i<leakage1; i++)
		memcpy(swdcfs[i], swd[i], ncol*sizeof(float));

	for(i=_inv_level-1; i>=0; i--)
	{
		indb = (int) (powf(ndb, i+1));
		Lo_R = alloc1float(indb);
		Hi_R = alloc1float(indb);
		memcpy(Lo_R, LoR[i], indb*sizeof(float));
		memcpy(Hi_R, HiR[i], indb*sizeof(float));
		memcpy(tmp_swdcfs, swdcfs[i], ncol*sizeof(float));

		id = convf(tmp_swdcfs, Hi_R, ncol, indb, &Niad);
		ia = convf(ca, Lo_R, ncol, indb, &Niad);

		mk = (int)((indb-2)/2.0);
		lk = (int) (pow(2.0, i));
		diad = Niad-mk-lk;

		for(j=0; j<lk; j++)
		{
			id[j] = id[diad+j] + id[j];
			ia[j] = ia[diad+j] + ia[j];
		}

		for(j=0; j<diad; j++)
			ca[j] = (id[j] + ia[j]) / 2.0;

		free1float(Lo_R);
		free1float(Hi_R);
		free1float(id);
		free1float(ia);
	}

	memcpy(trace, ca, ncol*sizeof(float));
	free1float(tmp_swdcfs);
	free1float(ca);
}


void inverse_swt(float **swa, float **swdcfs, float **LoR, float **HiR,
		int nrow, int ncol, int _inv_level, int ndb, float *trace)
{
	float *ca;
	float *Lo_R, *Hi_R;
	float *tmp_swdcfs, *ia, *id;
	int i, j;
	int indb, Niad, mk, lk, diad;

	ca = alloc1float(ncol);
	tmp_swdcfs = alloc1float(ncol);
	memcpy(ca, swa[_inv_level-1], ncol*sizeof(float));

	for(i=_inv_level-1; i>=0; i--)
	{
		indb = (int) (powf(ndb, i+1));
		Lo_R = alloc1float(indb);
		Hi_R = alloc1float(indb);
		memcpy(Lo_R, LoR[i], indb*sizeof(float));
		memcpy(Hi_R, HiR[i], indb*sizeof(float));
		memcpy(tmp_swdcfs, swdcfs[i], ncol*sizeof(float));

		id = convf(tmp_swdcfs, Hi_R, ncol, indb, &Niad);
		ia = convf(ca, Lo_R, ncol, indb, &Niad);

		mk = (int)((indb-2)/2.0);
		lk = (int) (pow(2.0, i));
		diad = Niad-mk-lk;

		for(j=0; j<lk; j++)
		{
			id[j] = id[diad+j] + id[j];
			ia[j] = ia[diad+j] + ia[j];
		}

		for(j=0; j<diad; j++)
			ca[j] = (id[j] + ia[j]) / 2.0;

		free1float(Lo_R);
		free1float(Hi_R);
		free1float(id);
		free1float(ia);
	}

	memcpy(trace, ca, ncol*sizeof(float));
	free1float(tmp_swdcfs);
	free1float(ca);
}
