/*
 * printLib.h
 *
 *  Created on: Apr 13, 2012
 *      Author: toto
 */

#ifndef PRINTLIB_H_
#define PRINTLIB_H_

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

void print1int(int *data, int ndata, bool ptype, bool showlineNumber, bool dispType);
void print2int(int **data, int nrow, int ncol, bool ptype, bool showlineNumber);
void print1float(float *data, int ndata, bool ptype, bool showlineNumber, bool dispType);
void print2float(float **data, int nrow, int ncol, bool ptype, bool showlineNumber);
void print2double(double **data, int nrow, int ncol, bool ptype, bool showlineNumber);

void print1f(char *cname, float *X, int nx);
void print1i(char *cname, int *X, int nx);

void print2f(char *cname, float **X, int nx, int ny);
#endif /* PRINTLIB_H_ */
