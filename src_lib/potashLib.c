/*
 * potashLib.c
 *
 *  Created on: Sep 26, 2012
 *      Author: toto
 */
#include "potashLib.h"

void computeRLag(float **firstdata, float **data, int ntrc, int nsp, int *rlag,
		int idxGather, int ishfm, int stmin, int stmax)
{
	int lag;		/* time shift in smaples between traces */
	int i, j, idx;
	int ntf, ntx, tlag, lcnt;
	float *xtr;
	float **tfirstdata, **tdata;

	ntf = stmax-stmin;
//	ntf = nsp;
	ntx = ntf-2;
	if(ntx%2==0) ntx++;
	xtr = alloc1float(ntx);
	tfirstdata = alloc2float(ntf, ntrc);
	tdata = alloc2float(ntf, ntrc);
	for(i=0; i<ntrc; i++)
	{
		idx = 0;
		for(j=stmin; j<stmax; j++)
		{
			tfirstdata[i][idx] = firstdata[i][j];
			tdata[i][idx] = data[i][j];
			idx++;
		}
	}

	lag=0;
	lcnt=0;
	for(i=0; i<ntrc; i++)
	{
		/* do the corss correalation */
		do_facor_shstk(tdata[i], tfirstdata[i], xtr, ntf, -ntx/2, ntx);

		/* pick the maximum */
		tlag = isamaxs(ntx,xtr,1)-ntx/2;
		if(abs(tlag) < ishfm)
		{
			lag += tlag;
			lcnt++;
		}
	}

	/* averaged lag of the traces for this record */
	if(lcnt==0) lag=0;
	else lag = (int)(lag/lcnt);
	rlag[idxGather] = lag;

	free1float(xtr);
	free2float(tdata);
	free2float(tfirstdata);
}

/* Fast crosscorelation with FFT */
void do_facor_sulib(float *data1,float *data2, float *acor,int n,int f,int nc)
/* Inputs data1, data2 - arrays to be xcorrelated
          n            - dimension of data1 and data2
	  acor         - xcorrelation array
	  nc           - size of acor
	  f            - first element of acor
	  		f=0            - asymmetric output
	  		f=-nc/2        - symmetric output
*/

{

	int nfft;
	int nf;
	int i,j;
	float snfft;
	complex *fft1;
	complex *fft2;
	float *pdata1;
	float *pdata2;
	int fst,flt;
	int ast;

	/* Set up pfa fft */
	nfft = npfaro(n,LOOKFAC*n);
	if (nfft >= SU_NFLTS || nfft >= PFA_MAX)
	{
		fprintf(stderr, "Padded nt=%d--too big", nfft);
		exit(0);
	}
	snfft=1.0/nfft;
	nf=nfft/2+1;


	fft1 = alloc1complex(nf);
	fft2 = alloc1complex(nf);
	pdata1 = alloc1float(nfft);
	pdata2 = alloc1float(nfft);

	memcpy( (void *) pdata1, (const void *) data1, n*FSIZE);
	memset( (void *) &pdata1[n],  (int) '\0', (nfft-n)*FSIZE);
	memcpy( (void *) pdata2, (const void *) data2, n*FSIZE);
	memset( (void *) &pdata2[n],  (int) '\0', (nfft-n)*FSIZE);

	pfarc(1,nfft,pdata1,fft1);
	pfarc(1,nfft,pdata2,fft2);
	for(i=0;i<nf;i++)
		fft1[i] = cmul(fft1[i],conjg(fft2[i]));

	pfacr(-1,nfft,fft1,pdata1);
	sscal(nfft,snfft,pdata1,1);

	for(i=0,j=nf-1;i<nf;i++,j--)
		pdata2[i]=pdata1[j];

	for(i=nf,j=nfft-1;i<nfft;i++,j--)
		pdata2[i]=pdata1[j];

	fst=MIN(MAX(nf+f,0),nfft-1);
	flt=MIN(fst+nc,nfft-1);
	ast=MIN(0,nf+f-1);


	for(i=fst,j=abs(ast);i<flt;i++,j++)
		acor[j]=pdata2[i];

	free1complex(fft1);
	free1complex(fft2);
	free1float(pdata1);
	free1float(pdata2);
}

/* Fast crosscorelation with FFT */
void do_facor_shstk(float *data1,float *data2, float *acor,int n,int f,int nc)
{
	int nfft;
	int nf;
	int i,j;
	float snfft;
	complex *fft1;
	complex *fft2;
	float *pdata1;
	float *pdata2;


	/* Set up pfa fft */
	nfft = npfaro(n,LOOKFAC*n);
	if (nfft >= SU_NFLTS || nfft >= PFA_MAX)
	{
		fprintf(stderr, "Padded nt=%d--too big", nfft);
		exit(0);
	}

	snfft=1.0/nfft;
	nf=nfft/2+1;
	if(abs(f) > nc )
	{
		fprintf(stderr, " First element of correlation is out of bound\n");
		exit(0);
	}
	if(nc>n)
	{
		fprintf(stderr, " Length of corr is larger than the length of input \n");
		exit(0);
	}


	fft1 = alloc1complex(nfft);
	fft2 = alloc1complex(nfft);
	pdata1 = alloc1float(nfft);
	pdata2 = alloc1float(nfft);

	memcpy( (void *) pdata1, (const void *) data1, n*FSIZE);
	memset( (void *) &pdata1[n],  (int) '\0', (nfft-n)*FSIZE);
	memcpy( (void *) pdata2, (const void *) data2, n*FSIZE);
	memset( (void *) &pdata2[n],  (int) '\0', (nfft-n)*FSIZE);

	pfarc(1,nfft,pdata1,fft1);
	pfarc(1,nfft,pdata2,fft2);
	for(i=0;i<nf;i++) fft1[i] = cmul(fft1[i],conjg(fft2[i]));

	pfacr(-1,nfft,fft1,pdata1);
	sscal(n,snfft,pdata1,1);

	if (f < 0)
	{
		for(i=abs(f),j=0;i>=MAX(0,-f-nc);i--,j++)
			acor[j] = pdata1[i];
		for(i=0;j<nc;i++,j++)
			acor[j] = pdata1[i];
	}
	else
	{
		for(i=f,j=0;i<MIN(nc,n-f);i++,j++)
			acor[j] = pdata1[i];
	}

	free1complex(fft1);
	free1complex(fft2);
	free1float(pdata1);
	free1float(pdata2);
}

int isamaxs(int n, float *a,int inc)
{
	int im=0,i;
	for(i=1;i<n;i+=inc)
		if(a[im]< a[i]) im=i;
	return(im);
}

void runSvdComputation(float **traces, int ntr, int nt, float npp, int np, int check, int svditer)
{
	float tmp1, tmp2;
	int *ik;
	float **ttraces;
	float *s;
	float *scwp;
	float **v;
	int ix, j, i, k;
	bool ret;


	if(np==-9999)
	{
		if(npp>0)
			np = MAX((int)(npp*ntr/100.0), 1);
		else
			np = MIN((int)(npp*ntr/100.0),-1);
	}

	/* allocate space */
	ik = alloc1int(ntr);

	/* matrixes for the svd decomposition */
	ttraces = matrix(1,nt,1,ntr);
	s = vector(1,ntr);
	scwp = s+1;
	v = matrix(1,ntr,1,ntr);

	/* transpose traces matrix to ttraces */
	for(ix=0; ix<ntr; ix++)
	{
		for(j=0; j<nt; j++)
			ttraces[j+1][ix+1] = traces[ix][j];
		ik[ix]=ix;
	}

	/* do requested operation */
	ret = svdcmp(ttraces,nt,ntr,s,v, svditer);
	if(!ret) //not convergence, save original data
	{
		free1int(ik);
		free_matrix(ttraces, 1, nt, 1, ntr);
		free_vector(s, 1, ntr);
		free_matrix(v,1,ntr,1,ntr);
		return;
	}

	/* sort the singular values; index sort */
	qkisort(ntr,scwp,ik);

	if(check)
	{
		fprintf(stderr," %d",tr.fldr);
		for(i=0;i<np;i++)
			fprintf(stderr," %f",scwp[ik[ntr-1-i]]);
		fprintf(stderr," \n");
		exit(0);
	}

	/* if np is positive keep only the largest np values and zero the rest
	*/
	if(np>0)
	{
		for(i=ntr-np-1;i>=0;i--)
			scwp[ik[i]]=0.0;
	}
	else
	{
		/* if np is negative keep but the largest np values */
		for(i=ntr-1;i>ntr-1+np;i--)
			scwp[ik[i]]=0.0;
	}

	for(i=0;i<nt;i++)
	{
		for(j=0;j<ntr;j++)
		{
			tmp2=0.0;
			tmp1=0.0;
			for(k=0;k<ntr;k++)
			{
				tmp1 = ttraces[i+1][k+1]*s[k+1]*v[j+1][k+1];
				tmp2+=tmp1;
			}
			traces[j][i]=tmp2;
		}
	}

	free1int(ik);
	free_matrix(ttraces, 1, nt, 1, ntr);
	free_vector(s, 1, ntr);
	free_matrix(v,1,ntr,1,ntr);

}

bool svdcmp(float **a, int m, int n, float w[], float **v, int svditer)
{
	float pythag(float a, float b);
	int flag,i,its,j,jj,k,l=0,nm=0;
	float anorm,c,f,g,h,s,scale,x,y,z,*rv1;

	rv1=vector(1,n);
	g=scale=anorm=0.0;
	for (i=1;i<=n;i++)
	{
		l=i+1;
		rv1[i]=scale*g;
		g=s=scale=0.0;

		if (i <= m)
		{
			for (k=i;k<=m;k++)
				scale += fabs(a[k][i]);

			if (scale)
			{
				for (k=i;k<=m;k++)
				{
					a[k][i] /= scale;
					s += a[k][i]*a[k][i];
				}

				f=a[i][i];
				g = -SIGN(sqrt(s),f);
				h=f*g-s;
				a[i][i]=f-g;
				for (j=l;j<=n;j++)
				{
					for (s=0.0,k=i;k<=m;k++)
						s += a[k][i]*a[k][j];

					f=s/h;

					for (k=i;k<=m;k++)
						a[k][j] += f*a[k][i];
				}
				for (k=i;k<=m;k++)
					a[k][i] *= scale;
			}
		}

		w[i]=scale *g;
		g=s=scale=0.0;
		if (i <= m && i != n)
		{
			for (k=l;k<=n;k++)
				scale += fabs(a[i][k]);

			if (scale)
			{
				for (k=l;k<=n;k++)
				{
					a[i][k] /= scale;
					s += a[i][k]*a[i][k];
				}

				f=a[i][l];
				g = -SIGN(sqrt(s),f);
				h=f*g-s;
				a[i][l]=f-g;
				for (k=l;k<=n;k++)
					rv1[k]=a[i][k]/h;

				for (j=l;j<=m;j++)
				{
					for (s=0.0,k=l;k<=n;k++)
						s += a[j][k]*a[i][k];
					for (k=l;k<=n;k++)
						a[j][k] += s*rv1[k];
				}

				for (k=l;k<=n;k++)
					a[i][k] *= scale;
			}
		}
		anorm=FMAX(anorm,(fabs(w[i])+fabs(rv1[i])));
	}

	for (i=n;i>=1;i--)
	{
		if (i < n)
		{
			if (g)
			{
				for (j=l;j<=n;j++)
					v[j][i]=(a[i][j]/a[i][l])/g;
				for (j=l;j<=n;j++)
				{
					for (s=0.0,k=l;k<=n;k++)
						s += a[i][k]*v[k][j];
					for (k=l;k<=n;k++)
						v[k][j] += s*v[k][i];
				}
			}
			for (j=l;j<=n;j++)
				v[i][j]=v[j][i]=0.0;
		}
		v[i][i]=1.0;
		g=rv1[i];
		l=i;
	}

	for (i=IMIN(m,n);i>=1;i--)
	{
		l=i+1;
		g=w[i];
		for (j=l;j<=n;j++)
			a[i][j]=0.0;

		if (g)
		{
			g=1.0/g;
			for (j=l;j<=n;j++)
			{
				for (s=0.0,k=l;k<=m;k++) s += a[k][i]*a[k][j];
				f=(s/a[i][i])*g;
				for (k=i;k<=m;k++) a[k][j] += f*a[k][i];
			}

			for (j=i;j<=m;j++)
				a[j][i] *= g;
		}
		else
			for (j=i;j<=m;j++)
				a[j][i]=0.0;

		++a[i][i];
	}

	for (k=n;k>=1;k--)
	{
		for (its=1;its<=30;its++)
		{
			flag=1;
			for (l=k;l>=1;l--)
			{
				nm=l-1;
				if ((float)(fabs(rv1[l])+anorm) == anorm)
				{
					flag=0;
					break;
				}
				if ((float)(fabs(w[nm])+anorm) == anorm) break;
			}
			if (flag)
			{
				c=0.0;
				s=1.0;
				for (i=l;i<=k;i++)
				{
					f=s*rv1[i];
					rv1[i]=c*rv1[i];
					if ((float)(fabs(f)+anorm) == anorm) break;
					g=w[i];
					h=pythag(f,g);
					w[i]=h;
					h=1.0/h;
					c=g*h;
					s = -f*h;
					for (j=1;j<=m;j++)
					{
						y=a[j][nm];
						z=a[j][i];
						a[j][nm]=y*c+z*s;
						a[j][i]=z*c-y*s;
					}
				}
			}
			z=w[k];
			if (l == k)
			{
				if (z < 0.0)
				{
					w[k] = -z;
					for (j=1;j<=n;j++)
						v[j][k] = -v[j][k];
				}
				break;
			}
			if (its == svditer)
			{
				fprintf(stderr, "no convergence in svditer=%i svdcmp iterations \n", svditer);
				return(0);
			}

			x=w[l];
			nm=k-1;
			y=w[nm];
			g=rv1[nm];
			h=rv1[k];
			f=((y-z)*(y+z)+(g-h)*(g+h))/(2.0*h*y);
			g=pythag(f,1.0);
			f=((x-z)*(x+z)+h*((y/(f+SIGN(g,f)))-h))/x;
			c=s=1.0;
			for (j=l;j<=nm;j++)
			{
				i=j+1;
				g=rv1[i];
				y=w[i];
				h=s*g;
				g=c*g;
				z=pythag(f,h);
				rv1[j]=z;
				c=f/z;
				s=h/z;
				f=x*c+g*s;
				g = g*c-x*s;
				h=y*s;
				y *= c;
				for (jj=1;jj<=n;jj++)
				{
					x=v[jj][j];
					z=v[jj][i];
					v[jj][j]=x*c+z*s;
					v[jj][i]=z*c-x*s;
				}

				z=pythag(f,h);
				w[j]=z;
				if (z)
				{
					z=1.0/z;
					c=f*z;
					s=h*z;
				}

				f=c*g+s*y;
				x=c*y-s*g;
				for (jj=1;jj<=m;jj++)
				{
					y=a[jj][j];
					z=a[jj][i];
					a[jj][j]=y*c+z*s;
					a[jj][i]=z*c-y*s;
				}
			}
			rv1[l]=0.0;
			rv1[k]=f;
			w[k]=x;
		}
	}
	free_vector(rv1,1,n);
	return(1);
}

float pythag(float a, float b)
{
	float absa,absb;
	absa=fabs(a);
	absb=fabs(b);
	if (absa > absb)
		return absa*sqrt(1.0+SQR1(absb/absa));
	else
		return (absb == 0.0 ? 0.0 : absb*sqrt(1.0+SQR1(absa/absb)));
}

float *vector(long nl, long nh)
/* allocate a float vector with subscript range v[nl..nh] */
{
	float *v;

	v=(float *)malloc((size_t) ((nh-nl+1+NR_END)*sizeof(float)));
	if (!v) err("allocation failure in vector()");
	return v-nl+NR_END;
}

int *ivector(long nl, long nh)

/* allocate an int vector with subscript range v[nl..nh] */
{
	int *v;

	v=(int *)malloc((size_t) ((nh-nl+1+NR_END)*sizeof(int)));
	if (!v) err("allocation failure in ivector()");
	return v-nl+NR_END;
}

float **matrix(long nrl, long nrh, long ncl, long nch)
/* allocate a float matrix with subscript range m[nrl..nrh][ncl..nch] */
{
	long i, nrow=nrh-nrl+1,ncol=nch-ncl+1;
	float **m;

	/* allocate pointers to rows */
	m=(float **) malloc((size_t)((nrow+NR_END)*sizeof(float*)));
	if (!m) err("allocation failure 1 in matrix()");
	m += NR_END;
	m -= nrl;

	/* allocate rows and set pointers to them */
	m[nrl]=(float *) malloc((size_t)((nrow*ncol+NR_END)*sizeof(float)));
	if (!m[nrl]) err("allocation failure 2 in matrix()");
	m[nrl] += NR_END;
	m[nrl] -= ncl;

	for(i=nrl+1;i<=nrh;i++) m[i]=m[i-1]+ncol;

	/* return pointer to array of pointers to rows */
	return m;
}

void free_vector(float *v, long nl, long nh)
/* free a float vector allocated with vector() */
{
	free((FREE_ARG) (v+nl-NR_END));
}

void free_ivector(int *v, long nl, long nh)
/* free an int vector allocated with ivector() */
{
	free((FREE_ARG) (v+nl-NR_END));
}

void free_matrix(float **m, long nrl, long nrh, long ncl, long nch)
/* free a float matrix allocated by matrix() */
{
	free((FREE_ARG) (m[nrl]+ncl-NR_END));
	free((FREE_ARG) (m+nrl-NR_END));
}

