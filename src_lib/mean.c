/*
 * mean.c
 *
 *  Created on: Nov 19, 2012
 *      Author: toto
 */

#include "mean.h"

float mean(float *a, int n)
/*< get a mean value >*/
{
	int i;
	float t=0;

	for (i=0; i < n; i++)
		t+=a[i];
	t = t/n;

	return t;
}

float alphamean(float *temp, int nfw, float alpha)
/*< get a alpha-trimmed-mean value >*/
{
	int i,j,pass,lowc,higc;
	float mean,a;
	for(pass=1;pass<nfw;pass++){
		for(i=0;i<nfw-pass;i++){
			if(temp[i]>temp[i+1]){
				a=temp[i];
				temp[i]=temp[i+1];
				temp[i+1]=a;
			}
		}
	}
	lowc=alpha*nfw;
	higc=(1-alpha)*nfw;
	mean=0.;
	j=lowc;
	do {
		mean+=temp[j];
		j++;
	}while(j<higc);
	mean/=(1-2*alpha)*(nfw-1)+1;
	return mean;
}

float rangemean(float *temp, int nfw, float pclip)
/*< get a modified-trimmed-mean value >*/
{
	int i,pass,n;
	float median,mean,a,dis1,dis2,*data,*weight;
	data = (float*) calloc(nfw, sizeof(float));
	weight = (float*) calloc(nfw, sizeof(float));
	for(pass=0;pass<nfw;pass++){
		data[pass]=temp[pass];
	}
	for(pass=1;pass<nfw;pass++){
		for(i=0;i<nfw-pass;i++){
			if(temp[i]>temp[i+1]){
				a=temp[i];
				temp[i]=temp[i+1];
				temp[i+1]=a;
			}
		}
	}
	median=temp[nfw/2];
	dis1=fabs(temp[0]-median);
	dis2=fabs(temp[nfw-1]-median);
	mean=0.;
	n=0;
	if (dis1 >= dis2){
		pclip*=dis1*0.01;
	} else {
		pclip*=dis2*0.01;
	}
	for(pass=0;pass<nfw;pass++){
		if (data[pass]>=(median-pclip) && data[pass]<=(median+pclip)){
			weight[pass]=1.;
			n++;
		} else{
			weight[pass]=0.;
		}
		mean+=data[pass]*weight[pass];
	}
	if (n==0) n=1;
	mean/=n;

	free(data);
	free(weight);
	return mean;
}

float sdeviation(float *temp, int nfw)
/*< calculate a standard deviation >*/
{
	int i;
	float m,sd,*data;
	data = (float*) calloc(nfw, sizeof(float));
	for(i=0;i<nfw;i++){
		data[i]=temp[i];
	}
	m=mean(data,nfw);
	for(i=0;i<nfw;i++){
		data[i]=(data[i]-m)*(data[i]-m);
	}
	sd=mean(data,nfw);
	sd=sqrtf(sd);

	free(data);
	return sd;
}

float medianfilter(float *temp,int nfw)
/*< get a median value >*/
{
	int i,pass;
	float median,a;
	for(pass=1;pass<nfw;pass++){
		for(i=0;i<nfw-pass;i++){
			if(temp[i]>temp[i+1]){
				a=temp[i];
				temp[i]=temp[i+1];
				temp[i+1]=a;
			}
		}
	}
	median=temp[nfw/2];
	return median;
}

