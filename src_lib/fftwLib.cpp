/*
 * fftwLib.cpp
 *
 *  Created on: Jul 10, 2012
 *      Author: toto
 */

#include "fftwLib.h"

/* --------------------------------------------------------------------
 * FFT for floating complex number data
 * Comparation with Matlab Command :
 * b = fft(A) --> b = fftwf_data(A, lenA, lenA)
 * b = fft(A, nfft) --> b = fftwf_data(A, lenA, nfft)
 * -------------------------------------------------------------------- */
fftwf_complex *fftwf_data(fftwf_complex *input, int ndata, int nfft)
{
	fftwf_complex *paddata;
	fftwf_complex *fdata;
	fftwf_plan plan_forward;

	if(nfft<ndata) {
		fprintf(stderr, "nfft < ndata \n");
		exit(0);
	}

	//allocate memory for data+padding
	paddata = ( fftwf_complex* ) fftwf_malloc( sizeof( fftwf_complex ) * nfft );

	//allocate data for output fft process
	fdata = ( fftwf_complex* ) fftwf_malloc( sizeof( fftwf_complex ) * nfft );

	//padding input data
	memset(paddata[0], 0, nfft*sizeof(fftwf_complex));
	memcpy(paddata[0], input[0], ndata*sizeof(fftwf_complex));

	plan_forward  = fftwf_plan_dft_1d( nfft, paddata, fdata, FFTW_FORWARD, FFTW_ESTIMATE );
	fftwf_execute( plan_forward ); //execute fft

	//free allocate memory
	fftwf_destroy_plan( plan_forward );
	fftwf_free( paddata );

	return(fdata);
}

/* --------------------------------------------------------------------
 * IFFT for floating complex number data
 * Comparation with Matlab Command :
 * b = ifft(A) --> b = ifftwf_data(A, lenA, lenA)
 * b = ifft(A, nfft) --> b = ifftwf_data(A, lenA, nfft)
 * -------------------------------------------------------------------- */
fftwf_complex *ifftwf_data(fftwf_complex *fdata, int ndata, int nfft)
{
	fftwf_complex *idata;
	fftwf_complex *idatapad;
	fftwf_plan plan_backward;
	int i;

	if(nfft<ndata) {
		fprintf(stderr, "nfft < ndata \n");
		exit(0);
	}

	//allocate memory for data
	idata = ( fftwf_complex* ) fftwf_malloc( sizeof( fftwf_complex ) * ndata );

	//allocate memory for inversfft + padding
	idatapad = ( fftwf_complex* ) fftwf_malloc( sizeof( fftwf_complex ) * nfft );

	plan_backward = fftwf_plan_dft_1d( nfft, fdata, idatapad, FFTW_BACKWARD, FFTW_ESTIMATE );
	fftwf_execute( plan_backward ); //execute invers fft

	//get invers fft with length ndata and divide the value with nfft
	for( i=0; i<ndata; i++ ){
		idata[i][0] = idatapad[i][0]/nfft;	//real data
		idata[i][1] = idatapad[i][1]/nfft;	//imaginer data
	}

	//free allocate memory
	fftwf_destroy_plan( plan_backward );
	fftwf_free( idatapad );

	return(idata);
}

/* --------------------------------------------------------------------
 * FFT for double complex number data
 * Comparation with Matlab Command :
 * b = fft(A) --> b = fftwf_data(A, lenA, lenA)
 * b = fft(A, nfft) --> b = fftwd_data(A, lenA, nfft)
 * -------------------------------------------------------------------- */
fftw_complex *fftwd_data(fftw_complex *input, int ndata, int nfft)
{
	fftw_complex *paddata;
	fftw_complex *fdata;
	fftw_plan plan_forward;

	if(nfft<ndata) {
		fprintf(stderr, "nfft < ndata \n");
		exit(0);
	}

	//allocate memory for data+padding
	paddata = ( fftw_complex* ) fftw_malloc( sizeof( fftw_complex ) * nfft );

	//allocate data for output fft process
	fdata = ( fftw_complex* ) fftw_malloc( sizeof( fftw_complex ) * nfft );

	//padding input data
	memset(paddata[0], 0, nfft*sizeof(fftw_complex));
	memcpy(paddata[0], input[0], ndata*sizeof(fftw_complex));

	plan_forward  = fftw_plan_dft_1d( nfft, paddata, fdata, FFTW_FORWARD, FFTW_ESTIMATE );
	fftw_execute( plan_forward ); //execute fft

	//free allocate memory
	fftw_destroy_plan( plan_forward );
	fftw_free( paddata );

	return(fdata);
}

/* --------------------------------------------------------------------
 * IFFT for double complex number data
 * Comparation with Matlab Command :
 * b = ifft(A) --> b = ifftwd_data(A, lenA, lenA)
 * b = ifft(A, nfft) --> b = ifftwd_data(A, lenA, nfft)
 * -------------------------------------------------------------------- */
fftw_complex *ifftwd_data(fftw_complex *fdata, int ndata, int nfft)
{
	fftw_complex *idata;
	fftw_complex *idatapad;
	fftw_plan plan_backward;
	int i;

	if(nfft<ndata) {
		fprintf(stderr, "nfft < ndata \n");
		exit(0);
	}

	//allocate memory for data
	idata = ( fftw_complex* ) fftw_malloc( sizeof( fftw_complex ) * ndata );

	//allocate memory for inversfft + padding
	idatapad = ( fftw_complex* ) fftw_malloc( sizeof( fftw_complex ) * nfft );

	plan_backward = fftw_plan_dft_1d( nfft, fdata, idatapad, FFTW_BACKWARD, FFTW_ESTIMATE );
	fftw_execute( plan_backward ); //execute invers fft

	//get invers fft with length ndata and divide the value with nfft
	for( i=0; i<ndata; i++ ){
		idata[i][0] = idatapad[i][0]/nfft;	//real data
		idata[i][1] = idatapad[i][1]/nfft;	//imaginer data
	}

	//free allocate memory
	fftw_destroy_plan( plan_backward );
	fftw_free( idatapad );

	return(idata);
}

void fftw1f(float *input, int ndata, int nf, int nfft, float *dreal, float *dimag)
{
	fftwf_complex *cinput;
	fftwf_complex *fdata;
	int i;

	cinput = ( fftw_complex* ) fftw_malloc( sizeof( fftw_complex ) * ndata );
	for(i=0; i<ndata; i++) {
		cinput[i][0] = input[i];
		cinput[i][1] = 0.0;
	}

	fdata = fftwf_data(cinput, ndata, nfft);

	for(i=0; i<nf; i++) {
		dreal[i] = fdata[i][0];
		dimag[i] = fdata[i][1];
	}

	fftwf_free( cinput );
	fftwf_free( fdata );
}

void ifftw1f(float *input, int ndata, int nfft, float *dreal, float *dimag)
{
	fftwf_complex *fdata;
	fftwf_complex *data;
	int i;

	fdata = ( fftw_complex* ) fftw_malloc( sizeof( fftw_complex ) * ndata );
	for(i=0; i<ndata; i++) {
		fdata[i][0] = dreal[i];
		fdata[i][1] = dimag[i];
	}

	data = ifftwf_data(fdata, ndata, nfft);
	for(i=0; i<ndata; i++)
		input[i] = data[i][0];

	fftwf_free(fdata);
	fftwf_free(data);
}

//void fftw2f(float **input, int nrow, ncol, int nfft, float **dreal, float **dimag, bool type)
//{
//	int idx1, idx2;
//	int i, j;
//
//	if (type==true)	//process by row
//	{
//		idx1 = nrow;
//		idx2 = ncol;
//	}
//	else	// process by column
//	{
//		idx1 = ncol;
//		idx2 = nrow;
//	}
//}
//
//void ifftw2f(float **input, int nrow, ncol, int nfft, float **dreal, float **dimag, bool type)
//{
//
//}
