/*
 * cadzowLib.c
 *
 *  Created on: Apr 23, 2012
 *      Author: toto
 */

#include "cadzowLib.h"

//change cadzow algorithm
// 28 November 2011
void runCadzow1D(float **data, int rank, int nx, int nz, int nf, int nfft, float d1,
		int tmin, int tmax)
{
	int j, fidx;
	double **dreal, **dimag;
	double *freqslice;
	double *fcadzowreal=NULL, *fcadzowimag=NULL;
	hankel1d *hankelr, *hankeli;

	/* allocated memory */
	dreal = alloc2double(nf, nx);
	dimag = alloc2double(nf, nx);
	memset(dreal[0], 0, nf*nx*sizeof(double));
	memset(dimag[0], 0, nf*nx*sizeof(double));

	/* fft data */
	fftProcessd(data, nz, nx, nf, nfft, dreal, dimag);

	freqslice = alloc1double(nx);
	fcadzowreal = alloc1double(nx);
	fcadzowimag = alloc1double(nx);

	/* process cadzow */
	for(fidx=tmin; fidx<tmax; fidx++)
	{

//		if(fidx%100==0)
//			fprintf(stderr, "Process FFT [%i / %i] tmin=%i tmax=%i d1=%f\n", fidx+1, nf, tmin, tmax, d1);

		/* process real data */
		getfreqslice(dreal, nx, nz, fidx, freqslice);
		hankelr = createhankel1d_modif(freqslice, nx);
		rankApproximationd(hankelr->data, hankelr->m, hankelr->l, rank);
		inverseHankel1D(hankelr, fcadzowreal);

		/* process imaginer data */
		getfreqslice(dimag, nx, nz, fidx, freqslice);
		hankeli = createhankel1d_modif(freqslice, nx);
		rankApproximationd(hankeli->data, hankeli->m, hankeli->l, rank);
		inverseHankel1D(hankeli, fcadzowimag);

		for(j=0; j<nx; j++)
		{
			dreal[j][fidx] = fcadzowreal[j];
			dimag[j][fidx] = fcadzowimag[j];
		}

		memset(fcadzowreal, 0, nx*sizeof(double));
		memset(fcadzowimag, 0, nx*sizeof(double));
		freehankel1d(hankelr);
		freehankel1d(hankeli);
	}

	/* invers fft */
	ifftProcessd(data, nz, nx, nf, nfft, dreal, dimag);

	/*free memory */
	free1double(fcadzowreal);
	free1double(fcadzowimag);
	free2double(dreal);
	free2double(dimag);
	free1double(freqslice);
}

// 28 Februari 2013
void robustRunCadzow1D(float **data, int rank, int nx, int nz, int nf, int nfft, float d1,
		int tmin, int tmax, float e, int maxiter)
{
	int j, fidx;
	int iter;
	double **dreal, **dimag;
	double *freqslicer, *freqslicei;
	double *fcadzowreal=NULL, *fcadzowimag=NULL;
	hankel1d *hankelr, *hankeli;

	/* allocated memory */
	dreal = alloc2double(nf, nx);
	dimag = alloc2double(nf, nx);
	memset(dreal[0], 0, nf*nx*sizeof(double));
	memset(dimag[0], 0, nf*nx*sizeof(double));

	/* fft data */
	fftProcessd(data, nz, nx, nf, nfft, dreal, dimag);

	freqslicer = alloc1double(nx);
	freqslicei = alloc1double(nx);
	fcadzowreal = alloc1double(nx);
	fcadzowimag = alloc1double(nx);

	/* process cadzow */
	for(fidx=tmin; fidx<tmax; fidx++)
	{
		/* process real data */
		getfreqslice(dreal, nx, nz, fidx, freqslicer);
		getfreqslice(dimag, nx, nz, fidx, freqslicei);
		for(iter=0; iter<maxiter; iter++)
		{
			hankelr = createhankel1d_modif(freqslicer, nx);
			rankApproximationd(hankelr->data, hankelr->m, hankelr->l, rank);
			inverseHankel1D(hankelr, fcadzowreal);

			reweight(fcadzowreal, freqslicer, e, nx);

			/* process imaginer data */
			hankeli = createhankel1d_modif(freqslicei, nx);
			rankApproximationd(hankeli->data, hankeli->m, hankeli->l, rank);
			inverseHankel1D(hankeli, fcadzowimag);

			reweight(fcadzowimag, freqslicei, e, nx);

			for(j=0; j<nx; j++)
			{
				dreal[j][fidx] = fcadzowreal[j];
				dimag[j][fidx] = fcadzowimag[j];
			}

			memset(fcadzowreal, 0, nx*sizeof(double));
			memset(fcadzowimag, 0, nx*sizeof(double));
			freehankel1d(hankelr);
			freehankel1d(hankeli);
		}
	}

	/* invers fft */
	ifftProcessd(data, nz, nx, nf, nfft, dreal, dimag);

	/*free memory */
	free1double(fcadzowreal);
	free1double(fcadzowimag);
	free2double(dreal);
	free2double(dimag);
	free1double(freqslicer);
	free1double(freqslicei);
}

void reweight(double *T, double *S, double e, int n)
{
	int i;
	double ui,w;
	for(i=0;i<n;i++)
	{
		ui=fabs(S[i]-T[i]);
		if(ui<e)
			w = pow( (1 - pow((ui/e),2)),2 );
		else
			w = 0.0;

		T[i]= ( (w*S[i]) + ( (1-w)*T[i] ) ) ;
	}
}

//change cadzow algorithm
// 8 September 2012
void runCadzow1D_wosa(float **data, int rank, int nx, int nz, float dt,
		int use, int span, int usetrace, int spantrace, float sigmax,
		float sigmay)
{
	int x,y;
	int nf, nfft;
	float d1;
	int jump, ww, jumptrace, wwtrace;
	float **gaussmask, **window;

	//compute computational parameter
	jump=(use*2)+1;
	ww = (span*2)+1;
	jumptrace=(usetrace*2)+1;
	wwtrace = (spantrace*2)+1;

	gaussmask = alloc2float(ww, wwtrace);
	window = alloc2float(ww, wwtrace);
//	fprintf(stderr,"jum=%i ww=%i jumptrace=%i wwtrace=%i \n", jump, ww, jumptrace, wwtrace);
	//compute gaussian mask
	gaussian2dmask(gaussmask, wwtrace, ww, sigmax, sigmay);

	//get FFT variable
	getNfft(ww, dt, &nf, &nfft, &d1);

	for(x=span;x<nz-span;x+=jump)
	{
		fprintf(stderr,"span %i / %i \n", x, nz-span);
		for(y=spantrace;y<nx-spantrace;y+=jumptrace)
		{
//			fprintf(stderr,"spantr %i / %i \n", y, nx-spantrace);
			getsubmatrix(data, window, x, y, span, spantrace);
			mask(window, gaussmask, wwtrace, ww);

			runCadzow1D(window, rank, wwtrace, ww, nf+1, nfft, d1, 0, nf);

			mask(window, gaussmask, wwtrace, ww);
			addsubmatrix(data, window, x, y, span, spantrace);

		}
	}

	/*free memory */
	free2float(gaussmask);
	free2float(window);
}

//change cadzow algorithm
// 14 November 2012
void runCadzow1D_wosa_rev1(float **data, int rank, int nx, int nz, float dt,
		int use, int span, int usetrace, int spantrace, float sigmax,
		float sigmay)
{
	int x,y;
	int nf, nfft;
	float d1;
	int jump, ww, jumptrace, wwtrace;
	float **gaussmask, **window;

	//rev1
	int nspanel, ntracespanel;
	float **panel, **panel_filter;

	nspanel = nz + (span*2);
	ntracespanel = nx + (spantrace*2);

	panel = alloc2float(nspanel, ntracespanel);
	panel_filter = alloc2float(nspanel,ntracespanel);
	//fprintf(stderr, "%i %i \n", nspanel, ntracespanel);
	memset(panel[0], 0, nspanel*ntracespanel*sizeof(float));
	memset(panel_filter[0], 0, nspanel*ntracespanel*sizeof(float));
	for(x=0;x<nz;x++)
		for(y=0;y<nx;y++)
			panel[y+spantrace][x+span] = data[y][x];
	memset(panel_filter[0], 0, nspanel*ntracespanel*sizeof(float));

	//compute computational parameter
	jump=(use*2)+1;
	ww = (span*2)+1;
	jumptrace=(usetrace*2)+1;
	wwtrace = (spantrace*2)+1;

	gaussmask = alloc2float(ww, wwtrace);
	window = alloc2float(ww, wwtrace);
	//compute gaussian mask
	gaussian2dmask(gaussmask, wwtrace, ww, sigmax, sigmay);

	//get FFT variable
	getNfft(ww, dt, &nf, &nfft, &d1);

	for(x=span;x<nspanel-span;x+=jump)
	{
		fprintf(stderr,"span1 %i / %i \n", x, nspanel-span);
		for(y=spantrace;y<ntracespanel-spantrace;y+=jumptrace)
		{
			getsubmatrix(panel, window, x, y, span, spantrace);
			mask(window, gaussmask, wwtrace, ww);

			runCadzow1D(window, rank, wwtrace, ww, nf, nfft, d1, 0, nf);

			mask(window, gaussmask, wwtrace, ww);
			addsubmatrix(panel_filter, window, x, y, span, spantrace);

		}
	}

	for(x=0;x<nz;x++)
		for(y=0;y<nx;y++)
			data[y][x] = panel_filter[y+spantrace][x+span];

	/*free memory */
	free2float(gaussmask);
	free2float(window);
	free2float(panel);
	free2float(panel_filter);
}

// 28 februari 2013
void robustRunCadzow1D_wosa_rev1(float **data, int rank, int nx, int nz, float dt,
		int use, int span, int usetrace, int spantrace, float sigmax,
		float sigmay, float e, int maxiter)
{
	int x,y;
	int nf, nfft;
	float d1;
	int jump, ww, jumptrace, wwtrace;
	float **gaussmask, **window;

	//rev1
	int nspanel, ntracespanel;
	float **panel, **panel_filter;

	nspanel = nz + (span*2);
	ntracespanel = nx + (spantrace*2);

	panel = alloc2float(nspanel, ntracespanel);
	panel_filter = alloc2float(nspanel,ntracespanel);
	//fprintf(stderr, "%i %i \n", nspanel, ntracespanel);
	memset(panel[0], 0, nspanel*ntracespanel*sizeof(float));
	memset(panel_filter[0], 0, nspanel*ntracespanel*sizeof(float));
	for(x=0;x<nz;x++)
		for(y=0;y<nx;y++)
			panel[y+spantrace][x+span] = data[y][x];
	memset(panel_filter[0], 0, nspanel*ntracespanel*sizeof(float));

	//compute computational parameter
	jump=(use*2)+1;
	ww = (span*2)+1;
	jumptrace=(usetrace*2)+1;
	wwtrace = (spantrace*2)+1;

	gaussmask = alloc2float(ww, wwtrace);
	window = alloc2float(ww, wwtrace);
	//compute gaussian mask
	gaussian2dmask(gaussmask, wwtrace, ww, sigmax, sigmay);

	//get FFT variable
	getNfft(ww, dt, &nf, &nfft, &d1);

	for(x=span;x<nspanel-span;x+=jump)
	{
		fprintf(stderr,"span1 %i / %i \n", x, nspanel-span);
		for(y=spantrace;y<ntracespanel-spantrace;y+=jumptrace)
		{
			getsubmatrix(panel, window, x, y, span, spantrace);
			mask(window, gaussmask, wwtrace, ww);

			robustRunCadzow1D(window, rank, wwtrace, ww, nf, nfft, d1, 0, nf, e, maxiter);

			mask(window, gaussmask, wwtrace, ww);
			addsubmatrix(panel_filter, window, x, y, span, spantrace);

		}
	}

	for(x=0;x<nz;x++)
		for(y=0;y<nx;y++)
			data[y][x] = panel_filter[y+spantrace][x+span];

	/*free memory */
	free2float(gaussmask);
	free2float(window);
	free2float(panel);
	free2float(panel_filter);
}

void runCadzow2D(float **data, int ntrc, int nsp, float dt, int rank,
		int nsort1, int nsort2, float ffbeg, float ffend, int fillzero)
{
	int nf, nfft;
	float d1;
	double **dreal, **dimag;
	int fbeg, fend;

	getNfft(nsp, dt, &nf, &nfft, &d1);
	dreal = alloc2double(nf, ntrc);
	dimag = alloc2double(nf, ntrc);
	fbeg = (int) (ffbeg/d1);
	fend = (int) (ffend/d1);
	//fprintf(stderr,"%i %i \n", fbeg, fend);
	fftProcessd(data, nsp, ntrc, nf, nfft, dreal, dimag); //fft data

	processDataCadzow2d(dreal, nf, ntrc, rank, nsort1, nsort2, fbeg, fend, fillzero);	//process real data
	processDataCadzow2d(dimag, nf, ntrc, rank, nsort1, nsort2, fbeg, fend, fillzero);	//process imag data

	ifftProcessd(data, nsp, ntrc, nf, nfft, dreal, dimag); //invers fft

	free2double(dreal);
	free2double(dimag);
}

void processDataCadzow2d(double **data, int nf, int ntrc, int rank, int nsort1, int nsort2,
		int fbeg, int fend, int fillzero)
{
	int i, j, tr_1, tr_2;
	int dex;
	hankel2d* hankel;
	double **panel, **filter, **returnback;

	for(i=0; i<fbeg; i++)
		for (j=0; j<ntrc; j++)
			data[j][i] = 0.0;

	for(i=fend+1; i<nf; i++)
		for (j=0; j<ntrc; j++)
			data[j][i] = 0.0;

	panel = alloc2double(nsort2, nsort1);
	for(i=fbeg; i<=fend; i++)	//loop over frequency
	{
//		if(i%100==0)
//			fprintf(stderr, "Process [ %i / %i ]\n", i+1, fend);

		dex = 0;
		for(tr_1=0; tr_1<nsort1; tr_1++)	// --- GET TIME SLICING -----
		{
			for(tr_2=0; tr_2<nsort2; tr_2++)
			{
				panel[tr_1][tr_2] = data[dex][i];
				dex++;
			}
		}
				//--- CADZOW 2D PROCESS ----
		hankel = createhankel2d_modif(panel, nsort1, nsort2);
		filter = rankApproximation_v2(hankel->data, hankel->hm, hankel->hl, rank);
		memcpy(hankel->data[0], filter[0], hankel->hm*hankel->hl*sizeof(double));
		returnback = inverseHankel2D(hankel);

		//if(fillzero!=0 && i==fbeg) memset(data[0], 0, nf*ntrc*sizeof(float));

		dex=0;
		for(tr_1=0; tr_1<nsort1; tr_1++)	//update output data
		{
			for(tr_2=0; tr_2<nsort2; tr_2++)
			{
				data[dex][i] = returnback[tr_1][tr_2];
				dex++;
			}
		}

		free2double(returnback);
		free2double(filter);
		freehankel2d(hankel);

	}
	free2double(panel);
}

void savedata(double **data, int nx, int ny)
{
	FILE *fod;
	float **tmp;
	int i,j;

	fod = fopen("test1.bin","w");
	printf("nx=%i ny=%i \n", nx, ny);

	tmp = alloc2float(ny, nx);
	for (i=0; i<nx; i++)
		for (j=0; j<ny; j++)
			tmp[i][j] = (float) (data[i][j]);
	fwrite(tmp[0], sizeof(float), nx*ny, fod);
	fclose(fod);
	free2float(tmp);
}

hankel1d *hankel1d_alloc(int n)
{
	hankel1d* hankel = (hankel1d*) calloc(1, sizeof(hankel1d));
	hankel->m = (n+1)/2;
	hankel->l = n-hankel->m+1;
	hankel->data = alloc2double(hankel->l,hankel->m);

	return(hankel);
}

hankel2d *hankel2d_alloc(int row, int col)
{
	hankel2d* hankel = (hankel2d*) calloc(1, sizeof(hankel2d));
	hankel->s = col;
	hankel->m = (hankel->s+1)/2;
	hankel->l = (hankel->s-hankel->m+1);

	hankel->ss = row;
	hankel->mm = (hankel->ss+1)/2;
	hankel->ll = hankel->ss-hankel->mm+1;

	hankel->hm = hankel->m*hankel->mm;
	hankel->hl = hankel->l*hankel->ll;
	hankel->data = alloc2double(hankel->hl, hankel->hm);

	return(hankel);
}

void freehankel1d(hankel1d* hankel)
{
	hankel->m = 0;
	hankel->l = 0;
	free2double(hankel->data);
	free(hankel);
}

void freehankel2d(hankel2d* hankel)
{
	hankel->s = 0;
	hankel->m = 0;
	hankel->l = 0;

	hankel->ss = 0;
	hankel->mm = 0;
	hankel->ll = 0;

	hankel->hm = 0;
	hankel->hl = 0;
	free2double(hankel->data);
	free(hankel);
}

hankel1d *createhankel1d(double* data, int n)
{
	int idx, row, col;

	hankel1d* hankel = (hankel1d*) calloc(1, sizeof(hankel1d));
	hankel->m = (n+1)/2;
	hankel->l = n-hankel->m+1;
	hankel->data = alloc2double(hankel->l,hankel->m);

	for(row=0; row<hankel->m; row++)
	{
		idx = row;
		for(col=0; col<hankel->l; col++)
		{
			hankel->data[row][col] = (data[idx]);
			idx++;
		}
	}

	return hankel;
}

hankel1d *createhankel1d_modif(double* data, int n)
{
	int row;

	hankel1d* hankel = hankel1d_alloc(n);

	for(row=0; row<hankel->m; row++)
		memcpy(hankel->data[row], data+row, hankel->l*sizeof(double));

	return hankel;
}

hankel2d *createhankel2d(double** in, int rowin, int colin)
{
	int beg=0;
	int row,col,r,c,i,j;

	hankel2d* hankel = (hankel2d*) calloc(1, sizeof(hankel2d));
	hankel->s = colin;
	hankel->m = (hankel->s+1)/2;
	hankel->l = (hankel->s-hankel->m+1);

	hankel->ss = rowin;
	hankel->mm = (hankel->ss+1)/2;
	hankel->ll = hankel->ss-hankel->mm+1;

	hankel->hm = hankel->m*hankel->mm;
	hankel->hl = hankel->l*hankel->ll;
	hankel->data = alloc2double(hankel->hl, hankel->hm);

	for(row=0; row<hankel->mm; row++)
	{
		for(col=0; col<hankel->ll; col++)
		{
			hankel1d* sub = createhankel1d(in[beg+col],colin);
			r = row*hankel->m;
			for(i=0; i<hankel->m; i++)
			{
				c = col*hankel->l;
				for(j=0; j<hankel->l; j++)
				{
					hankel->data[r][c] = sub->data[i][j];
					c++;
				}
				r++;
			}
			freehankel1d(sub);
		}
		beg++;
	}

	return hankel;
}

hankel2d *createhankel2d_modif(double** in, int rowin, int colin)
{
	int row, col, r, c, i;

	hankel2d* hankel = hankel2d_alloc(rowin, colin);

	for(row=0; row<hankel->mm; row++)
	{
		for(col=0; col<hankel->ll; col++)
		{
			hankel1d* sub = createhankel1d_modif(in[row+col],colin);

			r = row*hankel->m;
			for(i=0; i<hankel->m; i++)
			{
				c = col*hankel->l;
				memcpy(hankel->data[r+i]+c, sub->data[i], hankel->l*sizeof(double));
			}
			freehankel1d(sub);
		}
	}

	return hankel;
}

double **rankApproximation_v2(double** data, int row, int col, int rank)
{
	int k,i,j;
	double **u, *s, **v, **out;
	double eigenimage;

	u = alloc2double(row, row);
	s = alloc1double(row);
	v = alloc2double(col, col);

	svdlapackd(data, row, col, u, s, v);

	out = alloc2double(col, row);
	memset(out[0], 0, row*col*sizeof(double));

	for(i=0;i<row;i++)
	{
		for(j=0;j<col;j++)
		{
			eigenimage = 0.0;
			for(k=0; k<rank; k++) //loop over rank
				eigenimage = eigenimage + v[k][j] * u[i][k] * s[k];

			out[i][j] = eigenimage;
		}
	}

	free2double(u);
	free1double(s);
	free2double(v);

	return out;
}

double **getrank_v21(double** u,double* s,double** v, int row, int col, int rank)
{
	double **su = alloc2double(col, row);
	int i,j;
	for(i=0; i<row; i++)
		for(j=0; j<col; j++)
			su[i][j] = v[rank][j] * u[i][rank] * s[rank];

	return su;
}

double **getrank_v2(double** u,double** s,double** v, int row, int col, int rank)
{
	double **su = alloc2double(col, row);
	int i,j;
	for(i=0; i<row; i++)
		for(j=0; j<col; j++)
			su[i][j] = v[rank][j] * u[i][rank] * s[rank][rank];

	return su;
}

double **inverseHankel2D(hankel2d* hankel)
{
	int ssdex;
	int mm, ll, m, l, s, ss;
	int row, col, i, j;
	int indexlist;
	int r, c, cnt, mdex, dex;
	double **list=NULL, **sub=NULL, *av=NULL, **inv=NULL;
	double *a;

	mm = hankel->mm;
	ll = hankel->ll;
	m = hankel->m;
	l = hankel->l;
	s = hankel->s;
	ss = hankel->ss;
	ssdex = 0;

	list = alloc2double(l+m-1, mm*ll);

	indexlist=0;
	for(row=0; row<mm; row++)
	{
		for(col=0; col<ll; col++)
		{
			sub = alloc2double(l, m);
			r = row*m;
			for( i=0;i<m;i++)
			{
				c = col*l;
				for( j=0;j<l;j++)
				{
					sub[i][j] = hankel->data[r][c];
					c++;
				}
				r++;
			}
			av = average(sub, m, l);

			memcpy(list[indexlist], av, (m+l-1)*sizeof(double));

			indexlist++;
			ssdex++;
			free1double(av);
			free2double(sub);
		}
	}

	inv = alloc2double(s, ss);
	for( i=0;i<mm;i++)
	{
		a = alloc1double(s);
		memset(a, 0, s*sizeof(double));

		cnt = 0;

		for( j=0;j<i+1l;j++)
		{
			stack(a, list[(i+(j*(ll-1)))], s);
			cnt++;
		}

		divarr(a, cnt, s);
		memcpy(inv[i], a, s*sizeof(double));
		free1double(a);
	}

	if(mm==ll)
	{
		cnt  = ll-1;
		mdex = 2;
		for(i=mm; i<mm+ll-1; i++)
		{
			dex = (mdex*ll)-1;
			a = alloc1double(s);
			memset(a, 0, s*sizeof(double));

			for(j=0; j<cnt; j++)
			{
				stack(a, list[dex], s);
				dex+=(ll-1);
			}
			divarr(a, cnt, s);
			memcpy(inv[i], a, s*sizeof(double));
			cnt--;
			mdex++;
			free1double(a);
		}
	}
	else
	{
		cnt  = mm;
		mdex = 1;
		for(i=mm; i<ll+mm-1; i++)
		{
			dex = (mdex*ll)-1;
			a = alloc1double(s);
			memset(a, 0, s*sizeof(double));

			for( j=0;j<cnt;j++)
			{
				stack(a, list[dex], s);
				dex+=(ll-1);
			}
			divarr(a, cnt, s);
			memcpy(inv[i], a, s*sizeof(double));
			cnt--;
			mdex++;
			free1double(a);
		}
	}

	if(list) free2double(list);
//	if(sub) free2double(sub);
//	if(av) free1double(av);
	return inv;
}

void inverseHankel1D(hankel1d *hankel, double *sumcol)
{
	int m, l;
	double *val, *avg;
	int i, j, cnt, mdex, dex, mnt;
	int dx;

	m = hankel->m;
	l = hankel->l;

	val = alloc1double(l*m);
	avg = alloc1double(l+m-1);
	memset(val, '\0', l*m*sizeof(double));
	memset(avg, '\0', (l+m-1)*sizeof(double));

	dx = 0;
	for(i=0;i<m;i++)
	{
		for(j=0;j<l;j++)
		{
			val[dx]=hankel->data[i][j];
			dx++;
		}
	}

	for(i=0;i<m;i++)
	{
		cnt=0;
		for(j=0;j<i+1;j++)
		{
			avg[i]+=val[i+(j*(l-1))];
			cnt++;
		}
		avg[i]/=cnt;
//		printf("%i. %3.3f \n", i+1, avg[i]);
	}

	if(m==l)
	{
		cnt  = l-1;
		mdex = 2;
		for(i=m;i<l+m-1;i++)
		{
			dex = (mdex*l)-1;
			mnt = 0;
			for(j=0;j<cnt;j++)
			{
				mnt++;
				avg[i]+=val[dex];
				dex+=(l-1);
			}
			avg[i]/=cnt;
			cnt--;
			mdex++;
		}

	}
	else
	{
		cnt  = m;
		mdex = 1;
		for(i=m;i<l+m-1;i++)
		{
			dex = (mdex*l)-1;
			mnt = 0;
			for(j=0;j<cnt;j++)
			{
				mnt++;
				avg[i]+=val[dex];
				dex+=(l-1);
			}
			avg[i]/=cnt;
			cnt--;
			mdex++;
//			printf("%i  ", i);
		}
	}

	memcpy(sumcol, avg, (l+m-1)*sizeof(double));
	free1double(val);
	free1double(avg);
}

double *average(double** data, int m, int l)
{
	int i, j;
	int dx;
	int mdex, dex, mnt;
	double *val, *avg;
	double cnt;

	if(l<m){
		fprintf(stderr, "collum < row : this procedure are not intended for this condition\n");
		exit(0);
	}
	val = alloc1double(l*m); //(double*) calloc((l*m),sizeof(double));
	avg = alloc1double(l+m-1); //(double*) calloc((l+m-1),sizeof(double));
	memset(val, 0, l*m*sizeof(double));
	memset(avg, 0, (l+m-1)*sizeof(double));

	dx = 0;
	for(i=0; i<m; i++)
	{
		for(j=0; j<l; j++)
		{
			val[dx] = data[i][j];
			dx++;
		}
	}

	cnt = 0;
	for(i=0; i<m; i++)
	{
		cnt = 0;
		for(j=0; j<i+1; j++)
		{
			avg[i] += val[i+(j*(l-1))];
			cnt++;
		}
		avg[i] /= cnt;
	}


	if(m==l)
	{
		cnt  = l-1;
		mdex = 2;
		for(i=m; i<l+m-1; i++)
		{
			dex = (mdex*l)-1;
			mnt = 0;
			for(j=0; j<cnt; j++)
			{
				mnt++;
				avg[i] += val[dex];
				dex += (l-1);
			}
			avg[i] /= cnt;
			cnt--;
			mdex++;
		}

	}
	else
	{
		cnt  = m;
		mdex = 1;
		for(i=m; i<l+m-1; i++)
		{
			dex = (mdex*l)-1;
			mnt = 0;
			for(j=0; j<cnt; j++)
			{
				mnt++;
				avg[i] += val[dex];
				dex += (l-1);
			}
			avg[i] /= cnt;
			cnt--;
			mdex++;
		}
	}

	free1double(val);
	return avg;
}

void stack(double* d1,double* d2,int l)
{
	int i;
    for(i=0; i<l; i++)
        d1[i] += d2[i];
}

void divarr(double *d1,double div, int l)
{
    int i;
	for(i=0; i<l; i++)
        d1[i] /= div;
}

void getfreqslice(double **data, int rowdata, int coldata, int fidx, double *fslice)
{
	int row;

	for(row=0;row<rowdata;row++)
		fslice[row] = data[row][fidx];
}

//--------------------------- wosa -------------------------------------------
void gaussian2dmask(float **data, int nx, int ny,float sigmax, float sigmay)
{
	int cx, cy, x, y;
	double xx, yy;

	if(nx%2==0 || ny%2==0)
	{
		fprintf(stderr, "mod(nx,2) or mod(ny,2) == 0 \n");
		exit(0);
	}

	cx = ((nx-1)/2)+1;
	cy = ((ny-1)/2)+1;

	for(x=0;x<nx;x++)
	{
		for(y=0;y<ny;y++)
		{
			xx = ((x-cx)*(x-cx))/(2*sigmax*sigmax);
			yy = ((y-cy)*(y-cy))/(2*sigmay*sigmay);
			data[x][y] = (float) (exp(-1*(xx+yy)));
		}
	}
}

void getsubmatrix(float **data, float **window, int cx, int cy, int span,int spantrace)
{
	int i,j;
	int ndex, tdex;

	ndex = 0;
	tdex = 0;

	for(i=cx-span;i<=cx+span;i++)
	{
		tdex=0;
		for(j=cy-spantrace;j<=cy+spantrace;j++)
		{
			window[tdex][ndex]=data[j][i];
			tdex++;
		}
		ndex++;
	}
}

void mask(float **input, float **mask, int nrow, int ncol)
{
	int x,y;

	for(y=0;y<ncol;y++)
		for(x=0;x<nrow;x++)
			input[x][y] = input[x][y]*mask[x][y];
}

void addsubmatrix(float **data, float **window, int cx, int cy, int span,int spantrace)
{
	int i,j;
	int ndex, tdex;

	ndex=0;
	tdex=0;
	for(i=cx-span; i<=cx+span; i++)
	{
		tdex=0;
		for(j=cy-spantrace; j<=cy+spantrace; j++)
		{
			data[j][i]+=window[tdex][ndex];
			tdex++;
		}
		ndex++;
	}
}
