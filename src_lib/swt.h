/*
 * swt.h
 *
 *  Created on: Dec 14, 2012
 *      Author: toto
 */

#ifndef SWT_H_
#define SWT_H_

#include "segy_lib.h"
#include "../matlab_lib/wavelet/cWavelet.h"
#include "../matlab_lib/datafun/conv.h"
#include "../matlab_lib/signal/cSignal.h"
float *run_swt_v1(float *trace, int nt,
		int _filter_id, int _level_scale, float _treshold, int _ntreshold,
		int _leakage1, int _leakage2, int _inv_level/*, int *newNt*/);
float *run_swt_v2(float *trace, int nt,
		int _filter_id, int _level_scale, float _treshold,
		int _treshold_start, int _ntreshold,
		int _inv_level/*, int *newNt*/);
void forward_swt(float *ca, int Nca, int _level_scale, int ndb,
		float **LoD, float **HiD, float **LoR, float **HiR,
		float **swa, float **swd);

void threshold_data(float **swd, int nrow, int ncol,
		float _threshold, int _ntreshold, float **swdth);
void threshold_data_v2(float **swd, int nrow, int ncol,
		float _treshold, int _treshold_start, int _ntreshold, float **swdth);
void threshold_data_v3(float **swd, int nrow, int ncol,
		float _treshold, int _ntreshold, float **swdth);

void inverse_swt_coeff_leakswt(float **swd, float **swa, float **swdth,
		float **LoD, float **HiD,
		int nrow, int ncol,
		int leakage1, int leakage2, int ndb,
		float **swdcfs, float _scale);
void inverse_swt_leakswt(float **swa, float **swd, float **swdcfs, float **LoR, float **HiR,
		int nrow, int ncol, int _inv_level, int ndb, float *trace, int leakage1);

void inverse_swt_coeff(float **swd, float **swa, float **swdth,
		float **LoR, float **HiR,
		int nrow, int ncol,
		int leakage1, int leakage2, int ndb,
		float **swdcfs);
void inverse_swt(float **swa, float **swdcfs, float **LoR, float **HiR,
		int nrow, int ncol, int _inv_level, int ndb, float *trace);

#endif /* SWT_H_ */
