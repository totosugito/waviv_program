/*
 * tesWindowing.c
 *
 *  Created on: Jun 21, 2012
 *      Author: toto
 */
#include "tesWindowing.h"

int **createWindowingArray(int beg_pos, int end_pos, int spansize, int spanfilter, int *iwindow)
{
	int **posN1, nwindow1;

	nwindow1 = nWindowingCount(beg_pos, end_pos, spansize, spanfilter);
	posN1 = alloc2int(6, nwindow1);
	fillWindowingArray(posN1, nwindow1, beg_pos, end_pos, spansize, spanfilter);

	(*iwindow) = nwindow1;
	return(posN1);
}

void printWindowingPosArray(int **posN, int ndata)
{
	int i;

	for(i=0; i<ndata; i++)
	{
		printf("%i. READ [%i to %i], SAVE [%i to %i], INDEX [%i to %i] \n", i+1,
				posN[i][0], posN[i][1],
				posN[i][2], posN[i][3],
				posN[i][4], posN[i][5]);
	}
}

void fillWindowingArray(int **posN, int nwindow,
		int beg_pos, int end_pos, int spansize, int spanfilter)
{
	int firstkey, lastkey;
	int jump, begdata, enddata;
	int ibeg, iend, ff, fl;
	int i, ncount;

	firstkey = beg_pos;
	lastkey = end_pos;

	jump = (spanfilter*2) + 1;
	begdata = firstkey + spansize;
	enddata = lastkey - spansize;
	ff = begdata + spansize;
	fl = begdata - spanfilter - 1;

	ncount = 0;
	posN[ncount][0] = firstkey;
	posN[ncount][1] = ff;
	posN[ncount][2] = firstkey;
	posN[ncount][3] = fl;
	posN[ncount][4] = posN[ncount][2] - posN[ncount][0];
	posN[ncount][5] = posN[ncount][4] + posN[ncount][3] - posN[ncount][2];

	for ( i=begdata; i<enddata; i=i+jump )
	{
		ncount++;

		ibeg = i-spansize;
		iend = i+spansize;
		ff = i-spanfilter;
		fl = i+spanfilter;

		posN[ncount][0] = ibeg;
		posN[ncount][1] = iend;
		posN[ncount][2] = ff;
		posN[ncount][3] = fl;
		posN[ncount][4] = posN[ncount][2] - posN[ncount][0];
		posN[ncount][5] = posN[ncount][4] + posN[ncount][3] - posN[ncount][2];
	}

	ncount++;
	ff = i-spansize;
	fl = fl + 1;

	posN[ncount][0] = ff;
	posN[ncount][1] = lastkey;
	posN[ncount][2] = fl;
	posN[ncount][3] = lastkey;
	posN[ncount][4] = posN[ncount][2] - posN[ncount][0];
	posN[ncount][5] = posN[ncount][4] + posN[ncount][3] - posN[ncount][2];
}

int nWindowingCount(int beg_pos, int end_pos, int spansize, int spanfilter)
{
	int firstkey, lastkey;
	int jump, begdata, enddata;
	int ibeg, iend, ff, fl;
	int i, ncount;

	firstkey = beg_pos;
	lastkey = end_pos;

	jump = (spanfilter*2) + 1;
	begdata = firstkey + spansize;
	enddata = lastkey - spansize;
	ff = begdata + spansize;
	fl = begdata - spanfilter - 1;

	ncount = 0;

	for ( i=begdata; i<enddata; i=i+jump )
	{
		ncount++;

		ibeg = i-spansize;
		iend = i+spansize;
		ff = i-spanfilter;
		fl = i+spanfilter;
	}

	ncount++;
	ff = i-spansize;
	fl = fl + 1;
	return(ncount+1);
}

void addDataf1(float **A, int a1, int a2,
		float **B, int b11, int b12, int b21, int b22)
{
	int i, j;
	int itr1, itr2;

	itr2 = a2;
	for(i=b21; i<=b22; i++)
	{
		itr1 = a1;
		for(j=b11; j<=b12; j++)
		{
			A[itr2][itr1] += B[i][j];
			itr1++;
		}
		itr2++;
	}
}

void addDataf(float **data, float **procData, int posX1, int posX2, int posY1, int posY2)
{
	int i, j;
	int ix, iy;

	iy = 0;
	for(i=posY1; i<=posY2; i++)
	{
		ix = 0;
		for(j=posX1; j<=posX2; j++)
		{
			data[i][j] += procData[iy][ix];
			ix++;
		}
		iy++;
	}
}

void addDatai(int **data, int posX1, int posX2, int posY1, int posY2)
{
	int i, j;

	for(i=posY1; i<=posY2; i++)
		for(j=posX1; j<=posX2; j++)
			data[i][j] += 1;
}

float **getData(float **data, int ntrc, int nsp,
		int posX1, int posX2, int posY1, int posY2,
		int *nx, int *ny)
{
	int i, j;
	int inx, iny;
	int ix, iy;
	float **outdata=NULL;

	inx = posX2-posX1+1;
	iny = posY2-posY1+1;
	outdata = alloc2float(inx, iny);
	iy = 0;
	for(i=posY1; i<=posY2; i++)
	{
		ix = 0;
		for(j=posX1; j<=posX2; j++)
		{
			outdata[iy][ix] = data[i][j];
			ix++;
		}
		iy++;
	}
	(*nx) = inx;
	(*ny) = iny;
	return(outdata);
}

bool getpos1pos2(int nsp, int icenter, int nspan, int nsfilt,
		int lastspan1, int lastifilt1,
		int *sp1, int *sp2, int *ss1, int *ss2)
{
	int pos1, pos2;
	int poss1, poss2;
	bool bloop;
	bloop = true;

	if(icenter <= 0)
	{
		pos1 = 0;
		pos2 = pos1 + nspan-1;
		poss1 = 0;
		poss2 = poss1+nsfilt-1;

	}
	else
	{
		pos1 = icenter-nspan;
		if(pos1<0)	pos1 = 0;
		pos2 = icenter + nspan-1;


		poss1 = icenter-nsfilt;
		if(poss1<0)	poss1 = 0;
		poss2 = icenter+nsfilt-1;

	}
	if(pos2>=nsp-1)	{
		pos2 = nsp-1;
		bloop = false;
	}
	if(poss2>=nsp-1) {
		poss2 = nsp-1;
		bloop = false;
	}

	(*sp1) = pos1;
	(*sp2) = pos2;
	(*ss1) = poss1;
	(*ss2) = poss2;
	return(bloop);
}
