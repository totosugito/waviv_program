#include "wvfxssa.h"

void run_cadzow_v3(float **fdata, int ntrc, int nsp,
		int rank, int spatspan, int tempspan, float fftpad, float epsilon,
		int pocs, int maxiter, int interp, int old, int dealiased)
{
	int i,j;
	int spatjump,tempjump;
	int ntraces,ns;
	int ntracespanel,nspanel;
	int spatwin,tempwin;
	double **data;

	spatwin=(spatspan*2)+1;
	tempwin=(tempspan*2)+1;
	ntraces=ntrc;
	ns=nsp;
	ntracespanel=ntraces + spatspan;
	nspanel=ns + tempspan;
	spatjump=spatspan+1;
	tempjump=tempspan+1;

	int nspatjump = floor(ntracespanel/spatjump);
	int lastspatdex=nspatjump*spatjump;
	ntracespanel=lastspatdex+spatspan+spatjump;
	nspatjump+=1;
	lastspatdex+=spatjump;

	int ntempjump= floor(nspanel/tempjump);
	int lasttempdex=ntempjump*tempjump;
	nspanel=lasttempdex+tempspan+tempjump;
	ntempjump+=1;
	lasttempdex+=tempjump;

	data = alloc2double(nsp, ntrc);
	for (i=0; i<ntrc; i++)
		for(j=0; j<nsp; j++)
			data[i][j] = (double) fdata[i][j];

	double** panel = (double**) ealloc2double(nspanel,ntracespanel);
	double** panel_filter = (double**) ealloc2double(nspanel,ntracespanel);
	double** window = (double**) ealloc2double(tempwin,spatwin);
	double** windowoldreal = (double**) ealloc2double(tempwin,spatwin);
	double** windowoldimag = (double**) ealloc2double(tempwin,spatwin);
	double** windowold = (double**) ealloc2double(tempwin,spatwin);
	double** window_filtered = (double**) ealloc2double(tempwin,spatwin);
	double** windowreal = (double**) ealloc2double(tempwin,spatwin);
	double** windowimag = (double**) ealloc2double(tempwin,spatwin);

	double** windowreal_precond = (double**) ealloc2double(tempwin*2,spatwin);
	double** windowimag_precond = (double**) ealloc2double(tempwin*2,spatwin);

	double** windowrealcadzow = (double**) ealloc2double(tempwin,spatwin);
	double** windowimagcadzow = (double**) ealloc2double(tempwin,spatwin);
	double** trimaskspat = (double**) weightmaskspat(spatwin, nspanel);
	double** panel_filter_window = (double**) ealloc2double(nspanel,spatwin);
	double** trimaskspattemp = (double**) weightmasktemp(spatwin, tempwin);
	int* OBS_PAD = calloc(ntracespanel,sizeof(int));
	int* OBS_W = (int*) calloc(spatwin,sizeof(int));

	fill2d(panel,0.0,ntracespanel,nspanel);
	fill2d(panel_filter,0.0,ntracespanel,nspanel);
	for(i=0;i<ntraces;i++)
		for(j=0;j<ns;j++)
			panel[i+spatspan][j+tempspan]=data[i][j];

	int cns=floor(nspanel/2)+1;
	for(i=spatspan;i<lastspatdex;i+=spatjump)
	{
		fill2d(panel_filter_window,0.0,spatwin,nspanel);
		get1dsubmatrix(OBS_W, OBS_PAD,i-spatspan, i+spatspan);
		for(j=tempspan;j<lasttempdex;j+=tempjump)
		{
			getsubmatrix(panel,window,j,i,tempspan,spatspan);
			if(interp==0 && old==0 && dealiased==0 )
			{
				ffttemporal(window, windowreal, windowimag, spatwin, tempwin,fftpad);
				cadzow2d(windowreal,windowrealcadzow,spatwin,tempwin,0,tempwin,rank,maxiter);
				cadzow2d(windowimag,windowimagcadzow,spatwin,tempwin,0,tempwin,rank,maxiter);
				iffttemporal(window_filtered, windowrealcadzow, windowimagcadzow, spatwin, tempwin,fftpad);
			}
			else if(interp==1 &&  old==0 && dealiased==0)
			{
				ffttemporal(window, windowreal, windowimag, spatwin, tempwin,fftpad);
				cadzow2dint(windowreal,windowrealcadzow,OBS_W,spatwin,tempwin,0,tempwin,rank,pocs,maxiter);
				cadzow2dint(windowimag,windowimagcadzow,OBS_W,spatwin,tempwin,0,tempwin,rank,pocs,maxiter);
				iffttemporal(window_filtered,windowrealcadzow,windowimagcadzow,spatwin,tempwin,fftpad);
			}
			else if(interp==0 &&  old==1 && dealiased==0)
			{
				ffttemporal(window, windowreal, windowimag, spatwin, tempwin,fftpad);
				cadzow2dold(windowreal,windowrealcadzow,spatwin,tempwin,0,tempwin,rank);
				cadzow2dold(windowimag,windowimagcadzow,spatwin,tempwin,0,tempwin,rank);
				iffttemporal(window_filtered,windowrealcadzow,windowimagcadzow,spatwin,tempwin,fftpad);
			}
			else if(interp==1 &&  old==0 && dealiased==1)
			{
				ffttemporal(window, windowreal, windowimag, spatwin, tempwin,fftpad);
				preparepreconditioner(window, windowreal_precond, windowimag_precond,spatwin,tempwin);
				dealiasedcadzow2dint(windowreal,windowreal_precond,windowrealcadzow,OBS_W,spatwin,tempwin,0,tempwin*epsilon,rank,maxiter);
				dealiasedcadzow2dint(windowimag,windowimag_precond,windowimagcadzow,OBS_W,spatwin,tempwin,0,tempwin*epsilon,rank,maxiter);
				clearnand2(windowrealcadzow,spatwin,tempwin);
				clearnand2(windowimagcadzow,spatwin,tempwin);
				iffttemporal(window_filtered,windowrealcadzow,windowimagcadzow,spatwin,tempwin,fftpad);
				clearnand2(window_filtered,spatwin,tempwin);
			}
			mask(window_filtered,trimaskspattemp,spatwin,tempwin);
			addsubmatrix(panel_filter_window,window_filtered,j,(spatwin-1)/2,tempspan,spatspan);
		}
		mask(panel_filter_window,trimaskspat, spatwin,nspanel);
		addsubmatrix(panel_filter,panel_filter_window, cns, i, nspanel/2,spatspan);
	}

	for(i=0;i<ntraces;i++)
		for(j=0;j<ns;j++)
			fdata[i][j]= (float) (panel_filter[i+spatspan][j+tempspan]);

	free2double(data);
	free2double(panel);
	free2double(panel_filter);
	free2double(window);
	free2double(windowoldreal);
	free2double(windowoldimag);
	free2double(windowold);
	free2double(window_filtered);
	free2double(windowreal);
	free2double(windowimag);
	free2double(windowreal_precond);
	free2double(windowimag_precond);
	free2double(windowrealcadzow);
	free2double(windowimagcadzow);
	free2double(trimaskspat);
	free2double(trimaskspattemp);
	free2double(panel_filter_window);
	free(OBS_W);
	free(OBS_PAD);
}

void preparepreconditioner(double** datain, double** realout, double** imagout, int row,int col){
	double** dataprecond = (double**) ealloc2double(col,row);
	fill2d(dataprecond,0.0, row,col);
	fill2d(realout,0.0, row,col);
	fill2d(imagout,0.0, row,col);
	int i;
	for(i=0;i<=row/2;i++)
		memcpy(dataprecond[i],datain[i*2],col*sizeof(double));
	ffttemporal(dataprecond, realout, imagout, row, col,0.000);
	free2double(dataprecond);
}

double** weightmaskspat(int row, int col){
	float* mask1 = (float*) trianglekernel(row);
	double** mask = (double**) ealloc2double(col,row);
	int i,j;
	for(i=0;i<row;i++)
		for(j=0;j<col;j++)mask[i][j]=(double)mask1[i];

	free(mask1);
	return mask;
}

double** weightmasktemp(int row, int col){
	float* mask1 = (float*) trianglekernel(col);
	double** mask = (double**) ealloc2double(col,row);
	int i,j;
	for(i=0;i<row;i++)
		for(j=0;j<col;j++)mask[i][j]=(double)mask1[j];
	free(mask1);
	return mask;
}

void cadzow2d(double** data,double** outdata, int row, int col, int nbeg, int nend,int rank,int maxiter){
	int f;	
	int iter;
	double a=1;
	double v = (double)maxiter;
	double da=1.0/v;
	double* sold = (double*) malloc(row*sizeof(double));
	double* fsliced = (double*) malloc(row*sizeof(double));
	for(f=nbeg;f<nend;f++){
		a=1;
		getcold(data,f,fsliced,row);
		getcold(data,f,sold,row);
		for(iter=0;iter<maxiter;iter++){
			hankel1d* hankel = (hankel1d*) createhankel1d(fsliced,row);
			double** filter = (double**) rankApproximation(hankel->data,hankel->m,hankel->l,rank);
			memcpy(hankel->data[0],filter[0],hankel->m*hankel->l*sizeof(double));	
			double* invhankel = (double*) inverseHankel(hankel);
			reinsertamp(invhankel, sold,a,row);
			a-=da;
			memcpy(fsliced,invhankel,row*sizeof(double));
			free(invhankel);
			free2double(filter);
			freehankel1d(hankel);
		}
		setcold(outdata,f,fsliced,row);
	}	
	free(fsliced);
	free(sold);
}

void cadzow2dold(double** data,double** outdata, int row, int col, int nbeg, int nend,int rank){
	int f;	
	double* fsliced = (double*) malloc(row*sizeof(double));
	for(f=nbeg;f<nend;f++){
		getcold(data,f,fsliced,row);
		hankel1d* hankel = (hankel1d*) createhankel1d(fsliced,row);
		double** filter = (double**) rankApproximation(hankel->data,hankel->m,hankel->l,rank);
		memcpy(hankel->data[0],filter[0],hankel->m*hankel->l*sizeof(double));	
		double* invhankel = (double*) inverseHankel(hankel);
		free(invhankel);
		free2double(filter);
		freehankel1d(hankel);
		setcold(outdata,f,invhankel,row);
	}	
	free(fsliced);
}

void cadzow2dint(double** data,double** outdata,int* OBS,int row, int col, int nbeg, int nend,int rank, int pocs, int maxiter){
	int f,x;
	int iter;
	double a=1;
	double v = (double)maxiter;
	double da=1.0/v;
	double* sobs = (double*) calloc(row,sizeof(double));
	double* sold = (double*) calloc(row,sizeof(double));
	double* Sv = (double*) calloc(row,sizeof(double));
	for(f=nbeg;f<nend;f++){
		double mean=0;
		for(x=0;x<row;x++)mean+=data[x][f];
		mean/=(double)row;
		
		for(x=0;x<row;x++){
			if(data[x][f]<mean)
				data[x][f]=mean;
		}

		a=1;
		getcold(data,f,sobs,row);
		getcold(data,f,sold,row);
		for(iter=0;iter<maxiter;iter++){
			hankel1d* hankel = (hankel1d*) createhankel1d(sobs,row);
			double** filter = (double**) rankApproximation(hankel->data,hankel->m,hankel->l,rank);
			memcpy(hankel->data[0],filter[0],hankel->m*hankel->l*sizeof(double));	
			double* FS = (double*) inverseHankel(hankel);
			if(pocs!=1)reinsert(Sv,FS,sold,OBS,a,row);
			if(pocs==1)reinsert_pocs(Sv,FS,sold, OBS,a,row);
			a-=da;
			memcpy(sobs,Sv,row*sizeof(double));
			freehankel1d(hankel);
			free(FS);
			free2double(filter);
		}
		setcold(outdata,f,Sv,row);
	}
	free(sobs);
	free(sold);
	free(Sv);
}

void dealiasedcadzow2dint(double** data,double** dataprecond,double** outdata,int* OBS,int row, int col, int nbeg, int nend,int rank, int maxiter){
	int f,iter;	
	double a=1;
	double* fsliced = (double*) calloc(row,sizeof(double));
	double* fslicedprecond = (double*) calloc(row,sizeof(double));
	double* sold = (double*) calloc(row,sizeof(double));
	for(f=nbeg;f<nend;f++){
		getcold(data,f,sold,row);
		getcold(data,f,fsliced,row);
		getcold(dataprecond,f,fslicedprecond,row);
		for(iter=0;iter<maxiter;iter++){
			hankel1d* hankel = (hankel1d*) createhankel1d(fsliced,row);
			hankel1d* hankelprecond = (hankel1d*) createhankel1d(fslicedprecond,row);
			double** mult = (double**) ealloc2double(hankelprecond->l,hankelprecond->m);
			double** prec = (double**) getprecond(hankelprecond->data,hankelprecond->m,hankelprecond->l,rank);
			matmul(prec, hankel->data, mult, hankelprecond->m, hankelprecond->l,hankel->m,hankel->l);
			memcpy(hankel->data[0],mult[0],hankel->m*hankel->l*sizeof(double));
			double* invhankel = (double*) inverseHankel(hankel);
			reinsert_pocs(fsliced,invhankel, sold, OBS,a,row);
			free(invhankel);
			free2double(prec);
			free2double(mult);
			freehankel1d(hankel);
			freehankel1d(hankelprecond);
		}
		setcold(outdata,f,fsliced,row);
	}	
	free(fsliced);
	free(fslicedprecond);
	free(sold);
}

void svd_inv(double** data, int m, int n, double** S, double** U, double** VT){
	int i,j,r;
	double** temp = (double**) ealloc2double(n,m);
	fill2d(temp,0.0,m,n);
	fill2d(data,0.0,m,n);

	for(i=0;i<m;i++){
		for(j=0;j<n;j++){
			for(r=0;r<n;r++){
				temp[i][j]+=(S[i][r]*VT[r][j]);
			}
		}
	}

	for(i=0;i<m;i++){
		for(j=0;j<n;j++){
			for(r=0;r<m;r++){
				data[i][j]+=(U[i][r]*temp[r][j]);
			}
		}
	}
	free2double(temp);
}

void svd(double** data, int row, int col, double** S, double** U, double** VT){
	int m=row,n=col,lda=n,ldu=m,ldvt=n,info;
	double superb[min(m,n)-1];
	int scnt = min(m,n);
	double* aloc = (double*) malloc(lda*m*sizeof(double));
	double* s = (double*) malloc(n*sizeof(double));
	double* u = (double*) malloc(ldu*m*sizeof(double));
	double* vt = (double*) malloc(ldvt*n*sizeof(double));

	int i;
	for(i=0;i<m;i++){
		memcpy(&aloc[i*lda],data[i],lda*sizeof(double));
	}

    info = LAPACKE_dgesvd( LAPACK_ROW_MAJOR, 'A', 'A', m, n, aloc, lda,
                        s, u, ldu, vt, ldvt, superb );

    if( info > 0 ) {
         printf( "The algorithm computing SVD failed to converge.\n" );
          exit( 1 );
    }
	
	for(i=0;i<m;i++){
		memcpy(U[i],&u[i*m],m*sizeof(double));
	}
	
	for(i=0;i<n;i++){
		memcpy(VT[i],&vt[i*n],n*sizeof(double));
	}

	for(i=0;i<scnt;i++){
		S[i][i]=s[i];
	}

	free(aloc);
	free(s);
	free(u);
	free(vt);
}

double** rankApproximation(double** data,int row,int col, int rank){
	double** u = (double**)ealloc2double(row,row);
	double** s = (double**)ealloc2double(col,row);
	double** v = (double**)ealloc2double(col,col);
	
	svd(data, row, col, s, u, v);
	
	double** out = (double**)ealloc2double(col,row);
	int iter,i,j;
	for(i=0;i<row;i++){
		for(j=0;j<col;j++){
			out[i][j]=0.0;
		}
	}
	for(iter=0;iter<rank;iter++){
		double** eigenimage = getrank(u,s,v,row,col,iter);
		for(i=0;i<row;i++){
			for(j=0;j<col;j++){
				out[i][j]+=eigenimage[i][j];
			}
		}
		free2double(eigenimage);
	}
	
	free2double(u);
	free2double(s);
	free2double(v);
	
	return out;
}

double** getrank(double** u,double** s,double** v, int row, int col, int rank){
	double** su = (double**)ealloc2double(col,row);
	int i,j;
	for(i=0;i<row;i++){
		for(j=0;j<col;j++){
			su[i][j]=(v[rank][j]*u[i][rank]*s[rank][rank]);
		}
	}
	return su;
}

void ffttemporal(double** inputtx, double** outputreal, double** outputimag, int ntraces, int ns,float pad){
	/*
		inputtx[x][t] 		--> inputtx[ntrace][ns]
		data[x][2]  			--> data[ns][0=real,1=imaginer]
		outputreal[x][f] 	--> outputreal[trace][nsample]
	*/
	int SIZE=ns;
	SIZE+=floor(pad*ns);
	fftw_complex    *data, *fft_result;
	fftw_plan       plan_forward;
  
	data        = ( fftw_complex* ) fftw_malloc( sizeof( fftw_complex ) * SIZE );
	fft_result  = ( fftw_complex* ) fftw_malloc( sizeof( fftw_complex ) * SIZE );
	plan_forward  = fftw_plan_dft_1d( SIZE, data, fft_result, FFTW_FORWARD, FFTW_ESTIMATE );
	int x, f,t;

	for(x=0;x<ntraces;x++){
		for( t = 0 ; t < SIZE ; t++ ){
			data[t][0] = inputtx[x][t];
			data[t][1] = 0.0;
		}
		for( t = 0 ; t < ns ; t++ ) {
			data[t][0] = inputtx[x][t];
			data[t][1] = 0.0;
		}
		fftw_execute( plan_forward );
		for( f = 0 ; f < ns ; f++ ) {
			outputreal[x][f] = fft_result[f][0]/SIZE;
			outputimag[x][f] = fft_result[f][1]/SIZE;
		}
	}

	/* free memory */
	fftw_destroy_plan( plan_forward );
	fftw_free( data );
	fftw_free( fft_result );	
}

void iffttemporal(double** outputtx, double** inputreal, double** inputimag, int ntraces, int ns,float pad){
	/*
		input[x][f] 		--> outputtx[ntrace][ns]
		data[x][2]  			--> data[ns][0=real,1=imaginer]
		outputreal[x][t] 	--> outputreal[trace][nsample]
	*/ 
	int SIZE=ns;
	SIZE+=floor(pad*ns);
	fftw_complex    *ifft_result,*fft_result;
	fftw_plan       plan_backward;
  
	fft_result  = ( fftw_complex* ) fftw_malloc( sizeof( fftw_complex ) * SIZE );
	ifft_result = ( fftw_complex* ) fftw_malloc( sizeof( fftw_complex ) * SIZE );
	plan_backward = fftw_plan_dft_1d( SIZE, fft_result, ifft_result, FFTW_BACKWARD, FFTW_ESTIMATE );
	int f, x,t;

	for(x=0;x<ntraces;x++){
		for( f = 0 ; f < SIZE ; f++ ){
			fft_result[f][0]=0.0;
			fft_result[f][1]=0.0;
		}
		for( f = 0 ; f < ns ; f++ ) {
			fft_result[f][0]=inputreal[x][f];
			fft_result[f][1]=inputimag[x][f];
		}
		fftw_execute( plan_backward );

		for( t = 0 ; t < ns ; t++ ) {
			outputtx[x][t] = ifft_result[t][0]/SIZE;
		}
	}
	fftw_destroy_plan( plan_backward );
	fftw_free( ifft_result );
	fftw_free( fft_result );
}

void fftspatial(double** inputreal,double** inputimag, double** outputreal, double** outputimag, int ntraces, int ns,float pad){
	/*
		input[x][f] 		--> input[ntrace][ns]
		data[x][2]  		--> data[ns][0=real,1=imaginer]
		outputreal[k][f] 	--> outputreal[trace][nsample]
	*/
	int SIZE=ntraces;
	SIZE+=floor(pad*ntraces);
	
	fftw_complex    *data, *fft_result;
	fftw_plan       plan_forward;
  
	data        = ( fftw_complex* ) fftw_malloc( sizeof( fftw_complex ) * SIZE );
	fft_result  = ( fftw_complex* ) fftw_malloc( sizeof( fftw_complex ) * SIZE );
	plan_forward  = fftw_plan_dft_1d( SIZE, data, fft_result, FFTW_FORWARD, FFTW_ESTIMATE );
	int f,x,k;

	for(f=0;f<ns;f++){
		for( x = 0 ; x < SIZE ; x++ ) {
			data[x][0] = 0.0;
			data[x][1] = 0.0;
		}
		
		for( x = 0 ; x < ntraces ; x++ ) {
			data[x][0] = inputreal[x][f];
			data[x][1] = inputimag[x][f];
		}
		fftw_execute( plan_forward );
		for( k = 0 ; k < ntraces ; k++ ) {
			outputreal[k][f] = fft_result[k][0];
			outputimag[k][f] = fft_result[k][1];
		}
	}

	/* free memory */
	fftw_destroy_plan( plan_forward );
	fftw_free( data );
	fftw_free( fft_result );
		
}

void ifftspatial(double** outputreal,double** outputimag, double** inputreal, double** inputimag, int ntraces, int ns,float pad){
	/*
		input[k][f] 		--> output[ntrace][ns]
		data[x][2]  			--> data[ns][0=real,1=imaginer]
		outputreal[x][f] 	--> outputreal[trace][nsample]
	*/ 
	int SIZE=ntraces;
	SIZE+=floor(pad*ntraces);
	
	fftw_complex    *ifft_result,*fft_result;
	fftw_plan       plan_backward;
  
	fft_result  = ( fftw_complex* ) fftw_malloc( sizeof( fftw_complex ) * SIZE );
	ifft_result = ( fftw_complex* ) fftw_malloc( sizeof( fftw_complex ) * SIZE );
	plan_backward = fftw_plan_dft_1d( SIZE, fft_result, ifft_result, FFTW_BACKWARD, FFTW_ESTIMATE );
	int k, x,f;

	for(f=0;f<ns;f++){
		for( k = 0 ; k < SIZE ; k++ ) {
			fft_result[k][0]=0.0;
			fft_result[k][1]=0.0;
		}
		for( k = 0 ; k < ntraces ; k++ ) {
			fft_result[k][0]=inputreal[k][f];
			fft_result[k][1]=inputimag[k][f];
		}
		fftw_execute( plan_backward );
		for( x = 0 ; x < ntraces ; x++ ) {
			outputreal[x][f] = ifft_result[x][0]/SIZE;
			outputimag[x][f] = ifft_result[x][1]/SIZE;
		}
	}
	fftw_destroy_plan( plan_backward );
	fftw_free( ifft_result );
	fftw_free( fft_result );
}

void suifktransform(double** output,dcomplex** input,int ntraces,int ns,float pad){
	double** fxr = (double**) ealloc2double(ns,ntraces);
	double** fxi = (double**) ealloc2double(ns,ntraces);
	double** fkr = (double**) ealloc2double(ns,ntraces);
	double** fki = (double**) ealloc2double(ns,ntraces);
	int tr,t;
	for(tr=0;tr<ntraces;tr++){
		for(t=0;t<ns;t++){
			fkr[tr][t]=input[tr][t].r;
			fki[tr][t]=input[tr][t].i;
		}
	}
	ifftspatial(fxr,fxi,fkr,fki,ntraces,ns,pad);
	iffttemporal(output, fxr, fxi, ntraces, ns,pad);
	free2double(fxi);
	free2double(fxr);
	free2double(fki);
	free2double(fkr);
}

void sufktransform(double** input,dcomplex** output ,int ntraces,int ns,float pad){
	double** fxr = (double**) ealloc2double(ns,ntraces);
	double** fxi = (double**) ealloc2double(ns,ntraces);
	double** fkr = (double**) ealloc2double(ns,ntraces);
	double** fki = (double**) ealloc2double(ns,ntraces);
	ffttemporal(input, fxr, fxi, ntraces, ns,pad);
	fftspatial(fxr,fxi,fkr,fki,ntraces,ns,pad);
	int tr,t;	
	for(tr=0;tr<ntraces;tr++){
		for(t=0;t<ns;t++){
			output[tr][t].r=fkr[tr][t];
			output[tr][t].i=fki[tr][t];
		}
	}
	free2double(fxi);
	free2double(fxr);
	free2double(fki);
	free2double(fkr);
}

void reinsert(double* sv1,double* sv0, double* sobs, int* OBS,double a,int n){
	int i;
	for(i=0;i<n;i++){
		sv1[i]=a*sobs[i];
		sv1[i]+= (( 1.0 - a ) * (double)OBS[i] * sv0[i]);
		sv1[i]+= (( 1.0 - (double) OBS[i] ) *  sv0[i]);
	}
}

void reinsertamp(double* sv0, double* sobs,double a,int n){
	int i;
	for(i=0;i<n;i++){
		sv0[i] = (( 1.0 - a ) * sv0[i]);
		sv0[i]+= a*sobs[i];
	}
}

void reinsert_pocs(double* sv1,double* sv0, double* sobs, int* OBS,double a,int n){
	int i;
	for(i=0;i<n;i++){
		sv1[i]=sobs[i] + ( (1.0-(double)OBS[i]) * sv0[i] ) ;
	}
}

void suamp(dcomplex** input, double** output,int row,int col){
	int i,j;
	for(i=0;i<row;i++){
		for(j=0;j<col;j++){
			output[i][j] = sqrt( (input[i][j].r*input[i][j].r) + (input[i][j].i*input[i][j].i) );
		}
	}
}

void norm2max2dc(dcomplex** data, int row,int col){
	double** temp = (double**) ealloc2double(col,row);
	suamp(data, temp, row, col);
	double max = Max2dd(temp,  row, col);
	max = 1.0 /max;
	int i,j;
	for(i=0;i<row;i++){
		for(j=0;j<col;j++){
			data[i][j] = dcrmul(data[i][j],max);
		}
	}
	free2double(temp);
}

void savearray2dc(char* outfilename,dcomplex** array,int row,int col){
	FILE* outfileptr = efopen(outfilename,"w");
	double** amp = (double**) ealloc2double(col,row);
	float* outarray = (float*) calloc(col,sizeof(float));
	suamp(array,amp, row, col);

	int i,j;
	for(i=0;i<row;i++){
		for(j=0;j<col;j++){
			outarray[j]=amp[i][j];
		}
		efwrite(outarray,1,col*sizeof(float),outfileptr);
	}
	free(outarray);
	free2double(amp);
	fclose(outfileptr);
}

double** getprecond(double** data,int row,int col, int rank){
	double** u = (double**)ealloc2double(row,row);
	double** s = (double**)ealloc2double(col,row);
	double** v = (double**)ealloc2double(col,col);
	svd(data, row, col, s, u, v);
	
	
	double** out = (double**)ealloc2double(col,row);
	double** Uk = (double**)ealloc2double(rank,row);
	double** Ukh = (double**)ealloc2double(row,rank);
	int i,j;
	fill2d(out,0.0,row, col);

	for(i=0;i<row;i++){
		for(j=0;j<rank;j++){
			Uk[i][j]=u[i][j];
			Ukh[j][i]=u[i][j];
		}
	}
	
	matmul(Uk, Ukh, out, row, rank,rank,row);
	
	free2double(u);
	free2double(s);
	free2double(v);

	free2double(Uk);
	free2double(Ukh);
	
	return out;
}
