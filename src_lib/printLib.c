/*
 * printLib.c
 *
 *  Created on: Apr 13, 2012
 *      Author: toto
 */

#include "printLib.h"

void print1int(int *data, int ndata, bool ptype, bool showlineNumber, bool dispType)
{
	/*
	 * ptype = print type
	 *         0 = use printf
	 *         1 = use fprintf
	 */

	int i;

	for (i=0; i<ndata; i++)
	{
		if(ptype)
		{
			if(showlineNumber)
				fprintf(stderr, "%i. %i ", i, data[i]);
			else
				fprintf(stderr, "%i ", data[i]);

			if(dispType==0)
				fprintf(stderr,"\n");
			else
				fprintf(stderr,"\t");
		}
		else
		{
			if(showlineNumber)
				printf("%i. %i ", i, data[i]);
			else
				printf("%i ", data[i]);

			if(dispType==0)
				printf("\n");
			else
				printf("\t");
		}

	}

	if(ptype) fprintf(stderr, "\n");
	else  printf("\n");
}

void print2int(int **data, int nrow, int ncol, bool ptype, bool showlineNumber)
{
	/*
	 * ptype = print type
	 *         0 = use printf
	 *         1 = use fprintf
	 */

	int i, j;

	for (i=0; i<nrow; i++)
	{
		for (j=0; j<ncol; j++)
		{
			if(ptype)
			{
				if(showlineNumber)
					fprintf(stderr, "[%i, %i]. %i \n", i, j, data[i][j]);
				else
					fprintf(stderr, "%i \n", data[i][j]);
			}
			else
			{
				printf("%i ", data[i][j]);
			}
		}
		if(ptype==0)
			printf("\n");
	}

	if(ptype) fprintf(stderr, "\n");
	else  printf("\n");
}

void print1float(float *data, int ndata, bool ptype, bool showlineNumber, bool dispType)
{
	/*
	 * ptype = print type
	 *         0 = use printf
	 *         1 = use fprintf
	 */

	int i;

	for (i=0; i<ndata; i++)
	{
		if(ptype)
		{
			if(showlineNumber)
				fprintf(stderr, "%i. %5.3f", i, data[i]);
			else
				fprintf(stderr, "%5.3f", data[i]);

			if(dispType==0)
				fprintf(stderr,"\n");
			else
				fprintf(stderr,"\t");
		}
		else
		{
			if(showlineNumber)
				printf("%i. %5.3f \n", i, data[i]);
			else
				printf("%5.3f \n", data[i]);

			if(dispType==0)
				printf("\n");
			else
				printf("\t");
		}


	}

	if(ptype) fprintf(stderr, "\n");
	else  printf("\n");
}

void print2float(float **data, int nrow, int ncol, bool ptype, bool showlineNumber)
{
	/*
	 * ptype = print type
	 *         0 = use printf
	 *         1 = use fprintf
	 */

	int i, j;

	for (i=0; i<nrow; i++)
	{
		for (j=0; j<ncol; j++)
		{
			if(ptype)
			{
				if(showlineNumber)
					fprintf(stderr, "[%i, %i]. %5.3f \n", i, j, data[i][j]);
				else
					fprintf(stderr, "%i \n", data[i][j]);
			}
			else
			{
				if(showlineNumber)
					fprintf(stderr, "[%i, %i]. %5.3 \n", i, j, data[i][j]);
				else
					printf("%i \n", data[i][j]);
			}
		}
	}

	if(ptype) fprintf(stderr, "\n");
	else  printf("\n");
}

void print2double(double **data, int nrow, int ncol, bool ptype, bool showlineNumber)
{
	/*
	 * ptype = print type
	 *         0 = use printf
	 *         1 = use fprintf
	 */

	int i, j;

	for (i=0; i<nrow; i++)
	{
		for (j=0; j<ncol; j++)
		{
			if(ptype)
			{
				if(showlineNumber)
					fprintf(stderr, "[%i, %i]. %5.3f \n", i, j, data[i][j]);
				else
					fprintf(stderr, "%i \n", data[i][j]);
			}
			else
			{
				if(showlineNumber)
					fprintf(stderr, "[%i, %i]. %5.3 \n", i, j, data[i][j]);
				else
					printf("%i \n", data[i][j]);
			}
		}
	}

	if(ptype) fprintf(stderr, "\n");
	else  printf("\n");
}

void print1f(char *cname, float *X, int nx)
{
	int i;
	fprintf(stderr, "\nName=%s (%i)\n", cname, nx);
	for(i=0; i<nx; i++)
		fprintf(stderr, "%3.5f \n", X[i]);
	fprintf(stderr, "\n");
}

void print1i(char *cname, int *X, int nx)
{
	int i;
	fprintf(stderr, "\nName=%s (%i)\n", cname, nx);
	for(i=0; i<nx; i++)
		fprintf(stderr, "%i \n", X[i]);
	fprintf(stderr, "\n");
}

void print2f(char *cname, float **X, int nx, int ny)
{
	int i, j;
	fprintf(stderr, "\nName=%s (%i, %i)\n", cname, nx, ny);
	for(i=0; i<nx; i++)
	{
		for(j=0; j<ny; j++)
		{
			fprintf(stderr, "%e  ", X[i][j]);
		}
		fprintf(stderr, "\n");
	}
	fprintf(stderr, "\n");
}
