#include <cwp.h>
#include <su.h>
#include <segy.h>

typedef struct {
	int m,l;//m:row l:col
	double **data;
} hankel1d;

typedef struct {
    int s; //subhankel
    int m;
    int l;
    int ss;
    int mm;
    int ll;
    int hm;
    int hl;
	double** data;
} hankel2d;

void getcol(float** input, int colselect, float* coldata, int nrow);
void setcol(float** input, int colselect, float* coldata, int nrow);
void getrow(float** input, int rowselect, float* rowdata, int ncol);
void setrow(float** input, int rowselect, float* rowdata, int ncol);

void getcold(double** input, int colselect, double* coldata, int nrow);
void setcold(double** input, int colselect, double* coldata, int nrow);
void getrowd(double** input, int rowselect, double* rowdata, int ncol);
void setrowd(double** input, int rowselect, double* rowdata, int ncol);

float** ftranspose(float** data, int row, int col);
double** dtranspose(double** data, int row, int col);

double* todouble1d(float* data, int n);
float* tofloat1d(double* data, int n);
double** todouble2d(float** data, int row, int col);
float** tofloat2d(double** data, int row, int col);

void fill1f(float* data,float val, int len);
void fill1d(double* data,double val, int len);

void fill2d(double** data,double val, int row,int col);

void taper1dlin(float* data, int ns, int s);

hankel1d* createhankel1d(double* data, int n);
double* inverseHankel(hankel1d* hankel);
double** inverseHankel2D(hankel2d* hankel);
void freehankel1d(hankel1d* hankel);

hankel2d* createhankel2d(double** data, int rowin, int colin);
hankel1d** hankelarray(int row, int col);
double* average(double** data, int m, int l);
void stack(double* d1,double* d2,int l);
void divarr(double* d1,double div, int l);

void writearray(FILE* outfileptr,float** data, int n1, int n2);

void convolve1d(float* function, int flength, float* kernel, int klength);
void convolven1(float** input, int n1, int n2, float* kernel, int klength);
void convolven2(float** input, int n1, int n2, float* kernel, int klength);

float* gaussianderivative1kernel(int ksize,float sigma);
float* morletkernel(int ksize,float sigma);
float* gaussiankernel(int ksize,float sigma);
float* meankernel(int ksize);
float* trianglekernel(int ksize);

void printarray1d(double* data,  int col);
void printarray1i(int* data,  int col);
void printarray1f(float* data,  int col);
void printarray(double** data, int row, int col);

unsigned long fsize(FILE *fp);

void dot_hale(float** D11,float** D22,float** D12, float** in, float** out, float alpha, int row, int col);
void HaleCED(float** input,float** D11,float** D22, float** D12,float alf,int cgiter, int row, int col);
void constructcedtensor(float** S11, float** S22, float** S12,float** D11, float** D22, float** D12,float k, int row, int col);
void absarray(float** data, int row, int col);
void powarray(float** data, int row, int col);
void timesarray(float** data1,float** data2, float** result, int row, int col);
float herpr(float** X, float** Y, int row, int col);
void clearnan2(float** data, int row, int col);

void WeickertCED(float** data,float** a,float** c, float** b,float dt, int row, int col);
void cg(float** A, float* X, float* B, int row, int col,int cgiter);
void dot_vec(float** A, float* X,float* B, int row, int col);
float vec_dot_vec(float* a,float* b,int row);

hankel1d* createhankelM1d(double* data, int n, int m);
void clearnan1d(double* data, int row);
double* inverseHankelM1d(hankel1d* hankel);
void clearnand2(double** data, int row, int col);
double Maxd(double* Array, int row);
void normalizedmaxd(double* data,int row);
void normalizedmaxf(float* data,int row);
void savearray2d(char* outfilename,double** array,int row,int col);
void savearray1f(char* outfilename,float* array,int col);
float Maxf(float* Array, int row);

void getsubmatrix(double** data, double** window, int ccol, int crow, int collspan,int rowspan);
void setsubmatrix(double** data, double** window, int ccol, int crow, int collspan,int rowspan);
void setsubmatrixuse(double** data, double** window, int ccol, int crow,int colspan,int coluse,int rowspan, int rowuse);
void addsubmatrix(double** data, double** window, int ccol, int crow, int colspan,int rowspan);
void mask(double** data,double** mask, int row,int col);

int Maxdex(int* Array, int array_size);
int Mindex(int* Array, int array_size);
int Min(int* Array, int array_size);
int Max(int* Array, int array_size);
void get1dsubmatrix(int* dest, int* source,int beg, int end);

void copy2d(double** out,double** in, int row,int col);
double Max2dd(double** Array, int row,int col);
double Min2dd(double** Array, int row,int col);
void normtrace(float* data, int ns);
void normtraced(double* data, int ns);
void strecth(double** data, int row, int col, double nMax,double nMin);
void matmul(double** A, double** X, double** AX, int rowa, int cola,int rowx,int colx);
double** downsamplingtraces(double** data, int row,int col);
int* downsampling1di(int* data, int col);
