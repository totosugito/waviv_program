/*
 * binaryLib.c
 *
 *  Created on: Feb 8, 2013
 *      Author: toto
 */

#include "binaryLib.h"

char *createAutoFileNameWithDir(char *dirloc)
{
	char *cauto;
	time_t seconds;

	seconds = time (NULL);
	cauto = (char*) calloc(MAX_NAME_LEN, sizeof(char));
	sprintf(cauto, "%s/%ld_tempfile", dirloc, seconds);
	return(cauto);
}

char *createAutoFileName()
{
	char *cauto;
	time_t seconds;

	seconds = time (NULL);
	cauto = (char*) calloc(MAX_NAME_LEN, sizeof(char));
	sprintf(cauto, "%ld_tempfile", seconds);
	return(cauto);
}

void createAutomaticTemporaryFile()
{
	time_t seconds;

	seconds = time (NULL);

	cTrFile = (char*) calloc(MAX_NAME_LEN, sizeof(char));
	cHdrFile = (char*) calloc(MAX_NAME_LEN, sizeof(char));
	cGrouphFile = (char*) calloc(MAX_NAME_LEN, sizeof(char));

	sprintf(cHdrFile, "%ld_Header", seconds);
	sprintf(cTrFile, "%ld_Trace", seconds);
	sprintf(cGrouphFile, "%ld_Grouph", seconds);
}

void removeTemporaryFile()
{
	remove(cHdrFile);
	remove(cTrFile);
	remove(cGrouphFile);
}

void freeTemporaryFile()
{
	free(cHdrFile);
	free(cTrFile);
	free(cGrouphFile);
}

FILE *openFile(char *cfile, char *otype )
{
	FILE *fid=NULL;

	fid = fopen(cfile, otype);
	if(fid==NULL)
	{
		printf("Error opening file %s \n", cfile);
		exit(0);
	}
	return(fid);
}

int **readBinaryGrouphFile(char *cbinfile, int *nsp, int *ntrc, float *dt, int *ngrouph)
{
	int i;
	int **grouphFile=NULL;
	int nrow, intrc;
	int result;

	FILE *fid=NULL;

	fid = openFile(cbinfile, "r");
	result = fread(nsp, sizeof(int), 1, fid);
	result = fread(dt, sizeof(float), 1, fid);
	result = fread(&nrow, sizeof(int), 1, fid);

	grouphFile = alloc2int(COLUMN_GROUPH, nrow);
	intrc = 0;
	for(i=0; i<nrow; i++)
	{
		result = fread(grouphFile[i], sizeof(int), COLUMN_GROUPH, fid);
		if(result!=COLUMN_GROUPH)
		{
			printf("error reading line, read only %i from %i column\n", i, result, COLUMN_GROUPH);
			exit(0);
		}
		intrc += grouphFile[i][COLUMN_GROUPH-1];
	}

	(*ntrc) = intrc;
	(*ngrouph) = nrow;
	fclose(fid);

	return(grouphFile);
}

void writeBinaryGrouphFile(int **grouph, int irow, int icol, char *cname, int nsp, float dt)
{
	FILE *fid=NULL;
	int i;

	fid = openFile(cname, "w");
	fwrite(&nsp, sizeof(int), 1, fid);
	fwrite(&dt, sizeof(float), 1, fid);
	fwrite(&irow, sizeof(int), 1, fid);
	for(i=0; i<irow; i++)
		fwrite(grouph[i], sizeof(int), COLUMN_GROUPH, fid);
	fclose(fid);
}

float **readBinaryTracesFile(char *ctraces, int nsp, int ntrc)
{
	FILE *fid=NULL;
	float **traces;
	int i;
	int result;

	fid = openFile(ctraces, "r");
	traces = alloc2float(nsp, ntrc);
	for(i=0; i<ntrc; i++)
	{
		result = fread(traces[i], sizeof(float), nsp, fid);
		if(result!=nsp)
		{
			printf("traces at %i is not complete (nsp=%i, reading bytes=%i)\n", i, nsp, result);
			exit(0);
		}
	}

	fclose(fid);
	return(traces);
}

void writeBinaryTracesFile(char *ctraces, float **traces, int nsp, int ntrc)
{
	FILE *fid=NULL;
	int i;
	int result;

	//ctraces[0]='R';
	fid = openFile(ctraces, "w");
	for(i=0; i<ntrc; i++)
	{
		result = fwrite(traces[i], sizeof(float), nsp, fid);
		if(result!=nsp)
		{
			printf("traces at %i is not complete (nsp=%i, reading bytes=%i)\n", i, nsp, result);
			exit(0);
		}
	}

	fclose(fid);
}

void writeBinaryTracesFileTes(char *ctraces, float **traces, int nsp, int ntrc)
{
	FILE *fid=NULL;
	int i;
	int result;

	ctraces[0]='R';
	fid = openFile(ctraces, "w");
	for(i=0; i<ntrc; i++)
	{
		result = fwrite(traces[i], sizeof(float), nsp, fid);
		if(result!=nsp)
		{
			printf("traces at %i is not complete (nsp=%i, reading bytes=%i)\n", i, nsp, result);
			exit(0);
		}
	}

	fclose(fid);
}

void printGrouphData(int **data, int nrow, int ncol)
{
	int i, j;

	for(i=0; i<nrow; i++)
	{
		for(j=0; j<ncol; j++)
			printf("%i \t", data[i][j]);
		printf("\n");
	}
	printf("\n");
}

bool checkNewShot(int *ithdr, int keydata, int tmpkey)
{
	if(tmpkey!=ithdr[keydata])
		return false;
	else
		return true;
}

