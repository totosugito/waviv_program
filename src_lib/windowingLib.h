/*
 * windowingLib.h
 *
 *  Created on: Apr 13, 2012
 *      Author: toto
 */

#ifndef WINDOWINGLIB_H_
#define WINDOWINGLIB_H_

#ifdef SEISUNIX
#include "segy_lib.h"
#else
#include <cpromax.h>
#endif
#include <stdbool.h>

float **duplicateLastData(float **data, int ntrc, int ns, int newNtrc, int newNs);
void updateDataFromWindowing(float **data, float **windowData,  int nsMin, int nsMax,
		int ntrcMin, int ntrcMax);
float ** extraceTraceData(float **data, int nsMin, int nsMax, int Nns,
		int ntrcMin, int ntrcMax, int Nntrc);
int **getWindowingLength(int ndata, int windowLength, float overlap, int *posLength);
#endif /* WINDOWINGLIB_H_ */
