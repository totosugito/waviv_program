/*
 * fftLib.h
 *
 *  Created on: May 4, 2012
 *      Author: toto
 */

#ifndef FFTLIB_H_
#define FFTLIB_H_

#ifdef SEISUNIX
#include "segy_lib.h"
#else
#include <cpromax.h>
#endif

void swap(float *x,float *y);
void bublesort(float list[], int n);
float getMedian(float list[], int n);
void getNfft(int nz, float dt, int *nf, int *nfft, float *d1);
void fftProcess(float **data, int nsp, int nx, int nf, int nfft,
		float **dreal, float **dimag);
void ifftProcess(float **data, int nsp, int nx, int nf, int nfft,
		float **dreal, float **dimag);
void ifftProcess1(float *data, double d1, int nsp, int nx, int nf, int nfft,
		float *dreal, float *dimag);
void fftProcessd(float **data, int nsp, int nx, int nf, int nfft,
		double **dreal, double **dimag);
void ifftProcessd(float **data, int nsp, int nx, int nf, int nfft,
		double **dreal, double **dimag);

// ----------- UPDATE CODE
void fft1_1ff(float *data, int nsp, int nf, int nfft,
		float *dreal, float *dimag);
void ifft1_1ff(float *data, int nsp, int nf, int nfft,
		float *dreal, float *dimag);
void fft1_2ff(float **data, int nsp, int nx, int nf, int nfft,
		float **dreal, float **dimag, int type);
void ifft1_2ff(float **data, int nsp, int nx, int nf, int nfft,
		float **dreal, float **dimag, int type);
#endif /* FFTLIB_H_ */
