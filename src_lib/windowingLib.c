/*
 * windowingLib.c
 *
 *  Created on: Apr 13, 2012
 *      Author: toto
 */
#include "windowingLib.h"

float **duplicateLastData(float **data, int ntrc, int ns, int newNtrc, int newNs)
{
	int i, j;
	float **newdata=NULL;

	newdata = alloc2float(newNs, newNtrc);

	for(i=0; i<newNtrc; i++)
	{
		for(j=0; j<newNs; j++)
		{
			if(i<ntrc && j<ns)
				newdata[i][j] = data[i][j];
			else if(i>=ntrc && j<ns)
				newdata[i][j] = data[ntrc-1][j];
			else if(i<ntrc && j>=ns)
				newdata[i][j] = data[i][ns-1];
			else
				newdata[i][j] = 0.0;
		}
	}

	free2float(data);
	return(newdata);
}

void updateDataFromWindowing(float **data, float **windowData,  int nsMin, int nsMax,
		int ntrcMin, int ntrcMax)
{
	int i, j;
	int iNs, iNtrc;

	iNtrc = 0;
	for(i=ntrcMin; i<=ntrcMax; i++)
	{
		iNs = 0;
		for(j=nsMin; j<=nsMax; j++)
		{
			data[i][j] = windowData[iNtrc][iNs];
			iNs++;
		}
		iNtrc++;
	}
}

float **extraceTraceData(float **data, int nsMin, int nsMax, int Nns,
		int ntrcMin, int ntrcMax, int Nntrc)
{
	int i, j;
	int iNs, iNtrc;
	float **dout=NULL;

	dout = alloc2float(Nns, Nntrc);
	iNtrc = 0;
	for (i=ntrcMin; i<=ntrcMax; i++)
	{
		iNs = 0;
		for (j=nsMin; j<=nsMax; j++)
		{
			dout[iNtrc][iNs] = data[i][j];
			iNs++;
		}
		iNtrc++;
	}
	return(dout);
}

int **getWindowingLength(int ndata, int windowLength, float overlap, int *posLength)
{
	int nitr;
	int ileft, iright;
	int iwidth;
	int overlapWindow;
	int **windowLocation=NULL;
	int i;

	nitr = 1;
	ileft = 1;
	overlapWindow = round(windowLength*overlap);

	while (true)
	{
		iwidth = ileft + overlapWindow - 1;

		if(iwidth >= ndata)
			break;

		nitr = nitr + 1;
		ileft = ileft + overlapWindow;
	}

	windowLocation = alloc2int(3, nitr);
	ileft = 1;
	for (i=0; i<nitr; i++)
	{
		iwidth = ileft + overlapWindow - 1;
		iright = ileft + windowLength - 1;
		if(iwidth > ndata)
			iwidth = ndata;

		if(iright > ndata)
			iright = ndata;

		windowLocation[i][0] = ileft-1;
		windowLocation[i][1] = iright-1;
		windowLocation[i][2] = iwidth-1;

		ileft = ileft + overlapWindow;
	}

	(*posLength) = nitr;
	return(windowLocation);
}
