/*
 * potashLib.h
 *
 *  Created on: Sep 26, 2012
 *      Author: toto
 */

#ifndef POTASHLIB_H_
#define POTASHLIB_H_
#include "segy_lib.h"

#define LOOKFAC	2	/* Look ahead factor for npfaro	  */
#define PFA_MAX	720720	/* Largest allowed nfft	          */
#define SIGN(a,b) ((b) >= 0.0 ? fabs(a) : -fabs(a))
float maxarg1,maxarg2;
#define FMAX(a,b) (maxarg1=(a),maxarg2=(b),(maxarg1) > (maxarg2) ?\
		(maxarg1) : (maxarg2))
int iminarg1,iminarg2;
#define IMIN(a,b) (iminarg1=(a),iminarg2=(b),(iminarg1) < (iminarg2) ?\
		(iminarg1) : (iminarg2))
float sqrarg;
#define SQR1(a) ((sqrarg=(a)) == 0.0 ? 0.0 : sqrarg*sqrarg)

#define NR_END 1
#define FREE_ARG char*

void do_facor_sulib(float *data1,float *data2, float *acor,int n,int f,int nc);
void do_facor_shstk(float *data1,float *data2, float *acor,int n,int f,int nc);
int isamaxs(int n, float *a,int inc);
void computeRLag(float **firstdata, float **data, int ntrc, int nsp, int *rlag,
		int idxGather, int ishfm, int stmin, int stmax);

void free_matrix(float **m, long nrl, long nrh, long ncl, long nch);
void free_ivector(int *v, long nl, long nh);
void free_vector(float *v, long nl, long nh);
float **matrix(long nrl, long nrh, long ncl, long nch);
int *ivector(long nl, long nh);
float *vector(long nl, long nh);
float pythag(float a, float b);
bool svdcmp(float **a, int m, int n, float w[], float **v, int svditer);
void runSvdComputation(float **traces, int ntr, int nt, float npp, int np, int check,int svditer);

#endif /* POTASHLIB_H_ */
