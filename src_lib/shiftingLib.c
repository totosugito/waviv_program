/*
 * shiftingLib.c
 *
 *  Created on: Apr 13, 2012
 *      Author: toto
 */

#include "shiftingLib.h"

float **returnDipSteer(float **data_ori, float **dipsteer_data, int ntrc, int ns,
		int *shifting_pos, int nshifting, int midshifting)
{
	int i, j, jidx;
	int dshift;
	int idx1, idx2;
	float **outdata=NULL;

	outdata = alloc2float(ns, ntrc);
	for(i=0; i<ntrc; i++)
	{
		dshift = ABS ((midshifting-1) - shifting_pos[i]); //get delta shifting

		if(shifting_pos[i]<midshifting) //shifting top position
		{
			idx1 = dshift+2-1;
			idx2 = (ns-1)-idx1;

			jidx = 0;
			for(j=idx1; j<ns; j++)
			{
				outdata[i][jidx] = dipsteer_data[i][j];
				jidx++;
			}

			for(j=idx2+1; j<ns; j++)
				outdata[i][j] = data_ori[i][j];

			//fprintf(stderr, "1.    dshift=%i idx1=%i idx2=%i \n", dshift, idx1, idx2);
		}
		else	//shifting bottom position
		{
			idx1 = dshift-1;
			idx2 = ns-1;
			//didx = idx2 - idx1 + 1;

			jidx = 0;
			for(j=idx1; j<=idx2; j++)
			{
				outdata[i][j] = dipsteer_data[i][jidx];
				jidx++;
			}

			for(j=0; j<=idx1; j++)
				outdata[i][j] = data_ori[i][j];

			//fprintf(stderr, "2.    dshift=%i idx1=%i idx2=%i \n", dshift, idx1, idx2);
		}

	}

	return(outdata);
}

float **runDipSteer(float **data, int ntrc, int ns, int nshifting, int midshifting, int *shiftingPos)
{
	float *trace_ref=NULL;
	float **shifting_data=NULL;
	float *tmp1, *tmp2;
	int ishifting;
	int k, idx;

	trace_ref = stackingData(data, ntrc, ns);

	shifting_data = alloc2float(ns, ntrc);

	tmp1 = alloc1float(ns);
	for (k=0; k<ntrc; k++)  //loop over trace
	{
		for(idx=0; idx<ns; idx++)
			tmp1[idx] = data[k][idx];

		tmp2 = checkShifting(tmp1, trace_ref, ns, nshifting, midshifting, &ishifting);
		shiftingPos[k] = ishifting;

		for(idx=0; idx<ns; idx++)
			shifting_data[k][idx] = tmp2[idx];

		free1float(tmp2);
	}

	free1float(tmp1);
	free1float(trace_ref);
	return(shifting_data);
}

float *checkShifting(float *data, float *dataref, int ndata,
		int nshifting, int midshifting, int *ishifting)
{
	/*---------------------------------------------------
	   CHECK SHIFTING DATA
	---------------------------------------------------*/
	float *arr_corrcoef=NULL;
	int i;
	int iishifting, dlen, nref;
	float *tmpdata=NULL, *shiftdata=NULL;
	float *tmp_datates=NULL, *tmp_dataref=NULL;

	tmpdata = alloc1float(ndata+nshifting);	//allocate data+shifting
	memset(tmpdata, 0, (ndata+nshifting)*sizeof(float));
	memcpy(tmpdata+midshifting, data, ndata*sizeof(float));	//fill data

	arr_corrcoef = alloc1float(nshifting); //allocate array of shifting location
	for (i=0; i<nshifting; i++)  //loop over number of shifting
	{
		tmp_datates = shiftingData(tmpdata, ndata, i, midshifting, &dlen);
		tmp_dataref = shiftingReferenceData(dataref, ndata, i, dlen, midshifting, &nref);

//		print1float(tmp_datates, dlen, 1, 0, 1);
//		print1float(tmp_dataref, nref, 1, 0, 1);
//
//

		arr_corrcoef[i] = coefcorrdata(tmp_dataref, tmp_datates, dlen);
		if(arr_corrcoef[i] != arr_corrcoef[i]) //remove nan value
			arr_corrcoef[i] = 0.0;

//		fprintf(stderr,"%3.3f \n", arr_corrcoef[i]);
		free1float(tmp_datates);
		free1float(tmp_dataref);
	}
//	fprintf(stderr,"\n");

	// get maximum coefficient correlation
	iishifting = getMaximumPosition(arr_corrcoef, nshifting);
	shiftdata = getShiftingDataAtPosition(tmpdata, ndata, iishifting); // add zero in shifting data

	free1float(arr_corrcoef);
	free1float(tmpdata);

	(*ishifting) = iishifting;
	return(shiftdata);
}



float *getShiftingDataAtPosition(float *data, int ndata, int i)
{
	/*---------------------------------------------------
	  GET DATA AT SELECTED SHIFTING(i) POSITION
	  data         = data input
	  ndata        = length of output data
	  i            = get data from i to (i+ndata-1)
	---------------------------------------------------*/
	int idx1, idx2;
	float *outdata=NULL;

	idx1 = i;
	idx2 = idx1 + ndata - 1;
	outdata = alloc1float(ndata);

	memcpy(outdata, data+idx1, ndata*sizeof(float));

	return(outdata);
}

float *shiftingData(float *data, int ndata, int i, int midshifting, int *dpos)
{
	/*---------------------------------------------------
 	 SHIFTING DATA
 	 data        = input of data
 	 dpos        = length of output data
 	 i			 = current index position
 	 ndata       = length of tmpdata
 	 midshifting = half of total shifting (start and end shifting width)
	 example :
	 0 0 0 0 0 0 1 2 3 4 5 6 7 8 9 0 0 0 0 0 0
	 midshifting ------data ------ midshifting
	---------------------------------------------------*/

	int idx1, idx2;
	int pos3, pos4, ddpos;
	float *shiftingData=NULL;

	idx1 = i;
	idx2 = idx1 + ndata - 1;

	if(idx1<midshifting)
		pos3 = midshifting;
	else
		pos3 = idx1;

	if(idx2>=midshifting + ndata)
		pos4 = midshifting + ndata - 1;
	else
		pos4 = idx2;

	ddpos = pos4 - pos3 + 1;
	(*dpos) = ddpos;

//	printf("pos3=%i pos4=%i ddpos=%i \n", pos3, pos4, ddpos);
	shiftingData = alloc1float(ddpos);
	memcpy(shiftingData, data+pos3, ddpos*sizeof(float));
	return(shiftingData);
}

float *shiftingReferenceData(float *dataref, int ndata, int i, int dpos, int midshifting,
		int *nrefdata)
{
	/*---------------------------------------------------
	 SHIFTING REFERENCE DATA
	 dataref     = input vector reference data
	 ndata       = length of dataref
	 i           = checking shifting index
	 Example :
	 0 0 0 0 0 0 1 2 3 4 5 6 7 8 9 0 0 0 0 0 0 --> array of shifting (ndata = 10)
	 0 0 0 0 0 0 1 2 3 4 --> i=0
	 0 0 0 0 0 1 2 3 4 5 --> i=1
	 0 0 0 0 1 2 3 4 5 6 --> i=2 ... etc

	 dpos        = length of output data
	 midshifting = half of total shifting (start and end shifting width)
	 example :
	 0 0 0 0 0 0 1 2 3 4 5 6 7 8 9 0 0 0 0 0 0
	 midshifting ------data ------ midshifting
	---------------------------------------------------*/

	float *newShiftingData=NULL;
	int pos1, pos2;

	if(i<=midshifting-1)
		pos1 = (midshifting-1) - (i-1);
	else
		pos1 = 0;

	pos2 = pos1+dpos-1;

	newShiftingData = alloc1float(dpos);
	memcpy(newShiftingData, dataref+pos1, dpos*sizeof(float));

	(*nrefdata) = dpos;
	return(newShiftingData);
}


float coefcorrdata(float *x, float *y, int ndata)
{
	/*---------------------------------------------------
	 COMPUTE COEFFICIENT CORRELATION
	 http://www.reproducibility.org/RSF/book/jsg/simistack/paper_html/node3.html
	 x = input vector I
	 y = input vector II
	 ndata = length of x and y
	---------------------------------------------------*/
	int i;
	float xy, x2, y2;
	float cc;

	xy = 0.0;
	x2 = 0.0;
	y2 = 0.0;
	for(i=0; i<ndata; i++)
	{
		xy = xy + (x[i]*y[i]);
		x2 = x2 + (x[i]*x[i]);
		y2 = y2 + (y[i]*y[i]);
	}

	cc = xy/(sqrt(x2*y2));

	return(cc);
}

int getMaximumPosition(float *data, int ndata)
{
	int i, idxmax;
	float tmpmax;

	idxmax = 0;
	tmpmax = ABS(data[idxmax]);
	for (i=1; i<ndata; i++)
	{
		if(tmpmax < ABS(data[i]))
		{
			tmpmax = ABS(data[i]);
			idxmax = i;
		}
	}

	return(idxmax);
}

float *stackingData(float **data, int ntrc, int ns)
{
	int i, j;
	float *stackdata=NULL;

	stackdata = alloc1float(ns);
	memset(stackdata, 0, ns*sizeof(float));

	for(j=0; j<ntrc; j++)
		for (i=0; i<ns; i++)
			stackdata[i] = stackdata[i] + data[j][i];

	return(stackdata);
}
