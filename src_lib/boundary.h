/*
 * boundary.h
 *
 *  Created on: Nov 19, 2012
 *      Author: toto
 */

#ifndef BOUNDARY_H_
#define BOUNDARY_H_

#include <stdbool.h>
#include <math.h>
#include <stdlib.h>
void bound1(float* tempt,float* extendt,int nfw,int n1,int n2,bool boundary);
/*<extend seismic data>*/


void bound2(float* temp2,float* temp3,int n1,int tempnfw,int j,bool boundary);
/*<extend temporary seismic data>*/


void bound3(float* tempt,
	    float* extendt,
	    int nfw1        /* Sample direction*/,
	    int nfw2        /* Trace direction */,
	    int n1,int n2,
	    bool boundary);
/*<extend seismic data>*/

#endif /* BOUNDARY_H_ */
