/*
 * gD.c
 *
 *  Created on: Dec 19, 2012
 *      Author: toto
 */

// Gaussian (Derivative) Convolution
float *gDerivative(int order, float *X, float *Gs, float scale, int nX)
{
	float *g;
	float tmp;
	int i;

	g = (float*) calloc(nX, sizeof(float));
	switch (order)
	{
	case 0:
		memcpy(g, Gs, nX*sizeof(float));
		break;
	case 1:
		for(i=0; i<nX; i++)
		{
			tmp = -X[i]/powf(scale, 2.0);
			g[i] = tmp* Gs[i];
		}
		break;
	case 2:
		for(i=0; i<nX; i++)
		{
			tmp = powf(X[i], 2.0) - powf(scale, 2.0);
			g[i] = tmp/pow(scale, 4.0) * Gs[i];

		}
		break;
	default:
		fprintf(stderr, "only derivatives up to second order are supported");
		exit(0);
	}

	return g;
}

float **convSepBrd(float **f, int frow, int fcol, float *Gsx, float *Gsy, int nG)
{
	Vector_Int *vectorint;
	Array_Float *arrayfloat;
	SignalProcessing_FLoat *signprocflot;

	arrayf *out=NULL;
	arrayf *tmpout=NULL;
	float *iind;
	float *jind;

	int i, j;
	int M, N, nIJ;	//size of input array
	float K, L;

	// get row and column size
	M = frow;
	N = fcol;

	K = (nG - 1.0)/2.0;
	L = (nG - 1.0)/2.0;

	nIJ = (int) (N + 2*K);
	iind = (float*) calloc(nIJ, sizeof(float));
	jind = (float*) calloc(nIJ, sizeof(float));

	for(i=0; i<nIJ; i++)
	{

	}

	//get vector iind1 from ... to ...
	iind1 = vectorint->vectori_create_val(0, 1, N+2*K-1, -K);
	//change minimum vector with max operation
	vectorint->vectori_maxrepl(iind1, 0);
	//change maximum vector with min operation
	vectorint->vectori_minrepl(iind1, N-1);

	//get vector jind1 from ... to ...
	jind1 = vectorint->vectori_create_val(0, 1, M+2*L-1, -L);
	//change minimum vector with max operation
	vectorint->vectori_maxrepl(jind1, 0);
	//change maximum vector with min operation
	vectorint->vectori_minrepl(jind1, M-1);

	//create new array from new index
	tmpout = arrayfloat->arrayf_newfromvect(array, iind1, jind1);

	//process convolution2 with type valid (2)
	out = signprocflot->SignProcf_conv2(vect1, vect2, tmpout,2);

	arrayfloat->arrayf_free(tmpout);
	vectorint->vectori_free(iind1);
	vectorint->vectori_free(jind1);
	delete vectorint;
	delete signprocflot;
	delete arrayfloat;
	return out;
}
