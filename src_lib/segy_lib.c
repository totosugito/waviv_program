/*
 * fxemd_segy.c
 *
 *  Created on: Feb 18, 2010
 *      Author: toto
 */
#include "segy_lib.h"
#include "slicingLib.h"
#define CFL 0.7
#define LOOKFAC	2	/* Look ahead factor for npfaro	  */
#define PFA_MAX	720720	/* Largest allowed nfft	          */
#define SF_PI (3.14159265358979323846264338328)

char **alloc2char(int col, int row)
{
	int i;
	char **out;

	out = (char**) calloc(row, sizeof(char*));
	out[0] = (char*) calloc(row*col, sizeof(char));
	for(i=1; i<row; i++)
		out[i] = out[i-1] + col;

	return out;
}

void free2char(char **A)
{
	 free(A[0]);
	 free(A);
}

void segy_lib_err(char *namefunc, char *report)
{
	fprintf(stderr, "%s : %s \n", namefunc, report);
	exit(0); 
}

void segy_lib_warn(char *namefunc, char *report)
{
	fprintf(stderr, "%s : %s \n", namefunc, report);
}


void readEbcdicHeader(FILE *inpf, char cebcdic[])
/* -----------------------------------------------------------------
 * read 3200 byte ascii ebcdic header from segy
 * -----------------------------------------------------------------
 */
{
	char *ebcbuf=NULL;
	size_t result;

	ebcbuf = (char*) calloc(EBCBYTES, sizeof(char));

	/* - the ebcdic header file in ascii */
	result = fread(ebcbuf, sizeof(char), EBCBYTES, inpf);
	if(result!=EBCBYTES)
		segy_lib_err("readEbcdicHeader", "error reading EBCDIC header");

	memcpy(cebcdic, ebcbuf, 3200*sizeof(char));
	free(ebcbuf);
}

void readBinaryHeader(FILE *inpf, int endian, bhed *bh, int *nsegy)
/* -----------------------------------------------------------------
 * read 400 byte binary header from segy
 * nsegy   -> return number of byte in 1 trace
 * binhead -> return binary header information
 * -----------------------------------------------------------------
 */
{
	size_t result;
	int ns;
	int i;

	result = fread((char *) &tapebh, 1, BNYBYTES, inpf);
	if(result!=BNYBYTES)
		segy_lib_err("readBinaryHeader", "error reading binary header");

	/* Convert from bytes to ints/shorts */
	tapebhed_to_bhed(&tapebh, bh);

	/* if little endian machine, swap bytes in binary header */
	if (endian==0)
	{
		for (i = 0; i < BHED_NKEYS; ++i)
			swapbhval(bh,i);
	}

	ns = bh->hns; /*get number of sample value (ns) */

	/* check format data segy */
	switch (bh->format)
	{
	case 1:
		(*nsegy) = ns*4 + SEGY_HDRBYTES;
		break;
	case 2:
		(*nsegy) = ns*4 + SEGY_HDRBYTES;
		break;
	case 3:
		(*nsegy) = ns*2 + SEGY_HDRBYTES;
		break;
	case 5:
		(*nsegy) = ns*4 + SEGY_HDRBYTES;
		break;
	case 8:
		(*nsegy) = ns + SEGY_HDRBYTES;
		break;
	default:
		segy_lib_err("readBinaryHeader", "format not SEGY standard (1, 2, 3, 5, or 8)\n");
		break;
	}

}

void readTraceSegy(FILE *inpf, int endian, int ns, int format, int nsegy, segy *trace)
/* -----------------------------------------------------------------
 * read trace from segy
 * nsegy   -> number of byte in 1 trace
 * binhead -> binary header information
 * -----------------------------------------------------------------
 */
{
	int i;
	int result=0;

	result = (int) fread((char *) &tapetr, 1, nsegy, inpf);

	/* Convert from bytes to ints/shorts */
	tapesegy_to_segy(&tapetr, &tr);

	/* If little endian machine, then swap bytes in trace header */
	if (endian==0)
	{
		for (i = 0; i < SEGY_NKEYS; ++i)
			swaphval(&tr,i);
	}

	/* Check tr.ns field */
	if (ns != tr.ns)
	{
		fprintf(stderr, "discrepant tr.ns = %d with tape/user ns = %d\n"
				"\t... first noted \n",
				tr.ns, ns);
	}

	/* don't convert, if not appropriate */
	switch (format)
	{
	case 1: /* endian=0 byte swapping */
		/* Convert IBM floats to native floats */
		ibm_to_float((int *) tr.data,
				(int *) tr.data, ns, endian);
		break;
	case 5:
		if (endian==0)
			for (i = 0; i < ns ; ++i)
				swap_float_4(&tr.data[i]);
		break;
	case 2: /* convert longs to floats */
		/* SU has no provision for reading */
		/* data as longs */
		long_to_float((long *) tr.data,
				(float *) tr.data, ns, endian);
		break;
	case 3: /* shorts are the SHORTPAC format */
		/* used by supack2 and suunpack2 */
		if (endian==0)/* endian=0 byte swap */
			for (i = 0; i < ns ; ++i)
				swap_short_2((short *) &tr.data[i]);
		/* Set trace ID to SHORTPACK format */
		tr.trid = SHORTPACK;
		break;
	case 8: /* convert bytes to floats */
		/* SU has no provision for reading */
		/* data as bytes */
		integer1_to_float((signed char *)tr.data,
				(float *) tr.data, ns);
		break;
	}

	*trace = tr;
}

void readSegy(FILE *inpf, int endian, int ns, int format, int nsegy, segy *tr,
		char *header, float *data)
{
	readTraceSegy(inpf, endian, ns, format, nsegy, tr);
	memcpy(data, tr->data, ns*sizeof(float));
	memcpy(header, (char*)tr, HDRBYTES*sizeof(char));
}

void writeEbcdicHeader(FILE *outf, char cebcdic[])
/* -----------------------------------------------------------------
 * write ebcdic ascii header to segy
 * -----------------------------------------------------------------
 */
{
	size_t result;

	result = fwrite(cebcdic, sizeof(char), EBCBYTES, outf);
	if(result != EBCBYTES)
		segy_lib_err("writeEbcdicHeader",  "error writing header ebcidc header");
}

void writeBinaryHeader(FILE *outf, int endian, bhed *bh)
/* -----------------------------------------------------------------
 * write binary header to segy
 * -----------------------------------------------------------------
 */
{
	int i;

	/* if little endian machine, swap bytes in binary header */
	if (endian==0)
	{
		for (i = 0; i < BHED_NKEYS; ++i)
			swapbhval(bh,i);
	}

	/* Convert from bytes to ints/shorts */
	bhed_to_tapebhed(bh, &tapebh);

	fwrite((char*)&tapebh, 1, BNYBYTES, outf);
}

void writeTraceSegy(FILE *outf, int endian, segy *trace, int nsegy, int ns)
/* -----------------------------------------------------------------
 * write trace to segy
 * nsegy   -> number of byte in 1 trace
 * save data in segy IBM floating data
 * -----------------------------------------------------------------
 */
{
	int i;

	tr = *trace;

	float_to_ibm((int *) tr.data, (int *) tr.data, ns, endian);
	if (endian==0)
	{
		for (i = 0; i < SEGY_NKEYS; ++i)
			swaphval(&tr,i);
	}

	/* Convert from ints/shorts to bytes */
	segy_to_tapesegy(&tr, &tapetr, nsegy);

	/* Write the trace to disk */
	fwrite((char *)&tapetr, 1, nsegy, outf);
}

void writeSegy(FILE *outf, int endian, segy *trace, int nsegy, int ns,
		char *header, float *data)
{
	memcpy((char*)&tr, header, HDRBYTES);
	memcpy(tr.data, data, ns*sizeof(float));
	writeTraceSegy(outf, endian, &tr, nsegy, ns);
}

int getNumberOfTraceSegyFile(FILE *inpf, int nsegy)
/* -----------------------------------------------------------------
 * get Number Of SEGY Trace From File
 * nsegy  -> size of byte in 1 trace
 * -----------------------------------------------------------------
 */
{
	off64_t begPos, endPos;  /*size of file input*/
	off64_t bytetr;  /*size of trace data and data */
	int retseek;
	int ntrc;              /*output number of traces*/

	ntrc = 0;
	begPos = ftello64(inpf);                /*get beginning of file pointer location*/

	retseek = fseeko64(inpf, 0 , SEEK_END); /* go to end of file */
	if(retseek) segy_lib_err("getNumOfTrace", "error seek command.");

	endPos = ftello64(inpf);                /* get size of file */

	bytetr = endPos - 3600;              /* remove ascii and binary trace */

	if((bytetr % nsegy)==0)            /* get number of traces*/
		ntrc = bytetr/nsegy;
	else
		ntrc = 0;

	retseek = fseeko64(inpf, begPos , SEEK_SET);
	if(retseek) segy_lib_err("getNumOfTrace", "error set location to beginning pointer.");

	return (ntrc);
}

int getNumberOfTraceSuFile(FILE *ifile)
{
	int ns, ntrc;
	off64_t endPos;  /*size of file input*/
	segy tr;

	rewind(ifile);

	ntrc = 0;
	if(fgettr(ifile, &tr))
	{
		ns = tr.ns;
		fseeko64(ifile, 0 , SEEK_END); /* go to end of file */
		endPos = ftello64(ifile);

		ntrc = endPos/(240+ns*4);
		fseeko64(ifile, 0 , SEEK_SET); /* go to end of file */
	}

	return(ntrc);
}


void ibm_to_float(int from[], int to[], int n, int endian)
/***********************************************************************
ibm_to_float - convert between 32 bit IBM and IEEE floating numbers
 ************************************************************************
Input::
from		input vector
to		output vector, can be same as input vector
endian		byte order =0 little endian (DEC, PC's)
			    =1 other systems
 *************************************************************************
Notes:
Up to 3 bits lost on IEEE -> IBM

Assumes sizeof(int) == 4

IBM -> IEEE may overflow or underflow, taken care of by
substituting large number or zero

Only integer shifting and masking are used.
 *************************************************************************
Credits: CWP: Brian Sumner,  c.1985
 *************************************************************************/
{
	register int fconv, fmant, i, t;

	for (i=0;i<n;++i) {

		fconv = from[i];

		/* if little endian, i.e. endian=0 do this */
		if (endian==0) fconv = (fconv<<24) | ((fconv>>24)&0xff) |
				((fconv&0xff00)<<8) | ((fconv&0xff0000)>>8);

		if (fconv) {
			fmant = 0x00ffffff & fconv;
			/* The next two lines were added by Toralf Foerster */
			/* to trap non-IBM format data i.e. conv=0 data  */
			if (fmant == 0)
				segy_lib_warn("ibm_to_float",
						"mantissa is zero data may not be in IBM FLOAT Format !");
			t = (int) ((0x7f000000 & fconv) >> 22) - 130;
			while (!(fmant & 0x00800000)) { --t; fmant <<= 1; }
			if (t > 254) fconv = (0x80000000 & fconv) | 0x7f7fffff;
			else if (t <= 0) fconv = 0;
			else fconv = (0x80000000 & fconv) |(t << 23)|(0x007fffff & fmant);
		}
		to[i] = fconv;
	}
	return;
}

void tapebhed_to_bhed(const tapebhed *tapebhptr, bhed *bhptr)
/****************************************************************************
tapebhed_to_bhed -- converts the seg-y standard 2 byte and 4 byte
	integer header fields to, respectively, the
	machine's short and int types.
 *****************************************************************************
Input:
tapbhed		pointer to array of
 *****************************************************************************
Notes:
The present implementation assumes that these types are actually the "right"
size (respectively 2 and 4 bytes), so this routine is only a placeholder for
the conversions that would be needed on a machine not using this convention.
 *****************************************************************************
Author: CWP: Jack  K. Cohen, August 1994
 ****************************************************************************/

{
	register int i;
	Value val;

	/* convert binary header, field by field */
	for (i = 0; i < BHED_NKEYS; ++i) {
		gettapebhval(tapebhptr, i, &val);
		putbhval(bhptr, i, &val);
	}
}

void bhed_to_tapebhed(const bhed *bhptr, tapebhed *tapebhptr)
/***************************************************************************
bhed_tape_bhed -- converts the binary tape header in the machine's short
and int types to, respectively, the seg-y standard 2 byte and 4 byte integer
types.
 ****************************************************************************
Input:
bhptr		pointer to binary header vector

Output:
tapebhptr	pointer to tape binary header vector
 ****************************************************************************
Notes:
The present implementation assumes that these types are actually the "right"
size (respectively 2 and 4 bytes), so this routine is only a placeholder for
the conversions that would be needed on a machine not using this convention.
 ****************************************************************************
Author: CWP: Jack K. Cohen  August 1994
 ***************************************************************************/
{
	register int i;
	Value val;

	/* convert the binary header field by field */
	for (i = 0; i < BHED_NKEYS; ++i) {
		getbhval(bhptr, i, &val);
		puttapebhval(tapebhptr, i, &val);
	}
}

void tapesegy_to_segy(const tapesegy *tapetrptr, segy *trptr)
/****************************************************************************
tapesegy_to_segy -- converts the seg-y standard 2 byte and 4 byte
		    integer header fields to, respectively, the machine's
		    short and int types.
 *****************************************************************************
Input:
tapetrptr	pointer to trace in "tapesegy" (SEG-Y on tape) format

Output:
trptr		pointer to trace in "segy" (SEG-Y as in	 SU) format
 *****************************************************************************
Notes:
Also copies float data byte by byte.  The present implementation assumes that
the integer types are actually the "right" size (respectively 2 and 4 bytes),
so this routine is only a placeholder for the conversions that would be needed
on a machine not using this convention.	 The float data is preserved as
four byte fields and is later converted to internal floats by ibm_to_float
(which, in turn, makes additonal assumptions).
 *****************************************************************************
Author: CWP:Jack K. Cohen,  August 1994
 ****************************************************************************/
{
	register int i;
	Value val;

	/* convert header trace header fields */
	for (i = 0; i < SEGY_NKEYS; ++i) {
		gettapehval(tapetrptr, i, &val);
		puthval(trptr, i, &val);
	}

	/* copy the optional portion */
	memcpy((char *)&(trptr->otrav)+2, tapetrptr->unass, 60);

	/* copy data portion */
	memcpy(trptr->data, tapetrptr->data, 4*SU_NFLTS);
}

void long_to_float(long from[], float to[], int n, int endian)
/****************************************************************************
Author:	J.W. de Bruijn, May 1995
 ****************************************************************************/
{
	register int i;

	if (endian == 0) {
		for (i = 0; i < n; ++i) {
			swap_long_4(&from[i]);
			to[i] = (float) from[i];
		}
	} else {
		for (i = 0; i < n; ++i) {
			to[i] = (float) from[i];
		}
	}
}

void short_to_float(short from[], float to[], int n, int endian)
/****************************************************************************
short_to_float - type conversion for additional SEG-Y formats
 *****************************************************************************
Author: Delft: J.W. de Bruijn, May 1995
Modified by: Baltic Sea Reasearch Institute: Toralf Foerster, March 1997
 ****************************************************************************/
{
	register int i;

	if (endian == 0) {
		for (i = n-1; i >= 0 ; --i) {
			swap_short_2(&from[i]);
			to[i] = (float) from[i];
		}
	} else {
		for (i = n-1; i >= 0 ; --i)
			to[i] = (float) from[i];
	}
}

void integer1_to_float(signed char from[], float to[], int n)
/****************************************************************************
integer1_to_float - type conversion for additional SEG-Y formats
 *****************************************************************************
Author: John Stockwell,  2005
 ****************************************************************************/
{
	while (n--) {
		to[n] =  from[n];
	}
}

void segy_to_tapesegy(const segy *trptr, tapesegy *tapetrptr, size_t nsegy)
/***************************************************************************
tapesegy_to_segy -- converts the integer header fields from, respectively,
		    the machine's short and int types to the  seg-y standard
		    2 byte and 4 byte types.
 ****************************************************************************
Input:
trptr		pointer to SU SEG-Y data vector
nsegy		whole size of a SEG-Y trace in bytes

Output:
tapetrptr	pointer to tape SEG-Y data vector
 ****************************************************************************
Notes:
Also copies the float data byte by byte.  The present implementation assumes
that the integer types are actually the "right" size (respectively 2 and
4 bytes), so this routine is only a placeholder for the conversions that
would be needed on a machine not using this convention.	 The float data
is preserved as four byte fields and is later converted to internal floats
by float_to_ibm (which, in turn, makes additonal assumptions)
 ****************************************************************************
Author: CWP: Jack K. Cohen  August 1994
 ***************************************************************************/
{
	register int i;
	Value val;

	/* convert trace header, field by field */
	for (i = 0; i < SEGY_NKEYS; ++i) {
		gethval(trptr, i, &val);
		puttapehval(tapetrptr, i, &val);
	}

	/* copy optional portion */
	memcpy(tapetrptr->unass, (char *)&(trptr->otrav)+2, 60);

	/* copy data portion */
	memcpy(tapetrptr->data, trptr->data, 4*SU_NFLTS);

}

/* Assumes sizeof(int) == 4 */
void float_to_ibm(int from[], int to[], int n, int endian)
/**********************************************************************
 float_to_ibm - convert between 32 bit IBM and IEEE floating numbers
 ***********************************************************************
Input:
from	   input vector
n	   number of floats in vectors
endian	   =0 for little endian machine, =1 for big endian machines

Output:
to	   output vector, can be same as input vector

 ***********************************************************************
Notes:
Up to 3 bits lost on IEEE -> IBM

IBM -> IEEE may overflow or underflow, taken care of by
substituting large number or zero

Only integer shifting and masking are used.
 ***********************************************************************
Credits:     CWP: Brian Sumner
 ***********************************************************************/
{
	register int fconv, fmant, i, t;

	for (i=0;i<n;++i) {
		fconv = from[i];
		if (fconv) {
			fmant = (0x007fffff & fconv) | 0x00800000;
			t = (int) ((0x7f800000 & fconv) >> 23) - 126;
			while (t & 0x3) { ++t; fmant >>= 1; }
			fconv = (0x80000000 & fconv) | (((t>>2) + 64) << 24) | fmant;
		}
		if(endian==0)
			fconv = (fconv<<24) | ((fconv>>24)&0xff) |
			((fconv&0xff00)<<8) | ((fconv&0xff0000)>>8);

		to[i] = fconv;
	}
	return;
}

void warning_message(char *str)
{
	fprintf(stderr, "%s \n", str);
}

void gotoTraceSegyPosition(FILE *fin, int loc, int nsegy)
{
	int retseek;
	off64_t pos1, pos2;
	off64_t loc1;
	loc1 = (off64_t) loc;
	pos2 = loc1*nsegy;
	pos1 = 3600 + pos2;

	retseek = fseeko64(fin, pos1 , SEEK_SET);
	if(retseek) segy_lib_err("getNumOfTrace", "error set location to beginning pointer.");
}

void gotoTraceSuPosition(FILE *fin, int loc, int nsegy)
{
	int retseek;
	off64_t pos2;
	off64_t loc1;
	loc1 = (off64_t) loc;
	pos2 = loc1*nsegy;

	retseek = fseeko64(fin, pos2 , SEEK_SET);
	if(retseek) segy_lib_err("getNumOfTrace", "error set location to beginning pointer.");
}

int computeIntHeaderAtByte(FILE *fin, int pos, int nbyte, int endian, int tsigned)
{
	int myval, result;
	off64_t begPos;  /*pointer position*/
	unsigned char value[nbyte];

	begPos = ftello64(fin);  /*get beginning of file pointer location*/

	//go to at header location
	fseeko64(fin, (pos-1) , SEEK_CUR); /* go to end of file */
	result = fread(value, 1, nbyte, fin);

	fseeko64(fin, -(pos+nbyte-1) , SEEK_CUR); /* go to beginning of pointer location */

	myval = 0;
	if (endian==0)
	{
		if(nbyte==4)
		{
			myval = value[3] + (value[2]<<8) + (value[1]<<16) + (value[0]<<24);
		}
		else if(nbyte==2)
		{
			if(tsigned==1) //signed data
				myval = (short) (value[1] + (value[0]<<8));
			else			//unsigned data
				myval = value[1] + (value[0]<<8);
		}
	}
	else
	{
		if(nbyte==4)
		{
			myval = value[0] + (value[1]<<8) + (value[2]<<16) + (value[3]<<24);
		}
		else if(nbyte==2)
		{
			if(tsigned==1) //signed data
				myval = (short) (value[0] + (value[1]<<8));
			else	//unsigned data
				myval = value[0] + (value[1]<<8);
		}
	}

	return(myval);
}

float computeIBMFloatHeaderAtByte(FILE *fin, int pos, int nbyte, int endian)
{
	//off64_t begPos;  /*pointer position*/
	float *data, dataout;
	int result;

	data = (float*) calloc(1, sizeof(float));
	//begPos = ftello64(fin);  /*get beginning of file pointer location*/

	//go to at header location
	fseeko64(fin, (pos-1) , SEEK_CUR); /* go to end of file */
	result = fread(data, sizeof(float), 1, fin);
	ibm_to_float((int *) data, (int *) data, 1, endian);

	fseeko64(fin, -(pos+nbyte-1) , SEEK_CUR); /* go to beginning of pointer location */

	dataout = data[0];
	free(data);

	return(dataout);
}


void nextTrace(FILE *fin, int nsegy)
{
	int retseek;

	//go to next t race
	retseek = fseeko64(fin, nsegy , SEEK_CUR); /* go to end of file */
//	fread(data, sizeof(char), nsegy, fin);
	if(retseek) segy_lib_err("getNumOfTrace", "error seek command.");
}

int char2int(char *data, int pos, int npos, int endian)
{
	unsigned char *tmpdata;
	int i, ival;

	tmpdata = (unsigned char*) calloc(SEGY_HDRBYTES, 1);
	for(i=0; i<npos; i++)
		tmpdata[pos+i-1] = (unsigned char) data[pos+i-1];

	ival = uchar2int(tmpdata, pos, npos, endian);

	free(tmpdata);
	return(ival);
}

int uchar2int(unsigned char *data, int pos, int npos, int endian)
{
	int myval;
	unsigned char *value;

	pos = pos-1;
	value = (unsigned char*) calloc(npos, sizeof(unsigned char));
	memcpy(value, data+pos, npos*sizeof(unsigned char));
	if (endian==0)
	{
		if(npos==4)
			myval = value[3] + (value[2]<<8) + (value[1]<<16) + (value[0]<<24);
		else if(npos==2)
			myval = value[1] + (value[0]<<8);
		else
			myval = 0;
	}
	else
	{
		if(npos==4)
			myval = value[0] + (value[1]<<8) + (value[2]<<16) + (value[3]<<24);
		else if(npos==2)
			myval = value[0] + (value[1]<<8);
		else
			myval = 0;
	}
	free(value);
	return(myval);
}

float uchar2float(unsigned char *data, int pos, int npos, int endian)
{
	float myval;
	int ival;

	ival = uchar2int(data, pos, npos, endian);
	myval = int2float(ival);

	return(myval);
}

float uchar2ibm(unsigned char *data, int pos, int npos, int endian)
{
	float myval;
	int ival;

	ival = uchar2int(data, pos, npos, endian);

	myval = ibm2float(ival);

	return(myval);
}

float int2float(int data)
{
	float *fd;

	fd = (float *)&data;

	return(fd[0]);
}

float ibm2float(int ifrom)
{
	int fconv, fmant, t;
	float dibm;

	fconv = ifrom;

	if (fconv)
	{
		fmant = 0x00ffffff & fconv;
		/* The next two lines were added by Toralf Foerster */
		/* to trap non-IBM format data i.e. conv=0 data  */
		if (fmant == 0)
			fprintf(stderr, "ibm_to_float : mantissa is zero data may not be in IBM FLOAT Format !");
		t = (int) ((0x7f000000 & fconv) >> 22) - 130;
		while (!(fmant & 0x00800000)) { --t; fmant <<= 1; }
		if (t > 254) fconv = (0x80000000 & fconv) | 0x7f7fffff;
		else if (t <= 0) fconv = 0;
		else fconv = (0x80000000 & fconv) |(t << 23)|(0x007fffff & fmant);
	}
	dibm = int2float(fconv);

	return(dibm);
}

void writeSegyTraceData(FILE *fod, float **traces, char **headers,
		int ntrc, int nsp, int endian, int nsegy)
{
	int i;

	for(i=0; i<ntrc; i++)
	{
		memcpy((char*)&tr, headers[i], HDRBYTES);
		memcpy(tr.data, traces[i], nsp*sizeof(float));
		writeTraceSegy(fod, endian, &tr, nsegy, nsp);
	}
}

void writeSuTraceData(FILE *fod, float **traces, char **headers,
		int ntrc, int nsp)
{
	int i;

	for(i=0; i<ntrc; i++)
	{
		memcpy((char*)&tr, headers[i], HDRBYTES);
		memcpy(tr.data, traces[i], nsp*sizeof(float));
		puttr(&tr);
	}
}

void int2uchar(int ivalue, unsigned char *data, int pos, int npos, int endian)
{
	int tmp;
	pos = pos-1;
	if(npos==4)
	{
		if(endian==0)
		{
			tmp = ivalue>>24;
			data[pos] = (unsigned char) tmp;

			ivalue = ivalue - (tmp<<24);
			tmp = ivalue>>16;
			data[pos+1] = (unsigned char) tmp;

			ivalue = ivalue - (tmp<<16);
			tmp = ivalue>>8;
			data[pos+2] = (unsigned char) tmp;

			tmp = ivalue - (tmp<<8);
			data[pos+3] = (unsigned char) tmp;
		}
		else
		{
			tmp = ivalue>>24;
			data[pos+3] = (unsigned char) tmp;

			ivalue = ivalue - (tmp<<24);
			tmp = ivalue>>16;
			data[pos+2] = (unsigned char) tmp;

			ivalue = ivalue - (tmp<<16);
			tmp = ivalue>>8;
			data[pos+1] = (unsigned char) tmp;

			tmp = ivalue - (tmp<<8);
			data[pos] = (unsigned char) tmp;
		}
	}
	else
		err("int2uchar for 2 byte not yet implementated");
}

void int2char(int ivalue, char *data, int pos, int npos, int endian)
{
	int i;
	unsigned char *tmp;

	pos = pos-1;
	tmp = (unsigned char*) calloc(npos, sizeof(unsigned char));
	int2uchar(ivalue, tmp, 1, npos, endian);

	for (i=0; i<npos; i++)
		data[pos+i] = (char) (tmp[i]);

	free(tmp);
}
