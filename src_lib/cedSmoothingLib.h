/*
 * cedSmoothingLib.h
 *
 *  Created on: Jan 22, 2013
 *      Author: toto
 */

#ifndef CEDSMOOTHINGLIB_H_
#define CEDSMOOTHINGLIB_H_

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <par.h>
#include "../matlab_lib/datafun/conv.h"
#include "printLib.h"

float *gDerivative(int order, float *x, float *Gs, float scale, int nx);
float **convSepBrd(float **X, int irow, int icol,
		float *w1, int nw1, float *w2, int nw2, int *nrow, int *ncol);
float **gD(float **X, int irow, int icol, float scale, int ox, int oy,
		int *grow, int *gcol);
void runCED(float **traces, int ntrc, int nsp, float k,
		float obsscale, float intscale, float stepsize, int nosteps,
		int verbose);
float **tnldStep(float **L, int irow, int icol, float **a, float **b, float **c );
int *getTranslateImageIndex(int iLen, int dij);
float **translateImage(float **f, int irow, int icol, int di, int dj);
float **tnldStep(float **L, int irow, int icol, float **a, float **b, float **c );
#endif /* CEDSMOOTHINGLIB_H_ */
