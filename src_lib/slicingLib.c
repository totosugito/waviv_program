/*
 * slicingLib.c
 *
 *  Created on: Jan 27, 2012
 *      Author: toto
 */

#include "slicingLib.h"

/* CREATE TIME SLICING DATA
 * FORMAT DATA
 *
 *                   NTRC
 *    ----------------------------------->
 *  T |
 *  I |
 *  M |
 *  E |
 *
 */
void createTimeSlicing(FILE *segyinp, char *ctmpfile, int ntrc, int tmax,
		int nsp, int nsegy, int format, int endian,
		int nslice, int tslice, int **tslicearray, int vblock)
{
	int i, j, k, islice1, islice2, dslice;
	int idxvblock, totslice;
	float **dataslice=NULL;


	totslice = tslicearray[nslice-1][1] - tslicearray[0][0] + 1;
	dataslice = alloc2float(ntrc, tslice);
	for(i=0; i<nslice; i++) //loop over nslice
	{
		memset(dataslice[0], 0, tslice*ntrc*sizeof(float));
		gotoTraceSegyPosition(segyinp, 0, nsegy);

		islice1 = tslicearray[i][0];
		islice2 = tslicearray[i][1];
		dslice = tslicearray[i][2];

		idxvblock = 1;
		for (j=0; j<ntrc; j++)
		{
			if(j==idxvblock*vblock)
			{
				fprintf(stderr, "Split File At Time %i - %i / %i, Trace %i / %i \n",
						islice1, islice2, totslice, j, ntrc);
				idxvblock++;
			}
			readTraceSegy(segyinp, endian, nsp, format, nsegy, &tr);

			for(k=islice1; k<islice2; k++)
				dataslice[k-islice1][j] = tr.data[k];

		}
		savingSlicingFile(ctmpfile, dataslice, i, tslice, ntrc, tslicearray);
	}

	free2float(dataslice);
}

void savingSlicingFile(char *ctmpfile, float **dataslice, int islice, int tslice,
		int ntrc, int **tslicearray)
{
	char *tmpfileslice;
	FILE *tmpfileout;
	int i, dslice;

	tmpfileslice =createSlicingFileName(ctmpfile, tslicearray[islice][0], tslicearray[islice][1]);
	tmpfileout = fopen(tmpfileslice, "w");
	if(!tmpfileout) err("error opening temporary file %s", tmpfileslice);

	dslice = tslicearray[islice][2];
	for(i=0; i<dslice; i++)
		fwrite(dataslice[i], sizeof(float), ntrc, tmpfileout);

	fclose(tmpfileout);
	free(tmpfileslice);
}

void readSlicingFile(float **data, char *tmpfileslice, int islice, int nslice, int ntrc,
		int **tslicearray)
{
	FILE *fin=NULL;
	size_t result;
	int i, dslice;

	dslice = tslicearray[islice][2];
	fin = fopen(tmpfileslice, "r");
	if(!fin) err("error opening slicing file %s", tmpfileslice);

	for (i=0; i<dslice; i++)
	{
		fprintf(stderr, "SLICE [%i / %i] , Read Time Slicing %i / %i \n", islice+1, nslice, i+1, dslice);
		result = fread(data[i], sizeof(float), ntrc, fin);

		if(result!=ntrc)
			err("error reading file %s", tmpfileslice);
	}

	fclose(fin);
}

void removeSlicingFile(char *tmpfileslice)
{
	if(remove(tmpfileslice) < 0 )
		err("error removing temporary file %s", tmpfileslice);
}


void timeslice_to_Data2d(float *data,
		float **data2d, int winline, int wxline, pos3ddata **posdata)
{
	int i, j;
	int curr_inline, curr_xline, curr_trace;

	//memset(data2d[0], 0, winline*wxline*sizeof(float));
	for(i=0; i<winline; i++)
	{
		for(j=0; j<wxline; j++)
		{
			curr_inline = posdata[i][j].iline;
			curr_xline = posdata[i][j].xline;
			if(curr_inline>0 && curr_xline>0)
			{
				curr_trace = posdata[i][j].trace;
				data2d[i][j] = data[curr_trace];
			}
		}
	}
}

void Data2d_to_timeslice(float *data,
		float **data2d, int winline, int wxline, pos3ddata **posdata)
{
	int i, j;
	int curr_inline, curr_xline, curr_trace;

	for(i=0; i<winline; i++)
	{
		for(j=0; j<wxline; j++)
		{
			curr_inline = posdata[i][j].iline;
			curr_xline = posdata[i][j].xline;
			if(curr_inline>0 && curr_xline>0)
			{
				curr_trace = posdata[i][j].trace;
				data[curr_trace] = data2d[i][j];
			}
		}
	}
}

void mergeSlicingFile(char *cfin, float **data,
		int deltat, int ntrc, int islice, int nslice,
		int lendata, int **tslicearray, int vblock)
{
	FILE *fin=NULL;
	int i, j, dslice, idxvblock;
	float *tmpdata=NULL;
	size_t retseek;
	off64_t ipos, ntrcpos, nlendata;

	if(islice==0) 	fin = fopen(cfin, "w");
	else			fin = fopen(cfin, "r+");
	if(!fin) err("error opening file %s", cfin);

	dslice = tslicearray[islice][2];

	tmpdata = ealloc1float(dslice);
	memset(tmpdata, 0, dslice*sizeof(float));
	nlendata = (off64_t) (lendata*4);
	idxvblock = 0;
	for (i=0; i<ntrc; i++)
	{
		if(i==idxvblock*vblock)
		{
			fprintf(stderr, "SLICE [%i / %i] , Merge Time Slicing %i / %i traces \n", islice+1, nslice, i+1, ntrc);
			idxvblock++;
		}

		ntrcpos = ((off64_t) (deltat*4)) * ((off64_t)(i));
		ipos = ntrcpos + nlendata;
		retseek = fseeko64(fin, ipos , SEEK_SET);
		if(retseek) err("mergeSlicingFile : error set location to beginning pointer. %ld", ipos);

		for(j=0; j<dslice; j++)
			tmpdata[j] = data[j][i];

		fwrite(tmpdata, sizeof(float), dslice, fin);
	}
	fclose(fin);
	free1float(tmpdata);
}

void mergeSlicingFile_v2(char *cfin, float **data,
		int deltat, int ntrc, int islice, int nslice,
		int lendata, int **tslicearray, int vblock)
{
	FILE *tmpfin=NULL, *fin=NULL;
	int i, j, dslice, newlendata, idxvblock;
	float *tmpdata=NULL;
	size_t result;

	/*create temporary file */
	tmpfin = fopen("tmpfile.bin", "w");
	if(!tmpfin) err("error opening file tmpfile.bin");

	/*read the last temporary file*/
	if(islice>0)
	{
		fin = fopen(cfin, "r");
		if(!fin) err("error opening file %s", cfin);
	}

	dslice = tslicearray[islice][2];

	newlendata = lendata + dslice;
	tmpdata = ealloc1float(newlendata);
	memset(tmpdata, 0, newlendata*sizeof(float));
	idxvblock = 0;
	for (i=0; i<ntrc; i++)
	{
		if(i==idxvblock*vblock)
		{
			fprintf(stderr, "SLICE [%i / %i] , Merge Time Slicing %i / %i traces \n", islice+1, nslice, i+1, ntrc);
			idxvblock++;
		}

		if(islice>0)
			result = fread(tmpdata, sizeof(float), lendata, fin);

		for(j=0; j<dslice; j++)
			tmpdata[lendata+j] = data[j][i];

		fwrite(tmpdata, sizeof(float), newlendata, tmpfin);
	}

	/*close file */
	fclose(tmpfin);
	if(islice>0)	fclose(fin);

	/*saving data to the complete temporary file */
	result= rename( "tmpfile.bin", cfin );
	if ( result != 0 ) err( "Error renaming output file " );

	/*free allocated memory */
	free1float(tmpdata);
}

