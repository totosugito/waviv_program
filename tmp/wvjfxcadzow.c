/*
 * wvjfxcadzow.c
 *
 *  Created on: Jul 18, 2011
 *      Author: toto
 */

#include "wvjfxcadzow.h"

void processCadzow(float **data, int rank, int nx, int nz, int nf, int nfft, float d1)
{
	int j, fidx;
	double **dreal, **dimag;
	double *freqslice, **hankel, **filtered;
	int m, l, ncadzow;
	double *fcadzowreal=NULL, *fcadzowimag=NULL;

	/* allocated memory */
	dreal = ealloc2double(nf, nx);
	dimag = ealloc2double(nf, nx);

	/* fft data */
	fftProcessd(data, nz, nx, nf, nfft, dreal, dimag);

	m = (nx/2)+1;
	l = nx-m+1;
	ncadzow = m+l-1;
	freqslice = ealloc1double(nx);
	hankel = ealloc2double(l, m);
	filtered = ealloc2double(l, m);
	fcadzowreal = ealloc1double(ncadzow);
	fcadzowimag = ealloc1double(ncadzow);

	/* process cadzow */
	for(fidx=0; fidx<nf; fidx++){
		/* process real data */
		getfreqslice(dreal, nx, nz, fidx, freqslice);
		createHankel(freqslice, nx, hankel, m, l);
		rankApproximation(hankel, m, l, rank, filtered);
		avgdiagonal(filtered, m, l, fcadzowreal);

		/* process imaginer data */
		getfreqslice(dimag, nx, nz, fidx, freqslice);
		createHankel(freqslice, nx, hankel, m, l);
		rankApproximation(hankel, m, l, rank, filtered);
		avgdiagonal(filtered, m, l, fcadzowimag);

		for(j=0; j<ncadzow; j++)
		{
			dreal[j][fidx] = fcadzowreal[j];
			dimag[j][fidx] = fcadzowimag[j];
		}
		memset(fcadzowreal, 0, ncadzow*sizeof(double));
		memset(fcadzowimag, 0, ncadzow*sizeof(double));
	}

	/* invers fft */
	ifftProcessd(data, nz, nx, nf, nfft, dreal, dimag);

	/*free memory */
	free1double(fcadzowreal);
	free1double(fcadzowimag);
	free2double(dreal);
	free2double(dimag);
	free1double(freqslice);
	free2double(hankel);
	free2double(filtered);
}

//change cadzow algorithm
// 28 November 2011
void processCadzow_version2(float **data, int rank, int nx, int nz, int nf, int nfft, float d1)
{
	int j, fidx;
	double **dreal, **dimag;
	double *freqslice, **hankel, **filtered;
	int m, l, ncadzow;
	double *fcadzowreal=NULL, *fcadzowimag=NULL;

	/* allocated memory */
	dreal = ealloc2double(nf, nx);
	dimag = ealloc2double(nf, nx);

	/* fft data */
	fftProcessd(data, nz, nx, nf, nfft, dreal, dimag);

	m = (nx+1)/2;
	l = nx-m+1;
	ncadzow = m+l-1;
	freqslice = ealloc1double(nx);
	hankel = ealloc2double(l, m);
	filtered = ealloc2double(l, m);
	fcadzowreal = ealloc1double(ncadzow);
	fcadzowimag = ealloc1double(ncadzow);

	/* process cadzow */
	for(fidx=0; fidx<nf; fidx++){
		/* process real data */
		getfreqslice(dreal, nx, nz, fidx, freqslice);
		createHankel(freqslice, nx, hankel, m, l);
		rankApproximation_version2(hankel, m, l, rank, filtered);
		inverseHankel(filtered, m, l, fcadzowreal);

		/* process imaginer data */
		getfreqslice(dimag, nx, nz, fidx, freqslice);
		createHankel(freqslice, nx, hankel, m, l);
		rankApproximation_version2(hankel, m, l, rank, filtered);
		inverseHankel(filtered, m, l, fcadzowimag);

		for(j=0; j<nx; j++)
		{
			dreal[j][fidx] = fcadzowreal[j];
			dimag[j][fidx] = fcadzowimag[j];
		}
		memset(fcadzowreal, 0, ncadzow*sizeof(double));
		memset(fcadzowimag, 0, ncadzow*sizeof(double));
	}

	/* invers fft */
	ifftProcessd(data, nz, nx, nf, nfft, dreal, dimag);

	/*free memory */
	free1double(fcadzowreal);
	free1double(fcadzowimag);
	free2double(dreal);
	free2double(dimag);
	free1double(freqslice);
	free2double(hankel);
	free2double(filtered);
}

void rankApproximation_version2(double **data, int rowdata, int coldata, int rank, double **filtered)
{
	double **eigenimage;
	int iter, row, col;

	memset((void*)filtered[0], 0,rowdata*coldata*sizeof(double));

	for(iter=0;iter<rank;iter++){
		eigenimage = getrank(data, rowdata, coldata, iter);

		for(row=0;row<rowdata;row++)
			for(col=0;col<coldata;col++)
				filtered[row][col]+=eigenimage[row][col];

		free2double(eigenimage);
	}
}

void rmsMatching(double *ref, int nref, double *target, int ntarget)
{
	int i;
	double meanref = 0.0;
	double meantarget = 0.0;

	for(i=0;i<nref;i++){
		meanref+=ref[i];
		meantarget+=target[i];
	}
	meanref/=nref;
	meantarget/=ntarget;

	double rmsref =0.0;
	double rmstarget=0.0;

	for(i=0;i<ntarget;i++){
		rmsref+=pow((ref[i]-meanref),2.0);
		rmstarget+=pow((ref[i]-meantarget),2.0);
	}

	rmsref = sqrt(rmsref)/ntarget;
	rmstarget = sqrt(rmstarget)/nref;

	for(i=0;i<ntarget;i++){
		target[i]*=rmsref;
		target[i]*=rmstarget;
	}
}

void avgdiagonal(double **data, int rowdata, int coldata, double *sumcol)
{
	double **recon=NULL;
	int row, col, i;
	int colrecon;

	colrecon = coldata+rowdata-1;
	recon = ealloc2double(colrecon, rowdata);
	memset((void*)recon[0], 0, colrecon*rowdata*sizeof(double));

	for(row=0;row<rowdata;row++)
		for(col=0;col<coldata;col++)
			recon[row][col+row]=data[row][col];

	for(i=0;i<colrecon;i++)
		sumcol[i]=fsumcol(recon, rowdata, i);

	if(colrecon%2==0){
		int span = (colrecon) /2;
		int idx=0;
		for(col=1;col<=span;col++){
			sumcol[idx]/=abs(col);
			idx++;
		}
		for(col=span;col>0;col--){
			sumcol[idx]/=abs(col);
			idx++;
		}
	}else{
		int span = (colrecon-1) /2;
		int idx=0;
		for(col=1;col<=span;col++){
			sumcol[idx]/=abs(col);
			idx++;
		}
		for(col=span+1;col>0;col--){
			sumcol[idx]/=abs(col);
			idx++;
		}
	}

	free2double(recon);
}

double fsumcol(double **recon, int rowrecon, int colrecon)
{
	int row;
	double sum=0.0;

	for(row=0;row<rowrecon;row++){
		sum+=recon[row][colrecon];
	}
	return sum;
}

double *avgdiagonaldep(double **data, int rowdata, int coldata, int *nout)
{
	double **recon=NULL;
	double *avgval=NULL;
	int row, i, navgval;

	navgval = rowdata*2-1;
	recon = ealloc2double(navgval, rowdata);
	memset((void*)recon[0], 0, navgval*rowdata*sizeof(double));
	for(row=0;row<rowdata;row++)
		memcpy(recon[row], data[row]+row, rowdata*sizeof(double));


	avgval = ealloc1double(navgval);
	int val=-rowdata+1;
	for(i=0;i<navgval;i++){
		avgval[i]=avgcol(recon, rowdata, (rowdata*2-1), val, i);
		val++;
	}

	free2double(recon);

	(*nout) = navgval;
	return avgval;
}

double avgcol(double **recon, int rowrecon, int colrecon, int lag, int col)
{
	int row;
	double out;

	double retval=0.0;
	int lagu = abs(lag);
	double denom = (double) (rowrecon-lagu);

	if(lag<=0){
		for(row=0;row<rowrecon-(lagu);row++){
			retval+=recon[row][col];
		}
	}else if(lag>0){
		for(row=lagu;row<rowrecon;row++){
			retval+=recon[row][col];
		}
	}

	out = (double) (retval/denom);
	return out;
}

int indx(int n,int xpos)
{
	if(xpos==0){
		return n;
	}else{
		return (n - abs(xpos));
	}
}

void getfreqslice(double **data, int rowdata, int coldata, int fidx, double *fslice)
{
	int row;

	for(row=0;row<rowdata;row++)
		fslice[row]=data[row][fidx];
}

float **tofloat(double **in, int rowin, int colin)
{
	int row, col;
	float **out=NULL;

	out = ealloc2float(colin, rowin);

	for(row=0;row<rowin;row++){
		for(col=0;col<colin;col++){
			out[row][col]=(float) in[row][col];
		}
	}
	return out;
}

double **todouble(float **in, int rowin, int colin)
{
	int row, col;
	double **out=NULL;

	out = ealloc2double(colin, rowin);
	for(row=0;row<rowin;row++){
		for(col=0;col<colin;col++){
			out[row][col]= (double) in[row][col];
		}
	}
	return out;
}

void createHankel(double *in, int nin, double **hankel, int m, int l)
{
	int row, col;

	int idx;
	for(row=0;row<m;row++){
		idx=row;
		for(col=0;col<l;col++){
			hankel[row][col]=in[idx];
			idx++;
		}
	}
}

double **rankreduction(double **data, int rowdata, int coldata, int rank)
{
	int  row, col;
	double **u;
	double **v, *s;
	double *U1, *V1;
	double **su=NULL;

	u = ealloc2double(rowdata, rowdata);
	v = ealloc2double(coldata, coldata);
	s = ealloc1double(coldata);
	U1 = ealloc1double(rowdata);
	V1 = ealloc1double(coldata);

	/* process svd lapack */
	svdlapackd(data, rowdata, coldata, u, s, v);

	for (row=0; row<rowdata; row++) U1[row] = u[row][rank];
	for (col=0; col<coldata; col++) V1[col] = v[col][rank];

	su = ealloc2double(coldata, rowdata);
	for(row=0;row<rowdata;row++)
		for(col=0;col<coldata;col++)
			su[row][col] = (V1[col]*U1[row]*s[rank]);

	/* free allocated memory */
	free2double(u);
	free2double(v);
	free1double(s);
	free1double(U1);
	free1double(V1);

	return (su);
}

void rankApproximation(double **data, int rowdata, int coldata, int rank, double **filtered)
{
	double **eigenimage;
	int iter, row, col;

	memset((void*)filtered[0], 0,rowdata*coldata*sizeof(double));

	for(iter=0;iter<rank;iter++){
		eigenimage = rankreduction(data, rowdata, coldata, iter);

		for(row=0;row<rowdata;row++)
			for(col=0;col<coldata;col++)
				filtered[row][col]+=eigenimage[row][col];

		free2double(eigenimage);
	}
}

double **getrank(double **data, int rowdata, int coldata, int rank)
{
	int row,col;
	double **u;
	double **v, *s;
	double *U1, *V1;
	double **su=NULL;

	u = ealloc2double(rowdata, rowdata);
	v = ealloc2double(coldata, coldata);
	s = ealloc1double(coldata);
	U1 = ealloc1double(rowdata);
	V1 = ealloc1double(coldata);

	/* process svd lapack */
	svdlapackd(data, rowdata, coldata, u, s, v);

	for (row=0; row<rowdata; row++) U1[row] = u[row][rank];
	for (col=0; col<coldata; col++) V1[col] = v[col][rank];

	su = ealloc2double(coldata, rowdata);
	for(row=0;row<rowdata;row++)
		for(col=0;col<coldata;col++)
			su[row][col] = (V1[col]*U1[row]*s[rank]);

	/* free allocated memory */
	free2double(u);
	free2double(v);
	free1double(s);
	free1double(U1);
	free1double(V1);

	return su;
}

void inverseHankel(double **data, int rowdata, int coldata, double *sumcol)
{
	int m, l;
	double *val, *avg;
	int i, j, cnt, mdex, dex, mnt;
	int dx;

	m = rowdata;
	l = coldata;

	val = alloc1double(l*m);
	avg = alloc1double(l+m-1);
	memset(val, '\0', l*m*sizeof(double));
	memset(avg, '\0', (l+m-1)*sizeof(double));

	dx = 0;
	for(i=0;i<m;i++)
	{
		for(j=0;j<l;j++)
		{
			val[dx]=data[i][j];
			dx++;
		}
	}

	for(i=0;i<m;i++)
	{
		cnt=0;
		for(j=0;j<i+1;j++)
		{
			avg[i]+=val[i+(j*(l-1))];
			cnt++;
		}
		avg[i]/=cnt;
	}

	if(m==l)
	{
		cnt  = l-1;
		mdex = 2;
		for(i=m;i<l+m-1;i++)
		{
			dex = (mdex*l)-1;
			mnt = 0;
			for(j=0;j<cnt;j++)
			{
				mnt++;
				avg[i]+=val[dex];
				dex+=(l-1);
			}
			avg[i]/=cnt;
			cnt--;
			mdex++;
		}

	}
	else
	{
		cnt  = m;
		mdex = 1;
		for(i=m;i<l+m-1;i++)
		{
			dex = (mdex*l)-1;
			mnt = 0;
			for(j=0;j<cnt;j++)
			{
				mnt++;
				avg[i]+=val[dex];
				dex+=(l-1);
			}
			avg[i]/=cnt;
			cnt--;
			mdex++;
		}
	}

	memcpy(sumcol, avg, (l+m-1)*sizeof(double));
	free1double(val);
	free1double(avg);
}

//void fftProcessd(float **data, int nsp, int nx, int nf, int nfft,
//		double **dreal, double **dimag)
//{
//	int it, ifq, itr;
//	complex *ct;		/* complex transformed trace (window)  	*/
//	float *tidataw;       	/* real trace in time window - input   	*/
//	float *ttidataw;      	/* real trace in time window - input   	*/
//
//	/* space allocation */
//	ct = ealloc1complex(nfft);
//	tidataw=ealloc1float(nfft);
//	ttidataw=ealloc1float(nfft);
//
//	for(itr=0; itr<nx; itr++)  //loop over trace
//	{
//		memcpy(tidataw, data[itr], nsp*sizeof(float));
//
//		memset((void *) (tidataw + nsp), 0, (nfft-nsp)*FSIZE);
//		memset((void *) ct, 0, nfft*sizeof(complex));
//
//		/* FFT from t to f */
//		for (it=0;it<nfft;it++)
//			ttidataw[it]=(it%2 ? -tidataw[it] : tidataw[it]);
//		pfarc(1, nfft, ttidataw, ct);
//
//		/* Store values */
//		for (ifq = 0; ifq < nf; ifq++) {
//			dreal[itr][ifq] = (double) ct[nf-1-ifq].r;
//			dimag[itr][ifq] = (double) ct[nf-1-ifq].i;
//		}
//	}
//
//	/* free allocated memory */
//	free1float(tidataw);
//	free1float(ttidataw);
//	free1complex(ct);
//}
//
//void ifftProcessd(float **data, int nsp, int nx, int nf, int nfft,
//		double **dreal, double **dimag)
//{
//	int it, ifq, itr, itt;
//	complex *ct;		/* complex transformed trace (window)  	*/
//	complex *freqv;		/* frequency vector		      	*/
//	float *todataw;       	/* real trace in time window - output   */
//
//	/* space allocation */
//	ct = ealloc1complex(nfft);
//	freqv = ealloc1complex(nfft);
//	todataw = ealloc1float(nfft);
//
//	for(itr=0; itr<nx; itr++)  //loop over trace
//	{
//	/* select data */
//		for (ifq=0,itt=nf-1;ifq<nf;ifq++,itt--){
//			freqv[ifq].r = (float) dreal[itr][itt];
//			freqv[ifq].i = (float) dimag[itr][itt];
//		}
//
//		memset((void *) (freqv+nf), 0, (nfft-nf)*sizeof(complex));
//		memset((void *) todataw, 0, nfft*FSIZE);
//
//		/* FFT back from f to t and scaling */
//		pfacr(-1, nfft, freqv, todataw);
//		for (it=0;it<MIN(nsp,nfft);it++)
//		{
//			data[itr][it] = (it%2 ? (-todataw[it]/nfft) : (todataw[it]/nfft));
//		}
//	}
//
//	/* free allocated memory */
//	free1float(todataw);
//	free1complex(freqv);
//	free1complex(ct);
//}
