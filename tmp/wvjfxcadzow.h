/*
 * wvjfxcadzow.h
 *
 *  Created on: Jul 18, 2011
 *      Author: toto
 */

#ifndef WVJFXCADZOW_H_
#define WVJFXCADZOW_H_
#include "svdlapack.h"
#include "segy_lib.h"

#define CFL 0.7
#define LOOKFAC	2	/* Look ahead factor for npfaro	  */
#define PFA_MAX	720720	/* Largest allowed nfft	          */
#define SF_PI (3.14159265358979323846264338328)

void processCadzow(float **data, int rank, int nx, int nz, int nf, int nfft, float d1);
void processCadzow_version2(float **data, int rank, int nx, int nz, int nf, int nfft, float d1);
//void fftProcessd(float **data, int nsp, int nx, int nf, int nfft,
//		double **dreal, double **dimag);
//void ifftProcessd(float **data, int nsp, int nx, int nf, int nfft,
//		double **dreal, double **dimag);

void rmsMatching(double *ref, int nref, double *target, int ntarget);
void avgdiagonal(double **data, int rowdata, int coldata, double *sumcol);
double fsumcol(double **recon, int rowrecon, int colrecon);
double *avgdiagonaldep(double **data, int rowdata, int coldata, int *nout);
double avgcol(double **recon, int rowrecon, int colrecon, int lag, int col);
int indx(int n,int xpos);
void getfreqslice(double **data, int rowdata, int coldata, int fidx, double *fslice);
float **tofloat(double **in, int rowin, int colin);
double **todouble(float **in, int rowin, int colin);
void createHankel(double *in, int nin, double **hankel, int m, int l);
double **rankreduction(double **data, int rowdata, int coldata, int rank);
void rankApproximation(double **data, int rowdata, int coldata, int rank, double **filtered);
void rankApproximation_version2(double **data, int rowdata, int coldata, int rank, double **filtered);
void inverseHankel(double **data, int rowdata, int coldata, double *sumcol);
double **getrank(double **data, int rowdata, int coldata, int rank);
#endif /* WVJFXCADZOW_H_ */
