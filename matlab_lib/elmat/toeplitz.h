/*
 * toeplitz.h
 *
 *  Created on: Feb 6, 2012
 *      Author: toto
 */

#ifndef TOEPLITZ_H_
#define TOEPLITZ_H_

#include "par.h"

float **toeplitz(float *A, int nA);
#endif /* TOEPLITZ_H_ */
