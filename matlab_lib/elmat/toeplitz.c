/*
 * toeplitz.cpp
 *
 *  Created on: Feb 6, 2012
 *      Author: toto
 */
/*
 * TOEPLITZ Toeplitz matrix.
%   TOEPLITZ(C,R) is a non-symmetric Toeplitz matrix having C as its
%   first column and R as its first row.
%
%   TOEPLITZ(R) is a symmetric Toeplitz matrix for real R.
%   For a complex vector R with a real first element, T = toeplitz(r)
%   returns the Hermitian Toeplitz matrix formed from R. When the
%   first element of R is not real, the resulting matrix is Hermitian
%   off the main diagonal, i.e., T_{i,j} = conj(T_{j,i}) for i ~= j.
%
%   Class support for inputs C,R:
%      float: double, single
%
%   See also HANKEL.

%   Revised 10-8-92, LS - code from A.K. Booer.
%   Copyright 1984-2008 The MathWorks, Inc.
%   $Revision: 5.11.4.3 $  $Date: 2008/06/20 08:00:11 $
 */

#include "toeplitz.h"


float **toeplitz(float *A, int nA)
{
	int i, j;
	int pos1, pos2, idx;
	float **C;

	C = alloc2float(nA, nA);

	for (i=0; i<nA; i++)
	{
		pos1 = i;
		pos2 = nA;
		idx = 0;
		for (j=pos1; j<pos2; j++)
		{
			C[i][j] = A[idx];
			C[nA-i-1][nA-j-1] = A[idx];
			idx = idx+1;
		}
	}

	return(C);
}
