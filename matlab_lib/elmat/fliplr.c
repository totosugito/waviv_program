/*
 * fliplr.c
 *
 *  Created on: May 24, 2010
 *      Author: toto
 */

#include "fliplr.h"

/*flip data array*/
int *fliplri(int *A, int nx)
{
	int i;
	int *out=NULL;

	out = alloc1int(nx);

	for(i=0; i<nx; i++)
		out[i] = A[nx-1-i];

	return out;
}

/*flip data array*/
float *fliplrf(float *A, int nx)
{
	int i;
	float *out=NULL;

	out = alloc1float(nx);

	for(i=0; i<nx; i++)
		out[i] = A[nx-1-i];

	return out;
}
