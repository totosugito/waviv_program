/*
 * fliplr.h
 *
 *  Created on: May 24, 2010
 *      Author: toto
 */

#ifndef FLIPLR_H_
#define FLIPLR_H_
#include "par.h"

int *fliplri(int *A, int nx);
float *fliplrf(float *A, int nx);

#endif /* FLIPLR_H_ */
