/*
 * xcorr.c
 *
 *  Created on: Feb 6, 2012
 *      Author: toto
 */

#include "xcorr.h"

float *xcorr(float *A, int nA, float *B, int nB, int *nC)
{
	float *bB=NULL, *C=NULL;
	int lenC;

	bB = fliplrf(B, nB);
	C = convf(A, bB, nA, nB, &lenC);

	free1float(bB);
	(*nC) = lenC;
	return(C);
}

