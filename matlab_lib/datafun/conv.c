/*
 * conv.cpp
 *
 *  Created on: Feb 6, 2012
 *      Author: toto
 */

#include "conv.h"

//convolution algorithm
float *convf(float *A, float *B, int lenA, int lenB, int *lenC)
{
    int nconv;
    int i, j, i1;
    float tmp;
    float *C=NULL;

    //allocated convolution array
    nconv = lenA+lenB-1;
    C =  (float*) calloc(nconv, sizeof(float));

    //convolution process
    for (i=0; i<nconv; i++)
    {
        i1 = i;
        tmp = 0.0;
        for (j=0; j<lenB; j++)
        {
            if(i1>=0 && i1<lenA)
                tmp = tmp + (A[i1]*B[j]);

            i1 = i1-1;
            C[i] = tmp;
        }
    }

    //get length of convolution array
    (*lenC) = nconv;

    //return convolution array
    return(C);
}

//convolution algorithm
float *conv1f(float *A, int lenA, float *B, int lenB, int *lenC)
{
    int nconv;
    int i, j, i1;
    float tmp;
    float *C=NULL;

    //allocated convolution array
    nconv = lenA+lenB-1;
    C =  (float*) calloc(nconv, sizeof(float));

    //convolution process
    for (i=0; i<nconv; i++)
    {
        i1 = i;
        tmp = 0.0;
        for (j=0; j<lenB; j++)
        {
            if(i1>=0 && i1<lenA)
                tmp = tmp + (A[i1]*B[j]);

            i1 = i1-1;
            C[i] = tmp;
//            if(isnan(C[i]))
//            {
//            	for (i=0; i<lenA; i++)
//            		printf("%i. %f \n", i, A[i]);
//            	err("%f %f %f", tmp, A[i1], B[j]);
//            }
        }
    }

    //get length of convolution array
    (*lenC) = nconv;

    //return convolution array
    return(C);
}

float **conv2f(float *V1, int nv1, float *V2, int nv2,
		float **X, int irow, int icol, int mode,
		int *nrow, int *ncol)
{
	int i, j;
	int lenC;
	int b1, b2, b3;
	float *tmp_row=NULL, *tmp_col, *tmp_conv;
	float **tmpA=NULL, **tmpB;
	float **result=NULL;
	int ra, ca;
	int ir=0, ic=0;

	//get new size vector and array
	b1 = irow;
	b2 = icol + nv2 - 1;
	b3 = b1 + nv1 - 1;

	// COVOLUSI I
	// covolusi every row of array input with vector vector2
	tmp_row = alloc1float(icol);
	tmpA = alloc2float(b2, b1);
	for (i=0; i<irow; i++)
	{
		memcpy(tmp_row, X[i], icol*sizeof(float));
		tmp_conv = conv1f(tmp_row, icol, V2, nv2, &lenC);

//		for(j=0; j<nv2; j++)
//		{
//			if(isnan(V2[j]))
//				err("V2 is nan ");
//		}
//
//		for(j=0; j<icol; j++)
//		{
//			if(isnan(tmp_row[j]))
//				err("tmp_row is nan ");
//
//			if(isinf(tmp_row[j]))
//				err("tmp_row is inf");
//		}
//
//		for(j=0; j<lenC; j++)
//		{
//			if(isnan(tmp_conv[j]))
//				err("tmp_conv is nan ");
//		}

		//save tmpvc to new array (tmpa)
		memcpy(tmpA[i], tmp_conv, b2*sizeof(float));

		free1float(tmp_conv);
	}

	// COVOLUSI II
	// covolusi every column of array from CONV1 (tmpa) with vector vector1
	tmp_col = alloc1float(b1);
	tmpB = alloc2float(b2, b3);
	for(i=0; i<b2; i++)
	{
		for(j=0; j<b1; j++)
		{
			tmp_col[j] = tmpA[j][i];
//			if(isnan(tmpA[j][i]))
//				err("tmpA is nan ");
		}
		tmp_conv = conv1f(tmp_col, b1, V1, nv1, &lenC);

		for(j=0; j<b3; j++)
		{
			tmpB[j][i] = tmp_conv[j];
//			if(isnan(tmpB[j][i]))
//				err("tmpB is nan ");
		}

		free1float(tmp_conv);
	}

	if(mode==1) //same mode
	{
		result = alloc2float(icol, irow);
		ir = (int) (ceil((b3-irow)/2.0));
		ic = (int) (ceil((b2-icol)/2.0));
		for(i=0; i<irow; i++)
			for(j=0; j<icol; j++)
				result[i][j] = tmpB[ir+i][ic+j];

		(*nrow) = irow;
		(*ncol) = icol;
	}
	else if(mode==2) //valid mode
	{
		ra = irow-nv1+1;
		ca = icol-nv2+1;
		ir = (b3-ra)/2;
		ic = (b2-ca)/2;
		if(irow<nv1 || icol<nv2)
		{
			(*nrow) = 0;
			(*ncol) = 0;
			return(result);
		}

		result = alloc2float(ca, ra);
		for(i=0; i<ra; i++)
			for(j=0; j<ca; j++)
				result[i][j] = tmpB[ir+i][ic+j];

		(*nrow) = ra;
		(*ncol) = ca;
	}
	else //full mode
	{
		result = alloc2float(b2, b3);
		memcpy(result[0], tmpB[0], b2*b3*sizeof(float));
		(*nrow) = b3;
		(*ncol) = b2;
	}

	free1float(tmp_row);
	free1float(tmp_col);
	free2float(tmpA);
	free2float(tmpB);
	return(result);
}

