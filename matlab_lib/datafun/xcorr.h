/*
 * xcorr.h
 *
 *  Created on: Feb 6, 2012
 *      Author: toto
 */

#ifndef XCORR_H_
#define XCORR_H_

#include "par.h"
#include "../elmat/fliplr.h"
#include "conv.h"

float *xcorr(float *A, int nA, float *B, int nB, int *nC);
#endif /* XCORR_H_ */
