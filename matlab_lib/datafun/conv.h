/*
 * conv.h
 *
 *  Created on: Feb 6, 2012
 *      Author: toto
 */

#ifndef CONV_H_
#define CONV_H_
#include <stdlib.h>
#include <stdio.h>
#include <par.h>
float *convf(float *A, float *B, int lenA, int lenB, int *lenC);

float *conv1f(float *A, int lenA, float *B, int lenB, int *lenC);
float **conv2f(float *V1, int nv1, float *V2, int nv2,
		float **X, int irow, int icol, int mode,
		int *nrow, int *ncol);
#endif /* CONV_H_ */
