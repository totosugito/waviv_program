#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

float *upsample_f1(float *x, int nx, int N);
void upsample_f2(float *x, int nx, int N, float *result, int *Nr);
