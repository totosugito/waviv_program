/*
 * orthfilt.c
 *
 *  Created on: Dec 10, 2012
 *      Author: toto
 */
#include "cWavelet.h"

//function [Lo_D,Hi_D,Lo_R,Hi_R] = orthfilt(W,P)
//%ORTHFILT Orthogonal wavelet filter set.
//%   [LO_D,HI_D,LO_R,HI_R] = ORTHFILT(W) computes the
//%   four filters associated with the scaling filter W
//%   corresponding to a wavelet:
//%   LO_D = decomposition low-pass filter
//%   HI_D = decomposition high-pass filter
//%   LO_R = reconstruction low-pass filter
//%   HI_R = reconstruction high-pass filter.
//%
//%   See also BIORFILT, QMF, WFILTERS.
//
//%   M. Misiti, Y. Misiti, G. Oppenheim, J.M. Poggi 12-Mar-96.
//%   Last Revision: 13-May-2003.
//%   Copyright 1995-2004 The MathWorks, Inc.
//% $Revision: 1.11.4.2 $

void orthfilt_f(float *x, int nx, int p,
		float *Lo_D, float *Hi_D, float *Lo_R, float *Hi_R)
{
	float sumx;
	int i;
	float *tmpX;
	float *tHi_R, *tHi_D, *tLo_D;

	sumx = 0.0;
	for(i=0; i<nx; i++)
		sumx += x[i];

	// Normalize filter sum.
	tmpX = (float*) calloc(nx, sizeof(float));
	for(i=0; i<nx; i++)
		tmpX[i] = x[i]/sumx;

	// Associated filters.
	for(i=0; i<nx; i++)
		Lo_R[i] = ((float)(sqrtf(2.0)))*tmpX[i];
	tHi_R = qmf_f(Lo_R, nx, p);
	tHi_D = wrev_f(tHi_R, nx);
	tLo_D = wrev_f(Lo_R, nx);

	memcpy(Hi_R, tHi_R, nx*sizeof(float));
	memcpy(Hi_D, tHi_D, nx*sizeof(float));
	memcpy(Lo_D, tLo_D, nx*sizeof(float));

	free(tHi_R);
	free(tHi_D);
	free(tLo_D);
	free(tmpX);
}


