/*
 * qmf.c
 *
 *  Created on: Dec 10, 2012
 *      Author: toto
 */

#include "cWavelet.h"

//function y = qmf(x,p)
//%QMF    Quadrature mirror filter.
//%   Y = QMF(X,P) changes the signs of the even index entries
//%   of the reversed vector filter coefficients X if P is even.
//%   If P is odd the same holds for odd index entries.
//%
//%   Y = QMF(X) is equivalent to Y = QMF(X,0).
//
//%   M. Misiti, Y. Misiti, G. Oppenheim, J.M. Poggi 12-Mar-96.
//%   Last Revision: 24-Jul-2007.
//%   Copyright 1995-2007 The MathWorks, Inc.
//% $Revision: 1.10.4.3 $

float *qmf_f(float *x, int nx, int p)
{
	int i;
	int first;
	float *result;

	result = wrev_f(x, nx);

	// Compute quadrature mirror filter.
	first = 2-p%2;
	for(i=first-1; i<nx; i=i+2)
		result[i] = -result[i];

	return(result);
}

int *qmf_i(int *x, int nx, int p)
{
	int i;
	int first;
	int *result;

	result = wrev_i(x, nx);

	// Compute quadrature mirror filter.
	first = 2-p%2;
	for(i=first-1; i<nx; i=i+2)
		result[i] = -result[i];

	return(result);
}
