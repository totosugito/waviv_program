/*
 * cWavelet.h
 *
 *  Created on: Dec 10, 2012
 *      Author: toto
 */
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

float *wrev_f(float *x, int nx);
int *wrev_i(int *x, int nx);

float *qmf_f(float *x, int nx, int p);
int *qmf_i(int *x, int nx, int p);

void orthfilt_f(float *x, int nx, int p,
		float *Lo_D, float *Hi_D, float *Lo_R, float *Hi_R);
float *get_daubechies(int id, int *n);

