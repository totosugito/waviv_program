/*
 * wrev.c
 *
 *  Created on: Dec 10, 2012
 *      Author: toto
 */
#include "cWavelet.h"

//%WREV Flip vector.
//%   Y = WREV(X) reverses the vector X.
//%
//%   See also FLIPLR, FLIPUD.
//
//%   M. Misiti, Y. Misiti, G. Oppenheim, J.M. Poggi 01-May-96.
//%   Last Revision: 13-May-2003.
//%   Copyright 1995-2004 The MathWorks, Inc.
//% $Revision: 1.10.4.2 $

float *wrev_f(float *x, int nx)
{
	int i, idx;
	float *result;

	result = (float*) calloc(nx, sizeof(float));

	idx = nx-1;
	for(i=0; i<nx; i++){
		result[i] = x[idx];
		idx--;
	}

	return(result);
}

int *wrev_i(int *x, int nx)
{
	int i, idx;
	int *result;

	result = (int*) calloc(nx, sizeof(int));

	idx = nx-1;
	for(i=0; i<nx; i++){
		result[i] = x[idx];
		idx--;
	}

	return(result);
}


