/*
 * cWavelet.c
 *
 *  Created on: Dec 10, 2012
 *      Author: toto
 */

#include "cWavelet.h"

float *get_daubechies(int id, int *n)
{
	float *result;
	int n1;

	n1= 2*id;
	result = (float*) calloc(n1, sizeof(float));
	if(id==1){
		result[0] = 0.5;
		result[1] = 0.5;
	}
	else if(id==2) {
		result[0] = 0.3415;
		result[1] = 0.5915;
		result[2] = 0.1585;
		result[3] = -0.0915;
	}
	else if(id==3) {
		result[0] = 0.2352;
		result[1] = 0.5706;
		result[2] = 0.3252;
		result[3] = -0.0955;
		result[4] = -0.0604;
		result[5] = 0.0249;
	}
	(*n) = n1;
	return(result);
}



