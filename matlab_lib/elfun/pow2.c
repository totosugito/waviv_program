/*
 * pow2.c
 *
 *  Created on: Feb 6, 2012
 *      Author: toto
 */

#include "pow2.h"

float pow2f(float A)
{
	double dA;
	float out;

	dA = (double) A;
	out = (float) (pow(2.0, dA));

	return(out);
}

float *pow2vf(float *A, int nA)
{
	float *B;
	int i;

	B = alloc1float(nA);
	for(i=0; i<nA; i++)
		B[i] = pow2f(A[i]);

	return(B);
}
