/*
 * nextpow2.c
 *
 *  Created on: Feb 6, 2012
 *      Author: toto
 */
/*
 * %NEXTPOW2 Next higher power of 2.
%   NEXTPOW2(N) returns the first P such that 2.^P >= abs(N).  It is
%   often useful for finding the nearest power of two sequence
%   length for FFT operations.
%
%   Class support for input N:
%      float: double, single
%
%   See also LOG2, POW2.

%   Copyright 1984-2009 The MathWorks, Inc.
%   $Revision: 5.11.4.4 $  $Date: 2009/07/06 20:37:15 $
*/

#include "nextpow2.h"

int nextpow2f(float A)
{
	float lv, ln;
	float aA;
	int p;
	int k;

	aA = A>0 ? A:-A;
	lv = log2(aA, &ln);

	// Check for exact powers of 2.
	if(f==0.5)
	{
		k=1;
		p = ln-1;
	}
	else
	{
		k=0;
		p = ln;
	}
	return(p);
}
