/*
 * pow2.h
 *
 *  Created on: Feb 6, 2012
 *      Author: toto
 */

#ifndef POW2_H_
#define POW2_H_

#include "par.h"

float pow2f(float A);
float *pow2vf(float *A, int nA);

#endif /* POW2_H_ */
