/*
 * nextpow2.h
 *
 *  Created on: Feb 6, 2012
 *      Author: toto
 */

#ifndef NEXTPOW2_H_
#define NEXTPOW2_H_

#include "log2.h"

int nextpow2f(float A);
#endif /* NEXTPOW2_H_ */
