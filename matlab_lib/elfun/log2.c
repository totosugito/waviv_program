/*
 * log2.c
 *
 *  Created on: Feb 6, 2012
 *      Author: toto
 */

#include "log2.h"

float log2f(float A, int *n)
{
	double dA;
	float out;
	int iexp;

	dA = (double) A;
	out = frexp(dA, &iexp);

	(*n) = iexp;
	return(out);
}
