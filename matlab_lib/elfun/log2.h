/*
 * log2.h
 *
 *  Created on: Feb 6, 2012
 *      Author: toto
 */

#ifndef LOG2_H_
#define LOG2_H_

#include "par.h"

float log2f(float A, int *n);
#endif /* LOG2_H_ */
