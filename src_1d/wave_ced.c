/*
 * wave_ced.c
 *
 *  Created on: Dec 18, 2012
 *      Author: toto
 */
#include "../src_lib/cedSmoothingLib.h"
#include "../src_lib/segy_lib.h"
#include <time.h>

const char *sdoc[] = {
		"",
		" WAVE_CED : COHERENCE ENHANCING DIFFISION ",
		"",
		" ftype=0        0=SU file, 1=SEGY file                               	",
		" endian=0       set =0 for little-endian machines(PC's,DEC,etc.)",
		" obscale=5.0    sigma",
		" intscale=0.7   sigma derivative",
		" stepsize=0.2   coefficient diffusion",
		" k=0.001        coefficient exponent ",
		" nosteps=10     number of iteration",
		"",
		" Sample Command :",
		" wave_ced <input.su ftype=0 endian=0 obscale=5.0 intscale=0.7 stepsize=0.2 > output.su",
		"",
		NULL};

int main(int argc, char **argv)
{
	int ftype;
	int endian;
	float obsscale;
	float intscale;
	float stepsize;
	float k;
	int nosteps;
	int verbose;

	int i;
	int nsegy, ntrc, nsp, format=1;
	char ebcdic[3200];
	float **traces;
	char **headers;
	div_t divresult;
	time_t t1,t2;

	/* hook up getpar to handle the parameters */
	initargs(argc,argv);
	requestdoc(1);
	if (!getparint("ftype",&ftype)) ftype=0;
	if (!getparint("endian",&endian)) endian=0;
	if (!getparint("verbose",&verbose)) verbose=0;

	if (!getparfloat("obsscale",&obsscale)) obsscale=5.0;
	if (!getparfloat("intscale",&intscale)) intscale=0.7;
	if (!getparfloat("stepsize",&stepsize)) stepsize=0.2;
	if (!getparfloat("k",&k)) k=0.001;
	if (!getparint("nosteps",&nosteps)) nosteps=10;

	t1 = time(NULL);

	/*read header file */
	if(ftype) //segy file
	{
		readEbcdicHeader(stdin, ebcdic);
		readBinaryHeader(stdin, endian, &bh, &nsegy);
		ntrc = getNumberOfTraceSegyFile(stdin, nsegy);

		nsp = bh.hns;
		format = bh.format;

		bh.format = 1;
		writeEbcdicHeader(stdout, ebcdic);
		writeBinaryHeader(stdout, endian, &bh);
		gotoTraceSegyPosition(stdin, 0, nsegy);
	}
	else
	{
		gettr(&tr);
		nsp = tr.ns;
		ntrc = getNumberOfTraceSuFile(stdin);
		rewind(stdin);
	}

	traces = alloc2float(nsp, ntrc);
	headers = alloc2char(HDRBYTES, ntrc);

	//read traces
	for(i=0; i<ntrc; i++)
	{
		if(ftype==1) readTraceSegy(stdin, endian, nsp, format, nsegy, &tr);
		else gettr(&tr);

		memcpy(traces[i], tr.data, nsp*sizeof(float));
		memcpy(headers[i], (char*)&tr, HDRBYTES*sizeof(char));
	}
//	print2f("dtnldStep", traces, 10, 10);
	//run CED smoothing
	runCED(traces, ntrc, nsp, k, obsscale, intscale, stepsize, nosteps, verbose);

	//write traces
	for(i=0; i<ntrc; i++)
	{
		memcpy((char*)&tr, headers[i], HDRBYTES);
		memcpy(tr.data, traces[i], nsp*sizeof(float));

		if(ftype==1) writeTraceSegy(stdout, endian, &tr, nsegy, nsp);
		else puttr(&tr);
	}

	free2float(traces);
	free2char(headers);

	t2 = time(NULL);
	divresult = div (t2-t1, 60);
	fprintf (stderr, "Process time = %d min %d sec\n", divresult.quot, divresult.rem);
	return(1);
}



