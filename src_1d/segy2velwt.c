/*
 * segy2velwt.c
 *
 *  Created on: Mar 19, 2012
 *      Author: toto
 */

#include "segy_lib.h"
#include <string.h>
#include <stdlib.h>
#include <time.h>

/*********************** self documentation *****************************/
char *sdoc[] = {
		"									",
		" SEGY2VELWT = CONVERT SEGY FILE TO VELOCITY WAVETOMO	",
		"",
		" endian=0         (0=little endian)",
		"									",
		" How to Use :",
		" segy2velwt < input.sgy  out=vel_file",
		NULL};


int main(int argc, char **argv)
{
	int i, j, ntrc, nsp;
	float dt;
	char *strout, *buffer;
	int endian, format, dti, nsegy, nbyte;
	char ebcdic[3200];
	FILE *fodd=NULL, *fodi=NULL;
	float **data, *infofile;

	div_t divresult;
	time_t t1,t2;

	/* Initialize */
	initargs(argc, argv);
	requestdoc(1);

	MUSTGETPARSTRING("out",  &strout);
	if (!getparint("endian", &endian)) endian=0;

	/* start process */
	t1 = time(NULL);

	/* opening file segy*/
	readEbcdicHeader(stdin, ebcdic);
	readBinaryHeader(stdin, endian, &bh, &nsegy);
	ntrc =getNumberOfTraceSegyFile(stdin, nsegy);
	nsp = bh.hns;
	format = bh.format;
	dti = bh.hdt;
	dt = (float) (dti/1e+6);
	nbyte = 100;

	data = alloc2float(ntrc, nsp);
	for(i=0; i<ntrc; i++) //read all trace
	{
		fprintf(stderr, "Process Trace [ %i / %i ]\n", i+1, ntrc);
		readTraceSegy(stdin, endian, nsp, format, nsegy, &tr);
		for(j=0; j<nsp; j++)
			data[j][i] = tr.data[j];
	}

	/*create output file*/
	buffer = (char*) calloc(nbyte, sizeof(char));
	buffer = (char*) calloc(nbyte, sizeof(char));
	sprintf(buffer,"%s.info", strout);
	fodd = fopen(strout, "w");
	fodi = fopen(buffer, "w");
	//-----------------------------------------------

	infofile = alloc1float(6);
	buffer = (char*) calloc(nbyte, sizeof(char));
	for (i=0; i<nsp; i++)	//saving data
	{
		memset(buffer, 0, nbyte*sizeof(char));
		for(j=0; j<ntrc; j++)
		{
			sprintf(buffer,"%5.10E\n", data[i][j]);
			fwrite(buffer, sizeof(char), strlen(buffer), fodd);
		}
	}

	//fill infofile value
	infofile[0] = 0.0;
	infofile[1] = 0.0;
	infofile[2] = ntrc;
	infofile[3] = 0.0;
	infofile[4] = 0.0;
	infofile[5] = nsp;

	for (i=0; i<6; i++)	//saving infofile
	{
		memset(buffer, 0, nbyte*sizeof(char));
		sprintf(buffer,"%5.0f\n", infofile[i]);
		fwrite(buffer, sizeof(char), strlen(buffer), fodi);
	}

	fclose(fodd);
	fclose(fodi);
	free(buffer);
	free2float(data);
	free1float(infofile);

	t2 = time(NULL);
	divresult = div (t2-t1, 60);
	fprintf (stderr, "Process time = %d min %d sec\n", divresult.quot, divresult.rem);

	return(1);
}
