/*
 * su_off2ang.c
 *
 *  Created on: May 31, 2012
 *      Author: toto
 */
#include "../src_lib/segy_lib.h"
#include <stdbool.h>

char *sdoc[] = {
		"\n",
		"SU_OFF2ANG.v1.0 : OFFSET TO ANGLE",
		"",
		"su_off2ang.v1.0 < stdin >stdout",
		"									",
		" Required Parameters:							",
		" key=cdp 	(process by key)",
		" pad=5     (padding trace)",
		" na= 		(int -> angle samples)",
		" dz= 		(float -> depth sampling)",
		" dx= 		(float -> offset sampling)",
		" da= 		(float -> angle samples (in degre))",
		" a0= 		(float -> first angle (in degre))",
		" h0= 		(float -> first offset)",
		"			",
		" Optional Parameters:							",
		" dt=       (from input data)		",
		" taper=0.1 		",
		" twlen= 	(float)	    ",
		"",
		NULL};

#define LOOKFAC	2	/* Look ahead factor for npfaro	  */

#define PII 3.141592653589793238462643383279502884197169399375105


void off2ang(float **off, float **ang, float *dip, int nx, int nz, int na, int nfft,
		float dx, float dz, float da, float a0, float h0);
complex cplx_cmul(complex a, complex b);
complex cplx_crmul(complex a, float b);
complex cplx_create(float freal, float fimag);
complex cplx_cadd(complex a, complex b);
float **readSuTrace(FILE *input, int pos1, int pos2, int nsp, int nsegy);
void runOffset2Angle(float *dip, float **ang, int first_trpos, int intrc, int nz, int nsegy,
		Value currkeyval, int nfft, float dx, float dz, float da, float a0, float h0, float na, int pad);
int main(int argc, char **argv)
{
	int iitr, intrc, first_trpos;
	int nsegy;
	int nz;
	int na;
	int nfft;
	float dx;
	float dz;
	float da;
	float a0;
	float h0;

	int nspws;	       	/* number of samples in window (wo taper)*/
	int nspwf;	       	/* number of samples in first window    */
	int nstaper;	       	/* number of samples in taper		*/
	float dt;		/* sampling interval in secs		*/
	float taper;		/* length of taper			*/
	float twlen;       	/* time window length 			*/
	int pad;
	float *dip, **ang;

	cwp_String key;	/* header key word from segy.h		*/
	cwp_String type;/* ... its type				*/
	int indx;	/* ... its index			*/
	Value val, currkeyval;	/* value of key in current gather	*/
	segy tr;

	/* hook up getpar to handle the parameters */
	initargs(argc,argv);
	requestdoc(1);

	if (!getparstring("key", &key))		 key = "cdp";
	if (!getparint("pad",&pad)) 	pad=5;
	if (!getparint("na",&na)) 	err("must specify na!\n");
	if (!getparfloat("dz",&dz)) 	err("must specify dz!\n");
	if (!getparfloat("dx",&dx)) 	err("must specify dx!\n");
	if (!getparfloat("da",&da)) 	err("must specify da!\n");
	if (!getparfloat("a0",&a0)) 	err("must specify a0!\n");
	if (!getparfloat("h0",&h0)) 	err("must specify h0!\n");
	if (!getparfloat("taper", &taper)) taper=.1;


	//read the first trace
	gettr(&tr);
	dt = (float) (tr.dt/1e+6);
	nz = tr.ns;
	nsegy = nz*4+240;

	if (taper==0.0)	taper=.004;
	if (!getparfloat("twlen", &twlen)) twlen=(float)(nz-1)*dt;
	if (twlen<.3)	twlen=.3;

	/* convert to radians */
	da = (da * PI) / 180.0;
	a0 = (a0 * PI) / 180.0;
	/* setting taper and spatial and temporal windows */
	nstaper=NINT(taper/dt);
	nstaper = (nstaper%2 ? nstaper+1 : nstaper);
	nspws = NINT(twlen/dt) + 1;
	nspwf = nspws + nstaper/2;

	/* Set up pfa fft */
	nfft = npfaro(2*nspwf, LOOKFAC * 2*nspwf);
	ang = ealloc2float(nz, na);
	dip = ealloc1float(nz);


	fprintf(stderr, "delta angle %f   ,   first angle %f \n", da, a0);

	type = hdtype(key);
	indx = getindex(key);

	gethval(&tr, indx, &val);
	currkeyval = val;
	iitr = 1;
	intrc = 1;
	first_trpos = 0;
	while(gettr(&tr))
	{
		gethval(&tr, indx, &val);
		if(valcmp(type, val, currkeyval))
		{
			runOffset2Angle(dip, ang, first_trpos, intrc, nz,
					nsegy, currkeyval, nfft, dx, dz, da, a0, h0, na, pad);

			gotoTraceSuPosition(stdin, intrc, nsegy);
			first_trpos = intrc;
			intrc = intrc-1;
			iitr=0;
			currkeyval = val;
		}
		iitr++;
		intrc++;
	}

	//process off2ang int the last cdp
	runOffset2Angle(dip, ang, first_trpos, intrc, nz,
			nsegy, currkeyval, nfft, dx, dz, da, a0, h0, na, pad);

	free1float(dip);
	free2float(ang);
	return(1);
}

void runOffset2Angle(float *dip, float **ang, int first_trpos, int intrc, int nz, int nsegy,
		Value currkeyval, int nfft, float dx, float dz, float da, float a0, float h0, float na, int pad)
{
	int nx;
	float **off;
	int idx, i, j;
	segy tr;

	nx = (intrc-1)-first_trpos+1;
	off = alloc2float(nz, nx);
	gotoTraceSuPosition(stdin, first_trpos, nsegy);
	fprintf(stderr,"Process Header [ %i ], NTRC = %i \n", currkeyval.i, nx);

	idx = 0;
	for(i=first_trpos; i<=intrc-1; i++)	//read trace
	{
		gettr(&tr);
		for(j=0; j<nz; j++)
			off[idx][j] = tr.data[j];
		idx++;
	}

	//call off2ang
	memset(dip, 0, nz*sizeof(float));
	memset(ang[0], 0, nz*na*sizeof(float));
	off2ang(off, ang, dip, nx, nz, na, nfft, dx, dz, da, a0, h0);

	idx = 0;
	for (i=0; i<na-pad; i++)  //read over trace
	{
		for(j=0; j<nz; j++)
			tr.data[j] = ang[i][j];

		tr.tracl = i+1;
		puttr(&tr);
		idx++;
	}
	free2float(off);
}

float **readSuTrace(FILE *input, int pos1, int pos2, int nsp, int nsegy)
{
	segy tr;
	float **data;
	int ntrc, idx, i, j;

	ntrc = pos2-pos1+1;
	data = alloc2float(nsp, ntrc);
	gotoTraceSuPosition(input, pos1, nsegy);

	idx = 0;
	for(i=pos1; i<=pos2; i++)
	{
		gettr(&tr);
		for(j=0; j<nsp; j++)
			data[idx][j] = tr.data[j];
		idx++;
	}
	return(data);
}

/*
 * off = input: local offset gather [nx][nz]
 * ang = output: angle gather [na][nz]
 * dip = cross-line dip [nz]
 * dz = depth sampling
 * na = angle samples
 * da = angle sampling (in radians)
 * a0 = first angle (in radians)
 * h0 =  first offset
 */
void off2ang(float **off, float **ang, float *dip, int nx, int nz, int na, int nfft,
		float dx, float dz, float da, float a0, float h0)
{
	int ix, it, ifq, ip, itt;
	int iq;
	int nf, ntrc;
	int nspw;
	int nt, np;
	float dw, p, t, w, dp, p0;
	float x0;
	float dq;
	float **pp, *todataw, *ttodataw;
	float *tt;       	/* real trace in time window - input   	*/
	float *ttw;      	/* real trace in time window - input   	*/
	complex *ct;		/* complex transformed trace (window)  	*/
	complex *dd, *freqv;
	complex **fdata, **cm;
	complex m, c;

	nt = nz;
	np = na;
	dp = da;
	p0 = a0;
	x0 = h0;
	ntrc = nx;
	nf = nfft/2 + 1;
	nspw = nt;
	dw = 2.0*PI/(nfft*dz);

	tt       = ealloc1float(nfft);
	ttw      = ealloc1float(nfft);
	pp       = ealloc2float(nt, np);
	todataw  = ealloc1float(nfft);
	ttodataw = ealloc1float(nfft);

	ct       = ealloc1complex(nfft);
	dd       = ealloc1complex(nf);
	freqv    = ealloc1complex(nfft);
	fdata    = ealloc2complex(nf, ntrc);
	cm       = ealloc2complex (nf,np);

	/*** frequency-domain Radon transform ***/
	for (ix=0; ix < nx; ix++)  /* loop over offsets */
	{
		for (it=0; it < nt; it++)
			tt[it] = off[ix][it];
		for (it=nspw; it < nfft; it++)
			tt[it] = 0.0;

		memset((void *) ct, 0, nfft*sizeof(complex));

		/* FFT from t to f */
		for (it=0;it<nfft;it++)
			ttw[it]=(it%2 ? -tt[it] : tt[it]);
		pfarc(1, nfft, ttw, ct);

		/* Store values */
		for (ifq = 0; ifq < nf; ifq++)
			fdata[ix][ifq] = ct[nf-1-ifq];
	}

	for (ifq=0; ifq<nf; ifq++)  /* loop over frequencies */
	{
		w = ifq*dw;
		for (ix=0; ix < nx; ix++)  /* loop over offsets */
			dd[ix] = cplx_crmul(fdata[ix][ifq], 1/nfft);  /* transpose + FFT scaling */

		for (ip=0; ip < np; ip++)
		{ /* loop over slopes */
			p = tan(p0+ip*dp);
			t = w*p*dx;

			c = cplx_create(cos(t), sin(t));
			m = cplx_create(0.0, 0.0);
			for (ix=nx-1; ix >= 0; ix--)
			{
				m = cplx_cadd(cplx_cmul(m, c), dd[ix]);
			}

			t = w*p*x0;
			c = cplx_create(cos(t), sin(t));

			/* transpose */
			cm[ip][ifq] = cplx_cmul(m, c);
		}
	}

	//========== process IFFT ===================
	for (ip=0; ip < np; ip++) /* loop over slopes */
	{
		/* select data */
		for (ifq=0,itt=nf-1;ifq<nf;ifq++,itt--)
		{
			freqv[ifq] = cm[ip][itt];
		}

		memset((void *) (freqv+nf), 0, (nfft-nf)*sizeof(complex));
		memset((void *) todataw, 0, nfft*FSIZE);

		/* FFT back from f to t and scaling */
		pfacr(-1, nfft, freqv, todataw);
		for (it=0;it<MIN(nt, nfft);it++)
			todataw[it]/=nfft;
		for (it=0;it<MIN(nt, nfft);it++)
			ttodataw[it]=(it%2 ? -todataw[it] : todataw[it]);

		for (it=0; it<nt; it++)
			pp[ip][it]=ttodataw[it];
	}
	//========== end of process IFFT ===================

	/*** dip correction ***/
	for (it=0; it<nt; it++) /* loop over depth */
	{
		dq = sqrt(1+dip[it]);
		for (ip=0; ip < np; ip++)
		{
			p = (atan(tanf(p0+ip*dp)*dq)-p0)/dp;
			/* linear interpolation */
			iq = floor(p);
			if (iq >= 0 && iq < np-1)
			{
				p -= iq;
				ang[ip][it] = pp[iq][it] * (1.-p) + pp[iq+1][it] * p;
			}
			else
			{
				ang[ip][it]=0.;
			}
		}
	}

	free1float(tt);
	free1float(ttw);
	free1float(todataw);
	free1float(ttodataw);
	free2float(pp);

	free1complex(ct);
	free1complex(dd);
	free1complex(freqv);
	free2complex(cm);
	free2complex(fdata);
}

complex cplx_create(float freal, float fimag)
{
	complex c;
	c.r = freal;
	c.i = fimag;

	return c;
}

complex cplx_cmul(complex a, complex b)
/*< complex multiplication >*/
{
	complex c;
	c.r = a.r*b.r - a.i*b.i;
	c.i = a.i*b.r + a.r*b.i;
	return c;
}

complex cplx_crmul(complex a, float b)
/*< complex by real multiplication >*/
{
	complex c;
	c.r = a.r + b;
	c.i = a.i + b;
	return c;
}

complex cplx_cadd(complex a, complex b)
/*< complex addition >*/
{
	complex c;
	c.r = a.r + b.r;
	c.i = a.i + b.i;

	return c;
}
