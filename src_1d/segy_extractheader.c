/*
 * segy_extractheader.c
 *
 *  Created on: Jul 17, 2012
 *      Author: toto
 */

#include "../src_lib/segy_lib.h"
#include <time.h>
char *sdoc[] = {
		"									",
		" SEGY_EXTRACTHEADER = EXTRACT SEGY HEADER",
		"",
		" segy_extractheader < input.sgy key=1,4i/115,2i/33,4e/37,4f/ > output.txt",
		" vblock=1000      (show verbose every .. traces)",
		" start=0          read trace starting from ..",
		" end=0            read trace until .. (0=read until the end of file)",
		" ",
		" Extract Header : ",
		" Byte 1   - Type 4 Byte Integer",
		" Byte 115 - Type 2 Byte Integer",
		" Byte 33  - Type 4 Byte Floating IEEE",
		" Byte 37  - Type 4 Byte IBM Floating Point",
		"",
		NULL};

typedef struct headerInfo{
	int pos;
	int len;
	char type;
}headerInfo;

headerInfo *scanHeaderPos(char *str, int *lenHdrInfo);
void printHdrInfo(headerInfo *hdrinfo, int len);

int main(int argc, char **argv)
{

	initargs(argc,argv);
	requestdoc(1);

	char ebcdic[3200];
	bhed bh;
	headerInfo *hdrinfo;
	unsigned char *header, *data;
	int lenHdrInfo;
	char *key;
	int endian, nsegy, ntrc, result;
	int i, j, ndata;
	int iv, vblock;
	int istart, iend;
	float fv;
	div_t divresult;
	time_t t1,t2;

	MUSTGETPARSTRING("key", &key);
	if (!getparint("endian",&endian)) endian=0;
	if (!getparint("start",&istart)) istart=0;
	if (!getparint("end",&iend)) iend=0;
	if (!getparint("vblock",&vblock)) vblock=1000;
	t1 = time(NULL);
	hdrinfo = scanHeaderPos(key, &lenHdrInfo);
	printHdrInfo(hdrinfo, lenHdrInfo);

	readEbcdicHeader(stdin, ebcdic);
	readBinaryHeader(stdin, endian, &bh, &nsegy);
	ntrc = getNumberOfTraceSegyFile(stdin, nsegy);
	ndata = nsegy-SEGY_HDRBYTES;

	if(istart<0)
		istart = 0;
	if(iend==0 || iend > ntrc)
		iend = ntrc-1;

	header = (unsigned char*) calloc(SEGY_HDRBYTES, sizeof(unsigned char));
	data = (unsigned char*) calloc(ndata, sizeof(unsigned char));
	gotoTraceSegyPosition(stdin, istart, nsegy);
	for (i=istart; i<=iend; i++)
	{
		if(i%vblock==0)
			fprintf(stderr, "Read Trace %i / %i \n", i+1, iend);

		result = fread(header, SEGY_HDRBYTES, sizeof(unsigned char), stdin);
		result = fread(data, ndata, sizeof(unsigned char), stdin);

		for (j=0; j<lenHdrInfo; j++)
		{
			if(hdrinfo[j].type=='i'){
				iv = uchar2int(header, hdrinfo[j].pos, hdrinfo[j].len, endian);
				printf("%i\t", iv);
			}
			else if(hdrinfo[j].type=='e') {
				fv = uchar2float(header, hdrinfo[j].pos, hdrinfo[j].len, endian);
				printf("%f \t", fv);
			}
			else if(hdrinfo[j].type=='f') {
				fv = uchar2ibm(header, hdrinfo[j].pos, hdrinfo[j].len, endian);
				printf("%f \t", fv);
			}
			else {
				fprintf(stderr, "\nInput type must be 2i, 4i, 4f or 4e \n\n");
				exit(0);
			}
		}
		printf("\n");
	}

	free(header);
	free(data);
	free(hdrinfo);
	t2 = time(NULL);
	divresult = div (t2-t1, 60);
	fprintf (stderr, "Process time = %d min %d sec\n", divresult.quot, divresult.rem);
	return(1);
}

headerInfo *scanHeaderPos(char *str, int *lenHdrInfo)
{
	int i;
	int lenStr;
	headerInfo *hdrinfo;
	char *splitStr;

	lenStr = 0;
	for(i=0; i<strlen(str); i++)
	{
		if(str[i]=='/')
			lenStr = lenStr+1;
	}

	hdrinfo = (headerInfo*) calloc(lenStr, sizeof(headerInfo));
	splitStr = strtok (str, "/");
	i = 0;
	while (splitStr != NULL)
	{
		sscanf(splitStr, "%i,%i%c", &hdrinfo[i].pos, &hdrinfo[i].len, &hdrinfo[i].type);
		splitStr = strtok (NULL, "/");
		i = i+1;
	}

	(*lenHdrInfo) = lenStr;
	return(hdrinfo);
}

void printHdrInfo(headerInfo *hdrinfo, int len)
{
	int i;
	fprintf(stderr, "\n PRINT HEADER INFO \n");
	for(i=0; i<len; i++)
		fprintf(stderr,"%i. POS=%i  LEN=%i   TYPE=%c \n",
				i+1, hdrinfo[i].pos, hdrinfo[i].len, hdrinfo[i].type);
}
