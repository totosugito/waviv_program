/*
 * wave_creategroundrool.c
 *
 *  Created on: Feb 6, 2012
 *      Author: toto
 */
#include "segy_lib.h"
#include "../matlab_lib/datafun/xcorr.h"
#include "../matlab_lib/elmat/toeplitz.h"
#include <gsl/gsl_linalg.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

/*********************** self documentation *****************************/
char *sdoc[] = {
		"									",
		" WAVE_CREATEGROUNDROOL	: Add groundrool to SEG-Y	",
		"",
		" segyout=         (Ouput segy)",
		" T=3.0            (Propagation time of Groundroll)",
		" fb=0.5           (f beginning sweep)",
		" fe=10.0          (f end sweep)",
		" N=1.0            (slope of sweep power spectrum (dB/Octav))",
		" ftype=1          (1=liniar method, 0=non liniar method",
		" endian=0         (0=little endian)",
		"									",
		" How to Use :",
		" wave_creategroundrool < input.sgy  segyout=output.sgy",
		NULL};


void writeoutput2text(float *data, int ndata);
void computeMatlabLU_Decomp(float **A, float *V, int nlen);
void traceAddGroundroll(float *z, float *st, int nsp);
int main(int argc, char **argv)
{
	float T, fb, fe, N, *st, *z, t, Ft;
	int FType;
	int i, j, ntrc, nsp;
	float dt;
	char *csegyout;
	int endian, format, dti, nsegy;
	char ebcdic[3200];
	FILE *fod=NULL;
	div_t divresult;
	time_t t1,t2;

	/* Initialize */
	initargs(argc, argv);
	requestdoc(1);

	MUSTGETPARSTRING("segyout",  &csegyout);
	if (!getparfloat("T", &T)) T=3.0;
	if (!getparfloat("fb", &fb)) fb=0.5;
	if (!getparfloat("fe", &fe)) fe=10.0;
	if (!getparfloat("N", &N)) N=1.0;
	if (!getparint("ftype", &FType)) FType=1;
	if (!getparint("endian", &endian)) endian=0;

	/* start process */
	t1 = time(NULL);

	/* opening file segy*/
	readEbcdicHeader(stdin, ebcdic);
	readBinaryHeader(stdin, endian, &bh, &nsegy);
	ntrc =getNumberOfTraceSegyFile(stdin, nsegy);
	nsp = bh.hns;
	format = bh.format;
	dti = bh.hdt;
	dt = (float) (dti/1e+6);

	/*opening output file */
	bh.format = 1;
	fod = fopen(csegyout, "w");
	if(fod==NULL)
		err("error opening file %s", csegyout);
	writeEbcdicHeader(fod, ebcdic);
	writeBinaryHeader(fod, endian, &bh);

	z = alloc1float(nsp);
	st = alloc1float(nsp);

	for(i=0; i<nsp; i++)
	{
		t = i*dt;

		if(FType==1)	//LINEAR
			Ft = fb + (((fe-fb)*t) / pow(2*T,N));
		else 		//Non LINEAR
			Ft = fb + (((fe-fb)*pow(t, N)) / pow(2*T, N));

		/* St: Condition 1 */
		if(t>0.0 && t<=T+dt)
			st[i]= sin(2*PI*Ft*t);
		else
			st[i]= sin(0.0);

	}

	//read trace
	for(i=0; i<ntrc; i++)
	{
		fprintf(stderr, "Process Trace [ %i / %i ]\n", i+1, ntrc);
		readTraceSegy(stdin, endian, nsp, format, nsegy, &tr);
		for(j=0; j<nsp; j++)	z[j] = tr.data[j];

		traceAddGroundroll(z, st, nsp);

		for(j=0; j<nsp; j++)
			tr.data[j] = z[j];

		writeTraceSegy(fod, endian, &tr, nsegy, nsp);
	}

	free1float(z);
	free1float(st);

	t2 = time(NULL);
	divresult = div (t2-t1, 60);
	fprintf (stderr, "Process time = %d min %d sec\n", divresult.quot, divresult.rem);

	return(1);
}



void traceAddGroundroll(float *z, float *st, int nsp)
{
	int i;
	float *st_aks;
	float *bcorr, *Ci;
	float *acorr, *okcorr;
	int nbcorr, mid1, lenst_aks;
	int nacorr, mid2;
	float **Rt;

	/*Ci Value. Ci is cross-correlation of the corrupted trace (z) with ref. noise (st) */
	bcorr = xcorr(z, nsp, st, nsp, &nbcorr);
	mid1=((nbcorr+1)/2) - 1;
	Ci = alloc1float(nsp);
	for(i=mid1; i<mid1+nsp; i++)
		Ci[i-mid1] = bcorr[i];

	/*
	 * Rt Value
	 * Rt is auto-corr of the reference noise trace
	 * Autocorrelasi
	 */
	acorr = xcorr(st, nsp, st, nsp, &nacorr);
	mid2 = ((nacorr+1)/2) - 1;
	okcorr = alloc1float(nsp);
	for(i=mid2; i<mid2+nsp; i++)
		okcorr[i-mid2] = acorr[i];

	Rt = toeplitz(okcorr, nsp);
	computeMatlabLU_Decomp(Rt, Ci, nsp); /* Koefisien f */

	st_aks = convf(Ci, st, nsp, nsp, &lenst_aks);

	/* SIGNAL PREDICTION */
	for(i=0; i<nsp; i++)
		z[i] = z[i] - st_aks[i];

	//writeoutput2text(st_aks, lenst_aks);


	free1float(Ci);
	free1float(bcorr);
	free1float(acorr);
	free1float(okcorr);
	free2float(Rt);
	free1float(st_aks);
}

void computeMatlabLU_Decomp(float **A, float *V, int nlen)
{
	int i,j;

	int s;
	gsl_matrix *gslA;
	gsl_vector *gslV, *gslVX;
	gsl_permutation *p;

	gslV = gsl_vector_alloc(nlen);
	gslVX = gsl_vector_alloc(nlen);

	for(i=0; i<nlen; i++)
		gsl_vector_set(gslV, i, (double) (V[i]));

	gslA = gsl_matrix_calloc(nlen, nlen);
	for(i=0; i<nlen; i++)
		for(j=0; j<nlen; j++)
			gsl_matrix_set(gslA, i, j, (double)(A[i][j]));


	p = gsl_permutation_alloc(nlen);
	gsl_linalg_LU_decomp(gslA, p, &s);
	gsl_linalg_LU_solve(gslA, p, gslV, gslVX);

	for(i=0; i<nlen; i++)
		V[i] = (float) (gsl_vector_get(gslVX, i));

	gsl_matrix_free(gslA);
	gsl_vector_free(gslVX);
	gsl_vector_free(gslV);
	gsl_permutation_free(p);

}

void writeoutput2text(float *data, int ndata)
{
	FILE *fod;
	int i;
	char *buffer;

	fod = fopen("test.txt", "w");
	buffer = (char*) calloc(100, sizeof(char));
	for(i=0; i<ndata; i++)
	{
		sprintf(buffer,"%5.5f \n", data[i]);
		fwrite(buffer, sizeof(char), strlen(buffer), fod);
		memset(buffer, sizeof(char), strlen(buffer));
	}


	free(buffer);
	fclose(fod);
}

