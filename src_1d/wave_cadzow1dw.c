/*
 * wave_cadzow1dw.c
 *
 *  Created on: Jun 19, 2012
 *      Author: toto
 */

/*
 * wave_cadzow1d.c
 *
 *  Created on: Apr 25, 2012
 *      Author: toto
 */
#include "../src_lib/cadzowLib.h"
#include "../src_lib/fftLib.h"
#include "../src_lib/tesWindowing.h"
char *sdoc[] = {
		"			",
		"WAVE_CADZOW1DW ",
		"wave_cadzow1dw <stdin rank=1> output",
		"fmin=0      (minimum frequency)",
		"fmax=0      (maximum frequency)",
		"spanx=20    (Width of X-Axis windowing)",
		"spany=20    (Width of Y-Axis windowing)",
		"spanfx=10   (Saving X-Axis Length)",
		"spanfy=10   (Saving Y-Axis Length)",
		"method=1    (Windowing method method)",
		"            1=compute overlap",
		"            2=ignore overlap",
		"			",
		"rank=       rank cadzow  ",
		"			",
		NULL};


float **cadzow_windowing1(float **data, int ntrc, int nsp, float dt,
		int nspanX, int nspanY, int nsfiltX, int nsfiltY,
		int rank, float fmin, float fmax);
float **cadzow_windowing2(float **data, int ntrc, int nsp, float dt,
		int nspanX, int nspanY, int nsfiltX, int nsfiltY,
		int rank, float fmin, float fmax);

int main(int argc, char ** argv) {
	char cebcdic[3200];
	int i, j;
	int ns, ntrc, format, endian, nsegy;
	float **dataOri;
	float dt;
	float fmin, fmax;
	int rank;
	div_t divresult;
	time_t t1,t2;
	float **data=NULL;
	int imethod;

	int nspanx, nspany, nspanfx, nspanfy;

	/* hook up getpar to handle the parameters */
	initargs(argc,argv);
	requestdoc(1);

	if (!getparint("rank",&rank)) rank=1;
	if (!getparint("endian",&endian)) endian=0;
	if (!getparfloat("fmin",&fmin)) fmin=0.0;
	if (!getparfloat("fmax",&fmax)) fmax=60.0;
	if (!getparint("spanx",&nspanx)) nspanx=20;
	if (!getparint("spany",&nspany)) nspany=20;
	if (!getparint("spanfx",&nspanfx)) nspanfx=10;
	if (!getparint("spanfy",&nspanfy)) nspanfy=10;
	if (!getparint("method",&imethod)) imethod=1;

	/* get variable fft */
	t1 = time(NULL);
	readEbcdicHeader(stdin, cebcdic); /* read ebcdic header */
	readBinaryHeader(stdin, endian, &bh, &nsegy); /*read binary header */
	ntrc = getNumberOfTraceSegyFile(stdin, nsegy);
	ns = bh.hns;
	format = bh.format;
	dt = (float) (bh.hdt / 1e+6);

	bh.format = 1;
	writeEbcdicHeader(stdout, cebcdic);
	writeBinaryHeader(stdout, endian, &bh);

	//read data
	dataOri = alloc2float(ns, ntrc);
	for (i=0; i<ntrc; i++)
	{
		readTraceSegy(stdin, endian, ns, format, nsegy, &tr);
		for (j=0; j<ns; j++)
			dataOri[i][j] = tr.data[j];
	}

	if(imethod==1){
		data = cadzow_windowing1(dataOri, ntrc, ns, dt, nspanx, nspany, nspanfx, nspanfy,
				rank, fmin, fmax);
	}
	else {
		data = cadzow_windowing2(dataOri, ntrc, ns, dt, nspanx, nspany, nspanfx, nspanfy,
						rank, fmin, fmax);
	}




	gotoTraceSegyPosition(stdin, 0, nsegy);
	for(i=0; i<ntrc; i++)
	{
		readTraceSegy(stdin, endian, ns, format, nsegy, &tr);
		for(j=0; j<ns; j++)
			tr.data[j] = data[i][j];

		writeTraceSegy(stdout, endian, &tr, nsegy, ns);
	}

	free2float(dataOri);
	free2float(data);

	t2 = time(NULL);
	divresult = div (t2-t1, 60);
	fprintf (stderr, "Process time = %d min %d sec\n", divresult.quot, divresult.rem);

	return(1);
}

float **cadzow_windowing1(float **data, int ntrc, int nsp, float dt,
		int nspanX, int nspanY, int nsfiltX, int nsfiltY,
		int rank, float fmin, float fmax)
{
	int i, j;
	bool loopx, loopy;
	int icenterX, icenterY;
	int posX1, posX2, posY1, posY2;
	int posXs1, posXs2, posYs1, posYs2;
	int sp1, sp2, ss1, ss2;
	float **dataw=NULL;
	float **outdata;
	int **countdata;
	int nx, ny, nf, nfft, tmin, tmax;
	float d1;

	outdata = alloc2float(nsp, ntrc);
	countdata = alloc2int(nsp, ntrc);
	memset(outdata[0], 0, nsp*ntrc*sizeof(float));
	memset(countdata[0], 0, nsp*ntrc*sizeof(int));
	for(i=0; i<ntrc; i++)
	{
		for(j=0; j<nsp; j++)
		{
			outdata[i][j] = 0.0;
			countdata[i][j] = 0.0;
		}
	}

	loopx = true;
	icenterX = 0;
	while(loopx)
	{
		loopx = getpos1pos2(nsp, icenterX, nspanX, nsfiltX, posX1, posXs1,
				&sp1, &sp2, &ss1, &ss2);
		posX1 = sp1;
		posX2 = sp2;
		posXs1 = ss1;
		posXs2 = ss2;
		//fprintf(stderr, "\nX --- %i  %i  %i  %i  %i \n",posX1, posXs1, icenterX, posXs2, posX2);

		icenterY = 0;
		loopy = true;
		while(loopy)
		{
			loopy = getpos1pos2(ntrc, icenterY, nspanY, nsfiltY, posY1, posYs1,
					&sp1, &sp2, &ss1, &ss2);
			posY1 = sp1;
			posY2 = sp2;
			posYs1 = ss1;
			posYs2 = ss2;
			//fprintf(stderr, "Y[%i] --- %i  %i  %i  %i  %i \n",ntrc, posY1, posYs1, icenterY, posYs2, posY2);

			dataw = getData(data, ntrc, nsp, posX1, posX2, posY1, posY2, &nx, &ny); //get windowing data

			//process cadzow1d
			getNfft(nx, dt, &nf, &nfft, &d1);
			if(fmin<0.0)	fmin = 0.0;
			if(fmax>=nf*d1 || fmax<=0) fmax = nf*d1 - d1;
			tmin = (int) (fmin/d1);
			tmax = (int) (fmax/d1);

			runCadzow1D(dataw, rank, ny, nx, nf, nfft, d1, tmin, tmax);

			addDatai(countdata, posX1, posX2, posY1, posY2);	//update counting data
			addDataf(outdata, dataw, posX1, posX2, posY1, posY2);	//update output data

			icenterY = icenterY+2*nsfiltY;

			free2float(dataw);
		}

		icenterX = icenterX+2*nsfiltX;
	}

	//normalize data
	for(i=0; i<ntrc; i++)
		for(j=0; j<nsp; j++)
			outdata[i][j] = outdata[i][j]/countdata[i][j];

	free2int(countdata);
	return(outdata);
}

float **cadzow_windowing2(float **data, int ntrc, int nsp, float dt,
		int nspanX, int nspanY, int nsfiltX, int nsfiltY,
		int rank, float fmin, float fmax)
{
	int i, j;
	int posX1, posX2, posX3, posX4, posX5, posX6;
	int posY1, posY2, posY3, posY4, posY5, posY6;

	float **dataw=NULL;
	float **outdata;
	int nx, ny, nf, nfft, tmin, tmax;
	float d1;
	int **posN1, nwindow1;
	int **posN2, nwindow2;

	outdata = alloc2float(nsp, ntrc);
	memset(outdata[0], 0, nsp*ntrc*sizeof(float));

	//create windowing position
	posN1 = createWindowingArray(0, nsp-1, nspanX, nsfiltX, &nwindow1);
	posN2 = createWindowingArray(0, ntrc-1, nspanY, nsfiltY, &nwindow2);
	for(i=0; i<nwindow2; i++)
	{
		posY1 = posN2[i][0];
		posY2 = posN2[i][1];
		posY3 = posN2[i][2];
		posY4 = posN2[i][3];
		posY5 = posN2[i][4];
		posY6 = posN2[i][5];

		//fprintf(stderr,"Y %i %i %i %i %i %i \n", posY1, posY2, posY3, posY4, posY5, posY6);
		for(j=0; j<nwindow1; j++)
		{
			posX1 = posN1[j][0];
			posX2 = posN1[j][1];
			posX3 = posN1[j][2];
			posX4 = posN1[j][3];
			posX5 = posN1[j][4];
			posX6 = posN1[j][5];

			//extract data
			dataw = getData(data, ntrc, nsp, posX1, posX2, posY1, posY2, &nx, &ny);

			//process cadzow1d
			getNfft(nx, dt, &nf, &nfft, &d1);
			if(fmin<0.0)	fmin = 0.0;
			if(fmax>=nf*d1 || fmax<=0) fmax = nf*d1 - d1;
			tmin = (int) (fmin/d1);
			tmax = (int) (fmax/d1);

			//cadzow process
			runCadzow1D(dataw, rank, ny, nx, nf, nfft, d1, tmin, tmax);

			//update output
			addDataf1(outdata, posX3, posY3, dataw, posX5, posX6, posY5, posY6);

			//fprintf(stderr,"X %i %i %i %i %i %i \n", posX1, posX2, posX3, posX4, posX5, posX6);
			free2float(dataw);
		}
//		fprintf(stderr,"\n");
	}
	return(outdata);
}


