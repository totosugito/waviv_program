/*
 * wave_filt2d.c
 *
 *  Created on: Mar 8, 2012
 *      Author: toto
 */

#include "segy_lib.h"
#include "smoothingLib.h"
#include "iolibrary.h"
#include <signal.h>

/*********************** self documentation **********************/
char *sdoc[] = {
		" 									",
		" WAVE_FILT2D	",
		"",
		" wave_filt2d < input.su iradius=100 iwidth=50 r1=0 r2=0 rw=3.0 cval=1.0 lval=1.0> output.su",
		"",
		" Input Parameter:",
		" iradius=100       radius circle=2*iradius",
		" iwidth=50         width of vertical and horizontal line",
		" r1=0.0            smoothing parameter in the 1 direction",
		" r2=0.0            smoothing parameter in the 2 direction",
		" win=0,n1,0,n2     array for window range",
		" rw=0.0            smoothing parameter for window function",
		" cval=1.0          value for circle filter",
		" lval=1.0          value for line filter",
		" ",
		"",
		NULL};

#define PFA_MAX	720720	/* Largest allowed nfft		  */
float **getFloatCenterLocation(int nrow, int ncol, int cx, int cy, int iradius, int *nposc);
int **getIntCenterLocation(int nrow, int ncol, int cx, int cy, int iradius, int *nposc);

float **createCircleFilter(int nrow, int ncol, int iradius, float ival);
float **createLineFilter(int nrow, int ncol, int iradius, float ival);
int main(int argc, char **argv)
{
	int nx1,nx2;		/* numbers of samples			*/
	float dx1,dx2;		/* sampling intervals			*/
	int nx1fft,nx2fft;	/* dimensions after padding for FFT	*/
	int nK1,nK2;		/* transform (output) dimensions	*/
	complex **ct;	/* complex FFT workspace		*/
	float **rt;	/* float FFT workspace			*/
	float d1,d2;		/* output intervals in K1, K2		*/
	float f1,f2;		/* output first samples in K1, K2	*/
	float onfft;
	int ix1,ix2;		/* sample indices			*/
	int iradius, iwidth;
	float cval, lval;

	/*smoothing variable*/
	float r1, r2;
	int *win=NULL;
	float rw;

	/* Hook up getpar to handle the parameters */
	initargs(argc,argv);
	requestdoc(1);

	gettr(&tr);

	if (!getparfloat("cval",&cval)) cval = 1.0;
	if (!getparfloat("lval",&lval)) lval = 1.0;

	if (!getparfloat("rw",&rw)) rw = 0.;
	if (!getparfloat("r1",&r1)) r1 = 0.;
	if (!getparfloat("r2",&r2)) r2 = 0.;

	if (!getparint("iradius", &iradius)) iradius = 100;
	if (!getparint("iwidth", &iwidth)) iwidth = 50;

	/* get sampling intervals */
	if (!getparfloat("d1", &dx1))
	{
		if (tr.d1) { /* is d1 field set? */
			dx1 = (float) tr.d1;
		}
		else { /* d1 not set, assume 1.0 */
			dx1 = 1.0;
//			warn("tr.d1 not set, assuming d1=1.0");
		}
	}
	if (!getparfloat("d2",&dx2))
	{
		if (tr.d2) { /* is d2 field set? */
			dx2 = tr.d2;
		}
		else {
			dx2 = 1.0;
//			warn("tr.d2 not set, assuming d2=1.0");
		}
	}

	nx1 = tr.ns;
	nx2 = getNumberOfTraceSuFile(stdin);

	/* get parameters for window function */
	win = alloc1int(4);
	if (!getparint("win",win))
	{
		win[0] = 0;
		win[1] = nx1;
		win[2] = 0;
		win[3] = nx2;
	}

	/* Determine lengths for prime-factor FFTs */
	nx1fft = npfar(nx1);
	nx2fft = npfa(nx2);
	if (nx1fft >= SU_NFLTS || nx1fft >= PFA_MAX)
		err("Padded nx1=%d--too big",nx1fft);
	if (nx2fft >= SU_NFLTS || nx2fft >= PFA_MAX)
		err("Padded nx2=%d--too big",nx2fft);

	/* Determine output header values */
	d1 = 2.0/(nx1fft*dx1);
	d2 = 2.0/(nx2fft*dx2);
	f1 = -1.0/dx1 - d1/2.;
	f2 = -1.0/dx2 + d2;
	onfft = (float) (1.0/(nx1fft*nx2fft));

	/* Note: The choices for d1,d2,f1, and f2 were motivated by the
				 desire to have a plot that runs from -nyq to +nyq
				 in both directions.  The choice of the shift d1/2
				 is because the k1=0 information always falls on a
		 		 sample, whether the number of k1 values is even or odd.
				 The choice of the shift d2 is because the ISODD trick
				 centers k2=0 information between the two center traces
		 		 if the number of k1 values is even, or on the center trace
				 if the number is odd.
	 */

	/* Determine complex transform sizes */
	nK1 = nx1fft/2 + 1 ;
	nK2 = nx2fft;


	/* Allocate space */
	rt = alloc2float(nx1fft, nx2fft);
	ct = alloc2complex(nK1, nK2);

	/* zero rt[][] and ct[][] */
	memset( (void *) rt[0], 0, FSIZE*nx1fft*nx2fft);
	memset( (void *) ct[0], 0, sizeof(complex)*nK1*nK2);

	//read all trace
	rewind(stdin);
	for (ix2=0; ix2<nx2; ++ix2)
	{
		gettr(&tr);

		/* if ix2 odd, negate to center transform of dimension 2 */
		for (ix1=0; ix1<nx1; ++ix1)
		{
			if (ISODD(ix2))
				rt[ix2][ix1] = -tr.data[ix1];
			else
				rt[ix2][ix1] = tr.data[ix1];
		}
	}

	pfa2rc(-1,1,nx1fft,nx2,rt[0],ct[0]);	/* Fourier transform dimension 1 */
	pfa2cc(-1,2,nK1,nK2,ct[0]);				/* Fourier transform dimension 2 */

//	-------------------------------------
	float **spectrumb=NULL, **spectruma=NULL;
	float **circlFilter=NULL, **lineFilter=NULL, **clFilter=NULL, **clFilterApply=NULL;
	int i,j;

	/*GET SPECTRUM*/
	spectrumb = computeAmplitudeSpectrum(ct, nK2, nK1, nx1fft);
	fprintf(stderr, "SPectrum Data : nx1fft(col)=%i nK1=%i \n", nx1fft, nK2);
	saveBinaryFile("spectrum_before.bin", spectrumb, nx1fft, nK2);
//	saveBinaryFile("spectrum_before.bin", spectrumb, nK1, nK2);

	/*CREATE CIRCLE FILTER*/
	circlFilter = createCircleFilter(nK2, nK1, iradius, cval);
	fprintf(stderr, "Filter Size : ncol(col)=%i nrow=%i \n", nK1,nK2);
	saveBinaryFile("circle_filter.bin", circlFilter, nK2, nK1);

	/*CREATE CENTER DIAGONAL FILTER*/
	lineFilter = createLineFilter(nK2, nK1, iwidth, lval);
//	fprintf(stderr, "Line Data : ncol(col)=%i nrow=%i \n", nK1,nK2);
	saveBinaryFile("line_filter.bin", lineFilter, nK2, nK1);

	/*MERGE CIRCLE AND CENTER DIAGONAL FILTER*/
	clFilter = alloc2float(nK1, nK2);
	for(i=0; i<nK2; i++)
	{
		for(j=0; j<nK1; j++)
		{
			clFilter[i][(nK1-1)-j] = lineFilter[i][j] + circlFilter[i][j]; //reverse column
			if(clFilter[i][(nK1-1)-j]>1)
				clFilter[i][(nK1-1)-j] = 1.0;
		}
	}
	saveBinaryFile("cl_filter.bin", clFilter, nK2, nK1);

	/*SMOOTH FILTER */
	clFilterApply = smooth2(clFilter, nK2, nK1, r1, r2, win, rw);
	saveBinaryFile("cl_filterApply.bin", clFilterApply, nK2, nK1);

	/*APPLY FILTER*/
	for(i=0; i<nK2; i++)
	{
		for(j=0; j<nK1; j++)
		{
			ct[i][j].r *= clFilterApply[i][j];
			ct[i][j].i *= clFilterApply[i][j];
		}
	}

	/*SAVE NEW SPECTRUM*/
	spectruma = computeAmplitudeSpectrum(ct, nK2, nK1, nx1fft);
	saveBinaryFile("spectrum_after.bin", spectruma, nx1fft, nK2);
//	saveBinaryFile("spectrum_after.bin", spectruma, nK1, nK2);


	if (spectrumb!=NULL)free2float(spectrumb);
	if (spectruma!=NULL)free2float(spectruma);
	if (circlFilter!=NULL) free2float(circlFilter);
	if (lineFilter!=NULL) free2float(lineFilter);
	if (clFilter!=NULL) free2float(clFilter);
	if (clFilterApply!=NULL) free2float(clFilterApply);
	if(win!=NULL) free1int(win);

	pfa2cc(1,2,nK1,nx2fft,ct[0]);			/* Inverse Fourier transform dimension 2 */
	pfa2cr(1,1,nx1fft,nx2,ct[0],rt[0]);		/* Inverse Fourier transform dimension 1 */

	//SAVING DATA
	rewind(stdin);
	for (ix2=0; ix2 < nx2; ++ix2)
	{
		gettr(&tr);
		for (ix1=0; ix1<nx1; ++ix1)
		{
			if (ISODD(ix2))
				tr.data[ix1] = -rt[ix2][ix1]*onfft;
			else
				tr.data[ix1] = rt[ix2][ix1]*onfft;
		}

		puttr(&tr);
	}

	free2complex(ct);
	free2float(rt);
	return(1);
}

float **createCircleFilter(int nrow, int ncol, int iradius, float ival)
{
	int **centerPos;
	float **filtdata;
	int ndata;
	int i, j;
	int px1, px2, py;
	int cx, cy;

	filtdata = alloc2float(ncol, nrow);

	if(ncol%2==0)	cx=ncol/2;
	else cx = (ncol+1)/2;
	if(nrow%2==0)	cy=nrow/2;
	else cy = (nrow+1)/2;
	cx = ncol-1;

	memset(filtdata[0], 0, ncol*nrow*sizeof(int));	//fill filter data by 0
	centerPos = getIntCenterLocation(nrow, ncol, cx, cy, iradius, &ndata); //get circle filter location
	for(i=0; i<ndata; i++)	//update circle filter value
	{
		px1 = centerPos[0][i];
		px2 = cx;//cx-1;
		py = centerPos[1][i];
		for(j=px1; j<=px2; j++)
			filtdata[py][j] = ival;
	}

	free2int(centerPos);

	return(filtdata);
}

float **createLineFilter(int nrow, int ncol, int iradius, float ival)
{
	float **filtdata;
	int i, j;
	int cx, cy;

	filtdata = alloc2float(ncol, nrow);

	if(ncol%2==0)	cx=ncol/2;
	else cx = (ncol+1)/2;
	if(nrow%2==0)	cy=nrow/2;
	else cy = (nrow+1)/2;
	cx = ncol-1;

	/*set initial value for filter with 1*/
	for (i=0; i<ncol; i++)
		for (j=0; j<nrow; j++)
			filtdata[j][i] = ival;

	//apply Y axis filter
	for (i=ncol-1; i>=ncol-iradius; i--)
		for (j=0; j<nrow; j++)
			filtdata[j][i] = 0;

	//apply X axis filter
	for (i=0; i<ncol; i++)
		for (j=cy-iradius; j<=cy+iradius; j++)
			filtdata[j][i] = 0;

	return(filtdata);
}

float **getFloatCenterLocation(int nrow, int ncol, int cx, int cy, int iradius, int *nposc)
{
	float **posc=NULL;
	int i;
	float x,y;
	//		int x,y;
	int y1, y2, idx;
	int ndata, p1;
	float fdelta;

	ndata = 2*iradius+1;
	posc = alloc2float(ndata, 2);

	y1 = cy - iradius;
	y2 = cy;
	idx = 0;


	//	buat seperempat lingkaran
	for (i=y1; i<=y2; i++)
	{
	    y = (float) i;
	    x = (float) (sqrt(pow(iradius,2.0) - pow((y-cy),2.0)) + cx);
	    posc[0][iradius+idx] = x;
	    posc[1][iradius+idx] = y;
	    idx = idx+1;
	}

	// cerminkan data 1/4 lingkaran diatas dengan sumbu Y
	// untuk mendapatkan setengah lingkaran
	for (i=0; i<iradius; i++)
	{
	    p1 = ndata-i;
	    fdelta = posc[0][p1-1] - cx;

	    posc[0][i] = (float) (cx-fdelta);
	    posc[1][i] = posc[1][p1-1];
	}

	(*nposc) = ndata;
	return(posc);
}

int **getIntCenterLocation(int nrow, int ncol, int cx, int cy, int iradius, int *nposc)
{
	int **posc=NULL;
	int i;
	int x,y;
	int y1, y2, idx;
	int ndata, p1;
	int fdelta;

	ndata = 2*iradius+1;
	posc = alloc2int(ndata, 2);
	memset(posc[0], 0, 2*ndata*sizeof(float));

	y1 = cy;
	y2 = cy + iradius;
	idx = 0;


	//	buat seperempat lingkaran
	for (i=y1; i<=y2; i++)
	{
		y = (int) i;
		x = (int) (sqrt(pow(iradius,2.0) - pow((y-cy),2.0)) + cx);
		posc[0][iradius+idx] = (cx - (x - cx))-1;
		posc[1][iradius+idx] = y-1;
		idx = idx+1;
	}

//	if(posc[0][y1-1] != posc()) posc[0][ndata-1] = cx+1;

	// cerminkan data 1/4 lingkaran diatas dengan sumbu Y
	// untuk mendapatkan setengah lingkaran
	for (i=0; i<iradius; i++)
	{
		p1 = ndata-i;
		fdelta = posc[1][p1-1] - cy;
		posc[0][i] = posc[0][p1-1];
		posc[1][i] = (int) (cy-fdelta);
	}

//	for(i=0; i<ndata; i++)
//		fprintf(stderr,"%i %i \n", posc[0][i],posc[1][i]);
	(*nposc) = ndata;
	return(posc);
}
