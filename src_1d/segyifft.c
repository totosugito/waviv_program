/*
 * segyifft.c
 *
 *  Created on: Mar 19, 2012
 *      Author: toto
 */

#include "segy_lib.h"
#include "fftLib.h"
#include <string.h>
#include <stdlib.h>
#include <time.h>

/*********************** self documentation *****************************/
char *sdoc[] = {
		"									",
		" SEGYIFFT = Process IFFT Input file. Input file from amplitude and fase",
		"",
		" mode=0             (mode=0)",
		"      cfase=        (fill input fase filename)",
		"      camp=         (fill input amplitude filename)",
		"",
		"                    (mode=1)",
		"      creal=        (fill input real filename)",
		"      imag=         (fill input imaginer filename)",
		"",
		" endian=0           (0=little endian)",
		" nsp=0              (number of sample from output file (0=same as input file))",
		" vblock=1000        (show verbose every vblock)",
		"",
		" How to Use :",
		" segyifft mode=0 cfase=fase.sgy camp=amp.sgy endian=0 ns= > output.sgy",
		" segyifft mode=1 creal=real.sgy cimag=imag.sgy endian=0 ns= > output.sgy",
		"",
		NULL};


int main(int argc, char **argv)
{
	int endian, format, dti;
	char ebcdic1[3200], ebcdic2[3200];
	char *cfase=NULL, *camp=NULL;

	float dt;
	int i,j, nx, ntrc;
	int mode;

	float **z=NULL;
	float **freal=NULL, **fimag=NULL;
	float tfase, tamp;

	segy tr1, tr2;
	bhed bh1, bh2;
	int ntrc1, ntrc2, nsp1, nsp2, nsp, nf, nfft;
	int nsegy1, nsegy2, nsegy;
	int vblock, idxvblock;

	FILE *file1=NULL, *file2=NULL;
	div_t divresult;
	time_t t1,t2;

	/* Initialize */
	initargs(argc, argv);
	requestdoc(1);

	if (!getparint("mode", &mode)) mode=0;
	if(mode==0)	//input is fase and amplitude file
	{
		MUSTGETPARSTRING("cfase",  &cfase);
		MUSTGETPARSTRING("camp",  &camp);
	}
	else if(mode==1) //input is real and imaginer file
	{
		MUSTGETPARSTRING("creal",  &cfase);
		MUSTGETPARSTRING("cimag",  &camp);
	}

	if (!getparint("endian", &endian)) endian=0;
	if (!getparint("nsp", &nsp)) nsp=0;
	if (!getparint("vblock", &vblock)) vblock=1000;

	/* start process */
	t1 = time(NULL);

	file1 = fopen(cfase, "r");
	if(!file1) err("error opening file %s", cfase);

	file2 = fopen(camp, "r");
	if(!file2) err("error opening file %s", camp);

	/* opening file FASE file*/
	readEbcdicHeader(file1, ebcdic1);
	readBinaryHeader(file1, endian, &bh1, &nsegy1);
	ntrc1 =getNumberOfTraceSegyFile(file1, nsegy1);
	nsp1 = bh1.hns;
	format = bh1.format;
	dti = bh1.hdt;
	dt = (float) (dti/1e+6);

	/* opening file AMPLITUDE file*/
	readEbcdicHeader(file2, ebcdic2);
	readBinaryHeader(file2, endian, &bh2, &nsegy2);
	ntrc2 =getNumberOfTraceSegyFile(file2, nsegy2);
	nsp2 = bh2.hns;

	//check input file
	if((ntrc1!=ntrc2) && (nsp1!=nsp2))
		err("ntrc or nsp from phase and amplitude is not equal ");

	if(nsp==0)
		nsp = nsp1;
	nsegy = nsp*4+240;
	nf = nsp1;
	nfft = (nf-1)*2;
	ntrc = ntrc1;

	/*opening output file */
	bh1.format = 1;
	bh1.hns = nsp;

	//save output header
	writeEbcdicHeader(stdout, ebcdic1);
	writeBinaryHeader(stdout, endian, &bh1);

	//allocate space
	nx = 1;
	z = alloc2float(nsp,nx);
	freal = alloc2float(nf,nx);
	fimag = alloc2float(nf,nx);
	idxvblock=1;
	for(i=0; i<ntrc; i++)		//read trace
	{
		if(idxvblock*vblock==i){
			fprintf(stderr, "Process Trace [ %i / %i ]\n", i+1, ntrc);
			idxvblock++;
		}

		readTraceSegy(file1, endian, nsp1, format, nsegy1, &tr1);	//read fase file
		readTraceSegy(file2, endian, nsp2, format, nsegy2, &tr2);		//read amplitude file

		for(j=0; j<nsp1; j++)
		{
			tfase = tr1.data[j];
			tamp = tr2.data[j];
			if(mode==0)
			{
				freal[nx-1][j] = tamp*cos(tfase);
				fimag[nx-1][j] = tamp*sin(tfase);
			}
			else
			{
				freal[nx-1][j] = tfase;
				fimag[nx-1][j] = tamp;
			}
		}

		ifftProcess(z, nsp, nx, nf, nfft, freal, fimag);

		tr1.ns = nsp;
		for(j=0; j<nsp; j++)
				tr1.data[j] = z[nx-1][j];
		writeTraceSegy(stdout, endian, &tr1, nsegy, nsp);
	}

	free2float(z);
	free2float(freal);
	free2float(fimag);
	fclose(file1);
	fclose(file2);

	t2 = time(NULL);
	divresult = div (t2-t1, 60);
	fprintf (stderr, "Process time = %d min %d sec\n", divresult.quot, divresult.rem);

	return(1);
}
