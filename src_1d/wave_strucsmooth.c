/*
 * wave_strucsmooth.c
 *
 *  Created on: Jan 19, 2012
 *      Author: toto
 */

#include "segy_lib.h"
#include "smoothingLib.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *sdoc[] = {
		"									",
		" WAVE_STRUCSMOOTH : STRUCTURE ORIENTED SMOOTHING",
		" ",
		" Input Parameter :",
		" sigma=0.5             (window size for smoothing local atribute)",
		" sigmaderiv=0.5        (window size for dip analysis)",
		" k=0.5                 (seismic=0.5)",
		" dts=0.5               (strength of smoothing)",
		" klength=5             (size of analysis window in sample)",
		" nstep=8               (smoothing iteration)",
		"",
		" How to use :",
		" wave_strucsmooth < input.su > output.su",

		NULL};

float meandata (float **data, int row, int col);
int main(int argc, char **argv)
{
	int nsp, ntrc;
	int i,j;

	float k, sigma, sigmaderiv, dts;
	int klength, nstep;
	float **data=NULL;

	/* hook up getpar to handle the parameters */
	initargs(argc,argv);
	requestdoc(1);

	//input parameter
	if(!getparfloat("sigma",&sigma)) 	sigma = 0.5;
	if(!getparfloat("sigmaderiv",&sigmaderiv)) 	sigmaderiv = 0.5;
	if(!getparfloat("k",&k)) 	k = 0.5;
	if(!getparfloat("dts",&dts)) 	dts = 0.5;
	if(!getparint("klength",&klength)) 	klength = 5;
	if(klength%2==0) klength++;
	if(!getparint("nstep",&nstep)) 	nstep = 8;


	/*read first trace*/
	ntrc = getNumberOfTraceSuFile(stdin);
	gettr(&tr);
	nsp = tr.ns;
	rewind(stdin);

	/* initialization variable */
	data = alloc2float(nsp, ntrc);

	//read all data
	for (i=0; i<ntrc; i++)
	{
		gettr(&tr);
		for(j=0; j<nsp; j++)
			data[i][j] = tr.data[j];
	}

	wvstrucsmooth(data, ntrc, nsp, sigma, sigmaderiv, k, dts, klength, nstep);

	rewind(stdin);
	for(i=0; i<ntrc; i++)
	{
		gettr(&tr);
		for(j=0; j<nsp; j++)
			tr.data[j] = data[i][j];
		puttr(&tr);
	}

	free2float(data);

	return(1);
}

float meandata (float **data, int row, int col)
{
	int i, j;
	float sumd;

	sumd = 0.0;
	for(i=0; i<row; i++)
		for(j=0; j<col; j++)
			sumd = sumd + data[i][j];

	return(sumd);
}
