/*
 * segy_calc.c
 *
 *  Created on: Feb 7, 2012
 *      Author: toto
 */
#include "segy_lib.h"

char *sdoc[] = {
		"									",
		" SEGY_CALC --- CALCULATE TWO SEGY DATA		",
		" segy_calc inp1= inp2= > out.sgy [optional parameters]		",
		"",
		" parameter:		",
		" m1=1.0       multiply m1 by trace1",
		" m2=1.0       multiply m2 by trace2",
		" verbose=1    (verbose)",
		" type=        sum    -> sum data ",
		"              subs   -> substract data",
		"              mul    -> multiply data",
		"              div    -> divide data",
		"",
		" sample command :",
		" segy_calc inp1= inp2= m1= m2= type=sum > out.sgy",
		" format computation : [m1*inp1]  type  [m2*inp2] = out",

		NULL};

int main (int argc, char **argv)
{
	FILE *inp1=NULL, *inp2=NULL;
	char *cinp1, *cinp2;
	char *type;
	int itype, endian;
	float m1, m2;

	char ebcdic1[3200], ebcdic2[3200];
	bhed bh1, bh2;
	segy tr1, tr2;
	int nsegy1, nsegy2, ntrc1, ntrc2, dt1, dt2, nsp1, nsp2;
	int format1, format2;

	int i,j;

	/* hook up getpar to handle the parameters */
	initargs(argc,argv);
	requestdoc(1);

	/*cek list file exist or not*/
	MUSTGETPARSTRING("inp1",  &cinp1);	/* input file 1 */
	MUSTGETPARSTRING("inp2",  &cinp2);	/* input file 2 */
	MUSTGETPARSTRING("type",  &type);
	if (!getparfloat("m1",&m1)) 	m1 = 1.0;
	if (!getparfloat("m2",&m2)) 	m2 = 1.0;
	if (!getparint("endian",&endian)) 	endian = 0;

	/* open file */
	inp1 = fopen (cinp1,"r");
	if (inp1==NULL)
		err ("Error opening list file %s\n", cinp1);

	inp2 = fopen (cinp2,"r");
	if (inp2==NULL)
		err ("Error opening list file %s\n", cinp2);

	/*read header file */
	readEbcdicHeader(inp1, ebcdic1);
	readBinaryHeader(inp1, endian, &bh1, &nsegy1);
	ntrc1 = getNumberOfTraceSegyFile(inp1, nsegy1);
	nsp1 = bh1.hns;
	dt1 = bh1.hdt;
	format1 = bh1.format;

	readEbcdicHeader(inp2, ebcdic2);
	readBinaryHeader(inp2, endian, &bh2, &nsegy2);
	ntrc2 = getNumberOfTraceSegyFile(inp2, nsegy2);
	nsp2 = bh2.hns;
	dt2 = bh2.hdt;
	format2 = bh2.format;

	if( (ntrc1!=ntrc2) || (nsp1!=nsp2) || (dt1!=dt2) )
		err("input file have different value at ntrc, nsp or dt");

	if(strcmp(type, "sum")==0) {
		warn("sum data");
		itype = 0;
	}
	else if(strcmp(type, "subs")==0) {
		warn("substract data");
		itype = 1;
	}
	else if(strcmp(type, "mul")==0) {
		warn("multiply data");
		itype = 2;
	}
	else if(strcmp(type, "div")==0) {
		warn("divide data");
		itype = 3;
	}
	else
	{
		warn("sum data");
		itype = 0;
	}

	/*go to first trace */
	gotoTraceSegyPosition(inp1, 0, nsegy1);
	gotoTraceSegyPosition(inp2, 0, nsegy2);

	/*write header for segy output */
	bh1.format = 1;
	writeEbcdicHeader(stdout, ebcdic1);
	writeBinaryHeader(stdout, endian, &bh1);

	for(i=0; i<ntrc1; i++)
	{
		readTraceSegy(inp1, endian, nsp1, format1, nsegy1, &tr1);
		readTraceSegy(inp2, endian, nsp2, format2, nsegy2, &tr2);

		for(j=0; j<nsp1; j++)
		{
			if(itype==0)
				tr1.data[j] = (m1*tr1.data[j]) + (m2*tr2.data[j]);
			else if(itype==1)
				tr1.data[j] = (m1*tr1.data[j]) - (m2*tr2.data[j]);
			else if(itype==2)
				tr1.data[j] = (m1*tr1.data[j]) * (m2*tr2.data[j]);
			else if(itype==3)
				tr1.data[j] = (m1*tr1.data[j]) / (m2*tr2.data[j]);
			else
				tr1.data[j] = (m1*tr1.data[j]) + (m2*tr2.data[j]);
		}

		writeTraceSegy(stdout, endian, &tr1, nsegy1, nsp1);
	}

	warn("Process Complete");

	fclose(inp1);
	fclose(inp2);

	return(1);
}
