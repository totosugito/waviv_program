/*
 * wave_trimst.c
 *
 *  Created on: Sep 26, 2012
 *      Author: toto
 */

#include "segy_lib.h"
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include "../src_lib/potashLib.h"

/*********************** self documentation *****************************/
char *sdoc[] = {
		"									",
		" WAVE_TRIMST = cross-correlation statics",
		"                                                                       ",
		" wave_trimst <infile >outfile  [optional parameters]                 	",
		"                                                                       ",
		" Optional Parameters:                                                  ",
		"				                                	",
		" ftype=0           0=SU file, 1=SEGY file                               	",
		" key=cdp           Key header word to window on (see segy.h)",
		" endian=0          set =0 for little-endian machines(PC's,DEC,etc.)",
		" ntr=              Maximum number of traces in KEY",
		" tmin=0.1          minimum time in correlation window (s)  ",
		" tmax=1.1          maximum time in correlation window (s)  ",
		" maxshft=0.010     maximum allowable time shift  (s)       ",
		"",
		" a=0               if the computed shift is larger than    ",
		"                   maxshft and a=0 no time shift is performed ",
		"                   if a=1 maxshft time shift is applied    ",
		"								",
		" nit=3             Number of iteration to do on the gather ",

		"",
		NULL};
float *runTrimSt(float **traces, int ntr, int nt, int nit, int ntx, int maxlag, int a);
void writeSegyTraceData1(FILE *fod, float **traces, char **headers,
		int ntrc, int nsp, int endian, int nsegy, float *xcorv);
void writeSuTraceData1(FILE *fod, float **traces, char **headers,
		int ntrc, int nsp, float *xcorv);
int main(int argc, char **argv)
{
	char ebcdic[3200];
	bhed bh;
	int nt, format=0, nsegy, endian;
	cwp_String key;		/* header key word from segy.h		*/
	int maxtrrecords;

	int i, ftype;
	float dt;
	segy tr1;

	float **traces;
	char **headers;

	float *xcorv=NULL;
	int a, nit, ntx, maxlag;
	float tmin, tmax, maxshft;
	int idxtr, idxRecords, indx, totTr;
	Value *n_val, *o_val;

	div_t divresult;
	time_t t1,t2;

	/* Initialize */
	initargs(argc, argv);
	requestdoc(1);

	/* get general flags and parameters and set defaults */
	if (!getparint("ftype",&ftype)) ftype=0;
	if (!getparint("endian",&endian)) endian=0;
	if (!getparstring("key", &key))		key = "cdp";
	if (!getparint("ntr",&maxtrrecords)) 	err("ntr=??");

	if (!getparint("a", &a))		a = 0;
	if (!getparfloat("tmin",&tmin))		tmin = 0.1;
	if (!getparfloat("tmax",&tmax))		tmax = 1.1;
	if (!getparfloat("maxshft",&maxshft))	maxshft = 0.010;
	if (!getparint("nit",&nit))		nit = 3;

	t1 = time(NULL);

	/*read header file */
	if(ftype) //segy file
	{
		readEbcdicHeader(stdin, ebcdic);
		readBinaryHeader(stdin, endian, &bh, &nsegy);
		totTr = getNumberOfTraceSegyFile(stdin, nsegy);

		nt = bh.hns;
		format = bh.format;
		dt = (float) (bh.hdt/1000000.0);
	}
	else
	{
		gettr(&tr1);
		totTr = getNumberOfTraceSuFile(stdin);
		nt = tr1.ns;
		dt = (float) (tr1.dt/1000000.0);
	}
	/* Constans */
	ntx = NINT((tmax-tmin)/dt);
	maxlag = NINT(maxshft/dt);

	o_val = (Value*) calloc(1, sizeof(Value));
	n_val = (Value*) calloc(1, sizeof(Value));
	indx = getindex(key);

	traces = alloc2float(nt, maxtrrecords);
	headers = alloc2char(HDRBYTES, maxtrrecords);
	idxtr = 0;
	idxRecords = 0;

	/*write header for segy output */
	if(ftype) //segy file
	{
		bh.format = 1;
		writeEbcdicHeader(stdout, ebcdic);
		writeBinaryHeader(stdout, endian, &bh);

		readTraceSegy(stdin, endian, nt, format, nsegy, &tr1); //read the first trace
	}
	else
		gettr(&tr1);

	gethval(&tr1, indx, o_val);
	memcpy(traces[idxtr], tr1.data, nt*sizeof(float));
	memcpy(headers[idxtr], (char*)&tr1, HDRBYTES*sizeof(char));

	for(i=1; i<totTr; i++)
	{
		if(ftype) //segy file
			readTraceSegy(stdin, endian, nt, format, nsegy, &tr1);
		else
			gettr(&tr1);

		gethval(&tr1, indx, n_val);
		if(o_val->i==n_val->i)
		{
			idxtr++;

			if(idxtr>=maxtrrecords)
				err("trace in records [%i] >= ntr[%i] trace=%i", idxtr, maxtrrecords, i);

			memcpy(traces[idxtr], tr1.data, nt*sizeof(float));
			memcpy(headers[idxtr], (char*)&tr1, HDRBYTES*sizeof(char));
		}
		else
		{
			fprintf(stderr, "Run Records %i = %i traces, tr=%i/%i \n", idxRecords+1, idxtr+1, i, totTr);
			xcorv = runTrimSt(traces, idxtr+1, nt, nit, ntx, maxlag, a);

			if(ftype) //segy file
				writeSegyTraceData1(stdout, traces, headers, idxtr+1, nt, endian, nsegy, xcorv);
			else
				writeSuTraceData1(stdout, traces, headers, idxtr+1, nt, xcorv);

			free1float(xcorv);

			//update old variable
			idxRecords++;
			idxtr = 0;
			o_val->i = n_val->i;
			memcpy(traces[idxtr], tr1.data, nt*sizeof(float));
			memcpy(headers[idxtr], (char*)&tr1, HDRBYTES*sizeof(char));
		}
	}

	//process the last records
	fprintf(stderr, "Run Records %i = %i traces, tr=%i/%i \n", idxRecords+1, idxtr+1, i, totTr);
	xcorv =runTrimSt(traces, idxtr+1, nt, nit, ntx, maxlag, a);

	if(ftype) //segy file
		writeSegyTraceData1(stdout, traces, headers, idxtr+1, nt, endian, nsegy,xcorv);
	else
		writeSuTraceData1(stdout, traces, headers, idxtr+1, nt, xcorv);

	free2float(traces);
	free2char(headers);
	free(o_val);
	free(n_val);
	free1float(xcorv);

	t2 = time(NULL);
	divresult = div (t2-t1, 60);
	fprintf (stderr, "Process time = %d min %d sec\n", divresult.quot, divresult.rem);

	return(1);
}

float *runTrimSt(float **traces, int ntr, int nt, int nit, int ntx, int maxlag, int a)
{
	int iter, ix, it;
	float norm_str;
	float norm_tr;
	int *nnz;
	float *sptr;		/* super trace */
	float *xtr;		/* correlaton window */
	int lag=0;
	float *xcorv;		/* x correlation value */

	xtr = alloc1float(ntx);
	nnz = alloc1int(nt);
	sptr = alloc1float(nt);
	xcorv = alloc1float(ntr);
	for(iter=0;iter<nit;iter++)
	{
		/* Stack a super trace */
		memset( (void *) sptr, (int) '\0', nt*FSIZE);
		memset( (void *) nnz, (int) '\0', nt*FSIZE);

		for(ix=0; ix<ntr; ix++)
		{
			for(it=0; it<nt; it++)
			{
				if(!CLOSETO(traces[ix][it],0.0))
				{
					sptr[it] += traces[ix][it];
					nnz[it]+=1;
				}
			}
		}

		/* Normalize */
		for(it=0; it<nt; it++)
			if(nnz[it])
				sptr[it] /= (float)nnz[it];

		norm_str=0.0;
		for(it=0; it<nt; it++)
			norm_str+=SQR(sptr[it]);
		norm_str = sqrt(norm_str);

		/* Compute the shifts and apply them */
		for(ix=0; ix<ntr; ix++)
		{
			/* do the cross correlation */
			/* xcor(nt,0,sptr,nt,0,traces[ix],ntx,-ntx/2,xtr); */
			do_facor_sulib(sptr,traces[ix],xtr,nt,-ntx/2,ntx);

			norm_tr=0.0;
			for(it=0; it<nt; it++)
				norm_tr+=SQR(traces[ix][it]);
			norm_tr = sqrt(norm_tr);

			/* pick the maximum */
			lag = -isamaxs(ntx,xtr,1)+ntx/2-1;
			xcorv[ix] = xtr[isamaxs(ntx,xtr,1)]/(norm_tr*norm_str);

			/* do not exceed maxshift */
			if(abs(lag) > maxlag)
			{
				lag= lag/abs(lag)*maxlag;
				if(a==0) lag=0;
			}


			/* apply the shift */
			if ( lag <= 0)
			{
				memmove((void*) &(traces[ix])[0], (const void *)
						&(traces[ix])[abs(lag)],
						(size_t) (nt-abs(lag))*FSIZE);
			}
			else
			{
				memmove((void*) &(traces[ix])[lag], (const void *)
						&(traces)[ix][0],
						(size_t) (nt-lag)*FSIZE);
			}

		}
	}

	free1float(xtr);
	free1int(nnz);
	free1float(sptr);

	return(xcorv);
}


void writeSegyTraceData1(FILE *fod, float **traces, char **headers,
		int ntrc, int nsp, int endian, int nsegy, float *xcorv)
{
	int i;

	for(i=0; i<ntrc; i++)
	{
		memcpy((char*)&tr, headers[i], HDRBYTES);
		memcpy(tr.data, traces[i], nsp*sizeof(float));
		tr.corr=NINT(xcorv[i]*1000.0);
		writeTraceSegy(fod, endian, &tr, nsegy, nsp);
	}
}

void writeSuTraceData1(FILE *fod, float **traces, char **headers,
		int ntrc, int nsp, float *xcorv)
{
	int i;

	for(i=0; i<ntrc; i++)
	{
		memcpy((char*)&tr, headers[i], HDRBYTES);
		memcpy(tr.data, traces[i], nsp*sizeof(float));
		tr.corr=NINT(xcorv[i]*1000.0);
		puttr(&tr);
	}
}

