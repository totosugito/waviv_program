/*
 * segy_extractheader.c
 *
 *  Created on: Jul 17, 2012
 *      Author: toto
 */

#include "../src_lib/segy_lib.h"
#include <time.h>
char *sdoc[] = {
		"									",
		" SEGY_EXTRACTDATA = EXTRACT SEGY HEADER AND DATA",
		"",
		" segy_extractdata < input.sgy key=1,4i/115,2i/33,4e/37,4f/ > output.txt",
		" vblock=1000      (show verbose every .. traces)",
		" start=0          read trace starting from ..",
		" end=0            read trace until .. (0=read until the end of file)",
		" spacing=1        read data with spacing",
		" scalco=1         segy scalco value",
		" header=          header data",
		" ",
		" Extract Header : ",
		" Byte 1   - Type 4 Byte Integer",
		" Byte 115 - Type 2 Byte Integer",
		" Byte 33  - Type 4 Byte Floating IEEE",
		" Byte 37  - Type 4 Byte IBM Floating Point",
		"",
		NULL};

typedef struct headerInfo{
	int pos;
	int len;
	char type;
}headerInfo;

headerInfo *scanHeaderPos(char *str, int *lenHdrInfo);
void printHdrInfo(headerInfo *hdrinfo, int len);

int main(int argc, char **argv)
{

	initargs(argc,argv);
	requestdoc(1);

	char ebcdic[3200];
	bhed bh;
	headerInfo *hdrinfo;
	unsigned char *header, *data;
	int lenHdrInfo;
	char *key;
	int endian, nsegy, ntrc, result;
	int i, j, k, ndata;
	int iv, vblock;
	int istart, iend;
	float fv;
	int nsp;
	float dt;
	int space;
	int ipos;
	int scalco;
	float fscalco;
	char *addheader;
	char *splitStr, tmpheader;
	div_t divresult;
	time_t t1,t2;

	MUSTGETPARSTRING("key", &key);
	if (!getparint("endian",&endian)) endian=0;
	if (!getparint("start",&istart)) istart=0;
	if (!getparint("end",&iend)) iend=0;
	if (!getparint("vblock",&vblock)) vblock=1000;
	if (!getparint("spacing",&space)) space=1;
	if (!getparint("scalco",&scalco)) scalco=1;
	if (!getparstring("header",&addheader)) addheader="";
	if(scalco>0)
		fscalco = (float) (scalco);
	else
		fscalco = -1.0/scalco;

	t1 = time(NULL);
	hdrinfo = scanHeaderPos(key, &lenHdrInfo);
	printHdrInfo(hdrinfo, lenHdrInfo);

	readEbcdicHeader(stdin, ebcdic);
	readBinaryHeader(stdin, endian, &bh, &nsegy);
	ntrc = getNumberOfTraceSegyFile(stdin, nsegy);
	nsp = bh.hns;
	dt = bh.hdt/1000000.0;

	ndata = nsegy-SEGY_HDRBYTES;

	if(istart<0)
		istart = 0;
	if(iend==0 || iend > ntrc)
		iend = ntrc-1;

	header = (unsigned char*) calloc(SEGY_HDRBYTES, sizeof(unsigned char));
	data = (unsigned char*) calloc(ndata, sizeof(unsigned char));
	gotoTraceSegyPosition(stdin, istart, nsegy);

	if(strlen(addheader)!=0)
	{
		splitStr = strtok (addheader, ",");
		i = 0;
		while (splitStr != NULL)
		{
			sscanf(splitStr, "%s", &tmpheader);
			printf("%10s \t", &tmpheader);
			splitStr = strtok (NULL, ",");
		}
		printf("\n");
	}
	else
		printf("            ILINE 	   XLINE 	     SX 	     GX 	 TIME          VALUE\n");

	for (i=istart; i<=iend; i++)
	{
		if(i%vblock==0)
			fprintf(stderr, "Read Trace %i / %i \n", i+1, iend);

		result = fread(header, SEGY_HDRBYTES, sizeof(unsigned char), stdin);
		result = fread(data, ndata, sizeof(unsigned char), stdin);


		for (k=0; k<nsp; k=k+space)
		{
			for (j=0; j<lenHdrInfo; j++)
			{
				if(hdrinfo[j].type=='i')
				{
					iv = uchar2int(header, hdrinfo[j].pos, hdrinfo[j].len, endian);
					if((hdrinfo[j].pos==73) || (hdrinfo[j].pos==77))
					{
						fv = iv*fscalco;
						printf("%15.2f\t", fv);
					}
					else
						printf("%15i\t", iv);
				}
				else if(hdrinfo[j].type=='e') {
					fv = uchar2float(header, hdrinfo[j].pos, hdrinfo[j].len, endian);
					printf("%15.2f \t", fv);
				}
				else if(hdrinfo[j].type=='f') {
					fv = uchar2ibm(header, hdrinfo[j].pos, hdrinfo[j].len, endian);
					printf("%15.2f \t", fv);
				}
				else {
					fprintf(stderr, "\nInput type must be 2i, 4i, 4f or 4e \n\n");
					exit(0);
				}
			}

			ipos = k*4+1;
			if(bh.format==1)
			{
				fv = uchar2ibm(data, ipos, 4, endian);
				printf("%10.2f \t %15.5f \n", k*dt*1000, fv);
			}
			else if(bh.format==5)
			{
				fv = uchar2float(data, ipos, 4, endian);
				printf("%10.2f \t %15.5f \n", k*dt*1000, fv);
			}
			else
				err("segy format must be 1 or 5");
		}
		//printf("\n");
	}

	free(header);
	free(data);
	free(hdrinfo);
	t2 = time(NULL);
	divresult = div (t2-t1, 60);
	fprintf (stderr, "Process time = %d min %d sec\n", divresult.quot, divresult.rem);
	return(1);
}

headerInfo *scanHeaderPos(char *str, int *lenHdrInfo)
{
	int i;
	int lenStr;
	headerInfo *hdrinfo;
	char *splitStr;

	lenStr = 0;
	for(i=0; i<strlen(str); i++)
	{
		if(str[i]=='/')
			lenStr = lenStr+1;
	}

	hdrinfo = (headerInfo*) calloc(lenStr, sizeof(headerInfo));
	splitStr = strtok (str, "/");
	i = 0;
	while (splitStr != NULL)
	{
		sscanf(splitStr, "%i,%i%c", &hdrinfo[i].pos, &hdrinfo[i].len, &hdrinfo[i].type);
		splitStr = strtok (NULL, "/");
		i = i+1;
	}

	(*lenHdrInfo) = lenStr;
	return(hdrinfo);
}

void printHdrInfo(headerInfo *hdrinfo, int len)
{
	int i;
	fprintf(stderr, "\n PRINT HEADER INFO \n");
	for(i=0; i<len; i++)
		fprintf(stderr,"%i. POS=%i  LEN=%i   TYPE=%c \n",
				i+1, hdrinfo[i].pos, hdrinfo[i].len, hdrinfo[i].type);
}
