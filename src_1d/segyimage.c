/* Copyright (c) Colorado School of Mines, 2010.*/
/* All rights reserved.                       */

/* SUXIMAGE: $Revision: 1.38 $ ; $Date: 2005/02/16 23:07:41 $		*/

#include "segy_lib.h"
#include <signal.h>

/*********************** self documentation *****************************/
char *sdoc[] = {
		"									",
		" SEGYIMAGE - X-windows IMAGE plot of a segy data set	                ",
		"									",
		" segyimage infile= [optional parameters] | ...  (direct I/O)            ",
		"  or					                		",
		" segyimage <stdin [optional parameters] | ...	(sequential I/O)        ",
		"									",
		" Optional parameters:						 	",
		"									",
		" infile=NULL SU data to be ploted, default stdin with sequential access",
		"             if 'infile' provided, su data read by (fast) direct access",
		"									",
		"	      with ftr,dtr and n2 suximage will pass a subset of data   ",
		"	      to the plotting program-ximage:                           ",
		" ftr=1       First trace to be plotted                                 ",
		" dtr=1       Trace increment to be plotted                             ",
		" n2=tr.ntr   (Max) number of traces to be plotted (ntr is an alias for n2)",
		"	      Priority: first try to read from parameter line;		",
		"		        if not provided, check trace header tr.ntr;     ",
		"		        if still not provided, figure it out using ftello",
		"									",
		" d1=tr.d1 or tr.dt/10^6	sampling interval in the fast dimension	",
		"   =.004 for seismic 		(if not set)				",
		"   =1.0 for nonseismic		(if not set)				",
		" 							        	",
		" d2=tr.d2		sampling interval in the slow dimension	        ",
		"   =1.0 		(if not set or was set to 0)		        ",
		"									",
		" key=			key for annotating d2 (slow dimension)		",
		" 			If annotation is not at proper increment, try	",
		" 			setting d2; only first trace's key value is read",
		" 							        	",
		" f1=tr.f1 or tr.delrt/10^3 or 0.0  first sample in the fast dimension	",
		" 							        	",
		" f2=tr.f2 or tr.tracr or tr.tracl  first sample in the slow dimension	",
		"   =1.0 for seismic		    (if not set)			",
		"   =d2 for nonseismic		    (if not set)			",
		" 							        	",
		" verbose=0             =1 to print some useful information		",
		"									",
		" 									",
		" Note that for seismic time domain data, the \"fast dimension\" is	",
		" time and the \"slow dimension\" is usually trace number or range.	",
		" Also note that \"foreign\" data tapes may have something unexpected	",
		" in the d2,f2 fields, use segyclean to clear these if you can afford	",
		" the processing time or use d2= f2= to override the header values if	",
		" not.									",
		"									",
		" See the ximage selfdoc for the remaining parameters.		        ",
		"									",
		NULL};

/* Credits:
 *
 *	CWP: Dave Hale and Zhiming Li (ximage, etc.)
 *	   Jack Cohen and John Stockwell (suximage, etc.)
 *	MTU: David Forel, June 2004, added key for annotating d2
 *      ConocoPhillips: Zhaobo Meng, Dec 2004, added direct I/O
 *
 * Notes:
 *
 *	When the number of traces isn't known, we need to count
 *	the traces for ximage.  You can make this value "known"
 *	either by getparring n2 or by having the ntr field set
 *	in the trace header.  A getparred value takes precedence
 *	over the value in the trace header.
 */
/**************** end self doc *******************************************/

int main(int argc, char **argv)
{
	char plotcmd[BUFSIZ];   /* build ximage command for popen	*/
	FILE *plotfp;		/* fp for plot data			*/
	int nt;			/* number of samples on trace	  	*/
	int ntr;      	        /* number of traces			*/
	int verbose;		/* verbose flag				*/
	float d1;		/* time/depth sample rate		*/
	float d2;		/* trace/dx sample rate			*/
	float f1;		/* tmin/zmin			   	*/
	float f2;		/* tracemin/xmin			*/
	cwp_Bool seismic;	/* is this seismic data?		*/
	cwp_Bool have_ntr=cwp_false;/* is ntr known from header or user?*/
	char *infile=NULL;      /* dataset file name */
	FILE *infp=stdin;       /* input SU dataset to be viewed */
	int dtr,ftr,ntraces,jtr;	/* work variables */

	char *cwproot=NULL;	/* value of CWPROOT environment variable*/
	char *bindir;		/* directory path for tmp files		*/

	char ebcdic[3200];
	int nsegy, format, endian;

	/* Initialize */
	initargs(argc, argv);
	requestdoc(1);

	/* ZM: begin: get the line number, trace number type of control parameters for plotting */
	if (!getparint("ftr", &ftr)) ftr=1; /* using fortran convention */
	if (!getparint("dtr", &dtr)) dtr=1;
	if (!getparint("endian", &endian)) endian=0;

	if (ftr==0) ftr=1; /* for C-people like me */
	ftr--;             /* convert to C convention */
	if (ftr<0) err("First trace to be plotted ftr=%d must be positive",ftr);
	if (dtr<=0) err("Trace increment dtr=%d must be positive",dtr);
	if (ftr<0) err("ftr=%d must not be negative",ftr);

	if (!getparstring("infile",&infile))  /* default is stdin */
	{
		infile=NULL;
		infp=stdin;
	}
	else               /* must provide a valid dataset name */
	{
		if ((infp=fopen(infile, "r"))==NULL)
			err("Can not open %s to plot",infile);
	}

	readEbcdicHeader(infp, ebcdic);
	readBinaryHeader(infp, endian, &bh, &nsegy);
	ntraces =getNumberOfTraceSegyFile(infp, nsegy);
	ntr = ntraces;
	nt = bh.hns;
	format = bh.format;

	/* Get info from first trace */
	readTraceSegy(infp, endian, nt, format, nsegy, &tr);
	seismic = ISSEISMIC(tr.trid);

	if (!getparint("verbose", &verbose))  verbose=0;
	if (!getparfloat("d1", &d1))
	{
		if      (tr.d1)  d1 = tr.d1;
		else if (tr.dt)  d1 = ((double) tr.dt)/1000000.0;
		else
		{
			if (seismic)
			{
				d1 = 0.004;
				warn("tr.dt not set, assuming dt=0.004");
			} else { /* non-seismic data */
				d1 = 1.0;
				warn("tr.d1 not set, assuming d1=1.0");
			}
		}
	}
	if (fabs(d1)<=0.1E-20) d1=1.0;

	if (!getparfloat("d2", &d2)) d2 = (tr.d2) ? tr.d2 : 1.0;
	if (fabs(d2)<=0.1E-20) d2=1.0;

	if (!getparfloat("f1", &f1))
	{
		if      (tr.f1)     f1 = tr.f1;
		else if (tr.delrt)  f1 = (float) tr.delrt/1000.0;
		else                f1 = 0.0;
	}

	if (!getparfloat("f2", &f2))
	{
		if	(tr.f2)	f2 = tr.f2;
		else if (tr.tracr)  f2 = (float) tr.tracr;
		else if (tr.tracl)  f2 = (float) tr.tracl;
		else if (seismic)   f2 = 1.0;
		else	            f2 = 0.0;
	}

	/* See if CWPBIN environment variable is not set */
	if (!(bindir = getenv("CWPBIN")))  /* construct bindir from CWPROOT */
	{
		bindir = (char *) emalloc(BUFSIZ);

		/* Get value of CWPROOT environment variable */
		if (!(cwproot = getenv("CWPROOT"))) cwproot ="" ;
		if (STREQ(cwproot, ""))
		{
			warn("CWPROOT environment variable is not set! ");
			err("Set CWPROOT in shell environment as per instructions in CWP/SU Installation README files");
		}
	}

	/* ZM: begin: get n2 by random access */
	/* Get or set ntr */
	if (getparint("n2", &ntr) || getparint("ntr", &ntr)) have_ntr = cwp_true;

	/* Set up ximage command line */
	if (verbose)
	{
		fprintf(stderr,
				"%s/bin/ximage n1=%d n2=%d d1=%f d2=%f f1=%f f2=%f", cwproot,
				nt, ntr, d1, d2, f1, f2);
	}

	sprintf(plotcmd,
			"%s/bin/ximage n1=%d n2=%d d1=%f d2=%f f1=%f f2=%f verbose=%d ", cwproot,
			nt, ntr, d1, d2, f1, f2, verbose);

	for (--argc, ++argv; argc; --argc, ++argv)
	{
		if (strncmp(*argv, "d1=", 3) && /* skip those already set */
				strncmp(*argv, "d2=", 3) &&
				strncmp(*argv, "f1=", 3) &&
				strncmp(*argv, "verbose=", 8) &&
				strncmp(*argv, "n1=", 3) &&
				strncmp(*argv, "n2=", 3) &&
				strncmp(*argv, "f2=", 3))
		{

			strcat(plotcmd, " ");   /* put a space between args */
			strcat(plotcmd, "\"");  /* user quotes are stripped */
			strcat(plotcmd, *argv); /* add the arg */
			strcat(plotcmd, "\"");  /* user quotes are stripped */
		}
	}


	/* Open pipe to ximage and send the traces */
	plotfp = epopen(plotcmd, "w");
	gotoTraceSegyPosition(infp, 0, nsegy);
	for(jtr=0; jtr<ntraces; jtr++)
	{
		readTraceSegy(infp, endian, nt, format, nsegy, &tr);
		efwrite(tr.data, FSIZE, nt, plotfp);
	}


	/* Clean up */
	epclose(plotfp);
	efclose(infp);

	return EXIT_SUCCESS;
}
