/*
 * wave_k1k2filter.c
 *
 *  Created on: Jan 26, 2012
 *      Author: toto
 */

/* Copyright (c) Colorado School of Mines, 2010.*/
/* All rights reserved.                       */

/* SUK1K2FILTER: $Revision: 1.12 $ ; $Date: 2006/11/07 22:58:42 $	*/

#include "segy_lib.h"
#include "smoothingLib.h"
#include <signal.h>

/*********************** self documentation **********************/
char *sdoc[] = {
		" 									",
		" WAVE_K1K2FILTER - symmetric box-like K-domain filter defined by the	",
		"		  cartesian product of two sin^2-tapered polygonal	",
		"		  filters defined in k1 and k2				",
		" 									",
		" wave_k1k2filter <infile >outfile [optional parameters]		",
		" cf1=",
		" cf2=",
		"									",
		" Optional parameters:							",
		" nord=3.0",
		" alpha=0.0",
		" nzm=0",
		" nxm=0",
		" d1=1.0	             sampling interval in first (fast) dimension	",
		" d2=1.0	             sampling interval in second (slow) dimension	",
		"",
		NULL};

/* Credits:
 *     CWP: John Stockwell, November 1995.
 *
 * Trace header fields accessed: ns, d1, d2
 */
/**************** end self doc ***********************************/


/* definitions */
#define PFA_MAX	720720	/* Largest allowed nfft		  */
#define LOOKFAC 2	/* look factor			  */
#define FRAC0   0.10    /* Ratio of default k1 to Nyquist */
#define FRAC1   0.15    /* Ratio of default k2 to Nyquist */
#define FRAC2   0.45    /* Ratio of default k3 to Nyquist */
#define FRAC3   0.50    /* Ratio of default k4 to Nyquist */


int main(int argc, char **argv)
{
	int nx1,nx2;		/* numbers of samples			*/
	float dx1,dx2;		/* sampling intervals			*/
	float d1,d2;

	float nyq1;		/* K1 Nyquist wavenumber		*/
	float nyq2;		/* K2 Nyquist wavenumber		*/

	int ix1,ix2;		/* sample indices			*/
	int nx1fft,nx2fft;	/* dimensions after padding for FFT	*/
	float onfft;
	int nK1,nK2;		/* transform (output) dimensions	*/
	int	nzm,nxm;


	int verbose;		/* flag for echoing info		*/
	float **data;
	float cf1,cf2,nord,alpha;

	/* Hook up getpar to handle the parameters */
	initargs(argc,argv);
	requestdoc(1);

	if (!getparfloat("cf1", &cf1)) err("cf1 not set");
	if (!getparfloat("cf2", &cf2)) err("cf2 not set");
	if (!getparfloat("nord", &nord)) nord=3.0;
	if (!getparfloat("alpha", &alpha)) alpha=0.0;
	if (!getparint("nzm", &nzm)) nzm=0;
	if (!getparint("nxm", &nxm)) nxm=0;
	if (!getparfloat("d1", &dx1)) dx1 = 1.0;
	if (!getparfloat("d2",&dx2))  dx2 = 1.0;
	if (!getparint("verbose", &verbose))	verbose = 0;

	gettr(&tr);
	nx1 = tr.ns;
	nx2 = getNumberOfTraceSuFile(stdin);

	alpha = (alpha * PI) / 180.0;
	/* Compute Nyquist wavenumbers */
	nyq1 = 0.5/dx1;
	nyq2 = 0.5/dx2;

	/* Determine lengths for prime-factor FFTs */
	nx1fft = npfaro(nx1, LOOKFAC*nx1);
	nx2fft = npfa(nx2);

	if (nx1fft >= SU_NFLTS || nx1fft >= PFA_MAX)
		err("Padded nx1=%d--too big",nx1fft);
	if (nx2fft >= SU_NFLTS || nx2fft >= PFA_MAX)
		err("Padded nx2=%d--too big",nx2fft);

	onfft = (float) 1.0/(nx1fft*nx2fft);

	/* Determine number of wavenumbers in K1 and K2 */
	nK1 = nx1fft/2 + 1;
	nK2 = nx2fft/2 + 1;

	/* Determine output header values */
	d1 = 2.0/(nx1fft*dx1);
	d2 = 2.0/(nx2fft*dx2);

	data = alloc2float(nx1, nx2);
	rewind(stdin);
	for (ix2=0; ix2<nx2; ++ix2)
	{
		gettr(&tr);
		for(ix1=0; ix1<nx1; ix1++)
			data[ix2][ix1] = tr.data[ix1];
	}


	k1k2filter(data, nx1, nx2, nx1fft, nx2fft, nord, alpha, nK1, nK2, nxm, nzm, cf1, cf2, onfft);

	/* Output filtered traces */
	rewind(stdin);
	for (ix2=0; ix2 < nx2; ++ix2)
	{
		gettr(&tr);
		for (ix1=0; ix1<nx1; ++ix1)
			tr.data[ix1] = data[ix2][ix1];

		puttr(&tr);
	}

	free2float(data);

	return(1);
}
