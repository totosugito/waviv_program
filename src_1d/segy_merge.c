/*
 * segy_merge.c
 *
 *  Created on: May 11, 2012
 *      Author: toto
 */
#include <stdio.h>
#include <stdlib.h>

int getNumberOfTraceSegyFile(FILE *inpf, int nsegy);
void getBinaryHeaderInfo(char *filename, int *ns, int *dt, int *format);
int char2int(unsigned char *data, int pos, int endian);
void usage();

int getNumberOfTraceSegyFile(FILE *inpf, int nsegy)
/* -----------------------------------------------------------------
 * get Number Of SEGY Trace From File
 * nsegy  -> size of byte in 1 trace
 * -----------------------------------------------------------------
 */
{
	off64_t begPos, endPos;  /*size of file input*/
	off64_t bytetr;  /*size of trace data and data */
	int retseek;
	int ntrc;              /*output number of traces*/

	ntrc = 0;
	begPos = ftello64(inpf);                /*get beginning of file pointer location*/

	retseek = fseeko64(inpf, 0 , SEEK_END); /* go to end of file */
	if(retseek) {
		fprintf(stderr, "getNumOfTrace : error seek command.");
		exit(0);
	}

	endPos = ftello64(inpf);                /* get size of file */

	bytetr = endPos - 3600;              /* remove ascii and binary trace */

	if((bytetr % nsegy)==0)            /* get number of traces*/
		ntrc = bytetr/nsegy;
	else
		ntrc = 0;

	retseek = fseeko64(inpf, begPos , SEEK_SET);
	if(retseek) {
		fprintf(stderr, "getNumOfTrace : error set location to beginning pointer.");
		exit(0);
	}

	return (ntrc);
}

void getBinaryHeaderInfo(char *filename, int *ns, int *dt, int *format)
/*
 * GET SEGY BINARY NUMBER INFO
 * ns = number of sample
 * dt = time sample
 * format = segy format (1,2,3,4,5,8)
 */
{
	int retseek;
	int endian;
	int ins, iformat, idt;
	int tmp1, tmp2;
	unsigned char binheader[400];
	FILE *fid=NULL;

	fid = fopen(filename, "r");
	if(!fid)
	{
		fprintf(stderr, "getBinaryHeaderInfo : error opening file %s \n", filename);
		exit(0);
	}

	retseek = fseeko64(fid, 3200 , SEEK_SET); /* go to binary header location */
	if(retseek) {
		fprintf(stderr, "getBinaryHeaderInfo : error set pointer to binary header location in file %s\n", filename);
		exit(0);
	}

	retseek = fread(binheader, sizeof(unsigned char), 400, fid);
	if(retseek!=400)
	{
		fprintf(stderr, "getBinaryHeaderInfo : error reading binary header in file %s\n", filename);
		exit(0);
	}

	//check endian
	tmp1 = char2int(binheader, 24, 1);
	tmp2 = char2int(binheader, 24, 0);
	if(tmp1<tmp2)	endian=1;
	else endian = 0;

	idt = char2int(binheader, 16, endian);	//get ns
	ins = char2int(binheader, 20, endian);	//get dt
	iformat = char2int(binheader, 24, endian);//get format

	(*dt) = idt;
	(*ns) = ins;
	(*format) = iformat;
	fclose(fid);
}

int char2int(unsigned char *data, int pos, int endian)
/*
 * CONVERT UNSIGNED CHAR TO INTEGER
 */
{
	int a, b;

	if(endian==0)
	{
		a = data[pos]<<8;
		b = data[pos+1] + a;
	}
	else
	{
		a = data[pos+1]<<8;
		b = data[pos] + a;
	}
	return(b);
}

void usage()
{
	printf("\nSEGY_MERGE \n");
	printf("by toto (toto-share.com)\n\n");
	printf("segy_merge segy1 segy2 segy3 ... segyn > output.sgy \n");
	printf("\n");
}

int main(int argc, char **argv)
{
	int i, j, result;
	int ns, dt, format, nsegy, ntrc;
	int tmpns, tmpdt, tmpformat;
	FILE *fid=NULL;
	unsigned char segy_header[3600];
	unsigned char *buffer;

	if(argc==1)
	{
		usage();
		return(0);
	}

	//check ns, dt and format segy
	getBinaryHeaderInfo(argv[1], &ns, &dt, &format);
	for (i=2; i<argc; i++)
	{
		getBinaryHeaderInfo(argv[i], &tmpns, &tmpdt, &tmpformat);
		if(ns!=tmpns || dt!=tmpdt || format!=tmpformat)
		{
			fprintf(stderr,"segy_merge : ns, dt, or format segy not equal at index %i \n", i);
			return(1);
		}
	}

	if(format==1) nsegy = 240 + (ns*4);
	else if(format==2) nsegy = 240 + (ns*4);
	else if(format==3) nsegy = 240 + (ns*2);
	else if(format==4) nsegy = 240 + (ns*4);
	else if(format==5) nsegy = 240 + (ns*4);
	else if(format==8) nsegy = 240 + (ns);
	else
	{
		fprintf(stderr,"segy_merge : format file must be 1, 2, 3, 4, 5 or 8 \n");
		return(1);
	}

	// --------------- MERGE FILE --------------
	buffer = (unsigned char*) calloc(nsegy, sizeof(unsigned char));	//allocate header+trace
	for(i=1; i<argc; i++)
	{
		fid = fopen(argv[i], "r");
		if(!fid){
			fprintf(stderr,"Error opening file %s \n", argv[i]);
			exit(0);
		}
		ntrc = getNumberOfTraceSegyFile(fid, nsegy);	//get number of trace from segy file

		if(i==1)	//read and write segy header
		{
			result = fread(segy_header, sizeof(unsigned char), 3600, fid);
			result = fwrite(segy_header, sizeof(unsigned char), 3600, stdout);
		}
		else
			fseeko64(fid, 3600, SEEK_SET);	//skip reading segy header

		for(j=0; j<ntrc; j++)
		{
			if(j%1000==0)
				fprintf(stderr,"open file %i/%i, reading traces %i from %i \n",
						i, argc-1, j+1, ntrc);
			result = fread(buffer, sizeof(unsigned char), nsegy, fid);	//read trace
			result = fwrite(buffer, sizeof(unsigned char), nsegy, stdout);//write trace
		}
		fclose(fid);

	}

	free(buffer);
	fprintf(stderr,"Merging File Complete \n");
	return(1);
}
