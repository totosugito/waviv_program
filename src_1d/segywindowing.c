/*
 * segywindowing.c
 *
 *  Created on: Feb 28, 2012
 *      Author: toto
 */

#include "segy_lib.h"
#include <string.h>
#include <stdlib.h>
#include <time.h>

/*********************** self documentation *****************************/
char *sdoc[] = {
		"									",
		" SEGYWINDOWING	: SAVE SEGY DATA AT SELECTED TIME	",
		"",
		" itmin=0             (save from time index)",
		" itmax=0             (save until time index)",
		" vblock=10000        (verbose every ... traces)",
		" verbose=1           (show debug)",
		" endian=0            (0=little endian)",
		"									",
		" How to Use :",
		" segywindowing < input.sgy  itmin= itmax= >output.sgy",
		NULL};


unsigned char *getNew_ns(int ns, int endian);
void updateNs_value(unsigned char *header, unsigned char *newns);
int main(int argc, char **argv)
{
	int i, ntrc, idxblock;
	int itmin, itmax, vblock, verbose;
	int ndata1, ndata2;
	unsigned char *newns;

	int ns1, ns2, nsegy1;
	float dt;
	int endian, format;
	char ebcdic[3200];
	div_t divresult;
	time_t t1,t2;
	size_t result;
	int idx1, idx2;

	unsigned char *hbuffer, *dbuffer1, *dbuffer2;
	/* Initialize */
	initargs(argc, argv);
	requestdoc(1);

	if (!getparint("itmin", &itmin)) itmin=0;
	if (!getparint("itmax", &itmax)) itmax=1;
	if (!getparint("vblock", &vblock)) vblock=10000;
	if (!getparint("verbose", &verbose)) verbose=1;
	if (!getparint("endian", &endian)) endian=0;

	/* start process */
	t1 = time(NULL);

	/* opening file segy*/
	readEbcdicHeader(stdin, ebcdic);
	readBinaryHeader(stdin, endian, &bh, &nsegy1);
	ntrc =getNumberOfTraceSegyFile(stdin, nsegy1);
	ns1 = bh.hns;
	format = bh.format;
	dt = bh.hdt;

	if(itmin==0)
		itmin=0;
	else if(itmin<0)
	{
		fprintf(stderr,"itmin < 0, set itmin=0 \n");
		itmin=0;
	}
	else if(itmin>ns1)
	{
		fprintf(stderr,"itmin >= ns, set itmin=0 \n");
		itmin=0;
	}


	if(itmax==0)
		itmax=ns1;
	else if(itmax<0)
	{
		fprintf(stderr,"itmax < 0, set itmax=ns \n");
		itmax = ns1;
	}
	else if(itmax>ns1)
	{
		fprintf(stderr,"itmax > ns, set itmax=ns \n");
		itmax = ns1;
	}


	/*update the new variable */
	ns2 = itmax-itmin;
	ndata1 = ns1*4;
	ndata2 = ns2*4;
	bh.hns = ns2;
	bh.format = 1;
	idx1 = itmin*4;
	idx2 = itmax*4;

	writeEbcdicHeader(stdout, ebcdic);
	writeBinaryHeader(stdout, endian, &bh);

	newns = getNew_ns(ns2, endian); //get new ns for trace header

	idxblock=0;
	hbuffer = (unsigned char*) calloc(240,sizeof(unsigned char));
	dbuffer1 = (unsigned char*) calloc(ndata1,sizeof(unsigned char));
	dbuffer2 = (unsigned char*) calloc(ndata2,sizeof(unsigned char));
	for(i=0; i<ntrc; i++)
	{
		if(verbose==1 && idxblock*vblock==i)
		{
			fprintf(stderr, "Read Trace [%i / %i] \n", i, ntrc);
			idxblock = idxblock + 1;
		}

		result = fread(hbuffer, sizeof(unsigned char), 240, stdin);
		result = fread(dbuffer1, sizeof(unsigned char), ndata1, stdin);
		memcpy(dbuffer2, dbuffer1+idx1, ndata2*sizeof(unsigned char));

		updateNs_value(hbuffer, newns); //update ns value in header
		fwrite(hbuffer, sizeof(unsigned char), 240, stdout);
		fwrite(dbuffer2, sizeof(unsigned char), ndata2, stdout);

		memset(hbuffer, 0, 240*sizeof(unsigned char));
		memset(dbuffer1, 0, ndata1*sizeof(unsigned char));
		memset(dbuffer2, 0, ndata2*sizeof(unsigned char));
	}
	free(hbuffer);
	free(dbuffer1);
	free(dbuffer2);
	free(newns);
	t2 = time(NULL);
	divresult = div (t2-t1, 60);
	fprintf (stderr, "Process time = %d min %d sec\n", divresult.quot, divresult.rem);

	return(1);
}

unsigned char *getNew_ns(int ns, int endian)
{
	int a,b;
	unsigned char *newns;

	a = ns>>8;
	b = ns-a*256;
	newns = (unsigned char*) calloc(2,sizeof(unsigned char));
	if(endian==0)
	{
		newns[0] = a;
		newns[1] = b;
	}
	else
	{
		newns[0] = b;
		newns[1] = a;
	}
	return(newns);
}

void updateNs_value(unsigned char *header, unsigned char *newns)
{
	memcpy(header+114, newns, 2*sizeof(unsigned char));
//	memcpy(header+23, newns, 2*sizeof(unsigned char));
}
