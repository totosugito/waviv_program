/*
 * segyswapheader.c
 *
 *  Created on: Mar 22, 2012
 *      Author: toto
 */

#include "segy_lib.h"

char *sdoc[] = {
		"									",
		" SEGYSWAPHEADER --- SWAP SEGY HEADER		",
		"                REPLACE HEADER FROM inp2 WITH inp1",
		" segyswapheader inp1= inp2= > out.sgy [optional parameters]		",
		"",
		" parameter:		",
		" verbose=1    (verbose)",
		" endian=0     (0=little endian)",

		NULL};

int main (int argc, char **argv)
{
	FILE *inp1=NULL, *inp2=NULL;
	char *cinp1, *cinp2;
	int endian;

	char ebcdic1[3200], ebcdic2[3200];
	bhed bh1, bh2;
	int nsegy1, nsegy2, ntrc1, ntrc2, dt1, dt2, nsp1, nsp2;
	int format1, format2;
	size_t result1, result2, result3;

	int h1, h2;
	unsigned char *h11, *h12;
	unsigned char *h21, *h22;
	unsigned char *h31, *h32;
	int i, vblock;

	/* hook up getpar to handle the parameters */
	initargs(argc,argv);
	requestdoc(1);

	/*cek list file exist or not*/
	MUSTGETPARSTRING("inp1",  &cinp1);	/* input file 1 */
	MUSTGETPARSTRING("inp2",  &cinp2);	/* input file 2 */
	if (!getparint("endian",&endian)) 	endian = 0;
	if (!getparint("vblock",&vblock)) 	vblock = 10000;

	/* open file */
	inp1 = fopen (cinp1,"r");
	if (inp1==NULL)
		err ("Error opening list file %s\n", cinp1);

	inp2 = fopen (cinp2,"r");
	if (inp2==NULL)
		err ("Error opening list file %s\n", cinp2);

	/*read header file */
	readEbcdicHeader(inp1, ebcdic1);
	readBinaryHeader(inp1, endian, &bh1, &nsegy1);
	ntrc1 = getNumberOfTraceSegyFile(inp1, nsegy1);
	nsp1 = bh1.hns;
	dt1 = bh1.hdt;
	format1 = bh1.format;

	readEbcdicHeader(inp2, ebcdic2);
	readBinaryHeader(inp2, endian, &bh2, &nsegy2);
	ntrc2 = getNumberOfTraceSegyFile(inp2, nsegy2);
	nsp2 = bh2.hns;
	dt2 = bh2.hdt;
	format2 = bh2.format;

	nsegy1 = nsegy1-240;
	nsegy2 = nsegy2-240;

	if(ntrc1!=ntrc2)
		err("input file have different value at ntrc, nsp or dt");

	//rewind file
	rewind(inp1);
	rewind(inp2);

	//allocate array
	h1 = 3600;
	h2 = 240;
	h11 = (unsigned char*) calloc(h1, sizeof(unsigned char));
	h12 = (unsigned char*) calloc(h1, sizeof(unsigned char));
	h21 = (unsigned char*) calloc(h2, sizeof(unsigned char));
	h22 = (unsigned char*) calloc(h2, sizeof(unsigned char));
	h31 = (unsigned char*) calloc(nsegy1, sizeof(unsigned char));
	h32 = (unsigned char*) calloc(nsegy2, sizeof(unsigned char));

	//read header
	result1 = fread(h11, sizeof(unsigned char), h1, inp1);
	result2 = fread(h12, sizeof(unsigned char), h1, inp2);
	result3 = fwrite(h12, sizeof(unsigned char), h1, stdout);
	if(result1 != h1) err("error reading ebcdic header file %s", cinp1);
	if(result2 != h1) err("error reading ebcdic header file %s", cinp2);
	if(result3 != h1) err("error writing ebcdic header");

	for(i=0; i<ntrc1; i++)
	{
		if(i%vblock==0)
			fprintf(stderr, "Reading Trace [ %i / %i] \n", i+1, ntrc1);

		//read trace header
		result1 = fread(h21, sizeof(unsigned char), h2, inp1);
		result2 = fread(h22, sizeof(unsigned char), h2, inp2);
		if(result1 != h2) err("error reading trace header file %s at %i ", cinp1, i+1);
		if(result2 != h2) err("error reading trace header file %s at %i ", cinp2, i+1);

		//dont replace ns and dt header (byte 115-118)
		memcpy(h21+114, h22+114, 4*sizeof(unsigned char));

		//write trace header
		result3 = fwrite(h21, sizeof(unsigned char), h2, stdout);
		if(result3 != h2) err("error writing trace header");
		//------------------------------------------------------------------------------

		//read trace data
		result1 = fread(h31, sizeof(unsigned char), nsegy1, inp1);
		result2 = fread(h32, sizeof(unsigned char), nsegy2, inp2);
		if(result1 != nsegy1) err("error reading trace file %s at %i result=%i", cinp1, i+1, result1);
		if(result2 != nsegy2) err("error reading trace file %s at %i result=%i", cinp2, i+1, result2);

		//write trace data
		result3 = fwrite(h32, sizeof(unsigned char), nsegy2, stdout);
		if(result3 != nsegy2) err("error writing trace data");
	}

	warn("Process Complete");

	fclose(inp1);
	fclose(inp2);

	return(1);
}
