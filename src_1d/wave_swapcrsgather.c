/*
 * data_calc.c
 *
 *  Created on: Feb 7, 2011
 *      Author: toto
 */

#include "segy_lib.h"


char *sdoc[] = {
		"									",
		" WAVE_SWAPCRSGATHER --- Compare CRS with Gather data and ",
		"                        Remove duplicate traces. ",
		" wave_swapcrsgather gather=  crs=  out=outfile [optional parameters]		",
		"",
		" Input file in SEGY format",
		" parameter:		",
		" verbose=1    (verbose)",
		" endian=0    (little endian)",
		NULL};

int main (int argc, char **argv)
{
	int verbose;
	int itr, its, i, idx2;
	int cdp1, cdp2, offset1, offset2;
	int tmpcdp1, tmpoffset1;
	char *cinp1f, *cinp2f, *coutf;
	FILE *finp1=NULL, *finp2=NULL, *fout=NULL;

	int nsp1, nsp2, nsp, format1, format2, endian, duplicatetrace;
	bhed bh1, bh2;
	segy trace1, trace2;
	char cebcid1[3200], cebcid2[3200];
	int nsegy1, nsegy2, ntrc1, ntrc2;

	/* hook up getpar to handle the parameters */
	initargs(argc,argv);
	requestdoc(1);

	/*cek list file exist or not*/
	MUSTGETPARSTRING("gather",  &cinp1f);
	MUSTGETPARSTRING("crs",  &cinp2f);
	MUSTGETPARSTRING("out",  &coutf);
	if (!getparint("verbose",&verbose)) 	verbose = 1;  //show verbose
	if (!getparint("endian",&endian)) 	endian = 0;

	finp1 = fopen (cinp1f,"r");
	if (finp1==NULL) err ("Error opening file 1 : %s\n", cinp1f);

	finp2 = fopen (cinp2f,"r");
	if (finp2==NULL) err ("Error opening file 2 : %s\n", cinp2f);

	fout = fopen (coutf,"w");
	if (fout==NULL) err ("Error opening output file : %s\n", coutf);

	//read file 1 header information
	readEbcdicHeader(finp1, cebcid1); /* read ebcdic header */
	readBinaryHeader(finp1, endian, &bh1, &nsegy1); /*read binary header */
	ntrc1 = getNumberOfTraceSegyFile(finp1, nsegy1);

	//read file 2 header information
	readEbcdicHeader(finp2, cebcid2); /* read ebcdic header */
	readBinaryHeader(finp2, endian, &bh2, &nsegy2); /*read binary header */
	ntrc2 = getNumberOfTraceSegyFile(finp2, nsegy2);

	if( bh1.hns != bh2.hns) err("input ns is not equal. ns1=%i ns2=%i", bh1.hns, bh2.hns);

	if( bh1.hdt != bh2.hdt) err("input dt is not equal. dt1=%i dt2=%i", bh1.hdt, bh2.hdt);

	nsp1 = bh1.hns;
	nsp2 = bh2.hns;
	if(nsp1!=nsp2) err("ns1 <> ns2 ");
	nsp = nsp1;

	format1 = bh1.format;
	format2 = bh2.format;

	/*write output header */
	bh1.format = 1;
	writeEbcdicHeader(fout, cebcid1);
	writeBinaryHeader(fout, endian, &bh1);

	//loop over trace
	idx2=0;
	tmpcdp1 = 0;
	tmpoffset1 = 0;
	duplicatetrace = 0;
	for(itr=0; itr<ntrc1; itr++)
	{
		readTraceSegy(finp1, endian, nsp, format1, nsegy1, &trace1); /*read trace gather*/
		cdp1 = trace1.cdp;
		offset1 = trace1.offset;

		if((cdp1==tmpcdp1) && (offset1==tmpoffset1))
		{
			printf("%i. cdp=%i offset=%i\n", itr, cdp1, offset1);
			duplicatetrace++;
		}
		else
		{
			for(its=idx2; its<ntrc2; its++)
			{
				readTraceSegy(finp2, endian, nsp, format2, nsegy2, &trace2); /*read trace crs*/
				cdp2 = trace2.cdp;
				offset2 = trace2.offset;

				if(cdp1==cdp2)
				{
					if(offset1==offset2)
					{
						for(i=0; i<nsp; i++)
						{
							trace1.data[i] = trace2.data[i];
						}
						writeTraceSegy(fout, endian, &trace1, nsegy1, nsp);
						break;
					}
				}
				else if(cdp1<cdp2)
				{
					printf("can not find cdp %i offset=%i in crs data : cdp2=%i offset2=%i\n", cdp1, offset1, cdp2, offset2);
					writeTraceSegy(fout, endian, &trace1, nsegy1, nsp);
					gotoTraceSegyPosition(finp2, idx2, nsegy2);
					break;
				}
				idx2++;
			}
		}
		tmpcdp1 = cdp1;
		tmpoffset1 = offset1;

	}

	fprintf(stderr, "process %i traces\n", itr);
	fprintf(stderr, "get  duplicatetrace = %i traces\n", duplicatetrace);
	fclose(finp1);
	fclose(finp2);
	fclose(fout);
	return (1);
}
