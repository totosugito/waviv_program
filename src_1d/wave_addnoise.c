/*
 * wave_addnoise.c
 *
 *  Created on: Jan 8, 2013
 *      Author: toto
 */

#include "../src_lib/segy_lib.h"
#include <time.h>
#include <signal.h>

char *sdoc[] = {
		" 									",
		" WAVE_ADDNOISE - add noise to traces					",
		" 									",
		" wave_addnoise <stdin >stdout  sn=20  noise=gauss ",
		" 									",
		" 									",
		" Optional parameters:							",
		" ftype=0             0=SU file, 1=SEGY file                               	",
		" endian=0            set =0 for little-endian machines(PC's,DEC,etc.)",
		" nwindow=100         process traces every 'nwindow' traces",
		" vblock=10           show verbose every 'vblock' loops",
		"",
		" sn=20               signal to noise ratio			",
		" noise=gauss         noise probability distribution		",
		"                     =flat for uniform; default Gaussian	",
		" seed=from_clock     random number seed (integer)		",
//		"			",
//		" Required parameters:							",
//		" if any of f=f1,f2,... and amp=a1,a2,... are specified by the user",
		"",
		" Notes:								",
		" Output = Signal +  scale * Noise					",
		" 									",
		" scale = (1/sn) * (absmax_signal/sqrt(2))/sqrt(energy_per_sample)	",
//		" 									",
//		" If the signal is already band-limited, f=f1,f2,... and amps=a1,a2,...	",
//		" can be used, as in sufilter, to bandlimit the noise traces to match	",
//		" the signal band prior to computing the scale defined above.		",
//		" 									",
//		" Examples of noise bandlimiting:					",
//		" low freqency:    suaddnoise < data f=40,50 amps=1,0 | ...		",
//		" high freqency:   suaddnoise < data f=40,50 amps=0,1 | ...		",
//		" near monochromatic: suaddnoise < data f=30,40,50 amps=0,1,0 | ...	",
//		" with a notch:    suaddnoise < data f=30,40,50 amps=1,0,1 | ...	",
//		" bandlimited:     suaddnoise < data f=20,30,40,50 amps=0,1,1,0 | ...	",
		"",
		NULL};

/* Default signal to noise ratio */
#define SN	20

/* Noise probability distributions */
#define	GAUSS	0
#define	FLAT	1

//static char bandoutfile[L_tmpnam];  /* output file for sufilter	*/
//static FILE *bandoutfp;		    /* fp for output file	*/

void add_noise(int ftype, int ntrc, int nt, int itype, float sn,
		int endian, int format, int nsegy);

int main(int argc, char **argv)
{
	char cebcdic[3200];

	int nt;			/* number of points on trace		*/
	int ntrc;		/* number of traces			*/
	cwp_String stype;	/* noise type (gauss, flat) as string	*/
	int itype=GAUSS;	/* ... as integer (for use in switch)	*/
	float sn;		/* signal to noise ratio		*/
	unsigned int seed;	/* random number seed			*/
//	cwp_String f="";	/* frequency input for sufilter		*/
//	cwp_String amps="";	/* amplitude input for sufilter		*/
	int nwindow;
	int ftype;
	int endian;
	int vblock;

	int ntr;
	int nsegy=0, format=0;
	int i, idx;


	/* Initialize */
	initargs(argc, argv);
	requestdoc(1);

	if (!getparint("ftype", &ftype)) ftype=0;
	if (!getparint("endian", &endian)) endian=0;
	if (!getparint("nwindow", &nwindow))	nwindow = 100;
	if (!getparint("vblock", &vblock))	vblock = 10;

	/* Get noise type */
	if (!getparstring("noise", &stype))	stype = "gauss";

	/* Recall itype initialized as GAUSS */
	if (STREQ(stype, "flat"))  itype = FLAT;
	else if (!STREQ(stype, "gauss"))
		err("noise=\"%s\", must be gauss or flat", stype);


	/* Get signal to noise ratio */
	if (!getparfloat("sn", &sn))	sn = SN;
	if (sn <= 0) err("sn=%d must be positive", sn);


	/* Set seed */
	if (!getparuint("seed", &seed)) { /* if not supplied, use clock */
		if (-1 == (seed = (unsigned int) time((time_t *) NULL))) {
			err("time() failed to set seed");
		}
	}
	(itype == GAUSS) ? srannor(seed) : sranuni(seed);

	/* Get info from first trace */
	if(ftype==0)
	{
		if (!gettr(&tr)) err("can't get first trace");
		nt = tr.ns;
		ntrc = getNumberOfTraceSuFile(stdin);
		rewind(stdin);
	}
	else
	{
		readEbcdicHeader(stdin, cebcdic); /* read ebcdic header */
		readBinaryHeader(stdin, endian, &bh, &nsegy); /*read binary header */
		ntrc = getNumberOfTraceSegyFile(stdin, nsegy);
		nt = bh.hns;
		format = bh.format;

		//write header
		bh.format = 1;
		writeEbcdicHeader(stdout, cebcdic);
		writeBinaryHeader(stdout, endian, &bh);
		gotoTraceSegyPosition(stdin, 0, nsegy);
	}

	i = nwindow;
	idx = 1;
	while (true)
	{
		if(idx%vblock==0)
			fprintf(stderr, "run trace index %i from %i traces \n", i, ntrc);

		if(i<=ntrc){
			ntr = nwindow;
			add_noise(ftype, ntr, nt, itype, sn, endian, format, nsegy);
		}
		else
		{
			ntr = ntrc - (i-nwindow);
			add_noise(ftype, ntr, nt, itype, sn, endian, format, nsegy);
			break;
		}

		if(ntr==ntrc)
			break;

		i = i+nwindow;
		idx++;
	}

	return(CWP_Exit());
}

void add_noise(int ftype, int ntrc, int nt, int itype, float sn,
		int endian, int format, int nsegy)
{
	float **traces=NULL;
	char **headers=NULL;
	float **noise;		/* noise vector				*/
	float noiscale;		/* scale for noise			*/
	float absmaxsig;	/* absolute maximum in signal		*/
	float noipow;		/* a measure of noise power		*/
	int i, itr;

	/* Loop over input traces & write headers and data to tmp files */
	traces = alloc2float(nt, ntrc);
	headers = alloc2char(HDRBYTES, ntrc);


	/* read all traces and
	 * Compute absmax of signal over entire data set */
	absmaxsig = 0.0;
	for (itr=0; itr<ntrc; itr++)
	{
		if(ftype==0)
			gettr(&tr);
		else
			readTraceSegy(stdin, endian, nt, format, nsegy, &tr);

		memcpy(traces[itr], tr.data, nt*sizeof(float));
		memcpy(headers[itr], (char*)&tr, HDRBYTES*sizeof(char));
		for (i = 0; i < nt; ++i)
			absmaxsig = MAX(absmaxsig, ABS(tr.data[i]));
	}

	/* Compute noise vector elements in [-1, 1] */
	noise = alloc2float(nt, ntrc);
	switch (itype)
	{
	register int i;
	case GAUSS: /* frannor gives elements in N(0,1)--ie. pos & negs */
		for (itr=0; itr<ntrc; itr++)
			for (i = 0; i < nt; ++i)
				noise[itr][i] = frannor();
		break;
	case FLAT: /* franuni gives elements in [0, 1] */
		for (itr=0; itr<ntrc; itr++)
			for (i = 0; i < nt; ++i)
				noise[itr][i] = 2.0*franuni() - 1.0;
		break;
	default:	/* defensive programming */
		err("%d: mysterious itype = %d", __LINE__, itype);
		break;
	}

	/* Compute noise power */
	noipow = 0.0;
	for (itr=0; itr<ntrc; itr++)
		for (i = 0; i < nt; ++i)
			noipow += noise[itr][i] * noise[itr][i];

	/* Compute noise scale for desired noise/signal ratio */
	absmaxsig /= sqrt(2.0);  /* make it look like a rmsq value   */
	noipow /= (nt*ntrc);	 /* make it the square of rmsq value */
	if( absmaxsig != 0.0 )
		noiscale = absmaxsig / (sn * sqrt(noipow));
	else
		noiscale = 1.0;

	/* Add scaled noise to trace and output sum */
	for (itr = 0; itr < ntrc; ++itr)
	{
		memcpy((char*)&tr, headers[itr], HDRBYTES);
		memcpy(tr.data, traces[itr], nt*sizeof(float));
		for (i = 0; i < nt; ++i)
			tr.data[i] += noiscale * noise[itr][i];

		if(ftype==0)
			puttr(&tr);
		else
			writeTraceSegy(stdout, endian, &tr, nsegy, nt);
	}

	free2float(traces);
	free2float(noise);
	free2char(headers);
}
