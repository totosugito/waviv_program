/*
 * wave_freqsmooth.c
 *
 *  Created on: Mar 19, 2012
 *      Author: toto
 */

#include "../src_lib/segy_lib.h"
#include "../src_lib/fftLib.h"
#include "../src_lib/waviv_common.h"
#include <string.h>
#include <stdlib.h>
#include <time.h>

/*********************** self documentation *****************************/
char *sdoc[] = {
		"									",
		" WAVE_FREQSMOOTH = Smoothing Data in Complex",
		"",
		" ftype=1            0=SU file, 1=SEGY file                               	",
		" endian=0           ((0=little endian)",
		" vblock=1000        (show verbose every vblock)",
		" nweight=5          number of weighted input",
		" verbose=0	         =1 for diagnostic print					",
		" sigma=0.0          user defined weighted input",
		"    fweight=0.1,0.2,0.3,0.2,0.1  weighted input",
		" sigma<>0           using gaussian method",
		"",
		" How to Use :",
		" wave_freqsmooth < input.sgy  > output.sgy",
		"",
		NULL};


int main(int argc, char **argv)
{
	int endian, format=0, nsegy;
	int ftype;
	char ebcdic[3200];

	float sigma;
	int ntrc, nsp;
	int nf, nfft;
	float df, dt;
	int i, vblock, verbose;
	int nWeight;
	float *weightValue;

	float *freal=NULL, *fimag=NULL;

	div_t divresult;
	time_t t1,t2;

	/* Initialize */
	initargs(argc, argv);
	requestdoc(1);

	if(!getparint("ftype",&ftype))	ftype = 1;
	if (!getparint("endian", &endian)) endian=0;
	if (!getparint("vblock", &vblock)) vblock=1000;
	if (!getparfloat("sigma", &sigma)) sigma=0.0;
	if (!getparint("verbose", &verbose))	verbose=1;
	if (!getparint("nweight", &nWeight)) nWeight=5;
	if(nWeight%2 != 1)
		err("length of fweight must be odd");

	if(sigma!=0)
		weightValue = gaussiankernel(nWeight, sigma);
	else
	{
		weightValue = ealloc1float(nWeight);
		getparfloat("fweight", weightValue);
	}

	if(verbose)
		for(i=0; i<nWeight; i++)
			fprintf(stderr,"%i. %f\n", i+1, weightValue[i]);

	/* start process */
	t1 = time(NULL);

	/*read header file */
	if(ftype) //segy file
	{
		readEbcdicHeader(stdin, ebcdic);
		readBinaryHeader(stdin, endian, &bh, &nsegy);
		ntrc = getNumberOfTraceSegyFile(stdin, nsegy);

		nsp = bh.hns;
		format = bh.format;
		dt = (float) (bh.hdt/1000000.0);

		bh.format = 1;
		writeEbcdicHeader(stdout, ebcdic);
		writeBinaryHeader(stdout, endian, &bh);
		gotoTraceSegyPosition(stdin, 0, nsegy);
	}
	else
	{
		gettr(&tr);
		nsp = tr.ns;
		dt = (float) (tr.dt/1000000.0);
		ntrc = getNumberOfTraceSuFile(stdin);
		rewind(stdin);
	}
	getNfft(nsp, dt, &nf, &nfft, &df);

	//allocate space
	freal = alloc1float(nf);
	fimag = alloc1float(nf);
	for(i=0; i<ntrc; i++)		//read trace
	{
		if(i%vblock==0)
			fprintf(stderr, "Process Trace [ %i / %i ]\n", i+1, ntrc);

		if(ftype) readTraceSegy(stdin, endian, nsp, format, nsegy, &tr);
		else gettr(&tr);

		//process fft
		fft1_1ff(tr.data, nsp, nf, nfft, freal, fimag);

		//smoothing process
		convolve1d(freal, nf, weightValue, nWeight);
		convolve1d(fimag, nf, weightValue, nWeight);

		//process ifft
		ifft1_1ff(tr.data, nsp, nf, nfft, freal, fimag);

		if(ftype) writeTraceSegy(stdout, endian, &tr, nsegy, nsp);
		else puttr(&tr);
	}

	free1float(freal);
	free1float(fimag);
	t2 = time(NULL);
	divresult = div (t2-t1, 60);
	fprintf (stderr, "Process time = %d min %d sec\n", divresult.quot, divresult.rem);

	return(1);
}
