/*
 * segy_svd.c
 *
 *  Created on: Sep 25, 2012
 *      Author: toto
 */

#include "segy_lib.h"
#include "../src_lib/potashLib.h"
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>


/*********************** self documentation *****************************/
char *sdoc[] = {
		"									",
		" WAVE_SVD = Principal component coherence enhanchement",
		"                                                                       ",
		" wave_svd <infile >outfile  [optional parameters]                 	",
		"                                                                       ",
		" Optional Parameters:                                                  ",
		"				                                	",
		" ftype=0       0=SU file, 1=SEGY file                               	",
		" key=cdp       Key header word to window on (see segy.h)",
		" endian=0      set =0 for little-endian machines(PC's,DEC,etc.)",
		" ntr=          Maximum number of traces in KEY",
		" svditer=30    Maximum number of SVD iterations",
		"",
		" npp=3         percentage of principal components                      ",
		"               used to reconstruct data                        	",
		"               if npp<0 those compnenets are rejected			",
		"               if npp>0 those componenets are kept			",
		"",
		" np=0          Number of principal componenet to reconstruct the data	",
		"               if np is not given npp is used                          ",
		"				                                	",
		" check=0       1 No filtering is done only the first np principal      ",
		"               components are printed with the fldr header word      ",
		"               fldr,p1,p2,..,pnp                                     ",
		"",
		NULL};




int main(int argc, char **argv)
{
	char ebcdic[3200];
	bhed bh;
	int nt, dt, format=0, nsegy, endian;
	cwp_String key;		/* header key word from segy.h		*/
	int maxtrrecords;

	int i, np, svditer, ftype;
	float npp;
	segy tr1;

	float **traces;
	char **headers;

	int check, idxtr, idxRecords, indx, totTr;
	Value *n_val, *o_val;

	div_t divresult;
	time_t t1,t2;

	/* Initialize */
	initargs(argc, argv);
	requestdoc(1);

	/* get general flags and parameters and set defaults */
	if (!getparint("ftype",&ftype)) ftype=0;
	if (!getparint("endian",&endian)) endian=0;
	if (!getparstring("key", &key))		key = "cdp";
	if (!getparint("ntr",&maxtrrecords)) 	err("ntr=??");
	if (!getparint("svditer",&svditer))  svditer = 30;
	if (!getparfloat("npp",&npp))  npp = 3;
	if (!getparint("check",&check)) check=0;
	if (!getparint("np",&np)) np=-9999;

	t1 = time(NULL);

	/*read header file */
	if(ftype) //segy file
	{
		readEbcdicHeader(stdin, ebcdic);
		readBinaryHeader(stdin, endian, &bh, &nsegy);
		totTr = getNumberOfTraceSegyFile(stdin, nsegy);

		nt = bh.hns;
		dt = bh.hdt;
		format = bh.format;
	}
	else
	{
		gettr(&tr1);
		totTr = getNumberOfTraceSuFile(stdin);
		nt = tr1.ns;
		dt = tr1.dt;
	}

	o_val = (Value*) calloc(1, sizeof(Value));
	n_val = (Value*) calloc(1, sizeof(Value));
	indx = getindex(key);

	traces = alloc2float(nt, maxtrrecords);
	headers = alloc2char(HDRBYTES, maxtrrecords);
	idxtr = 0;
	idxRecords = 0;

	/*write header for segy output */
	if(ftype) //segy file
	{
		bh.format = 1;
		writeEbcdicHeader(stdout, ebcdic);
		writeBinaryHeader(stdout, endian, &bh);

		readTraceSegy(stdin, endian, nt, format, nsegy, &tr1); //read the first trace
	}
	else
		gettr(&tr1);

	gethval(&tr1, indx, o_val);
	memcpy(traces[idxtr], tr1.data, nt*sizeof(float));
	memcpy(headers[idxtr], (char*)&tr1, HDRBYTES*sizeof(char));

	for(i=1; i<totTr; i++)
	{
		if(ftype) //segy file
			readTraceSegy(stdin, endian, nt, format, nsegy, &tr1);
		else
			gettr(&tr1);

		gethval(&tr1, indx, n_val);
		if(o_val->i==n_val->i)
		{
			idxtr++;

			if(idxtr>=maxtrrecords)
				err("trace in records [%i] >= ntr[%i] trace=%i", idxtr, maxtrrecords, i);

			memcpy(traces[idxtr], tr1.data, nt*sizeof(float));
			memcpy(headers[idxtr], (char*)&tr1, HDRBYTES*sizeof(char));
		}
		else
		{
			fprintf(stderr, "Run Records %i = %i traces, tr=%i/%i \n", idxRecords+1, idxtr+1, i, totTr);
			runSvdComputation(traces, idxtr+1, nt, npp, np, check, svditer);

			if(ftype) //segy file
				writeSegyTraceData(stdout, traces, headers, idxtr+1, nt, endian, nsegy);
			else
				writeSuTraceData(stdout, traces, headers, idxtr+1, nt);

			//update old variable
			idxRecords++;
			idxtr = 0;
			o_val->i = n_val->i;
			memcpy(traces[idxtr], tr1.data, nt*sizeof(float));
			memcpy(headers[idxtr], (char*)&tr1, HDRBYTES*sizeof(char));
		}
	}

	//process the last records
	fprintf(stderr, "Run Records %i = %i traces, tr=%i/%i \n", idxRecords+1, idxtr+1, i, totTr);
	runSvdComputation(traces, idxtr+1, nt, npp, np, check, svditer);
	if(ftype) //segy file
		writeSegyTraceData(stdout, traces, headers, idxtr+1, nt, endian, nsegy);
	else
		writeSuTraceData(stdout, traces, headers, idxtr+1, nt);

	free2float(traces);
	free2char(headers);
	free(o_val);
	free(n_val);

	t2 = time(NULL);
	divresult = div (t2-t1, 60);
	fprintf (stderr, "Process time = %d min %d sec\n", divresult.quot, divresult.rem);

	return(1);
}




