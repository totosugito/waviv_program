/*
 * wave_split.c
 *
 *  Created on: Oct 8, 2012
 *      Author: toto
 */

/* Copyright (c) Colorado School of Mines, 2010.*/
/* All rights reserved.                       */

/* SUSPLIT */
#include <sys/stat.h>
#include "../src_lib/segy_lib.h"

/*********************** self documentation ******************************/
char *sdoc[] = {
"									",
" WAVE_SPLIT - Split traces into different output files by keyword value	",
"									",
"     wave_split <stdin >stdout [options]					",
"									",
" Required Parameters:							",
"	none								",
"									",
" Optional Parameters:							",
"   ftype=0     0=SU file, 1=SEGY file                               	",
"   endian=0    set =0 for little-endian machines(PC's,DEC,etc.)",
"   key=cdp     Key header word to split on (see segy.h)	",
"   stem=split_ Stem name for output files			",
"   middle=key  middle of name of output files			",
"   suffix=.su  Suffix for output files				",
"   numlength=7 Length of numeric part of filename		",
"   verbose=0   =1 to echo filenames, etc.			",
"   close=1     =1 to close files before opening new ones	",
"									",
" Notes:								",
" The most efficient way to use this program is to presort the input data",
" into common keyword gathers, prior to using susplit.			"
"									",
" Use \"suputgthr\" to put SU data into SU data directory format.	",
NULL};

/* Credits:
 *	Geocon: Garry Perratt hacked together from various other codes
 *
 */
/**************** end self doc *******************************************/

bool FileExists(const char* filename);
int main(int argc, char **argv)
{
	cwp_String key;		/* header key word from segy.h		*/
	int ns;			/* ns as an  int			*/
	int numlength;		/* length of split key number format	*/
	char format[BUFSIZ];	/* output filename format		*/

	int index;		/* index of key				*/
	Value val;		/* value of key				*/
	int val_i=0;		/* key value as integer 		*/
	int lastval_i=0;	/* last key value as integer	 	*/

	cwp_String type;	/* key's type				*/
	char filename[BUFSIZ];	/* output file name			*/

	cwp_String stem;	/* output file stem			*/
	cwp_String middle;	/* output file middle			*/
	cwp_String suffix;	/* output file suffix			*/
	FILE *outfp=NULL;	/* pointer to output file		*/

	int init;		/* initialisation flag for first efopen	*/
	int verbose;		/* =1 to echo filenames, etc.		*/
	int close;	/* =1 to close files before opening a new one	*/

	int ftype, i, endian, nsegy, iformat=0;
	int result=0;
	int ntrc=0;
	bool isexist;
	char ebcdic[3200], buff[3600];

	/* Initialize */
	initargs(argc, argv);
	requestdoc(1);
	init=0;

	/* Default parameters;  User-defined overrides */
	if (!getparint("ftype",&ftype)) ftype=0;
	if (!getparint("endian",&endian)) 	endian = 0;

	if (!getparstring("key", &key))	 key="cdp";
	if (!getparstring("stem", &stem)) stem="split_";
	if (!getparstring("middle", &middle)) middle=key;
	if (!getparstring("suffix", &suffix)) suffix=".su";
	if (!getparint("numlength", &numlength)) numlength=7;
	if (!getparint("verbose",&verbose)) verbose=0;
	if (!getparint("close",&close)) close=1;

	/* Evaluate time bounds from getpars and first header */
	if(ftype)
	{
		readEbcdicHeader(stdin, ebcdic);
		readBinaryHeader(stdin, endian, &bh, &nsegy);
		ntrc = getNumberOfTraceSegyFile(stdin, nsegy);
		ns = bh.hns;
		iformat = bh.format;

		rewind(stdin);
		result = fread(buff, 1, EBCBYTES+BNYBYTES, stdin);
		if(result!=EBCBYTES+BNYBYTES)
			err("error reading segy header");
		readTraceSegy(stdin, endian, ns, iformat, nsegy, &tr);
	}
	else {
		ntrc = getNumberOfTraceSuFile(stdin);
		if (!gettr(&tr)) err("can't get first trace");
	}

	ns = tr.ns;

	type = hdtype(key);
	index = getindex(key);

	if (verbose) warn("key is %s",key);

	/* Main loop over traces */
	for (i=1; i<ntrc+1; i++)
	{
		gethval(&tr, index, &val);
		val_i = vtoi(type, val);

		if (val_i!=lastval_i || init==0)
		{
			if (init==0)
				init=1;
			else if (close)
				efclose(outfp);

			(void)sprintf(format, "%%s%%s%%0%dd%%s",numlength);
			(void)sprintf(filename, format, stem, middle, val_i, suffix);

			if (verbose)
				warn("output file is %s",filename);

			isexist = FileExists(filename);
			if(ftype && !isexist)
			{
				outfp = efopen(filename, "w");
				fwrite(buff, 1, EBCBYTES+BNYBYTES, outfp); //write segy header
			}
			else
				outfp = efopen(filename, "ab");
		}

		if(ftype)
		{
			writeTraceSegy(outfp, endian, &tr, nsegy, ns);
			readTraceSegy(stdin, endian, ns, iformat, nsegy, &tr);
		}
		else
		{
			fputtr(outfp,&tr);
			gettr(&tr);
		}

		lastval_i = val_i;
	}

	return(CWP_Exit());
}

bool FileExists(const char* filename)
{
	struct stat info;
	int ret = -1;

	//get the file attributes
	ret = stat(filename, &info);
	if(ret == 0)
	{
		//stat() is able to get the file attributes,
		//so the file obviously exists
		return true;
	}
	else
	{
		//stat() is not able to get the file attributes,
		//so the file obviously does not exist or
		//more capabilities is required
		return false;
	}
}

