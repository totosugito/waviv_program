/*
 * wave_nan.c
 *
 *  Created on: Mar 6, 2013
 *      Author: toto
 */




/* Copyright (c) Colorado School of Mines, 2010.*/
/* All rights reserved.                       */

/* SUNAN: $Revision: 1.5 $ ; $Date: 2010/01/25 22:52:50 $        */

#include <math.h>
#include <time.h>
#include "../src_lib/segy_lib.h"

#ifdef SUN
#include <ieeefp.h>
#endif

/*********************** self documentation **********************/
char *sdoc[]={
"								",
" WAVE_NAN - remove NaNs & Infs from the input stream		",
"								",
"    wave_nan < inp.sgy ftype=1 >out.sgy					",
"								",
" Optional parameters:						",
" ftype=1        0=SU file, 1=SEGY file                               	",
" endian=0       set =0 for little-endian machines(PC's,DEC,etc.)",
" vblock=1000    show verbose",
" value=0.0	     NaNs and Inf replacement value			",
" ... and/or....						",
" interp=0	=1 replace NaNs and Infs by interpolating	",
"                   neighboring finite values			",
"								",
" Notes:							",
" A simple program to remove NaNs and Infs from an input stream.",
" The program sets NaNs and Infs to \"value\" if interp=0. When	",
" interp=1 NaNs are replaced with the average of neighboring values",
" provided that the neighboring values are finite, otherwise	",
" NaNs and Infs are replaced by \"value\".			",
NULL};

/*
 * Author: Reginald H. Beardsley  2003   rhb@acm.org
 *
 *  A simple program to remove NaNs & Infs from an input stream. They
 *  shouldn't be there, but it can be hard to find the cause and fix
 *  the problem if you can't look at the data.
 *
 *  Interpolation idea comes from a version of sunan modified by
 *  Balasz Nemeth while at Potash Corporation in Saskatchewan.
 *
 */

/**************** end self doc ********************************/

segy tr;

int
main(int argc, char **argv)
{

	int i;			/* counter			*/
	int itr=0;		/* trace counter		*/
	int vblock;		/* =0 silent,  =1 chatty	*/
	int interp;		/* =1 interpolate to get NaN	*/
				/* and Inf replacement values	*/

	float value;		/* value to set NaN and Infs to */
	int ftype;
	int endian;
	div_t divresult;
	time_t t1,t2;
	char ebcdic[3200];
	int nsegy, ntrc, nsp, format=1;

	/* Initialize */
   	initargs(argc,argv);
   	requestdoc(1);

	/* Get info from first trace */
	if(!gettr(&tr) ) err("Can't get first trace \n");

	/* Get parameters */
	if(!getparint("ftype",&ftype))	ftype = 1;
	if(!getparint("endian",&endian))	endian = 0;
	if(!getparint("vblock",&vblock))	vblock = 1000;
	if(!getparint("interp",&interp))	interp = 0;
	if(!getparfloat("value",&value))	value = 0.0;

	t1 = time(NULL);

	/*read header file */
	if(ftype) //segy file
	{
		readEbcdicHeader(stdin, ebcdic);
		readBinaryHeader(stdin, endian, &bh, &nsegy);
		ntrc = getNumberOfTraceSegyFile(stdin, nsegy);

		nsp = bh.hns;
		format = bh.format;

		bh.format = 1;
		writeEbcdicHeader(stdout, ebcdic);
		writeBinaryHeader(stdout, endian, &bh);
		gotoTraceSegyPosition(stdin, 0, nsegy);
	}
	else
	{
		gettr(&tr);
		nsp = tr.ns;
		ntrc = getNumberOfTraceSuFile(stdin);
		rewind(stdin);
	}

	/* Loop over traces */
	for(itr=0; itr<ntrc; itr++)
	{
		if(ftype) readTraceSegy(stdin, endian, nsp, format, nsegy, &tr);
		else gettr(&tr);

		for(i=0; i<tr.ns; ++i)
		{
			if(!finite(tr.data[i]))
			{
				if (interp)
				{ /* interpolate nearest neighbors */
					/* for NaN replacement value     */
					if (i==0 && finite(tr.data[i+1]))
						tr.data[i]=tr.data[i+1];
					else if(i==tr.ns-1 && finite(tr.data[i-2]))
						tr.data[i]= tr.data[i-2];
					else if( finite(tr.data[i-1]) &&
							finite(tr.data[i+1]) )
						tr.data[i]=(tr.data[i-1]+tr.data[i+1])/2.0;
				}
				/* use user defined NaNs replacement value */
				else
					tr.data[i] = value;
			}
		}

		if(ftype) writeTraceSegy(stdout, endian, &tr, nsegy, nsp);
		else puttr(&tr);
	}
	t2 = time(NULL);
	divresult = div (t2-t1, 60);
	fprintf (stderr, "Process time = %d min %d sec\n", divresult.quot, divresult.rem);
	return(CWP_Exit());
}
