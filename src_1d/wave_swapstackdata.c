/*
 * wave_swapstackdata.c
 *
 *  Created on: Jan 11, 2012
 *      Author: toto
 */

#include "segy_lib.h"
#include <sys/types.h>
#include <stdbool.h>
#include "wave_3dlib.h"

char *sdoc[] = {
		"									",
		" WAVE_SWAPSTACKDATA = SWAP STACKING DATA",
		"",
		" inp1=         (input segy file 1)",
		" inp2=         (input segy file 2)",
		" out=          (output segy file. change data from inp1 with inp2)",
		" inline=       (inline byte position)",
		" xline=        (xline byte position)",
		"",
		" Sample Command :",
		" wave_swapstackdata inp1= inp2= out= inline= xline=",
		NULL};

int main (int argc, char **argv)
{
	int verbose;
	char *cinp1, *cinp2, *ccout;
	int iinline, ixline;
	int endian;
	FILE *finp1=NULL, *finp2=NULL, *fout=NULL;

	bhed bh1, bh2;
	//segy trace1, trace2;
	char cebcid1[3200], cebcid2[3200];
	int nsegy1, nsegy2, ntrc1, ntrc2;
	int nsp1, nsp2, nsp;
	int itr1, itr2, idx2;
	int inline1, inline2, xline1, xline2;
	char *buff_header1=NULL, *buff_header2=NULL, *buff_data1=NULL, *buff_data2=NULL;
	int nheader, ndata;
	size_t result;

	/* hook up getpar to handle the parameters */
	initargs(argc,argv);
	requestdoc(1);

	MUSTGETPARSTRING("inp1",  &cinp1);
	MUSTGETPARSTRING("inp2",  &cinp2);
	MUSTGETPARSTRING("out",  &ccout);
	if (!getparint("inline",&iinline)) 	err("inline=??");  //byte inline position
	if (!getparint("xline",&ixline)) 	err("xline=??");  //byte xline position
	if (!getparint("verbose",&verbose)) 	verbose = 1;  //show verbose
	if (!getparint("endian",&endian)) 	endian = 0;

	finp1 = fopen (cinp1,"r");
	if (finp1==NULL) err ("Error opening file 1 : %s\n", cinp1);

	finp2 = fopen (cinp2,"r");
	if (finp2==NULL) err ("Error opening file 2 : %s\n", cinp2);

	fout = fopen (ccout,"w");
	if (fout==NULL) err ("Error opening output file : %s\n", ccout);

	/* READ SEGY INFO */
	//read file 1 header information
	readEbcdicHeader(finp1, cebcid1); /* read ebcdic header */
	readBinaryHeader(finp1, endian, &bh1, &nsegy1); /*read binary header */
	ntrc1 = getNumberOfTraceSegyFile(finp1, nsegy1);

	//read file 2 header information
	readEbcdicHeader(finp2, cebcid2); /* read ebcdic header */
	readBinaryHeader(finp2, endian, &bh2, &nsegy2); /*read binary header */
	ntrc2 = getNumberOfTraceSegyFile(finp2, nsegy2);

	if( bh1.hns != bh2.hns) err("input ns is not equal. ns1=%i ns2=%i", bh1.hns, bh2.hns);
	if( bh1.hdt != bh2.hdt) err("input dt is not equal. dt1=%i dt2=%i", bh1.hdt, bh2.hdt);

	nsp1 = bh1.hns;
	nsp2 = bh2.hns;
	if(nsp1!=nsp2) err("ns1 <> ns2 ");
	nsp = nsp1;

	/*write output header */
	bh1.format = 1;
	writeEbcdicHeader(fout, cebcid1);
	writeBinaryHeader(fout, endian, &bh1);

	//create buffer header and data
	nheader = 240;
	ndata = nsegy2 - nheader;
	buff_header1 = (char*) calloc(nheader, sizeof(char));
	buff_data1 = (char*) calloc(ndata, sizeof(char));
	buff_header2 = (char*) calloc(nheader, sizeof(char));
	buff_data2 = (char*) calloc(ndata, sizeof(char));

	idx2 = 0;
	itr2 = 0;
	for(itr1=0; itr1<ntrc1; itr1++)
	{
		inline1 = computeIntHeaderAtByte(finp1, iinline, 4, endian, 0); //get inline file 1
		xline1 = computeIntHeaderAtByte(finp1, ixline, 4, endian, 0); //get xline file 1

		for(itr2=idx2; itr2<ntrc2; itr2++)
		{
			inline2 = computeIntHeaderAtByte(finp2, iinline, 4, endian, 0); //get inline file 2
			xline2 = computeIntHeaderAtByte(finp2, ixline, 4, endian, 0); //get xline file 2

			//compare inline file 1 and 2 WITH xline file 1 and 2
			if((inline1==inline2) && (xline1==xline2)) //if equal, go to trace data and save data
			{
				if(verbose)
					printf("write inline %i xline=%i \n", inline1, xline1);

				result = fread(buff_header1, sizeof(char), nheader, finp1); //read header file 1
				result = fread(buff_data1, sizeof(char), ndata, finp1); //read data file 1

				result = fread(buff_header2, sizeof(char), nheader, finp2); //read header file 2
				result = fread(buff_data2, sizeof(char), ndata, finp2); //read data file 2

				result = fwrite(buff_header1, sizeof(char), nheader, fout); //write header file 1 to output
				result = fwrite(buff_data2, sizeof(char), ndata, fout); //write data file 2 to output
				idx2++;
				break;
			}
			else //if not equal --> file 2 go to the next trace
			{
				nextTrace(finp2, nsegy2);
				idx2++;
			}


			//if inline1 less than inline2, show error
			if(inline1<inline2)
			{
				fprintf(stderr, "can not find inline %i xline %i in file 1 \n", inline1, xline1);
				fprintf(stderr, "        read inline %i xline %i in file 2 \n", inline2, xline2);
				exit(0);
			}
		}

		if(itr2==ntrc2) break;	//exit if file 2 read end of trace

		//file 1--> go to the next trace
		//nextTrace(finp1, nsegy1);
	}

	printf("Process %i (file 1)  -->  %i (file 2) traces\n", itr1, itr2);
	free(buff_header1);
	free(buff_header2);
	free(buff_data1);
	free(buff_data2);

	fclose(finp1);
	fclose(finp2);
	fclose(fout);
	return (1);
}
