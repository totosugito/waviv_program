/*
 * wave_epsf.c
 *
 *  Created on: Nov 19, 2012
 *      Author: toto
 */

#include "segy_lib.h"
#include "boundary.h"
#include "mean.h"

/*********************** self documentation **********************/
char *sdoc[] = {
		" 									",
		" wave_epsf	",
		" ftype=0       0=SU file, 1=SEGY file                               	",
		" endian=0      set =0 for little-endian machines(PC's,DEC,etc.)",
		" nfw1=10       filter-window length in n1 direction (positive and odd integer)",
		" nfw2=10       filter-window length in n2 direction (default=1, 1D case)",
		" boundary=0    if=1, boundary is data, whereas zero",
		" type=0        0=stack, 1=mean",
        "     if type=0",
		"       weight=0     Gaussian weight flag (only for stack)",
        "       sigma=3.0    Gaussian weight radius (only for stack)",
		" vblock=100    show verbose every ... block",
		"",
		NULL};

int nsp;
int ntrc;
int endian;
int ftype;
int format;
int nsegy;
int vblock;

int nfw1; /* filter-window length in n1 direction (positive and odd integer) */
int nfw2; /* filter-window length in n2 direction (default=1, 1D case)*/
int boundary; /* if y, boundary is data, whereas zero*/
int type; /* [0=stack, 1=median] filter choice, the default is stack (mean) */
int weight; /* Gaussian weight flag (only for stack) */
float sigma; /* Gaussian weight radius (only for stack) */

void run_epsf(float *trace, int m1, int m2);
int main(int argc, char **argv)
{
	char ebcdic[3200];
	int i, ipos;
	int m1, m2;
	float *trace;
	char **header;
	div_t divresult;
	time_t t1,t2;

	/* Hook up getpar to handle the parameters */
	initargs(argc,argv);
	requestdoc(1);

	if (!getparint("ftype",&ftype)) ftype=0;
	if (!getparint("endian",&endian)) endian=0;
	if (!getparint("nfw1",&nfw1)) nfw1=10;
	if (!getparint("nfw2",&nfw2)) nfw2=10;
	if (!getparint("boundary",&boundary)) boundary=0;
	if (!getparint("type",&type)) type=0;
	if (!getparint("vblock",&vblock)) vblock=100;

	if (nfw1%2 == 0)  nfw1 = (nfw1+1);
	if (nfw2%2 == 0)  nfw2 = (nfw2+1);
	m1 = nfw1-1;
	m2 = nfw2-1;

	if(type==0){
		if (!getparint("weight",&weight)) weight=0;
		if (!getparfloat("sigma",&sigma)) sigma=3.0;
	}

	t1 = time(NULL);

	/*read header file */
	if(ftype) //segy file
	{
		readEbcdicHeader(stdin, ebcdic);
		readBinaryHeader(stdin, endian, &bh, &nsegy);
		ntrc = getNumberOfTraceSegyFile(stdin, nsegy);

		nsp = bh.hns;
		format = bh.format;

		bh.format = 1;
		writeEbcdicHeader(stdout, ebcdic);
		writeBinaryHeader(stdout, endian, &bh);
		gotoTraceSegyPosition(stdin, 0, nsegy);
	}
	else
	{
		gettr(&tr);
		nsp = tr.ns;
		ntrc = getNumberOfTraceSuFile(stdin);
		rewind(stdin);
	}

	fprintf(stderr, "    SHOW LOG    \n");
	fprintf(stderr, "      nsp = %i \n", nsp);
	fprintf(stderr, "     ntrc = %i \n", ntrc);
	fprintf(stderr, "     nfw1 = %i \n", nfw1);
	fprintf(stderr, "     nfw2 = %i \n", nfw2);
	fprintf(stderr, " boundary = %i \n", boundary);
	fprintf(stderr, "     type = %i \n", type);
	fprintf(stderr, "   weight = %i \n", weight);
	fprintf(stderr, "    sigma = %f \n\n", sigma);

	trace = alloc1float(nsp*ntrc);
	header = alloc2char(SEGY_HDRBYTES, ntrc);

	//read trace data and header
	for (i=0; i<ntrc; i++) {
		ipos = nsp*i;
		if(ftype==1) readTraceSegy(stdin, endian, nsp, format, nsegy, &tr);
		else gettr(&tr);

		memcpy(trace+ipos, tr.data, nsp*sizeof(float));
		memcpy(header[i], (char*)&tr, HDRBYTES*sizeof(char));
	}

	run_epsf(trace, m1, m2);

	//read trace data and header
	for (i=0; i<ntrc; i++) {
		ipos = nsp*i;

		memcpy((char*)&tr, header[i], HDRBYTES);
		memcpy(tr.data, trace+ipos, nsp*sizeof(float));

		if(ftype==1) writeTraceSegy(stdout, endian, &tr, nsegy, nsp);
		else puttr(&tr);
	}

	free2char(header);
	free1float(trace);
	t2 = time(NULL);
	divresult = div (t2-t1, 60);
	fprintf (stderr, "Process time = %d min %d sec\n", divresult.quot, divresult.rem);
	return(1);
}

void run_epsf(float *trace, int m1, int m2)
{
	int i, j, p, pp, k, kk;
	int index;
	int n1, n2;
	float *tempt, *temp1, *extendt, *sdev, *result, *w, *nindex;
	float tindex;

	n1 = nsp;
	n2 = ntrc;

	tempt = alloc1float(nsp*ntrc);
	extendt = alloc1float((nsp+2*m1)*(ntrc+2*m2));

	temp1 = alloc1float(nfw1*nfw2);
	w = alloc1float(nfw1*nfw2);
	sdev = alloc1float(nfw1*nfw2);
	result = alloc1float(nfw1*nfw2);
	nindex = alloc1float(nfw1*nfw2);

	memcpy(tempt, trace, n1*n2*sizeof(float));
	bound3(tempt,extendt,2*nfw1-1,2*nfw2-1,n1,n2,boundary);

	/************edge-preserving smoothing filter****************/
	fprintf(stderr, "smoothing filter \n");
	for(i=0;i<n2;i++)
	{
		if(i%vblock==0)
			fprintf(stderr, "Process trace %i / %i \n", i+1, n2);
		for(j=0;j<n1;j++)
		{
			//				fprintf(stderr, "n2=%i Process time(n1) %i / %i \n", i+1, j+1, n1);
			for(p=0;p<nfw1*nfw2;p++)
				nindex[p]=p;

			for(p=0;p<nfw2;p++)
			{
				for(pp=0;pp<nfw1;pp++)
				{
					for(k=0;k<nfw2;k++)
					{
						for(kk=0;kk<nfw1;kk++)
						{
							temp1[k*nfw1+kk]=extendt[(n1+2*m1)*(i+p+k)+j+pp+kk];
							if (weight)
								w[k*nfw1+kk] = exp(-1*((p+k-m2)*(p+k-m2)+(pp+kk-m1)*(pp+kk-m1))/(2*sigma*sigma+FLT_EPSILON));
						}
					}

					sdev[p*nfw1+pp]=sdeviation(temp1,nfw1*nfw2);
					if(type==0)
					{
						if (weight)
							for (k=0;k<nfw1*nfw2;k++)
								temp1[k] = temp1[k]*w[k];
						result[p*nfw1+pp]=mean(temp1,nfw1*nfw2);
					}
					else if(type==1)
						result[p*nfw1+pp]=medianfilter(temp1,nfw1*nfw2);
					else
						err("Unknown method type=%i",type);
				}
			}

			index=0;
			for(p=0;p<nfw1*nfw2-1;p++)
			{
				if(sdev[p]<sdev[p+1])
				{
					tindex=sdev[p];
					sdev[p]=sdev[p+1];
					sdev[p+1]=tindex;
					index=nindex[p];
					nindex[p]=nindex[p+1];
					nindex[p+1]=index;
				}
			}
			index=nindex[nfw1*nfw2-1];
			trace[n1*i+j]=result[index];
		}
	}

	free1float(tempt);
	free1float(extendt);
	free1float(temp1);
	free1float(w);
	free1float(sdev);
	free1float(result);
	free1float(nindex);

}


