/*
 * wave_eigenfilter.c
 *
 *  Created on: Apr 13, 2012
 *      Author: toto
 */

/*
 * segy_calc.c
 *
 *  Created on: Feb 7, 2012
 *      Author: toto
 */
#include "../src_lib/windowingLib.h"
#include "../src_lib/printLib.h"
#include "../src_lib/filterLib.h"
#include "../src_lib/shiftingLib.h"
#include "../src_lib/iolibrary.h"
#include "segy_lib.h"
#include <time.h>

char *sdoc[] = {
		"									",
		" WAVE_EIGENFILTER.V1.0 --- EIGEN FILTER		",
		" ",
		" rank=1              (rank data)",
		" window=0            (1=windowing data)",
		"                     if window=1",
		"        overlap=0.5  (overlap data)",
		"        dnsp=32      (width of nsp window)",
		"        dntrc=20     (width of ntrc window)",
		"        nshifting=10 (length of checking dip steering data)",
		"",
		" How To Use :",
		" wave_eigenfilter.v1.0 rank=1 window=0 <input.sgy > output.sgy",
		" wave_eigenfilter.v1.0 rank=1 window=1 overlap=0.5 dnsp=32 dntrc=20 nshifting=10 <input.sgy > output.sgy",
		"",
		" Algorithm :",
		" 1. load data",
		" 2. Windowing Data",
		" 3. Dip steering",
		" 4. Eigen Filter",
		" 5. Invers Dip steering",
		" 6. Save Data",
		"",
		NULL};

void processWindowingData(float **dataOri, int rank, int ntrc, int ns,
		float overlap,
		int nshifting, int nsWindowLength, int ntrcWindowLength);
int main (int argc, char **argv)
{
	char cebcdic[3200];
	int ns, ntrc;
	int i,j;
	int rank;
	int iswindowing;
	float overlap;
	int nsWindowLength, ntrcWindowLength;
	int nshifting;
	float **dataOri=NULL;
	int format, nsegy, endian;
	float dt;
	div_t divresult;
	time_t t1,t2;


	initargs(argc,argv);
	requestdoc(1);

	if (!getparint("window", &iswindowing)) iswindowing=0;
	if (!getparfloat("overlap", &overlap)) overlap=0.5;
	if (!getparint("dnsp", &nsWindowLength)) nsWindowLength=32;
	if (!getparint("dntrc", &ntrcWindowLength)) ntrcWindowLength=20;
	if (!getparint("rank", &rank)) rank=1;
	if (!getparint("nshift", &nshifting)) nshifting=10;
	if (!getparint("endian", &endian)) endian=0;

	t1 = time(NULL);
	readEbcdicHeader(stdin, cebcdic); /* read ebcdic header */
	readBinaryHeader(stdin, endian, &bh, &nsegy); /*read binary header */
	ntrc = getNumberOfTraceSegyFile(stdin, nsegy);
	ns = bh.hns;
	format = bh.format;
	dt = (float) (bh.hdt / 1e+6);

	bh.format = 1;
	writeEbcdicHeader(stdout, cebcdic);
	writeBinaryHeader(stdout, endian, &bh);

	//read data
	dataOri = alloc2float(ns, ntrc);
	for (i=0; i<ntrc; i++)
	{
		readTraceSegy(stdin, endian, ns, format, nsegy, &tr);
		for (j=0; j<ns; j++)
			dataOri[i][j] = tr.data[j];
	}

	if(iswindowing>0)
		processWindowingData(dataOri, rank, ntrc, ns, overlap,
				nshifting, nsWindowLength, ntrcWindowLength);
	else
		rankApproximationf(dataOri, ntrc, ns, rank);

	gotoTraceSegyPosition(stdin, 0, nsegy);
	for(i=0; i<ntrc; i++)
	{
		readTraceSegy(stdin, endian, ns, format, nsegy, &tr);
		for(j=0; j<ns; j++)
			tr.data[j] = dataOri[i][j];

		writeTraceSegy(stdout, endian, &tr, nsegy, ns);
	}

	free2float(dataOri);

	t2 = time(NULL);
	divresult = div (t2-t1, 60);
	fprintf (stderr, "Process time = %d min %d sec\n", divresult.quot, divresult.rem);
	return(1);
}

void processWindowingData(float **dataOri, int rank, int ntrc, int ns,
		float overlap,
		int nshifting, int nsWindowLength, int ntrcWindowLength)
{
	int i,j;
	int ins0, ins1, ins2, dins;
	int intrc0, intrc1, intrc2, dintrc;
	int **nsWindowPos=NULL, **ntrcWindowPos=NULL;
	int nsWindowWidth, ntrcWindowWidth;
	int nsMaxOverlap, ntrcMaxOverlap;
	int midshifting, *shiftingPos=NULL;

	float **dataProcess=NULL;
	float **tmpExtractData0, **tmpExtractData1, **tmpExtractData3;

	if(nshifting%2>0)	//compute half of shifting data.
		nshifting = nshifting+1;
	midshifting = nshifting/2;
	nsMaxOverlap = (int) (nsWindowLength * overlap);
	ntrcMaxOverlap = (int) (ntrcWindowLength * overlap);

	nsWindowPos = getWindowingLength(ns, nsWindowLength, overlap, &nsWindowWidth);
	ntrcWindowPos = getWindowingLength(ntrc, ntrcWindowLength, overlap, &ntrcWindowWidth);

	dataProcess = alloc2float(ns, ntrc);
	shiftingPos = alloc1int(ntrcWindowLength);
	for (i=0; i<nsWindowWidth; i++)
	{
		ins0 = nsWindowPos[i][0];
		ins1 = nsWindowPos[i][1];
		ins2 = nsWindowPos[i][2];
		dins = ins1-ins0+1;

		if(i%30==0)
			fprintf(stderr, "Loop [ %i / %i ] \n", i+1, nsWindowWidth);
		for(j=0; j<ntrcWindowWidth; j++)
		{
			intrc0 = ntrcWindowPos[j][0];
			intrc1 = ntrcWindowPos[j][1];
			intrc2 = ntrcWindowPos[j][2];
			dintrc = intrc1-intrc0+1;

			tmpExtractData0 = extraceTraceData(dataOri, ins0, ins1, dins,
					intrc0, intrc1, dintrc);

			if(dins!=nsWindowLength || dintrc!=ntrcWindowLength)	//update data in the last windowing
				tmpExtractData0 = duplicateLastData(tmpExtractData0, dintrc, dins, ntrcWindowLength, nsWindowLength);

			tmpExtractData1 = runDipSteer(tmpExtractData0, ntrcWindowLength, nsWindowLength,
					nshifting, midshifting, shiftingPos); //process dipsteer

			rankApproximationf(tmpExtractData1, ntrcWindowLength, nsWindowLength, rank); //eigen filter

			tmpExtractData3 = returnDipSteer(tmpExtractData0, tmpExtractData1,
					ntrcWindowLength, nsWindowLength, shiftingPos, nshifting, midshifting);

			free2float(tmpExtractData1);

			updateDataFromWindowing(dataProcess, tmpExtractData3, ins0, ins2, intrc0, intrc2);

			free2float(tmpExtractData0);
			free2float(tmpExtractData3);
		}
	}
	memcpy(dataOri[0], dataProcess[0], ns*ntrc*sizeof(float));

	free2int(nsWindowPos);
	free2int(ntrcWindowPos);
	free1int(shiftingPos);
	free2float(dataProcess);
}
