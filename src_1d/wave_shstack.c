/*
 * wave_shstack.c
 *
 *  Created on: Sep 24, 2012
 *      Author: toto
 */
#include "segy_lib.h"
#include "../src_lib/potashLib.h"

char *sdoc[] = {
		"									",
		" WAVE_SHSTACK.v1	: Gather stacking with an optional static shift correction	",
		"",
		" ftype=0       0=SU file, 1=SEGY file                               	",
		" endian=0      set =0 for little-endian machines(PC's,DEC,etc.)",
		" tmin=0.0      Start Time Shifting Position (secon)",
		" tmax=0.0      End Time Shifting Position (secon), 0.0=nsp",
		" stmin=0.0     Start Time Shifting SOURCE Position (secon)",
		" stmax=0.0     End Time Shifting SOURCE Position (secon), 0.0=nsp",
		" stk=1         (0=Do not stack the records)",
		" shfm=.015     (maximum allowed static shift)                            ",
		" key=fldr      (segy keyword that marks the records)                     ",
		" nrecords=     (maximum number of records)",
		" trrecords=    (maximum number of trace in records)",
		" verbose=0     (show debug)",
		"",
		" How to use : ",
		" wave_shstack.v1 shfm=0.15 stk=1 key=offset nrecords=100 trrecords=802 <input.su >output.su",
		NULL};

int *getGatherGroupInfo(int totTr, int ntrc, int nsp, cwp_String *key, int nrecords,
		int ishfm, int verbose, int vblock, int *trRecords, int *nRecords,
		int ftype, int nsegy, int format, int endian, int stmin, int stmax);

void runShStack(int *rlag, int nsp, int trRecords, int nRecords, int stk,
		int verbose, int vblock, int ftype, int nsegy, int format, int endian,
		int tmin, int tmax, float fmin, float fmax);

int main (int argc, char **argv)
{
	char ebcdic[3200];
	bhed bh;
	int format=1, nsegy, totTr, endian;

	cwp_String key;		/* header key word from segy.h		*/
	int maxtrrecords;
	float shfm;
	int stk, verbose, vblock;
	int ns, ishfm, nrecords;
	float dt;
	int nRecords, trRecords;
	int *rlag, ftype;
	div_t divresult;
	time_t t1,t2;

	float tmin, tmax, stmin, stmax;
	int TMin, TMax, sTMin, sTMax;

	/* hook up getpar to handle the parameters */
	initargs(argc,argv);
	requestdoc(1);

	if (!getparint("ftype",&ftype)) ftype=0;
	if (!getparint("endian",&endian)) endian=0;
	if (!getparstring("key", &key))		key = "offset";
	if (!getparint("stk", &stk))		stk = 1;
	if (!getparfloat("shfm", &shfm))	shfm = 0.015;
	if (!getparint("trrecords",&maxtrrecords)) 	maxtrrecords = 802;
	if (!getparint("nrecords", &nrecords)) nrecords = 100;
	if (!getparint("verbose", &verbose))	verbose = 1;
	if (!getparint("vblock", &vblock))	vblock = 500;

	if (!getparfloat("tmin", &tmin))	tmin = 0.0;
	if (!getparfloat("tmax", &tmax))	tmax = 0.0;
	if (!getparfloat("stmin", &stmin))	stmin = 0.0;
	if (!getparfloat("stmax", &stmax))	stmax = 0.0;

	t1 = time(NULL);

	/*read header file */
	if(ftype) //segy file
	{
		readEbcdicHeader(stdin, ebcdic);
		readBinaryHeader(stdin, endian, &bh, &nsegy);
		totTr = getNumberOfTraceSegyFile(stdin, nsegy);

		ns = bh.hns;
		dt = (float) (bh.hdt/1000000.0);
		format = bh.format;

		bh.format = 1;
		writeEbcdicHeader(stdout, ebcdic);
		writeBinaryHeader(stdout, endian, &bh);
	}
	else
	{
		gettr(&tr);
		ns = tr.ns;
		totTr = getNumberOfTraceSuFile(stdin);
		dt = (float) (tr.dt/1000000.0);
	}

	ishfm=(int)(shfm/dt);

	sTMin = (int) (stmin/dt);
	sTMax = (int) (stmax/dt);
	if(sTMin<0) sTMin = 0;
	if(sTMax<=0) sTMax = ns;
	if(sTMax>ns) sTMax = ns;
	rlag = getGatherGroupInfo(totTr, maxtrrecords, ns, &key, nrecords, ishfm,
			verbose, vblock, &trRecords, &nRecords, ftype, nsegy, format, endian,
			sTMin, sTMax);

	if(ftype)
		gotoTraceSegyPosition(stdin, 0, nsegy);
	else
		rewind(stdin);

	TMin = (int) (tmin/dt);
	TMax = (int) (tmax/dt);
	if(TMin<0) TMin = 0;
	if(TMax<=0) TMax = ns;
	if(TMax>ns) TMax = ns;

	runShStack(rlag, ns, trRecords, nRecords, stk, verbose, vblock, ftype, nsegy,
			format, endian, TMin, TMax, tmin, tmax);

	free1int(rlag);

	t2 = time(NULL);
	divresult = div (t2-t1, 60);
	fprintf (stderr, "Process time = %d min %d sec\n", divresult.quot, divresult.rem);
	return(1);
}

void runShStack(int *rlag, int nsp, int trRecords, int nRecords, int stk,
		int verbose, int vblock, int ftype, int nsegy, int format, int endian,
		int tmin, int tmax, float fmin, float fmax)
{
	int it, lag;
	int i, j;

	float **data, **stkdata;
	char **headers;
	int **nnz;

	//allocate data
	stkdata = alloc2float(nsp, trRecords);
	data = alloc2float(nsp, trRecords);
	headers = alloc2char(HDRBYTES, trRecords);
	nnz = alloc2int(nsp, trRecords);
	memset(stkdata[0], 0, nsp*trRecords*sizeof(float));
	memset(nnz[0], 0, trRecords*nsp*sizeof(int));

	for(i=0; i<nRecords; i++) //loop over gather
	{
		if(verbose && (i%(vblock/100)==0))
			fprintf(stderr, "RUN records %i / %i\n", i, nRecords);

		memset(data[0], 0, trRecords*nsp*sizeof(float));
		lag = rlag[i];

		for(j=0; j<trRecords; j++) //loop over trace in gather
		{
			if(ftype)
				readTraceSegy(stdin, endian, nsp, format, nsegy, &tr);
			else
				gettr(&tr); //read the first trace

			if(i==0) //save header
				memcpy(headers[j], (char*)&tr, HDRBYTES*sizeof(char));

			/* MODIFIED SHIFTING DATA AT tmin to tmax POSITION */
			memcpy(data[j], tr.data, nsp*sizeof(float)); //copy original data
			if ( lag > 0)    /* trace has to be shifted down */
			{
				if(tmax>nsp-lag) tmax = nsp-lag;
				for(it=tmin; it<tmax; it++)
					data[j][it+lag] = tr.data[it];
			}
			else 	   /* trace has to be shifted up */
			{
				if(tmax>nsp+lag) tmax = nsp+lag;
				for(it=tmin; it<tmax; it++)
					data[j][it] = tr.data[it-lag];
			}

			if(j==0)
				fprintf(stderr, "TMin=%f  TMax=%f tmin=%i  tmax=%i \n", fmin, fmax, tmin, tmax);
			if(stk) //stacking data
			{
				for(it=0; it<nsp; it++)
				{
					if(data[j][it] != 0.0)
					{
						stkdata[j][it] += data[j][it];

						if(i>0)
							nnz[j][it]++;
					}
				}
			}
			else
			{
				memcpy(tr.data, data[j], nsp*sizeof(float)); //save gather data
				if(ftype)
					writeTraceSegy(stdout, endian, &tr, nsegy, nsp);
				else
					puttr(&tr);

			}
		}
	}

	if(stk) //stacking data
	{
		for(j=0; j<trRecords; j++) //loop over trace in gather
		{
			for(it=0; it<nsp; it++) //loop over time
			{
				if(nnz[j][it]>0)
					stkdata[j][it] /= nnz[j][it];
			}

			memcpy((char*)&tr, headers[j], HDRBYTES*sizeof(char));
			memcpy(tr.data, stkdata[j], nsp*sizeof(float)); //save stacking data

			if(ftype)
				writeTraceSegy(stdout, endian, &tr, nsegy, nsp);
			else
				puttr(&tr);
		}
	}

	free2float(stkdata);
	free2float(data);
	free2int(nnz);
}

int *getGatherGroupInfo(int totTr, int ntrc, int nsp, cwp_String *key, int nrecords,
		int ishfm, int verbose, int vblock, int *trRecords, int *nRecords,
		int ftype, int nsegy, int format, int endian, int stmin, int stmax)
{
	int i, j;
	int indx, idxtr, idxRecords;
	int maxtrrecord, recordsNum;
	Value *n_val, *o_val;
	int *rlag;		/* realtive shift between records in samples */
	int avlag;
	float **firstdata, **data;

	o_val = (Value*) calloc(1, sizeof(Value));
	n_val = (Value*) calloc(1, sizeof(Value));
	indx = getindex(*key);
	rlag = alloc1int(nrecords);

	maxtrrecord = ntrc;
	idxRecords = 0;
	idxtr = 0;

	/* ----------------------------------------------------------------
	 * READ THE FIRST OFFSET GATHER
	 * -------------------------------------------------------------- */
	firstdata = alloc2float(nsp, maxtrrecord);
	data = alloc2float(nsp, maxtrrecord);
	if(ftype)
		readTraceSegy(stdin, endian, nsp, format, nsegy, &tr);
	else
		gettr(&tr); //read the first trace


	gethval(&tr, indx, o_val);
	memcpy(firstdata[idxtr], tr.data, nsp*sizeof(float));

	for(i=1; i<totTr; i++)
	{
		if(ftype)
			readTraceSegy(stdin, endian, nsp, format, nsegy, &tr);
		else
			gettr(&tr); //read the first trace


		if(verbose && (i%vblock==0))
			fprintf(stderr, "read file %i/%i traces \n", i, totTr);

		gethval(&tr, indx, n_val);
		if(o_val->i==n_val->i)
		{
			idxtr++;
			memcpy(firstdata[idxtr], tr.data, nsp*sizeof(float));
		}
		else
		{
			maxtrrecord = idxtr+1;

			//update old variable
			idxRecords++;
			idxtr = 0;
			o_val->i = n_val->i;
			memcpy(data[idxtr], tr.data, nsp*sizeof(float));
			break;
		}
	}

	/* ----------------------------------------------------------------
	 * LOOP OVER THE OTHER OFFSET GATHER
	 * -------------------------------------------------------------- */
	for(j=i+1; j<totTr; j++)
	{
		if(ftype)
			readTraceSegy(stdin, endian, nsp, format, nsegy, &tr);
		else
			gettr(&tr); //read the first trace


		if(verbose && (j%vblock==0))
			fprintf(stderr, "read file %i/%i traces \n", j, totTr);

		gethval(&tr, indx, n_val);
		if(o_val->i==n_val->i)
		{
			idxtr++;
			memcpy(data[idxtr], tr.data, nsp*sizeof(float));
		}
		else
		{
			//process data
			if(maxtrrecord!=idxtr+1)
			{
				fprintf(stderr,"Please binning your data\n");
				exit(0);
			}

			if(idxRecords>=nrecords)
			{
				fprintf(stderr,"Maximum input records = %i. Change your input parameter\n", nrecords);
				exit(0);
			}
			computeRLag(firstdata, data, maxtrrecord, nsp, rlag, idxRecords, ishfm, stmin, stmax);

			//update old variable
			idxRecords++;
			idxtr = 0;
			o_val->i = n_val->i;
			memcpy(data[idxtr], tr.data, nsp*sizeof(float));
		}
	}
	//process the last gather
	if(maxtrrecord!=idxtr+1)
	{
		fprintf(stderr,"Please binning your data\n");
		exit(0);
	}
	computeRLag(firstdata, data, idxtr+1, nsp, rlag, idxRecords, ishfm, stmin, stmax);
	recordsNum = idxRecords+1;

	/* compute the average lag relative to the first */
	avlag = 0;
	for(i=1; i<recordsNum; i++)
		avlag +=rlag[i];
	avlag =(int)rint((double)(avlag/(recordsNum-1)));

	/* create the shift values */
	rlag[0] = -avlag;
	for(i=1; i<recordsNum; i++)
		rlag[i] -= avlag;

	free2float(firstdata);
	free2float(data);
	free(o_val);
	free(n_val);

	(*trRecords) = maxtrrecord;
	(*nRecords) = recordsNum;
	return(rlag);
}



