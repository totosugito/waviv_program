/*
 * wave_sortheader.c
 *
 *  Created on: Oct 19, 2012
 *      Author: toto
 */

#include "segy_lib.h"

char *sdoc[] = {
		"									",
		" WAVE_SORTHEADER --- WAVE SORT HEADER		",

		NULL};

char *key;
int nkey;
static int *hindex;	/* header key indices			*/
static cwp_Bool *up;	/* sort direction (+ = up = ascending)	*/
static cwp_String type;	/* header key types		*/

int main (int argc, char **argv)
{
	FILE *inp1=NULL, *inp2=NULL;
	int i;
	int pos1, pos2;
	char *strkey;

	/* hook up getpar to handle the parameters */
	initargs(argc,argv);
	requestdoc(1);

	if ((nkey = countparval("key"))!=0)
	{
		/* Allocate lists for key indices, sort directions and key types */
		hindex = ealloc1int(nkey);
		up = (cwp_Bool *) ealloc1(nkey, sizeof(cwp_Bool));
		type = (char *) ealloc1(nkey, sizeof(char));

		strkey = (char *) ealloc1(nkey, sizeof(char));
		getparstring("key", &strkey);
//		pos1 = 0;
		for(i=0; i<nkey; i++)
		{
			printf("key=%s \n", strkey[i]);
		}
	}
//	else  err("key=??");
//
//	/* Allocate lists for key indices, sort directions and key types */
//	index = ealloc1int(nkey);
//  	up = (cwp_Bool *) ealloc1(nkey, sizeof(cwp_Bool));
//	type = (char *) ealloc1(nkey, sizeof(char));
//	for (i = 0; i < nkey; ++i) {
//		switch (**++argv) { /* sign char of next arg */
//		case '+':
//			up[i] = cwp_true;
//			++*argv;   /* discard sign char in arg */
//			break;
//		case '-':
//			up[i] = cwp_false;
//			++*argv;   /* discard sign char in arg */
//			break;
//		default:
//			up[i] = cwp_true;
//			break;
//		}
//		index[i] = getindex(*argv);
//		type[i] = hdtype(*argv)[0]; /* need single char */
//	}
	return(0);
}
