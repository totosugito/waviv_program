/*
 * wave_swt_v1.0.c
 *
 *  Created on: Dec 10, 2012
 *      Author: toto
 */

#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

#include "../src_lib/swt.h"

/*********************** self documentation *****************************/
//seleksi nilai koefisien details yang akan dilekage dengan perbandingan stgh nilai level dibawahnya
char *sdoc[] = {
		"									",
		" WAVE_SWT_V2.0",
		"",
		" ftype=0           0=SU file, 1=SEGY file                               	",
		" endian=0          set =0 for little-endian machines(PC's,DEC,etc.)",
		" tmode=1           (swt mode)",
		"   tmode 1:",
		"     leakage1=3   leakage data from 'leakage1'",
		"     leakage2=5   leakage data until 'leakage2'",
		"   tmode 2:",
		"     treshold1=1  treshold start",
		" filterid=1        1=daubechies1",
		" treshold=0.5      ",
		" treshold1=1",
		" ntreshold=3       threshold until 'details level=nthreshold+1'",
		" level=7           number of decomposition sinyal",
		" invlevel=3        get inverse details from 'invlevel'",
		" vblock=100        show verbose every 'vblock'",
		"",
		" How to use :",
		" wave_swt_v1.0 leakage1=3 leakage2=5 filterid=1 threshold=0.5 level=7 invlevel=3 <input >output",
		"",
		NULL};

int nsp;
int format;
int nsegy;
int endian;
int ntrc;
float dt;
int ftype;

void printdata(char *chr, float *x, int nx);
void save2csv(char *chr, float **x, int nrow, int ncol);
int main(int argc, char **argv)
{
	//swt variable
	int tmode;
	int _leakage2, _leakage1;
	int _inv_level;
	int _ntreshold, _threshold_start;
	float _treshold;
	int _level_scale;
	int _filter_id;

	int i;
	char ebcdic[3200];
	bhed bh;
	float *trace=NULL, *tresult=NULL;
	int vblock;

	div_t divresult;
	time_t t1,t2;

	/* Initialize */
	initargs(argc, argv);
	requestdoc(1);

	t1 = time(NULL);

	if (!getparint("ftype",&ftype)) ftype=0;
	if (!getparint("endian",&endian)) endian=0;
	if (!getparint("tmode",&tmode)) tmode=1;
	if (!getparint("vblock",&vblock)) vblock=100;
	if (!getparint("leakage1",&_leakage1)) _leakage1=3;
	if (!getparint("leakage2",&_leakage2)) _leakage2=5;
	if((_leakage2<_leakage1) || (_leakage2<=0) || (_leakage1<=1))
		err("[leakage2 < leakage1] or [leakage2<=0] or [leakage1<=1]!!");

	if (!getparint("filterid",&_filter_id)) _filter_id=1;
	if (!getparfloat("treshold",&_treshold)) _treshold=0.5;
	if (!getparint("treshold1",&_threshold_start)) _threshold_start=1;
	if (!getparint("ntreshold",&_ntreshold)) _ntreshold=3;
	if (!getparint("level",&_level_scale)) _level_scale=7;
	if (!getparint("invlevel",&_inv_level)) _inv_level=3;
	if(_filter_id<0) _filter_id=1;

	if(_leakage2 > _level_scale)
		err("leakage2 > level");

	if(_inv_level > _level_scale)
		err("invlevel > level");

	if(_ntreshold>=_level_scale)
		err("ntreshold >= level");

	if(_leakage1 > _leakage2)
		err("_leakage1 > _leakage2");

	if(_threshold_start > _ntreshold)
		err("treshold1 > _ntreshold");

	/*read header file */
	if(ftype) //segy file
	{
		readEbcdicHeader(stdin, ebcdic);
		readBinaryHeader(stdin, endian, &bh, &nsegy);
		ntrc = getNumberOfTraceSegyFile(stdin, nsegy);

		nsp = bh.hns;
		format = bh.format;
		dt = (float) (bh.hdt/1000000.0);

		gotoTraceSegyPosition(stdin, 0, nsegy);
	}
	else
	{
		gettr(&tr);
		ntrc = getNumberOfTraceSuFile(stdin);
		nsp = tr.ns;
		dt = (float) (tr.dt/1000000.0);

		rewind(stdin);
	}

	/*write header for segy output */
	if(ftype) //segy file
	{
		bh.format = 1;
		writeEbcdicHeader(stdout, ebcdic);
		writeBinaryHeader(stdout, endian, &bh);
	}

	trace = alloc1float(nsp);
	for(i=0; i<ntrc; i++)
	{
		if(i%vblock==0)
			fprintf(stderr, "run trace %i / %i \n", i+1, ntrc);
		if (ftype) readTraceSegy(stdin, endian, nsp, format, nsegy, &tr);
		else gettr(&tr);

		memcpy(trace, tr.data, nsp*sizeof(float));

		//process swt
		if(tmode==1)
			tresult = run_swt_v1(trace, nsp, _filter_id, _level_scale, _treshold, _ntreshold,
					_leakage1, _leakage2, _inv_level/*, &newNt*/);
		else if(tmode==2)
			tresult = run_swt_v2(trace, nsp, _filter_id, _level_scale, _treshold,
					_threshold_start, _ntreshold,
					_inv_level);
		else
			tresult = run_swt_v1(trace, nsp, _filter_id, _level_scale, _treshold, _ntreshold,
					_leakage1, _leakage2, _inv_level/*, &newNt*/);

		memcpy(tr.data, tresult, nsp*sizeof(float));

		if (ftype) writeTraceSegy(stdout, endian, &tr, nsegy, nsp);
		else puttr(&tr);

		free1float(tresult);
	}


	free1float(trace);
	t2 = time(NULL);
	divresult = div (t2-t1, 60);
	fprintf (stderr, "Process time = %d min %d sec\n", divresult.quot, divresult.rem);


	return(0);
}


void printdata(char *chr, float *x, int nx)
{
	int i;
	int idx;

	fprintf(stderr,"%s : \n", chr);
	idx = 0;
	for(i=0; i<nx; i++){
		fprintf(stderr,"%5.5f  ", x[i]);
		idx++;
		if(idx==5){
			fprintf(stderr,"\n");
			idx = 0;
		}
	}
	fprintf(stderr,"\n\n");
}

void save2csv(char *chr, float **x, int nrow, int ncol)
{
	FILE *fod=NULL;
	int i, j;
	char *buff;
	int nbuf=10;

	fprintf(stderr, "SAVE %s : %i x %i \n", chr, nrow, ncol);
	fod = fopen(chr, "w");
	if(!fod) err("error opening file %s", chr);

	buff = (char*) calloc(nbuf, sizeof(char));
	for(i=0; i<ncol; i++)
	{
		for(j=0; j<nrow; j++)
		{
			if(j==nrow-1)
				sprintf(buff, "%f \n", x[j][i]);
			else
				sprintf(buff, "%f \t", x[j][i]);
			fwrite(buff, 1, strlen(buff), fod);
			memset(buff, 0, nbuf);
		}
	}
	fclose(fod);
	free(buff);
}
