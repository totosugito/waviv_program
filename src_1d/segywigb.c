/* Copyright (c) Colorado School of Mines, 2010.*/
/* All rights reserved.                       */

/* SUXWIGB: $Revision: 1.37 $ ; $Date: 2007/09/20 21:12:54 $		*/

#include "segy_lib.h"
#include <signal.h>

/*********************** self documentation *****************************/
char *sdoc[] = {
		"									",
		" SEGYWIGB - X-windows Bit-mapped WIGgle plot of a segy data set		",
		" This is a modified suxwigb that uses the depth or coordinate scaling	",
		" when such values are used as keys.					",
		"									",
		" segywigb <stdin [optional parameters] | ...				",
		"									",
		" Optional parameters:							",
		" key=(keyword)		if set, the values of x2 are set from header field",
		"			specified by keyword				",
		" n2=tr.ntr or number of traces in the data set (ntr is an alias for n2)",
		" d1=tr.d1 or tr.dt/10^6	sampling interval in the fast dimension ",
		"   =.004 for seismic		(if not set)				",
		"   =1.0 for nonseismic		(if not set)				",
		" d2=tr.d2			sampling interval in the slow dimension ",
		"   =1.0			(if not set)				",
		" f1=tr.f1 or tr.delrt/10^3 or 0.0  first sample in the fast dimension	",
		" f2=tr.f2 or tr.tracr or tr.tracl  first sample in the slow dimension	",
		"   =1.0 for seismic		    (if not set)			",
		"   =d2 for nonseismic		    (if not set)			",
		"									",
		" style=seismic         normal (axis 1 horizontal, axis 2 vertical) or",
		"                       seismic (axis 1 vertical, axis 2 horizontal)",
		" verbose=0              =1 to print some useful information		",
		"									",
		" 									",
		" Note that for seismic time domain data, the \"fast dimension\" is	",
		" time and the \"slow dimension\" is usually trace number or range.	",
		" Also note that \"foreign\" data tapes may have something unexpected	",
		" in the d2,f2 fields, use segyclean to clear these if you can afford	",
		" the processing time or use d2= f2= to override the header values if	",
		" not.									",
		"									",
		" This program is really just a wrapper for the plotting program: xwigb	",
		" See the xwigb selfdoc for the remaining parameters.			",
		"									",
		NULL};

/* Credits:
 *
 *	CWP: Dave Hale and Zhiming Li (xwigb, etc.)
 *	   Jack Cohen and John Stockwell (suxwigb, etc.)
 *	Delphi: Alexander Koek, added support for irregularly spaced traces
 *
 *	Modified by Brian Zook, Southwest Research Institute, to honor
 *	 scale factors, added vsp style
 *
 * Notes:
 *	When the number of traces isn't known, we need to count
 *	the traces for xwigb.  You can make this value "known"
 *	either by getparring n2 or by having the ntr field set
 *	in the trace header.  A getparred value takes precedence
 *	over the value in the trace header.
 *
 *	When we must compute ntr, we don't allocate a 2-d array,
 *	but just content ourselves with copying trace by trace from
 *	the data "file" to the pipe into the plotting program.
 *	Although we could use tr.data, we allocate a trace buffer
 *	for code clarity.
 */
/**************** end self doc *******************************************/

int main(int argc, char **argv)
{
	char *plotcmd;		/* build xwigb command for popen	*/
	FILE *plotfp;		/* fp for plot data			*/
	int nt;			/* number of samples on trace		*/
	int ntr;		/* number of traces			*/
	int verbose;		/* verbose flag				*/
	float d1;		/* time/depth sample rate		*/
	float d2;		/* trace/dx sample rate			*/
	float f1;		/* tmin/zmin				*/
	float f2;		/* tracemin/xmin			*/
	cwp_Bool seismic;	/* is this seismic data?		*/
	cwp_Bool have_ntr=cwp_false;/* is ntr known from header or user?	*/

	char *cwproot;		/* value of CWPROOT environment variable*/
	cwp_String style;	/* style parameter			*/

	int jtr;
	char ebcdic[3200];
	int nsegy, format, endian, ntraces;

	/* Initialize */
	initargs(argc, argv);
	requestdoc(1);

	if (!getparint("endian", &endian)) endian=0;

	readEbcdicHeader(stdin, ebcdic);
	readBinaryHeader(stdin, endian, &bh, &nsegy);
	ntraces =getNumberOfTraceSegyFile(stdin, nsegy);
	ntr = ntraces;
	nt = bh.hns;
	format = bh.format;

	/* Get info from first trace */
	readTraceSegy(stdin, endian, nt, format, nsegy, &tr);
	seismic = ISSEISMIC(tr.trid);

	if (!getparint("verbose", &verbose))	verbose=0;
	if (!getparfloat("d1", &d1))
	{
		if	(tr.d1)	 d1 = tr.d1;
		else if (tr.dt)	 d1 = ((double) tr.dt)/1000000.0;
		else
		{
			if (seismic)
			{
				d1 = 0.004;
				warn("tr.dt not set, assuming dt=0.004");
			}
			else  /* non-seismic data */
			{
				d1 = 1.0;
				warn("tr.d1 not set, assuming d1=1.0");
			}
		}
	}

	/* Get or set ntr */
	if (getparint("n2", &ntr) || getparint("ntr", &ntr)) have_ntr = cwp_true;
	if (!getparfloat("d2", &d2)) d2 = (tr.d2) ? tr.d2 : 1.0;

	if (!getparfloat("f1", &f1))
	{
		if	(tr.f1)	    f1 = tr.f1;
		else if (tr.delrt)  f1 = (float) tr.delrt/1000.0;
		else		f1 = 0.0;
	}

	if (!getparfloat("f2", &f2))
	{
		if	(tr.f2) f2 = tr.f2;
		else if (tr.tracr)  f2 = (float) tr.tracr;
		else if (tr.tracl)  f2 = (float) tr.tracl;
		else if (seismic)   f2 = 1.0;
		else		f2 = 0.0;
	}

	if (!getparstring("style", &style)) style = "seismic";

	/* Get value of CWPROOT environment variable */
	if (!(cwproot = getenv("CWPROOT"))) cwproot ="" ;
	if (STREQ(cwproot, ""))
	{
		warn("CWPROOT environment variable is not set! ");
		err("Set CWPROOT in shell environment as per instructions in CWP/SU Installation README files");
	}

	plotcmd = (char *) emalloc(BUFSIZ);
	sprintf(plotcmd,
			"xwigb n1=%d n2=%d d1=%f d2=%f f1=%f f2=%f style=%s",
			nt, ntr, d1, d2, f1, f2, style);

	for (--argc, ++argv; argc; --argc, ++argv)
	{
		if (strncmp(*argv, "d1=", 3) && /* skip those already set */
				strncmp(*argv, "d2=", 3) &&
				strncmp(*argv, "f1=", 3) &&
				strncmp(*argv, "f2=", 3) &&
				strncmp(*argv, "style=", 6))
		{

			strcat(plotcmd, " ");	/* put a space between args */
			strcat(plotcmd, "\"");	/* user quotes are stripped */
			strcat(plotcmd, *argv); /* add the arg */
			strcat(plotcmd, "\"");	/* user quotes are stripped */
		}
	}


	/* Open pipe to xwigb and send the traces */
	plotfp = epopen(plotcmd, "w");
	free(plotcmd);

	gotoTraceSegyPosition(stdin, 0, nsegy);
	for(jtr=0; jtr<ntraces; jtr++)
	{
		readTraceSegy(stdin, endian, nt, format, nsegy, &tr);
		efwrite(tr.data, FSIZE, nt, plotfp);
	}

	epclose(plotfp);
	return EXIT_SUCCESS;
}
