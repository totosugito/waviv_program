/*
 * segyfft.c
 *
 *  Created on: Mar 19, 2012
 *      Author: toto
 */

#include "segy_lib.h"
#include "fftLib.h"
#include <string.h>
#include <stdlib.h>
#include <time.h>

/*********************** self documentation *****************************/
char *sdoc[] = {
		"									",
		" SEGYFFT = Process FFT input file and save as amp, phase, real or imag",
		"",
		" endian=0      ((0=little endian)",
		" mode=amp      (output flag)		 			",
		"               (=amp	output amplitude traces)			",
		"               (=phase	output phase traces)			",
		"               (=real	output real parts)			",
		"               (=imag	output imag parts)	 		",
		" bfase=0       (=1 save fase too)",
		" cfase=        (if bfase=1, fill output fase filename)",
		" vblock=1000   (show verbose every vblock)",
		"",
		" How to Use :",
		" segyfft < input.sgy  mode=amp > output.sgy",
		" segyfft < input.sgy  mode=amp bfase=1 cfase=fase.sgy > output.sgy",
		"",
		NULL};


int main(int argc, char **argv)
{
	int endian, format, dti, nsegy, nsegy1;
	char ebcdic[3200];
	char *mode, *cfase=NULL;

	int ntrc, nsp;
	int nx, nf, nfft, imode, bfase;
	float df, dt;
	int i,j, idxblock, vblock;

	float **z=NULL;
	float **freal=NULL, **fimag=NULL;

	segy *tr1;
	bhed *bh1;
	FILE *FFase=NULL;
	div_t divresult;
	time_t t1,t2;

	/* Initialize */
	initargs(argc, argv);
	requestdoc(1);

	MUSTGETPARSTRING("mode",  &mode);
	if (!getparint("endian", &endian)) endian=0;
	if (!getparint("bfase", &bfase)) bfase=0;
	if (!getparint("vblock", &vblock)) vblock=1000;

	/* start process */
	t1 = time(NULL);

	if(strcmp(mode, "amp")==0)
		imode = 0;
	else if(strcmp(mode, "phase")==0)
		imode = 1;
	else if(strcmp(mode, "real")==0)
		imode = 2;
	else if(strcmp(mode, "imag")==0)
		imode = 3;
	else	//default save as amplitude
		imode = 0;

	/* opening file segy*/
	readEbcdicHeader(stdin, ebcdic);
	readBinaryHeader(stdin, endian, &bh, &nsegy);
	ntrc =getNumberOfTraceSegyFile(stdin, nsegy);
	nsp = bh.hns;
	format = bh.format;
	dti = bh.hdt;
	dt = (float) (dti/1e+6);
	getNfft(nsp, dt, &nf, &nfft, &df);

	/*opening output file */
	bh.format = 1;
	bh.hns = nf;
	nsegy1 = nf*4+240;

	bh1 = (bhed*) calloc(1,sizeof(bhed));
	tr1 = (segy*) calloc(1, sizeof(segy));
	memcpy(bh1, &bh, sizeof(bhed));

	writeEbcdicHeader(stdout, ebcdic);
	writeBinaryHeader(stdout, endian, &bh);

	if(bfase)
	{
		MUSTGETPARSTRING("cfase",  &cfase);
		FFase = fopen(cfase, "w");
		if(!FFase)	err("error opening file %s", cfase);
		writeEbcdicHeader(FFase, ebcdic);
		writeBinaryHeader(FFase, endian, bh1);
	}

	//allocate space
	nx = 1;
	z = alloc2float(nsp,nx);
	freal = alloc2float(nf,nx);
	fimag = alloc2float(nf,nx);
	idxblock = 1;
	for(i=0; i<ntrc; i++)		//read trace
	{
		if(idxblock*vblock==i){
			fprintf(stderr, "Process Trace [ %i / %i ]\n", i+1, ntrc);
			idxblock++;
		}

		readTraceSegy(stdin, endian, nsp, format, nsegy, &tr);
		memcpy(tr1, &tr, sizeof(segy));

		for(j=0; j<nsp; j++)	z[nx-1][j] = tr.data[j];

		fftProcess(z, nsp, nx, nf, nfft, freal, fimag);

		for(j=0; j<nf; j++)
		{
			if(imode==0) //amp
				tr.data[j] = sqrt(freal[0][j]*freal[0][j] + fimag[0][j]*fimag[0][j]);
			else if(imode==1) //phase
				tr.data[j] = atan2(fimag[0][j], freal[0][j]);
			else if(imode==2)	//real
				tr.data[j] = freal[0][j];
			else if(imode==3)	//image
				tr.data[j] = fimag[0][j];
			else	//default save as amplitude
				tr.data[j] = sqrt(freal[0][j]*freal[0][j] + fimag[0][j]*fimag[0][j]);

			if(bfase)
				tr1->data[j] = atan2(fimag[0][j], freal[0][j]);
		}

		tr.ns = nf;
		writeTraceSegy(stdout, endian, &tr, nsegy1, nf);

		if(bfase)
		{
			tr1->ns = nf;
			writeTraceSegy(FFase, endian, tr1, nsegy1, nf);
		}
	}

	free2float(z);
	free2float(freal);
	free2float(fimag);
	if(bfase) {
		fclose(FFase);
		free(bh1);
		free(tr1);
	}

	t2 = time(NULL);
	divresult = div (t2-t1, 60);
	fprintf (stderr, "Process time = %d min %d sec\n", divresult.quot, divresult.rem);

	return(1);
}
