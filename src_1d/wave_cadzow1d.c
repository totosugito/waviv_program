/*
 * wave_cadzow1d.c
 *
 *  Created on: Apr 25, 2012
 *      Author: toto
 */
#include "../src_lib/cadzowLib.h"
#include "../src_lib/fftLib.h"

char *sdoc[] = {
		"			",
		"WAVE_CADZOW1D ",
		"wave_cadzow1d <stdin rank=1> output",
		"			",
		"rank=             rank cadzow  ",
		"iswindowing=1     windowing data",
		"			",
		NULL};


int main(int argc, char ** argv) {
	char cebcdic[3200];
	int i, j;
	int ns, ntrc, format, endian, nsegy;
	float **dataOri;
	float d1, dt;
	float fmin, fmax;
	int tmin, tmax;
	int nf, nfft;
	int rank;
	int  iswindowing;
	div_t divresult;
	time_t t1,t2;

	float e;
	int maxiter;

	int span,use,spantrace,usetrace;
	float sigmax,sigmay;

	/* hook up getpar to handle the parameters */
	initargs(argc,argv);
	requestdoc(1);

	if (!getparint("rank",&rank)) rank=1;
	if (!getparint("endian",&endian)) endian=0;
	if (!getparfloat("fmin",&fmin)) fmin=0.0;
	if (!getparfloat("fmax",&fmax)) fmax=60.0;

	if (!getparint("iswindowing",&iswindowing)) iswindowing=1;
	if(!getparint("span",&span))span=60;
	if(!getparint("use",&use))use=10;

	if(!getparint("spantrace",&spantrace))spantrace=30;
	if(!getparint("usetrace",&usetrace))usetrace=5;

	if(!getparfloat("sigmax",&sigmax))sigmax=20;
	if(!getparfloat("sigmay",&sigmay))sigmay=20;

	if(!getparfloat("e",&e)) e=20;
	if(!getparint("maxiter",&maxiter))maxiter=10;
	/* get variable fft */
	t1 = time(NULL);
	readEbcdicHeader(stdin, cebcdic); /* read ebcdic header */
	readBinaryHeader(stdin, endian, &bh, &nsegy); /*read binary header */
	ntrc = getNumberOfTraceSegyFile(stdin, nsegy);
	ns = bh.hns;
	format = bh.format;
	dt = (float) (bh.hdt / 1e+6);
	getNfft(ns, dt, &nf, &nfft, &d1);
	if(fmin<0.0)	fmin = 0.0;
	if(fmax<=0 || fmax>=nf*d1) fmax = nf*d1 - d1;
	tmin = (int) (fmin/d1);
	tmax = (int) (fmax/d1);

	fprintf(stderr, "nf=%i nfft=%i fmin=%f fmax=%f tmin=%i tmax=%i \n",
			nf, nfft, fmin, fmax, tmin, tmax);
	bh.format = 1;
	writeEbcdicHeader(stdout, cebcdic);
	writeBinaryHeader(stdout, endian, &bh);

	//read data
	dataOri = alloc2float(ns, ntrc);
	for (i=0; i<ntrc; i++)
	{
		readTraceSegy(stdin, endian, ns, format, nsegy, &tr);
		for (j=0; j<ns; j++)
			dataOri[i][j] = tr.data[j];
	}

	if(ntrc > 2*(rank-1))
	{
		if(iswindowing==0)
			runCadzow1D(dataOri, rank, ntrc, ns, nf, nfft, d1, tmin, tmax);
		else if(iswindowing==1)
			runCadzow1D_wosa(dataOri, rank, ntrc, ns, dt,
					use, span, usetrace, spantrace, sigmax, sigmay);
		else if(iswindowing==2)
			runCadzow1D_wosa_rev1(dataOri, rank, ntrc, ns, dt,
					use, span, usetrace, spantrace, sigmax, sigmay);
		else if(iswindowing==3)
			robustRunCadzow1D_wosa_rev1(dataOri, rank, ntrc, ns, dt,
					use, span, usetrace, spantrace, sigmax, sigmay, e, maxiter);
	}
	gotoTraceSegyPosition(stdin, 0, nsegy);
	for(i=0; i<ntrc; i++)
	{
		readTraceSegy(stdin, endian, ns, format, nsegy, &tr);
		for(j=0; j<ns; j++)
			tr.data[j] = dataOri[i][j];

		writeTraceSegy(stdout, endian, &tr, nsegy, ns);
	}

	free2float(dataOri);

	t2 = time(NULL);
	divresult = div (t2-t1, 60);
	fprintf (stderr, "Process time = %d min %d sec\n", divresult.quot, divresult.rem);

	return(1);
}
