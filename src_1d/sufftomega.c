/*
 * sufftomega.c
 *
 *  Created on: May 5, 2011
 *      Author: toto
 */

#include "../src_lib/segy_lib.h"
#include "../src_lib/fftLib.h"
#include <time.h>

/*********************** self documentation **********************/
char *sdoc[] = {
		" 									",
		" sufftomega.v1.0              ",
		"									",
		" sufftomega.v1.0 <stdin >stdout [...]	                                ",
		"									",
		" Optional Parameters:							",
		" iomega=1        (iomega value= 0, 1 or 3",
		"                 iomega=0  -> F*(1/omega)",
		"                 iomega=1  -> F*(1/(omega^2))",
		"                 iomega=2  -> F*sqrt(i/omega)",
		" fmin=0.0        minimum frequency",
		" fmax=0.0        maximum frequency",
		" cval=1.0        constant value",
		" fmin=0.0 fmax=0.0 --> process all frequency",
		"",
		" Sample Command :"
		" sufftomega.v1.0 fmin=0.0 fmax=0.0 iomega=1 < input.su > output.su",
		"									",
		NULL};

/**************** end self doc ***********************************/
#define LOOKFAC	2	/* Look ahead factor for npfaro	  */
#define PFA_MAX	720720	/* Largest allowed nfft	          */
void processIOmega(float *trace, int nsp, int nfft, int nf, float d1, int iomega,
		int tmin, int tmax, float cval);

int main(int argc, char **argv)
{
	div_t divresult;
	time_t t1,t2;

	float d1;		/* sample interval in Hz		*/
	int nfft;		/* transform length			*/
	int nf;			/* number of frequencies		*/

	float dt;		/* sampling interval in secs		*/
	int nsp;
	float *data;
	float fmin, fmax, cval;
	int tmin, tmax;
	int i, iomega;

	/* Initialize */
	initargs(argc, argv);
	requestdoc(1);

	if (!getparint("iomega", &iomega)) iomega=1;
	if (!getparfloat("fmin", &fmin)) fmin=0.0;
	if (!getparfloat("fmax", &fmax)) fmax=0.0;
	if (!getparfloat("cval", &cval)) cval=1.0;
	//	if (!getparint("verbose", &verbose)) verbose=1;
	//	if (!getparint("endian", &endian)) endian=0;
	//	if (!getparint("vblock", &vblock)) vblock=200;

	gettr(&tr);	/*read first trace */

	dt = ((double) tr.dt)/1000000.0;
	nsp = tr.ns;

	getNfft(nsp, dt, &nf, &nfft, &d1);

	if(fmax<fmin)	err("fmax < fmin !");

	if(fmin<=0) fmin = 0.0;
	else if(fmin>=nf) fmin=nf*d1-d1;

	if(fmax<=0) fmax = nf*d1-d1;
	else if(fmax>=nf) fmax = nf*d1-d1;

	tmin = (int) (fmin/d1);
	tmax = (int) (fmax/d1);

	fprintf(stderr, "nf=%i   df=%f   ftotal=%f \n", nf, d1, d1*nf);
	fprintf(stderr, "fmin=%f   fmax=%f   tmin=%i   tmax=%i \n", fmin, fmax, tmin, tmax);
	/* start process */
	t1 = time(NULL);

	data = ealloc1float(nsp);
	do
	{
		/* convert trace to array */
		for(i=0; i<nsp; i++)
			data[i] = tr.data[i];

		/*process data */
		processIOmega(data, nsp, nfft, nf, d1, iomega, tmin, tmax, cval);

		/* convert array to trace*/
		for(i=0; i<nsp; i++)
			tr.data[i] = data[i];

		puttr(&tr);

	}while (gettr(&tr));

	t2 = time(NULL);
	divresult = div (t2-t1, 60);

	warn ("Process time = %d min %d sec\n", divresult.quot, divresult.rem);


	free1float(data);
	return(1);
}


void processIOmega(float *trace, int nsp, int nfft, int nf, float d1, int iomega,
		int tmin, int tmax, float cval)
{
	int it, ifq, itt, nspw;
	complex *ct;		/* complex transformed trace (window)  	*/
	float *tidataw;       	/* real trace in time window - input   	*/
	float *ttidataw;      	/* real trace in time window - input   	*/
	complex *fdata;	/* data - freq. domain          	*/
	float *todataw;       	/* real trace in time window - output   */
	complex *ffreq;	/* filtered frequency vector 	  	*/
	complex *freqv;		/* frequency vector		      	*/

	nspw = nsp;
	tidataw = alloc1float(nfft);
	ttidataw = alloc1float(nfft);
	fdata = alloc1complex(nf);

	ffreq = alloc1complex(nf);

	freqv = alloc1complex(nfft);
	todataw = alloc1float(nfft);
	ct = alloc1complex(nfft);


	memcpy(tidataw, trace , nsp*sizeof(float));

	memset((void *) (tidataw + nspw), 0, (nfft-nspw)*FSIZE);
	memset((void *) ct, 0, nfft*sizeof(complex));

	/* FFT from t to f */
	for (it=0;it<nfft;it++)
		ttidataw[it]=(it%2 ? -tidataw[it] : tidataw[it]);
	pfarc(1, nfft, ttidataw, ct);

	/* Store values */
	for (ifq = 0; ifq < nf; ifq++)
		fdata[ifq] = ct[nf-1-ifq];

	/*PROCESS COMPLEX NUMBER
	 * ---------------------------------------*/
	float fvar, ival;
	complex cplxval, cfvar;
	ffreq[tmin] = fdata[tmin];
	for(ifq=tmin+1; ifq<=tmax; ifq++)
	{
		if (iomega==0)
		{
			fvar = 1/(2*PI*ifq*d1);
			fvar *= cval;
			ffreq[ifq] = crmul(fdata[ifq], fvar);
		}
		else if(iomega==1)
		{
			fvar = (float) (1.0/pow(2*PI*ifq*d1, 2.0));
			fvar *= cval;
			ffreq[ifq] = crmul(fdata[ifq], fvar);
		}
		else if(iomega==2)
		{
			ival = (float) (1.0/(2*PI*ifq*d1));
			fvar *= cval;
			cplxval = cmplx(0.0, ival);
			cfvar = csqrt(cplxval);
			ffreq[ifq] = cmul(fdata[ifq], cfvar);
		}
	}

	/*=============================================================================
	 * RETURN DATA TO T-X
	=============================================================================*/
	/* select data */
	for (ifq=0,itt=nf-1;ifq<nf;ifq++,itt--)
		freqv[ifq] = ffreq[itt];

	memset((void *) (freqv+nf), 0, (nfft-nf)*sizeof(complex));
	memset((void *) todataw, 0, nfft*FSIZE);

	/* FFT back from f to t and scaling */
	pfacr(-1, nfft, freqv, todataw);
	for (it=0;it<MIN(nsp,nfft);it++)
		trace[it]=(it%2 ? -todataw[it]/nfft : todataw[it]/nfft);

	free1float(tidataw);
	free1float(ttidataw);
	free1complex(fdata);

	free1float(todataw);
	free1complex(ct);
	free1complex(freqv);
	free1complex(ffreq);
}

