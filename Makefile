#valgrind --tool=memcheck --leak-check=yes --show-reachable=yes --num-callers=20 --track-fds=yes 

SRC_LIB=./src_lib
SRC_1D=./src_1d
SRC_3D=./src_3d
SRC_MAT=./matlab_lib
TESTING=./testing
PARALLEL=./parallel

#compile code
srclib:
	cd ${SRC_LIB} && make
	
src1d:
	cd ${SRC_1D} && make
	
src3d:
	cd ${SRC_3D} && make

tes:
	cd ${TESTING} && make

parallel:
	cd ${PARALLEL} && make
	
#clean code
clean_srclib:
	cd ${SRC_LIB} && make clean
	
clean_src1d:
	cd ${SRC_1D} && make  clean
	
clean_src3d:
	cd ${SRC_3D} && make  clean
	
