/*
 * spdiags.c
 *
 *  Created on: May 20, 2010
 *      Author: toto
 */
//SPDIAGS Sparse matrix formed from diagonals.
//   SPDIAGS, which generalizes the function "diag", deals with three
//   matrices, in various combinations, as both input and output.
//
//   [B,d] = SPDIAGS(A) extracts all nonzero diagonals from the m-by-n
//   matrix A.  B is a min(m,n)-by-p matrix whose columns are the p
//   nonzero diagonals of A.  d is a vector of length p whose integer
//   components specify the diagonals in A.
//
//   B = SPDIAGS(A,d) extracts the diagonals specified by d.
//   A = SPDIAGS(B,d,A) replaces the diagonals of A specified by d with
//       the columns of B.  The output is sparse.
//   A = SPDIAGS(B,d,m,n) creates an m-by-n sparse matrix from the
//       columns of B and places them along the diagonals specified by d.
//
//   Roughly, A, B and d are related by
//       for k = 1:p
//           B(:,k) = diag(A,d(k))
//       end
//
//   Example: These commands generate a sparse tridiagonal representation
//   of the classic second difference operator on n points.
//       e = ones(n,1);
//       A = spdiags([e -2*e e], -1:1, n, n)
//
//   Some elements of B, corresponding to positions "outside" of A, are
//   not actually used.  They are not referenced when B is an input and
//   are set to zero when B is an output.  If a column of B is longer than
//   the diagonal it's representing, elements of super-diagonals of A
//   correspond to the lower part of the column of B, while elements of
//   sub-diagonals of A correspond to the upper part of the column of B.
//
//   Example: This uses the top of the first column of B for the second
//   sub-diagonal and the bottom of the third column of B for the first
//   super-diagonal.
//       B = repmat((1:n)',1,3);
//       S = spdiags(B,[-2 0 1],n,n);

#include "spdiags.h"

//program ini adalah modifikasi program spdiags.m dari matlab
//perbedaan terletak kalau pada matlab menggunakan matrix sparse, sedangkan
//pada program ini menggunakan matrix full
//program hanya mampu menerima input untuk arg2 = [-1 0 1]
//untuk input arg1 yang bermacam2 belum diimplementasikan
//21-05-2010
double *get_s_spdiags(double *b, double **arg1, int row, int col, /*int *arg2, int len2, */int arg3, int arg4)
{
	int i;
	int s;
	double *s_spline=NULL;
	gsl_matrix *aspdiags=NULL;
	gsl_matrix *aspdiagsInv=NULL;
	gsl_matrix *spline_var=NULL;
	gsl_matrix *gsl_mb=NULL;
	gsl_vector *gsl_b=NULL;
	gsl_vector *tmp=NULL;
	gsl_permutation *p=NULL;

	if((row!=arg3) && (row!=arg4))
	{
		fprintf(stderr, "spdiags : (row!=arg3) && (row!=arg4) \n");
		exit(0);
	}

	//create matrix (gsl library) for process invers
	s_spline = su_ealloc1double(arg3);
	gsl_b = gsl_vector_calloc(arg3);
	tmp = gsl_vector_calloc(arg3);
	gsl_mb = gsl_matrix_calloc(1, arg4);
	memcpy(gsl_b->data, b, arg3*sizeof(double));
	gsl_matrix_set_row(gsl_mb, 0, gsl_b);

	aspdiags = gsl_matrix_calloc(arg3, arg4);
	aspdiagsInv = gsl_matrix_calloc(arg3, arg4);
	spline_var = gsl_matrix_calloc(1, arg4);
	for(i=0; i<arg4; i++) //loop over column
	{
		if(i==0)
		{
			gsl_matrix_set(aspdiags, i, 0, arg1[i][1]);
			gsl_matrix_set(aspdiags, i+1, 0, arg1[i][0]);
		}
		else if(i==arg3-1)
		{
			gsl_matrix_set(aspdiags, i-1, i, arg1[i][2]);
			gsl_matrix_set(aspdiags, i, i, arg1[i][1]);
		}
		else
		{
			gsl_matrix_set(aspdiags, i-1, i, arg1[i][2]);
			gsl_matrix_set(aspdiags, i, i, arg1[i][1]);
			gsl_matrix_set(aspdiags, i+1, i, arg1[i][0]);
		}
	} //end loop over column

	/*INVERS MATRIX aspdiags*/
	p = gsl_permutation_alloc (arg4);
	gsl_linalg_LU_decomp (aspdiags, p, &s);
	gsl_linalg_LU_invert (aspdiags, p, aspdiagsInv);

	/*PRODUCT arg1 and aspdiagsInv Matrix*/
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, gsl_mb, aspdiagsInv, 0.0, spline_var);

	//copy output to pointer
	gsl_matrix_get_row(tmp, spline_var, 0);
	memcpy(s_spline, tmp->data, arg4*sizeof(double));

	gsl_vector_free(gsl_b);
	gsl_vector_free(tmp);
	gsl_matrix_free(aspdiags);
	gsl_matrix_free(aspdiagsInv);
	gsl_matrix_free(spline_var);
	gsl_matrix_free(gsl_mb);
	gsl_permutation_free (p);
	return(s_spline);
}

