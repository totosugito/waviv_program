/*
 * spdiags.h
 *
 *  Created on: May 20, 2010
 *      Author: toto
 */

#ifndef SPDIAGS_H_
#define SPDIAGS_H_

#include "../array/array.h"
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_vector.h>

double *get_s_spdiags(double *b, double **arg1, int row, int col, /*int *arg2, int len2, */int arg3, int arg4);
#endif /* SPDIAGS_H_ */
