#include "waviv_common.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <sys/types.h>
//#include <sys/stat.h>
#include <unistd.h>
#include <time.h>


float* gaussianderivative1kernel(int ksize,float sigma)
{
	if(ksize%2==0){printf("%i\n", ksize%2);exit(EXIT_FAILURE);}
	int span = (ksize-1) /2;
	float con = sqrt(2*3.14159)*sigma;
	float* kernel = (float*) malloc(ksize*sizeof(float));
	int x,index=0;
	for(x=span*-1; x<=span; x++){
		kernel[index]  = exp((-1.0*(float)x*(float)x)/(2.0*sigma*sigma))/con;
		kernel[index] *= ( (-1.0)* (float)x / (sigma*sigma));	
		index++;
	}
	return kernel;
}

float* morletkernel(int ksize,float sigma)
{
	if(ksize%2==0){printf("%i\n", ksize%2);exit(EXIT_FAILURE);}
	int span = (ksize-1) /2;
	float con = 2.0 / (sqrt(3*sigma)*1.331335083);
	float* kernel = (float*) malloc(ksize*sizeof(float));
	int x,index=0;

	for(x=span*-1; x<=span; x++){
		float xf = (float)x;
		kernel[index]  = exp((-1.0*xf*xf)/(2.0*sigma*sigma))*con;
		kernel[index] *= ( 1 - ( (xf*xf)/(sigma*sigma) ) );	
		index++;
	}
	return kernel;
}


float* gaussiankernel(int ksize,float sigma)
{
	if(ksize%2==0){printf("%i\n", ksize%2);exit(EXIT_FAILURE);}
	int span = (ksize-1) /2;
	float con = sqrt(2*3.14159)*sigma;
	float* kernel = (float*) malloc(ksize*sizeof(float));
	int x,index=0;
	for(x=span*-1; x<=span; x++){
		kernel[index]  = exp((-1.0*(float)x*(float)x)/(2.0*sigma*sigma))/con;	
		index++;
	}
	return kernel;
}

double* gaussiankerneld(int ksize,float sigma)
{
	if(ksize%2==0)
		printf("%i\n", ksize%2);exit(EXIT_FAILURE);
	int span = (ksize-1) /2;
	float con = sqrt(2*PI)*sigma;

	double* kernel = (double*) malloc(ksize*sizeof(double));
	int x,index=0;
	for(x=span*-1; x<=span; x++){
		kernel[index]  = exp((-1.0*(double)x*(double)x)/(2.0*sigma*sigma))/con;
		index++;
	}
	return kernel;
}


float* meankernel(int ksize)
{
	if(ksize%2==0){printf("%i\n", ksize%2);exit(EXIT_FAILURE);}
//	int span = (ksize-1) /2;
	float* kernel = (float*) malloc(ksize*sizeof(float));
	int x;	
	float sum=0;
	for(x=0; x<ksize; x++){
		kernel[x]=1.0/(float)ksize;
		sum+=kernel[x];
	        //printf(" %f ",kernel[x]);
	}
	//printf("\n %f \n",sum);
	return kernel;
}

double* meankerneld(int ksize)
{
	if(ksize%2==0){
		fprintf(stderr, "%i\n", ksize%2);
		exit(EXIT_FAILURE);
	}

	double* kernel = (double*) malloc(ksize*sizeof(double));
	int x;
	for(x=0; x<ksize; x++)
		kernel[x]=1.0/(double)ksize;

	return kernel;
}


void convolve(float* function, int flength, float* kernel, int klength)
{
	int span=(klength-1)/2;
	int plength = (flength+(2*span));
	float* panel = (float*) malloc( plength * sizeof(float));
	memcpy(&panel[span],&function[0],sizeof(float)*flength);
	int i;
	for(i=0;i<span;i++){panel[i]=function[0];}
	for(i=plength-span;i<plength;i++){panel[i]=function[flength-1];}
	int upperspan;
	int lowerspan;
	int indexConv;
	float ConvReslt;
	int cc;
	float* D = (float*) malloc(sizeof(float)*klength);
	for(i=span; i<(plength-span);i++){
	    memset(D,'\0',klength);
		indexConv=0;
            ConvReslt=0;
            upperspan=i-span;
            lowerspan=i+span;
            for(cc=upperspan; cc<=lowerspan; cc++){
                D[indexConv]=(panel[cc] * kernel[indexConv]);
                ConvReslt=ConvReslt+D[indexConv];
                indexConv++;
            }
		function[i-span]=ConvReslt;
	}
	free(D);
	free(panel);
}

void convolved(double* function, int flength, double* kernel, int klength)
{
	int span=(klength-1)/2;
	int plength = (flength+(2*span));
	double* panel = (double*) malloc( plength * sizeof(double));
	memcpy(&panel[span],&function[0],sizeof(double)*flength);
	int i;
	for(i=0;i<span;i++){panel[i]=function[0];}
	for(i=plength-span;i<plength;i++){panel[i]=function[flength-1];}
	int upperspan;
	int lowerspan;
	int indexConv;
	double ConvReslt;
	int cc;
	double* D = (double*) malloc(sizeof(double)*klength);
	for(i=span; i<(plength-span);i++){
	    memset(D,'\0',klength);
		indexConv=0;
            ConvReslt=0;
            upperspan=i-span;
            lowerspan=i+span;
            for(cc=upperspan; cc<=lowerspan; cc++){
                D[indexConv]=(panel[cc] * kernel[indexConv]);
                ConvReslt=ConvReslt+D[indexConv];
                indexConv++;
            }
		function[i-span]=ConvReslt;
	}
	free(D);
	free(panel);
}

void convolve_tv(float* function, int flength, float sigma, float dsigma, int klength)
{
	int span=(klength-1)/2;
	int plength = (flength+(2*span));
	float* panel = (float*) malloc( plength * sizeof(float));
	memcpy(&panel[span],&function[0],sizeof(float)*flength);
	int i;
	for(i=0;i<span;i++){panel[i]=function[0];}
	for(i=plength-span;i<plength;i++){panel[i]=function[flength-1];}
	int upperspan;
        int lowerspan;
        int indexConv;
        float ConvReslt;
	int cc;
	float* D = (float*) malloc(sizeof(float)*klength);
	float sigmacomp = sigma;
	for(i=span; i<(plength-span);i++){
	    memset(D,'\0',klength);
	    float* kernel = (float*) morletkernel(klength,sigmacomp);
            indexConv=0;
            ConvReslt=0;
            upperspan=i-span;
            lowerspan=i+span;
            for(cc=upperspan; cc<=lowerspan; cc++){
                D[indexConv]=(panel[cc] * kernel[indexConv]);
                ConvReslt=ConvReslt+D[indexConv];
                indexConv++;
            }
            function[i-span]=ConvReslt;
	    sigmacomp += dsigma;
	    free(kernel);
        }
	free(D);
	free(panel);
}

void convolven1(float** input, int n1, int n2, float* kernel, int klength)
{
	int row;
	for(row=0;row<n1;row++){
		convolve(input[row],n2,kernel,klength);
	}
}

void convolven2(float** input, int n1, int n2, float* kernel, int klength)
{
	//printf("convolven2 %i %i \n",n1,n2);
	float* panel = (float*) malloc(n1*sizeof(float));
	int col;
	for(col=0;col<n2;col++){
		getcol(input,col,panel,n1);
		convolve(panel,n1,kernel,klength);
		setcol(input,col,panel,n1);
	}
	free(panel);

}

float** transpose(float** input, int row,int col)
{
	
	float** output = alloc2float(row,col);

	int i,j;
	for(i=0;i<row;i++){
		for(j=0;j<col;j++){
			output[j][i]=input[i][j];
		}
	}
	return output;
}

void getcol(float** input, int colselect, float* coldata, int nrow)
{
	int i;
	for(i=0;i<nrow;i++){
		coldata[i]=input[i][colselect];
	}
}

void setcol(float** input, int colselect, float* coldata, int nrow)
{
	int i;
	for(i=0;i<nrow;i++){
		input[i][colselect]=coldata[i];
	}
}

void timesarray(float** data1,float** data2, float** result, int row, int col)
{
	int i,j;
	for(i=0; i<row; i++){
		for(j=0; j<col; j++){
			result[i][j]=data1[i][j]*data2[i][j];
		}
	}
}

void powarray(float** data, int row, int col)
{
	int i,j;
	for(i=0; i<row; i++){
		for(j=0; j<col; j++){
			data[i][j]*=data[i][j];
		}
	}
}

void absarray(float** data, int row, int col)
{
	int i,j;
	for(i=0; i<row; i++){
		for(j=0; j<col; j++){
			data[i][j]=abs(data[i][j]);
		}
	}
}


void constructcedtensor(float** S11, float** S22, float** S12,float** D11, float** D22, float** D12,float k, int row, int col){
	int i,j;
	float c1,c2=0.001f,alpha,eigen1,eigen2;
	float temp;
	for( i=0; i<row; i++){
		for(j=0; j<col; j++){
			alpha = (float) sqrt( pow((S11[i][j]-S22[i][j]),2) + 4* pow(S12[i][j], 2) );
			eigen1 = (float) ((0.5) * (S11[i][j] + S22[i][j] - alpha));
			eigen2 = (float) ((0.5) * (S11[i][j] + S22[i][j] + alpha));
			temp = (float) ((-1) * pow(eigen1 - eigen2, 2));
			c1 = (float) MAX(c2, 1 - exp(temp/(k*k)));
			D11[i][j]=(float) ((0.5) * (c1 + c2 + ((c2 - c1) * (S11[i][j] - S22[i][j]) / alpha)));
			D12[i][j]= ((c2-c1) * S12[i][j]) / alpha;
			D22[i][j]=(float) ((0.5) * (c1 + c2 - ((c2 - c1) * (S11[i][j] - S22[i][j]) / alpha)));
		}
	}
}

void WeickertCED(float** data,float** a,float** c, float** b,float dt, int row, int col){

		int i,j;
        float** panel = alloc2float(col,row);
        for(i=0;i<row;i++){
			for(j=0;j<col;j++){
				panel[i][j]=data[i][j];
			}
        }

        float K00,K01,K02;
        float K10,K11,K12;
        float K20,K21,K22;

		
        for(i=1; i<row-1; i++){
            for(j=1; j<col-1; j++){
                K00 = (-1) * ( (b[i-1][j] + b[i][j+1]) / 4) * panel[i-1][j+1] ;
                K01 = ((c[i][j+1]+c[i][j]) / 2) * panel[i][j+1];
                K02 = ((b[i+1][j]+b[i][j+1]) / 4) * panel[i+1][j+1];
                K10 = ((a[i-1][j] + a[i][j]) /2 ) * panel[i-1][j];
                K11 = (a[i-1][j]) + (2 * a[i][j]) + (a[i+1][j]) + (c[i-1][j]) + (2* c[i][j]) + (c[i+1][j]);
                K11 = (K11 /2.0f)*(-1)*panel[i][j];
                K12 = ((a[i+1][j] + a[i][j]) / 2) * panel[i+1][j];
                K20 = ((b[i-1][j] + b[i][j-1]) /4) * panel[i-1][j-1];
                K21 = ((c[i][j-1] + c[i][j]) / 2) * panel[i][j-1];
                K22 = (-1) * ((b[i+1][j] + b[i][j-1]) /4) * panel[i+1][j-1];
                data[i][j] = panel[i][j] + (dt * (K00 + K01 + K02 +
                                                         K10 + K11 + K12 +
                                                         K20 + K21 + K22));
            }
        }
    }

//sugather* providefldr(sugather* gatherin, int fldrselect)
//{
//	sugather* gatherout = (sugather*) malloc(sizeof(sugather));
//	gatherout->ns = gatherin->ns;
//	int count=0;
//	int i;
//	for(i=0;i<gatherin->ntraces;i++){
//	   if(gatherin->traces[i].header->fldr == fldrselect){count++;}
//	}
//	gatherout->traces = (sutrace*) malloc(count*sizeof(sutrace));
//	gatherout->ns = gatherin->ns;
//	gatherout->dt = gatherin->dt;
//	gatherout->ntraces = count;
//	count=0;
//	for(i=0;i<gatherin->ntraces;i++){
//		if(gatherin->traces[i].header->fldr == fldrselect){
//			gatherout->traces[count]=gatherin->traces[i];
//			count++;
//		}
//	}
//	return gatherout;
//}

void medianfilter(float** data, int radius,int row, int col){
        float* window = (float*) malloc(radius*radius*sizeof(float));
//        float** panel = ealloc2float(col,row);

        int edgex = (radius-1)/2;
        int edgey = (radius-1)/2;

	int x,y,cnt,fx,fy;
        for(x=edgex;x<col-edgex;x++){
            for(y=edgey;y<row-edgey;y++){
                cnt=0;
                for(fx=0;fx<radius;fx++){
                    for(fy=0;fy<radius;fy++){
                        window[cnt]=data[y + fy - edgey][x + fx - edgex];
                        cnt++;
                    }
                }
                qksort(radius*radius,window);
                data[y][x]=window[((radius*radius)-1)/2];
            }
        }

	free(window);
}

void bclip(float* data, int n, float trim)
{
	int i;
	for(i=0;i<n;i++){
		if((data[i]<trim) && (data[i]>(-trim)) ){
			data[i]=0.0;
		}	
	}
}

void bclip2(float* data,float*dataclip, int n, float trim)
{
	int i;
	for(i=0;i<n;i++){
		if((data[i]<trim) && (data[i]>(-trim)) ){
			dataclip[i]=0.0;
		}	
	}
}

void getexterma(float* derivative,float* data, int n)
{
	int i;
	for(i=0;i<n-1;i+=2){
		if(derivative[i-1]*derivative[i+1]<0){
		}else{
			data[i]=0.0;
		}
	}

}


float* oversampled(float* data, int n)
{
	float* output = (float*) malloc(2*n*sizeof(float));
	int i;
	for(i=0;i<(n*2);i++){
		output[i]=0.0;
	}
	int dex=0;
	for(i=0;i<(n*2);i+=2){
		output[i]=data[dex];
		dex++;
	}
	return output;
}

float* undersampled(float* data, int n)
{
	float* output = (float*) malloc((n/2)*sizeof(float));
	int i;
	for(i=0;i<(n/2);i++){
		output[i]=0.0;
	}
	int dex=0;
	for(i=0;i<(n/2);i++){
		output[i]=data[dex];
		dex+=2;;
	}
	return output;
}

void HaleCED(float** input,float** D11,float** D22, float** D12,
                 float alf,int cgiter, int row, int col){

	int iter=0;

	float** S = (float**) alloc2float(col,row);
	float**  R = (float**) alloc2float(col,row); // X=SOLUTION
	float**  P = (float**) alloc2float(col,row);
	float**  APJ = (float**) alloc2float(col,row);
	float**  res = (float**) alloc2float(col,row);
	float**  BTB = (float**) alloc2float(col,row);

	int i2,i1;	
	float r00=0,r01=0,r10=0,r11=0;
	for(i2=1;i2<row;i2++){
		for(i1=1;i1<col;i1++){
			r00=input[i2  ][i1  ];
			r01=input[i2  ][i1-1];
			r10=input[i2-1][i1  ];
			r11=input[i2-1][i1-1];
			BTB[i2][i1]=0.25f*(r00+r01+r10+r11);
			S[i2][i1]=0;
			R[i2][i1]=BTB[i2][i1];
           }
       }

	dot_hale(D11,D22,D12,R,S,alf,row,col);

	int i,j;
	for(i=0;i<row;i++){
		for(j=0;j<col;j++){
			res[i][j]=(input[i][j])-S[i][j];
			P[i][j]=res[i][j];
		}
	}

	float rjrj=0;
	float rj1rj1=0;
	float alpha=0;
	float apjpj=0;
	float beta=0;
        
	int ii,jj;
	for(i=0;i<cgiter;i++){
		iter++;

		rjrj = herpr(res,res,row,col);

		dot_hale(D11,D22,D12,P,APJ,alf,row,col);
                
		apjpj = herpr(APJ,P,row,col);
		alpha = rjrj / apjpj;
                
		for( ii=0;ii<row;ii++){
			for(jj=0;jj<col;jj++){
				R[ii][jj] += alpha*P[ii][jj];
				res[ii][jj]=res[ii][jj] - (alpha*APJ[ii][jj]);
			}
		}

		rj1rj1 = herpr(res,res,row,col);
		beta = rj1rj1/rjrj;

		for(ii=0;ii<row;ii++){
			for( jj=0;jj<col;jj++){
				P[ii][jj]= res[ii][jj]+ (beta*P[ii][jj]);
				}
			}
        	}

		for(ii=0;ii<row;ii++){
			memcpy(input[ii],R[ii],col*sizeof(float));
		}

		free2float(S);
		free2float(R);
		free2float(P);
		free2float(APJ);
		free2float(res);
		free2float(BTB);
    }

void dot_hale(float** D11,float** D22,float** D12, float** in, float** out, float alpha, int row, int col)
{

        float r00=0,r01=0,r10=0,r11=0;
        float e11,e12,e22;
        float rs,ra,rb,r1,r2,s1,s2,sa,sb;
		int i2,i1;
        for( i2=1;i2<row;++i2){
            for( i1=1;i1<col;++i1){
                e11 = alpha*D11[i2][i1];
                e12 = alpha*D12[i2][i1];
                e22 = alpha*D22[i2][i1];
                r00=in[i2  ][i1  ];
                r01=in[i2  ][i1-1];
                r10=in[i2-1][i1  ];
                r11=in[i2-1][i1-1];
                rs=(r00+r01+r10+r11)/4.0f;
                ra=r00-r11;
                rb=r01-r10;
                r1=ra-rb;
                r2=ra+rb;
                s1=(e11*r1)+(e12*r2);
                s2=(e12*r1)+(e22*r2);
                sa=s1+s2;
                sb=s1-s2;
                out[i2  ][i1  ]+=sa+rs;
                out[i2  ][i1-1]-=sb-rs;
                out[i2-1][i1  ]+=sb+rs;
                out[i2-1][i1-1]-=sa-rs;
            }
        }
}

float herpr(float** X, float** Y, int row, int col)
{
	float val=0;
	int i,j;
	for(i=0;i<row;i++){
		for(j=0;j<col;j++){
			val+=(X[i][j]*Y[i][j]);
		}
	}
	return val;
}

void dot_vec(float** A, float* X,float* B, int row, int col)
{
	float val=0;
	int i,j;
	for(i=0;i<row;i++){
		val=0;
		for(j=0;j<col;j++){
			val+=(A[i][j]*X[j]);
		}
		B[i]=val;
	}
}

float vec_dot_vec(float* a,float* b,int row)
{
	int i;
	float sum=0;
	for(i=0;i<row;i++){
		sum+=(a[i]*b[i]);
	}
	return sum;
}

float* extractexterma(float* data, int ns, int klength, float sigmaderiv)
{
	float* kernelderiv = (float*) gaussianderivative1kernel(klength,sigmaderiv);
	float* derivative=(float*) oversampled(data,ns);
	float* exterma=(float*) oversampled(data,ns);
	
	convolve(derivative,2*ns,kernelderiv,klength);
	getexterma(derivative,exterma, 2*ns);
	float* extermaoriginal = (float*) undersampled(exterma,2*ns);
	return extermaoriginal;
}

float* extractmaxima(float* data, int ns, int klength, float sigmaderiv)
{
	float* kernelderiv = (float*) gaussianderivative1kernel(klength,sigmaderiv);
	float* derivative=(float*) oversampled(data,ns);
	float* secondderivative=(float*) oversampled(data,ns);
	float* exterma=(float*) oversampled(data,ns);
	
	convolve(secondderivative,2*ns,kernelderiv,klength);
	convolve(secondderivative,2*ns,kernelderiv,klength);

	convolve(derivative,2*ns,kernelderiv,klength);
	getmaxima(derivative,secondderivative,exterma, 2*ns);
	float* extermaoriginal = (float*) undersampled(exterma,2*ns);
	return extermaoriginal;
}

float* extractminima(float* data, int ns, int klength, float sigmaderiv)
{
	float* kernelderiv = (float*) gaussianderivative1kernel(klength,sigmaderiv);
	float* derivative=(float*) oversampled(data,ns);
	float* secondderivative=(float*) oversampled(data,ns);
	float* exterma=(float*) oversampled(data,ns);
	
	convolve(secondderivative,2*ns,kernelderiv,klength);
	convolve(secondderivative,2*ns,kernelderiv,klength);

	convolve(derivative,2*ns,kernelderiv,klength);
	getminima(derivative,secondderivative,exterma, 2*ns);
	float* extermaoriginal = (float*) undersampled(exterma,2*ns);
	return extermaoriginal;
}

void getmaxima(float* derivative,float* secondderivative,float* data, int n)
{
	int i;
	for(i=0;i<n-1;i+=2){
		if(derivative[i-1]*derivative[i+1]<0){
			if(secondderivative[i]<0){
			}else{
				data[i]=0.0;
			}
		}else{
			data[i]=0.0;
		}
	}
}

void getminima(float* derivative,float* secondderivative,float* data, int n)
{
	int i;
	for(i=0;i<n-1;i+=2){
		if(derivative[i-1]*derivative[i+1]<0){
			if(secondderivative[i]>0){

			}else{
				data[i]=0.0;
			}
		}else{
			data[i]=0.0;
		}
	}
}

int nonzerocount(float* data, int n)
{
	int i;
	int count=0;
	for(i=0;i<n;i++){
		if(data[i]!=0.0){
			count++;
		}
	}
	return count;
}

float gaussian(float beta, float x1, float x2)
{
	return exp(-1*beta*(x2-x1)*(x2-x1));
}

float tpl(float order, float x1, float x2)
{
	
	float r=abs(x2-x1);
	float ret = pow(r,order)*log(r);
	printf(" %f %f %f %f \n",r,ret,pow(r,order),log(r));
	return ret;
}
float rorder(float order, float x1, float x2)
{
	
	float r=abs(x2-x1);
	float ret = pow(r,order);
	return ret;
}

float mq(float beta, float x1, float x2)
{
	float r=abs(x2-x1);
	return sqrt(1 + (beta*r*r));

}

float imq(float beta, float x1, float x2)
{
	float r=abs(x2-x1);
	return 1.0/(1.0 + (beta*r*r));

}

void nonzeroextract(float* data, int n,float* y, float* x)
{
	int i;
	int count=0;
	for(i=0;i<n;i++){
		if(data[i]!=0.0){
			y[count]=data[i];
			x[count]=i;
			count++;
		}
	}
}

void cg(float** A, float* X, float* B, int row, int col,int cgiter)
{
	int i;
	float* R = (float*) malloc(row*sizeof(float));
	float* P = (float*) malloc(row*sizeof(float));

	float* APJ = (float*) malloc(row*sizeof(float));

	dot_vec(A,X,R,row,col);

	for(i=0;i<row;i++){
		R[i] = B[i] - R[i];
		P[i] = R[i];
		
	}
	
	int iter;
	for(iter=0;iter<cgiter;iter++){
		float rjrj = vec_dot_vec(R,R,row);
		
		dot_vec(A, P,APJ,  row, col);
		float apjpj = vec_dot_vec(P,APJ,row);

		float alpha = rjrj/apjpj;

		for(i=0;i<row;i++){
			X[i]+=alpha*P[i];
			R[i]-=alpha*APJ[i];
		}

		float rjirji= vec_dot_vec(R,R,row);

		float beta = rjirji/rjrj;
		for(i=0;i<row;i++){
			P[i]=R[i]+ (beta*P[i]);
		}
		
	}
}

float Min(float* data, int n){
	float min = data[0];
	int i;
	for(i=0; i<n; i++){
		if(data[i] < min){
			min = data[i];
		}
	}
	return min;
}

float Max(float* data, int n){
	float max = data[0];
	int i;
	for(i=0; i<n; i++){
		if(data[i] > max)
		max = data[i];
	}
	return max;
}

void osfmax(float* output,float* data, int n, int w)
{
	memset(output,'\0',n);
	float* window = (float*) malloc(w*sizeof(float));
	int span = (w-1) /2;
	
	int i,j,dex;
	
	for(i=span;i<n-span;i++){
		memset(window,'\0',w);
		dex=0;
		for(j=i-span;j<=i+span;j++){
			window[dex]=data[j];
			dex++;
		}
		qksort(w,window);
		output[i]=window[w-1];
	}
	free(window);
}

void osfmaxd(double* output,double* data, int n, int w)
{
	float* window = (float*) malloc(w*sizeof(float));
	int span = (w-1) /2;

	int i,j,dex;

	for(i=span;i<n-span;i++){
		dex=0;
		for(j=i-span;j<=i+span;j++)
		{
			window[dex]=(float)data[j];
			dex++;
		}
		qksort(w,window);
		output[i]=(double)window[w-1];
	}
	free(window);
}

void osfmaxdn(double* output,double* function, int flength, int klength)
{
	int i;
	int upperspan;
	int lowerspan;
	int indexConv;
	int cc;

	int span=(klength-1)/2;
	int plength = (flength+(2*span));
	float* panel = (float*) malloc( plength * sizeof(float));
	for(i=0; i<flength; i++) panel[i+span] = (float) function[i];

	for(i=0;i<span;i++)	panel[i] = (float) function[0];
	for(i=plength-span;i<plength;i++)	panel[i]=(float) function[flength-1];

	float* D = (float*) malloc(sizeof(float)*klength);
	for(i=span; i<(plength-span);i++)
	{
		indexConv=0;
		upperspan=i-span;
		lowerspan=i+span;
		for(cc=upperspan; cc<=lowerspan; cc++)
		{
			D[indexConv]=(panel[cc]);
			indexConv++;
		}
		qksort(klength,D);
		output[i-span] = (double) D[klength-1];
	}
	free(D);
	free(panel);
}

void osfmin(float* output,float* data, int n, int w)
{
	memset(output,'\0',n);
	float* window = (float*) malloc(w*sizeof(float));
	int span = (w-1) /2;
	
	int i,j,dex;
	
	for(i=span;i<n-span;i++){
		memset(window,'\0',w);
		dex=0;
		for(j=i-span;j<=i+span;j++){
			window[dex]=data[j];
			dex++;
		}
		qksort(w,window);
		output[i]=window[0];
	}
	free(window);
}

void osfmind(double* output,double* data, int n, int w)
{
	float* window = (float*) malloc(w*sizeof(float));
	int span = (w-1) /2;

	int i,j,dex;

	for(i=span;i<n-span;i++){
		dex=0;
		for(j=i-span;j<=i+span;j++){
			window[dex]=(float)data[j];
			dex++;
		}
		qksort(w,window);
		output[i]=(double)window[0];
	}
	free(window);
}

void osfmindn(double* output,double* function, int flength, int klength)
{
	int i;
	int upperspan;
	int lowerspan;
	int indexConv;
	int cc;

	int span=(klength-1)/2;
	int plength = (flength+(2*span));
	float* panel = (float*) malloc( plength * sizeof(float));
	for(i=0; i<flength; i++) panel[i+span] = (float) function[i];

	for(i=0;i<span;i++)		panel[i]=function[0];
	for(i=plength-span;i<plength;i++)	panel[i]=(float)function[flength-1];

	float* D = (float*) malloc(sizeof(float)*klength);
	for(i=span; i<(plength-span);i++)
	{
		indexConv=0;
		upperspan=i-span;
		lowerspan=i+span;
		for(cc=upperspan; cc<=lowerspan; cc++){
			D[indexConv]=(panel[cc]);
			indexConv++;
		}
		qksort(klength,D);
		output[i-span]= (double) D[0];
	}
	free(D);
	free(panel);
}

void /*float* */emdextract(double* tracedata1, int ns, int div, float divg, int klength, float sigmaderiv)
{
	int i, j, k;

	//convert data to double
	float *tracedata = (float*) malloc(ns*sizeof(float));
	for(i=0; i<ns; i++) tracedata[i] = (float) tracedata1[i];

	//extract maxima
	float* maxima = extractmaxima(tracedata, ns, klength, sigmaderiv);
	int countmax = nonzerocount(maxima,ns); //count maxima data

	//allocate maximum data x and y
	float* ymax = (float*) malloc(countmax*sizeof(float));
	float* xmax = (float*) malloc(countmax*sizeof(float));
	float* distancemax = (float*) malloc(countmax*sizeof(float));

	//crate maxima x and y data
	nonzeroextract(maxima,ns,ymax,xmax);

	for(j=0;j<countmax-1;j++)	distancemax[j]=(xmax[j+1]-xmax[j]); //count distance

	//get maximum windowing length
	distancemax[countmax-1]=distancemax[countmax-2];
	int wmax = ceil(Max(distancemax,countmax)/div);
	if(wmax%2==0){wmax+=1;}

	// create envelope maximum and convolution with kernel max
	float* uenvelope = (float*) malloc(ns*sizeof(float));
	float* meanmax= (float*) meankernel(wmax);
	osfmax(uenvelope,tracedata,ns,wmax);
	convolve(uenvelope,ns,meanmax,wmax);

	//------------------------------------------------------------------------
	//extract minima
	float* minima = extractminima(tracedata, ns, klength, sigmaderiv);
	int countmin = nonzerocount(minima,ns);

	//allocate minimum data x and y
	float* ymin = (float*) malloc(countmin*sizeof(float));
	float* xmin = (float*) malloc(countmin*sizeof(float));
	float* distancemin = (float*) malloc(countmin*sizeof(float));

	//crate minima x and y data
	nonzeroextract(minima,ns,ymin,xmin);
	
	for(k=0;k<countmin-1;k++) distancemin[k]=(xmin[k+1]-xmin[k]); //count distance

	//get maximum windowing length
	distancemin[countmin-1]=distancemin[countmin-2];
	int wmin = ceil(Max(distancemin,countmin)/div);
	if(wmin%2==0) wmin+=1;

	// create envelope minimum and convolution with kernel min
	float* meanmin= (float*) meankernel(wmin);
	float* lenvelope = (float*) malloc(ns*sizeof(float));
	osfmin(lenvelope,tracedata,ns,wmin);
	convolve(lenvelope,ns,meanmin,wmin);

	//compute mean data
	for(i=0;i<ns;i++)
		tracedata1[i]=(double) ((uenvelope[i]+lenvelope[i])/2);


	free(maxima);
	free(ymax);
	free(xmax);
	free(distancemax);
	free(lenvelope);
	free(minima);
	free(ymin);
	free(xmin);
	free(distancemin);
	free(uenvelope);
	free(tracedata);
}
