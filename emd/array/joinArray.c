/*
 * joinArray.c
 *
 *  Created on: May 24, 2010
 *      Author: toto
 */
#include "joinArray.h"

/*merge array*/
int *join2array_int(int *data1, int *data2, int ndata1, int ndata2)
{
	int *out=NULL;
	int newN;

	newN = ndata1 + ndata2;
	if(newN>0)
		out = su_ealloc1int(newN);

	if((ndata1>0) && (ndata2>0))
	{
		memcpy(out, data1, ndata1*sizeof(int));
		memcpy(out+ndata1, data2, ndata2*sizeof(int));
	}
	else if((ndata1 > 0) && (ndata2<=0))
	{
		memcpy(out, data1, ndata1*sizeof(int));
	}
	else if((ndata1<=0) && ((ndata2>0)))
	{
		memcpy(out, data2, ndata2*sizeof(int));
	}

	return(out);
}

int *join3array_int(int *data1, int *data2, int *data3, int ndata1, int ndata2, int ndata3)
{
	int *out;
	int n, tmp1, tmp2;

	n = ndata1 + ndata2 + ndata3;
	out = su_ealloc1int(n);
	tmp1 = 0;
	tmp2 = ndata1;
	memcpy(out+tmp1, data1, tmp2*sizeof(int));

	tmp1 = ndata1;
	tmp2 = ndata2;
	memcpy(out+tmp1, data2, tmp2*sizeof(int));

	tmp1 = ndata1 + ndata2;
	tmp2 = ndata3;
	memcpy(out+tmp1, data3, tmp2*sizeof(int));

	return out;
}

double *join2array_double(double *data1, double *data2, int ndata1, int ndata2)
{
	double *out;
	int newN;

	newN = ndata1 + ndata2;
	out = su_ealloc1double(newN);

	if((ndata1>0) && (ndata2>0))
	{
		memcpy(out, data1, ndata1*sizeof(double));
		memcpy(out+ndata1, data2, ndata2*sizeof(double));
	}
	else if((ndata1 > 0) && (ndata2<0))
	{
		memcpy(out, data1, ndata1*sizeof(double));
	}
	else if((ndata1<0) && ((ndata2>0)))
	{
		memcpy(out, data2, ndata2*sizeof(double));
	}

	return(out);
}

double *join3array_double(double *data1, double *data2, double *data3, int ndata1, int ndata2, int ndata3)
{
	double *out;
	int n, tmp1, tmp2;

	n = ndata1 + ndata2 + ndata3;
	out = su_ealloc1double(n);
	tmp1 = 0;
	tmp2 = ndata1;
	memcpy(out+tmp1, data1, tmp2*sizeof(double));

	tmp1 = ndata1;
	tmp2 = ndata2;
	memcpy(out+tmp1, data2, tmp2*sizeof(double));

	tmp1 = ndata1 + ndata2;
	tmp2 = ndata3;
	memcpy(out+tmp1, data3, tmp2*sizeof(double));

	return out;
}
