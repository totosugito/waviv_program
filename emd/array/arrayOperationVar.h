/*
 * arrayOperationVar.h
 *
 *  Created on: May 24, 2010
 *      Author: toto
 */

#ifndef ARRAYOPERATIONVAR_H_
#define ARRAYOPERATIONVAR_H_

#include "su_ealloc.h"
#include <stdio.h>

int *arrayOperationVar_int(int *x, int nx, int var, int type);
void arrayOperationVarv_int(int *x, int nx, int var, int type);

float *arrayOperationVar_float(float *x, int nx, float var, int type);

double *arrayOperationVar_int2d(int *x, int nx, double var, int type);
double *arrayOperationVar_double(double *x, int nx, double var, int type);
void arrayOperationVarv_double(double *x, int nx, double var, int type);
#endif /* ARRAYOPERATIONVAR_H_ */
