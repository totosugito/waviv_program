/*
 * getMidArray.h
 *
 *  Created on: Jun 14, 2010
 *      Author: toto
 */

#ifndef GETMIDARRAY_H_
#define GETMIDARRAY_H_
#include "su_ealloc.h"

double *getmidArray_double(double *data1, double *data2, int nx);
#endif /* GETMIDARRAY_H_ */
