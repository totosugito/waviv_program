/*
 * checkarray.c
 *
 *  Created on: May 24, 2010
 *      Author: toto
 */

#include "checkarray.h"

/*type = type of operation
		0 = equal
		1 = less than
		2 = more than
		3 = less or equal
		4 = more or equal*/
int *cekArray_double(double *data, int ndata, double ceknumb, int type)
{
	int i;
	int *out;

	out = su_ealloc1int(ndata);
	for (i=0; i<ndata; i++)
	{
		switch (type)
		{
		case 0:
			if(data[i]==ceknumb)
				out[i] = 1;
			else
				out[i] = 0;
			break;

		case 1:
			if(data[i]<ceknumb)
				out[i] = 1;
			else
				out[i] = 0;
			break;
		case 2:
			if(data[i]>ceknumb)
				out[i] = 1;
			else
				out[i] = 0;
			break;
		case 3:
			if(data[i]<=ceknumb)
				out[i] = 1;
			else
				out[i] = 0;
			break;
		case 4:
			if(data[i]>=ceknumb)
				out[i] = 1;
			else
				out[i] = 0;
			break;
		}
	}

	return(out);
}

int *cekArray_int(int *data, int ndata, int ceknumb, int type)
{
	int i;
	int *out;

	out = su_ealloc1int(ndata);
	for (i=0; i<ndata; i++)
	{
		switch (type)
		{
		case 0:
			if(data[i]==ceknumb)
				out[i] = 1;
			else
				out[i] = 0;
			break;

		case 1:
			if(data[i]<ceknumb)
				out[i] = 1;
			else
				out[i] = 0;
			break;
		case 2:
			if(data[i]>ceknumb)
				out[i] = 1;
			else
				out[i] = 0;
			break;
		case 3:
			if(data[i]<=ceknumb)
				out[i] = 1;
			else
				out[i] = 0;
			break;
		case 4:
			if(data[i]>=ceknumb)
				out[i] = 1;
			else
				out[i] = 0;
			break;
		}
	}

	return(out);
}

