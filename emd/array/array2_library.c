/*
 * array2_library.cpp
 *
 *  Created on: Apr 20, 2010
 *      Author: toto
 */

#include "array2_library.h"

//
////float **array2_create(int row, int col)
////{
////	int i;
////	float **out;
////
////	out = (float*) calloc(row, sizeof(float));
////	out[0] = new float[row*col];
////	for(i=1; i<row; i++)
////	{
////		out->data[i] = out->data[i-1] + col;
////	}
////	out->row = row;
////	out->col = col;
////	return out;
////}
//
////bool array2_free(float **array)
////{
////	free array[0];
////	free array;
//////	array->row = 0;
//////	array->col = 0;
//////
//////	delete array;
////	return true;
////}

void array2t_checksize(int row, int col, int row_from, int row_to,
		int col_from, int col_to)
{
	//check row size
	if((row_from<=0) || (row_to>row) || (row_from>row_to))
	{
		fprintf(stderr, "array2_checksize : Size row in outside array size from : %i, to=%i, size=%i\n",
				row_from, row_to, row);
		exit(0);
	}
	//check column size
	if((col_from<=0) || (col_to>col) || (col_from>col_to))
	{
		fprintf(stderr, "array2_checksize : Size colum in outside array size : from : %i, to=%i, size=%i\n",
				col_from, col_to, col);
		exit(0);
	}
}

//void array2_print(float **A, int row, int col, int row_from, int row_to,
//		int col_from, int col_to, bool type)
//{
//	int i,j, k;
//	//check input size array
//	array2_checksize(row, col, row_from, row_to, col_from, col_to);
//
//	//set index array
//	//minimum = 1, maximum=n
//	row_from = row_from - 1;
//	row_to = row_to - 1;
//	col_from = col_from - 1;
//	col_to = col_to - 1;
//	k = 1;
//	for(i=row_from; i<=row_to; i++)
//	{
//		for(j=col_from; j<=col_to; j++)
//		{
//			if(type)
//				printf("%5.5f \t", A[i][j]);
//			else
//				printf("%i. \t %5.5f \n", k, A[i][j]);
//			k = k+1;
//		}
//		if(type)
//			printf("\n");
//	}
//
//}
//
void array2_printd(double **A, int row, int col, int row_from, int row_to,
		int col_from, int col_to, bool type)
{
	int i,j, k;
	//check input size array
	array2t_checksize(row, col, row_from, row_to, col_from, col_to);

	//set index array
	//minimum = 1, maximum=n
	row_from = row_from - 1;
	row_to = row_to - 1;
	col_from = col_from - 1;
	col_to = col_to - 1;
	k = 1;
	for(i=row_from; i<=row_to; i++)
	{
		for(j=col_from; j<=col_to; j++)
		{
			if(type)
				printf("%5.5f \t", A[i][j]);
			else
				printf("%i. \t %5.5f \n", k, A[i][j]);
			k = k+1;
		}
		if(type)
			printf("\n");
	}
}

//float *array2_getvect_float(float **A, int row, int col, int row_from, int row_to,
//		int col_from, int col_to)
//{
//	float *out=NULL;
//	int i,j, k;
//
//	//check input size array
//	array2_checksize(row, col, row_from, row_to, col_from, col_to);
//
//	//set index array
//	//minimum = 1, maximum=n
//	row_from = row_from - 1;
//	row_to = row_to - 1;
//	col_from = col_from - 1;
//	col_to = col_to - 1;
//	k = 0;
//	if(row_from==row_to)
//	{
////		out = (float*) calloc(col_to - col_from +1, sizeof(float));
//		out = ealloc1float(col_to - col_from +1);
//		for(i=row_from; i<=row_to; i++)
//		{
//			for(j=col_from; j<=col_to; j++)
//			{
//				out[k] = A[i][j];
//				k = k+1;
//			}
//		}
//		return out;
//	}
//	else if(col_from==col_to)
//	{
////		out = (float*) calloc(row_to - row_from + 1, sizeof(float));
//		out = ealloc1float(row_to - row_from + 1);
//		for(i=row_from; i<=row_to; i++)
//		{
//			for(j=col_from; j<=col_to; j++)
//			{
//				out[k] = A[i][j];
//				k = k+1;
//			}
//		}
//		return out;
//	}
//	else
//	{
//		fprintf(stderr, "array2_getvect_float : output must be a vector, input row_from==row_to or col_from==col_to\n");
//		exit(0);
//	}
//}
//
//float **array2_transpose(float **array, int row, int col, int *rowout, int *colout)
//{
//	float **out;
//	int i,j;
//	int m,n;
//
//	m = row;
//	n = col;
//	out = ealloc2float(m,n);
//	(*rowout) = n;
//	(*colout) = m;
//	for(i=0; i<m; i++)
//	{
//		for(j=0; j<n; j++)
//		{
//			out[j][i] = array[i][j];
//		}
//	}
//	return out;
//}
//
//float **array2_join3d(float **array1, int r1, int c1, float **array2, int r2, int c2,
//		float **array3, int r3, int c3, bool type, int *r4, int *c4)
//{
//	float **out=NULL;
//	int i,j, k;
//	int row,col;
//
//	if(type == 0)
//	{
//		col = c1;
//		row = r1+r2+r3;
//	}
//	else
//	{
//		row = r1;
//		col = c1+c2+c3;
//	}
//
//	(*r4) = row;
//	(*c4) = col;
//	out = ealloc2float(col, row);
//	if(type==0)
//	{
//		//process array I
//		k = 0;
//		for(i=0; i<r1; i++)
//		{
//			for(j=0; j<c1; j++)
//			{
//				out[k][j] = array1[i][j];
//			}
//			k = k+1;
//		}
//
//		//process array II
//		for(i=0; i<r2; i++)
//		{
//			for(j=0; j<c2; j++)
//			{
//				out[k][j] = array2[i][j];
//			}
//			k = k+1;
//		}
//
//		//process array III
//		for(i=0; i<r3; i++)
//		{
//			for(j=0; j<c3; j++)
//			{
//				out[k][j] = array3[i][j];
//			}
//			k = k+1;
//		}
//	}
//	else
//	{
//		//process array I
//		k = 0;
//		for(i=0; i<c1; i++)
//		{
//			for(j=0; j<r1; j++)
//			{
//				out[j][k] = array1[j][i];
//			}
//			k = k+1;
//		}
//
//		//process array II
//		for(i=0; i<c2; i++)
//		{
//			for(j=0; j<r2; j++)
//			{
//				out[j][k] = array2[j][i];
//			}
//			k = k+1;
//		}
//
//		//process array III
//		for(i=0; i<c3; i++)
//		{
//			for(j=0; j<r3; j++)
//			{
//				out[j][k] = array3[j][i];
//			}
//			k = k+1;
//		}
//	}
//
//	return out;
//}
//
//float **array2_arithm(float **A, float **B, int row, int col, short type)
//{
//	// array process with array
//	// type 0 : sum array A and B
//	//      1 : substract array A and B
//	//      2 : multiply array A and B
//	//      3 : divide array A and B
//	int i,j;
//	float **out;
//
//	out = ealloc2float(col, row);
//	for(i=0; i<row; i++)
//	{
//		for(j=0; j<col; j++)
//		{
//			switch (type)
//			{
//				case 0:  //add  operation
//					out[i][j] = A[i][j] + B[i][j];
//					break;
//				case 1:  //substract  operation
//					out[i][j] = A[i][j] - B[i][j];
//					break;
//				case 2:  //multiply  operation
//					out[i][j] = A[i][j] * B[i][j];
//					break;
//				case 3:  //divide  operation
//					out[i][j] = A[i][j] / B[i][j];
//					break;
//				default: //add  operation
//					out[i][j] = A[i][j] + B[i][j];
//					break;
//			}
//		}
//	}
//	return out;
//}

//20 Mei 2010
double *array2_getvect_double(double **A, int row, int col, int row_from, int row_to,
		int col_from, int col_to)
{
	double *out=NULL;
	int i,j, k;

	//check input size array
	array2t_checksize(row, col, row_from, row_to, col_from, col_to);

	//set index array
	//minimum = 1, maximum=n
	row_from = row_from - 1;
	row_to = row_to - 1;
	col_from = col_from - 1;
	col_to = col_to - 1;
	k = 0;
	if(row_from==row_to)
	{
//		out = (float*) calloc(col_to - col_from +1, sizeof(float));
		out = su_ealloc1double(col_to - col_from +1);
		for(i=row_from; i<=row_to; i++)
		{
			for(j=col_from; j<=col_to; j++)
			{
				out[k] = A[i][j];
				k = k+1;
			}
		}
		return out;
	}
	else if(col_from==col_to)
	{
//		out = (float*) calloc(row_to - row_from + 1, sizeof(float));
		out = su_ealloc1double(row_to - row_from + 1);
		for(i=row_from; i<=row_to; i++)
		{
			for(j=col_from; j<=col_to; j++)
			{
				out[k] = A[i][j];
				k = k+1;
			}
		}
		return out;
	}
	else
	{
		fprintf(stderr, "array2_getvect_double : output must be a vector, input row_from==row_to or col_from==col_to\n");
		exit(0);
	}
}
