/*
 * arrayOperationVar.c
 *
 *  Created on: May 24, 2010
 *      Author: toto
 */

#include "arrayOperationVar.h"

/* type = 1 --> operasi penjumlahan
 * type = 2 --> operasi pengurangan
 * type = 3 --> opearasi perkalian
 * type = 4 --> operasi pembagian
 */
int *arrayOperationVar_int(int *x, int nx, int var, int type)
{
	int i;
	int *out=NULL;

	if(nx<=0)
	{
		fprintf(stderr, "arrayOperationVar_int : length input array is %i\n", nx);
		exit(0);
	}

	out = su_ealloc1int(nx);
	for(i=0; i<nx; i++)
	{
		switch(type)
		{
		case 1:
			out[i] = x[i] + var;
			break;
		case 2:
			out[i] = x[i] - var;
			break;
		case 3:
			out[i] = x[i] * var;
			break;
		case 4:
			out[i] = x[i] / var;
			break;
		}
	}
	return(out);
}

/* type = 1 --> operasi penjumlahan
 * type = 2 --> operasi pengurangan
 * type = 3 --> opearasi perkalian
 * type = 4 --> operasi pembagian
 */
void arrayOperationVarv_int(int *x, int nx, int var, int type)
{
	int i;

	if(nx<=0)
	{
		fprintf(stderr, "arrayOperationVar_int : length input array is %i\n", nx);
		exit(0);
	}

	for(i=0; i<nx; i++)
	{
		switch(type)
		{
		case 1:
			x[i] += var;
			break;
		case 2:
			x[i] -= var;
			break;
		case 3:
			x[i] *= var;
			break;
		case 4:
			x[i] /= var;
			break;
		}
	}
}

double *arrayOperationVar_int2d(int *x, int nx, double var, int type)
{
	int i;
	double *out=NULL;

	if(nx<=0)
	{
		fprintf(stderr, "arrayOperationVar_int : length input array is %i\n", nx);
		exit(0);
	}

	out = su_ealloc1double(nx);
	for(i=0; i<nx; i++)
	{
		switch(type)
		{
		case 1:
			out[i] = (double) x[i] + var;
			break;
		case 2:
			out[i] = (double) x[i] - var;
			break;
		case 3:
			out[i] = (double) x[i] * var;
			break;
		case 4:
			out[i] = (double) x[i] / var;
			break;
		}
	}
	return(out);
}

float *arrayOperationVar_float(float *x, int nx, float var, int type)
{
	int i;
	float *out=NULL;

	if(nx<=0)
	{
		fprintf(stderr, "arrayOperationVar_float : length input array is %i\n", nx);
		exit(0);
	}

	out = su_ealloc1float(nx);
	for(i=0; i<nx; i++)
	{
		switch(type)
		{
		case 1:
			out[i] = x[i] + var;
			break;
		case 2:
			out[i] = x[i] - var;
			break;
		case 3:
			out[i] = x[i] * var;
			break;
		case 4:
			out[i] = x[i] / var;
			break;
		}
	}
	return(out);
}

double *arrayOperationVar_double(double *x, int nx, double var, int type)
{
	int i;
	double *out=NULL;

	if(nx<=0)
	{
		fprintf(stderr, "arrayOperationVar_double : length input array is %i\n", nx);
		exit(0);
	}

	out = su_ealloc1double(nx);
	for(i=0; i<nx; i++)
	{
		switch(type)
		{
		case 1:
			out[i] = x[i] + var;
			break;
		case 2:
			out[i] = x[i] - var;
			break;
		case 3:
			out[i] = x[i] * var;
			break;
		case 4:
			out[i] = x[i] / var;
			break;
		}
	}
	return(out);
}

void arrayOperationVarv_double(double *x, int nx, double var, int type)
{
	int i;

	if(nx<=0)
	{
		fprintf(stderr, "arrayOperationVar_double : length input array is %i\n", nx);
		exit(0);
	}

	for(i=0; i<nx; i++)
	{
		switch(type)
		{
		case 1:
			x[i] += var;
			break;
		case 2:
			x[i] -= var;
			break;
		case 3:
			x[i] *= var;
			break;
		case 4:
			x[i] /= var;
			break;
		}
	}
}
