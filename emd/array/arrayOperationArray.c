/*
 * arrayOperationArray.c
 *
 *  Created on: May 24, 2010
 *      Author: toto
 */

#include "arrayOperationArray.h"

/* type = 1 --> operasi penjumlahan
 * type = 2 --> operasi pengurangan
 * type = 3 --> opearasi perkalian
 * type = 4 --> operasi pembagian
 */
float *arrayOperationArrayo_float(float *data1, float *data2, int ndata,int type)
{
	int i;
	float *out;

	out=su_ealloc1float(ndata);
	for (i=0; i<ndata; i++)
	{
		switch(type)
		{
		case 1:
			out[i] = data1[i] + data2[i];
			break;
		case 2:
			out[i] = data1[i] - data2[i];
			break;
		case 3:
			out[i] = data1[i] * data2[i];
			break;
		case 4:
			out[i] = data1[i] / data2[i];
			break;
		}
	}
	return out;
}

/* type = 1 --> operasi penjumlahan
 * type = 2 --> operasi pengurangan
 * type = 3 --> opearasi perkalian
 * type = 4 --> operasi pembagian
 */
void arrayOperationArray_double(double *data1, double *data2, int ndata,int type)
{
	int i;

	for (i=0; i<ndata; i++)
	{
		switch(type)
		{
		case 1:
			data1[i] += data2[i];
			break;
		case 2:
			data1[i] -= data2[i];
			break;
		case 3:
			data1[i] *= data2[i];
			break;
		case 4:
			data1[i] /= data2[i];
			break;
		}
	}
}

/* type = 1 --> operasi penjumlahan
 * type = 2 --> operasi pengurangan
 * type = 3 --> opearasi perkalian
 * type = 4 --> operasi pembagian
 */
double *arrayOperationArrayo_double(double *data1, double *data2, int ndata,int type)
{
	int i;
	double *out;

	out=su_ealloc1double(ndata);
	for (i=0; i<ndata; i++)
	{
		switch(type)
		{
		case 1:
			out[i] = data1[i] + data2[i];
			break;
		case 2:
			out[i] = data1[i] - data2[i];
			break;
		case 3:
			out[i] = data1[i] * data2[i];
			break;
		case 4:
			out[i] = data1[i] / data2[i];
			break;
		}
	}
	return out;
}

void arrayOperationArray_int(int *data1, int *data2, int ndata,int type)
{
	int i;

	for (i=0; i<ndata; i++)
	{
		switch(type)
		{
		case 1:
			data1[i] += data2[i];
			break;
		case 2:
			data1[i] -= data2[i];
			break;
		case 3:
			data1[i] *= data2[i];
			break;
		case 4:
			data1[i] /= data2[i];
			break;
		}
	}
}

double *arrayMid_double(double *data1, double *data2, int nx)
{
	int i;
	double *out=NULL;

	out = su_ealloc1double(nx);
	for (i=0; i<nx; i++)
	{
		out[i] = (data1[i] + data2[i]) / 2;
	}
	return out;
}

void arrayMidv_double(double *data1, double *data2, int nx)
{
	int i;

	for (i=0; i<nx; i++)
	{
		data1[i] = (data1[i] + data2[i]) / 2;
	}
}
