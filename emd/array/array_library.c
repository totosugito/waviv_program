/*
 * array_library.c
 *
 *  Created on: Apr 20, 2010
 *      Author: toto
 */

#include "array_library.h"


void array_checksize(int len, int from, int to)
{
	//check row size
	if((from<=0) || (to>len) || (from>to))
	{
		fprintf(stderr, "Size row in outside array size from : %i, to=%i, size=%i\n",
				from, to, len);
		exit(0);
	}
}

void array_printd(double *V, int len, int from, int to, short type)
{
	int i,k;
	//check input size array
	array_checksize(len, from, to);

	//set index array
	//minimum = 1, maximum=n
	from = from - 1;
	to = to - 1;

	k = 1;
	for(i=from; i<=to; i++)
	{
		if(type==1)
			printf("[%i]. %5.5f \t", k, V[i]);
		else if(type==0)
			printf("[%i]. \t %5.5f \n", k, V[i]);
		else if(type==2)
			printf("%5.5f \t",V[i]);
		k = k+1;
	}
	if(type)
		printf("\n");
}

void array_printf(float *V, int len, int from, int to, bool type)
{
	int i,k;
	//check input size array
	array_checksize(len, from, to);

	//set index array
	//minimum = 1, maximum=n
	from = from - 1;
	to = to - 1;

	k = 1;
	for(i=from; i<=to; i++)
	{
		if(type)
			printf("[%i]. %5.5f \t", k, V[i]);
		else
			printf("[%i]. \t %5.5f \n", k, V[i]);
		k = k+1;
	}
	if(type)
		printf("\n");
}

void array_printi(int *V, int len, int from, int to, bool type)
{
	int i,k;
	//check input size array
	array_checksize(len, from, to);

	//set index array
	//minimum = 1, maximum=n
	from = from - 1;
	to = to - 1;

	k = 1;
	for(i=from; i<=to; i++)
	{
		if(type)
			printf("[%i]. %i \t", k, V[i]);
		else
			printf("[%i]. \t %i \n", k, V[i]);
		k = k+1;
	}
	if(type)
		printf("\n");
}

float array_mean(float *V, int len)
{
	int i, n;
	float out2;

	out2 = 0.0;
	n = len;
	for(i=0; i<n; i++)
		out2 = out2 + V[i];

	out2 = out2/n;

	return out2;
}

void array_sum(float *X, int nx, float *Y, int ny)
{
	int i;

	for (i=0; i<nx; i++)
	{
		X[i] = X[i] + Y[i];
	}
}

float *array_create_val(float t0, float dt, float t1, int *len)
{
	float *out;
	int i;
	int n;
	float tmp;

	if((t1<t0) && (dt>0))
	{
		fprintf(stderr, "array_create_val : error create vector t1<t0 and dt>0\n");
		exit(1);
	}
	else if((t1>t0) && (dt<0))
	{
		fprintf(stderr, "array_create_val : error create vector t1>t0 and dt<0\n");
		exit(1);
	}

	if(dt>0.0)
		tmp = fabs(t1-t0) + dt;
	else
		tmp = fabs(t1-t0) - dt;

	n = (int) fabs(ceil(tmp/dt));
	(*len) = n;
	out = (float*) calloc(abs(n), sizeof(float));
	for(i=0; i<n; i++)
		out[i] = i*dt + t0;

	return out;
}

float **array_createfromvect(float *vector, int len, int numb, bool type, int *row, int *col)
{
	float **out;
	int i,j, n;

	n = len;
	if(type==0)
	{
		out = su_ealloc2float(n, numb);
		(*row) = numb;
		(*col) = n;
	}
	else
	{
		out = su_ealloc2float(numb, n);
		(*row) = n;
		(*col) = numb;
	}

	for (i=0; i<numb; i++)
	{
		for(j=0; j<n; j++)
		{
			if(type==0)
				out[i][j] = vector[j];
			else
				out[j][i] = vector[j];
		}
	}

	return out;
}

void array_setval(float *sx, int nx, double val)
{
	int i;
	for (i=0; i<nx; i++)
		sx[i] = (float) val;
}

//20-05-2010
//type 1="+", 2="-", 3="*", 4="/"
double *array_operation_double(double *A, double *B, int len, short type)
{
	int i;
	double *C=NULL;

	C = su_ealloc1double(len);
	for(i=0; i<len; i++)
	{
		switch(type)
		{
		case 1:
			C[i] = A[i] + B[i];
			break;
		case 2:
			C[i] = A[i] - B[i];
			break;
		case 3:
			C[i] = A[i] * B[i];
			break;
		case 4:
			C[i] = A[i] / B[i];
			break;
		default:
			C[i] = A[i] + B[i];
			break;
		}
	}
	return (C);
}

//22-06-2010
//type 1="+", 2="-", 3="*", 4="/"
void array_operationv_double(double *A, double *B, int len, short type)
{
	int i;

	for(i=0; i<len; i++)
	{
		switch(type)
		{
		case 1:
			A[i] += B[i];
			break;
		case 2:
			A[i] -= B[i];
			break;
		case 3:
			A[i] *= B[i];
			break;
		case 4:
			A[i] /= B[i];
			break;
		default:
			A[i] += B[i];
			break;
		}
	}
}

double *array_int2double(int *A, int lenA)
{
	int i;
	double *B=NULL;

	B = su_ealloc1double(lenA);
	for(i=0; i<lenA; i++)
	{
		B[i] = (double) A[i];
	}
	return(B);
}



