/*
 * getMidArray.c
 *
 *  Created on: Jun 14, 2010
 *      Author: toto
 */
#include "getMidArray.h"

double *getmidArray_double(double *data1, double *data2, int nx)
{
	int i;
	int n;
	double *out;

	n = nx;
	out = su_ealloc1double(n);
	for (i=0; i<n; i++)
	{
		out[i] = (data1[i] + data2[i]) / 2;
	}
	return out;

}
