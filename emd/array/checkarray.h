/*
 * checkarray.h
 *
 *  Created on: May 24, 2010
 *      Author: toto
 */

#ifndef CHECKARRAY_H_
#define CHECKARRAY_H_


#include "su_ealloc.h"
#include <stdio.h>

int *cekArray_int(int *data, int ndata, int ceknumb, int type);
int *cekArray_double(double *data, int ndata, double ceknumb, int type);

#endif /* CHECKARRAY_H_ */
