/*
 * array_library.h
 *
 *  Created on: Apr 20, 2010
 *      Author: toto
 */

#ifndef ARRAY_LIBRARY_H_
#define ARRAY_LIBRARY_H_
#include "su_ealloc.h"
#include "stdbool.h"

void array_checksize(int len, int from, int to);
void array_printd(double *A, int len, int from, int to, short type);
void array_printf(float *A, int len, int from, int to, bool type);
void array_printi(int *A, int len, int from, int to, bool type);
float array_mean(float *V, int len);
void array_sum(float *X, int nx, float *Y, int ny);
float *array_create_val(float t0, float dt, float t1, int *len);
float **array_createfromvect(float *vector, int len, int numb, bool type, int *row, int *col);
void array_setval(float *sx, int nx, double val);

//double data
//type 1="+", 2="-", 3="*", 4="/"
double *array_operation_double(double *A, double *B, int len, short type);
void array_operationv_double(double *A, double *B, int len, short type);

double *array_int2double(int *A, int lenA);
double *array_getValue_double(double *data, int *loc, int ndata, int nloc);
#endif /* ARRAY_LIBRARY_H_ */
