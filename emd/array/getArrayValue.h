/*
 * getArrayValue.h
 *
 *  Created on: Jun 14, 2010
 *      Author: toto
 */

#ifndef GETARRAYVALUE_H_
#define GETARRAYVALUE_H_

#include "su_ealloc.h"

int *getArrayValue_int(int *data, int *loc, int ndata, int nloc);
double *getArrayValue_double(double *data, int *loc, int ndata, int nloc);
double *getArrayValuev_double(double *data, int ndata, int pos1, int pos2);
#endif /* GETARRAYVALUE_H_ */
