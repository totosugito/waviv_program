/*
 * joinArray.h
 *
 *  Created on: May 24, 2010
 *      Author: toto
 */

#ifndef JOINARRAY_H_
#define JOINARRAY_H_

#include "su_ealloc.h"

int *join2array_int(int *data1, int *data2, int ndata1, int ndata2);
int *join3array_int(int *data1, int *data2, int *data3, int ndata1, int ndata2, int ndata3);

double *join2array_double(double *data1, double *data2, int ndata1, int ndata2);
double *join3array_double(double *data1, double *data2, double *data3, int ndata1, int ndata2, int ndata3);
#endif /* JOINARRAY_H_ */
