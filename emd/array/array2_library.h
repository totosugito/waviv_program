/*
 * array2_library.h
 *
 *  Created on: Apr 20, 2010
 *      Author: toto
 */

#ifndef ARRAY2_LIBRARY_H_
#define ARRAY2_LIBRARY_H_
#include "stdbool.h"
#include "su_ealloc.h"

////array 2D
//float *array2_getvect_float(float **A, int row, int col, int row_from, int row_to,
//		int col_from, int col_to);
//void array2_printf(float **A, int row, int col, int row_from, int row_to,
//		int col_from, int col_to, bool type);

void array2t_checksize(int row, int col, int row_from, int row_to,
		int col_from, int col_to);
//float **array2_transpose(float **array, int row, int col, int *rowout, int *colout);
//float **array2_join3d(float **array1, int r1, int c1, float **array2, int r2, int c2,
//		float **array3, int r3, int c3, bool type, int *r4, int *c4);
//float **array2_arithm(float **A, float **B, int row, int col, short type);

//double array
void array2_printd(double **A, int row, int col, int row_from, int row_to,
		int col_from, int col_to, bool type);
double *array2_getvect_double(double **A, int row, int col, int row_from, int row_to,
		int col_from, int col_to);
#endif /* ARRAY2_LIBRARY_H_ */
