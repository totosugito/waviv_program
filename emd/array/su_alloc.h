/*
 * su_alloc.h
 *
 *  Created on: May 19, 2010
 *      Author: toto
 */

#ifndef ALLOC_H_
#define ALLOC_H_
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include <limits.h>
#include <float.h>



/* allocate and free multi-dimensional arrays */
void *su_alloc1 (size_t n1, size_t size);
void *su_realloc1 (void *v, size_t n1, size_t size);
void **su_alloc2 (size_t n1, size_t n2, size_t size);
void ***su_alloc3 (size_t n1, size_t n2, size_t n3, size_t size);

void su_free1 (void *p);
void su_free2 (void **p);
void su_free3 (void ***p);

int *su_alloc1int (size_t n1);
int *su_realloc1int (int *v, size_t n1);
int **su_alloc2int (size_t n1, size_t n2);
int ***su_alloc3int (size_t n1, size_t n2, size_t n3);
float *su_alloc1float (size_t n1);
float *su_realloc1float (float *v, size_t n1);
float **su_alloc2float (size_t n1, size_t n2);
float ***su_alloc3float (size_t n1, size_t n2, size_t n3);

double *su_alloc1double (size_t n1);
double *su_realloc1double (double *v, size_t n1);
double **su_alloc2double (size_t n1, size_t n2);
double ***su_alloc3double (size_t n1, size_t n2, size_t n3);

void su_free1int (int *p);
void su_free2int (int **p);
void su_free3int (int ***p);
void su_free1float (float *p);
void su_free2float (float **p);
void su_free3float (float ***p);

void su_free1double (double *p);
void su_free2double (double **p);
void su_free3double (double ***p);

#define su_calloc1float(n) (float*) calloc (n,sizeof(float))
#define su_calloc1double(n) (double*) calloc (n,sizeof(double))
#define su_calloc1int(n) (int*) calloc (n,sizeof(int))

#endif /* SU_ALLOC_H_ */
