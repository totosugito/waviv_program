/*
 * getArrayValue.c
 *
 *  Created on: Jun 14, 2010
 *      Author: toto
 */
#include "getArrayValue.h"

int *getArrayValue_int(int *data, int *loc, int ndata, int nloc)
{
	int i;

	int *out;

	out = su_ealloc1int(nloc);
	for(i=0; i<nloc; i++)
	{
		if(loc[i]>ndata-1)
		{
			fprintf(stderr, "error get mlm... value --> (loc>ndata)\n");
			exit(0);
		}
		else
		{
			out[i] = data[loc[i]];
		}
	}
	return out;
}

double *getArrayValue_double(double *data, int *loc, int ndata, int nloc)
{
	int i;

	double *out;

	out = su_ealloc1double(nloc);
	for(i=0; i<nloc; i++)
	{
		if(loc[i]>ndata-1)
		{
			fprintf(stderr, "error get mlm... value --> (loc>ndata)\n");
			exit(0);
		}
		else
		{
			out[i] = data[loc[i]];
		}
	}
	return out;
}

double *getArrayValuev_double(double *data, int ndata, int pos1, int pos2)
{
	double *out;

	out = su_ealloc1double(pos2-pos1);
	memcpy(out, data+pos1, (pos2-pos1)*sizeof(double));
	return out;
}
