/*
 * su_ealloc.h
 *
 *  Created on: May 19, 2010
 *      Author: toto
 */

#ifndef SU_EALLOC_H_
#define SU_EALLOC_H

#include "su_alloc.h"
#include "../printout/su_errpkg.h"

void *su_ealloc1 (size_t n1, size_t size);
void *su_erealloc1 (void *v, size_t n1, size_t size);
void **su_ealloc2 (size_t n1, size_t n2, size_t size);
void ***su_ealloc3 (size_t n1, size_t n2, size_t n3, size_t size);

int *su_ealloc1int(size_t n1);
int *su_erealloc1int(int *v, size_t n1);
int **su_ealloc2int(size_t n1, size_t n2);
int ***su_ealloc3int(size_t n1, size_t n2, size_t n3);

float *su_ealloc1float(size_t n1);
float *su_erealloc1float(float *v, size_t n1);
float **su_ealloc2float(size_t n1, size_t n2);
float ***su_ealloc3float(size_t n1, size_t n2, size_t n3);

double *su_ealloc1double(size_t n1);
double *su_erealloc1double(double *v, size_t n1);
double **su_ealloc2double(size_t n1, size_t n2);
double ***su_ealloc3double(size_t n1, size_t n2, size_t n3);

#endif /* SU_EALLOC_H_ */
