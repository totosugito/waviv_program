/*
 * array.h
 *
 *  Created on: May 20, 2010
 *      Author: toto
 */

#ifndef ARRAY_H_
#define ARRAY_H_

//20 Mei 2010
#include "array_library.h"
#include "array2_library.h"
#include "su_ealloc.h"
#include "checkarray.h"
#include "arrayOperationVar.h"
#include "arrayOperationArray.h"
#include "joinArray.h"
#include "getArrayValue.h"
#include "getMidArray.h"
#endif /* ARRAY_H_ */
