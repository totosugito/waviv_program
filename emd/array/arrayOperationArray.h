/*
 * arrayOperationArray.h
 *
 *  Created on: May 24, 2010
 *      Author: toto
 */

#ifndef ARRAYOPERATIONARRAY_H_
#define ARRAYOPERATIONARRAY_H_

#include "su_ealloc.h"
#include <stdio.h>

float *arrayOperationArrayo_float(float *data1, float *data2, int ndata,int type);

void arrayOperationArray_double(double *data1, double *data2, int ndata,int type);
double *arrayOperationArrayo_double(double *data1, double *data2, int ndata,int type);
void arrayOperationArray_int(int *data1, int *data2, int ndata,int type);
double *arrayMid_double(double *data1, double *data2, int nx);
void arrayMidv_double(double *data1, double *data2, int nx);
#endif /* ARRAYOPERATIONARRAY_H_ */
