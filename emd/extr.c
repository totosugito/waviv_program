/*
 * extr.c
 *
 *  Created on: Jan 17, 2010
 *      Author: toto
 */

#include "extr.h"

/*get zeroes crossing*/
void get_indzer(double *x, int nx, int *indzer, int *nindzer)
{
	int i;
	int tmpnindzer;
	int *iz, niz;
	int *debz, ndebz;
	int *finz, nfinz;
	int *iz_diff;
	int *dz;
	int *tmp;
	int *tmp_join;
	int *zer2_i;
	int *indz;
	double ttmp_d;
	int ntmp1, ndz;
	float ftmp;
	tmpnindzer = 0;
	tmp = (int*) calloc(1, sizeof(int));

	for(i=0; i<nx-1; i++)
	{
		ttmp_d = x[i]*x[i+1];		/* find negative values */
		if(ttmp_d <0)	//find data less than zero
		{
			indzer[tmpnindzer] = i;
			tmpnindzer = tmpnindzer+1;
		}
	}

	if(anyequal_double(x, nx, 0))		//check array x if value zero
	{
		iz = find_double(x, nx, 0, 0, &niz);

		iz_diff = diff1_int(iz, niz, 1, &ntmp1);
		if(anyequal_int(iz_diff, ntmp1, 1))
		{
			zer2_i = cekArray_double(x, nx , 0, 0);
			tmp_join = join3array_int(tmp, zer2_i, tmp, 1, nx, 1);
			dz = diff1_int(tmp_join, nx+2, 1, &ndz);
			debz = find_int(dz, ndz, 1, 0, &ndebz);
			finz = find_int(dz, ndz, -1, 0, &nfinz);

			indz = su_ealloc1int(ndebz);
			for(i=0; i<ndebz; i++)
			{
				ftmp = ((float)(debz[i]+1+finz[i]))/2.0;
				indz[i] = roundfunc(ftmp);
				indz[i] = indz[i] - 1;
			}
			niz = ndebz;

			if(zer2_i) su_free1int(zer2_i);
			if(tmp_join) su_free1int(tmp_join);
			if(dz) su_free1int(dz);
			if(debz) su_free1int(debz);
			if(finz) su_free1int(finz);
		}
		else
		{
			indz = su_ealloc1int(niz);
			memcpy(indz, iz, niz*sizeof(int));
		}

		tmpnindzer = tmpnindzer+niz;
		memcpy(indzer+tmpnindzer, indz, niz*sizeof(int));
		gsl_sort_int(indzer, 1, tmpnindzer);

		if(iz_diff) su_free1int(iz_diff);
		if(iz) su_free1int(iz);
		if(indz) su_free1int(indz);
	}

	(*nindzer) = tmpnindzer;
	su_free1int(tmp);
}

/*take 2nd order derivative of x to find extrema*/
void extr(double *x, int nx, int *indmin, int *nindmin, int *indmax, int *nindmax)
{
	int k, i;
	int ndd;
	int nd;
	int *dd=NULL;
	int *debs=NULL, ndebs=0;
	int *fins=NULL, nfins=0;
	int *tmpindmin=NULL, ntmpindmin=0;
	int *tmpindmax=NULL, ntmpindmax=0;
	int *tmp;
	int *bad=NULL;
	int *ddjoin=NULL;
	int *imin=NULL, nimin=0;
	int *imax=NULL, nimax=0;
	double *d1=NULL, *d2=NULL;
	double *d=NULL;

	tmp = (int*) calloc(1, sizeof(int));

	// take 2nd order derivative of x to find extrema
	d = diff1_double(x, nx, 1, &nd);
	d1 = getArrayValuev_double(d, nd, 0, nd-1);
	d2 = getArrayValuev_double(d, nd, 1, nd);
	arrayOperationArray_double(d2, d1, nd-1, 3);
	tmpindmin = find2_double(d2, 0, 1, d1, 0, 1, nd-1, &ntmpindmin);
	tmpindmax = find2_double(d2, 0, 1, d1, 0, 2, nd-1, &ntmpindmax);

	if(ntmpindmin > 0)	{
		for(i=0; i<ntmpindmin; i++)
			tmpindmin[i] += 1;
	}

	if(ntmpindmax > 0)	{
		for(i=0; i<ntmpindmax; i++)
			tmpindmax[i] += 1;
	}

	// repeat for extrema not associated with a zero crossing
	if (anyequal_double(d, nd, 0))
	{
		bad = cekArray_double(d, nd, 0, 0);
		ddjoin = join3array_int(tmp, bad, tmp, 1, nd, 1);
		dd = diff1_int(ddjoin, nd+2, 1, &ndd);
		debs = find_int(dd, ndd, 1, 0, &ndebs);
		fins = find_int(dd, ndd, -1, 0, &nfins);

		if((ndebs>0) && (debs[0]==0))
		{
			if(ndebs>1)
			{
				for(i=0; i<ndebs-1; i++)
				{
					debs[i] = debs[i+1];
					fins[i] = fins[i+1];
				}
				ndebs = ndebs-1;
				nfins = nfins-1;
			}
			else
			{
				ndebs=0;
				nfins=0;
			}
		}

		if(ndebs>0)
		{
			if(fins[nfins-1]==nx-1)
			{
				if(ndebs>1)
				{
					ndebs = ndebs-1;
					nfins = nfins-1;
				}
				else
				{
					ndebs=0;
					nfins=0;
				}
			}
		}

		if(ndebs>0)
		{
			imin = su_ealloc1int(ndebs);
			imax = su_ealloc1int(ndebs);
			nimin = 0;
			nimax = 0;
			for(k=0; k<ndebs; k++)
			{
				if(d[debs[k]-1] > 0)
				{
					if(d[fins[k]] < 0)
					{
						imax [nimax] = roundfunc((fins[k]+debs[k])/2);
						nimax += 1;
					}
				}
				else
				{
					if(d[fins[k]] > 0)
					{
						imin [nimin] = roundfunc((fins[k]+debs[k])/2);
						nimin += 1;
					}
				}
			}
			ndebs=0;
			nfins=0;
		}
	}

	(*nindmax) = ntmpindmax + nimax;
	memcpy(indmax, tmpindmax, ntmpindmax*sizeof(int));
	memcpy(indmax+ntmpindmax, imax, nimax*sizeof(int));
	if((*nindmax)>1)
		gsl_sort_int(indmax, 1, (*nindmax));


	(*nindmin) = ntmpindmin + nimin;
	memcpy(indmin, tmpindmin, ntmpindmin*sizeof(int));
	memcpy(indmin+ntmpindmin, imin, nimin*sizeof(int));
	if((*nindmin)>1)
		gsl_sort_int(indmin, 1, (*nindmin));

	//free allocated memory
	if(dd) su_free1int(dd);
	if(ddjoin) su_free1int(ddjoin);
	if(debs) su_free1int(debs);
	if(fins) su_free1int(fins);
	if(bad) su_free1int(bad);
	if(imin) su_free1int(imin);
	if(imax) su_free1int(imax);
	if(tmp) su_free1int(tmp);
	if(tmpindmax) su_free1int(tmpindmax);
	if(tmpindmin) su_free1int(tmpindmin);

	if(d) su_free1double(d);
	if(d1) su_free1double(d1);
	if(d2) su_free1double(d2);
}

void extr2(double *x, int *t, int nx, int num_sd,
		int *indmin, int *tnindmin, int *indmax, int *tnindmax)
{
	int i, j;
	int tbuf1,tbuf2, tmp;
	int nidx, nidx2, ndiffidx;
	double diffidxstd, tmpd, tmpd1, diffmean;
	int nindmin, nindmax;
	int *tmpindmin=NULL, ntmpindmin;
	int *tmpindmax=NULL, ntmpindmax;
	int *idx=NULL;
	int *tbuf=NULL;
	int *idx2=NULL;
	int *diffidx=NULL;
	double *xbuf=NULL;
	double *polyValue_d=NULL;
	double *mat_poly=NULL, nmat_poly;

	tmpindmin = su_ealloc1int(nx);
	tmpindmax = su_ealloc1int(nx);
	nmat_poly = 3;
	nidx2 = 0;
	diffidxstd =0;

	extr(x, nx, indmin, &nindmin, indmax, &nindmax);
	nidx = nindmin + nindmax;
	idx = join2array_int(indmin, indmax, nindmin, nindmax);
	if(nidx>1)
	{
		gsl_sort_int(idx, 1, nidx);
		diffidx = diff1_int(idx, nidx, 1, &ndiffidx);
		diffidxstd = gsl_stats_int_sd(diffidx, 1, ndiffidx);
	}

	if(diffidxstd>0)
	{
		diffmean = mean_i(diffidx, ndiffidx);
		tmpd = num_sd*diffidxstd;
		idx2 = su_ealloc1int(ndiffidx);

		for(i=0; i<ndiffidx; i++)
		{
			tmpd1 = (double)diffidx[i] - diffmean;
			if(abs_d(tmpd1)>tmpd)
			{
				idx2[nidx2] = i;
				nidx2 += 1;
			}
		}
	}

	if(nidx2>0)
	{
		for(i=0; i<nidx2; i++)
		{
			tbuf1 = idx[idx2[i]];
			tbuf2 = idx[idx2[i]+1];
			tmp = tbuf2-tbuf1+1;
			if(tmp>3)
			{
				tbuf = su_ealloc1int(tmp);
				xbuf = su_ealloc1double(tmp);

				memcpy(tbuf,t+tbuf1,tmp*sizeof(int));
				memcpy(xbuf,x+tbuf1,tmp*sizeof(double));

				mat_poly = polyfit_int(tbuf, xbuf, tmp, nmat_poly);	//get polynomial equation
				polyValue_d = polyval_int(tbuf, mat_poly, tmp, nmat_poly+1); //get polynomial value from input point
				arrayOperationArray_double(xbuf, polyValue_d, tmp, 2);

				extr(xbuf, tmp, tmpindmin, &ntmpindmin, tmpindmax, &ntmpindmax);	//compute extr
				for(j=0; j<ntmpindmax; j++)
					indmin[nindmin+j] = tmpindmax[j] + tbuf1+1;

				for(j=0; j<ntmpindmin; j++)
					indmax[nindmax+j] = tmpindmin[j] + tbuf1+1;
				nindmin += ntmpindmax;
				nindmax += ntmpindmin;

				//free allocated memory
				if(tbuf) su_free1int(tbuf);
				if(xbuf) su_free1double(xbuf);
				if(mat_poly) su_free1double(mat_poly);
				if(polyValue_d) su_free1double(polyValue_d);
			}
		}
	}
	uniquev_int(indmin, nindmin, &ntmpindmin);
	uniquev_int(indmax, nindmax, &ntmpindmax);
	(*tnindmax) = ntmpindmax;
	(*tnindmin) = ntmpindmin;

	//free variable
	su_free1int(tmpindmin);
	su_free1int(tmpindmax);
	if(idx) su_free1int(idx);
	if(idx2) su_free1int(idx2);
	if(diffidx) su_free1int(diffidx);
}


