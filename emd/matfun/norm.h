/*
 * norm.h
 *
 *  Created on: May 24, 2010
 *      Author: toto
 */

#ifndef NORM_H_
#define NORM_H_
#include <math.h>
#include <stdio.h>

double norm_double(double *data, int nx);

#endif /* NORM_H_ */
