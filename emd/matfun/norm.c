/*
 * norm.c
 *
 *  Created on: May 24, 2010
 *      Author: toto
 */
//NORM   Matrix or vector norm.
//   For matrices...
//     NORM(X) is the largest singular value of X, max(svd(X)).

#include "norm.h"

double norm_double(double *data, int nx)
{
	int i;
	double tmp;
	double out1, out;

	out1 = 0.0;
	for(i=0; i<nx; i++)
	{
		tmp = pow(data[i],2.0);
		out1 += tmp;
	}
	out = sqrt(out1);
	return out;
}
