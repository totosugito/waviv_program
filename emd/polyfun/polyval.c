/*
 * polyval.c
 *
 *  Created on: May 24, 2010
 *      Author: toto
 */
#include "polyval.h"

double *polyval_double(double *X, double *poly, int nx, int npoly)
{
	double tmp,tmp1;
	double expn;
	double *Y;
	int i,j;

	Y = su_ealloc1double(nx);

	for(i=0; i<nx; i++)
	{
		tmp = 0.0;
		for(j=0; j<npoly; j++)
		{
			expn = (double) npoly-j-1;
			tmp1= pow(X[i], expn);
			tmp = tmp +  (tmp1 * poly[j]);
		}
		Y[i] = tmp;
	}
	return(Y);
}

double *polyval_int(int *X, double *poly, int nx, int npoly)
{
	double tmp,tmp1, tmpx;
	double expn;
	double *Y;
	int i,j;

	Y = su_ealloc1double(nx);

	for(i=0; i<nx; i++)
	{
		tmp = 0.0;
		for(j=0; j<npoly; j++)
		{
			expn = (double) npoly-j-1;
			tmpx = (double) X[i];
			tmp1= pow(tmpx, expn);
			tmp = tmp +  (tmp1 * poly[j]);
		}
		Y[i] = tmp;
	}
	return(Y);
}
