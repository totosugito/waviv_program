/*
 * pwch.c
 *
 *  Created on: May 19, 2010
 *      Author: toto
 */

//PWCH Piecewise cubic Hermite interpolation
//
//   PC = PWCH(X,Y,S) returns the ppform of the piecewise cubic Hermite
//   interpolant f to the given data values Y and the given slopes S
//   at the given data sites X. X must be a vector, and S must be of the same
//   size as Y.
//   If Y is a vector, then it must be of the same length as X and will be
//   resized if necessary to be a row vector.
//   If Y is a matrix, then size(Y,2) must equal length(X).
//
//   With d equal to size(Y,1), the piecewise cubic Hermite interpolant f to
//   these data is the d-valued piecewise cubic function with breaks at the
//   data sites X that satisfies
#include "pwch.h"

//input x, y, s, dx, divdif is a vector column (array with row=1)
//output row<< pangkat >>
//ex spline = [a1 a2 a3 a4]
//at data=5 --> Y = a1*data^3 + a2*data^2 + a3*data + a4
//
//20-05-2010
double **pwch_double(double *x, double *y, double *s, int lenxys, double *dx, double *divdif, int lendxdif)
{
	int d;
	int i;
	int n;  //number of element array x
	int dnm1;
	double *dxd;
	double dzzdx, dzdxdx;
	double **var_spline=NULL;  //output constanta spline

	if(lenxys!=lendxdif+1)
	{
		fprintf(stderr, "pwch_double : lenxys != lendxdif+1 \n");
		exit(0);
	}

	d = 1;
	dxd = su_ealloc1double(lendxdif);
	var_spline = su_ealloc2double(4, lendxdif);
	memcpy(dxd, dx, lendxdif*sizeof(double));
	n = lenxys;
	dnm1 = d*(n-1);

	for(i=0; i<lendxdif; i++) //loop to get constanta spline
	{
		dzzdx = (divdif[i]- s[i]) / dxd[i];
		dzdxdx = (s[i+1]-divdif[i]) / dxd[i];
		var_spline[i][0] = (dzdxdx-dzzdx)/dxd[i];
		var_spline[i][1] = 2*dzzdx-dzdxdx;
		var_spline[i][2] = s[i];
		var_spline[i][3] = y[i];
	}

	su_free1double(dxd);
	return (var_spline);
}
