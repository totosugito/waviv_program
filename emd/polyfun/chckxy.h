/*
 * chckxy.h
 *
 *  Created on: May 20, 2010
 *      Author: toto
 */

#ifndef CHCKXY_H_
#define CHCKXY_H_

#include "../array/array.h"

void chckxy(double *X, double *Y, int lenXY, int *sizeY);

#endif /* CHCKXY_H_ */
