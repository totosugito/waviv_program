/*
 * spline.c
 *
 *  Created on: May 20, 2010
 *      Author: toto
 */
//SPLINE Cubic spline data interpolation.
//   PP = SPLINE(X,Y) provides the piecewise polynomial form of the
//   cubic spline interpolant to the data values Y at the data sites X,
//   for use with the evaluator PPVAL and the spline utility UNMKPP.
//   X must be a vector.
//   If Y is a vector, then Y(j) is taken as the value to be matched at X(j),
//   hence Y must be of the same length as X  -- see below for an exception
//   to this.
//   If Y is a matrix or ND array, then Y(:,...,:,j) is taken as the value to
//   be matched at X(j),  hence the last dimension of Y must equal length(X) --
//   see below for an exception to this.
//
//   YY = SPLINE(X,Y,XX) is the same as  YY = PPVAL(SPLINE(X,Y),XX), thus
//   providing, in YY, the values of the interpolant at XX.  For information
//   regarding the size of YY see PPVAL.
//
//   Ordinarily, the not-a-knot end conditions are used. However, if Y contains
//   two more values than X has entries, then the first and last value in Y are
//   used as the endslopes for the cubic spline.  If Y is a vector, this
//   means:
//       f(X) = Y(2:end-1),  Df(min(X))=Y(1),    Df(max(X))=Y(end).
//   If Y is a matrix or N-D array with SIZE(Y,N) equal to LENGTH(X)+2, then
//   f(X(j)) matches the value Y(:,...,:,j+1) for j=1:LENGTH(X), then
//   Df(min(X)) matches Y(:,:,...:,1) and Df(max(X)) matches Y(:,:,...:,end).

#include "spline.h"

//update 21-05-2010
double **spline(double *X, double *Y, int lenX, int lenY)
{
//	int i;
	int sizeY;
	int n;
	int yd;
	int dd;
	int endslopes=0;   //for now, endslopes=0. endslopes>0 if input data have nan variable
	double x31, xn;  //temporary variable
	double *dx=NULL; int ndx;
	double *dy=NULL; int ndy;
	double *divdif=NULL;
	double *s_spline=NULL;
	double **pp=NULL; //output constanta spline
	double *tmpdiv=NULL; int ntmpdiv; //temporary diff(divdif)
	double *b=NULL;
	double **tmpspdiags=NULL;

	if(lenX!=lenY)
	{
		fprintf(stderr, "spline : lenX != lenY, lenX=%i, lenY=%i\n", lenX, lenY);
		exit(0);
	}

	//Check that data are acceptable and, if not, try to adjust them appropriately
	//accept input Y is a vector
	//------------------------------------------------------------------
	//check this part if program can accept nan input data
	//??????
	chckxy(X, Y, lenX, &sizeY);
	n = lenX;
	yd = 1;

	//Generate the cubic spline interpolant in ppform
	dd = 1;

	dx = diff1_double(X, n, 1, &ndx);
	dy = diff1_double(Y, n, 1, &ndy);

	divdif = array_operation_double(dy, dx, ndx, 4);
	//------------------------------------------------------------------

	if(n<=1)
	{
		fprintf(stderr, "spline : Input data must be > 1\n");
		exit(0);
	}

//	pp = ealloc2double(4, n-1);
	if (n==2)
	{
	    if (endslopes==0) // the interpolant is a straight line
		{
	    	pp[0][2] = divdif[0];
	    	pp[0][3] = Y[0];

		}
	    else  // the interpolant is the cubic Hermite polynomial
	    {
	    	fprintf(stderr, "spline : Not yet implementated, can not accep input NAN \n");
	    	exit(0);
	        //pp = pwch(x,y,endslopes,dx,divdif);
	    	//pp.dim = sizey;
	    }
	}
	else if ((n==3) && (endslopes==0)) // the interpolant is a parabola
	{
		tmpdiv = diff1_double(divdif, ndx, 1, &ntmpdiv);
		pp[0][3] = Y[0];
		pp[0][1] = tmpdiv[0]/(X[2]-X[0]);
		pp[0][2] = divdif[0] - (pp[0][1]*dx[0]);
	}
	else // set up the sparse, tridiagonal, linear system b = ?*c for the slopes
	{
		b = su_ealloc1double(n);
		setBvar_spline(b, dx, divdif, n);

	    if (endslopes==0)
	    {
	        x31 = X[2] - X[0];    //perhitungan untuk mendapatkan nilai awal b
	        xn = X[n-1] - X[n-3]; //perhitungan untuk mendapatkan nilai akhir b
	        b[0] = ((dx[0]+2*x31) * dx[1]*divdif[0] + pow(dx[0],2) * divdif[1]) / x31;
	        b[n-1] = (pow(dx[n-2],2) * divdif[n-3] + (2*xn+dx[n-2]) * dx[n-3] * divdif[n-2]) / xn;
	    }
	    else
	    {
	    	fprintf(stderr, "spline : Not yet implementated, can not accep input NAN \n");
	    	exit(0);
	        //x31 = 0; xn = 0; b(:,[1 n]) = dx(dd,[2 n-2]).*endslopes;
	    }

	    tmpspdiags = su_ealloc2double(3, n);
	    set_tmpspdiags_spline(tmpspdiags, dx, x31, xn, n);
	    s_spline = get_s_spdiags(b, tmpspdiags, n, 3, n, n);
	    pp = pwch_double(X, Y, s_spline, n, dx, divdif, ndx);
//	    array2_printd(pp, n, 4, 1, n, 1, 4,1);
	}
	su_free2double(tmpspdiags);
	su_free1double(b);
	su_free1double(s_spline);
	su_free1double(dx);
	su_free1double(dy);
	su_free1double(divdif);
	return (pp);
}

void setBvar_spline(double *B, double *dx, double *divdif, int n)
{
	int i;
	double tmp1, tmp2;

	for(i=0; i<n-2; i++)
	{
		tmp1 = dx[i+1]*divdif[i];
		tmp2 = dx[i]*divdif[i+1];
		B[i+1] = 3 * (tmp1 + tmp2);
	}
}

void set_tmpspdiags_spline(double **tmpspdiags, double *dx, double x31, double xn, int n)
{
	int i;

	for(i=0; i<n; i++)
	{
		if(i==0)
		{
			tmpspdiags[i][0] = x31;
			tmpspdiags[i][1] = dx[1];
			tmpspdiags[i][2] = 0.0;
		}
		else if(i==n-1)
		{
			tmpspdiags[i][0] = 0.0;
			tmpspdiags[i][1] = dx[n-3];
			tmpspdiags[i][2] = xn;
		}
		else
		{
			tmpspdiags[i][0] = dx[i-1];
			tmpspdiags[i][1] = 2 * (dx[i]+dx[i-1]);
			tmpspdiags[i][2] = dx[i];
		}
	}
}

//create spline matlab
//evaluate value at vector T
//output process spline is new point spline from input X and Y evaluate at X = valspline
double *spline_val(double *X, double *Y, int lenX, int lenY, double *T, int nt)
{
	double **tssp=NULL;
	double *valspline=NULL;

	tssp = spline(X, Y, lenX, lenY);
	valspline = ppval(tssp, lenX-1, X, T, nt);

	su_free2double(tssp);
	return (valspline);
}

//create spline matlab
//evaluate value at vector T
//output process spline is new point spline from input X and Y evaluate at X = value
void splinev_val(double *X, double *Y, int lenX, int lenY, double *T, int nt, double *value)
{
	double **tssp=NULL;

	tssp = spline(X, Y, lenX, lenY);
	ppvalv(tssp, lenX-1, X, T, nt, value);

	su_free2double(tssp);
}

double *spline_gsl_double(double *X, double *Y, double *T, int nx, int nt)
{
	double *out=NULL;
	int i;
	double tmp;
	gsl_interp_accel *acc;
	gsl_spline *spline;

	out = su_ealloc1double(nt);
	if(nx>2)
	{
		acc = gsl_interp_accel_alloc ();
		spline = gsl_spline_alloc (gsl_interp_cspline, nx);
		gsl_spline_init (spline, X, Y, nx);
		for(i=0; i<nt; i++)
		{
			tmp = gsl_spline_eval(spline,T[i],acc);
			out[i] = tmp;
		}

		gsl_spline_free (spline);
		gsl_interp_accel_free (acc);
	}
	else
	{
		memcpy(out, Y, nt*sizeof(double));
	}
	return out;
}

double *spline_gsl_doublei(double *X, double *Y, double *T, int nx, int nt)
{
	int i;
	double *out;
	double tmp;
	gsl_interp_accel *acc;
	gsl_spline *spline;

	out = su_ealloc1double(nt);
	if(nx>2)
	{
		acc = gsl_interp_accel_alloc ();
		spline = gsl_spline_alloc (gsl_interp_cspline, nx);
		gsl_spline_init (spline, X, Y, nx);
		for(i=0; i<nt; i++)
		{
			tmp = gsl_spline_eval(spline,T[i],acc);
			out[i] = tmp;
		}

		gsl_spline_free (spline);
		gsl_interp_accel_free (acc);
	}
	else
	{
		memcpy(out, Y, nt*sizeof(double));
	}
	return out;
}

void splinev_gsl_doublei(double *X, double *Y, double *T, int nx, int nt, double *value)
{
	int i;
	double tmp;
	gsl_interp_accel *acc;
	gsl_spline *spline;

	if(nx>2)
	{
		acc = gsl_interp_accel_alloc ();
		spline = gsl_spline_alloc (gsl_interp_cspline, nx);
		gsl_spline_init (spline, X, Y, nx);
		for(i=0; i<nt; i++)
		{
			tmp = gsl_spline_eval(spline,T[i],acc);
			value[i] = tmp;
		}

		gsl_spline_free (spline);
		gsl_interp_accel_free (acc);
	}
	else
	{
		memcpy(value, Y, nt*sizeof(double));
	}
}
