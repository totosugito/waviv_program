/*
 * polyfit.h
 *
 *  Created on: May 24, 2010
 *      Author: toto
 */

#ifndef POLYFIT_H_
#define POLYFIT_H_
#include <gsl/gsl_sort.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_block.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_linalg.h>
#include "../array/array.h"
#include "../elmat/vander.h"

double *polyfit_double(double *datax, double *datay, int nx,int N);
double *polyfit_int(int *datax, double *datay, int nx,int N);
#endif /* POLYFIT_H_ */
