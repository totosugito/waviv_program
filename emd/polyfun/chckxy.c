/*
 * chckxy.c
 *
 *  Created on: May 20, 2010
 *      Author: toto
 */

//CHCKXY check and adjust input for SPLINE and PCHIP
//   [X,Y,SIZEY] = CHCKXY(X,Y) checks the data sites X and corresponding data
//   values Y, making certain that there are exactly as many sites as values,
//   that no two data sites are the same, removing any data points that involve
//   NaNs, reordering the sites if necessary to ensure that X is a strictly
//   increasing row vector and reordering the data values correspondingly,
//   and reshaping Y if necessary to make sure that it is a matrix, with Y(:,j)
//   the data value corresponding to the data site X(j), and with SIZEY the
//   actual dimensions of the given values.
//   This call to CHCKXY is suitable for PCHIP.
//
//   [X,Y,SIZEY,ENDSLOPES] = CHCKXY(X,Y) also considers the possibility that
//   there are two more data values than there are data sites.
//   If there are, then the first and the last data value are removed from Y
//   and returned separately as ENDSLOPES. Otherwise, an empty ENDSLOPES is
//   returned.  This call to CHCKXY is suitable for SPLINE.

#include "chckxy.h"

//program cannot accept nan file
//matlab accept nan input data
//20-05-2010
void chckxy(double *X, double *Y, int lenXY, int *sizeY)
{
	int i;
	double tmp;

	if (lenXY<2)
	{
		fprintf(stderr, "chckxy : NotEnoughPts, There should be at least two data points.");
		exit(0);
	}

	//check nan file
	//check X data to ensure strictly increasing site sequence
	//The data sites should be distinct
	for(i=0; i<lenXY-1; i++)
	{
		if((isnan(Y[i])==1) || ((isnan(Y[i])==1)))
		{
			fprintf(stderr, "chckxy : All data points with NaN as their site will be ignored");
			exit(0);
		}

		tmp = X[i+1]-X[i];
		if(tmp<=0)
		{
			fprintf(stderr, "chckxy : The data sites should be distinct and increasing\n");
			fprintf(stderr, "Point %i = %5.5f, %i = %5.5f\n", i+1, X[i], i+2, X[i+1]);
			exit(0);
		}
	}

	//input is a vector row or column. So set sizeY=1
	//in matlab, input Y is an array or vector
	(*sizeY) = 1;
}
