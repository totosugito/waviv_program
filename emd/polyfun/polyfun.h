/*
 * polyfun.h
 *
 *  Created on: May 20, 2010
 *      Author: toto
 */

#ifndef POLYFUN_H_
#define POLYFUN_H_

#include "pwch.h"
#include "chckxy.h"
#include "spline.h"
#include "ppval.h"
#include "polyval.h"
#include "polyfit.h"

#endif /* POLYFUN_H_ */
