/*
 * ppval.c
 *
 *  Created on: May 21, 2010
 *      Author: toto
 */
//PPVAL  Evaluate piecewise polynomial.
//   V = PPVAL(PP,XX) returns the value, at the entries of XX, of the
//   piecewise polynomial f contained in PP, as constructed by PCHIP, SPLINE,
//   INTERP1, or the spline utility MKPP.

#include "ppval.h"

double *ppval(double **S, int rowS, double *breaks, double *X, int nx)
{
	int i;  //index
	int k;  //index
	int b1, b2; //index
	int getloc;
	int nbreaks;
	double *value=NULL;
	double tmp;

	nbreaks = rowS+1;
	value = su_ealloc1double(nx);
	getloc = 0;
	for(i=0; i<nx; i++)  // 1: loop every point data-X
	{
		k = 0;
		if(X[i]<breaks[0])  //check jika nilai X berada sebelum titik awal break
		{
			getloc = 0;
			tmp = X[i]-breaks[getloc];
			value[i] = S[getloc][0]*pow(tmp,3) + S[getloc][1]*pow(tmp,2) + S[getloc][2]*tmp + S[getloc][3];
		}
		else if(X[i]>breaks[rowS-1])   //check jika nilai X berada setelah titik akhir break
		{
			getloc = rowS-1;
			tmp = X[i]-breaks[getloc];
			value[i] = S[getloc][0]*pow(tmp,3) + S[getloc][1]*pow(tmp,2) + S[getloc][2]*tmp + S[getloc][3];
		}
		else  //check jika nilai X berada diantara dua titik break
		{
			while (k==0)
			{
				b1 = getloc;
				b2 = getloc+1;
				if((X[i]>=breaks[b1]) && (X[i]<=breaks[b2])) //check nilai X berada pada tengah titik break
				{
					tmp = X[i]-breaks[b1];
					value[i] = S[b1][0]*pow(tmp,3) + S[b1][1]*pow(tmp,2) + S[b1][2]*tmp + S[b1][3];
					k = 1;  //exit from looping
				}
				getloc += 1;

				if(getloc==rowS-1) //check jika setelah data dilooping tidak ketemu, ubah pengecekan dari awal
				{
					getloc = 0;
					break;
				}
			}

			getloc -= 1;  //kembalikan ketitik sebelum nya (pengaruh pada while --> getloc += 1
						  //supaya pencarian dimulai dari lokasi terakhir pengecekan
		}
	}  // 1: end loop every point data-X
	return(value);
}

void ppvalv(double **S, int rowS, double *breaks, double *X, int nx, double *value)
{
	int i;  //index
	int k;  //index
	int b1, b2; //index
	int getloc;
	int nbreaks;
	double tmp;

	nbreaks = rowS+1;
	getloc = 0;
	for(i=0; i<nx; i++)  // 1: loop every point data-X
	{
		k = 0;
		if(X[i]<breaks[0])  //check jika nilai X berada sebelum titik awal break
		{
			getloc = 0;
			tmp = X[i]-breaks[getloc];
			value[i] = S[getloc][0]*pow(tmp,3) + S[getloc][1]*pow(tmp,2) + S[getloc][2]*tmp + S[getloc][3];
		}
		else if(X[i]>breaks[rowS-1])   //check jika nilai X berada setelah titik akhir break
		{
			getloc = rowS-1;
			tmp = X[i]-breaks[getloc];
			value[i] = S[getloc][0]*pow(tmp,3) + S[getloc][1]*pow(tmp,2) + S[getloc][2]*tmp + S[getloc][3];
		}
		else  //check jika nilai X berada diantara dua titik break
		{
			while (k==0)
			{
				b1 = getloc;
				b2 = getloc+1;
				if((X[i]>=breaks[b1]) && (X[i]<=breaks[b2])) //check nilai X berada pada tengah titik break
				{
					tmp = X[i]-breaks[b1];
					value[i] = S[b1][0]*pow(tmp,3) + S[b1][1]*pow(tmp,2) + S[b1][2]*tmp + S[b1][3];
					k = 1;  //exit from looping
				}
				getloc += 1;

				if(getloc==rowS-1) //check jika setelah data dilooping tidak ketemu, ubah pengecekan dari awal
				{
					getloc = 0;
					break;
				}
			}

			getloc -= 1;  //kembalikan ketitik sebelum nya (pengaruh pada while --> getloc += 1
						  //supaya pencarian dimulai dari lokasi terakhir pengecekan
		}
	}  // 1: end loop every point data-X

}

