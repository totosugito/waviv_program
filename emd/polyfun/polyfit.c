/*
 * polyfit.c
 *
 *  Created on: May 24, 2010
 *      Author: toto
 */
//POLYFIT Fit polynomial to data.
//   P = POLYFIT(X,Y,N) finds the coefficients of a polynomial P(X) of
//   degree N that fits the data Y best in a least-squares sense. P is a
//   row vector of length N+1 containing the polynomial coefficients in
//   descending powers, P(1)*X^N + P(2)*X^(N-1) +...+ P(N)*X + P(N+1).
//
//   [P,S] = POLYFIT(X,Y,N) returns the polynomial coefficients P and a
//   structure S for use with POLYVAL to obtain error estimates for
//   predictions.  S contains fields for the triangular factor (R) from a QR
//   decomposition of the Vandermonde matrix of X, the degrees of freedom
//   (df), and the norm of the residuals (normr).  If the data Y are random,
//   an estimate of the covariance matrix of P is (Rinv*Rinv')*normr^2/df,
//   where Rinv is the inverse of R.

#include "polyfit.h"

double *polyfit_double(double *datax, double *datay, int nx,int N)
{
	int row,col;
	gsl_matrix *Vx;
	gsl_matrix *Vy;
	gsl_matrix *prodQY;
	gsl_matrix *q, *Q;
	gsl_matrix *r, *R, *Rinv, *poly_equa;
	gsl_vector *d, *tmpvec, *tmpvec1;
	gsl_permutation *p;
	double *out;
	int s=0;
	int i;
	int pn;

	pn = N;
//	Vx = (gsl_matrix*) calloc(1, sizeof(gsl_matrix));
	/*create Vandermonde array*/
	if(pn==nx-1)
	{
		row = nx;
		col = nx;
		Vx = vander_gsl_double(datax,row,col);
	}
	else if(pn<nx-1)
	{
		row = nx;
		col = pn+1;
		Vx = vander_gsl_double(datax,row,col);
	}
	else
	{
		/*not yet implementation*/
		/*X = V;
		for k = n+1:p+1
				X = [X xx'^k];
				     end*/
		fprintf(stderr, "vandermonde array not yet impelementated\n");
		exit(0);
	}

	/*create qr array*/
	out = su_ealloc1double(col);
	poly_equa = gsl_matrix_alloc(col,1);
	prodQY = gsl_matrix_calloc(col,1);

	Vy = gsl_matrix_calloc(row,1);
	q = gsl_matrix_calloc(row,row);

	Q = gsl_matrix_alloc(col, row);	/* transpose of q*/
	r = gsl_matrix_alloc(row,col);
	R = gsl_matrix_alloc(col,col);
	Rinv = gsl_matrix_alloc(col,col);
	d = gsl_vector_alloc(GSL_MIN(row,col));
	gsl_linalg_QR_decomp(Vx, d);
	gsl_linalg_QR_unpack(Vx, d, q, r);

	/*============================================*/
	/*setting matrix*/
	/*size matrix q = (row, col)*/
	tmpvec = gsl_vector_alloc(row);
	for(i=0; i<col; i++)
	{
//		gsl_matrix_transpose_memcpy(Q, q);
		gsl_matrix_get_col(tmpvec,q,i);
		gsl_matrix_set_row(Q,i,tmpvec);
	}

	/*size matrix r = (col, col)*/
	tmpvec1 = gsl_vector_alloc(col);
	for(i=0; i<col; i++)
	{
		gsl_matrix_get_row(tmpvec1,r,i);
		gsl_matrix_set_row(R,i,tmpvec1);
	}

	for(i=0; i<row; i++)
		gsl_matrix_set(Vy,i,0,datay[i]);
	/*============================================*/

	gsl_vector_free(tmpvec);
	gsl_vector_free(tmpvec1);
	gsl_vector_free(d);
	gsl_matrix_free(Vx);
	gsl_matrix_free(q);
	gsl_matrix_free(r);

	/*INVERS MATRIX R*/
	p = gsl_permutation_alloc (col);
	gsl_linalg_LU_decomp (R, p, &s);
	gsl_linalg_LU_invert (R, p, Rinv);

	/*PRODUCT Q and Y Matrix*/
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,1.0,Q,Vy,0.0,prodQY);

	/*PRODUCT Rinv and prodQY Matrix --> polynomial equation*/
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,1.0,Rinv,prodQY,0.0,poly_equa);

	for(i=0; i<col; i++)
		out[i] = gsl_matrix_get(poly_equa,i,0);

	gsl_matrix_free(Vy);
	gsl_matrix_free(Q);
	gsl_matrix_free(prodQY);
	gsl_matrix_free(R);
	gsl_matrix_free(Rinv);
	gsl_matrix_free(poly_equa);
	gsl_permutation_free (p);

	return out;
}

double *polyfit_int(int *datax, double *datay, int nx,int N)
{
	int row,col;
	gsl_matrix *Vx;
	gsl_matrix *Vy;
	gsl_matrix *prodQY;
	gsl_matrix *q, *Q;
	gsl_matrix *r, *R, *Rinv, *poly_equa;
	gsl_vector *d, *tmpvec, *tmpvec1;
	gsl_permutation *p;
	double *out;
	int s=0;
	int i;
	int pn;

	pn = N;
//	Vx = (gsl_matrix*) calloc(1, sizeof(gsl_matrix));
	/*create Vandermonde array*/
	if(pn==nx-1)
	{
		row = nx;
		col = nx;
		Vx = vander_gsl_int(datax,row,col);
	}
	else if(pn<nx-1)
	{
		row = nx;
		col = pn+1;
		Vx = vander_gsl_int(datax,row,col);
	}
	else
	{
		/*not yet implementation*/
		/*X = V;
		for k = n+1:p+1
				X = [X xx'^k];
				     end*/
		fprintf(stderr, "vandermonde array not yet impelementated\n");
		exit(0);
	}

	/*create qr array*/
	out = su_ealloc1double(col);
	poly_equa = gsl_matrix_alloc(col,1);
	prodQY = gsl_matrix_calloc(col,1);

	Vy = gsl_matrix_calloc(row,1);
	q = gsl_matrix_calloc(row,row);

	Q = gsl_matrix_alloc(col, row);	/* transpose of q*/
	r = gsl_matrix_alloc(row,col);
	R = gsl_matrix_alloc(col,col);
	Rinv = gsl_matrix_alloc(col,col);
	d = gsl_vector_alloc(GSL_MIN(row,col));
	gsl_linalg_QR_decomp(Vx, d);
	gsl_linalg_QR_unpack(Vx, d, q, r);

	/*============================================*/
	/*setting matrix*/
	/*size matrix q = (row, col)*/
	tmpvec = gsl_vector_alloc(row);
	for(i=0; i<col; i++)
	{
//		gsl_matrix_transpose_memcpy(Q, q);
		gsl_matrix_get_col(tmpvec,q,i);
		gsl_matrix_set_row(Q,i,tmpvec);
	}

	/*size matrix r = (col, col)*/
	tmpvec1 = gsl_vector_alloc(col);
	for(i=0; i<col; i++)
	{
		gsl_matrix_get_row(tmpvec1,r,i);
		gsl_matrix_set_row(R,i,tmpvec1);
	}

	for(i=0; i<row; i++)
		gsl_matrix_set(Vy,i,0,datay[i]);
	/*============================================*/

	gsl_vector_free(tmpvec);
	gsl_vector_free(tmpvec1);
	gsl_vector_free(d);
	gsl_matrix_free(Vx);
	gsl_matrix_free(q);
	gsl_matrix_free(r);

	/*INVERS MATRIX R*/
	p = gsl_permutation_alloc (col);
	gsl_linalg_LU_decomp (R, p, &s);
	gsl_linalg_LU_invert (R, p, Rinv);

	/*PRODUCT Q and Y Matrix*/
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,1.0,Q,Vy,0.0,prodQY);

	/*PRODUCT Rinv and prodQY Matrix --> polynomial equation*/
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,1.0,Rinv,prodQY,0.0,poly_equa);

	for(i=0; i<col; i++)
		out[i] = gsl_matrix_get(poly_equa,i,0);

	gsl_matrix_free(Vy);
	gsl_matrix_free(Q);
	gsl_matrix_free(prodQY);
	gsl_matrix_free(R);
	gsl_matrix_free(Rinv);
	gsl_matrix_free(poly_equa);
	gsl_permutation_free (p);

	return out;
}
