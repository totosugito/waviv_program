/*
 * spline.h
 *
 *  Created on: May 20, 2010
 *      Author: toto
 */

#ifndef SPLINE_H_
#define SPLINE_H_

#include "../array/array.h"
#include "../datafun/datafun.h"
#include "../sparfun/spdiags.h"
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_spline.h>
#include "chckxy.h"
#include "pwch.h"
#include "ppval.h"

double **spline(double *X, double *Y, int lenX, int lenY);
void setBvar_spline(double *B, double *dx, double *divdif, int n);
void set_tmpspdiags_spline(double **tmpspdiags, double *dx, double x31, double xn, int n);

double *spline_val(double *X, double *Y, int lenX, int lenY, double *T, int nt);
void splinev_val(double *X, double *Y, int lenX, int lenY, double *T, int nt, double *value);

double *spline_gsl_double(double *X, double *Y, double *T, int nx, int nt);
double *spline_gsl_doublei(double *X, double *Y, double *T, int nx, int nt);
void splinev_gsl_doublei(double *X, double *Y, double *T, int nx, int nt, double *value);
#endif /* SPLINE_H_ */
