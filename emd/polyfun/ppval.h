/*
 * ppval.h
 *
 *  Created on: May 21, 2010
 *      Author: toto
 */

#ifndef PPVAL_H_
#define PPVAL_H_

#include "../array/array.h"
//#include "spline.h"

double *ppval(double **S, int rowS, double *breaks, double *X, int nx);
void ppvalv(double **S, int rowS, double *breaks, double *X, int nx, double *value);
#endif /* PPVAL_H_ */
