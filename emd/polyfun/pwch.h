/*
 * pwch.h
 *
 *  Created on: May 19, 2010
 *      Author: toto
 */

#ifndef PWCH_H_
#define PWCH_H_

#include "../array/array.h"

double **pwch_double(double *x, double *y, double *s, int lenxys, double *dx, double *divdif, int lendxdif);
#endif /* PWCH_H_ */
