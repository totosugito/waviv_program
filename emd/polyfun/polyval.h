/*
 * polyval.h
 *
 *  Created on: May 24, 2010
 *      Author: toto
 */

#ifndef POLYVAL_H_
#define POLYVAL_H_
#include "../array/array.h"

double *polyval_double(double *X, double *poly, int nx, int npoly);
double *polyval_int(int *X, double *poly, int nx, int npoly);
#endif /* POLYVAL_H_ */
