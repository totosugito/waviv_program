/*
 * semd.c
 *
 *  Created on: Jan 14, 2010
 *      Author: toto
 */

#include "semd.h"

void semd(double *x, int*t, double *T, int nx, int maxiterasi, int nm_imf, int NBSYM, int spline)
{
	double sx1, sx2, sx, sd;

	bool brkflag;
	int i;
	int lm, lM, nem;
	int vartmp;
	int ner, nzr;
	int k, nbit, NBIt,test,nzm,ext;
	int num_sd;

	int *t_mx, nt_mx, *t_mn, nt_mn, nt_mx1, nt_mn1;
	int *tmpblock, *tmpblock1;
	int *indzer, nindzer;
	int *indmin, nindmin;
	int *indmax, nindmax;
	int *lmin, *tlmin, nlmin;
	int *lmax, *tlmax, nlmax;
	int *rmin, *trmin, nrmin;
	int *rmax, *trmax, nrmax;
	int lsym, rsym;

	double *mlmax, *mlmin, *mrmax, *mrmin;
	double *r, *m;
	double *m_mx, *m_mn;
	double *t_mxd, *t_mnd;
	double *spline1, *spline2;
	double *tmpblockf, *tmpblockf1;

	//	 intitial starting condition
	sd = abs_d(x[0]);
	for (i=1; i<nx; i++)
	{
		if(abs_d(x[i])<sd)
			sd = abs_d(x[i]);
	}
	sd = sd*0.003;

	//	maximum number of extr2ema and zero-crossings in residual
	ner = nx;
	nzr = nx;
	num_sd = 2;

	k = 1;
	nbit = 1; 	/*iterations counter for extraction of 1 mode*/
	NBIt = 1;	/*total iterations counter*/

	//initialization array size
	r = su_calloc1double(nx);
	m = su_calloc1double(nx);
	indzer = su_calloc1int(nx);
	indmin = su_calloc1int(nx);
	indmax = su_calloc1int(nx);
	lmin = su_calloc1int(nx);
	lmax = su_calloc1int(nx);
	tlmin = su_calloc1int(nx);
	tlmax = su_calloc1int(nx);

	rmin = su_calloc1int(nx);
	rmax = su_calloc1int(nx);
	trmin = su_calloc1int(nx);
	trmax = su_calloc1int(nx);

	spline1 = su_calloc1double(nx);
	spline2 = su_calloc1double(nx);

	nindzer = 0;
	nindmin = 0;
	nindmax = 0;
	nlmin = 0;
	nlmax = 0;

	nrmin = 0;
	nrmax = 0;
	memcpy(r, x, nx*sizeof(double));

	while(ner>2)
	{
		memcpy(m, r, nx*sizeof(double));	//current mode

		sx = sd + 1.0;	//initial starting condition
		test = 0;		//tests if enough extrema to proceed

		get_indzer(m, nx, indzer, &nindzer);		//get zeroes break location
		extr(m, nx, indmin, &nindmin, indmax, &nindmax);	//get minimum and maximum break location

		if((nindmin + nindmax)<=NBSYM)
		{
			detrendv_d(m, 0, nx);
			memcpy(x, m, nx*sizeof(double));
			brkflag = 1;
		}
		else
		{
			lm = nindmin;
			lM = nindmax;
			nem = lm+lM;
			nzm = nindzer;
			brkflag = 0;

			while((nbit<=maxiterasi) && (test!=1))
			{
				ext = abs(nem-nzm);
				extr2(m, t, nx, num_sd, indmin, &nindmin, indmax, &nindmax);

				if(((sx<=sd) && (ext<=1))  || (nem<=3))
					test=1;		//end of process EMD

				// boundary conditions for interpolations
				// extend signal head and tail to reduce edge effects of splines
				get_lminmax(indmin, indmax, nindmin, nindmax, m, NBSYM, lmin, &nlmin, lmax, &nlmax, &lsym);
				get_rminmax(indmin, indmax, nindmin, nindmax, m, nx, NBSYM, rmin, &nrmin, rmax, &nrmax, &rsym);

				getTlTr_v(2*t[lsym], t, lmin, NBSYM, tlmin);
				getTlTr_v(2*t[lsym], t, lmax, NBSYM, tlmax);
				getTlTr_v(2*t[rsym], t, rmin, NBSYM, trmin);
				getTlTr_v(2*t[rsym], t, rmax, NBSYM, trmax);


				/*TRY CATCH DEFINITION --> NOT YET IMPLEMENTATED*/
				if((tlmin[0]>t[0]) || (tlmax[0]>t[0]))
				{
					if(lsym == indmax[0])
					{
						nlmax = GSL_MIN_INT(nindmax, NBSYM);
						memcpy(lmax, indmax, nlmax*sizeof(int));
						fliplrv_int(lmax, nlmax);
					}
					else
					{
						nlmin = GSL_MIN_INT(nindmin, NBSYM);
						memcpy(lmin, indmin, nlmin*sizeof(int));
						fliplrv_int(lmin, nlmin);
					}
					if(lsym==0)
					{
						printf("ERROR TRY-CATCH lsym \n");
						exit(0);
					}
					lsym = 0;
					getTlTr_v(2*t[lsym], t, lmin, NBSYM, tlmin);
					getTlTr_v(2*t[lsym], t, lmax, NBSYM, tlmax);
				}

				if((trmin[NBSYM-1]<t[nx-1]) || (trmax[NBSYM-1]<t[nx-1]))
				{
					if(rsym == indmax[nindmax-1])
					{
						vartmp = GSL_MAX_INT(nindmax-NBSYM+1,1);
						nrmax = nindmax - vartmp + 1;

						memcpy(rmax, indmax+vartmp-1, nrmax*sizeof(int));
						fliplrv_int(rmax, nrmax);
					}
					else
					{
						vartmp = GSL_MAX_INT(nindmin-NBSYM+1, 1);
						nrmin = nindmin - vartmp + 1;

						memcpy(rmin, indmin+vartmp-1, nrmin*sizeof(int));
						fliplrv_int(rmin, nrmin);
					}
					if(rsym==(nx-1))
					{
						printf("ERROR TRY-CATCH rsym \n");
						exit(0);
					}
					rsym = nx-1;
					getTlTr_v(2*t[rsym], t, rmin, NBSYM, trmin);
					getTlTr_v(2*t[rsym], t, rmax, NBSYM, trmax);
				}

				mlmin = getArrayValue_double(m, lmin, nx, nlmin);
				mlmax = getArrayValue_double(m, lmax, nx, nlmax);
				mrmin = getArrayValue_double(m, rmin, nx, nrmin);
				mrmax = getArrayValue_double(m, rmax, nx, nrmax);

				/*compute t_mx and m_mx*/
				/*=======================================================*/
				tmpblock = getArrayValue_int(t, indmax, nx, nindmax);
				t_mx = join3array_int(tlmax, tmpblock, trmax, nlmax, nindmax, nrmax);
				nt_mx1 = nlmax + nindmax + nrmax;
				su_free1int(tmpblock);

				tmpblockf = getArrayValue_double(m, indmax, nx, nindmax);
				m_mx = join3array_double(mlmax, tmpblockf, mrmax, nlmax, nindmax, nrmax);
				su_free1double(tmpblockf);

				/*compute t_mn and m_mn*/
				tmpblock1 = getArrayValue_int(t, indmin, nx, nindmin);
				t_mn = join3array_int(tlmin, tmpblock1, trmin, nlmin, nindmin, nrmin);
				nt_mn1 = nlmin + nindmin + nrmin;
				if(tmpblock1!=NULL) free(tmpblock1);

				tmpblockf1 = getArrayValue_double(m, indmin, nx, nindmin);
				m_mn = join3array_double(mlmin, tmpblockf1, mrmin, nlmin, nindmin, nrmin);
				if(tmpblockf1) su_free1double(tmpblockf1);
				/*=======================================================*/

				/*get unique data*/
				uniquev_int(t_mx, nt_mx1, &nt_mx);
				uniquev_int(t_mn, nt_mn1, &nt_mn);
				t_mxd = array_int2double(t_mx, nt_mx);
				t_mnd = array_int2double(t_mn, nt_mn);
				//-------------------------------------------------------
				//CHANGE METODE SPLINE
				//-------------------------------------------------------
				if(spline==1)
				{
					splinev_gsl_doublei(t_mnd, m_mn, T, nt_mn, nx, spline1);
					splinev_gsl_doublei(t_mxd, m_mx, T, nt_mx, nx, spline2);
				}
				else
				{
					splinev_val(t_mnd, m_mn, nt_mn, nt_mn, T, nx, spline1);
					splinev_val(t_mxd, m_mx, nt_mx, nt_mx, T, nx, spline2);
				}
				arrayMidv_double(spline1, spline2, nx);

				/*generate stopping criterion statistic*/
				sx1 = norm_double(m, nx)/nx;
				arrayOperationArray_double(m, spline1, nx, 2);
				sx2 = norm_double(m, nx)/nx;
				sx = sx1-sx2;


				/*determine if enough extrema to continue sifting*/
				get_indzer(m, nx, indzer, &nindzer);
				extr(m, nx, indmin, &nindmin, indmax, &nindmax);

				lm = nindmin;
				lM = nindmax;
				nem = lm+lM;
				nzm = nindzer;
				nbit=nbit+1;

				/*free allocated memory*/
				su_free1double(mlmin);
				su_free1double(mlmax);
				su_free1double(mrmin);
				su_free1double(mrmax);

				su_free1int(t_mx);
				su_free1int(t_mn);
				su_free1double(m_mx);
				su_free1double(m_mn);

				su_free1double(t_mxd);
				su_free1double(t_mnd);
			}
			k = k+1;
			arrayOperationArray_double(r, m, nx, 2);

			/*determine if enough extrema to continue sifting*/
			get_indzer(m, nx, indzer, &nindzer);
			extr(m, nx, indmin, &nindmin, indmax, &nindmax);

			lm = nindmin;
			lM = nindmax;
			ner = lm+lM;
			nzr = nindzer;
			nbit= 1;
			if(k>nm_imf)
			{
//				memcpy(x, m, nx*sizeof(double));
				memcpy(x, r, nx*sizeof(double));
				break;
			}
		}


		if(brkflag==1)
			break;
	}

	su_free1double(r);
	su_free1double(m);
	su_free1int(indzer);
	su_free1int(indmin);
	su_free1int(indmax);
	su_free1int(lmin);
	su_free1int(lmax);
	su_free1int(tlmin);
	su_free1int(tlmax);

	su_free1int(rmin);
	su_free1int(rmax);
	su_free1int(trmin);
	su_free1int(trmax);

	su_free1double(spline1);
	su_free1double(spline2);
}

void get_lminmax(int *indmin, int *indmax, int nindmin, int nindmax, double *m, int NBSYM,
		int *lmin, int *nlmin1, int *lmax, int *nlmax1, int *lsym1)
{
	int b1, b2;
	int nlmax, nlmin, lsym;

	if(indmax[0] < indmin[0])
	{
		if(m[0] > m[indmin[0]])
		{
			b1 = GSL_MIN_INT(nindmax,NBSYM+1);
			b2 = GSL_MIN_INT(nindmin,NBSYM);
			nlmax = b1-1;
			nlmin = b2;
			lsym = indmax[0];

			memcpy(lmax, indmax+1, nlmax*sizeof(int));
			fliplrv_int(lmax, nlmax);

			memcpy(lmin, indmin, nlmin*sizeof(int));
			fliplrv_int(lmin, nlmin);
		}
		else
		{
			b1 = GSL_MIN_INT(nindmax, NBSYM);
			b2 = GSL_MIN_INT(nindmin, NBSYM-1);
			nlmax = b1;
			nlmin = b2+1;
			lsym = 0;

			memcpy(lmax, indmax, nlmax*sizeof(int));
			fliplrv_int(lmax, nlmax);

			lmin[b2] = 0;
			memcpy(lmin , indmin, b2*sizeof(int));
			fliplrv_int(lmin, b2);
		}
	}

	else
	{
		if(m[0] < m[indmax[0]])
		{
			b1 = GSL_MIN_INT(nindmax, NBSYM);
			b2 = GSL_MIN_INT(nindmin, NBSYM+1);
			nlmax = b1;
			nlmin = b2-1;
			lsym = indmin[0];

			memcpy(lmax, indmax, nlmax*sizeof(int));
			fliplrv_int(lmax, nlmax);

			memcpy(lmin, indmin+1, nlmin*sizeof(int));
			fliplrv_int(lmin, nlmin);
		}
		else
		{
			b1 = GSL_MIN_INT(nindmax, NBSYM-1);
			b2 = GSL_MIN_INT(nindmin, NBSYM);
			nlmax = b1+1;
			nlmin = b2;
			lsym = 0;

			lmax[b1] = 0;
			memcpy(lmax, indmax, b1*sizeof(int));
			fliplrv_int(lmax, b1);

			memcpy(lmin, indmin, nlmin*sizeof(int));
			fliplrv_int(lmin, nlmin);
		}
	}

	(*nlmax1) = nlmax;
	(*nlmin1) = nlmin;
	(*lsym1) = lsym;
}

void get_rminmax(int *indmin, int *indmax, int nindmin, int nindmax, double *m, int nm, int NBSYM,
		int *rmin, int *nrmin1, int *rmax, int *nrmax1, int *rsym1)
{
	int b1, b2;
	int nrmin, nrmax, rsym;

	if(indmax[nindmax-1] < indmin[nindmin-1])
	{
		if(m[nm-1] < m[indmax[nindmax-1]])
		{
			b1 = GSL_MAX_INT(nindmax-NBSYM+1, 1);
			b2 = GSL_MAX_INT(nindmin-NBSYM, 1);

			nrmax = nindmax - b1 + 1;
			nrmin = nindmin - b2;

			memcpy(rmax, indmax+b1-1, nrmax * sizeof(int));
			fliplrv_int(rmax, nrmax);

			memcpy(rmin, indmin+b2-1, nrmin * sizeof(int));
			fliplrv_int(rmin, nrmin);
			rsym = indmin[nindmin-1];
		}
		else
		{
			b1 = GSL_MAX_INT(nindmax-NBSYM+2, 1);
			b2 = GSL_MAX_INT(nindmin-NBSYM+1, 1);

			nrmin = nindmin - b2+1;
			nrmax = (nindmax - b1+1) + 1;

			rmax[0] = nm-1;
			memcpy(rmax+1, indmax+b1-1, (nrmax-1) * sizeof(int));
			fliplrv1_int(rmax, 1, nrmax);

			memcpy(rmin, indmin+b2-1, nrmin * sizeof(int));
			fliplrv_int(rmin, nrmin);
			rsym = nm-1;
		}
	}

	else
	{
		if(m[nm-1] > m[indmax[nindmax-1]])
		{
			b1 = GSL_MAX_INT(nindmax-NBSYM, 1);
			b2 = GSL_MAX_INT(nindmin-NBSYM+1, 1);

			nrmax = nindmax - b1;
			nrmin = nindmin - b2 + 1;

			memcpy(rmax, indmax+b1-1, nrmax * sizeof(int));
			fliplrv_int(rmax, nrmax);

			memcpy(rmin, indmin+b2-1, nrmin * sizeof(int));
			fliplrv_int(rmin, nrmin);
			rsym = indmax[nindmax-1];
		}
		else
		{
			b1 = GSL_MAX_INT(nindmax-NBSYM+1,1);
			b2 = GSL_MAX_INT(nindmin-NBSYM+2,1);

			nrmax = nindmax - b1 + 1;
			nrmin = (nindmin - b2 + 1) + 1;

			memcpy(rmax, indmax+b1-1, nrmax*sizeof(int));
			fliplrv_int(rmax, nrmax);

			rmin[0] = nm-1;
			memcpy(rmin+1, indmin+b2-1, (nrmin-1) * sizeof(int));
			fliplrv1_int(rmin, 1, nrmin);
			rsym = nm-1;
		}
	}

	(*nrmin1) = nrmin;
	(*nrmax1) = nrmax;
	(*rsym1) = rsym;
}

int *getTlTr(int var1, int *data, int *boundary, int nboundary)
{
	int *out=NULL;
	int i;

	out = su_ealloc1int(nboundary);
	for(i=0; i<nboundary; i++)
	{
		out[i] = var1 - data[boundary[i]];
	}
	return (out);
}

void getTlTr_v(int var1, int *data, int *boundary, int nboundary, int *out)
{
	int i;

	for(i=0; i<nboundary; i++)
	{
		out[i] = var1 - data[boundary[i]];
	}
}
