/*
 * semd.h
 *
 *  Created on: Jan 18, 2010
 *      Author: toto
 */

#ifndef SEMD_H_
#define SEMD_H_

#include "extr.h"
#include "polyfun/spline.h"
#include "elmat/fliplr.h"
#include "array/array.h"
#include "ops/ops.h"
#include "matfun/norm.h"

void semd(double *x, int*t, double *T, int nx, int maxiterasi, int nm_imf, int NBSYM, int spline);
void get_lminmax(int *indmin, int *indmax, int nindmin, int nindmax, double *m, int NBSYM,
		int *lmin, int *nlmin1, int *lmax, int *nlmax1, int *lsym1);
void get_rminmax(int *indmin, int *indmax, int nindmin, int nindmax, double *m, int nm, int NBSYM,
		int *rmin, int *nrmin1, int *rmax, int *nrmax1, int *rsym1);
int *getTlTr(int var1, int *data, int *boundary, int nboundary);
void getTlTr_v(int var1, int *data, int *boundary, int nboundary, int *out);
#endif /* SEMD_H_ */
