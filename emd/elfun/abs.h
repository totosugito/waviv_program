/*
 * abs_int.h
 *
 *  Created on: May 24, 2010
 *      Author: toto
 */

#ifndef ABS_H_
#define ABS_H_

#include "../array/array.h"

int *abs1_i(int *x, int nx);
float *abs1_f(float *x, int nx);
double *abs1_d(double *x, int nx);

int abs_i(int x);
float abs_f(float x);
double abs_d(double x);
#endif /* ABS_INT_H_ */
