/*
 * fix.h
 *
 *  Created on: Jan 19, 2011
 *      Author: toto
 */

#ifndef FIX_H_
#define FIX_H_

#include "abs.h"
#include <math.h>

int fix_f(float A);
int fix_d(double A);
#endif /* FIX_H_ */
