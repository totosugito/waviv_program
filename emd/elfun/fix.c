/*
 * fix.c
 *
 *  Created on: Jan 19, 2011
 *      Author: toto
 */
//FIX    Round towards zero.
//   FIX(X) rounds the elements of X to the nearest integers
//   towards zero.
//
//   Copyright 1984-2005 The MathWorks, Inc.
//   $Revision: 5.7.4.4 $  $Date: 2005/06/21 19:28:01 $
//   Built-in function.

#include "fix.h"

int fix_f(float A)
{
	int sign;
	int fout, out;

	sign = A<0 ? -1:1;
	fout = abs_f(A);
	out = floor(A) * sign;

	return (out);
}

int fix_d(double A)
{
	int sign;
	int fout, out;

	sign = A<0 ? -1:1;
	fout = abs_f(A);
	out = floor(A) * sign;

	return (out);
}
