/*
 * abs_int.c
 *
 *  Created on: May 24, 2010
 *      Author: toto
 */
//ABS    Absolute value.
//   ABS(X) is the absolute value of the elements of X. When
//   X is complex, ABS(X) is the complex modulus (magnitude) of
//   the elements of X.

#include "abs.h"

int *abs1_i(int *x, int nx)
{
	int i;
	int *out=NULL;

	out = su_ealloc1int(nx);

	for (i=0; i<nx; i++)
	{
		out[i] = abs_i(x[i]);
	}
	return out;
}

float *abs1_f(float *x, int nx)
{
	int i;
	float *out=NULL;

	out = su_ealloc1float(nx);

	for (i=0; i<nx; i++)
	{
		out[i] = abs_f(x[i]);
	}
	return out;
}

double *abs1_d(double *x, int nx)
{
	int i;
	double *out=NULL;

	out = su_ealloc1double(nx);

	for (i=0; i<nx; i++)
	{
		out[i] = abs_d(x[i]);
	}
	return out;
}

int abs_i(int x)
{
	if(x<0)
		return(x*(-1));
	else
		return x;
}

float abs_f(float x)
{
	if(x<0)
		return(x*(-1));
	else
		return x;
}

double abs_d(double x)
{
	if(x<0.0)
		return(x*(-1));
	else
		return x;
}
