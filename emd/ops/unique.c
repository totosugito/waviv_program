/*
 * unique.c
 *
 *  Created on: May 24, 2010
 *      Author: toto
 */
//UNIQUE Set unique.
//   B = UNIQUE(A) for the array A returns the same values as in A but
//   with no repetitions. B will also be sorted.

#include "unique.h"

int *unique_int(int *x,int nx, int *nout)
{
	int *out;

	int *tmpout;
	int i,k;
	int tmp;

	k = 0;

	tmpout = su_ealloc1int(nx);

	/* sort data */
	gsl_sort_int(x,1,nx);

	tmp = x[k];
	tmpout[k] = tmp;
	k = k+1;
	for(i=1; i<nx; i++)
	{
		if(x[i]!=tmp)
		{
			tmpout[k] = x[i];
			tmp = tmpout[k];
			k = k+1;
		}
	}
	out = su_ealloc1int(k);

	(*nout) = k;
	memcpy(out,tmpout,k*sizeof(int));
	su_free1int(tmpout);
	return out;
}

void uniquev_int(int *x,int nx, int *nout)
{
	int *tmpout;
	int i,k;
	int tmp;

	k = 0;

	tmpout = su_ealloc1int(nx);

	/* sort data */
	gsl_sort_int(x,1,nx);

	tmp = x[k];
	tmpout[k] = tmp;
	k = k+1;
	for(i=1; i<nx; i++)
	{
		if(x[i]!=tmp)
		{
			tmpout[k] = x[i];
			tmp = tmpout[k];
			k = k+1;
		}
	}

	(*nout) = k;
	memset(x, '\0', nx*sizeof(int));
	memcpy(x, tmpout, k*sizeof(int));
	su_free1int(tmpout);
}

double *unique_double(double *x,int nx, int *nout)
{
	double *out;

	double *tmpout;
	int i,k;
	int tmp;

	k = 0;

	tmpout = su_ealloc1double(nx);

	/* sort data */
	gsl_sort(x,1,nx);

	tmp = x[k];
	tmpout[k] = tmp;
	k = k+1;
	for(i=1; i<nx; i++)
	{
		if(x[i]!=tmp)
		{
			tmpout[k] = x[i];
			tmp = tmpout[k];
			k = k+1;
		}
	}
	out = su_ealloc1double(k);

	(*nout) = k;
	memcpy(out,tmpout,k*sizeof(double));
	su_free1double(tmpout);
	return out;
}
