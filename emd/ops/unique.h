/*
 * unique.h
 *
 *  Created on: May 24, 2010
 *      Author: toto
 */

#ifndef UNIQUE_H_
#define UNIQUE_H_

#include "../array/array.h"
#include <gsl/gsl_sort.h>

int *unique_int(int *x,int nx, int *nout);
void uniquev_int(int *x,int nx, int *nout);
double *unique_double(double *x,int nx, int *nout);
#endif /* UNIQUE_H_ */
