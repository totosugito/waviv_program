/*
 * any.h
 *
 *  Created on: May 24, 2010
 *      Author: toto
 */

#ifndef ANY_H_
#define ANY_H_

#include <stdio.h>
#include <stdbool.h>

bool any_int(int *data, int ndata);
bool any_double(double *data, int ndata);

bool anyequal_int(int *data, int ndata, int value);
bool anyequal_double(double *data, int ndata, double value);
#endif /* ANY_H_ */
