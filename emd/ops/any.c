/*
 * any.c
 *
 *  Created on: May 24, 2010
 *      Author: toto
 */
//ANY    True if any element of a vector is a nonzero number or is
//   logical 1 (TRUE).  ANY ignores entries that are NaN (Not a Number).
//
//   For vectors, ANY(V) returns logical 1 (TRUE) if any of the
//   elements of the vector is a nonzero number or is logical 1 (TRUE).
//   Otherwise it returns logical 0 (FALSE).  For matrices, ANY(X)
//   operates on the columns of X, returning a row vector of logical 1's
//   and 0's.  For multi-dimensional arrays, ANY(X) operates on the
//   first non-singleton dimension.

#include "any.h"

bool any_int(int *data, int ndata)
{
	int i;

	for(i=0; i<ndata; i++)
	{
		if(data[i]>0)
		{
			return 1;
			break;
		}
	}
	return 0;
}

bool any_double(double *data, int ndata)
{
	int i;

	for(i=0; i<ndata; i++)
	{
		if(data[i]>0.0)
		{
			return 1;
			break;
		}
	}
	return 0;
}

bool anyequal_int(int *data, int ndata, int value)
{
	int i;

	for(i=0; i<ndata; i++)
	{
		if(data[i]==value)
		{
			return 1;
			break;
		}
	}
	return 0;
}

bool anyequal_double(double *data, int ndata, double value)
{
	int i;

	for(i=0; i<ndata; i++)
	{
		if(data[i]==value)
		{
			return 1;
			break;
		}
	}
	return 0;
}
