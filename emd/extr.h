/*
 * extr.h
 *
 *  Created on: Jan 17, 2010
 *      Author: toto
 */

#ifndef EXTR_H_
#define EXTR_H_
#include "ops/ops.h"
#include "elmat/find.h"
#include "elmat/fliplr.h"
#include "datafun/datafun.h"
#include "elfun/abs.h"
#include "polyfun/polyfit.h"
#include "polyfun/polyval.h"
#include <gsl/gsl_statistics.h>

#define roundfunc(x) ((int)((x)>0.0?(x)+0.5:(x)-0.5));
void get_indzer(double *x, int nx, int *indzer, int *nindzer);
void extr(double *x, int nx, int *indmin, int *nindmin, int *indmax, int *nindmax);
void extr2(double *x, int *t, int nx, int num_sd,
		int *indmin, int *nindmin, int *indmax, int *nindmax);
#endif /* EXTR_H_ */
