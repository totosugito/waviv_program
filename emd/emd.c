/*
 * emd.c
 *
 *  Created on: Mar 20, 2013
 *      Author: toto
 */
#include "emd.h"
#define LOOKFAC	2	/* Look ahead factor for npfaro	  */

void run_emd(float **tdatain, int ntrc, int nsp, float dt,
		float _taper, float _toptaper, int _splittime, int _splinetype,
		int _nbsym, int _nimf, int _maxiter, int _verbose, int _tapermethod,
		int _rescaling)
{
	int jw;
	int uptaper, taperval;
	int ndtw;		/* number of samples in time window     */
	int ndtwspc;		/* 50% number of samples in time window     */
	int ntw;	       	/* number of time windows		*/
	int nspw;
	int *t;				//create time axis
	double *T;
	int nf, nfft;
	float d1;
	int nspws;	       	/* number of samples in window (wo taper)*/
	int nspwf;	       	/* number of samples in first window    */
	int nstaper;
	float twlen;

	uptaper = (int) (_toptaper/dt);
	taperval = (int) (_taper/dt);

	if(_splittime==1)
	{
		/* setting taper and spatial and temporal windows */
		nstaper=NINT(_taper/dt);
		nstaper = (nstaper%2 ? nstaper+1 : nstaper);
		ndtw = 512/round(dt*1000.0);
		ndtwspc = ndtw/2;
		twlen = (float) (ndtw-1)*dt;

		ntw = NINT(nsp/ndtwspc);
		if((ntw*ndtwspc)+ndtw > nsp)
			ntw = ntw-1;

		nspws = NINT(twlen/dt) + 1;
		nspwf = nspws + nstaper/2;
	}
	else
	{
		nstaper=NINT(_taper/dt);
		nstaper = (nstaper%2 ? nstaper+1 : nstaper);
		twlen=(float)(nsp-1)*dt;
		ntw = NINT((nsp-1)*dt/twlen);
		ndtw = nsp;
		ndtwspc = 0;

		nspws = NINT(twlen/dt) + 1;
		nspwf = nspws + nstaper/2;
	}

	if (ntw==1)
		_taper=0.0;

	/* Set up pfa fft */
	nfft = npfaro(2*nspwf, LOOKFAC * 2*nspwf);
	nf = nfft/2 + 1;
	d1 = 1.0/(nfft*dt);
	nspw = nsp;

	t = alloc1int(ntrc);
	T = alloc1double(ntrc);
	for(jw=0; jw<ntrc; jw++)
	{
		t[jw] = jw+1;
		T[jw] = (double) (jw+1);
	}

	EMD_Freq_Process(tdatain, ntrc, nsp,
			nfft, nf, nspw, ntw, _splittime, ndtwspc, ndtw,
			_splinetype, _nbsym, _nimf, _maxiter, t, T,
			_verbose, uptaper, taperval, _tapermethod, _rescaling);

	free1int(t);
	free1double(T);
}

void EMD_Freq_Process(float **tdatain, int ntrc, int nsp,
		int nfft, int nf, int nspw, int ntw, int splittime, int ndtwspc, int ndtw,
		int splinety, int nbsym, int nimf, int maxiter, int *t, double *T,
		int verbose, int uptaper, int taperval,
		int tapermeth, int rescaling)
{
	int iw, it, jw, ifq, itt, itr;			//index loop
	int b1, b2;		//temporary variable

	complex *ct;		/* complex transformed trace (window)  	*/
	float *tidataw;       	/* real trace in time window - input   	*/
	float *ttidataw;      	/* real trace in time window - input   	*/
	complex **fdata;	/* data - freq. domain          	*/
	double *datareal, *dataimag;
	complex **ffreq;	/* filtered frequency vector 	  	*/

	float **tdataout;     	/* real trace			   	*/
	float *todataw;       	/* real trace in time window - output   */
	complex *freqv;		/* frequency vector		      	*/
	float *ttodataw;      	/* real trace in time window - output   */

	float tmpd1, tmpd2;		//temporary data
	int i;
	float *tracer, *tracei, *trreal, *trimag;

	trreal = alloc1float(nf);
	trimag = alloc1float(nf);

	ct = alloc1complex(nfft);
	tidataw = alloc1float(nfft);
	ttidataw = alloc1float(nfft);
	fdata = alloc2complex(nf,ntrc);
	datareal = alloc1double(ntrc);
	dataimag = alloc1double(ntrc);
	ffreq = alloc2complex(nf,ntrc);

	todataw = alloc1float(nfft);
	tdataout = alloc2float(nsp,ntrc);
	freqv = alloc1complex(nfft);
	ttodataw = alloc1float(nfft);

	/* loop over time windows */
	//================TIME WINDOWING======================
	for (iw=0;iw<ntw;iw++)
	{
		if(splittime==1)
		{
			b1 = ndtwspc*iw;
			b2 = b1 + ndtw/*- 1*/;
		}
		else
		{
			b1 = 0;
			b2 = nsp;
		}

		if((verbose)&&(splittime))
			printf("Proses Time Window %i - %i\n", b1, b2);

		/* zero fdata */
		memset((void *) fdata[0], 0, nf*ntrc*sizeof(complex));

		/* select data */
		for (jw=0;jw<ntrc;jw++)
		{
			if(splittime==1)
			{
				for (it=b1;it<b2;it++){
					tidataw[it-b1]=tdatain[jw][it];
				}
			}
			else
			{
				for (it=b1;it<b2;it++){
					tidataw[it]=tdatain[jw][it];
				}
			}
			memset((void *) (tidataw + ndtw), 0, (nfft-ndtw)*FSIZE);
			memset((void *) ct, 0, nfft*sizeof(complex));

			/* FFT from t to f */
			for (it=0;it<nfft;it++)
				ttidataw[it]=(it%2 ? -tidataw[it] : tidataw[it]);
			pfarc(1, nfft, ttidataw, ct);

			/* Store values */
			for (ifq = 0; ifq < nf; ifq++)
			{
				if(splittime==1)
				{
					if(ifq<nf*0.6)
						fdata[jw][ifq] = ct[nf-1-ifq];
					else
					{
						fdata[jw][ifq].r = 0.0;
						fdata[jw][ifq].i = 0.0;
					}
				}
				else
				{
					fdata[jw][ifq] = ct[nf-1-ifq];
				}
			}
		}

		/*=============================================================================
		 * EMD FREQUENCY PROCESS
	=============================================================================*/
		for (ifq=0; ifq<nf; ifq++)
		{
			memset((void *) datareal, 0, (ntrc)*sizeof(double));
			memset((void *) dataimag, 0, (ntrc)*sizeof(double));
			for (jw=0;jw<ntrc;jw++)
			{
				datareal[jw] = (double) fdata[jw][ifq].r;
				dataimag[jw] = (double) fdata[jw][ifq].i;
			}

			if(ntrc<4){
				for (jw=0;jw<ntrc;jw++)
				{
					ffreq[jw][ifq].r = (float) datareal[jw];
					ffreq[jw][ifq].i = (float) dataimag[jw];
				}
			}
			else
			{
				if(check_zeroes(datareal, ntrc) == 0)
					semd(datareal, t, T, ntrc, maxiter, nimf, nbsym, splinety);
				else
					memset(datareal, 0, ntrc*sizeof(double));

				if(check_zeroes(dataimag, ntrc) == 0)
					semd(dataimag, t, T, ntrc, maxiter, nimf, nbsym, splinety);
				else
					memset(dataimag, 0, ntrc*sizeof(double));

				for (jw=0;jw<ntrc;jw++)
				{
					ffreq[jw][ifq].r = (float) datareal[jw];
					ffreq[jw][ifq].i = (float) dataimag[jw];
				}
			}
		}

		/* loop along space windows */
		for (jw=0;jw<ntrc;jw++)
		{
			/* select data */
			for (ifq=0,itt=nf-1;ifq<nf;ifq++,itt--)
			{
				freqv[ifq] = ffreq[jw][itt];
			}

			if(tapermeth!=0)
			{
				for(i=0; i<nf; i++)
				{
					trreal[i] = freqv[i].r;
					trimag[i] = freqv[i].i;
				}
				tracer = setMethodTaper(trreal, 1, uptaper, tapermeth, nf, taperval);
				tracei = setMethodTaper(trimag, 1, uptaper, tapermeth, nf, taperval);
				for(i=0; i<nf; i++)
				{
					freqv[i].r = tracer[i];
					freqv[i].i = tracei[i];
				}

				free1float(tracer);
				free1float(tracei);
			}

			memset((void *) (freqv+nf), 0, (nfft-nf)*sizeof(complex));
			memset((void *) todataw, 0, nfft*FSIZE);

			/* FFT back from f to t and scaling */
			pfacr(-1, nfft, freqv, todataw);
			for (it=0;it<MIN(ndtw,nfft);it++)
				todataw[it]/=nfft;
			for (it=0;it<MIN(ndtw,nfft);it++)
			{
				ttodataw[it]=(it%2 ? -todataw[it] : todataw[it]);
			}

			if(splittime==1)
			{
				for (it=0;it<ndtwspc;it++)
				{
					if(it<ndtwspc)
						tdataout[jw][ndtwspc*iw+it]=ttodataw[it];
				}
			}
			else
			{
				for (it=0;it<nsp;it++)
					tdataout[jw][it]=ttodataw[it];
			}

			memset((void *) freqv, 0, nfft*sizeof(complex));
		} /* end loop over space windows */
	}

	/*=============================================================================
			CHECK DATA INPUT AND OUTPUT
	*=============================================================================*/
	if(rescaling)
	{
		for(jw=0; jw<ntrc; jw++)
		{
			for(it=0; it<nsp; it++)
			{
				tmpd1 = ABS(tdatain[jw][it]);
				tmpd2 = ABS(tdataout[jw][it]);
				if(tmpd1<tmpd2)
					tdataout[jw][it] = tdatain[jw][it];
			}
		}
	}

	for(itr=0; itr<ntrc; itr++)
	{
		//stPutTrace(tdataout[itr], header[itr]);
		memcpy(tdatain[itr], tdataout[itr], nsp*sizeof(float));
	}

//	memset((void *) tdatain[0], 0, nsp*ntrc*sizeof(float));	//set trace to zeros
//	memset((void *) header[0], 0, hnth*ntrc*sizeof(int));  //set header trace to zeros

	/* Free memory */
	/* memFree functions check for memory overruns and
	 * abort if an overrun is found */
	free1float(trreal);
	free1float(trimag);
	free1complex(ct);
	free1float(tidataw);
	free1float(ttidataw);
	free2complex(fdata);
	free1double(datareal);
	free1double(dataimag);
	free2complex(ffreq);
	free1float(todataw);
	free2float(tdataout);
	free1complex(freqv);
	free1float(ttodataw);
}


float *setMethodTaper(float *Data, int lt, int gt, int method, int nt, int n_taper)
{
	int n_lt, n_gt;
	int ncoef, tmpsize1;
	float *tmpcoef, *coef, *tmpcoef1, *tmpcoef2, *out;
	tmpcoef = su_ealloc1float(nt);

	out = su_ealloc1float(nt);
	memcpy(out, Data, nt*sizeof(float));
	n_lt = lt;
	n_gt = nt-gt;

	// mute
	if (method	== 1)
	{
		memset(out, 0, n_lt*sizeof(float));
		memset(out+(n_gt-1), 0, (nt-n_gt)*sizeof(float));
	}
	// mute and taper
	else if (method	== 2)
	{
		coef = cos_taper(1,n_taper,1, &ncoef);

		memcpy(tmpcoef, out+n_lt, ncoef*sizeof(float));
		tmpcoef1 = VectorOperation_d1(tmpcoef, coef, ncoef, 1.0);

		memset(tmpcoef, 0, ncoef*sizeof(float));

		tmpsize1 = n_gt-n_taper;
		memcpy(tmpcoef, out+tmpsize1, ncoef*sizeof(float));
		//tmpcoef2 =VectorOperation_f(tmpcoef, coef, ncoef, 3);
		tmpcoef2 = arrayOperationArrayo_float(tmpcoef, coef, ncoef, 3);
		memcpy(out+tmpsize1, tmpcoef2, ncoef*sizeof(float));
		memset(out+n_gt, 0, (nt-n_gt)*sizeof(float));

		if(tmpcoef1!=NULL) free(tmpcoef1);
		if(tmpcoef2!=NULL) free(tmpcoef2);
		if(coef!=NULL) free(coef);
	}

	free(tmpcoef);
	return out;
}

float *cos_taper(int sp, int ep, int samp, int *ncostaper)
//COS_TAPER: used by KIRK_MIG
//coefficients of cos taper
//
//coef=cos_taper(sp,ep,samp)
//
//coef:  coefficient with length (start-end)/samp + 1
//sp:    start point to begin taper
//ep:    end point to stop taper
//samp:  the sample interval; default is 1.
//
//By Xinxiang Li, CREWES project, U of C.
//FEB. 1996
{
	int len;
	float dd;
	float *coef;
	int i;

	if(samp<0)
		samp = samp*(-1);

	len = abs(ep-sp)/samp;
	len = len+1;

	if (len <= 1)
	{
		coef = su_ealloc1float(1);
		coef[0] = 1.0;
	}
	else if (len > 1)
	{
		coef = su_ealloc1float(len);
	}

	dd = 1.0/(len-1)*PI*0.5;
	for (i=0; i<len; i++)
		coef[i] = cos(i*dd);

	(*ncostaper) = len;
	return coef;
}

float *VectorOperation_d1(float *data1, float *data2, int ndata, float var)
{
	float *out;
	int i;

	out = su_ealloc1float(ndata);
	for (i=0; i<ndata; i++)
	{
		out[i] = data1[i] * (var - data2[i]);
	}

	return(out);
}

bool check_zeroes(double *data, int nx)
{
	int i;
	bool out;
	out = true;
	for(i=0; i<nx; i++)
	{
		if((data[i]>-1e-37) || (data[i]<1e-38))
		{
			return false;
		}
	}
	return true;
}
