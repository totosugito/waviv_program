/*
 * emd.h
 *
 *  Created on: Mar 20, 2013
 *      Author: toto
 */

#ifndef EMD_H_
#define EMD_H_
#include "semd.h"

void run_emd(float **tdatain, int ntrc, int nsp, float dt,
		float _taper, float _toptaper, int _splittime, int _splinetype,
		int _nbsym, int _nimf, int _maxiter, int _verbose, int _tapermethod,
		int _rescaling);
void EMD_Freq_Process(float **tdatain, int ntrc, int nsp,
		int nfft, int nf, int nspw, int ntw, int splittime, int ndtwspc, int ndtw,
		int splinety, int nbsym, int nimf, int maxiter, int *t, double *T,
		int verbose, int uptaper, int taperval,
		int tapermeth, int rescaling);
float *setMethodTaper(float *Data, int lt, int gt, int method, int nt, int n_taper);
float *cos_taper(int sp, int ep, int samp, int *ncostaper);
float *VectorOperation_d1(float *data1, float *data2, int ndata, float var);
bool check_zeroes(double *data, int nx);
#endif /* EMD_H_ */
