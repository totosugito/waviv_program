/*
 * randSu.h
 *
 *  Created on: Jan 19, 2011
 *      Author: toto
 */

#ifndef RANDSU_H_
#define RANDSU_H_

#include "../array/su_ealloc.h"
#include "frannor.h"
#include "franuni.h"
#include <stdio.h>
#include <time.h>

double rand1(int itype, int iseed, unsigned int vseed);
double *rand2(int ndata, int itype, int iseed, unsigned int vseed);
#endif /* RANDSU_H_ */
