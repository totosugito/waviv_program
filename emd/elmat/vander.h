/*
 * vander.h
 *
 *  Created on: May 24, 2010
 *      Author: toto
 */

#ifndef VANDER_H_
#define VANDER_H_
#include <gsl/gsl_matrix.h>
#include <math.h>

gsl_matrix *vander_gsl_double(double *datax, int row, int col);
gsl_matrix *vander_gsl_int(int *datax, int row, int col);
#endif /* VANDER_H_ */
