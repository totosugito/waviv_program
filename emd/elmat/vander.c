/*
 * vander.c
 *
 *  Created on: May 24, 2010
 *      Author: toto
 */
/*
		Produces the Vandermonde matrix based on vector x.
		If vector x has the components [x(1) x(2) ... x(n)],
		the corresponding Vandermonde matrix is written as:
		     [  1   x(1)  x(1)^2  x(1)^3 ... x(1)^(n-1) ]
		     [  1   x(2)  x(2)^2  x(2)^3 ... x(2)^(n-1) ]
		     [  .    .     .       .     ...  .         ]
		     [  .    .     .       .     ...  .         ]
		     [  1   x(n)  x(n)^2  x(n)^3 ... x(n)^(n-1) ]
 */
#include "vander.h"

gsl_matrix *vander_gsl_double(double *datax, int row, int col)
{
	int i,j,k;

	/*definition empty array for array Vandermonde*/
	gsl_matrix *V = gsl_matrix_alloc(row,col);


	/*create Vandermonde array*/
	for (i=0; i<row; i++)
	{
		k = col-1;
		for(j=0; j<col; j++)
		{
			gsl_matrix_set(V,i,j,pow(datax[i],k));
			k = k-1;
		}
	}
	return(V);
}

gsl_matrix *vander_gsl_int(int *datax, int row, int col)
{
	int i,j,k;
	double tmp;

	/*definition empty array for array Vandermonde*/
	gsl_matrix *V = gsl_matrix_alloc(row,col);


	/*create Vandermonde array*/
	for (i=0; i<row; i++)
	{
		k = col-1;
		for(j=0; j<col; j++)
		{
			tmp = (double) pow(datax[i],k);
			gsl_matrix_set(V,i,j, tmp);
			k = k-1;
		}
	}
	return(V);
}
