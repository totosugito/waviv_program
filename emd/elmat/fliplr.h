/*
 * fliplr.h
 *
 *  Created on: May 24, 2010
 *      Author: toto
 */

#ifndef FLIPLR_H_
#define FLIPLR_H_
#include "../array/array.h"

int *fliplr_int(int *A, int nx);
void fliplrv_int(int *A, int nx);
void fliplrv1_int(int *A, int pos0, int nx);
double *fliplr_double(double *A, int nx);
#endif /* FLIPLR_H_ */
