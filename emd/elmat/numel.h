/*
 * numel.h
 *
 *  Created on: May 19, 2010
 *      Author: toto
 */

#ifndef NUMEL_H_
#define NUMEL_H_
#include <stdio.h>

int numel2_double(double **A, int row, int col);
int numel1_double(double *A, int lenA);

#endif /* NUMEL_H_ */
