/*
 * numel.c
 *
 *  Created on: May 19, 2010
 *      Author: toto
 */
//NUMEL   Number of elements in an array or subscripted array expression.
//   N = NUMEL(A) returns the number of elements, N, in array A.
//
//   N = NUMEL(A, VARARGIN) returns the number of subscripted
//   elements, N, in A(index1, index2, ..., indexN), where VARARGIN
//   is a cell array whose elements are index1, index2, ... indexN.

#include "numel.h"

//19 Mei 2010
int numel2_double(double **A, int row, int col)
{
	return (row*col);
}

int numel1_double(double *A, int lenA)
{
	return lenA;
}
