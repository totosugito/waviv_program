/*
 * find.h
 *
 *  Created on: May 24, 2010
 *      Author: toto
 */

#ifndef FIND_H_
#define FIND_H_

#include "../array/su_ealloc.h"

int *find_int(int *data, int ndata, int cekdata, int type, int *nout);
int *find_double(double *data, int ndata, double cekdata, int type, int *nout);
int  *find2_double(
		double *data1, double cekdata1, short type1,
		double *data2, double cekdata2, short type2,
		int ndata, int *nout);
#endif /* FIND_H_ */
