/*
 * reshape.c
 *
 *  Created on: May 19, 2010
 *      Author: toto
 */
//RESHAPE Change size.
//   RESHAPE(X,M,N) returns the M-by-N matrix whose elements
//   are taken columnwise from X.  An error results if X does
//   not have M*N elements.
//
//   RESHAPE(X,M,N,P,...) returns an N-D array with the same
//   elements as X but reshaped to have the size M-by-N-by-P-by-...
//   M*N*P*... must be the same as PROD(SIZE(X)).
//
//   RESHAPE(X,[M N P ...]) is the same thing.
//
//   RESHAPE(X,...,[],...) calculates the length of the dimension
//   represented by [], such that the product of the dimensions
//   equals PROD(SIZE(X)). PROD(SIZE(X)) must be evenly divisible
//   by the product of the known dimensions. You can use only one
//   occurrence of [].
//
//   In general, RESHAPE(X,SIZ) returns an N-D array with the same
//   elements as X but reshaped to the size SIZ.  PROD(SIZ) must be
//   the same as PROD(SIZE(X)).

#include "reshape.h"

//19 Mei 2010
double **reshape2_double(double **A, int row, int col, int setrow, int setcol)
{
	int i, j;
	int m, n;
	double **B=NULL;

	if(row*col != setrow*setcol)
	{
		fprintf(stderr, "??? Error using ==> reshape\n");
		fprintf(stderr, "To RESHAPE the number of elements must not change.\n");
		exit(0);
	}

	B = su_ealloc2double(setcol, setrow);
	m = 0;
	n = 0;
	for(j=0; j<col; j++)
	{
		for(i=0; i<row; i++)
		{
			B[m][n] = A[i][j];
			m += 1;
			if(m==setrow)
			{
				m = 0;
				n += 1;
			}

		}
	}
	return B;
}

