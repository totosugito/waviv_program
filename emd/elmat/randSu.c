/*
 * randSu.c
 *
 *  Created on: Jan 19, 2011
 *      Author: toto
 */
#include "randSu.h"


double rand1(int itype, int iseed, unsigned int vseed)
{
	unsigned int seed;
	double noise;

	if(iseed==0)
	{
		if (-1 == (seed = (unsigned int) time((time_t *) NULL)))
		{
			fprintf(stderr, "time() failed to set seed");
			exit(0);
		}
	}
	else
		seed = vseed;

	(itype == 1) ? srannor(seed) : sranuni(seed);

	/* Compute noise vector elements in [-1, 1] */
	if(itype==1)/* GAUSS METHOD. frannor gives elements in N(0,1)--ie. pos & negs */
		noise = (double) frannor();
	else /* FLAT METHOD. franuni gives elements in [0, 1] */
		noise = 2.0*franuni() - 1.0;

	return (noise);
}

double *rand2(int ndata, int itype, int iseed, unsigned int vseed)
{
	int i;
	unsigned int seed;
	double *noise;

	if(iseed==0)
	{
		if (-1 == (seed = (unsigned int) time((time_t *) NULL)))
		{
			fprintf(stderr, "time() failed to set seed");
			exit(0);
		}
	}
	else
		seed = vseed;

	(itype == 1) ? srannor(seed) : sranuni(seed);

	noise = su_ealloc1double(ndata);
	for(i=0; i<ndata; i++)
	{
		/* Compute noise vector elements in [-1, 1] */
		if(itype==1)/* GAUSS METHOD. frannor gives elements in N(0,1)--ie. pos & negs */
			noise[i] = (double) frannor();
		else /* FLAT METHOD. franuni gives elements in [0, 1] */
			noise[i] = 2.0*franuni() - 1.0;
	}

	return (noise);
}
