/*
 * find.c
 *
 *  Created on: May 24, 2010
 *      Author: toto
 */
//FIND   Find indices of nonzero elements.
//   I = FIND(X) returns the linear indices corresponding to
//   the nonzero entries of the array X.  X may be a logical expression.
//   Use IND2SUB(SIZE(X),I) to calculate multiple subscripts from
//   the linear indices I.

#include "find.h"

//type
// 0 --> ==
// 1 --> <
// 2 --> >
// 3 --> <=
// 4 --> >=
int  *find_int(int *data, int ndata, int cekdata, int type, int *nout)
{
	int i;
	int k;
	int *tmpout=NULL;
	int *out=NULL;

	if(ndata==0)
		return NULL;

	k = 0;
	tmpout = su_ealloc1int(ndata);

	for (i=0; i<ndata; i++)
	{
		switch (type)
		{
		case 0:
			if(data[i]==cekdata)
			{
				tmpout[k] = i;
				k += 1;
			}
			break;
		case 1:
			if(data[i]<cekdata)
			{
				tmpout[k] = i;
				k += 1;
			}
			break;
		case 2:
			if(data[i]>cekdata)
			{
				tmpout[k] = i;
				k += 1;
			}
			break;
		case 3:
			if(data[i]<=cekdata)
			{
				tmpout[k] = i;
				k += 1;
			}
			break;
		case 4:
			if(data[i]>=cekdata)
			{
				tmpout[k] = i;
				k += 1;
			}
			break;
		}
	}

	if(k==0)
	{
		(*nout)=0;
	}
	else
	{
		out = su_ealloc1int(k);
		memcpy(out,tmpout, k*sizeof(int));
		(*nout) = k;
	};

	su_free1int(tmpout);
	return(out);
}

int *find_double(double *data, int ndata, double cekdata, int type, int *nout)
{
	int i;
	int k;
	int *tmpout=NULL;
	int *out=NULL;

	if(ndata==0)
		return NULL;

	k = 0;
	tmpout = su_ealloc1int(ndata);

	for (i=0; i<ndata; i++)
	{
		switch (type)
		{
		case 0:
			if(data[i]==cekdata)
			{
				tmpout[k] = i;
				k += 1;
			}
			break;
		case 1:
			if(data[i]<cekdata)
			{
				tmpout[k] = i;
				k += 1;
			}
			break;
		case 2:
			if(data[i]>cekdata)
			{
				tmpout[k] = i;
				k += 1;
			}
			break;
		case 3:
			if(data[i]<=cekdata)
			{
				tmpout[k] = i;
				k += 1;
			}
			break;
		case 4:
			if(data[i]>=cekdata)
			{
				tmpout[k] = i;
				k += 1;
			}
			break;
		}
	}

	if(k==0)
	{
		(*nout)=0;
	}
	else
	{
		out = su_ealloc1int(k);
		memcpy(out,tmpout, k*sizeof(int));
		(*nout) = k;
	};

	su_free1int(tmpout);
	return(out);
}

//find data with two variable
int  *find2_double(
		double *data1, double cekdata1, short type1,
		double *data2, double cekdata2, short type2,
		int ndata, int *nout)
{
	int i;
	int k;
	int *tmpout=NULL;
	int *out=NULL;

	k = 0;
	tmpout = su_ealloc1int(ndata);

	for (i=0; i<ndata; i++)
	{
		switch (type1)
		{
		case 0:
			/*not yet implementated*/
			fprintf(stderr, "not yet implementated\n");
			exit(0);
			break;
		case 1:
			switch(type2)
			{
			case 0:
				/*not yet implementated*/
				break;
			case 1:
				if((data1[i]<cekdata1) && (data2[i]<cekdata2))
				{
					tmpout[k] = i;
					k += 1;
				}
				break;
			case 2:
				if((data1[i]<cekdata1) && (data2[i]>cekdata2))
				{
					tmpout[k] = i;
					k += 1;
				}
				break;
			}
			break;
		case 2:
			/*not yet implementated*/
			fprintf(stderr, "not yet implementated\n");
			exit(0);
			break;
		case 3:
			/*not yet implementated*/
			fprintf(stderr, "not yet implementated\n");
			exit(0);
			break;
		case 4:
			/*not yet implementated*/
			fprintf(stderr, "not yet implementated\n");
			exit(0);
			break;
		}
	}
	if(k==0)
	{
		(*nout) = 0;
	}
	else
	{
		out = su_ealloc1int(k);
		(*nout) = k;
		memcpy(out,tmpout, k*sizeof(int));
	}
	su_free1int(tmpout);

	return(out);
}
