/*
 * reshape.h
 *
 *  Created on: May 19, 2010
 *      Author: toto
 */

#ifndef RESHAPE_H_
#define RESHAPE_H_
#include "../array/su_ealloc.h"


double **reshape2_double(double **A, int row, int col, int setrow, int setcol);
#endif /* RESHAPE_H_ */
