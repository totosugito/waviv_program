/*
 * fliplr.c
 *
 *  Created on: May 24, 2010
 *      Author: toto
 */

#include "fliplr.h"

/*flip data array*/
int *fliplr_int(int *A, int nx)
{
	int i;
	int *out=NULL;

	out = su_ealloc1int(nx);

	for(i=0; i<nx; i++)
	{
		out[i] = A[nx-1-i];
	}
	return out;
}

/*flip data array*/
void fliplrv_int(int *A, int nx)
{
	int i;
	int *out=NULL;

	out = su_ealloc1int(nx);

	for(i=0; i<nx; i++)
	{
		out[i] = A[nx-1-i];
	}

	memcpy(A, out, nx*sizeof(int));
	su_free1int(out);
}

/*flip data array*/
void fliplrv1_int(int *A, int pos0, int nx)
{
	int i;
	int n1;
	int *out=NULL;

	n1 = nx-pos0;
	out = su_ealloc1int(n1);

	for(i=0; i<n1; i++)
	{
		out[i] = A[nx-1-i];
	}

	memcpy(A+pos0, out, n1*sizeof(int));
	su_free1int(out);
}

double *fliplr_double(double *A, int nx)
{
	int i;
	double *out=NULL;

	out = su_ealloc1double(nx);

	for(i=0; i<nx; i++)
	{
		out[i] = A[nx-1-i];
	}
	return out;
}

