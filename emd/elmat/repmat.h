/*
 * repmat.h
 *
 *  Created on: May 19, 2010
 *      Author: toto
 */

#ifndef REPMAT_H_
#define REPMAT_H_
#include "../array/array.h"

double **repmat2_double(double **A, int row, int col, int setrow, int setcol, int *nrow, int *ncol);
double **repmat1_double(double *A, int lenA, int setrow, int setcol, int *nrow, int *ncol, short type);
#endif /* REPMAT_H_ */
