/*
 * repmat.c
 *
 *  Created on: May 19, 2010
 *      Author: toto
 */
//REPMAT Replicate and tile an array.
//   B = repmat(A,M,N) creates a large matrix B consisting of an M-by-N
//   tiling of copies of A. The size of B is [size(A,1)*M, size(A,2)*N].
//   The statement repmat(A,N) creates an N-by-N tiling.
//
//   B = REPMAT(A,[M N]) accomplishes the same result as repmat(A,M,N).
//
//   B = REPMAT(A,[M N P ...]) tiles the array A to produce a
//   multidimensional array B composed of copies of A. The size of B is
//   [size(A,1)*M, size(A,2)*N, size(A,3)*P, ...].
//
//   REPMAT(A,M,N) when A is a scalar is commonly used to produce an M-by-N
//   matrix filled with A's value and having A's CLASS. For certain values,
//   you may achieve the same results using other functions. Namely,

#include "repmat.h"

//repmat array 2D double
//20 Mei 2010
double **repmat2_double(double **A, int row, int col, int setrow, int setcol, int *nrow, int *ncol)
{
	int i,j, k;  //index
	int nrow1, ncol1; //set new row and column size
	double **B=NULL;  //array output

	nrow1 = row*setrow;
	ncol1 = col*setcol;
	//set array output size
	(*nrow) = nrow1;
	(*ncol) = ncol1;

	if((nrow1==0) || (ncol1==0))
	{
		fprintf(stderr, "repmat2_double : output is empty array\n");
		exit (0);
	}
	B = su_ealloc2double(ncol1, nrow1);

	//set arah column
	for(i=0; i<row; i++)
	{
		k = 0;
		for(j=0; j<setcol; j++)
		{
			k = col*j;
			memcpy(B[i]+k, A[i], col*sizeof(double));
		}
	}

	//set arrah row
	for(j=1; j<setrow; j++)
	{
		for(i=0; i<row; i++)
		{
			k = j*row + i;
			memcpy((void*)B[k], B[i], ncol1*sizeof(double));
		}
	}

	return B;
}

//20-05-2010
//type=1 -> A = matrix row (with 1 row)
//type=2 -> A = matrix column (with 1 column)
double **repmat1_double(double *A, int lenA, int setrow, int setcol, int *nrow, int *ncol, short type)
{
	int i;  //index
	int row, col;
	int nrow1, ncol1; //set new row and column size
	double **B=NULL;  //array output
	double **tmp=NULL;


	if(type==1)
	{
		row = 1;
		col = lenA;
		tmp = su_ealloc2double(col, row);
		memcpy(tmp[0], A, col*sizeof(double));
	}
	else
	{
		row = lenA;
		col = 1;
		tmp = su_ealloc2double(col, row);
		for(i=0; i<row; i++)
			tmp[i][0] = A[i];
	}

	if((nrow1==0) || (ncol1==0))
	{
		fprintf(stderr, "repmat1_double : output is empty array\n");
		exit (0);
	}

	B = repmat2_double(tmp, row, col, setrow, setcol, &nrow1, &ncol1);
	(*nrow) = nrow1;
	(*ncol) = ncol1;

	return (B);
}
