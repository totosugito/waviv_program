/*
 * su_errpkg.h
 *
 *  Created on: May 19, 2010
 *      Author: toto
 */

#ifndef SU_ERRPKG_H_
#define SU_ERRPKG_H_

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

/* GLOBAL DECLARATIONS */
extern int xargc; extern char **xargv;

void err(char *fmt, ...);
void warn(char *fmt, ...);
void syserr(char *fmt, ...);

#endif /* SU_ERRPKG_H_ */
