#include <stdio.h>
#include <stdlib.h>
//#include <cpromax.h>
//#include <cglobal.h>
#include <cwp.h>
#include <su.h>

float* gaussiankernel(int span,float sigma);
double* gaussiankerneld(int ksize,float sigma);
float* gaussianderivative1kernel(int ksize,float sigma);
void convolve(float* function, int flength, float* kernel, int klength);
void convolved(double* function, int flength, double* kernel, int klength);
float* meankernel(int span);
double* meankerneld(int ksize);
void convolven1(float** input, int n1, int n2, float* kernel, int klength);
void getcol(float** input, int colselect, float* coldata, int nrow);
void setcol(float** input, int colselect, float* coldata, int nrow);
void convolven2(float** input, int n1, int n2, float* kernel, int klength);
void constructcedtensor(float** S11, float** S22, float** S12,float** D11, float** D22, float** D12,float k, int row, int col);
void WeickertCED(float** data,float** a,float** c, float** b,float dt, int row, int col);
//sugather* providefldr(sugather* gatherin, int fldrselect);
float** transpose(float** input, int row,int col);
void medianfilter(float** data, int radius,int row, int col);
void bclip(float* data, int n, float trim);
void bclip2(float* data,float*dataclip, int n, float trim);
float* gaussianderivative2kernel(int ksize,float sigma);
float herpr(float** X, float** Y, int row, int col);
void dot_hale(float** D11,float** D22,float** D12, float** in, float** out, float alpha, int row, int col);
void HaleCED(float** input,float** D11,float** D22, float** D12,float alf,int cgiter, int row, int col);
float* undersampled(float* data, int n);
void convolve_tv(float* function, int flength, float sigma, float dsigma, int klength);
void getexterma(float* derivative,float* data, int n);
float* morletkernel(int ksize,float sigma);
float* extractexterma(float* data, int ns, int klength, float sigmaderiv);
float* extractmaxima(float* data, int ns, int klength, float sigmaderiv);
void getmaxima(float* derivative,float* secondderivative,float* data, int n);
void getminima(float* derivative,float* secondderivative,float* data, int n);
float* extractminima(float* data, int ns, int klength, float sigmaderiv);
float gaussian(float beta, float x1, float x2);
void nonzeroextract(float* data, int n,float* y, float* x);
void dot_vec(float** A, float* X,float* B, int row, int col);
float vec_dot_vec(float* a,float* b,int row);
void cg(float** A, float* X, float* B, int row, int col,int cgiter);
float tpl(float order, float x1, float x2);
float rorder(float order, float x1, float x2);
float mq(float beta, float x1, float x2);
float imq(float beta, float x1, float x2);
float Min(float* data, int n);
float Max(float* data, int n);
void osfmax(float* output,float* data, int n, int w);
void osfmaxd(double* output,double* data, int n, int w);
void osfmaxdn(double* output,double* function, int flength, int klength);
void osfmin(float* output,float* data, int n, int w);
void osfmind(double* output,double* data, int n, int w);
void osfmindn(double* output,double* function, int flength, int klength);
void /*float* */emdextract(double* tracedata1, int ns, int div, float divg, int klength, float sigmaderiv);
