/*
 * getminmax.c
 *
 *  Created on: May 24, 2010
 *      Author: toto
 */

#include "getminmax.h"

/*get minimum n maximum data*/
void getminmax_int(int *data, int ndata, int *mind, int *maxd)
{
	int i;
	int b1;
	int max, min;

	min = data[0];
	max = data[0];
	for (i=1; i<ndata; i++)
	{
		b1 = data[i];

		if(b1<min)
			min = b1;

		if(b1>max)
			max = b1;
	}
	(*maxd) = max;
	(*mind) = min;
}

void getminmax_double(double *data, int ndata, double *mind, double *maxd)
{
	int i;
	double b1;
	double max, min;
	min = data[0];
	max = data[0];
	for (i=1; i<ndata; i++)
	{
		b1 = data[i];

		if(b1<min)
			min = b1;

		if(b1>max)
			max = b1;
	}
	(*maxd) = max;
	(*mind) = min;
}

