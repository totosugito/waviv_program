/*
 * std.c
 *
 *  Created on: Jan 18, 2011
 *      Author: toto
 *
 *      http://en.wikipedia.org/wiki/Standard_deviation
 *      COMPUTE STANDARD DEVIATION
 */

#include "std.h"

double std_i(int *data, int ndata)
{
	int i;
	double meand;  //mean data
	double tmpdata; //temporary data
	double stdval;
	double tmptot, total;

	meand = mean_i(data, ndata);

	stdval = 0.0;
	total = 0.0;
	for(i=0; i<ndata; i++)
	{
		tmpdata = (double) data[i];
		tmptot = pow(tmpdata-meand, 2.0);
		total += tmptot;
	}
	stdval = sqrt(total/ndata);

	return stdval;
}

double std_f(float *data, int ndata)
{
	int i;
	double meand;  //mean data
	double tmpdata; //temporary data
	double stdval;
	double tmptot, total;

	meand = mean_f(data, ndata);

	stdval = 0.0;
	total = 0.0;
	for(i=0; i<ndata; i++)
	{
		tmpdata = (double) data[i];
		tmptot = pow(tmpdata-meand, 2.0);
		total += tmptot;
	}
	stdval = sqrt(total/ndata);

	return stdval;
}

double std_d(double *data, int ndata)
{
	int i;
	double meand;  //mean data
	double tmpdata; //temporary data
	double stdval;
	double tmptot, total;

	meand = mean_d(data, ndata);

	stdval = 0.0;
	total = 0.0;
	for(i=0; i<ndata; i++)
	{
		tmpdata = data[i];
		tmptot = pow(tmpdata-meand, 2.0);
		total += tmptot;
	}
	stdval = sqrt(total/ndata);

	return stdval;
}
