/*
 * datafun.h
 *
 *  Created on: May 20, 2010
 *      Author: toto
 */

#ifndef DATAFUN_H_
#define DATAFUN_H_

#include "diff.h"
#include "prod.h"
#include "mean.h"
#include "detrend.h"
#include "getminmax.h"
#include "std.h"
#endif /* DATAFUN_H_ */
