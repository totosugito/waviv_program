/*
 * getminmax.h
 *
 *  Created on: May 24, 2010
 *      Author: toto
 */
//get maximum and minimum from array

#ifndef GETMINMAX_H_
#define GETMINMAX_H_

#include <stdio.h>

void getminmax_int(int *A, int nx, int *minA, int *maxA);
void getminmax_double(double *A, int nx, double *minA, double *maxA);
#endif /* GETMINMAX_H_ */
