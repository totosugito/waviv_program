/*
 * mean_Arr.h
 *
 *  Created on: May 24, 2010
 *      Author: toto
 */

#ifndef MEAN_ARR_H_
#define MEAN_ARR_H_

#include "../array/array.h"

double mean_i(int *input, int ndata);
double mean_f(float *input, int ndata);
double mean_d(double *input, int ndata);

#endif /* MEAN_ARR_H_ */
