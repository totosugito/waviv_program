/*
 * diff.h
 *
 *  Created on: May 20, 2010
 *      Author: toto
 */

#ifndef DIFF_H_
#define DIFF_H_

#include "../array/array.h"

double **diff2_double(double **A, int row, int col, int N, int DIM, int *nrow, int *ncol);
double *diff1_double(double *A, int len, int N, int *nlen);
int *diff1_int(int *A, int len, int N, int *nlen);
int *diff1abs_int(int *A, int len, int N, int *nlen);
#endif /* DIFF_H_ */
