/*
 * detrend.c
 *
 *  Created on: May 24, 2010
 *      Author: toto
 */
//DETREND Remove a linear trend from a vector, usually for FFT processing.
//   Y = DETREND(X) removes the best straight-line fit linear trend from the
//   data in vector X and returns the residual in vector Y.  If X is a
//   matrix, DETREND removes the trend from each column of the matrix.
//
//   Y = DETREND(X,'constant') removes just the mean value from the vector X,
//   or the mean value from each column, if X is a matrix.
//
//   Y = DETREND(X,'linear',BP) removes a continuous, piecewise linear trend.
//   Breakpoint indices for the linear trend are contained in the vector BP.
//   The default is no breakpoints, such that one single straight line is
//   removed from each column of X.

#include "detrend.h"

//type 0=constant
//type 1=linear
double *detrend_d(double *input, bool type, int ndata)
{
	double *output;
	double meand;
	int i;

	output = su_ealloc1double(ndata);

	meand = mean_d(input,ndata);
	if (type==0)
	{
		for(i=0; i<ndata; i++)
		{
			output[i]= input[i] - meand;
		}
	}
	else
	{
		/*not impelemented*/
	}
	return output;
}

//type 0=constant
//type 1=linear
void detrendv_d(double *input, bool type, int ndata)
{
	double meand;
	int i;

	meand = mean_d(input,ndata);
	if (type==0)
	{
		for(i=0; i<ndata; i++)
			input[i] -= meand;
	}
	else
	{
		/*not impelemented*/
		printf("not impelemented \n");
		exit(0);
	}
}
