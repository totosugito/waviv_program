/*
 * std.h
 *
 *  Created on: Jan 18, 2011
 *      Author: toto
 *
 *      COMPUTE STANDARD DEVIATION
 */

#ifndef STD_H_
#define STD_H_
#include "mean.h"
#include <math.h>

double std_i(int *data, int ndata);
double std_f(float *data, int ndata);
double std_d(double *data, int ndata);
#endif /* STD_H_ */
