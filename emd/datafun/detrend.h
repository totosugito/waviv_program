/*
 * detrend.h
 *
 *  Created on: May 24, 2010
 *      Author: toto
 */

#ifndef DETREND_H_
#define DETREND_H_

#include "../array/array.h"
#include "mean.h"

double *detrend_d(double *input, bool type, int ndata);
void detrendv_d(double *input, bool type, int ndata);
#endif /* DETREND_H_ */
