/*
 * prod.c
 *
 *  Created on: May 20, 2010
 *      Author: toto
 */
//PROD Product of elements.
//   For vectors, PROD(X) is the product of the elements of X. For
//   matrices, PROD(X) is a row vector with the product over each
//   column. For N-D arrays, PROD(X) operates on the first
//   non-singleton dimension.
//
//   PROD(X,DIM) works along the dimension DIM.
//
//   Example: If X = [0 1 2
//                    3 4 5]
//
//   then prod(X,1) is [0 4 10] and prod(X,2) is [ 0
//                                                60]

#include "prod.h"

//20-05-2010
double prod1_double(double *A, int lenA)
{
	int i;
	double prodA;

	prodA = 1.0;
	for (i=0; i<lenA; i++)
		prodA *= A[i];

	return (prodA);
}

//20-05-2010
int prod1_int(int *A, int lenA)
{
	int i;
	int prodA;

	prodA = 1.0;
	for (i=0; i<lenA; i++)
		prodA *= A[i];

	return (prodA);
}
