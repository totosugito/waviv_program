/*
 * prod.h
 *
 *  Created on: May 20, 2010
 *      Author: toto
 */

#ifndef PROD_H_
#define PROD_H_
#include <stdio.h>

double prod1_double(double *A, int lenA);
int prod1_int(int *A, int lenA);
#endif /* PROD_H_ */
