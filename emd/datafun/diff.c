/*
 * diff.c
 *
 *  Created on: May 20, 2010
 *      Author: toto
 */
//DIFF Difference and approximate derivative.
//   DIFF(X), for a vector X, is [X(2)-X(1)  X(3)-X(2) ... X(n)-X(n-1)].
//   DIFF(X), for a matrix X, is the matrix of row differences,
//      [X(2:n,:) - X(1:n-1,:)].
//   DIFF(X), for an N-D array X, is the difference along the first
//      non-singleton dimension of X.
//   DIFF(X,N) is the N-th order difference along the first non-singleton
//      dimension (denote it by DIM). If N >= size(X,DIM), DIFF takes
//      successive differences along the next non-singleton dimension.
//   DIFF(X,N,DIM) is the Nth difference function along dimension DIM.
//      If N >= size(X,DIM), DIFF returns an empty array.
//
//   Examples:
//      h = .001; x = 0:h:pi;
//      diff(sin(x.^2))/h is an approximation to 2*cos(x.^2).*x
//      diff((1:10).^2) is 3:2:19
//
//      If X = [3 7 5
//              0 9 2]
//      then diff(X,1,1) is [-3 2 -3], diff(X,1,2) is [4 -2
//                                                     9 -7],
//      diff(X,2,2) is the 2nd order difference along the dimension 2, and
//      diff(X,3,2) is the empty matrix.

#include "diff.h"

//20 Mei 2010
double **diff2_double(double **A, int row, int col, int N, int DIM, int *nrow, int *ncol)
{
	int i,j;
	int nlen; //new size of diff
	double *tmp1=NULL, *tmp2=NULL;
	double **B=NULL;

	if(DIM==1)
	{
		B = su_ealloc2double(col, row-N);
		for(i=0; i<col; i++)
		{
			tmp1 = array2_getvect_double(A, row, col, 1, row, i+1, i+1);
			tmp2 = diff1_double(tmp1, row, N, &nlen);
			for(j=0; j<nlen; j++)
				B[j][i] = tmp2[j];
			if(tmp1) su_free1double(tmp1);
			if(tmp2) su_free1double(tmp2);
		}
		(*ncol) = col;
		(*nrow) = nlen;
	}
	else if(DIM==2)
	{
		B = su_ealloc2double(col-N, row);
		for(i=0; i<row; i++)
		{
			tmp1 = array2_getvect_double(A, row, col, i+1, i+1, 1, col);
			tmp2 = diff1_double(tmp1, col, N, &nlen);
			for(j=0; j<nlen; j++)
				B[i][j] = tmp2[j];
			if(tmp1) su_free1double(tmp1);
			if(tmp2) su_free1double(tmp2);
		}
		(*ncol) = nlen;
		(*nrow) = row;
	}
	else
	{
		fprintf(stderr, "diff2_double : array is 2D, input DIM must be 1 or 2\n");
		exit(0);
	}
	return (B);
}

//20 Mei 2010
double *diff1_double(double *A, int len, int N, int *nlen)
{
	int i, j;
	double *tmp1, *tmp2;

	tmp1 = su_ealloc1double(len);  //set temporary I
	tmp2 = su_ealloc1double(len-1); //set temporary II
	memcpy(tmp1, A, len*sizeof(double));  //copy input to temporary I
	for (i=0; i<N; i++)  //loop at N differentiate
	{
		for(j=0; j<len-1; j++)  //loop every variable
		{
			tmp2[j] = tmp1[j+1] - tmp1[j];
		}
		memset(tmp1, '\0', len*sizeof(double)); //set temporary I to NULL
		len = len - 1;
		memcpy(tmp1, tmp2, len*sizeof(double));  //copy value from differentiate to temporary I
	}

	su_free1double(tmp2);
	(*nlen) = len;
	return (tmp1);
}

//20 Mei 2010
int *diff1_int(int *A, int len, int N, int *nlen)
{
	int i, j;
	int *tmp1, *tmp2;

	tmp1 = su_ealloc1int(len);  //set temporary I
	tmp2 = su_ealloc1int(len-1); //set temporary II
	memcpy(tmp1, A, len*sizeof(int));  //copy input to temporary I
	for (i=0; i<N; i++)  //loop at N differentiate
	{
		for(j=0; j<len-1; j++)  //loop every variable
		{
			tmp2[j] = tmp1[j+1] - tmp1[j];
		}
		memset(tmp1, '\0', len*sizeof(int)); //set temporary I to NULL
		len = len - 1;
		memcpy(tmp1, tmp2, len*sizeof(int));  //copy value from differentiate to temporary I
	}

	su_free1int(tmp2);
	(*nlen) = len;
	return (tmp1);
}

int *diff1abs_int(int *A, int len, int N, int *nlen)
{
	int i, j;
	int *tmp1, *tmp2;

	tmp1 = su_ealloc1int(len);  //set temporary I
	tmp2 = su_ealloc1int(len-1); //set temporary II
	memcpy(tmp1, A, len*sizeof(int));  //copy input to temporary I
	for (i=0; i<N; i++)  //loop at N differentiate
	{
		for(j=0; j<len-1; j++)  //loop every variable
		{
			tmp2[j] = tmp1[j+1] - tmp1[j];
			if(tmp2[j]<0)
				tmp2[j] = tmp2[j] * (-1);
		}
		memset(tmp1, '\0', len*sizeof(int)); //set temporary I to NULL
		len = len - 1;
		memcpy(tmp1, tmp2, len*sizeof(int));  //copy value from differentiate to temporary I
	}

	su_free1int(tmp2);
	(*nlen) = len;
	return (tmp1);
}
