/*
 * mean_Arr.c
 *
 *  Created on: May 24, 2010
 *      Author: toto
 */
//MEAN   Average or mean value.
//   For vectors, MEAN(X) is the mean value of the elements in X. For
//   matrices, MEAN(X) is a row vector containing the mean value of
//   each column.  For N-D arrays, MEAN(X) is the mean value of the
//   elements along the first non-singleton dimension of X.

#include "mean.h"

double mean_i(int *input, int ndata)
{
	int jdata;
	double out;
	int i;

	jdata = 0;
	out = 0.0;

	for (i=0; i<ndata; i++)
	{
		jdata += input[i];
	}
	out = (double)(jdata/ndata);
	return out;
}

double mean_f(float *input, int ndata)
{
	float jdata;
	double out;
	int i;

	jdata = 0.0;
	out = 0.0;

	for (i=0; i<ndata; i++)
	{
		jdata += input[i];
	}
	out = (double)(jdata/ndata);
	return out;
}

double mean_d(double *input, int ndata)
{
	double jdata, out;
	int i;

	jdata = 0.0;
	out = 0.0;

	for (i=0; i<ndata; i++)
	{
		jdata += input[i];
	}
	out = jdata/ndata;
	return out;
}
