/*
 * wave_writeneighborfile.c
 *
 *  Created on: Feb 7, 2011
 *      Author: toto
 */

#include "segy_lib.h"
#include <sys/types.h>
#include <stdbool.h>
#include "wave_3dlib.h"

char *sdoc[] = {
		"									",
		" WAVE_WRITENEIGHBORFILE v.9 = GET NEIGHBORHOD DATA FROM 3D SEGY FILE AND SAVE TO BINARY FILE",
		" Output Format (OUT) : INLINE    SUM_XLINE     MIN_XLINE    MAX_XLINE ",
		"",
		"",
		" inp=         (input segy file)",
		" out=         (output text file)",
		" outpos=      (output inline xline position)",
		" ftype=1      (0=segy file, 1=su file)",
		" maxtr=0      (maximum number of trace. 0=read all traces)",
		" verbose=1    (show info)",
		" vblock=10000  (show verbose every vblock traces)",
		" endian=0     (0=little endian)",
		" iline=9      (inline location in byte, 9=same as ffid)",
		" xline=17     (xline location in byte, 17=as sp/ep)",
		" isx=33       (X source location in byte)",
		" isy=37       (Y source location in byte)",
		" xyformat=1   (1=integer, 2=IBM Float)",
		" iscalco=71   (scalco number. fill -1 if dont use scalco)",
		"",
		" Sample Command :",
		" wave_writeneighborfile inp=mysegy.segy out=myout.txt outpos=posdata.bin maxtr=1000 ftype=0",
		NULL};

void processDataSegy(char *CFIN, char *CFOD, char *CFPOS, int maxtr, int verbose, int endian, int vblock,
		int iline, int xline, int isx, int isy, int iscalco, int xyformat);

void getSegy3DInfo(FILE *FFIN, int maxtr, int endian, int vblock, int verbose,
		int iline, int xline,
		int *intrc, int *insegy, int *insp, int *iformat,
		int *imin_inline, int *imax_inline,
		int *imin_xline, int *imax_xline,
		int *iwidth_inline, int *iwidth_xline);


int main (int argc, char **argv)
{
	int verbose;
	char *ccinp, *ccout, *coutpos;
	int maxtr, iline, xline;
	int endian, vblock, ftype;
	int isx, isy, iscalco;
	int xyformat;

	/* hook up getpar to handle the parameters */
	initargs(argc,argv);
	requestdoc(1);

	MUSTGETPARSTRING("inp",  &ccinp);
	MUSTGETPARSTRING("out",  &ccout);
	MUSTGETPARSTRING("outpos",  &coutpos);
	if (!getparint("maxtr",&maxtr)) 	maxtr = 0;  //maximum number of traces
	if (!getparint("verbose",&verbose)) 	verbose = 1;
	if (!getparint("endian",&endian)) 	endian = 0;
	if (!getparint("ftype",&ftype)) 	ftype = 0;
	if (!getparint("vblock",&vblock)) 	vblock = 10000;
	if (!getparint("iline",&iline)) 	iline = 9;
	if (!getparint("xline",&xline)) 	xline = 17;
	if (!getparint("isx",&isx)) 	isx = 33;
	if (!getparint("isy",&isy)) 	isy = 37;
	if (!getparint("xyformat",&xyformat))     xyformat = 1;
	if (!getparint("iscalco",&iscalco)) 	iscalco = 71;

	if(ftype!=0)
		err("input su file not yet implementated");

	processDataSegy(ccinp, ccout, coutpos, maxtr, verbose, endian, vblock, iline, xline, isx, isy, iscalco, xyformat);

	return (1);
}

void processDataSegy(char *CFIN, char *CFOD, char *CFPOS, int maxtr, int verbose, int endian, int vblock,
		int iline, int xline, int isx, int isy, int iscalco, int xyformat)
{
	FILE *FFIN=NULL, *FFOD=NULL;
	int inval, xval, curr_inval, curr_xval;
	float curr_sx, curr_sy;
	int scalco;

	int min_inval, max_inval;
	int min_xval, max_xval, max_xvalt, min_xvalt;
	int width_inline, width_xline;

	int idx, nxline, ninline, nbyte;
	int nsegy, ntrc, nsp, format, iblock;
	char *buffer;
	pos3ddata **posInXline;

	nbyte = 200;
	buffer = (char*) calloc(nbyte, sizeof(char));

	FFIN = fopen (CFIN,"r");
	if (FFIN==NULL) err ("Error opening file 1 : %s\n", CFIN);

	FFOD = fopen (CFOD,"w");
	if (FFOD==NULL) err ("Error opening file 2 : %s\n", CFOD);

	fprintf(stderr, "\n Get Inline-Xline Information  \n");
	getSegy3DInfo(FFIN, maxtr, endian, vblock, verbose,
			iline, xline,
			&ntrc, &nsegy, &nsp, &format,
			&min_inval, &max_inval,
			&min_xvalt, &max_xvalt,
			&width_inline, &width_xline);

	if (verbose)
	{
		fprintf(stderr, "\nnsp=%i \n", nsp);
		fprintf(stderr, "format=%i \n", format);
		fprintf(stderr, "nsegy=%i \n", nsegy);
		fprintf(stderr, "inline location = %i \n", iline);
		fprintf(stderr, "xline location =%i \n", xline);
		fprintf(stderr, "min inline = %i \n", min_inval);
		fprintf(stderr, "max inline = %i \n", max_inval);
		fprintf(stderr, "min xline = %i \n", min_xvalt);
		fprintf(stderr, "max xline = %i \n", max_xvalt);
		fprintf(stderr, "inline width = %i \n", width_inline);
		fprintf(stderr, "xline  width = %i \n\n", width_xline);
	}
	//	fprintf(stderr,"close file\n");
	/* saving inline and xline position in array 2D */
	posInXline = pos3ddata_create(width_inline, width_xline);
	pos3ddata_setindex(posInXline, width_inline, width_xline);	//set initial value to zero

	/*----------- READ FIRST TRACE ---------------------------*/
	curr_inval = computeIntHeaderAtByte(FFIN, iline, 4, endian, 0); //get current inline header
	curr_xval = computeIntHeaderAtByte(FFIN, xline, 4, endian, 0);  //get current xline header
	if(iscalco>0)
		scalco = computeIntHeaderAtByte(FFIN, iscalco, 2, endian, 1);
	else
		scalco = 1;

	if(xyformat==1)
	{
		curr_sx = (float) (computeIntHeaderAtByte(FFIN, isx, 4, endian, 0));
                curr_sy = (float) (computeIntHeaderAtByte(FFIN, isy, 4, endian, 0));
	//	fprintf(stderr, "%f %f \n",curr_sx,curr_sy);
	}
	else if(xyformat==2)
	{
		curr_sx = computeIBMFloatHeaderAtByte(FFIN, isx, 4, endian);
		curr_sy = computeIBMFloatHeaderAtByte(FFIN, isy, 4, endian);
	}
	else
	{
		curr_sx = 0.0;
		curr_sy = 0.0;
		err("xyformat must b 1(integer) or 2 (IBM float)");
	}

	nextTrace(FFIN, nsegy); 	//next trace

	/*save position coordinate reference, set position to zero*/
	setValuePositionInXline(posInXline, min_inval, min_xvalt, curr_inval, curr_xval, scalco,
			curr_sx, curr_sy, 0);


	inval =curr_inval;
	xval = curr_xval;

	/*minimum and maximum xline*/
	min_xval = curr_xval;
	max_xval = curr_xval;

	nxline=1;
	ninline=0;
	iblock = 1;

	for(idx=1; idx<ntrc; idx++)
	{
		/*show verbose*/
		if(verbose)
		{
			if(iblock*vblock==idx)
			{
				fprintf(stderr, "Read Header traces %i / %i \n", idx, ntrc);
				iblock++;
			}
		}

		curr_inval = computeIntHeaderAtByte(FFIN, iline, 4, endian, 0); //get current inline header
		curr_xval = computeIntHeaderAtByte(FFIN, xline, 4, endian, 0);  //get current xline header
		if(iscalco>0)
			scalco = computeIntHeaderAtByte(FFIN, iscalco, 2, endian, 1);
		else
			scalco = 1;

		if(xyformat==1)
		{
			curr_sx = (float) (computeIntHeaderAtByte(FFIN, isx, 4, endian, 1));
			curr_sy = (float) (computeIntHeaderAtByte(FFIN, isy, 4, endian, 1));
		}
		else if(xyformat==2)
		{
			curr_sx = computeIBMFloatHeaderAtByte(FFIN, isx, 4, endian);
			curr_sy = computeIBMFloatHeaderAtByte(FFIN, isy, 4, endian);
		}
		else
		{
			curr_sx = 0.0;
			curr_sy = 0.0;
			err("xyformat must have value 1(integer) or 2 (IBM float)");
		}


		nextTrace(FFIN, nsegy); //next trace
		setValuePositionInXline(posInXline, min_inval, min_xvalt, curr_inval, curr_xval, scalco,
				curr_sx, curr_sy, idx);

		if( (curr_inval!=inval) || (idx==maxtr-1)) /* check inline value */
		{
			/*write data*/
			if(idx==maxtr-1)
				sprintf(buffer, "%i \t %i \t\t %i \t\t %i \n", inval, nxline+1, min_xval, curr_xval);
			else
				sprintf(buffer, "%i \t %i \t\t %i \t\t %i \n", inval, nxline, min_xval, max_xval);
			fwrite(buffer, sizeof(char), strlen(buffer), FFOD);
			memset(buffer, '\0', strlen(buffer));

			/*set new xline value*/
			nxline = 0;
			min_xval = curr_xval;
			max_xval = curr_xval;
			xval = curr_xval;

			/*set new inline value*/
			inval = curr_inval;
			ninline++;
		}
		else
		{
			/* set maximum and minimum xline*/
			if(curr_xval<xval) min_xval =curr_xval;
			else if(curr_xval>xval) max_xval =curr_xval;
		}

		nxline++;
	}

	/* create the last report */
	sprintf(buffer, "\n TOTAL NTRC = %i \n Read Trace = %i \n "
			"Min_Inline = %i \n Max_Inline = %i \n Total Inline = %i \n\n"
			" Min_Xline = %i \n Max_Xline = %i\n",
			ntrc, maxtr, min_inval, max_inval, ninline, min_xvalt, max_xvalt);
	fwrite(buffer, sizeof(char), strlen(buffer), FFOD);


	//write position data
	pos3ddata_write(posInXline, width_inline, width_xline, CFPOS,
			min_inval, max_inval, min_xvalt, max_xvalt);

	free(buffer);
	pos3ddata_free(posInXline);
	fclose(FFIN);
	fclose(FFOD);
}

void getSegy3DInfo(FILE *FFIN, int maxtr, int endian, int vblock, int verbose,
		int iline, int xline,
		int *intrc, int *insegy, int *insp, int *iformat,
		int *imin_inline, int *imax_inline,
		int *imin_xline, int *imax_xline,
		int *iwidth_inline, int *iwidth_xline)
{
	int idx;
	char cebcid[3200];
	bhed bh;
	int nsp, format, nsegy, ntrc;
	int curr_inval, curr_xval;
	int min_inval, max_inval;
	int min_xval, max_xval;
	int retseek;
	int iblock;

	//read file 1 header information
	readEbcdicHeader(FFIN, cebcid); /* read ebcdic header */
	readBinaryHeader(FFIN, endian, &bh, &nsegy); /*read binary header */
	ntrc = getNumberOfTraceSegyFile(FFIN, nsegy);

	if(maxtr>ntrc) maxtr=ntrc;
	if(maxtr==0) maxtr=ntrc;

	nsp = bh.hns;
	format = bh.format;

	/*----------- READ FIRST TRACE ---------------------------*/
	curr_inval = computeIntHeaderAtByte(FFIN, iline, 4, endian, 0); //get current inline header
	curr_xval = computeIntHeaderAtByte(FFIN, xline, 4, endian, 0);  //get current xline header
	nextTrace(FFIN, nsegy); 	//next trace

	/*minimum and maximum inline*/
	min_inval = curr_inval;
	max_inval = curr_inval;

	/*minimum and maximum xline*/
	min_xval = curr_xval;
	max_xval = curr_xval;

	iblock = 1;
	for(idx=1; idx<maxtr; idx++)
	{
		/*show verbose*/
		if(verbose)
		{
			if(iblock*vblock==idx)
			{
				fprintf(stderr, "Get Info traces %i / %i \n", idx, maxtr);
				iblock++;
			}
		}

		curr_inval = computeIntHeaderAtByte(FFIN, iline, 4, endian, 0); //get current inline header
		curr_xval = computeIntHeaderAtByte(FFIN, xline, 4, endian, 0);  //get current xline header
		nextTrace(FFIN, nsegy); 	//next trace

		if(curr_inval<min_inval) min_inval = curr_inval;	//set minimum inline
		if(curr_inval>max_inval) max_inval = curr_inval;	//set maximum inline
		if(curr_xval<min_xval) min_xval = curr_xval;	//set minimum xline
		if(curr_xval>max_xval) max_xval = curr_xval;	//set maximum xline
	}

	/*return output*/
	(*intrc) = maxtr;
	(*insegy) = nsegy;
	(*insp) = nsp;
	(*iformat) = format;
	(*imin_inline) = min_inval;
	(*imax_inline) = max_inval;
	(*imin_xline) = min_xval;
	(*imax_xline) = max_xval;
	(*iwidth_inline) = max_inval - min_inval + 1;
	(*iwidth_xline) = max_xval - min_xval + 1;

	retseek = fseeko64(FFIN, 3600 , SEEK_SET);
}




