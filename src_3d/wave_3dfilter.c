/*
 * wave_3dfilter.c
 *
 *  Created on: Dec 29, 2011
 *      Author: toto
 */

#include "segy_lib.h"
#include "fftLib.h"
#include <sys/types.h>
#include <stdbool.h>
#include "wave_3dlib.h"
#include <time.h>

char *sdoc[] = {
		"									",
		" WAVE_3DFILTER.v9.2.3",
		" segyinp=           (input segy file)",
		" segyout=           (output segy file)",
		" inpos=             (input file position)",
		" usegauss=1         (0=ignore gauss computation)",
		" inline1=0          (first inline position, 0=minimum inline)",
		" inline2=0          (last inline position, 0=maximum inline)",
		" xline1=0           (first xline position, 0=minimum xline)",
		" xline2=0           (last xline position, 0=maximum xline)",
		" tfilter=3          (1=time median filter, 2=freq median filter, 3=gaussian filter)",
		" ampgauss=          (gaussian amplitude)",
		" sigma=             (gaussian standard deviation of the distribution)" ,
		" gaussnorm=1        (gaussian normalization)",
		" multd=1.0          (multiply data before ifft)",
		" distance=d1,d2,... (maximum distance from center to neighbor)",
		"",
		" mode=0             (FFT ALL DATA)",
		"    ifreq=f1,f2,f3,f4, (process in frequency f1 to fn)",
		"",
		" mode=1             (FFT SELECTED TIME POSITION)",
		"    tmin=0.0        (start time)",
		"    tmax=0.0        (stop time)",
		"    stmin=0.0       (start destination time)",
		"    stmax=0.0       (stop destination time)",
		"",
		" mode=2             (GAUSSIAN TIME WINDOW)",
		"    taper=0.5       (length of taper)",
		"    twlen=          (default: all time. time window length (minimum .3 for lower freqs))",
		"",
		" radius=2           (width radius from center to neighbor)",
		" nignore=0          (dont process data if count of neighbor < nignore)",
		" verbose=0          (show debug)",
		" vblock=100         (verbose every vblock)",
		" endian=0           (0=little endian)",
		" waituserinput=1    (press keyboard before process 3dfilter)",
		NULL};

#define LOOKFAC	2	/* Look ahead factor for npfaro	  */
#define PFA_MAX	720720	/* Largest allowed nfft*/
#define PI (3.141592653589793)
/* --------- GLOBAL VARIABLE --------------- */
int usegauss;
int bnormalize;
int inline1, inline2, xline1, xline2;
int tfilter, tradius, lenradius, nignore;
int ndistance;
int nf, nfft;
int nsegy, ntrc;
float dt, d1, multd;
int centerpos;
int nwindowing, nsp, format;
int verbose, vblock, endian;
int widthInline, widthXline, min_iline, max_iline, min_xline, max_xline;
int mode;
float sigma, ampgauss;
float ftmin, ftmax, fstmin, fstmax;
int tmin=0, tmax=0, stmin=0, stmax=0;

int nspws;	       	/* number of samples in window (wo taper)*/
int nspwf;	       	/* number of samples in first window    */
int nspwi;	       	/* number of samples in intermed. window */
int nstaper;	       	/* number of samples in taper		*/
int ntw;	       	/* number of time windows		*/
float taper;		/* length of taper			*/
float twlen;       	/* time window length 			*/

void processWindowingTrace(pos3ddata **windowing, FILE *inpsegy, FILE *outsegy,
		int curr_trace, int *timeData, int ntime);

void runGaussMode0(pos3ddata **windowing, FILE *inpsegy, FILE *outsegy,
		int curr_trace, int *timeData, int ntime, float *distanceData);
void runGaussMode1(pos3ddata **windowing, FILE *inpsegy, FILE *outsegy,
		int curr_trace, float *distanceData);
void runGaussMode2(pos3ddata **windowpos, FILE *inpsegy, FILE *outsegy,
		int curr_trace, float *distanceData);

float *processMedianFilter(float **tmpdata, int nwindowing, int nsp, int tfilter, int nf, int nfft,
		int *timeData, int ntime, int centerpos);

void gaussTimeWindowingData(pos3ddata **windowpos, float **tdatain, float **tdataout,
		int curr_trace, float *distanceData);
float **readTraceInWindow(FILE *inpsegy, pos3ddata **windowing);
int getTimePositionFromDistance(float *distanceData, int ndistance, float r);
void computeGaussVariable(pos3ddata **windowing, float *frnorm, float *ftotgauss);
int main (int argc, char **argv)
{
	/* hook up getpar to handle the parameters */
	initargs(argc,argv);
	requestdoc(1);

	char cebcdic[3200];
	char *csegyinp, *csegyout, *inpos;
	int *timeData=NULL, ntime=0;
	float *freqData=NULL;
	int it1;
	int waituserinput;

	float *distanceData=NULL, maxDistance;
	pos3ddata **data=NULL;
	FILE *segyinp=NULL, *segyout=NULL;
	int i;

	int leninline, lenxline;
	int idxinline, idxxline, tmp2;
	int j, curr_trace;

	pos3ddata **windowpos=NULL;
	int itr, idxblock;


	div_t divresult;
	time_t t1,t2;

	MUSTGETPARSTRING("segyinp",  &csegyinp);
	MUSTGETPARSTRING("segyout",  &csegyout);
	MUSTGETPARSTRING("inpos",  &inpos);
	if (!getparint("usegauss",&usegauss)) 	usegauss=1;

	if (!getparint("inline1",&inline1)) 	inline1=0;
	if (!getparint("inline2",&inline2)) 	inline2=0;
	if (!getparint("xline1",&xline1)) 	    xline1=0;
	if (!getparint("xline2",&xline2)) 	    xline2=0;
	if (!getparint("tfilter",&tfilter)) 	tfilter=3;		//type of filter. 1=time median, 2=frequency median, 3=gaussian
	if (!getparint("tradius",&tradius)) 	tradius=2;		//type of radius. 1=xline/inline, 2=position sx/gx
	if (!getparint("waituser",&waituserinput)) 	waituserinput=1;
	if (!getparint("gaussnorm",&bnormalize)) 	bnormalize=1;
	if (!getparfloat("multd",&multd)) 	multd=1.0;

	if ((ndistance = countparval("distance"))!=0)  //maximum distance from center to (sx,sy)neighbor
	{
		distanceData = ealloc1float(ndistance);
		getparfloat("distance", distanceData);
	}
	else  err("distance=??");


	if (!getparint("mode",&mode)) 	mode=0;
	if(mode==0)
	{
		if ((ntime = countparval("ifreq"))!=0)
		{
			if(ntime != ndistance*2)
				err("length(ifreq) must be 2x length(distance)");
			timeData = ealloc1int(ntime);
			freqData = ealloc1float(ntime);
			getparfloat("ifreq", freqData);
		}

		if(ntime==0) err("or ifreq=??");
	}
	else
	{
		if (!getparfloat("tmin",&ftmin)) 	ftmin=0.0;
		if (!getparfloat("tmax",&ftmax)) 	ftmax=0.0;
		if (!getparfloat("stmin",&fstmin)) 	fstmin=0.0;
		if (!getparfloat("stmax",&fstmax)) 	fstmax=0.0;
	}

	if (!getparint("radius",&lenradius)) 	lenradius=2;	//radius neigbor trace
	if (!getparint("nignore",&nignore)) 	nignore=0; 		//ignore is neighbor trace less than nignore
	if (!getparint("verbose",&verbose)) 	verbose = 0;
	if (!getparint("vblock",&vblock)) vblock = 100;
	if (!getparint("endian",&endian)) endian = 0;
	if(tfilter==3){ //gaussian filter
		if (!getparfloat("ampgauss",&ampgauss)) err("ampgauss=??");
		if (!getparfloat("sigma",&sigma)) err("sigma=""");
	}

	/* start process */
	t1 = time(NULL);

	//read pos xline and xline data
	data = pos3ddata_read(inpos, &widthInline, &widthXline,
			&min_iline, &max_iline, &min_xline, &max_xline);

	fprintf(stderr, "\nTRACE INFO\n");
	fprintf(stderr, "Input File          = %s \n", csegyinp);
	fprintf(stderr, "Output File         = %s \n", csegyout);
	fprintf(stderr, "Input Position File = %s \n\n", inpos);
	fprintf(stderr, "Inline Min          = %i \n", min_iline);
	fprintf(stderr, "Inline Max          = %i \n", max_iline);
	fprintf(stderr, "Inline Width        = %i \n", widthInline);
	fprintf(stderr, "Xline Min           = %i \n", min_xline);
	fprintf(stderr, "Xline Max           = %i \n", max_xline);
	fprintf(stderr, "Xline Width         = %i \n\n", widthXline);
        fprintf(stderr, "tfilter             = %i \n\n", tfilter);

	//-----------create log----------------
	if(verbose)
	{
		printf("\nTRACE INFO\n");
		printf("Input File = %s \n", csegyinp);
		printf("Output File = %s \n", csegyout);
		printf("Input Position File = %s \n\n", inpos);
		printf("Inline Min = %i \n", min_iline);
		printf("Inline Max = %i \n", max_iline);
		printf("Inline Width= %i \n", widthInline);
		printf("Xline Min = %i \n", min_xline);
		printf("Xline Max = %i \n", max_xline);
		printf("Xline Width= %i \n\n", widthXline);
	}
	//-----------end of log----------------
	segyinp = fopen (csegyinp,"r");
	if (segyinp==NULL) err ("Error opening file 1 : %s\n", csegyinp);

	segyout = fopen (csegyout,"w");
	if (segyout==NULL) err ("Error opening file 2 : %s\n", csegyout);

	//read file 1 header information
	readEbcdicHeader(segyinp, cebcdic); /* read ebcdic header */
	readBinaryHeader(segyinp, endian, &bh, &nsegy); /*read binary header */
	ntrc = getNumberOfTraceSegyFile(segyinp, nsegy);

	nsp = bh.hns;
	format = bh.format;
	dt = (float) (bh.hdt/1e+6);
	getNfft(nsp, dt, &nf, &nfft, &d1);

	/* GAUSSIAN TIME WINDOWING INPUT */
	if (!getparfloat("taper", &taper)) taper=.1;
	if (taper==0.0) taper=.004;
	if (!getparfloat("twlen", &twlen)) twlen=(float)(nsp-1)*dt;
	if (twlen<.3)
	{
		twlen=.3;
		if (verbose) warn("twlen cannot be less than .3s, using .3s");
	}

	/* check it time input value */
	if(ntime==2) //process all data
	{
		if(freqData[0]==0.0) 	timeData[0] = 0;
		else 	timeData[0] = (int) (round(freqData[0]/d1));

		if(freqData[1]==0.0)  timeData[1] = nf-1;
		else  timeData[1] = (int) (round(freqData[1]/d1));

		if(timeData[1]>nf) timeData[1] = nf;
	}
	else  //check selected time position
	{
		for(i=0; i<ntime; i++)
		{
			it1 = (int) (round(freqData[i]/d1));
			if(it1>=nf)
				timeData[i] = nf-1;
			else
				timeData[i] = it1;
		}
	}

	//check distance value
	maxDistance = distanceData[0];
	for(i=1; i<ndistance; i++)
	{
		if(maxDistance<distanceData[i])
			maxDistance = distanceData[i];
	}
	/*----------------------- show time and distance log -----------------*/
	fprintf(stderr, "\n SHOW DISTANCE \n");
	fprintf(stderr, "Maximum distance    = %5.3f \n", maxDistance);
	for(i=0; i<ndistance; i++)
		fprintf(stderr, "[%i] = %5.3f \n", i, distanceData[i]);
	fprintf(stderr, "\n");

	fprintf(stderr, "\n");
	fprintf(stderr, " nsp = %i \n", nsp);
	fprintf(stderr, " dt = %f \n", dt);
	if(mode==0){
		fprintf(stderr, "        nf = %i \n", nf);
		fprintf(stderr, "      nfft = %i \n", nfft);
		fprintf(stderr, "        d1 = %f \n", d1);
		fprintf(stderr, "         F = %f \n\n", d1*nf);
		fprintf(stderr, "\n SHOW TIME/FREQUENCY SLICING \n");
		for(i=0; i<ntime; i++)
			fprintf(stderr, "[%i]   freq=[%5.3f]   index=[%i] \n", i, freqData[i], timeData[i]);
		fprintf(stderr, "\n");

		for (i=0; i<ntime/2; i++)
		{
			if(timeData[i*2]>timeData[i*2+1])
				err("Error slicing time at index %i. [%i > %i] ", i*2, timeData[i*2], timeData[i*2+1]);
		}
	}
	else if(mode==1)
	{
		if(ftmin==0.0) tmin = 0;
		else tmin = (int) (ftmin/dt);
		if(ftmax==0) tmax = nsp;
		else tmax = (int) (ftmax/dt);
		if(tmax>nsp) tmax = nsp;

		if(fstmin==0.0) stmin = 0;
		else stmin = (int) (fstmin/dt);
		if(fstmax==0) stmax = nsp;
		else stmax = (int) (fstmax/dt);
		if(stmax>nsp) stmax = nsp;
		fprintf(stderr, "      tmin = %i \n", tmin);
		fprintf(stderr, "      tmax = %i \n", tmax);
		fprintf(stderr, "     stmin = %i \n", stmin);
		fprintf(stderr, "     stmax = %i \n", stmax);
		if((tmax-tmin) < 0 || (tmax-tmin)!=(stmax-stmin))
			err("Error : (tmax-tmin) < 0) OR (tmax-tmin)!=(stmax-stmin)");
	}
	else if(mode==2)
	{
		/* setting taper and spatial and temporal windows */
		nstaper=NINT(taper/dt);
		nstaper = (nstaper%2 ? nstaper+1 : nstaper);
		ntw = NINT((nsp-1)*dt/twlen);
		if (ntw==1) taper=0.0;
		nspws = NINT(twlen/dt) + 1;
		nspwf = nspws + nstaper/2;
		nspwi = nspws + nstaper;

		/* Set up pfa fft */
		nfft = npfaro(2*nspwf, LOOKFAC * 2*nspwf);
		if (nfft >= PFA_MAX)
			err("Padded nsp=%d--too big", nfft);
		nf = nfft/2 + 1;
		d1 = 1.0/(nfft*dt);

		fprintf(stderr," Mode    : %i \n", mode);
		fprintf(stderr," nf      : %i \n", nf);
		fprintf(stderr," nfft    : %i \n", nfft);
		fprintf(stderr," nspws   : %i \n", nspws);
		fprintf(stderr," nspwf   : %i \n", nspwf);
		fprintf(stderr," nspwi   : %i \n", nspwi);
		fprintf(stderr," d1      : %f \n\n", d1);
	}
	else
		err("use mode=0, 1 or 2");
		/* -------------------------------------------------------------------*/
	if(waituserinput!=0)
		getchar();

	if(inline1==0) inline1 = min_iline;
	if(inline2==0) inline2 = max_iline;
	if(xline1==0) xline1 = min_xline;
	if(xline2==0) xline2 = max_xline;

	if(inline1<min_iline) inline1 = min_iline;
	if(inline2>max_iline) inline2 = max_iline;
	if(inline1>inline2) err("inline1 (%i) > inline2 (%i) ", inline1, inline2);

	if(xline1<min_xline) xline1 = min_xline;
	if(xline2>max_xline) xline2 = max_xline;
	if(xline1>xline2) err("xline1 (%i) > xline2 (%i) ", xline1, xline2);

	/*write output header */
	bh.format = 1;
	writeEbcdicHeader(segyout, cebcdic);
	writeBinaryHeader(segyout, endian, &bh);

	/*length of input inline*/
	idxinline = inline1 - min_iline;
	tmp2 = inline2 - min_iline;
	leninline = tmp2-idxinline+1;

	/*length of input xline*/
	idxxline = xline1 - min_xline;
	tmp2 = xline2 - min_xline;
	lenxline = tmp2-idxxline+1;

	idxblock = 1;
	if(verbose){
		printf("\ncenter trace : trace neighbor = sum of trace neighbor \n");
		printf("----------------------------------------------------------- \n");
	}

	for(i=0; i<leninline; i++)
	{
		for(j=0; j<lenxline; j++)
		{
			//get position
			if(data[idxinline+i][idxxline+j].iline>0 && data[idxinline+i][idxxline+j].xline>0)
			{

				if (tradius==1)//inline-xline radius
				{
					windowpos = pos3ddata_getradiusWithSpace(data, idxxline+j, idxinline+i,
							lenradius, widthInline, widthXline, nignore, &nwindowing, &centerpos);
				}
				else //sx-gx radius
				{
					windowpos = pos3ddata_getradiusWithRadius(data, idxinline+i, idxxline+j,
							lenradius, maxDistance, widthInline, widthXline, nignore, &nwindowing, &centerpos, verbose);
				}

				curr_trace = data[idxinline+i][idxxline+j].trace; //get current trace
				if(nwindowing>=nignore)
				{
					fprintf(stderr,"Process [%i,%i] - [%i] - %i - %i\n", inline1+i, xline1+j, nwindowing, curr_trace, tfilter);

					if(tfilter==3)
					{
						if(mode==0)
						{
							runGaussMode0(windowpos, segyinp, segyout,
									curr_trace, timeData, ntime, distanceData);
						}
						else if(mode==1)
						{
							runGaussMode1(windowpos, segyinp, segyout,
									curr_trace, distanceData);
						}
						else if(mode==2) //gaussian time windowing
						{
							runGaussMode2(windowpos, segyinp, segyout, curr_trace, distanceData);
						}
					}
					else
					{
						processWindowingTrace(windowpos, segyinp, segyout, curr_trace,
								timeData, ntime);
					}
				}
				else //save original data
				{
					gotoTraceSegyPosition(segyinp, curr_trace, nsegy);   //	go to current trace position
					readTraceSegy(segyinp, endian, nsp, format, nsegy, &tr); //	read current trace
					writeTraceSegy(segyout, endian, &tr, nsegy, nsp); //save trace
				}

				if(windowpos) pos3ddata_free(windowpos);
			}
			itr++;
		}
	}

	pos3ddata_free(data);
	free1float(distanceData);
	free1int(timeData);
	if(freqData!=NULL) free1float(freqData);

	fclose(segyinp);
	fclose(segyout);

	t2 = time(NULL);
	divresult = div (t2-t1, 60);
	fprintf (stderr, "Process time = %d min %d sec\n", divresult.quot, divresult.rem);
	if(verbose)
		printf ("\nProcess time = %d min %d sec\n", divresult.quot, divresult.rem);

	return(1);
}

void runGaussMode1(pos3ddata **windowpos, FILE *inpsegy, FILE *outsegy,
		int curr_trace, float *distanceData)
{
	float center_sx, center_sy;
	float curr_sx, curr_sy;
	int i,j,k, itrace;
	float gausval, r, rn;
	double dsx, dsy;

	float d1;
	int ntwindow, nf, nfft;


	float rmax, rnorm, totgauss;

	float **indata, **dreal, **dimag;
	float **dsumreal, **dsumimag;
	float **outdata;

	ntwindow = tmax-tmin+1;
	getNfft(ntwindow, dt, &nf, &nfft, &d1);
	center_sx = windowpos[0][centerpos].sx;
	center_sy = windowpos[0][centerpos].sy;

	//read trace and compute distance from all trace
	outdata = alloc2float(ntwindow, 1);
	indata = alloc2float(ntwindow, 1);
	dreal = alloc2float(nf, 1);
	dimag = alloc2float(nf, 1);
	dsumreal = alloc2float(nf, 1);
	dsumimag = alloc2float(nf, 1);
	memset(dsumreal[0], '\0', nf*sizeof(float));
	memset(dsumimag[0], '\0', nf*sizeof(float));

	// get central trace
	memset(indata[0], '\0', ntwindow*sizeof(float));
	memset(dreal[0], '\0', nf*sizeof(float));
	memset(dimag[0], '\0', nf*sizeof(float));
	itrace = windowpos[0][centerpos].trace;//get trace location
	gotoTraceSegyPosition(inpsegy, itrace, nsegy); //go to curr_trace location
	readTraceSegy(inpsegy, endian, nsp, format, nsegy, &tr); //read current trace

	//get trace data
	for (j=0; j<ntwindow; j++) indata[0][j] = tr.data[j+tmin];
	fftProcess(indata, ntwindow, 1, nf, nfft, dreal, dimag);
	for (j=0; j<nf; j++) {
		dsumreal[0][j] = dreal[0][j];
		dsumimag[0][j] = dimag[0][j];
	}

	rnorm = 1.0;
	totgauss = 1.0;
	rmax = 0.0;
	if(bnormalize)	//normalisasi distance
	{
		for(i=0; i<nwindowing; i++){
			//get distance from all trace
			curr_sx = windowpos[0][i].sx;
			curr_sy = windowpos[0][i].sy;
			dsx = (double) (center_sx - curr_sx);
			dsy = (double) (center_sy - curr_sy);

			r = (float) (sqrt(pow(dsx, 2.0) + pow(dsy, 2.0))); 	//compute distance

			if(i==0)
				rmax = r;
			else
				if(r>rmax)	rmax = r;
		}

		rnorm = rmax/(2*PI);

		//compute sum of gauss value
		totgauss = 0.0;
		for(i=0; i<nwindowing; i++){
			//get distance from all trace
			curr_sx = windowpos[0][i].sx;
			curr_sy = windowpos[0][i].sy;
			dsx = (double) (center_sx - curr_sx);
			dsy = (double) (center_sy - curr_sy);

			r = (float) (sqrt(pow(dsx, 2.0) + pow(dsy, 2.0))); 	//compute distance
			rn = r/rnorm;
			gausval = gaussian1d(ampgauss, rn,  sigma);			//compute gaussian
			totgauss = totgauss + gausval;
		}
		if(totgauss==0)	totgauss = 1.0;
	}

	for(i=0; i<nwindowing; i++)
	{
		if (i != centerpos) { // get trace other than center trace location

			memset(indata[0], '\0', ntwindow*sizeof(float));
			memset(dreal[0], '\0', nf*sizeof(float));
			memset(dimag[0], '\0', nf*sizeof(float));

			itrace = windowpos[0][i].trace;								//get trace location
			gotoTraceSegyPosition(inpsegy, itrace, nsegy); 				//go to curr_trace location
			readTraceSegy(inpsegy, endian, nsp, format, nsegy, &tr); 	//read current trace

			//get distance from all trace
			curr_sx = windowpos[0][i].sx;
			curr_sy = windowpos[0][i].sy;
			dsx = (double) (center_sx - curr_sx);
			dsy = (double) (center_sy - curr_sy);

			r = (float) (sqrt(pow(dsx, 2.0) + pow(dsy, 2.0))); 	//compute distance
			rn = r/rnorm;	//distance normalization
			gausval = gaussian1d(ampgauss, rn,  sigma);			//compute gaussian
			gausval = gausval/totgauss;	//gauss normalization
			
			//get trace data	
			for (j=0; j<ntwindow; j++) indata[0][j] = tr.data[j+tmin];
			fftProcess(indata, ntwindow, 1, nf, nfft, dreal, dimag);

			if(!usegauss) gausval = 1.0;

			for(k=0; k<ndistance; k++) 			//loop over ndistance
			{
				if(r<=distanceData[k])			//check distance location
				{
					for(j=0; j<nf; j++)	//fill data in time/frequency selected window
					{
						dsumreal[0][j] += dreal[0][j]*gausval;
						dsumimag[0][j] += dimag[0][j]*gausval;
					}
				}

			}
		}
	}

	if(multd!=1.0)
	{
		for(i=0; i<nf; i++){
			dsumreal[0][i] = dsumreal[0][i]*multd;
			dsumimag[0][i] = dsumimag[0][i]*multd;
		}
	}

	ifftProcess(outdata, ntwindow, 1, nf, nfft, dsumreal, dsumimag);	//invers fft data

	gotoTraceSegyPosition(inpsegy, curr_trace, nsegy);   //	go to center trace location
	readTraceSegy(inpsegy, endian, nsp, format, nsegy, &tr); //	read current trace

	int idx = 0;
	for (j=stmin; j<stmax; j++) //replace trace value
	{
		tr.data[j] = outdata[0][idx];
		idx++;
	}
//	warn("write");

	writeTraceSegy(outsegy, endian, &tr, nsegy, nsp); //save trace

	free2float(outdata);
	free2float(indata);
	free2float(dreal);
	free2float(dimag);
	free2float(dsumreal);
	free2float(dsumimag);
}

void runGaussMode0(pos3ddata **windowpos, FILE *inpsegy, FILE *outsegy,
		int curr_trace, int *timeData, int ntime, float *distanceData)
{
	float center_sx, center_sy;
	float curr_sx, curr_sy;
	int i,j;
	float gausval, r, rn;
	double dsx, dsy;
	int itmin, itmax, k;

	float rnorm, totgauss;

	float **indata, **dreal, **dimag;
	float *dsumreal, *dsumimag;
	float *outdata;

	center_sx = windowpos[0][centerpos].sx;
	center_sy = windowpos[0][centerpos].sy;

	//read trace and compute distance from all trace
	dreal = alloc2float(nf, nwindowing);
	dimag = alloc2float(nf, nwindowing);
	outdata = alloc1float(nsp);
	dsumreal = alloc1float(nf);
	dsumimag = alloc1float(nf);

	memset(dsumreal, '\0', nf*sizeof(float));
	memset(dsumimag, '\0', nf*sizeof(float));
	memset(dreal[0], '\0', nf*nwindowing*sizeof(float));
	memset(dimag[0], '\0', nf*nwindowing*sizeof(float));

	indata = readTraceInWindow(inpsegy, windowpos);
	fftProcess(indata, nsp, nwindowing, nf, nfft, dreal, dimag);

	//fill default sum data with the center of trace
	for (j=0; j<nf; j++) {
		dsumreal[j] = dreal[centerpos][j];
		dsumimag[j] = dimag[centerpos][j];
	}

	computeGaussVariable(windowpos, &rnorm, &totgauss);

	for(i=0; i<nwindowing; i++)
	{
		if(i==centerpos) continue;

		//get distance from all trace
		curr_sx = windowpos[0][i].sx;
		curr_sy = windowpos[0][i].sy;
		dsx = (double) (center_sx - curr_sx);
		dsy = (double) (center_sy - curr_sy);

		r = (float) (sqrt(pow(dsx, 2.0) + pow(dsy, 2.0))); 	//compute distance
		rn = r/rnorm;	//distance normalization
		gausval = gaussian1d(ampgauss, rn,  sigma);			//compute gaussian
		gausval = gausval/totgauss;	//gauss normalization

		if(!usegauss) gausval = 1.0;

		for(k=0; k<ndistance; k++) 			//loop over ndistance
		{
			if(r>distanceData[k])			//check distance location
				continue;

			itmin = timeData[k*2];		//compute t minimum
			itmax = timeData[k*2+1];	//compute t maximum
			for(j=itmin; j<=itmax; j++)	//fill data in time/frequency selected window
			{
				dsumreal[j] += dreal[i][j]*gausval;
				dsumimag[j] += dimag[i][j]*gausval;
			}
		}
	}

	if(multd!=1.0)
	{
		for(i=0; i<nf; i++){
			dsumreal[i] *= multd;
			dsumimag[i] *= multd;
		}
	}
	ifft1_1ff(outdata, nsp, nf, nfft, dsumreal, dsumimag);	//invers fft data

	gotoTraceSegyPosition(inpsegy, curr_trace, nsegy);   //	go to center trace location
	readTraceSegy(inpsegy, endian, nsp, format, nsegy, &tr); //	read current trace
	memcpy(tr.data, outdata, nsp*sizeof(float)); //copy trace

	writeTraceSegy(outsegy, endian, &tr, nsegy, nsp); //save trace

	free2float(indata);
	free2float(dreal);
	free2float(dimag);
	free1float(outdata);
	free1float(dsumreal);
	free1float(dsumimag);
}

int getTimePositionFromDistance(float *distanceData, int ndistance, float r)
{
	int i;
	int pos=0;

	for (i=ndistance-1; i>=0; i--)
	{
		if(r<distanceData[i])
		{
			pos = i;
			break;
		}
	}
	return(pos);
}

void processWindowingTrace(pos3ddata **windowpos, FILE *inpsegy, FILE *outsegy,
		int curr_trace, int *timeData, int ntime)
{
	int i, j;
	int itrace;
	float **tmpdata, *outdata=NULL;

	//fprintf(stderr,"alloc data \n");

	tmpdata = alloc2float(nsp, nwindowing);
	for(i=0; i<nwindowing; i++)
	{
		itrace = windowpos[0][i].trace;//get trace location
		gotoTraceSegyPosition(inpsegy, itrace, nsegy); //go to curr_trace location
		readTraceSegy(inpsegy, endian, nsp, format, nsegy, &tr); //read current trace

		//convert trace to array
		for (j=0; j<nsp; j++) tmpdata[i][j] = tr.data[j];
	}
	//fprintf(stderr,"start median filter nwindowing=%i\n",nwindowing);
	outdata = processMedianFilter(tmpdata, nwindowing, nsp, tfilter, nf, nfft, timeData, ntime, centerpos); //process windowing data (filter)

	//fprintf(stderr,"go to center of trace \n");
	gotoTraceSegyPosition(inpsegy, curr_trace, nsegy);   // go to center trace location
	readTraceSegy(inpsegy, endian, nsp, format, nsegy, &tr); //     read current trace

	//fprintf(stderr,"save data \n");
	for (j=0; j<nsp; j++) //replace trace value
		tr.data[j] = outdata[j];
	writeTraceSegy(outsegy, endian, &tr, nsegy, nsp); //save trace

	free2float(tmpdata);
	free1float(outdata);
}

//void processWindowingTrace(pos3ddata **windowing, int nwindowing, FILE *inpsegy, FILE *outsegy,
//		int curr_trace, int nsegy, int nsp, int format, int endian, int *timeData, int ntime,
//		int tfilter, int nf, int nfft, int centerpos)
//{
//	int i, j;
//	int itrace;
//	float **tmpdata, *outdata=NULL;
//	float center_sx, center_sy;
//	float curr_sx, curr_sy, dsx, dsy, *r;
//	float *tmpdataimean;
//	int itmpdataimean;
//
//	center_sx = windowing[0][centerpos].sx;
//	center_sy = windowing[0][centerpos].sy;
//
//	tmpdata = alloc2float(nsp, nwindowing);
//	r = alloc1float(nwindowing);
//	for(i=0; i<nwindowing; i++)
//	{
//		//get distance from all trace
//		curr_sx = windowing[0][i].sx;
//		curr_sy = windowing[0][i].sy;
//		dsx = (double) (center_sx - curr_sx);
//		dsy = (double) (center_sy - curr_sy);
//
//		r[i] = (float) (sqrt(pow(dsx, 2.0) + pow(dsy, 2.0))); 	//compute distance
//
//		itrace = windowing[0][i].trace;//get trace location
//		gotoTraceSegyPosition(inpsegy, itrace, nsegy); //go to curr_trace location
//		readTraceSegy(inpsegy, endian, nsp, format, nsegy, &tr); //read current trace
//
//		//convert trace to array
//		for (j=0; j<nsp; j++) tmpdata[i][j] = tr.data[j];
//	}
//
//	tmpdataimean = alloc1float(nwindowing);
////	for(k=0; k<ndistance; k++) 			//loop over ndistance
////	{
////		itmpdataimean = 0;
////		for(i=0; i<nwindowing; i++)
////		{
////			if(r[i]<=distanceData[k])			//check distance location
////			{
////				for(j=0; j<ns; j++)
////				{
////				tmpdataimean[i] = tmpdata[]
////				}
////			}
////		}
////
////		itmin = timeData[k*2];		//compute t minimum
////		itmax = timeData[k*2+1];	//compute t maximum
////		for(j=itmin; j<=itmax; j++)	//fill data in time/frequency selected window
////		{
////			dsumreal[0][j] += dreal[0][j]*gausval;
////			dsumimag[0][j] += dimag[0][j]*gausval;
////		}
////	}
////
////	outdata = processMedianFilter(tmpdata, nwindowing, nsp, tfilter, nf, nfft, timeData, ntime, centerpos); //process windowing data (filter)
////
////
////	gotoTraceSegyPosition(inpsegy, curr_trace, nsegy);   //	go to center trace location
////	readTraceSegy(inpsegy, endian, nsp, format, nsegy, &tr); //	read current trace
////
////
////	for (j=0; j<nsp; j++) //replace trace value
////		tr.data[j] = outdata[j];
////
////
////	writeTraceSegy(outsegy, endian, &tr, nsegy, nsp); //save trace
////
////	free2float(tmpdata);
////	free1float(outdata);
////	free1float(r);
////	free1float(tmpdataimean);
//}

float *processMedianFilter(float **tmpdata, int nwindowing, int nsp, int tfilter, int nf, int nfft,
		int *timeData, int ntime, int centerpos)
{
	float *outdata=NULL;
	float *tdata=NULL, *tdatar=NULL, *tdatai=NULL;
	float **dreal=NULL, **dimag=NULL, **drealf=NULL, **dimagf=NULL;
	int i=0,j=0,k=0;
	int itmin, itmax;
	outdata = alloc1float(nsp);

	if(tfilter==1)	//time median filter
	{
		memcpy(outdata, tmpdata[centerpos], nsp*sizeof(float));
		tdata = alloc1float(nwindowing);
		for(i=0; i<ntime/2; i++)//loop over time windowing
		{
			itmin = timeData[i*2];
			itmax = timeData[i*2+1];
			for(j=itmin; j<itmax; j++)	//loop over tmin and tmax
			{
				for(k=0; k<nwindowing; k++)	//loop over windowing
					tdata[k] = tmpdata[k][j];

				outdata[j] = getMedian(tdata, nwindowing);
			}
		}
		free1float(tdata);
	}
	else if(tfilter==2)	//frequency median filter
	{
		tdatar = alloc1float(nwindowing);
		tdatai = alloc1float(nwindowing);
		dreal = alloc2float(nf, nwindowing);
		dimag = alloc2float(nf, nwindowing);
		drealf = alloc2float(nf, 1);
		dimagf = alloc2float(nf, 1);
		fftProcess(tmpdata, nsp, nwindowing, nf, nfft, dreal, dimag); //fft data

		//fprintf(stderr, "copy real and image data nwindowing=%i\n",nwindowing);
		memcpy(drealf[0], dreal[centerpos], nf*sizeof(float)); //save original value
		memcpy(dimagf[0], dimag[centerpos], nf*sizeof(float));
		for(i=0; i<ntime/2; i++)
		{
			itmin = timeData[i*2];
			itmax = timeData[i*2+1];
			//fprintf(stderr,"itmin=%i itmax=%i [%i/%i]\n", itmin, itmax,i+1, ntime);
			for(j=itmin; j<itmax; j++)
			{
				for(k=0; k<nwindowing; k++)
				{
					tdatar[k] = dreal[k][j];
					tdatai[k] = dimag[k][j];
				}
				drealf[0][j] = getMedian(tdatar, nwindowing);
				dimagf[0][j] = getMedian(tdatai, nwindowing);
			}
		}

		//fprintf(stderr,"invers fft\n");
		ifftProcess(tmpdata, nsp, 1, nf, nfft, drealf, dimagf);
		memcpy(outdata, tmpdata[0], nsp*sizeof(float));

		free1float(tdatar);
		free1float(tdatai);
		free2float(dreal);
		free2float(dimag);
		free2float(drealf);
		free2float(dimagf);
	}
	return(outdata);
}

void runGaussMode2(pos3ddata **windowpos, FILE *inpsegy, FILE *outsegy,
		int curr_trace, float *distanceData)
{
	float **data=NULL;
	float **odata=NULL;


	//read data
	data = readTraceInWindow(inpsegy, windowpos);
	odata = alloc2float(nsp, 1);

	//windowing and process gauss
	gaussTimeWindowingData(windowpos, data, odata, curr_trace, distanceData);

	gotoTraceSegyPosition(inpsegy, curr_trace, nsegy); 				//go to curr_trace location
	readTraceSegy(inpsegy, endian, nsp, format, nsegy, &tr); 	//read current trace
	memcpy(tr.data, odata[0], nsp*sizeof(float));
	writeTraceSegy(outsegy, endian, &tr, nsegy, nsp); //save trace

	 free2float(data);
	 free2float(odata);
}

void gaussTimeWindowingData(pos3ddata **windowpos, float **tdatain, float **tdataout,
		int curr_trace, float *distanceData)
{
	int iw, jw, it, ifq, itt;
	int nspw;
	int wntrc; //trace in window

	float *ttidataw;      	/* real trace in time window - input   	*/
	float *tidataw;       	/* real trace in time window - input   	*/
	float *todataw;       	/* real trace in time window - output   */
	float *ttodataw;      	/* real trace in time window - output   */
	complex **fdata;	/* data - freq. domain          	*/
	complex *ct;		/* complex transformed trace (window)  	*/
	complex **ffreq;	/* filtered frequency vector 	  	*/
	complex *freqv;		/* frequency vector		      	*/

	//gaussian variable
	int i, j, k;
	float center_sx, center_sy;
	float curr_sx, curr_sy;
	float rnorm, totgauss;
	double dsx, dsy;
	float gausval, r, rn;

	center_sx = windowpos[0][centerpos].sx;
	center_sy = windowpos[0][centerpos].sy;

	//compute gaussian variable
	computeGaussVariable(windowpos, &rnorm, &totgauss);
	wntrc = nwindowing;

	tidataw=ealloc1float(nfft);
	ttidataw=ealloc1float(nfft);
	ct = ealloc1complex(nfft);
	fdata = alloc2complex(nf,wntrc);
	ffreq = alloc2complex(nf,1);
	freqv = alloc1complex(nfft);

	todataw = ealloc1float(nfft);
	ttodataw = ealloc1float(nfft);

	for (iw=0;iw<ntw;iw++) /*00. loop over time windows */
	{

		if (iw>0 && iw<ntw-1)
			nspw=nspwi;
		else if (iw==0)
		{
			if (ntw>1) nspw = nspwf;
			else        nspw = nsp;
		}
		else
			nspw = nsp - nspws*iw + nstaper/2;

		if(verbose)
			fprintf(stderr, "Time Window %i / %i Length=%i\n", iw+1, ntw, nspw);

		/* zero fdata */
		memset((void *) fdata[0], 0, nf*wntrc*sizeof(complex));

		for (jw=0;jw<wntrc;jw++)  /*01. loop over trace */
		{
			if (iw>0)
			{
				for (it=0;it<nspw;it++)
					tidataw[it] = tdatain[jw][it + iw*nspws - nstaper/2];
			}
			else
			{
				for (it=0;it<nspw;it++)
					tidataw[it]=tdatain[jw][it];
			}

			memset((void *) (tidataw + nspw), 0, (nfft-nspw)*sizeof(float));
			memset((void *) ct, 0, nfft*sizeof(complex));

			/* FFT from t to f */
			for (it=0;it<nfft;it++)
				ttidataw[it]=(it%2 ? -tidataw[it] : tidataw[it]);
			pfarc(1, nfft, ttidataw, ct);

			/* Store values */
			for (ifq = 0; ifq < nf; ifq++)
				fdata[jw][ifq] = ct[nf-1-ifq];
		} /*01. end of trace loop*/

		/* ------------ Process FFT Data At Here. Save Output Data in ffreq ----------- */
		//fill default sum data with the center of trace
		for (j=0; j<nf; j++) {
			ffreq[0][j].r = fdata[centerpos][j].r;
			ffreq[0][j].i = fdata[centerpos][j].i;
		}

		for(i=0; i<wntrc; i++)
		{
			if(i==centerpos) continue;

			//get distance from all trace
			curr_sx = windowpos[0][i].sx;
			curr_sy = windowpos[0][i].sy;
			dsx = (double) (center_sx - curr_sx);
			dsy = (double) (center_sy - curr_sy);

			r = (float) (sqrt(pow(dsx, 2.0) + pow(dsy, 2.0))); 	//compute distance
			rn = r/rnorm;	//distance normalization
			gausval = gaussian1d(ampgauss, rn,  sigma);			//compute gaussian
			gausval = gausval/totgauss;	//gauss normalization

			if(!usegauss) gausval = 1.0;

			for(k=0; k<ndistance; k++) 			//loop over ndistance
			{
				if(r>distanceData[k])			//check distance location
					continue;

				for(j=0; j<nf; j++)	//fill data in time/frequency selected window
				{
					ffreq[0][j].r += fdata[i][j].r * gausval;
					ffreq[0][j].i += fdata[i][j].i * gausval;
				}
			}
		}

		if(multd!=1.0)
		{
			for(j=0; j<nf; j++){
				ffreq[0][j].r *= multd;
				ffreq[0][j].r *= multd;
			}
		}

		/* -----------------------------------------------------------------------------*/

		// ------- INVERS FFT -----------
		for (jw=0;jw<1;jw++) /*02. loop over trace */
		{
			/* select data */
			for (ifq=0,itt=nf-1;ifq<nf;ifq++,itt--)
				freqv[ifq] = ffreq[jw][itt];

			memset((void *) (freqv+nf), 0, (nfft-nf)*sizeof(complex));
			memset((void *) todataw, 0, nfft*sizeof(float));

			/* FFT back from f to t and scaling */
			pfacr(-1, nfft, freqv, todataw);
			for (it=0;it<MIN(nsp,nfft);it++)
				ttodataw[it]=(it%2 ? -todataw[it]/nfft : todataw[it]/nfft);

			/*loop along time */
			if (ntw>1)
			{
				/* first portion of time window */
				if (iw>0)
					for (it=0;it<nstaper;it++)
						tdataout[jw][it+iw*nspws-nstaper/2]+=
								ttodataw[it]*((float)(it)*dt/taper);
				else
					for (it=0;it<nstaper;it++)
						tdataout[jw][it]=ttodataw[it];

				/* intermediate portion of time window */
				if (iw>0)
					for (it=nstaper;it<nspw-nstaper;it++)
						tdataout[jw][it+iw*nspws-nstaper/2]=ttodataw[it];
				else
					for (it=nstaper;it<nspw-nstaper;it++)
						tdataout[jw][it]=ttodataw[it];

				/* last portion of time window */
				if (iw>0 && iw<ntw-1)
					for (it=nspw-nstaper;it<nspw;it++)
						tdataout[jw][it+iw*nspws-nstaper/2]+=
								ttodataw[it]*(1.-((float)(it-nspw+nstaper))*dt/taper);
				else if (iw==ntw-1)
					for (it=nspw-nstaper;it<nspw;it++)
						tdataout[jw][it+iw*nspws-nstaper/2]=ttodataw[it];
				else
					for (it=nspw-nstaper;it<nspw;it++)
						tdataout[jw][it]+=ttodataw[it]*(1.-((float)(it-nspw+nstaper))*dt/taper);
			}
			else
			{
				for (it=0;it<nsp;it++)
					tdataout[jw][it]=ttodataw[it];
			}
		}/*02. end of trace loop*/
	} /*00. end of time window*/

	free1float(tidataw);
	free1float(ttidataw);
	free1complex(ct);
	free2complex(fdata);
	free2complex(ffreq);
	free1complex(freqv);

	free1float(todataw);
	free1float(ttodataw);
}

void gaussianWindow(complex **cdata, complex *sum_freq, pos3ddata **windowpos, int nwindowing,
		int nf, float ampgauss, float sigma, float rnorm, float totgauss,
		float *distanceData, int ndistance, float multd)
{
	int i=0, j=0, k=0;
	float center_sx, center_sy;
	float curr_sx, curr_sy;
	float r, rn, gausval;
	double dsx, dsy;

	center_sx = windowpos[0][centerpos].sx;
	center_sy = windowpos[0][centerpos].sy;
	for(i=0; i<nwindowing; i++)
	{
		//get distance from all trace
		curr_sx = windowpos[0][i].sx;
		curr_sy = windowpos[0][i].sy;
		dsx = (double) (center_sx - curr_sx);
		dsy = (double) (center_sy - curr_sy);

		r = (float) (sqrt(pow(dsx, 2.0) + pow(dsy, 2.0))); 	//compute distance
		rn = r/rnorm;	//distance normalization
		if(usegauss){
			gausval = gaussian1d(ampgauss, rn,  sigma);			//compute gaussian
			gausval = gausval/totgauss;	//gauss normalization
		}
		else gausval = 1.0;

		for(k=0; k<ndistance; k++) 			//loop over ndistance
		{
			if(r<=distanceData[k])			//check distance location
			{
				for(j=0; j<nf; j++)	//fill data in time/frequency selected window
				{
					sum_freq[j].r += cdata[i][j].r*gausval;
					sum_freq[j].i += cdata[i][j].i*gausval;
				}
			}

		}

		if(multd!=1.0)
		{
			for(i=0; i<nf; i++)
			{
				sum_freq[j].r *= multd;
				sum_freq[j].i *= multd;
			}
		}
	}
}

float **readTraceInWindow(FILE *inpsegy, pos3ddata **windowpos)
{
	int i;
	int itrace;
	float **data;

	data = alloc2float(nsp, nwindowing);
	for(i=0; i<nwindowing; i++)
	{
		itrace = windowpos[0][i].trace;								//get trace location
		gotoTraceSegyPosition(inpsegy, itrace, nsegy); 				//go to curr_trace location
		readTraceSegy(inpsegy, endian, nsp, format, nsegy, &tr); 	//read current trace
		memcpy(data[i], tr.data, nsp*sizeof(float));
	}

	return(data);
}

void computeGaussVariable(pos3ddata **windowpos, float *frnorm, float *ftotgauss)
{
	int i;

	float rnorm;
	float totgauss;
	float rmax;
	float center_sx, center_sy;
	float curr_sx, curr_sy;
	float r, rn, gausval;
	double dsx, dsy;

	center_sx = windowpos[0][centerpos].sx;
	center_sy = windowpos[0][centerpos].sy;

	rnorm = 1.0;
	totgauss = 1.0;
	rmax = 0.0;
	if(bnormalize)	//normalisasi distance
	{
		for(i=0; i<nwindowing; i++){
			//get distance from all trace
			curr_sx = windowpos[0][i].sx;
			curr_sy = windowpos[0][i].sy;
			dsx = (double) (center_sx - curr_sx);
			dsy = (double) (center_sy - curr_sy);

			r = (float) (sqrt(pow(dsx, 2.0) + pow(dsy, 2.0))); 	//compute distance

			if(i==0)
				rmax = r;
			else
				if(r>rmax)	rmax = r;
		}

		rnorm = rmax/(2*PI);

		//compute sum of gauss value
		totgauss = 0.0;
		for(i=0; i<nwindowing; i++){
			//get distance from all trace
			curr_sx = windowpos[0][i].sx;
			curr_sy = windowpos[0][i].sy;
			dsx = (double) (center_sx - curr_sx);
			dsy = (double) (center_sy - curr_sy);

			r = (float) (sqrt(pow(dsx, 2.0) + pow(dsy, 2.0))); 	//compute distance
			rn = r/rnorm;
			gausval = gaussian1d(ampgauss, rn,  sigma);			//compute gaussian
			totgauss = totgauss + gausval;
		}
		if(totgauss==0)	totgauss = 1.0;
	}
	(*frnorm) = rnorm;
	(*ftotgauss) = totgauss;
}
