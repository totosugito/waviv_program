/*
 * wave_cadzow3d.c
 *
 *  Created on: May 24, 2012
 *      Author: toto
 */
#include "wave_3dlib.h"
#include "../src_lib/cadzowLib.h"
#include <time.h>
#include <stdbool.h>

char *sdoc[] = {
		"									",
		" WAVE_CADZOW3D.v1.0 = WAVE_CADZOW3D",
		" inp=                 (SEGY input)",
		" out=                 (SEGY output)",
		" boutpos=0            (0=create and read header data)",
		"                      (1= read header data)",
		" outpos=              (header filename)",
		" iline1=0             (minimum inline index position)",
		" iline2=0             (maximum inline index position)",
		" fbeg=0.5             (Minimum frequency)",
		" fend=60.0            (Maximum frequency)",
		" rank=1               (Cadzow rank) ",
		" idxinline=9          (inline index position in byte)",
		" idxcdp=21            (cdp index position in byte)",
		" spansize=7           (windowing data = [spansize*2]+1)",
		" spanfilter=5         (saving data every [spanfilter]",
		" fillzero=1           (fill zero data outside frequency)",
		" ",
		" verbose=1            (show debug)",
		" endian=0             (1=litte endian, 0=big endian)",
		" ",
		" Sample Command : ",
		" wave_cadzow3d inp=segyinput.sgy out=segyout.sgy boutpos=1 outpos=header.bin rank=1 ",
		"                   fbeg=0.5 fend=60.0 idxinline=9 idxcdp=21 spansize=7 spanfilter=5",

		NULL};

typedef struct cadzow3Header{
	int iline;	//inline number
	int cdp;	//[cdp trmin_pos trmax_pos]
	int trmin;
	int trmax;
	int ntrc;
}cadzow3Header;

cadzow3Header **cadzow3d_header_createfile(char *segyInput, char *cadzow3dHeaderInput,
		int endian, int idxinline, int idxcdp,
		int *ntrc, int *minIline, int *maxIline, int *totalIline,
		int *minCdp, int *maxCdp, int *totalCdp, int *trFold);
cadzow3Header **cadzow3d_header_loadfile(char *cadzow3dHeaderInput,
		int *ntrc, int *minIline, int *maxIline, int *totalIline,
		int *minCdp, int *maxCdp, int *totalCdp, int *trFold);

void errCdpNotEqual(int oldcdp, int oldcdp_tr, int newcdp, int newcdp_tr);
void readCadzow3Header(FILE *fid, FILE *tmpfod, int nsegy, int endian, int ntrc,
		int idxinline, int idxcdp,
		int *iminIline, int *imaxIline, int *totalIline, int *iNiline,
		int *iminCdp, int *imaxCdp, int *totalCdp, int *iNcdp, int *trFold);
char *createAutomaticFileName();
cadzow3Header **cadzow3Header_create(int row, int col);
void cadzow3Header_free(cadzow3Header **data);
void cadzow3_Process(char *segyInp, char *segyOut, int endian, int rank,
		cadzow3Header **header, int hrow, int hcol,
		int iline1, int iline2, int minCdp, int trFold,
		int spanfilter, int spansize, float ffbeg, float ffend, int fillzero);
void readSegyFileAtWindowingLocation(FILE *fid, FILE *fod, int nsp, int ntrc,
		int nsegy, int format, int endian, int trFold, float dt, int rank,
		cadzow3Header **header, int idxinline, int idxp1, int idxp2, int idxs1,int idxs2,
		int spanfilter, int spansize , float ffbeg, float ffend, int fillzero);
int main(int argc, char **argv)
{
	char *ccinp=NULL, *ccout=NULL, *coutpos=NULL;
	int endian, idxinline, idxcdp;
	int verbose;
	int boutpos;	//creating and reading binary header file
	int spansize, spanfilter;
	cadzow3Header **cadzow3head=NULL;
	int ntrc, rank;
	int minIline, maxIline, totalIline;
	int minCdp, maxCdp, totalCdp, trFold;
	int iline1, iline2;
	div_t divresult;
	time_t t1,t2;
	int fillzero;
	float ffbeg, ffend;

	/* hook up getpar to handle the parameters */
	initargs(argc,argv);
	requestdoc(1);

	MUSTGETPARSTRING("inp",  &ccinp);
	MUSTGETPARSTRING("out",  &ccout);

	if (!getparint("boutpos",&boutpos)) boutpos=0;	//0=create+reading, 1=only reading
	MUSTGETPARSTRING("outpos",  &coutpos);	//read binary header file

	if (!getparint("iline1",&iline1)) iline1=0;
	if (!getparint("iline2",&iline2)) iline2=0;

	if(!getparfloat("fbeg",&ffbeg))ffbeg=0.5;
	if(!getparfloat("fend",&ffend))ffend=60.0;
	if (!getparint("idxinline",&idxinline)) idxinline=9;
	if (!getparint("idxcdp",&idxcdp)) idxcdp=21;
	if (!getparint("spansize",&spansize)) spansize=7;
	if (!getparint("spanfilter",&spanfilter)) spanfilter=5;
	if (!getparint("rank",&rank)) rank=1;
	if (!getparint("fillzero",&fillzero)) fillzero=1;
	if (!getparint("verbose",&verbose)) verbose=1;
	if (!getparint("endian",&endian)) endian=0;


	t1 = time(NULL);

	//create header data
	if(boutpos==0)	cadzow3head = cadzow3d_header_createfile(ccinp, coutpos, endian, idxinline, idxcdp,
			&ntrc, &minIline, &maxIline, &totalIline, &minCdp, &maxCdp, &totalCdp, &trFold);
	else			cadzow3head = cadzow3d_header_loadfile(coutpos,
			&ntrc, &minIline, &maxIline, &totalIline, &minCdp, &maxCdp, &totalCdp, &trFold);

	//process cadzow 3d
	if(iline1<0)	iline1=0;
	else if(iline1>totalIline)	iline1 = totalIline-1;
	if(iline2<=0)	iline2=totalIline-1;
	else if(iline2>totalIline)	iline2 = totalIline-1;

	if(iline1>iline2)
		err("iline1 > iline2");
	cadzow3_Process(ccinp, ccout, endian, rank, cadzow3head, totalIline, totalCdp,
			iline1, iline2, minCdp, trFold, spanfilter, spansize, ffbeg, ffend, fillzero);

	//free allocated memory
	cadzow3Header_free(cadzow3head);

	t2 = time(NULL);
	divresult = div (t2-t1, 60);
	fprintf (stderr, "Process time = %d min %d sec\n", divresult.quot, divresult.rem);

	return(1);
}

cadzow3Header **cadzow3d_header_createfile(char *segyInput, char *cadzow3dHeaderInput,
		int endian, int idxinline, int idxcdp,
		int *ntrc, int *minIline, int *maxIline, int *totalIline,
		int *minCdp, int *maxCdp, int *totalCdp, int *trFold)
{
	FILE *fid=NULL;
	char cebcid[3200];
	bhed bh;
	int nsegy, intrc;
	FILE *tmpfod=NULL, *fod=NULL;
	cadzow3Header *tmpheader;
	cadzow3Header **cadzow3head=NULL;
	int iminIline, imaxIline, itotalIline, iminCdp, imaxCdp, itotalCdp;
	int iNcdp, iNiline, itrFold;
	int i, irow, icol;
	size_t result;

	fid = fopen(segyInput, "r");
	if(fid==NULL){
		err("cadzow3d_header_create : error opening file %s \n", segyInput);
		exit(0);
	}
	//read file 1 header information
	readEbcdicHeader(fid, cebcid); /* read ebcdic header */
	readBinaryHeader(fid, endian, &bh, &nsegy); /*read binary header */
	intrc = getNumberOfTraceSegyFile(fid, nsegy);

	//create temporary header filename
	char *strfile = createAutomaticFileName();
	tmpfod = fopen(strfile, "w+");
	if(!tmpfod) err("error opening file %s !", strfile);
	fprintf(stderr, "\nLoading SEG-Y ... \n\n");

	//scan segy and get header
	readCadzow3Header(fid, tmpfod, nsegy, endian, intrc, idxinline, idxcdp,
			&iminIline, &imaxIline, &itotalIline, &iNiline,
			&iminCdp, &imaxCdp, &itotalCdp, &iNcdp, &itrFold);

	//allocate and read header
	rewind(tmpfod);
	tmpheader = (cadzow3Header*) calloc(iNiline*iNcdp, sizeof(cadzow3Header));
	result = fread(tmpheader, sizeof(cadzow3Header), iNiline*iNcdp, tmpfod);

	//allocate header in array 2D
	cadzow3head = cadzow3Header_create(itotalIline, itotalCdp);
	for(i=0; i<iNcdp; i++)
	{
		if(i%10000==0)	fprintf(stderr, "Allocate CDP index [ %i / %i ] \n", i+1, iNcdp);
		irow = tmpheader[i].iline-iminIline;
		icol = tmpheader[i].cdp-iminCdp;
		cadzow3head[irow] [icol] = tmpheader[i];
	}

	//------------------------------------------------------------------
	// CREATE BINARY HEADER OUTPUT
	//------------------------------------------------------------------
	//saving header to file
	fod = fopen(cadzow3dHeaderInput, "w");
	if(!fod) err("error opening file %s !", cadzow3dHeaderInput);
	fwrite(&intrc, sizeof(int), 1, fod);	//saving total of trace
	fwrite(&iminIline, sizeof(int), 1, fod);	//saving minimum of inline
	fwrite(&imaxIline, sizeof(int), 1, fod);	//saving maximum of inline
	fwrite(&iNiline, sizeof(int), 1, fod);	//saving sum of inline
	fwrite(&itotalIline, sizeof(int), 1, fod);	//saving total of inline

	fwrite(&iminCdp, sizeof(int), 1, fod);	//saving minimum of cdp
	fwrite(&imaxCdp, sizeof(int), 1, fod);	//saving maximum of cdp
	fwrite(&iNcdp, sizeof(int), 1, fod);	//saving sum of cdp
	fwrite(&itotalCdp, sizeof(int), 1, fod);	//saving total of cdp
	fwrite(&itrFold, sizeof(int), 1, fod);	//total fold trace in cdp

	fprintf(stderr, "\n");
	for(i=0; i<itotalIline; i++)
	{
		if(i%100==0)	fprintf(stderr, "Saving Inline [ %i / %i ] \n", i+1, itotalIline);
		fwrite(cadzow3head[i], sizeof(cadzow3Header), itotalCdp, fod);
	}

	free(tmpheader);
	fclose(tmpfod);
	fclose(fod);
	fclose(fid);
	remove(strfile);	//remove temporary filename

	//------------------------------------------------------------------
	// SHOW LOG BINARY HEADER
	fprintf(stderr, "\n ---- LOG FILE ---- \n");
	fprintf(stderr, " Binary Header = %s \n", cadzow3dHeaderInput);
	fprintf(stderr, "          NTRC = %i \n", intrc);
	fprintf(stderr, "    MIN INLINE = %i \n", iminIline);
	fprintf(stderr, "    MAX INLINE = %i \n", imaxIline);
	fprintf(stderr, "INLINE IN SEGY = %i \n", iNiline);
	fprintf(stderr, "  TOTAL INLINE = %i \n", itotalIline);
	fprintf(stderr, "       MIN CDP = %i \n", iminCdp);
	fprintf(stderr, "       MAX CDP = %i \n", imaxCdp);
	fprintf(stderr, "   CDP IN SEGY = %i \n", iNcdp);
	fprintf(stderr, "     TOTAL CDP = %i \n", itotalCdp);
	fprintf(stderr, "    TRACE FOLD = %i \n\n", itrFold);
	//------------------------------------------------------------------

	(*ntrc) = intrc;
	(*minIline) = iminIline;
	(*maxIline) = imaxIline;
	(*totalIline) = itotalIline;
	(*minCdp) = iminCdp;
	(*maxCdp) = imaxCdp;
	(*totalCdp) = itotalCdp;
	(*trFold) = itrFold;
	return(cadzow3head);
}

cadzow3Header **cadzow3d_header_loadfile(char *cadzow3dHeaderInput,
				int *ntrc, int *minIline, int *maxIline, int *totalIline,
				int *minCdp, int *maxCdp, int *totalCdp, int *trFold)
{
	FILE *fid=NULL;
	cadzow3Header **cadzow3head=NULL;
	int iminIline, imaxIline, itotalIline, iminCdp, imaxCdp, itotalCdp;
	int iNiline, iNcdp, i, itrFold;
	int intrc, result;
	//------------------------------------------------------------------
	// CREATE BINARY HEADER OUTPUT
	//------------------------------------------------------------------
	//saving header to file
	fid = fopen(cadzow3dHeaderInput, "r");
	if(!fid) err("error opening file %s !", cadzow3dHeaderInput);
	result = fread(&intrc, sizeof(int), 1, fid);	//reading total of trace
	result = fread(&iminIline, sizeof(int), 1, fid);	//reading minimum of inline
	result = fread(&imaxIline, sizeof(int), 1, fid);	//reading maximum of inline
	result = fread(&iNiline, sizeof(int), 1, fid);	//reading sum of inline
	result = fread(&itotalIline, sizeof(int), 1, fid);	//reading total of inline

	result = fread(&iminCdp, sizeof(int), 1, fid);	//reading minimum of cdp
	result = fread(&imaxCdp, sizeof(int), 1, fid);	//reading maximum of cdp
	result = fread(&iNcdp, sizeof(int), 1, fid);	//reading sum of cdp
	result = fread(&itotalCdp, sizeof(int), 1, fid);	//reading total of cdp
	result = fread(&itrFold, sizeof(int), 1, fid);	//total fold in cdp

	cadzow3head = cadzow3Header_create(itotalIline, itotalCdp);		//allocate header in array 2D

	fprintf(stderr, "\n");
	for(i=0; i<itotalIline; i++)
	{
		if(i%100==0)	fprintf(stderr, "Reading Inline [ %i / %i ] \n", i+1, itotalIline);
		result= fread(cadzow3head[i], sizeof(cadzow3Header), itotalCdp, fid);
	}
	fclose(fid);

	//------------------------------------------------------------------
	// SHOW LOG BINARY HEADER
	fprintf(stderr, "\n ---- LOG FILE ---- \n");
	fprintf(stderr, " Binary Header = %s \n", cadzow3dHeaderInput);
	fprintf(stderr, "          NTRC = %i \n", intrc);
	fprintf(stderr, "    MIN INLINE = %i \n", iminIline);
	fprintf(stderr, "    MAX INLINE = %i \n", imaxIline);
	fprintf(stderr, "INLINE IN SEGY = %i \n", iNiline);
	fprintf(stderr, "  TOTAL INLINE = %i \n", itotalIline);
	fprintf(stderr, "       MIN CDP = %i \n", iminCdp);
	fprintf(stderr, "       MAX CDP = %i \n", imaxCdp);
	fprintf(stderr, "   CDP IN SEGY = %i \n", iNcdp);
	fprintf(stderr, "     TOTAL CDP = %i \n", itotalCdp);
	fprintf(stderr, "    TRACE FOLD = %i \n\n", itrFold);
	//------------------------------------------------------------------

	(*ntrc) = intrc;
	(*minIline) = iminIline;
	(*maxIline) = imaxIline;
	(*totalIline) = itotalIline;
	(*minCdp) = iminCdp;
	(*maxCdp) = imaxCdp;
	(*totalCdp) = itotalCdp;
	(*trFold) = itrFold;
	return(cadzow3head);
}

/*----------------------------------------------------------------
 * READ SEGY FILE AND GET INLINE+CDP HEADER INFORMATION
 * fid : segy input file identifier
 * tmpfod : temporary output filename
 * nsegy : total number of byte in trace (trace+header trace)
 * endian : machine format
 * ntrc : total number of trace
 * idxinline : index of byte for inline position
 * idxcdp : index of byte for cdp position
 ----------------------------------------------------------------*/
void readCadzow3Header(FILE *fid, FILE *tmpfod, int nsegy, int endian, int ntrc,
		int idxinline, int idxcdp,
		int *iminIline, int *imaxIline, int *totalIline, int *iNiline,
		int *iminCdp, int *imaxCdp, int *totalCdp, int *iNcdp, int *trFold)
{
	unsigned char *buffer;
	int minInline, maxInline;
	int minCdp, maxCdp;
	int tmpInline, currInline;
	int tmpCdp, currCdp, firstcdp;
	int trcdp, maxtrcdp;
	int ncdp, ninline;
	bool bget1cdp;
	int result, i;
	cadzow3Header tmpheader;

	buffer = (unsigned char*) calloc(nsegy, sizeof(unsigned char));

	//read the first of trace
	result = fread(buffer, sizeof(unsigned char), nsegy, fid);
	currInline = uchar2int(buffer, idxinline-1, 4, endian);	//read inline
	currCdp = uchar2int(buffer, idxcdp-1, 4, endian); 		//read cdp
	firstcdp = currCdp;

	//initialization variable
	ninline = 1;
	ncdp = 1;
	trcdp = 1;
	maxtrcdp = trcdp;
	bget1cdp = false;

	//set inline and cdp (minimum, maximum)
	minCdp = firstcdp;
	maxCdp = minCdp;
	minInline = currInline;
	maxInline = minInline;

	//setting data header
	tmpheader.iline = currInline;
	tmpheader.cdp = currCdp;
	tmpheader.trmin = 0;

	for(i=1; i<ntrc; i++)
	{
		if(i%10000==1)
			fprintf(stderr, "Scan Trace Header [ %i / %i ] \n", i, ntrc);
		result = fread(buffer, sizeof(unsigned char), nsegy, fid);
		tmpInline = uchar2int(buffer, idxinline-1, 4, endian);	//read inline
		tmpCdp = uchar2int(buffer, idxcdp-1, 4, endian); 		//read cdp

		if(tmpInline!=currInline)
		{
			//if(verbose) printf("\nCurrent Inline = %i \n", currInline);
			currInline = tmpInline;
			if(minInline>currInline) minInline=currInline;	//set minimum inline
			if(maxInline<currInline) maxInline = currInline;	//set maximum inline
			ninline++;
		}

		if(tmpCdp!=currCdp)
		{
			//if(verbose) printf("Current CDP = %i [%i] \n", currCdp, trcdp);

			if(bget1cdp==false)	{ //create tag if get the first cdp
				bget1cdp = true;
				maxtrcdp = trcdp;
			}
			else
			{
				if(maxtrcdp != trcdp)
					errCdpNotEqual(firstcdp, maxtrcdp, currCdp, trcdp);
			}

			tmpheader.trmax = i-1;
			tmpheader.ntrc = trcdp;
//			printf("inline=%i cdp=%i \n", tmpheader.iline, tmpheader.cdp);
			fwrite(&tmpheader, sizeof(cadzow3Header), 1, tmpfod);

			trcdp = 0;
			currCdp = tmpCdp;
			if(minCdp>currCdp) minCdp=currCdp;	//set minimum cdp
			if(maxCdp<currCdp) maxCdp = currCdp;	//set maximum cdp

			//setting data header
			tmpheader.iline = currInline;
			tmpheader.cdp = currCdp;
			tmpheader.trmin = i;

			ncdp++;
		}
		trcdp++;
	}
	//check number of trace in the last of cdp
	if(maxtrcdp != trcdp)
		errCdpNotEqual(firstcdp, maxtrcdp, currCdp, trcdp);

	//save the last of cdp
	tmpheader.trmax = i-1;
	tmpheader.ntrc = trcdp;
	fwrite(&tmpheader, sizeof(cadzow3Header), 1, tmpfod);

	(*iminIline) = minInline;
	(*imaxIline) = maxInline;
	(*totalIline) = maxInline-minInline+1;
	(*iNiline) = ninline;
	(*iminCdp) = minCdp;
	(*imaxCdp) = maxCdp;
	(*totalCdp) = maxCdp-minCdp+1;
	(*iNcdp) = ncdp;
	(*trFold) = maxtrcdp;
	printf("\n");

	free(buffer);
}

void errCdpNotEqual(int oldcdp, int oldcdp_tr, int newcdp, int newcdp_tr)
{
	fprintf(stderr, "\nThe number of trace in cdp is not equal \n");
	fprintf(stderr, "Maximum Number of Trace in First CDP(%i) = %i \n",
			oldcdp, oldcdp_tr);
	fprintf(stderr, "Maximum Number of Trace in Current CDP(%i) = %i \n",
			newcdp, newcdp_tr);
	exit(0);
}

char *createAutomaticFileName()
{
	  time_t rawtime;
	  struct tm * timeinfo;
	  static char strFileName[100];

	  time ( &rawtime );
	  timeinfo = localtime ( &rawtime );

//	  printf("YY=%i  MM=%i DD=%i HH=%i MM=%i SS=%i \n",
//			  timeinfo->tm_year, timeinfo->tm_mon, timeinfo->tm_mday,
//			  timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);

	  sprintf(strFileName,"/tmp/cad3_%i%i%i_%i%i%i",
			  timeinfo->tm_year, timeinfo->tm_mon, timeinfo->tm_mday,
			  timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);

	  return(strFileName);
}

cadzow3Header **cadzow3Header_create(int row, int col)
{
	int i;
	cadzow3Header **data=NULL;

	/* allocate pointers to rows */
	data = (cadzow3Header **) malloc((row*sizeof(cadzow3Header*)));
	if (!data) {
		fprintf(stderr, "cadzow3Header_create : error allocation row");
		exit(0);
	}

	/* allocate rows and set pointers to them */
	data[0]=(cadzow3Header*) malloc((row*col)*sizeof(cadzow3Header));
	if (!data[0]) {
		fprintf(stderr, "cadzow3Header_create : error allocation column");
		exit(0);
	}

	for(i=1; i<row; i++)
		data[i]=data[i-1] + col;

	memset(data[0], 0, row*col*sizeof(cadzow3Header));

	/* return pointer to array of pointers to rows */
	return data;
}

void cadzow3Header_free(cadzow3Header **data)
{
	free (data[0]);
	free (data);
}

void cadzow3_Process(char *segyInp, char *segyOut, int endian, int rank,
		cadzow3Header **header, int hrow, int hcol,
		int iline1, int iline2, int minCdp, int trFold,
		int spanfilter, int spansize, float ffbeg, float ffend, int fillzero)
{
	int i, itr;
	int firstkey, lastkey;
	int jump, begdata, enddata;
	int ibeg, iend, ff, fl;
	int idx1, idx2;

	char cebcdic[3200];
	FILE *fid=NULL, *fod=NULL;
	int format, nsp, nsegy, ntrc;
	float dt;

	fid = fopen (segyInp,"r");
	if (fid==NULL) err ("Error opening file : %s\n", segyInp);

	fod = fopen (segyOut,"w");
	if (fod==NULL) err ("Error opening file : %s\n", segyOut);

	//read file 1 header information
	readEbcdicHeader(fid, cebcdic); /* read ebcdic header */
	readBinaryHeader(fid, endian, &bh, &nsegy); /*read binary header */
	ntrc = getNumberOfTraceSegyFile(fid, nsegy);
	nsp = bh.hns;
	format = bh.format;
	dt = (float) (bh.hdt/1e+6);

	/*write output header */
	bh.format = 1;
	writeEbcdicHeader(fod, cebcdic);
	writeBinaryHeader(fod, endian, &bh);

	firstkey = 0;
	lastkey = hcol;

	// ----------------------------- CREATE WINDOWING DATA --------------------------

	for(itr=iline1; itr<=iline2; itr++)	//loop over inline
	{
		//read the first windowing data
		jump = (spanfilter*2) + 1;
		begdata = firstkey + spansize;
		enddata = lastkey - spansize;
		ff = begdata + spansize;
		fl = begdata - spanfilter - 1;
		idx1 = firstkey;	idx2 = fl;

		fprintf(stderr, "INLINE [%i / %i], CDP [(%i - %i) / %i] -> READ [%i - %i], SAVE [%i - %i ]\n",
				itr+1, iline2,
				idx1, idx2, hcol,
				minCdp+firstkey, minCdp+ff,
				minCdp+idx1, minCdp+idx2);

		//PROCESS CADZOW3D
		readSegyFileAtWindowingLocation(fid, fod, nsp, ntrc, nsegy, format, endian,
				trFold, dt, rank, header, itr, firstkey, ff, idx1, idx2, spanfilter, spansize, ffbeg, ffend, fillzero);

		//loop over cdp
		for ( i=begdata; i<enddata; i=i+jump )
		{
			ibeg = i-spansize;
			iend = i+spansize;
			ff = i-spanfilter;
			fl = i+spanfilter;
			idx1 = ff;	idx2 = fl;

			fprintf(stderr, "INLINE [%i / %i], CDP [(%i - %i) / %i] -> READ [%i - %i], SAVE [%i - %i ]\n",
					itr+1, iline2, idx1, idx2, hcol, minCdp+ibeg, minCdp+iend, minCdp+idx1, minCdp+idx2);

			//PROCESS CADZOW3D
			readSegyFileAtWindowingLocation(fid, fod, nsp, ntrc, nsegy, format, endian,
					trFold, dt, rank, header, itr, ibeg, iend, idx1, idx2, spanfilter, spansize, ffbeg, ffend, fillzero);
		}

		//read the last windowing data
		ff = i-spansize;
		fl = fl + 1;
		idx1 = fl;	idx2 = lastkey-1;

		fprintf(stderr, "INLINE [%i / %i], CDP [(%i - %i) / %i] -> READ [%i - %i], SAVE [%i - %i ]\n",
				itr+1, iline2, idx1, idx2, hcol, minCdp+ff, minCdp+idx2, minCdp+idx1, minCdp+idx2);

		//PROCESS CADZOW3D
		readSegyFileAtWindowingLocation(fid, fod, nsp, ntrc, nsegy, format, endian,
				trFold, dt, rank, header, itr, ff, idx2, idx1, idx2, spanfilter, spansize, ffbeg, ffend, fillzero);
		//------------------------------- SAVING SEGY DATA --------------------------------


	}
	fprintf(stderr, "\n");

	fclose(fid);
	fclose(fod);
}

void readSegyFileAtWindowingLocation(FILE *fid, FILE *fod, int nsp, int ntrc,
		int nsegy, int format, int endian, int trFold, float dt, int rank,
		cadzow3Header **header, int idxinline, int idxp1, int idxp2, int idxs1, int idxs2,
		int spanfilter, int spansize , float ffbeg, float ffend, int fillzero)
{
	int i, j, k;
	float **data=NULL;
	int ncdp, totntrc;
	int trloc, tridx, cdpidx;
	int bottrace;
	int cdp_spansize;


	//allocate data
	cdp_spansize = idxp2-idxp1+1;
	totntrc = cdp_spansize*trFold;
	//printf("cdp_spansize=%i idxp1=%i idxp2=%i nsp=%i totntrc=%i \n", cdp_spansize, idxp1, idxp2, nsp, totntrc);
	data = alloc2float(nsp, totntrc);
	memset(data[0], 0, totntrc*nsp*sizeof(float));

	//read spansize cdp trace
	ncdp = 0;
	for(i=0; i<cdp_spansize; i++)	//loop over cdp
	{
		cdpidx = i+idxp1;

		if(header[idxinline][cdpidx].ntrc <= 0)
			continue;

		trloc = header[idxinline][cdpidx].trmin;	//trace position in SEGY
		gotoTraceSegyPosition(fid, trloc, nsegy);	//go to at first trace in current cdp
		tridx = i*trFold;	//trace index in data
		//printf("%i. GET trpos=%i , idx=%i \n", i+1, trloc, tridx+trFold);
		for(j=0; j<trFold; j++)	//loop over trace fold in cdp
		{
			readTraceSegy(fid, endian, nsp, format, nsegy, &tr);	//read trace
			for(k=0; k<nsp; k++)//fill trace to data
				data[j+tridx][k] = tr.data[k];
		}
		ncdp++;
	}

	if(ncdp>=3)
		runCadzow2D(data, totntrc, nsp, dt, rank, cdp_spansize, trFold, ffbeg, ffend, fillzero);

	//saving data
	bottrace = (idxs1-idxp1)*trFold;
	for(i=0; i<(idxs2-idxs1+1); i++)
	{
		cdpidx = i+idxs1;

		if(header[idxinline][cdpidx].ntrc <= 0)
			continue;

		trloc = header[idxinline][cdpidx].trmin;	//trace position in SEGY
		gotoTraceSegyPosition(fid, trloc, nsegy);	//go to at first trace in current cdp
		tridx = bottrace + (i*trFold);	//trace index in data

		//printf("%i. SET trpos=%i , idx=%i \n", i+1, trloc, tridx+trFold);
		for(j=0; j<trFold; j++)	//loop over trace fold in cdp
		{
			readTraceSegy(fid, endian, nsp, format, nsegy, &tr);	//read trace
			for(k=0; k<nsp; k++)//fill trace
				tr.data[k] = data[j+tridx][k];
			writeTraceSegy(fod, endian, &tr, nsegy, nsp);			//write trace
		}
	}

	free2float(data);
}

