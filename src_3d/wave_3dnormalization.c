/*
 * wave_3dnormalitation.c
 *
 *  Created on: Jan 16, 2012
 *      Author: toto
 */

#include "segy_lib.h"
#include <sys/types.h>
#include <stdbool.h>
#include <time.h>

char *sdoc[] = {
		"									",
		" WAVE_3DNORMALIZATION = NORMALIZATION TWO SU FILE",
		"",
		" inp=          (input segy file)",
		" out=          (output su file)",
		NULL};

void computeMinMaxData(segy trace, float oldfmin, float oldfmax, float *newfmin, float *newfmax);
int main (int argc, char **argv)
{
	char *ccinp1=NULL, *ccinp2=NULL, *ccout=NULL;
	FILE *finp1=NULL, *finp2=NULL, *fout=NULL;
	float mmin1, mmax1, mmin2, mmax2;
	float tmpmax, tmpmin;
	float delta1, delta2;
	float tmp1, tmp2;
	int itr, ndiv, i;
	segy tr1, tr2;

	/* hook up getpar to handle the parameters */
	initargs(argc,argv);
	requestdoc(1);

	MUSTGETPARSTRING("inp1",  &ccinp1);
	MUSTGETPARSTRING("inp2",  &ccinp2);
	MUSTGETPARSTRING("out",  &ccout);
	if (!getparint("ndiv",&ndiv)) 	ndiv = 100;

	/*read su input I*/
	finp1 = fopen (ccinp1,"r");
	if (finp1==NULL) err ("Error opening file : %s\n", ccinp1);

	/*read su input II*/
	finp2 = fopen (ccinp2,"r");
	if (finp2==NULL) err ("Error opening file : %s\n", ccinp2);

	//read first trace
	fgettr(finp1, &tr1);
	fgettr(finp2, &tr2);

	mmin1 = tr1.data[0];
	mmax1 = tr1.data[0];
	mmin2 = tr2.data[0];
	mmax2 = tr2.data[0];

	/*set pointer t beginning of file*/
	fseek(finp1, 0, SEEK_SET);
	fseek(finp2, 0, SEEK_SET);

	itr = 0;
	while(fgettr(finp1, &tr1))
	{
		/*get minimum and maximum file I*/
		computeMinMaxData(tr1, mmin1, mmax1, &tmpmin, &tmpmax);
		mmin1 = tmpmin;
		mmax1 = tmpmax;

		/*get minimum and maximum file II*/
		fgettr(finp2, &tr2);
		computeMinMaxData(tr2, mmin2, mmax2, &tmpmin, &tmpmax);
		mmin2 = tmpmin;
		mmax2 = tmpmax;
		itr = itr+1;
	}

	/*split size data by ndiv variable*/
	delta1 = (mmax1-mmin1)/ndiv;
	delta2 = (mmax2-mmin2)/ndiv;

	/*loop data to get new su data normalization*/
	/*create output file*/
	fout = fopen (ccout,"w");
	if (fout==NULL) err ("Error opening file : %s\n", ccout);

	fseek(finp2, 0, SEEK_SET);
	while(fgettr(finp2, &tr2))
	{
		for(i=0; i<tr2.ns; i++)
		{
			tmp1 = (tr2.data[i]-mmin2)/delta2;
			tmp2 = mmin1 + (tmp1*delta1);

			tr2.data[i] = tmp2;
		}
		fputtr(fout, &tr2);
	}

	fprintf(stderr,"itr=%i \n", itr);
	fprintf(stderr, "FIle I min=%f max=%f \n", mmin1, mmax1);
	fprintf(stderr, "FIle II min=%f max=%f \n", mmin2, mmax2);
	fclose(finp1);
	fclose(finp2);
	fclose(fout);

	return(1);
}

void computeMinMaxData(segy trace, float oldfmin, float oldfmax, float *newfmin, float *newfmax)
{
	int itr;
	float ftmpmax, ftmpmin;

	ftmpmax = oldfmax;
	ftmpmin = oldfmin;
	for (itr=0; itr<trace.ns; itr++)
	{
		if(ftmpmax<trace.data[itr])	ftmpmax = trace.data[itr];	//get maximum data

		if(ftmpmin>trace.data[itr])	ftmpmin = trace.data[itr];	//get minimum data
	}

	(*newfmin) = ftmpmin;
	(*newfmax) = ftmpmax;

}
