/*
 * wave_writeneighborfile.c
 *
 *  Created on: Feb 7, 2011
 *      Author: toto
 */

#include "segy_lib.h"
#include "wave_3dlib.h"
#include <sys/types.h>
#include <stdbool.h>
#include <time.h>

char *sdoc[] = {
		"									",
		" WAVE_3DTIMESLICE.v8 = EXTRACT TIME SLICE FROM 3D SEGY",
		"",
		" inp=          (input segy file)",
		" out=          (output su file)",
		" posfile=      (input inline xline position)",
		" tpos1=        (extract at time position from ..",
		" tpos2=        (extract at time position until ..",
		"",
		" verbose=1     (show info)",
		" vblock=10000  (show verbose every vblock traces)",
		" endian=0      (0=little endian)",
		" inline=9      (inline location in byte, 9=same as ffid)",
		" xline=17      (xline location in byte, 17=as sp/ep)",
		" transpose=0   (0=xaxis(xline),yaxis(inline), 1=xaxis(inline),yaxis(xline))",
		"",
		" Sample Command :",
		" posfile=      use  wave_writeneighborfile.v2",
		" ",
		" wave_3dtimeslice inp= out= posfile= ",
		"",
		" Output is a SU file with Header Information:",
		" fldr = time slice location",
		" cdp = xline position (transpose=0), inline position(transpose=1)",
		NULL};


int main (int argc, char **argv)
{
	int verbose;
	char *ccinp, *ccout, *cposfile;
	int iline, xline;
	int endian, vblock, idxvblock;
	FILE *FFIN=NULL, *FFOD=NULL;
	pos3ddata **data=NULL;
	float **timeslice=NULL;
	int i,j=0, k, xpos, ypos, xxpos, idxpos;
	char cebcdic[3200];
	segy trace;
	int widthInline, widthXline, xwidth;
	int min_inline, max_inline, min_xline, max_xline;
	int curr_inline, curr_xline;
	int nsegy, ntrc, nsp, format;
	int tpos1, tpos2, dtime;
	int transpose;
	div_t divresult;
	time_t t1,t2;

	/* hook up getpar to handle the parameters */
	initargs(argc,argv);
	requestdoc(1);

	MUSTGETPARSTRING("inp",  &ccinp);
	MUSTGETPARSTRING("out",  &ccout);
	MUSTGETPARSTRING("posfile",  &cposfile);
	if (!getparint("tpos1",&tpos1)) err("tpos1=??");
	if (!getparint("tpos2",&tpos2)) err("tpos2=??");

	if (!getparint("verbose",&verbose)) 	verbose = 1;
	if (!getparint("endian",&endian)) 	endian = 0;
	if (!getparint("vblock",&vblock)) 	vblock = 10000;
	if (!getparint("inline",&iline)) 	iline = 9;
	if (!getparint("xline",&xline)) 	xline = 17;

	if (!getparint("transpose",&transpose)) 	transpose = 0;
	/* start process */
	t1 = time(NULL);

	//read pos xline and xline data
	data = pos3ddata_read(cposfile, &widthInline, &widthXline,
			&min_inline, &max_inline, &min_xline, &max_xline);

	/*read segy input */
	FFIN = fopen (ccinp,"r");
	if (FFIN==NULL) err ("Error opening file 1 : %s\n", ccinp);
	readEbcdicHeader(FFIN, cebcdic); /* read ebcdic header */
	readBinaryHeader(FFIN, endian, &bh, &nsegy); /*read binary header */
	ntrc = getNumberOfTraceSegyFile(FFIN, nsegy);
	nsp = bh.hns;
	format = bh.format;

	if (verbose)
	{
		printf("\n");
		printf("input SEGY           = %s \n", ccinp);
		printf("input Posisition     = %s \n", cposfile);
		printf("\n");
		printf("nsp                  = %i \n", nsp);
		printf("format               = %i \n", format);
		printf("nsegy                = %i byte\n", nsegy);
		printf("inline byte location = %i \n", iline);
		printf("xline byte location  = %i \n", xline);
		printf("\n");
		printf("min inline           = %i \n", min_inline);
		printf("max inline           = %i \n", max_inline);
		printf("min xline            = %i \n", min_xline);
		printf("max xline            = %i \n", max_xline);
		printf("inline width         = %i \n", widthInline);
		printf("xline  width         = %i \n\n", widthXline);
	}

	dtime = tpos2-tpos1+1;
	xwidth = widthXline*dtime;
	timeslice = alloc2float(xwidth, widthInline);
	if(!timeslice)
		err("can not allocate data, tpos1 and tpos2 too big!");

	memset(timeslice[0], '\0', xwidth*widthInline*sizeof(float));

	idxvblock = 1;
	for(i=0; i<ntrc; i++) //loop over trace
	{
		if(idxvblock*vblock==i && verbose==1)
		{
			printf("Read Trace %i / %i \n", i, ntrc);
			idxvblock++;
		}

		curr_inline = computeIntHeaderAtByte(FFIN, iline, 4, endian, 0); //get current inline header
		curr_xline = computeIntHeaderAtByte(FFIN, xline, 4, endian, 0);  //get current xline header
		readTraceSegy(FFIN, endian, nsp, format, nsegy, &tr);

		xpos = curr_xline - min_xline;				//get x/column/xline position
		ypos = curr_inline - min_inline;			//get y/row/inline position

		idxpos = 0;
		for(j=tpos1; j<=tpos2; j++)					//loop over time
		{
			xxpos = idxpos*widthXline+xpos;
			timeslice[ypos][xxpos] = tr.data[j];	//save data at selected inline and xline
			idxpos++;
		}
	}

	/* write output to su file */
	FFOD = fopen (ccout,"w");
	if (FFOD==NULL) err ("Error opening file 1 : %s\n", ccout);
	if(transpose)	trace.ns = widthXline;
	else	trace.ns = widthInline;
	trace.dt = 1000;

	printf("\n");
	printf("Output SU            = %s \n", ccout);
	printf("Output ns            = %i \n", trace.ns);
	printf("Output dt            = %i \n", trace.dt);
	printf("\n");

	for(k=0; k<dtime; k++)				//loop over selected time
	{
		printf("Saving data in time %i / %i secon \n", k+tpos1, tpos2);
		trace.fldr = tpos1+k;			//time header

		if(transpose) //x-axis=inline, y-axis=xline
		{
			xxpos = k*widthXline;
			for(i=0; i<widthInline; i++)		//loop over inline
			{
				trace.cdp = min_inline+i;	//xline header header

				for(j=0; j<trace.ns; j++)	//loop over xline
					trace.data[j] = timeslice[i][xxpos+j];

				fputtr(FFOD, &trace);		//saving trace
			}
		}
		else	//x-axis=xline, y-axis=inline
		{
			for(i=0; i<widthXline; i++)		//loop over xline
			{
				xxpos = (k*widthXline) + i;
				trace.cdp = min_xline+i;	//xline header header
				for(j=0; j<trace.ns; j++)	//loop over inline
					trace.data[j] = timeslice[j][xxpos];

				fputtr(FFOD, &trace);		//saving trace
			}
		}
	}

	/* free and close file*/
	pos3ddata_free(data);
	free2float(timeslice);
	fclose(FFIN);
	fclose(FFOD);

	/*get time running of program */
	t2 = time(NULL);
	divresult = div (t2-t1, 60);
	fprintf (stderr, "Process time = %d min %d sec\n", divresult.quot, divresult.rem);

	return (1);
}

