/*
 * wave_writesortneighbor.c
 *
 *  Created on: Oct 22, 2012
 *      Author: toto
 */



#include "segy_lib.h"
#include "wave_3dlib.h"
#include <sys/types.h>
#include <stdbool.h>
#include <gsl/gsl_sort.h>

char *sdoc[] = {
		"",
		" WAVE_WRITESORTNEIGHBOR : Create header for wave_gatherfilter	",
		" ",
		" inp=                 (input seismic file)",
		" out=                 (output binary file position)",
		" log=                 (output log file)",
		"",
		" cdp=21               (CDP position)",
		" iline=9              (Inline position)" ,
		" xline=17             (Xline position)",
		" scalco=71            (scalco position)",
		" sx=73i               (source X position)",
		" sy=77i               (source Y position)",
		" offset=37i           (offset position)",
		"",
		" verbose=0            (show debug)",
		" endian=0             (0=little endian)",
		" vblock=10000         (verbose every vblock)",
		"",
		" i=integer format",
		" e=ieee floating format",
		" f=ibm floating point format",
		"",
		" Usage:",
		" wave_writesortneighbor inp= out= log= cdp= iline= ",
		"",
		NULL};

int verbose;
char *ccinp, *cclog, *coutpos;

FILE *fiseis=NULL; //input seismic file
FILE *fobin=NULL; //output binary sesmic position
FILE *folog=NULL; //output log file

int vblock, ftype;
int _endian;
int  _nsp, _nsegy, _ntrc, _scalco;

//group header
int cdp_pos, sx_pos, sy_pos, scalco_pos, offset_pos, iline_pos, xline_pos;
char sx_format, sy_format, offset_format;

//header sorting
int sort_pos;
char sort_format;

hdrinfo hdrlog;
hdrgauss *hdrgroup;
float *hdrsort;
size_t *hdrsort_index;

void parseHeaderFormat(char *str, int *cpos, char *cformat)
{
	sscanf(str,"%i%c", cpos, cformat);

	if(cformat[0]!='i' && cformat[0]!='f' && cformat[0]!='e')
		cformat[0] = 'i';
}

float getHeaderValue(unsigned char *data, int cpos, char cformat)
{
	float value=0.0;

	if(cformat=='i')
		value = (float) (uchar2int(data, cpos, 4, _endian));
	else if(cformat=='f')
		value = uchar2ibm(data, cpos, 4, _endian);
	else if(cformat=='e')
		value = uchar2float(data, cpos, 4, _endian);
	else
		err("format data must be i, e, or f");

	return(value);
}

void createGroupHeader(unsigned char *cdata, hdrgauss *hdr, int idx)
{
	hdr[idx].cdp = uchar2int(cdata, cdp_pos, 4, _endian);
	hdr[idx].iinline = uchar2int(cdata, iline_pos, 4, _endian);
	hdr[idx].ixline = uchar2int(cdata, xline_pos, 4, _endian);

	if(scalco_pos>0)
	{
		if(_scalco>0){
			hdr[idx].sx = getHeaderValue(cdata, sx_pos, sx_format) * _scalco;
			hdr[idx].sy = getHeaderValue(cdata, sy_pos, sy_format) * _scalco;
		}
		else {
			hdr[idx].sx = getHeaderValue(cdata, sx_pos, sx_format) / (-_scalco);
			hdr[idx].sy = getHeaderValue(cdata, sy_pos, sy_format) / (-_scalco);
		}
	}
	hdr[idx].offset = getHeaderValue(cdata, offset_pos, offset_format);
}

hdrinfo getHeaderInfo()
{
	hdrinfo info;
	int cdpmin;
	int cdpmax;
	int inlinemin;
	int inlinemax;
	int xlinemin;
	int xlinemax;

	float offsetmin;
	float offsetmax;
	int i;

	cdpmin = hdrgroup[0].cdp;
	cdpmax = cdpmin;
	inlinemin = hdrgroup[0].iinline;
	inlinemax = inlinemin;
	xlinemin = hdrgroup[0].ixline;
	xlinemax = xlinemin;
	offsetmin = hdrgroup[0].offset;
	offsetmax = offsetmin;

	for(i=1; i<_ntrc; i++)
	{
		if(hdrgroup[i].cdp < cdpmin) cdpmin = hdrgroup[i].cdp;
		if(hdrgroup[i].cdp > cdpmax) cdpmax = hdrgroup[i].cdp;

		if(hdrgroup[i].iinline < inlinemin) inlinemin = hdrgroup[i].iinline;
		if(hdrgroup[i].iinline > inlinemax) inlinemax = hdrgroup[i].iinline;

		if(hdrgroup[i].ixline < xlinemin) xlinemin = hdrgroup[i].ixline;
		if(hdrgroup[i].ixline > xlinemax) xlinemax = hdrgroup[i].ixline;

		if(hdrgroup[i].offset < offsetmin) offsetmin = hdrgroup[i].offset;
		if(hdrgroup[i].offset > offsetmax) offsetmax = hdrgroup[i].offset;
	}

	info.cdpmin = cdpmin;
	info.cdpmax = cdpmax;
	info.inlinemin = inlinemin;
	info.inlinemax = inlinemax;
	info.xlinemin = xlinemin;
	info.xlinemax = xlinemax;
	info.offsetmin = offsetmin;
	info.offsetmax = offsetmax;
	return(info);
}

int main (int argc, char **argv)
{
	char buffer[500];
	char ebcdic[3200];
	bhed bh;
	//	segy tr;
	unsigned char *cdata;
	int i, result;
	char *strsx, *strsy, *stroffset, *strsort;

	/* hook up getpar to handle the parameters */
	initargs(argc,argv);
	requestdoc(1);

	MUSTGETPARSTRING("inp",  &ccinp);
	MUSTGETPARSTRING("log",  &cclog);
	MUSTGETPARSTRING("out",  &coutpos);
	if (!getparint("verbose",&verbose)) 	verbose = 1;
	if (!getparint("endian",&_endian)) 	_endian = 0;

	if (!getparint("ftype",&ftype)) 	ftype = 1;
	if (!getparint("vblock",&vblock)) 	vblock = 10000;

	if (!getparint("cdp",&cdp_pos)) 	cdp_pos = 21;
	if (!getparint("iline",&iline_pos)) 	iline_pos = 9;
	if (!getparint("xline",&xline_pos)) 	xline_pos = 17;
	if (!getparint("scalco",&scalco_pos)) 	scalco_pos = 71;
	if (!getparstring("sx",&strsx)) 	strsx = "73i";
	if (!getparstring("sy",&strsy)) 	strsy = "77i";
	if (!getparstring("offset",&stroffset)) 	stroffset = "37i";

	if (!getparstring("sort",&strsort)) 	strsort = "37i";
	if(ftype<=0)
		err("input su file not yet implementated");

	//parse parameter
	parseHeaderFormat(strsx, &sx_pos, &sx_format);
	parseHeaderFormat(strsy, &sy_pos, &sy_format);
	parseHeaderFormat(stroffset, &offset_pos, &offset_format);
	parseHeaderFormat(strsort, &sort_pos, &sort_format);

	if(ftype==0)
	{

	}
	else
	{
		fiseis = fopen(ccinp, "rb");
		if(!fiseis)
			err("can not open %s", ccinp);

		readEbcdicHeader(fiseis, ebcdic);
		readBinaryHeader(fiseis, _endian, &bh, &_nsegy);
		_ntrc = getNumberOfTraceSegyFile(fiseis, _nsegy);
	}

	//------------------- sorting allocated variable ------------------
	cdata = (unsigned char*) calloc(_nsegy, sizeof(unsigned char));
	hdrgroup = (hdrgauss*) calloc(_ntrc, sizeof(hdrgauss));
	hdrsort = alloc1float(_ntrc);
	hdrsort_index = malloc(_ntrc*sizeof(size_t));
	//-----------------------------------------------------------------

	//read the first trace
	result = fread(cdata, 1, _nsegy, fiseis);
	if(result!=_nsegy) err("error reading trace 1 ");

	//compute scalco
	if(scalco_pos<0) _scalco = 1;
	else _scalco = (short) (uchar2int(cdata, scalco_pos, 2, _endian));

	//go to the first traces
	if(ftype) gotoTraceSegyPosition(fiseis, 0, _nsegy);
	else rewind(fiseis);

	for (i=0; i<_ntrc; i++)
	{
		if(i%vblock==0)
			fprintf(stderr, "Read Traces %i / %i \n", i, _ntrc);

		result = fread(cdata, 1, _nsegy, fiseis);
		if(result!=_nsegy) err("error reading trace %i ", i+i);

		//grouping header
		createGroupHeader(cdata, hdrgroup, i);
		hdrsort[i] = getHeaderValue(cdata, sort_pos, sort_format);
		hdrsort_index[i] = i;
	}
	fclose(fiseis);

	//sorting header
	gsl_sort_float_index(hdrsort_index, hdrsort, 1, _ntrc);

	//------------- create log file ------------------
	hdrlog = getHeaderInfo();
	folog = fopen(cclog, "w");
	if(!folog)
		err("can not open %s", cclog);
	sprintf(buffer,
			" Input File    : %s \n"
			" Total Ntrc    : %i \n"
			" CDP Min       : %i \n"
			" CDP Max       : %i \n"
			" Inline Min    : %i \n"
			" Inline Max    : %i \n"
			" Xline Min     : %i \n"
			" Xline Max     : %i \n"
			" Offset Min    : %5.3f \n"
			" Offset Max    : %5.3f \n\n"
			,
			ccinp, _ntrc, hdrlog.cdpmin, hdrlog.cdpmax,
			hdrlog.inlinemin, hdrlog.inlinemax,
			hdrlog.xlinemin, hdrlog.xlinemax,
			hdrlog.offsetmin, hdrlog.offsetmax);
	fwrite(buffer, sizeof(char), strlen(buffer), folog);

	memset(buffer, 0, strlen(buffer));
	sprintf(buffer, "No. \t CDP \t Inline \t Xline \t Offset \t Sx \t Sy \n");
	fwrite(buffer, 1, strlen(buffer), folog);
	for(i=0; i<_ntrc; i++){
		memset(buffer, 0, strlen(buffer));
		sprintf(buffer, "%i. \t %i \t %i \t %i %3.2f \t %3.2f \t %3.2f \n",
				i, hdrgroup[i].cdp, hdrgroup[i].iinline, hdrgroup[i].ixline,
				hdrgroup[i].offset, hdrgroup[i].sx, hdrgroup[i].sy);
		fwrite(buffer, 1, strlen(buffer), folog);
	}
	fclose(folog);

	//---------- saving header grouph in binary file --------------
	fobin = fopen(coutpos, "wb");
	if(!fiseis)
		err("can not open %s", coutpos);

	fwrite(&_ntrc, 4, 1, fobin); //save total number of trace
	fwrite(hdrsort_index, sizeof(size_t), _ntrc, fobin); //save trace position
	fwrite(hdrgroup, sizeof(hdrgauss), _ntrc, fobin); //save trace position
	fclose(fobin);


	free(cdata);
	free(hdrgroup);
	free1float(hdrsort);
	free(hdrsort_index);
	return (1);
}


