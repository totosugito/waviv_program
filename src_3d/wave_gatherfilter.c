/*
 * wave_gatherfilter.c
 *
 *  Created on: Oct 23, 2012
 *      Author: toto
 */


#include "segy_lib.h"
#include <sys/types.h>
#include <stdbool.h>
#include "wave_3dlib.h"
#include "fftLib.h"
#include <time.h>

char *sdoc[] = {
		"									",
		" WAVE_GATHERFILTER  : GAUSSIAN FILTER IN GATHER SEISMIC",
		" ",
		" inp=                 (input seismic file)",
		" out=                 (output seismic file)",
		" pos=                 (input binary file position)",
		" ampgauss=0.5         (gaussian amplitude)",
		" sigma=0.5            (gaussian standard deviation of the distribution)" ,
		" gaussnorm=1          (gaussian normalization)",
		" mult=1.0             (multiply data before ifft)",
		" distance=d1,d2,...   (maximum distance from center to neighbor)",
		" freq=f1,f2,f3,f4,    (process in frequency f1 to fn)",
		"",
		" tmin=0.0             (process only from tmin)",
		" tmax=0.0             (process only until tmax)",
		" verbose=0            (show debug)",
		" endian=0             (0=little endian)",
		" vblock=10            (verbose every vblock)",
		"",
		" if you use tmin and tmax, the program will ignore input parameter freq",
		" Usage:",
		" use wave_writesortneighbor to create position input file",
		" wave_gatherfilter inp= out= pos= ampgauss= gaussnorm= ",
		"",
		NULL};

FILE *fiseis=NULL; //input seismic file
FILE *foseis=NULL; //output seismic file
int vblock, _ftype;
int _endian, verbose;
int  _nsp, _format, _nsegy, _ntrc;
float _dt, _d1;
int _tmin, _tmax, _ntwindow;
int _nf, _nfft;
float _ampgauss, _sigma;
int _gauss_normalize;
float _max_radius;

float *_distance;
float _mult;
int *_timeloc;
int _ndistance;

hdrgauss *hdrgroup;
size_t *hindex;

void parseFrequencyInput(float *freqloc, int nfreq)
{
	int i;
	int it;

	//if user process in window data tmin to tmax
	if(_ntwindow != _nsp)
	{
		_ndistance = 1;
		_timeloc[0] = 0;
		_timeloc[1] = _nf;
		return;
	}

	/* check it time input value */
	if(nfreq==2) //process all data
	{
		if(freqloc[0]==0.0) 	_timeloc[0] = 0;
		else 	_timeloc[0] = (int) (round(freqloc[0]/_d1));

		if(freqloc[1]==0.0)  _timeloc[1] = _nf;
		else  _timeloc[1] = (int) (round(freqloc[1]/_d1));

		if(_timeloc[1]>_nf) _timeloc[1] = _nf;
		if(_timeloc[0]<0 || _timeloc[1]<0) err("freq value < 0");
		if(_timeloc[0]>=_timeloc[1]) err("freq[0] >= freq[1]");
	}
	else  //check selected time position
	{
		for(i=0; i<nfreq; i++)
		{
			it = (int) (round(freqloc[i]/_d1));
			if(it>_nf)
				_timeloc[i] = _nf;
			else
				_timeloc[i] = it;
		}
	}

	//check _timeloc value
	it = _timeloc[0];
	for(i=1; i<nfreq; i++)
	{
		if(it>_timeloc[i])
			err("please check your freq input. f1<f2<f3<...");
	}
}

void readBinaryPosHeader(char *posfile){
	FILE *fibin=NULL;
	size_t result;

	fibin = fopen(posfile, "r");
	if(!fibin) err("error opening file %s", posfile);

	result = fread(&_ntrc, 4, 1, fibin);
	hdrgroup = (hdrgauss*) calloc(_ntrc, sizeof(hdrgauss));
	hindex = malloc(_ntrc*sizeof(size_t));

	result = fread(hindex, sizeof(size_t), _ntrc, fibin);
	result = fread(hdrgroup, sizeof(hdrgauss), _ntrc, fibin);

	fclose(fibin);
}

void getTrace(float **data, int tridx, float *win_data)
{
	int i;

	for(i=0; i<_ntwindow; i++) {
		win_data[i] = data[tridx][_tmin+i];
	}
}

float computeRadius(float cent_sx, float cent_sy,
		float cur_sx, float cur_sy)
{
	double dsx, dsy;
	float r;

	dsx = (double) (cent_sx - cur_sx);
	dsy = (double) (cent_sy - cur_sy);

	r = (float) (sqrt(pow(dsx, 2.0) + pow(dsy, 2.0))); 	//compute distance
	return(r);
}

float computeGaussNormalization(int ntrc, int b_tr,
		float max_radius,
		float cent_sx, float cent_sy, float *rNorm)
{
	int i;
	float cur_sx, cur_sy;
	float r, rmax=0.0;
	float rnorm, totgauss, rn;
	float gausval;

	if(!_gauss_normalize){
		totgauss = 1.0;
		(*rNorm) = 1.0;
		return(totgauss);
	}

	for(i=0; i<ntrc; i++){
		cur_sx = hdrgroup[b_tr+i].sx;
		cur_sy = hdrgroup[b_tr+i].sy;

		r = computeRadius(cent_sx, cent_sy, cur_sx, cur_sy);
		if(r>max_radius) continue;

		if(r>rmax)	rmax = r;
	}

	rnorm = rmax/(2*PI);
	if(rnorm==0.0) rnorm = 1.0;

	//compute sum of gauss value
	totgauss = 0.0;
	for(i=0; i<ntrc; i++){
		cur_sx = hdrgroup[b_tr+i].sx;
		cur_sy = hdrgroup[b_tr+i].sy;

		r = computeRadius(cent_sx, cent_sy, cur_sx, cur_sy);
		if(r>max_radius) continue;

		rn = r/rnorm;
		gausval = gaussian1d(_ampgauss, rn,  _sigma);			//compute gaussian
		totgauss = totgauss + gausval;
	}
	if(totgauss==0)	totgauss = 1.0;

	(*rNorm) = rnorm;
	return(totgauss);
}
void runOffsetGatherFilter(int b_tr, int e_tr)
{
	int i, j, k, l;
	int ntrc, trpos, nneighbor;
	float **oridata, **data;
	char **header;

	float **dreal, **dimag;
	float **dsumreal, **dsumimag;
	float cent_sx, cent_sy;
	float cur_sx, cur_sy, r;

	//gaussian variable
	float totgauss;
	float rn, gausval ,rnorm;

	int itmin=0, itmax=_nf;

	//get number of trace over sorting header
	ntrc = e_tr - b_tr + 1;

	//printf("start gaussian\n");
	//------------ read seimic data ---------------
	oridata  = alloc2float(_nsp, _ntrc);
	data  = alloc2float(_nsp, _ntrc);
	header = alloc2char(SEGY_HDRBYTES, _ntrc);
	for(i=0; i<ntrc; i++)
	{
		//printf("read trace %i / %i \n", i, ntrc);
		trpos = hindex[b_tr+i];
		if(_ftype==0) {
			gotoTraceSuPosition(fiseis, trpos, _nsegy);

		}
		else {
			gotoTraceSegyPosition(fiseis, trpos, _nsegy);
			readSegy(fiseis, _endian, _nsp, _format, _nsegy, &tr, header[i], oridata[i]);
		}
	}

	memcpy(data[0], oridata[0], ntrc*_nsp*sizeof(float));

	//gaussian filter
	dreal = alloc2float(_nf, ntrc);
	dimag = alloc2float(_nf, ntrc);
	dsumreal = alloc2float(_nf, ntrc);
	dsumimag = alloc2float(_nf, ntrc);
	memset(dsumreal[0], 0, _nf*ntrc*sizeof(float));
	memset(dsumimag[0], 0, _nf*ntrc*sizeof(float));

	//fft all data in window
	fftProcess(data, _ntwindow, ntrc, _nf, _nfft, dreal, dimag);

	for(i=0; i<ntrc; i++) //loop over centre of trace
	{
		cent_sx = hdrgroup[b_tr+i].sx;
		cent_sy = hdrgroup[b_tr+i].sy;

		//printf("%i/%i %f3.0f %3.0f \n", i, ntrc, cent_sx, cent_sy);
		//compute gauss normalization
		totgauss = computeGaussNormalization(ntrc, b_tr,
				_max_radius, cent_sx, cent_sy, &rnorm);

		//printf("gausnorm=%f gaustot=%f \n", rnorm, totgauss);

		//fill the result sum data with the center of trace data
		for(k=0; k<_nf; k++) {
			dsumreal[i][k] = dreal[i][k];
			dsumimag[i][k] = dimag[i][k];
		}

		//compute data in boundary
		nneighbor = 1;
		for(j=0; j<ntrc; j++)
		{
			if(j==i) continue; //get the center trace, continue to the next trace

			cur_sx = hdrgroup[b_tr+i].sx;
			cur_sy = hdrgroup[b_tr+i].sy;
			r = computeRadius(cent_sx, cent_sy, cur_sx, cur_sy);
			rn = r/rnorm;	//radius normalization
			gausval = gaussian1d(_ampgauss, rn,  _sigma);	//compute gaussian
			gausval = gausval/totgauss;	//gauss normalization

			if(verbose && r<_max_radius)
				nneighbor++;

			//loop over distance
			for(k=0; k<_ndistance; k++) 			//loop over ndistance
			{
				if(r>_distance[k]) continue;			//check distance location

				itmin = _timeloc[k*2];		//compute t minimum
				itmax = _timeloc[k*2+1];	//compute t maximum
//				printf("%i %i \n", itmin, itmax);
				for(l=itmin; l<itmax; l++)	//fill data in time/frequency selected window
				{
					dsumreal[i][l] += dreal[j][l]*gausval;
					dsumimag[i][l] += dimag[j][l]*gausval;
				}
			}
		}
		if(verbose)
			printf("trace %i -> neighbor %i \n", i, nneighbor);

	}
	if(verbose) printf("\n");

	if(_mult!=1.0)
	{
		for(i=0; i<ntrc; i++)
		{
			for(j=0; j<_nf; j++){
				dsumreal[i][j] = dsumreal[i][j]*_mult;
				dsumimag[i][j] = dsumimag[i][j]*_mult;
			}
		}
	}

	//ifft all data in window
	ifftProcess(data, _ntwindow, ntrc, _nf, _nfft, dsumreal, dsumimag);

	if(_ntwindow!=_nsp)
	{
		for(i=0; i<ntrc; i++)
			for(j=_tmin; j<_tmax; j++)
				oridata[i][j] = data[i][j-_tmin];
	}
	else
		memcpy(oridata[0], data[0], ntrc*_nsp*sizeof(float));

	for(i=0; i<ntrc; i++) //write to output trace
	{
		trpos = hindex[b_tr+i];
		if(_ftype==0) {
			gotoTraceSuPosition(foseis, trpos, _nsegy);

		}
		else {
			gotoTraceSegyPosition(foseis, trpos, _nsegy);
			writeSegy(foseis, _endian, &tr, _nsegy, _nsp, header[i], oridata[i]);
		}
	}

	free2float(oridata);
	free2float(data);
	free2char(header);
	free2float(dreal);
	free2float(dimag);
	free2float(dsumreal);
	free2float(dsumimag);
}

int main(int argc, char **argv){
	char *ccinp=NULL, *ccout=NULL, *ccpos=NULL;
	char ebcdic[3200];
	int i;
	int nfreq;
	int b_tr, e_tr;
	float off1, off2;
	float *freqloc=NULL;
	float ftmin, ftmax;

	/* hook up getpar to handle the parameters */
	initargs(argc,argv);
	requestdoc(1);

	MUSTGETPARSTRING("inp",  &ccinp);
	MUSTGETPARSTRING("out",  &ccout);
	MUSTGETPARSTRING("pos",  &ccpos);

	if (!getparint("verbose",&verbose)) 	verbose = 1;
	if (!getparint("endian",&_endian)) 	_endian = 0;

	if (!getparint("ftype",&_ftype)) 	_ftype = 1;
	if (!getparint("vblock",&vblock)) 	vblock = 10;

	if (!getparfloat("tmin",&ftmin)) 	ftmin = 0.0;
	if (!getparfloat("tmax",&ftmax)) 	ftmax = 0.0;

	if (!getparfloat("ampgauss",&_ampgauss)) _ampgauss=0.5;
	if (!getparfloat("sigma",&_sigma)) _sigma = 0.5;
	if (!getparint("gaussnorm",&_gauss_normalize)) 	_gauss_normalize=1;
	if (!getparfloat("mult",&_mult)) 	_mult=1.0;

	//maximum distance from center to (sx,sy)neighbor
	if ((_ndistance = countparval("distance"))!=0)
	{
		_distance = ealloc1float(_ndistance);
		getparfloat("distance", _distance);
		_max_radius = _distance[0];
		for(i=1; i<_ndistance; i++)
			if(_max_radius<_distance[i])
				_max_radius = _distance[i];
	}
	else  err("distance=??");

	if ((nfreq = countparval("freq"))!=0)
	{
		if(nfreq != _ndistance*2)
			err("length(ifreq) must be 2x length(distance)");
		_timeloc = ealloc1int(nfreq);
		freqloc = ealloc1float(nfreq);
		getparfloat("freq", freqloc);
	}
	else err("freq=??");

	//printf("read binary position file\n");
	readBinaryPosHeader(ccpos);

	if(_ftype==0)
	{
		err("input su file not yet implementated");
	}
	else
	{
		fiseis = fopen(ccinp, "rb");
		if(!fiseis)
			err("can not open %s", ccinp);

		readEbcdicHeader(fiseis, ebcdic);
		readBinaryHeader(fiseis, _endian, &bh, &_nsegy);
		_ntrc = getNumberOfTraceSegyFile(fiseis, _nsegy);
		_format = bh.format;
		_nsp = bh.hns;
		_dt = (float) (bh.hdt/1000000.0);

		foseis = fopen(ccout, "wb");
		if(!fiseis)
			err("can not open %s", ccout);
		writeEbcdicHeader(foseis, ebcdic);
		writeBinaryHeader(foseis, _endian, &bh);
	}

	//compute time position
	if(ftmin<=0.0) _tmin = 0;
	else _tmin =(int) (ftmin/_dt);
	if(ftmax<=0.0) _tmax = _nsp;
	else _tmax =(int) (ftmax/_dt);
	if(_tmax>_nsp) _tmax = _nsp;
	if(_tmin > _tmax) err("tmin > tmax !");

	_ntwindow = _tmax - _tmin;
	getNfft(_ntwindow, _dt, &_nf, &_nfft, &_d1);
	parseFrequencyInput(freqloc, nfreq);

	//--------------- show debug program ------------------
	printf("\n");
	printf("Input Seismic     : %s \n", ccinp);
	printf("Output Seismic    : %s \n", ccout);
	printf("Position File     : %s \n\n", ccpos);
	printf("ampgauss          : %f \n", _ampgauss);
	printf("sigma             : %f \n", _sigma);
	printf("mult              : %f \n", _mult);
	printf("tmin              : %f \n", ftmin);
	printf("tmax              : %f \n", ftmax);
	printf("\n");
	for(i=0; i<_ndistance*2; i++)
		printf("Freq. Index[%i]   =  %i \n", i, _timeloc[i]);
	printf("\n");
	for(i=0; i<_ndistance; i++)
		printf("distance[%i]   =  %f \n", i, _distance[i]);
	printf("\n");
//	getchar();

	b_tr = 0;
	e_tr = 0;
	off1 = hdrgroup[hindex[0]].offset;
	off2 = hdrgroup[hindex[0]].offset;
	for(i=1; i<_ntrc; i++)
	{
		off2 = hdrgroup[hindex[i]].offset;
		if(off1!=off2)
		{
			e_tr = i-1;
			if(i%vblock==0)
				printf("Run offset %3.1f -> ntrc=%i \n", off1, e_tr-b_tr+1);

			//read data from b_tr to e_tr
			//run gaussian from b_tr to e_tr
			runOffsetGatherFilter(b_tr, e_tr);

			b_tr = i;
			e_tr = i;
			off1 = off2;
		}
	}

	//run the last trace
	e_tr = _ntrc;
	printf("Run offset %3.1f -> ntrc=%i \n", off1, e_tr-b_tr+1);
	runOffsetGatherFilter(b_tr, e_tr);

	fclose(fiseis);
	fclose(foseis);
	free(hdrgroup);
	free(hindex);
	free1int(_timeloc);
	free1float(freqloc);
	return(0);
}
