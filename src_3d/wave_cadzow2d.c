/*
 * wave_cadzow2d.c
 *
 *  Created on: Apr 23, 2012
 *      Author: toto
 *      modified from : ditya
 */


#include "segy_lib.h"
#include "cadzowLib.h"
#include "printLib.h"
#include <time.h>

char *sdoc[] = {
		"									",
		" WAVE_CADZOW2D = CADZOW2D",
		"",

		NULL};


float **readWindowingData(FILE *FFID, int nsegy, int format, int endian, int nsp,
		int tr1pos, int tr2pos);
void writeWindowingData(FILE *FFID, FILE *FFOD, int nsegy, int format, int endian,
		int tr1pos,
		float **data, int nsp, int ntrc);

int main (int argc, char **argv)
{
	char *csegyinp, *csegyout;
	char cebcdic[3200];
	int nsp, ntrc, format, nsegy;
	float dt;
	int rank;
	int endian;
	div_t divresult;
	time_t t1,t2;

	FILE *FFIN, *FFOD;
	int nsort1, nsort2;
	int fillzero;
	float **data;
	float ffbeg, ffend;

	/* hook up getpar to handle the parameters */
	initargs(argc,argv);
	requestdoc(1);

	MUSTGETPARSTRING("segyin",&csegyinp);
	MUSTGETPARSTRING("segyout",&csegyout);
	if(!getparint("rank",&rank))rank=3;
	if(!getparint("nsort1",&nsort1))nsort1=40;
	if(!getparint("nsort2",&nsort2))nsort2=40;
	if(!getparint("endian",&endian))endian=0;
	if(!getparint("fillzero",&fillzero))fillzero=1;
	if(!getparfloat("fbeg",&ffbeg))ffbeg=0.5;
	if(!getparfloat("fend",&ffend))ffend=60.0;

	t1 = time(NULL);
	/*read segy input */
	FFIN = fopen (csegyinp,"r");
	if (FFIN==NULL) err ("Error opening file : %s\n", csegyinp);
	readEbcdicHeader(FFIN, cebcdic); /* read ebcdic header */
	readBinaryHeader(FFIN, endian, &bh, &nsegy); /*read binary header */
	ntrc = getNumberOfTraceSegyFile(FFIN, nsegy);
	nsp = bh.hns;
	format = bh.format;
	dt = (float) (bh.hdt / 1e+6);

	/*write output header */
	FFOD = fopen (csegyout,"w");
	if (FFOD==NULL) err ("Error opening file : %s\n", csegyout);
	bh.format = 1;
	writeEbcdicHeader(FFOD, cebcdic);
	writeBinaryHeader(FFOD, endian, &bh);

	data = readWindowingData(FFIN, nsegy, format, endian, nsp, 0, ntrc);
	runCadzow2D(data, ntrc, nsp, dt, rank, nsort1, nsort2, ffbeg, ffend, fillzero);
	writeWindowingData(FFIN, FFOD, nsegy, format, endian, 0, data, nsp, ntrc);

	fclose(FFIN);
	fclose(FFOD);
	free2float(data);

	t2 = time(NULL);
	divresult = div (t2-t1, 60);
	fprintf (stderr, "Process time = %d min %d sec\n", divresult.quot, divresult.rem);
	return(1);
}

float **readWindowingData(FILE *FFID, int nsegy, int format, int endian, int nsp,
		int tr1pos, int tr2pos)
{
	int ntrc, i, j;
	float **wdata;

	ntrc = tr2pos-tr1pos+1;
	wdata = alloc2float(nsp, ntrc);

	gotoTraceSegyPosition(FFID, tr1pos, nsegy); //go to the beginning of selected trace
	for(i=0; i<ntrc; i++)
	{
		readTraceSegy(FFID, endian, nsp, format, nsegy, &tr);
		for(j=0; j<nsp; j++)
			wdata[i][j] = tr.data[j];
	}
	return(wdata);
}

void writeWindowingData(FILE *FFID, FILE *FFOD, int nsegy, int format, int endian,
		int tr1pos,
		float **data, int nsp, int ntrc)
{
	int i, j;

	gotoTraceSegyPosition(FFID, tr1pos, nsegy); //go to the beginning of selected trace
	for(i=0; i<ntrc; i++)
	{
		readTraceSegy(FFID, endian, nsp, format, nsegy, &tr);
		for(j=0; j<nsp; j++)
			tr.data[j] = data[i][j];
		writeTraceSegy(FFOD, endian, &tr, nsegy, nsp);
	}
}
