/*
 * wave_3dsegywindowing.c
 *
 *  Created on: Jan 20, 2012
 *      Author: toto
 */

#include "segy_lib.h"
#include "wave_3dlib.h"
#include <sys/types.h>
#include <stdbool.h>
#include <time.h>

char *sdoc[] = {
		"									",
		" WAVE_3DSEGYWINDOWING = WINDOWING DATA BY HEADER SELECTED",
		"",
		" inp=          (input segy file)",
		" out=          (output su file)",
		" pos1=         (extract from header value..",
		" pos2=         (extract until header value ..",
		" iheader=9     (9=same as ffid)",
		" bheader=4     (read header as 4 byte)",
		" theader=int   (int, float, ibmfloat)",
		"",
		" verbose=1     (show info)",
		" vblock=10000  (show verbose every vblock traces)",
		" endian=0      (0=little endian)",
		"",
		" Sample Command :",
		" posfile=      use  wave_writeneighborfile.v2",
		" ",
		" wave_3dsegywindowing inp= out= posfile= ",
		"",
		NULL};


int main (int argc, char **argv)
{
	char *ccinp, *ccout;
	int verbose;
	int endian, vblock, idxvblock;
	FILE *FFIN=NULL, *FFOD=NULL;
	pos3ddata **data=NULL;
	char cebcdic[3200];
	segy trace;
	int nsegy, ntrc, nsp, format;
	int pos1, pos2, iheader, bheader;
	char theader;
	div_t divresult;
	time_t t1,t2;

	/* hook up getpar to handle the parameters */
	initargs(argc,argv);
	requestdoc(1);

	MUSTGETPARSTRING("inp",  &ccinp);
	MUSTGETPARSTRING("out",  &ccout);
	if (!getparint("pos1",&pos1)) err("pos1=??");
	if (!getparint("pos2",&pos2)) err("pos2=??");
	if (!getparint("iheader",&iheader)) iheader=4;
	if (!getparint("bheader",&bheader)) bheader=4;
	if (!getparstring("theader",&theader)) theader="int";


	/*read segy input */
	FFIN = fopen (ccinp,"r");
	if (FFIN==NULL) err ("Error opening file 1 : %s\n", ccinp);
	readEbcdicHeader(FFIN, cebcdic); /* read ebcdic header */
	readBinaryHeader(FFIN, endian, &bh, &nsegy); /*read binary header */
	ntrc = getNumberOfTraceSegyFile(FFIN, nsegy);
	nsp = bh.hns;
	format = bh.format;

	FFOD = fopen(ccout, "w");
	writeEbcdicHeader(FFOD, cebcdic); /* read ebcdic header */
	bh.hns = pos2-pos1+1;
	writeBinaryHeader(FFOD, endian, bh);

	for(i=0; i<ntrc; i++)
	{
//		readTraceSegy(FFIN, endian, nsp,)
	}

	fclose(FFIN);
	fclose(FFOD);
	return(1);
}

