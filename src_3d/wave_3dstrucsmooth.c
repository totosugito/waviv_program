/*
 * wave_3dstrucsmooth.c
 *
 *  Created on: Jan 24, 2012
 *      Author: toto
 */

#include "segy_lib.h"
#include "smoothingLib.h"
#include "wave_3dlib.h"
#include "slicingLib.h"
#include "iolibrary.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <omp.h>

char *sdoc[] = {
		"									",
		" WAVE_3DSTRUCSMOOTH.v8",
		" ",
		" Input Parameter :",
		" segyinp=              (input segy file)",
		" segyout=              (output segy file)",
		" inpos=                (input file position)",
		" tmpfile=              (temporary slicing file)",
		"",
		" tslice=50             (process data every tslice)",
		" bcreateslice=1        (1=create slicing file)",
		" bremoveslice=1        (1=remove temporary slicing file)",
		" tmin=0                (process from t minimum tmin",
		" tmax=0                (process until t maximum tmax, 0=nsp",
		" tsaving=0             (0=saving all time, 1=saving selected time)",
		" ",
		" sigma=0.5             (window size for smoothing local atribute)",
		" sigmaderiv=0.5        (window size for dip analysis)",
		" k=0.5                 (seismic=0.5)",
		" dts=0.5               (strength of smoothing)",
		" klength=5             (size of analysis window in sample)",
		" nstep=8               (smoothing iteration)",
		"",
		" vblock=10000          (show verbose every vblock)",
		" verbose=0             (=1 show debug)",
		" endian=0              (0=little endian)",
		"",
		" How to use :",
		" wave_3dstrucsmooth segyinp= segyout= inpos= ",

		NULL};

void processSlicingData(float **data_ori, float **data_process, int ny, int nx,
		int islice, int nslice,
		int widthInline, int widthXline, pos3ddata **posdata,
		float sigma, float sigmaderiv, float k, float dts, int klength, int nstep);

int main(int argc, char **argv)
{
	int i, j, verbose;
	float k, sigma, sigmaderiv, dts;
	int klength, nstep;
	int tslice, nslice;

	char *csegyinp, *csegyout, *inpos, *ctmpfile, *ctmpmergefile;
	FILE *segyinp=NULL, *segyout=NULL, *tmpmergefile=NULL;
	char cebcdic[3200];
	int vblock, endian;
	int nsegy, nsegyo, ntrc, nsp, format;
	int **tslicearray=NULL;
	int tmppos1, tmppos2;
	char *tmpfileslice;
	float **data_ori=NULL, **data_process=NULL, *tmpdata;
	pos3ddata **posdata=NULL;
	int lendata, result, idxvblock, bcreateslice, bremoveslice;

	int widthInline, widthXline, min_iline, max_iline, min_xline, max_xline;
	int tmin, tmax, deltat, tsaving;

	div_t divresult;
	time_t t1,t2;

	/* hook up getpar to handle the parameters */
	initargs(argc,argv);
	requestdoc(1);

	//input parameter
	MUSTGETPARSTRING("segyinp",  &csegyinp);
	MUSTGETPARSTRING("segyout",  &csegyout);
	MUSTGETPARSTRING("inpos",  &inpos);
	MUSTGETPARSTRING("tmpfile",  &ctmpfile);

	if(!getparfloat("sigma",&sigma)) 	sigma = 0.5;
	if(!getparfloat("sigmaderiv",&sigmaderiv)) 	sigmaderiv = 0.5;
	if(!getparfloat("k",&k)) 	k = 0.5;
	if(!getparfloat("dts",&dts)) 	dts = 0.5;
	if(!getparint("klength",&klength)) 	klength = 5;
	if(klength%2==0) klength++;
	if(!getparint("nstep",&nstep)) 	nstep = 8;
	if(!getparint("tmin",&tmin)) 	tmin = 0;
	if(!getparint("tmax",&tmax)) 	tmax = 0;
	if(!getparint("tsaving",&tsaving)) 	tsaving = 0; /* 0 = saving all, 1 = saving selected time */

	if(!getparint("verbose",&verbose)) 	verbose = 0;
	if(!getparint("endian",&endian)) 	endian = 0;
	if(!getparint("tslice",&tslice)) 	tslice = 50;
	if(!getparint("vblock",&vblock)) 	vblock = 10000;
	if(!getparint("bcreateslice",&bcreateslice)) 	bcreateslice = 1;
	if(!getparint("bremoveslice",&bremoveslice)) 	bremoveslice = 1;

	/* start process */
	t1 = time(NULL);

	ctmpmergefile = tmp_mergeSliceFileName(ctmpfile); /*create temp merge file*/

	//read pos xline and xline data
	posdata = pos3ddata_read(inpos, &widthInline, &widthXline,
			&min_iline, &max_iline, &min_xline, &max_xline);

	//----------- read segy input----------------
	segyinp = fopen (csegyinp,"r");
	if (segyinp==NULL) err ("Error opening input file : %s\n", csegyinp);

	//read file 1 header information
	readEbcdicHeader(segyinp, cebcdic); /* read ebcdic header */
	readBinaryHeader(segyinp, endian, &bh, &nsegy); /*read binary header */
	ntrc = getNumberOfTraceSegyFile(segyinp, nsegy);

	nsp = bh.hns;
	format = bh.format;
	nsegyo = nsegy;

	/* setting tmin and tmax */
	if(tmin<0)	tmin=0;
	if(tmax==0 || tmax>nsp)	tmax = nsp;
	if(tmin > tmax)	err("Input tmin > tmax !");
	deltat = tmax-tmin;

	nslice = (int) (ceil((float)deltat/(float)tslice));
	tslicearray = alloc2int(3, nslice);

	/*get slicing position */
	for (i=0; i<nslice; i++)
	{
		tmppos1 = tmin + (i*tslice);
		tmppos2 = tmppos1 + tslice;
		if(tmppos2>tmax)	tmppos2 = tmax;
		tslicearray[i][0] = tmppos1;			//minimum slice position
		tslicearray[i][1] = tmppos2;			//maximum slice position
		tslicearray[i][2] = tmppos2 - tmppos1;	//delta slice (width of slice)
	}

	/*create slicing input segy file*/
	if(bcreateslice){
		createTimeSlicing(segyinp, ctmpfile, ntrc, tmax, nsp, nsegy, format, endian,
				nslice, tslice, tslicearray, vblock);
	}
	fclose(segyinp);	/*close input segy*/

	/*process slicing file*/
	data_ori = alloc2float(ntrc, tslice);
	data_process = alloc2float(ntrc, tslice);
	lendata = 0;
	for(i=0; i<nslice; i++)
	{
		tmpfileslice =createSlicingFileName(ctmpfile, tslicearray[i][0], tslicearray[i][1]);

		readSlicingFile(data_ori, tmpfileslice, i, nslice, ntrc, tslicearray); 			/* read slicing file */

		processSlicingData(data_ori, data_process, tslicearray[i][2], ntrc,
				i, nslice,
				widthInline, widthXline, posdata,
				sigma, sigmaderiv, k, dts, klength, nstep); 	/*process slicing file*/

		mergeSlicingFile(ctmpmergefile, data_process, deltat, ntrc, i, nslice,
				lendata, tslicearray, vblock); /*merge slicing file*/

		if(bremoveslice)
			removeSlicingFile(tmpfileslice);				/* remove temporary time slicing file */

		lendata = lendata + tslicearray[i][2];
		free(tmpfileslice);
	}


	/* open again input segy */
	//----------- read segy input----------------
	segyinp = fopen (csegyinp,"r");
	if (segyinp==NULL) err ("Error opening input file : %s\n", csegyinp);
	gotoTraceSegyPosition(segyinp, 0, nsegy);

	//convert temporary binary output to segy file
	segyout = fopen(csegyout, "w");
	if(!segyout)	err("error opening output file %s", csegyout);

	/* open temporary output file */
	tmpmergefile = fopen(ctmpmergefile, "r");
	if(!tmpmergefile) err("error opening merge file %s", ctmpmergefile);

	writeEbcdicHeader(segyout, cebcdic);
	if(tsaving != 0)	/* saving selected time */
	{
		bh.hns = deltat;
		nsegyo = 240 + (deltat*4);
	}
	writeBinaryHeader(segyout, endian, &bh);

	gotoTraceSegyPosition(segyinp, 0, nsegy);
	tmpdata = alloc1float(nsp);
	idxvblock = 0;
	for(i=0; i<ntrc; i++)
	{
		if(i==idxvblock*vblock)
		{
			fprintf(stderr, "\nWriting Output File : Trace %i / %i", i+1, ntrc);
			idxvblock++;
		}

		result = fread(tmpdata, sizeof(float), deltat, tmpmergefile);	/*read temporary file*/
		readTraceSegy(segyinp, endian, nsp, format, nsegy, &tr);

		for(j=tmin; j<tmax; j++)
		{
			if(tsaving==0)
				tr.data[j] = tmpdata[j-tmin];
			else	/* saving selected time */
			{
				tr.ns = deltat;
				tr.data[j-tmin] = tmpdata[j-tmin];
			}
		}

		writeTraceSegy(segyout, endian, &tr, nsegyo, nsp);
	}

	remove(ctmpmergefile);	/*remove temporary file*/
	fclose(segyinp);
	fclose(segyout);
	fclose(tmpmergefile);

	free1float(tmpdata);
	free2float(data_ori);
	free2float(data_process);
	free2int(tslicearray);
	free(ctmpmergefile);

	t2 = time(NULL);
	divresult = div (t2-t1, 60);
	fprintf (stderr, "\n\nProcess time = %d min %d sec\n", divresult.quot, divresult.rem);

	return(1);
}



void processSlicingData(float **data_ori, float **data_process, int ny, int nx,
		int islice, int nslice,
		int widthInline, int widthXline, pos3ddata **posdata,
		float sigma, float sigmaderiv, float k, float dts, int klength, int nstep)
{
	int i;
	float **data=NULL;

	int chunk, tid;

	chunk=5;
#pragma omp parallel shared(data_ori, data_process) private(i, data, tid)
	{
		tid = omp_get_thread_num();
#pragma omp for schedule(dynamic,chunk) nowait
	for(i=0; i<ny; i++)		//process 2D data time slice
	{
		fprintf(stderr, "SLICE [%i / %i] , Thread [%i] Time Slicing %i / %i \n", islice+1, nslice, tid, i+1, ny);

		data = alloc2float(widthXline, widthInline);

		/*convert timeslice to array 2D*/
		timeslice_to_Data2d(data_ori[i], data, widthInline, widthXline, posdata);

		/* ------------ PROCESS DATA -------------------*/
		/*structural oriented smoothing*/
		wvstrucsmooth(data, widthInline, widthXline, sigma, sigmaderiv, k, dts, klength, nstep);

		//convert array 2D to timeslice
		Data2d_to_timeslice(data_process[i], data, widthInline, widthXline, posdata);

		free2float(data);
	}

	}
}



