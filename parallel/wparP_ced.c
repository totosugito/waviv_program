/*
 * wparP_ced.c
 *
 *  Created on: Dec 18, 2012
 *      Author: toto
 */
#include "../src_lib/cedSmoothingLib.h"
#include "../src_lib/segy_lib.h"
#include "../src_lib/binaryLib.h"
#include <time.h>
#include <mpi.h>

const char *sdoc[] = {
		"",
		" WPARP_CED.V.1 : PARALLEL PROMAX ADDONS COHERENCE ENHANCING DIFFUSION ",
		"",
		" Usage:",
		" mpirun -np [NUMBEROFPROCESS] -hostfile [HOSTFILE]",
		"",
		" grouph=        input binary grouph file",
		" traces=        input binary traces file",
		" obscale=5.0    sigma",
		" intscale=0.7   sigma derivative",
		" stepsize=0.2   coefficient diffusion",
		" k=0.001        coefficient exponent ",
		" nosteps=10     number of iteration",
		" verbose=0      show debug",
		"",
		" Sample Command :",
		" wparP_ced grouph=grouphData traces=tracesData obscale=5.0 intscale=0.7 stepsize=0.2",
		"",
		NULL};

// ----------------- segy variable ---------------------
int ntrc, nsp;
float dt;

// ----------------- mpi variable ----------------------
MPI_Status status;
int numprocs, rankmpi, namelen;
char processor_name[MPI_MAX_PROCESSOR_NAME];

// ----------------- temporary variable -----------------
int i;
int iidx, intrc;
float **traces=NULL;
int **grouphdata=NULL, ngrouph, ntrc_grouph;
char *cgrouph=NULL, *ctraces=NULL;
float **tmp_data;

// ----------------- program variable -------------------
float obsscale;
float intscale;
float stepsize;
float k;
int nosteps;
int verbose;

void get_input_parameter()
{
	if (!getparfloat("obsscale",&obsscale)) obsscale=5.0;
	if (!getparfloat("intscale",&intscale)) intscale=0.7;
	if (!getparfloat("stepsize",&stepsize)) stepsize=0.2;
	if (!getparfloat("k",&k)) k=0.001;
	if (!getparint("nosteps",&nosteps)) nosteps=10;
	if (!getparint("verbose",&verbose)) verbose=0;
}
// -------------------------------------------------------

void broadcasting_data()
{
	MPI_Bcast(&nsp, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&obsscale, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&intscale, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&stepsize, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&k, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&nosteps, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&verbose, 1, MPI_INT, 0, MPI_COMM_WORLD);
}

void run_parallel()
{
	//run CED smoothing
	runCED(tmp_data, ntrc, nsp, k, obsscale, intscale, stepsize, nosteps, verbose);
}

int main(int argc, char **argv)
{
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
	MPI_Comm_rank(MPI_COMM_WORLD, &rankmpi);
	MPI_Get_processor_name(processor_name, &namelen);

	/* hook up getpar to handle the parameters */
	initargs(argc,argv);
	requestdoc(1);

	MUSTGETPARSTRING("grouph",  &cgrouph);	/* input grouph file */
	MUSTGETPARSTRING("traces",  &ctraces);	/* input traces file */

	// --------------- get input parameter ---------------------
	get_input_parameter();

	// read grouph and trace data
	MPI_Barrier(MPI_COMM_WORLD); //01
	if (rankmpi == 0)
	{

		grouphdata = readBinaryGrouphFile(cgrouph, &nsp, &ntrc_grouph, &dt, &ngrouph);
		traces = readBinaryTracesFile(ctraces, nsp, ntrc_grouph);
		if(numprocs!=ngrouph+1){
			printf("\nERROR: Change number of processor from %i to %i \n\n\n", numprocs, ngrouph+1);
			exit(0);
		}
	}
	MPI_Barrier(MPI_COMM_WORLD); //01

	// broadcasting parameter
	broadcasting_data();

	MPI_Barrier(MPI_COMM_WORLD); //03
	if (rankmpi == 0)
	{
		// ------------- send data to every processor -------------
		iidx = 0;
		for (i = 1; i <= ngrouph; i++)
		{
			intrc = grouphdata[i-1][3];
            if(verbose)
	            printf("running ntrc=%i traces \n", intrc);

			MPI_Send(&intrc, 1, MPI_INT, i, 1, MPI_COMM_WORLD); //number of traces
			MPI_Send(traces[iidx], nsp*intrc, MPI_FLOAT, i, 1, MPI_COMM_WORLD);	//send trace data
			iidx += intrc;
		}

		//--------------- receive data from every processor -------------
		iidx = 0;
		for(i = 1; i <= ngrouph; i++)
		{
			intrc = grouphdata[i-1][3];
			MPI_Recv(traces[iidx], nsp*intrc, MPI_FLOAT, i, 1, MPI_COMM_WORLD, &status);
			iidx += intrc;
		}
	}
	else
	{
		//receive data from master
		MPI_Recv(&ntrc, 1, MPI_INT, 0, 1, MPI_COMM_WORLD, &status);

		tmp_data = alloc2float(nsp, ntrc);
		MPI_Recv(tmp_data[0], nsp*ntrc, MPI_FLOAT, 0, 1, MPI_COMM_WORLD, &status);

		//running parallel process
		run_parallel();

		//send data to master
		MPI_Send(tmp_data[0], nsp*ntrc, MPI_FLOAT, 0, 1, MPI_COMM_WORLD);

		//free temporary data
		free2float(tmp_data);
	}
	MPI_Barrier(MPI_COMM_WORLD); //03

	//save data and free allocated memory
	MPI_Barrier(MPI_COMM_WORLD); //04
	if (rankmpi == 0)
	{
		writeBinaryTracesFile(ctraces, traces, nsp, ntrc_grouph);
		free2float(traces);
		free2int(grouphdata);
	}
	MPI_Barrier(MPI_COMM_WORLD); //04

	MPI_Finalize();
	return(1);
}



