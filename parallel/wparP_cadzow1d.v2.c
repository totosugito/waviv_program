/*
 * wparP_ced.c
 *
 *  Created on: Dec 18, 2012
 *      Author: toto
 */
#include "../src_lib/wvfxssa.h"
#include "../src_lib/segy_lib.h"
#include "../src_lib/binaryLib.h"
#include "../src_lib/fftLib.h"
//#include "../src_lib/waviv_common.h"

#include <time.h>
#include <mpi.h>

const char *sdoc[] = {
		"",
		" WPARP_CADZOW1D.V.2 : PARALLEL PROMAX ADDONS CADZOW 1D ",
		"",
		" Usage:",
		" mpirun -np [NUMBEROFPROCESS] -hostfile [HOSTFILE]",
		"",
		" grouph=          input binary grouph file",
		" traces=          input binary traces file",
		" rank=1           Rank used for Low-Rank Approx.",
		" spatspan=10      Spatial Analysis Window Span (trace)",
		" tempspan=120     Temporal Analysis Window Span (time)",
		" fftpad=0.00      Percent Padding for FX-FFT Transform",
//		" epsilon=0.45     ",
//		" pocs=0           ",
		" maxiter=5        iteration for reweighting process",
//		" interp=0         Interpolation",
//		" old=0            ",
//		" dealiased=0      Dealiased",
		" ",
		" Sample Command :",
		" wparP_cadzow1d.v2 grouph=grouphData traces=tracesData rank=3 ",
		"",
		NULL};

// ----------------- segy variable ---------------------
int ntrc, nsp;
float dt;

// ----------------- mpi variable ----------------------
MPI_Status status;
int numprocs, rankmpi, namelen;
char processor_name[MPI_MAX_PROCESSOR_NAME];

// ----------------- temporary variable -----------------
int i;
int iidx, intrc;
float **traces=NULL;
int **grouphdata=NULL, ngrouph, ntrc_grouph;
char *cgrouph=NULL, *ctraces=NULL;
float **tmp_data;

// ----------------- program variable -------------------
int nf, nfft;
float d1;
int rank;
float ffmin;
float ffmax;
int spatspan;
int tempspan;
float fftpad;
float epsilon;
int pocs;
int interp;
int old;
int maxiter;
int tmin, tmax;
int dealiased;


void get_input_parameter()
{
	if (!getparint("rank",&rank)) rank=1;
	if (!getparfloat("fmin",&ffmin)) ffmin=0.0;
	if (!getparfloat("fmax",&ffmax)) ffmax=60.0;

	if(!getparint("spatspan",&spatspan))spatspan=10;
	if(!getparint("tempspan",&tempspan))tempspan=120;
	if(!getparfloat("fftpad",&fftpad))fftpad=0.00;
	if(!getparfloat("epsilon",&epsilon))epsilon=0.45;

	if(!getparint("pocs",&pocs))pocs=0;
	if(!getparint("maxiter",&maxiter))maxiter=5;
	if(!getparint("interp",&interp))interp=0;
	if(!getparint("old",&old))old=0;
	if(!getparint("dealiased",&dealiased))dealiased=0;
}
// -------------------------------------------------------

void broadcasting_data()
{
	getNfft(nsp, dt, &nf, &nfft, &d1);
    if(ffmin<=0.0)    ffmin = 0.0;
    if(ffmin>=nf*d1) ffmin = nf*d1 - d1;

    if(ffmax<=0.0)    ffmax = nf*d1 - d1;
    if(ffmax>=nf*d1) ffmax = nf*d1 - d1;

    if(ffmax<ffmin) ffmax = nf*d1 - d1;

    tmin = (int) (ffmin/d1);
    tmax = (int) (ffmax/d1);

	MPI_Bcast(&nsp, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&nf, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&nfft, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&d1, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&rank, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&tmin, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&tmax, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&maxiter, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);

	MPI_Bcast(&spatspan, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&tempspan, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&fftpad, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&epsilon, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&pocs, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&interp, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&old, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&dealiased, 1, MPI_INT, 0, MPI_COMM_WORLD);
}

void run_parallel()
{
	run_cadzow_v3(tmp_data, ntrc, nsp,
			rank, spatspan, tempspan, fftpad, epsilon,
			pocs, maxiter, interp, old, dealiased);
}

int main(int argc, char **argv)
{
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
	MPI_Comm_rank(MPI_COMM_WORLD, &rankmpi);
	MPI_Get_processor_name(processor_name, &namelen);

	/* hook up getpar to handle the parameters */
	initargs(argc,argv);
	requestdoc(1);

	MUSTGETPARSTRING("grouph",  &cgrouph);	/* input grouph file */
	MUSTGETPARSTRING("traces",  &ctraces);	/* input traces file */

	// --------------- get input parameter ---------------------
	get_input_parameter();

	// read grouph and trace data
	MPI_Barrier(MPI_COMM_WORLD); //01
	if (rankmpi == 0)
	{

		grouphdata = readBinaryGrouphFile(cgrouph, &nsp, &ntrc_grouph, &dt, &ngrouph);
		traces = readBinaryTracesFile(ctraces, nsp, ntrc_grouph);
		if(numprocs!=ngrouph+1){
			printf("\nERROR: Change number of processor from %i to %i \n\n\n", numprocs, ngrouph+1);
			exit(0);
		}
	}
	MPI_Barrier(MPI_COMM_WORLD); //01

	// broadcasting parameter
	broadcasting_data();

	MPI_Barrier(MPI_COMM_WORLD); //03
	if (rankmpi == 0)
	{
		// ------------- send data to every processor -------------
		iidx = 0;
		for (i = 1; i <= ngrouph; i++)
		{
			intrc = grouphdata[i-1][3];
			MPI_Send(&intrc, 1, MPI_INT, i, 1, MPI_COMM_WORLD); //number of traces
			MPI_Send(traces[iidx], nsp*intrc, MPI_FLOAT, i, 1, MPI_COMM_WORLD);	//send trace data
			iidx += intrc;
		}

		//--------------- receive data from every processor -------------
		iidx = 0;
		for(i = 1; i <= ngrouph; i++)
		{
			intrc = grouphdata[i-1][3];
			MPI_Recv(traces[iidx], nsp*intrc, MPI_FLOAT, i, 1, MPI_COMM_WORLD, &status);
			iidx += intrc;
		}
	}
	else
	{
		//receive data from master
		MPI_Recv(&ntrc, 1, MPI_INT, 0, 1, MPI_COMM_WORLD, &status);

		tmp_data = alloc2float(nsp, ntrc);
		MPI_Recv(tmp_data[0], nsp*ntrc, MPI_FLOAT, 0, 1, MPI_COMM_WORLD, &status);

		//running parallel process
		run_parallel();

		//send data to master
		MPI_Send(tmp_data[0], nsp*ntrc, MPI_FLOAT, 0, 1, MPI_COMM_WORLD);

		//free temporary data
		free2float(tmp_data);
	}
	MPI_Barrier(MPI_COMM_WORLD); //03

	//save data and free allocated memory
	MPI_Barrier(MPI_COMM_WORLD); //04
	if (rankmpi == 0)
	{
		writeBinaryTracesFile(ctraces, traces, nsp, ntrc_grouph);
		free2float(traces);
		free2int(grouphdata);
	}
	MPI_Barrier(MPI_COMM_WORLD); //04

	MPI_Finalize();
	return(1);
}
