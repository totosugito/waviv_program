/*
 * wparP_ced.c
 *
 *  Created on: Dec 18, 2012
 *      Author: toto
 */
#include "../src_lib/segy_lib.h"
#include "../src_lib/binaryLib.h"
#include "../src_lib/cadzowLib.h"
#include <time.h>
#include <mpi.h>

const char *sdoc[] = {
		"",
		" WPARP_CADZOW1D.V.1 : PARALLEL PROMAX ADDONS CADZOW 1D ",
		"",
		" Usage:",
		" mpirun -np [NUMBEROFPROCESS] -hostfile [HOSTFILE]",
		"",
		" grouph=          input binary grouph file",
		" traces=          input binary traces file",
		" rank=3           input cadzow rank",
		" iswindowing=1    Windowing method",
		" ",
		" iswindowing=[0, 3]    ",
		"    fmin=0.5         Minimum frequency",
		"    fmax=105.0       Maximum frequency",
		" ",
		" iswindowing=[1, 2]    ",
		"    timewindow=30    Half Width for Time windowing",
		"    timeoverlap=5    Half Width for Overlap save. Value<=(Time_Window)/3",
		"    tracewindow=30   Half Width for Trace windowing",
		"    traceoverlap= 5  Half Width for Overlap save. Value<=(Trace_Window)/3",
		"    sigmax=10.0      Sigma X for Gaussian Window ",
		"    sigmay=10.0      Sigma Y for Gaussian Window ",
		" ",
		" iswindowing=[3]    ",
		"    e=20.0           normalization weight",
		"    maxiter=5        iteration for reweighting process",
		" ",
		" Sample Command :",
		" wparP_cadzow1d grouph=grouphData traces=tracesData rank=3 fmin=0.5 fmax=105",
		"",
		NULL};

// ----------------- segy variable ---------------------
int ntrc, nsp;
float dt;

// ----------------- mpi variable ----------------------
MPI_Status status;
int numprocs, rankmpi, namelen;
char processor_name[MPI_MAX_PROCESSOR_NAME];

// ----------------- temporary variable -----------------
int i;
int iidx, intrc;
float **traces=NULL;
int **grouphdata=NULL, ngrouph, ntrc_grouph;
char *cgrouph=NULL, *ctraces=NULL;
float **tmp_data;

// ----------------- program variable -------------------
int nf, nfft;
float d1;
int rank;
float ffmin;
float ffmax;
int iswindowing;
int span;
int use;
int spantrace;
int usetrace;
float sigmax;
float sigmay;
float e;
int maxiter;
int tmin, tmax;



void get_input_parameter()
{
	if (!getparint("rank",&rank)) rank=1;
	if (!getparfloat("fmin",&ffmin)) ffmin=0.0;
	if (!getparfloat("fmax",&ffmax)) ffmax=60.0;
	if (!getparint("iswindowing",&iswindowing)) iswindowing=1;
	if(!getparint("span",&span))span=60;
	if(!getparint("use",&use))use=10;
	if(!getparint("spantrace",&spantrace))spantrace=30;
	if(!getparint("usetrace",&usetrace))usetrace=5;
	if(!getparfloat("sigmax",&sigmax))sigmax=20;
	if(!getparfloat("sigmay",&sigmay))sigmay=20;
	if(!getparfloat("e",&e))e=20;
	if(!getparint("maxiter",&maxiter))maxiter=5;
}
// -------------------------------------------------------

void broadcasting_data()
{
	getNfft(nsp, dt, &nf, &nfft, &d1);
    if(ffmin<=0.0)    ffmin = 0.0;
    if(ffmin>=nf*d1) ffmin = nf*d1 - d1;

    if(ffmax<=0.0)    ffmax = nf*d1 - d1;
    if(ffmax>=nf*d1) ffmax = nf*d1 - d1;

    if(ffmax<ffmin) ffmax = nf*d1 - d1;

    tmin = (int) (ffmin/d1);
    tmax = (int) (ffmax/d1);

	MPI_Bcast(&nsp, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&nf, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&nfft, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&d1, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&iswindowing, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&rank, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&tmin, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&tmax, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&use, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&span, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&usetrace, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&spantrace, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&sigmax, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&sigmay, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&e, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&maxiter, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
}

void run_parallel()
{
	//---------------------- run CADZOW----------------------------
	if(iswindowing==0)
		runCadzow1D(tmp_data, rank, ntrc, nsp, nf, nfft, d1, tmin, tmax);
	else if(iswindowing==1)
		runCadzow1D_wosa(tmp_data, rank, ntrc, nsp, dt,
				use, span, usetrace, spantrace, sigmax, sigmay);
	else if(iswindowing==2)
		runCadzow1D_wosa_rev1(tmp_data, rank, ntrc, nsp, dt,
				use, span, usetrace, spantrace, sigmax, sigmay);
	else if(iswindowing==3)
		robustRunCadzow1D(tmp_data, rank, ntrc, nsp, nf, nfft, d1,
				tmin, tmax, e, maxiter);
	//------------------------------------------------------------------
}

int main(int argc, char **argv)
{
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
	MPI_Comm_rank(MPI_COMM_WORLD, &rankmpi);
	MPI_Get_processor_name(processor_name, &namelen);

	/* hook up getpar to handle the parameters */
	initargs(argc,argv);
	requestdoc(1);

	MUSTGETPARSTRING("grouph",  &cgrouph);	/* input grouph file */
	MUSTGETPARSTRING("traces",  &ctraces);	/* input traces file */

	// --------------- get input parameter ---------------------
	get_input_parameter();

	// read grouph and trace data
	MPI_Barrier(MPI_COMM_WORLD); //01
	if (rankmpi == 0)
	{

		grouphdata = readBinaryGrouphFile(cgrouph, &nsp, &ntrc_grouph, &dt, &ngrouph);
		traces = readBinaryTracesFile(ctraces, nsp, ntrc_grouph);
		if(numprocs!=ngrouph+1){
			printf("\nERROR: Change number of processor from %i to %i \n\n\n", numprocs, ngrouph+1);
			exit(0);
		}
	}
	MPI_Barrier(MPI_COMM_WORLD); //01

	// broadcasting parameter
	broadcasting_data();

	MPI_Barrier(MPI_COMM_WORLD); //03
	if (rankmpi == 0)
	{
		// ------------- send data to every processor -------------
		iidx = 0;
		for (i = 1; i <= ngrouph; i++)
		{
			intrc = grouphdata[i-1][3];
			MPI_Send(&intrc, 1, MPI_INT, i, 1, MPI_COMM_WORLD); //number of traces
			MPI_Send(traces[iidx], nsp*intrc, MPI_FLOAT, i, 1, MPI_COMM_WORLD);	//send trace data
			iidx += intrc;
		}

		//--------------- receive data from every processor -------------
		iidx = 0;
		for(i = 1; i <= ngrouph; i++)
		{
			intrc = grouphdata[i-1][3];
			MPI_Recv(traces[iidx], nsp*intrc, MPI_FLOAT, i, 1, MPI_COMM_WORLD, &status);
			iidx += intrc;
		}
	}
	else
	{
		//receive data from master
		MPI_Recv(&ntrc, 1, MPI_INT, 0, 1, MPI_COMM_WORLD, &status);

		tmp_data = alloc2float(nsp, ntrc);
		MPI_Recv(tmp_data[0], nsp*ntrc, MPI_FLOAT, 0, 1, MPI_COMM_WORLD, &status);

		//running parallel process
		run_parallel();

		//send data to master
		MPI_Send(tmp_data[0], nsp*ntrc, MPI_FLOAT, 0, 1, MPI_COMM_WORLD);

		//free temporary data
		free2float(tmp_data);
	}
	MPI_Barrier(MPI_COMM_WORLD); //03

	//save data and free allocated memory
	MPI_Barrier(MPI_COMM_WORLD); //04
	if (rankmpi == 0)
	{
		writeBinaryTracesFile(ctraces, traces, nsp, ntrc);
		free2float(traces);
		free2int(grouphdata);
	}
	MPI_Barrier(MPI_COMM_WORLD); //04

	MPI_Finalize();
	return(1);
}
