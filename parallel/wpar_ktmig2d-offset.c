/*
 * sarp_parallel_suktmig2d-offset.c
 *
 *  Created on: Oct 1, 2012
 *      Author: toto
 */
#include <stdio.h>
#include <string.h>
#include <mpi.h>
#include <unistd.h>
#include <stdlib.h>
#include "../src_lib/segy_lib.h"
#include "../src_lib/fftLib.h"
#include <time.h>
#include <limits.h>
#include <stdbool.h>

char *sdoc[] = {
		"			",
		" WPAR_KTMIG2D-OFFSET.v1.1: prestack time migration of a common-offset section",
		"                                with the double-square root (DSR) operator",
		"",
		" Usage:",
		" mpirun -np [NUMBEROFPROCESS] -hostfile [HOSTFILE]",
		"			",
		" Required Parameters:	",
		" inp=             Input Data						",
		" out=             Output Data",
		" vfile=           rms velocity file (units/s) v(t,x) as a function of time",
		" mode=0           0=suktmig2d, 1=tosco suktmig2d-offset, 2=suktmig2d+aperture",
		" ftype=0          0=SU file, 1=SEGY file                               	",
		" dx=              distance (units) between consecutive traces		",
		" nvelcdp=         number of consecutive cdps in velocity file 	        ",
		" firstcdp=        first cdp number in velocity file			",
		" lastcdp=         last cdp number in velocity file			",
		" offmin=          minimum offset						",
		" offmax=          maximum offset						",
		" ntrc=240         Maximum number of trace in current offset",
		" pad=5.0          padding data",
		"									",
		" Optional parameters:							",
		" intoff=0         interval between offsets				",
		" angmax=40        maximum aperture angle for migration (degrees)		",
		" aprtmax=6000     (mode=2) maximum aperture  ",
		" nfc=16           number of Fourier-coefficients to approximate low-pass	",
		"                  filters. The larger nfc the narrower the filter		",
		" fwidth=5         high-end frequency increment for the low-pass filters	",
		"                  in Hz. The lower this number the more the number of	",
		"                  lowpass filters to be calculated for each input trace.	",
		"									",
		" endian=0         set =0 for little-endian machines(PC's,DEC,etc.)",
		"",
		" Notes:								",
		" Data must be preprocessed with sufrac to correct for the wave-shaping	",
		" factor using phasefac=.25 for 2D migration.				",
		"									",
		" Input traces must be sorted into offset and cdp number. The velocity	",
		" file consists of rms velocities for all CMPs as a function of vertical",
		" time and horizontal position v(t,z) in C-style binary floating point	",
		" numbers. It's easiest to supply v(t,z) that has the same dimensions as",
		" the input data to be migrated.					",
		"									",
		" The units may be feet or meters, as long as these are consistent for	",
		" Antialias filter is performed using (Gray,1992, Geoph. Prosp), using	",
		" nc low- pass filtered copies of the data. The cutoff frequencies are	",
		" calculated  as fractions of the Nyquist frequency.			",
		"									",
		" The maximum allowed angle is 80 degrees(a 10 degree taper is applied	",
		" to the end of the aperture)						",
		"",
		NULL};

typedef struct suktmig2dvar{
	int firstOffset;
	int ntrc;
	int cdpFirst;
	int cdpEnd;
	int deltacdp;
}suktmig2dvar;

int getNumberOfLoop(int ftype, FILE *inputfile, int endian, int nsp, int format, int nsegy, int ntrc,
		int numprocs);
/* Prototype of functions used internally */
void lpfilt(int nfc, int nfft, float dt, float fhi, float *filter);

void  migkt2d ( float **data, int ntr, int nt, float dx, float dt, float tmax,
		int nfft, int fnyq, float h, float *fc, int nf, int nc, int nfc,
		int cdp_trace_first,float angmax, float **vel,float **mig, int dxcdp, int firstcdp);
void  suktmig2d ( float **data, int ntr, int nt, float dx, float dt, float tmax,
		int nfft, float fnyq, float h, float *fc, int nf, int nc, int nfc,
		int cdp_trace_first,float angmax, float **vel,float **mig, int dxcdp, int firstcdp);
void  suktmig2d_ver1 ( float **data, int ntr, int nt, float dx, float dt, float tmax,
		int nfft, float fnyq, float h, float *fc, int nf, int nc, int nfc,
		int cdp_trace_first,float angmax, float **vel,float **mig, int dxcdp, int firstcdp,
		float aprtmax);

bool getGahterGrouphByOffset(int ftype, FILE *inputfile, int endian, int nsp, int format, int nsegy,
		int numprocs, int recNtrc, float **data, char **headers, suktmig2dvar *hdrVar,
		int maxntrc, int curTr, int *endTr, int *nRecord);
void parallel_suktmig2d_offset(int ftype, char *fileinp, char *fileout, char *vfile,
		int endian, int nvelcdp, int fwidth, float dx, float angmax, int nfc,
		int recNtrc, int firstcdp, int mode,
		int numprocs, int rankmpi, float aprtmax, float fpad);

int main(int argc, char *argv[])
{
	int numprocs, rankmpi, namelen;
	char processor_name[MPI_MAX_PROCESSOR_NAME];

	int recNtrc;
	int endian;
	char *filenameinput=NULL;
	char *filenameoutput=NULL;
	char *vfile=NULL;
	int mode;
	float aprtmax;

	int offmin,offmax, ftype;
	int intoff, verbose;
	float angmax;   /* maximum aperture angle */
	int nfc;	/* number of Fourier coefficients for low-pass filter */
	int fwidth;	/* high-end frequency increment for the low-pass filter*/
	int firstcdp;	/* first cdp in velocity file		*/
	int lastcdp;	/* last cdp in velocity file		*/
	int nvelcdp;     /* number of cdps in the velocity file */
	float dx;	/* cdp sample interval */
	float fpad;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
	MPI_Comm_rank(MPI_COMM_WORLD, &rankmpi);
	MPI_Get_processor_name(processor_name, &namelen);

	initargs(argc,argv);
	requestdoc(1);

	if (!getparint("ftype",&ftype)) ftype=0;
	MUSTGETPARSTRING("inp", &filenameinput);
	MUSTGETPARSTRING("out", &filenameoutput);
	MUSTGETPARSTRING("vfile", &vfile);

	if (!getparint("mode",&mode)) mode=0;

	MUSTGETPARFLOAT("dx",&dx);
	MUSTGETPARINT("nvelcdp",&nvelcdp);
	MUSTGETPARINT("firstcdp",&firstcdp);
	MUSTGETPARINT("lastcdp",&lastcdp);
	MUSTGETPARINT("offmin",&offmin);
	MUSTGETPARINT("offmax",&offmax);
	if (!getparfloat("pad",&fpad)) fpad=5.0;

	if (!getparfloat("angmax",&angmax)) angmax=40;
	if (!getparfloat("aprtmax",&aprtmax)) aprtmax=6000;
	if (!getparint("nfc",&nfc)) nfc=16;
	if (!getparint("fwidth",&fwidth)) fwidth=5;
	if (!getparint("intoff",&intoff)) intoff=0;

	if (!getparint("ntrc",&recNtrc)) recNtrc=240;
	if (!getparint("endian",&endian)) endian=0;
	if (!getparint("verbose",&verbose)) verbose=0;

	parallel_suktmig2d_offset(ftype, filenameinput, filenameoutput, vfile, endian,
			nvelcdp, fwidth, dx, angmax, nfc, recNtrc, firstcdp, mode,
			numprocs, rankmpi, aprtmax, fpad);

	MPI_Finalize();

	return 0;
}

void parallel_suktmig2d_offset(int ftype, char *fileinp, char *fileout, char *vfile,
		int endian, int nvelcdp, int fwidth, float dx, float angmax, int nfc,
		int recNtrc, int firstcdp, int mode,
		int numprocs, int rankmpi, float aprtmax, float fpad)
{
	char ebcdic[3200];
	FILE *inputfile = NULL;
	FILE *outfile = NULL;
	FILE *vfp=NULL;
	suktmig2dvar *hdrVar=NULL;
	int ipad1, ipad2;

	float **vel=NULL;
	float dt, d1, fnyq;
	int nfft, nf, nc;
	float tmax;
	float *fc=NULL;		/* cut-frequencies for low-pass filters */
	bool gettrace, rettr;
	int nRecords, idxbuff, recordsize;
	float h;

	int i,j;
	int nsegy;
	int nsp = 0, format = 0;
	int ntrc = 0;
	char **headers=NULL;
	float **data = NULL;
	int fvaloffset;

	float **bdata = NULL, **bvel=NULL;
	float **pbdata=NULL, **pbvel=NULL;
	float **bmig = NULL, **pbmig=NULL;
	float *bfc=NULL;
	int intrc, icdpfirst, ideltacdp;

	int trntrc = 0;
	int itag;
	int curTr, endTr;
	int nloop=0, itr;

	MPI_Status status;

	time_t t1 = time(NULL);
	time_t t2;
	div_t divresult;

	MPI_Barrier(MPI_COMM_WORLD); //01
	if (rankmpi == 0)
	{
		//read segy header
		inputfile = fopen(fileinp,"r");
		if(ftype)
		{
			readEbcdicHeader(inputfile, ebcdic);
			readBinaryHeader(inputfile, endian, &bh, &nsegy);

			ntrc = getNumberOfTraceSegyFile(inputfile, nsegy);
			nsp = bh.hns;
			format = bh.format;
			dt = (float) (bh.hdt/1000000.0);
		}
		else
		{
			fgettr(inputfile, &tr);
			nsp = tr.ns;
			dt = (float) (tr.dt/1000000.0);
			ntrc = getNumberOfTraceSuFile(inputfile);
			nsegy = nsp*4+240;
		}

		//----------- SUKTMIG2D PARAMETER ----------------
		tmax = (nsp-1)*dt;
		getNfft(nsp, dt, &nf, &nfft, &d1);

		/* Determine number of filters for antialiasing */
		fnyq= 1.0/(2*dt);
		nc = ceil(fnyq/fwidth);

		fc  =	alloc1float(nc+1);
		for(i=1; i<nc+1; ++i)
			fc[i]= fnyq*i/nc;

		//write segy header
		outfile = fopen(fileout, "w");
		if(ftype){
			writeEbcdicHeader(outfile, ebcdic);
			writeBinaryHeader(outfile, endian, &bh);
			gotoTraceSegyPosition(inputfile, 0, nsegy);
		}

		//read velocity file
		vfp = fopen(vfile,"r");
		if(!vfp)
			err("error reading velocity file");
		vel = alloc2float(nsp, nvelcdp);
		efread(vel[0], FSIZE, nsp*nvelcdp, vfp);
		fclose(vfp);

		//create buffer of data
		recordsize = nsp*recNtrc;
		trntrc = (numprocs-1)*recNtrc;
		data = alloc2float(nsp, trntrc);
		headers = alloc2char(HDRBYTES, trntrc);
		hdrVar = (suktmig2dvar*) calloc(numprocs-1, sizeof(suktmig2dvar));

		nloop = getNumberOfLoop(ftype, inputfile, endian, nsp, format, nsegy, ntrc, numprocs);
		fprintf(stderr, "\n");
		fprintf(stderr, "Input File       : %s \n", fileinp);
		fprintf(stderr, "Output File      : %s \n", fileout);
		fprintf(stderr, "Velocity File    : %s \n", vfile);
		fprintf(stderr, "nsp       : %i \n", nsp);
		fprintf(stderr, "ntrc      : %i \n", ntrc);
		fprintf(stderr, "nf        : %i \n", nf);
		fprintf(stderr, "nfft      : %i \n", nfft);
		fprintf(stderr, "nloop     : %i \n", nloop);
		fprintf(stderr, "\n");
	}
	MPI_Barrier(MPI_COMM_WORLD); //01

	MPI_Barrier(MPI_COMM_WORLD); //02
	MPI_Bcast(&nloop, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&mode, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&dx, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&dt, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&fnyq, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&tmax, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&angmax, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);

	MPI_Bcast(&firstcdp, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&nvelcdp, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&recNtrc, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&recordsize, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&nc, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&nfc, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&nsp, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&nfft, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&nf, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD); //02

	curTr = 0;
	endTr = 0;
	gettrace = true;

	for(itr=0; itr<nloop; itr++)
	{
		//fprintf(stderr, "itr=%i / %i \n", itr, nloop);
		MPI_Barrier(MPI_COMM_WORLD); //03

		if (rankmpi == 0)
		{
			fprintf(stderr, "itr=%i / %i \n", itr+1, nloop);
			rettr = getGahterGrouphByOffset(ftype, inputfile, endian, nsp, format, nsegy,
					numprocs, recNtrc, data, headers, hdrVar, ntrc, curTr, &endTr, &nRecords);

			if(!rettr || endTr==ntrc) {
				gettrace = false; //end of trace
			}
			curTr = endTr;

			// send data to every processor
			for (i = 1; i < numprocs; i++)
			{
				if (i <= nRecords)
				{
					idxbuff = (i-1)*recNtrc;
					itag = 1;
					h = .5*hdrVar[i-1].firstOffset;

					MPI_Send(&itag, 1, MPI_INT, i, 1, MPI_COMM_WORLD);
					MPI_Send(&hdrVar[i-1].firstOffset, 1, MPI_INT, i, 1, MPI_COMM_WORLD);
					MPI_Send(&i, 1, MPI_INT, i, 1, MPI_COMM_WORLD); //send processor number id
					MPI_Send(&hdrVar[i-1].ntrc, 1, MPI_INT, i, 1, MPI_COMM_WORLD);
					MPI_Send(&hdrVar[i-1].cdpFirst, 1, MPI_INT, i, 1, MPI_COMM_WORLD);
					MPI_Send(&hdrVar[i-1].deltacdp, 1, MPI_INT, i, 1, MPI_COMM_WORLD);
					MPI_Send(&h, 1, MPI_FLOAT, i, 1, MPI_COMM_WORLD);

					MPI_Send(fc, nc+1, MPI_FLOAT, i, 1, MPI_COMM_WORLD);
					MPI_Send(vel[0], nsp*nvelcdp, MPI_FLOAT, i, 1, MPI_COMM_WORLD);
					MPI_Send(data[idxbuff], recordsize, MPI_FLOAT, i, 1, MPI_COMM_WORLD);	//send trace data
				}
				else
				{
					itag = 0;
					MPI_Send(&itag, 1, MPI_INT, i, 1, MPI_COMM_WORLD); //send tag identifier
				}
			}

			//receive data from every processor
			for(i = 1; i < numprocs; i++)
			{
				if(i <= nRecords)
				{
					idxbuff = (i-1)*recNtrc;
					MPI_Recv(&recordsize, 1, MPI_INT, i, 1, MPI_COMM_WORLD, &status);
					MPI_Recv(data[idxbuff], recordsize, MPI_FLOAT, i, 1, MPI_COMM_WORLD, &status);
				}
			}

			//save output
			for (i = 0; i < numprocs-1; i++)
			{
				if(i<nRecords)
				{
					idxbuff = i*recNtrc;
					for(j = 0; j < hdrVar[i].ntrc; j++){
						memcpy(&tr, headers[idxbuff+j], HDRBYTES*sizeof(char));
						memcpy(&tr.data, data[idxbuff+j], nsp*sizeof(float));

						if(ftype) writeTraceSegy(outfile, endian, &tr, nsegy, nsp);
						else fputtr(outfile, &tr);
					}
				}
			}
		}
		else
		{
			MPI_Recv(&itag, 1, MPI_INT, 0, 1, MPI_COMM_WORLD, &status);	//get tag

			if (itag == 1)
			{
				MPI_Recv(&fvaloffset, 1, MPI_INT, 0, 1, MPI_COMM_WORLD, &status);
				MPI_Recv(&i, 1, MPI_INT, 0, 1, MPI_COMM_WORLD, &status);
				MPI_Recv(&intrc, 1, MPI_INT, 0, 1, MPI_COMM_WORLD, &status);
				MPI_Recv(&icdpfirst, 1, MPI_INT, 0, 1, MPI_COMM_WORLD, &status);
				MPI_Recv(&ideltacdp, 1, MPI_INT, 0, 1, MPI_COMM_WORLD, &status);
				MPI_Recv(&h, 1, MPI_FLOAT, 0, 1, MPI_COMM_WORLD, &status);

				bdata = alloc2float(nsp, recNtrc);
				bvel = alloc2float(nsp, nvelcdp);
				bfc = alloc1float(nc+1);
				bmig = alloc2float(nsp, recNtrc);

				MPI_Recv(bfc, nc+1, MPI_FLOAT, 0, 1, MPI_COMM_WORLD, &status);
				MPI_Recv(bvel[0], nsp*nvelcdp, MPI_FLOAT, 0, 1, MPI_COMM_WORLD, &status);
				MPI_Recv(bdata[0], recordsize, MPI_FLOAT, 0, 1, MPI_COMM_WORLD, &status);

				ipad1 = (int) (intrc*fpad);
				ipad2 = (int) (nvelcdp*fpad);

				pbdata = alloc2float(nsp, intrc+2*ipad1);
				pbvel = alloc2float(nsp, nvelcdp+2*ipad2);
				pbmig = alloc2float(nsp, intrc+2*ipad1);

				//add padding
				for(i=0; i<ipad1; i++)
				{
					memcpy(pbdata[i], bdata[0], nsp*sizeof(float));
					memcpy(pbdata[intrc+2*ipad1-1-i], bdata[intrc-1], nsp*sizeof(float));
				}
				memcpy(pbdata[ipad1], bdata[0], intrc*nsp*sizeof(float));

				for(i=0; i<ipad2; i++)
				{
					memcpy(pbvel[i], bvel[0], nsp*sizeof(float));
					memcpy(pbvel[nvelcdp+2*ipad2-1-i], bvel[nvelcdp-1], nsp*sizeof(float));
				}
				memcpy(pbvel[ipad2], bvel[0], nvelcdp*nsp*sizeof(float));

				// PROCESS
				fprintf(stderr, "%i. Offset = %i ntrc=%i \n", i, fvaloffset, intrc);
				//fprintf(stderr, "intrc=%i dx=%f tmax=%f ideltacdp=%i \n",
				//		intrc, dx, tmax, ideltacdp);

				if(mode==0) { //using seismic unix method
					//fprintf(stderr, "SUKTMIG2D \n");
					ideltacdp = ideltacdp-1;
					suktmig2d ( pbdata, intrc+2*ipad1, nsp, dx, dt, tmax, nfft, fnyq, h, bfc, nf, nc, nfc,
							icdpfirst-ipad1, angmax, pbvel, pbmig, ideltacdp, firstcdp-ipad2);
				}
				else if(mode==1){ //using tosco method
					//fprintf(stderr, "KTMIG2D-OFFSET TOSCO \n");
					migkt2d ( pbdata, intrc+2*ipad1, nsp, dx, dt, tmax, nfft, fnyq, h, bfc, nf, nc, nfc,
							icdpfirst-ipad1, angmax, pbvel, pbmig, ideltacdp, firstcdp-ipad2);
				}
				else if(mode==2){
					ideltacdp = ideltacdp-1;
					suktmig2d_ver1( pbdata, intrc+2*ipad1, nsp, dx, dt, tmax, nfft, fnyq, h, bfc, nf, nc, nfc,
							icdpfirst-ipad1, angmax, pbvel, pbmig, ideltacdp, firstcdp-ipad2, aprtmax);
				}

				memset(bmig[0], 0, recordsize*sizeof(float));
				memcpy(bmig[0], pbmig[ipad1], nsp*intrc*sizeof(float));
				MPI_Send(&recordsize, 1, MPI_INT, 0, 1, MPI_COMM_WORLD);
				MPI_Send(bmig[0], recordsize, MPI_FLOAT, 0, 1, MPI_COMM_WORLD);

				free2float(bvel);
				free2float(bmig);
				free2float(bdata);
				free2float(pbvel);
				free2float(pbmig);
				free2float(pbdata);
				free1float(bfc);
			}
		}
		MPI_Barrier(MPI_COMM_WORLD); //03
	}

	MPI_Barrier(MPI_COMM_WORLD); //04
	if (rankmpi == 0)
	{
		free2float(data);
		free2float(vel);
		free1float(fc);
		free(hdrVar);
		fclose(outfile);
		fclose(inputfile);

		fprintf(stderr, "SUKTMIG2D-OFFSET COMPLETE!\n");

		t2 = time(NULL);
		divresult = div (t2-t1, 60);
		fprintf (stderr, "Process time = %d min %d sec\n\n", divresult.quot, divresult.rem);
	}

	MPI_Barrier(MPI_COMM_WORLD);
}

int getNumberOfLoop(int ftype, FILE *inputfile, int endian, int nsp, int format, int nsegy, int ntrc,
		int numprocs)
{
	int i;
	int oldoffset, nloop;
	int ngather;

	if(ftype)
	{
		gotoTraceSegyPosition(inputfile, 0, nsegy);
		readTraceSegy(inputfile, endian, nsp, format, nsegy, &tr);
	}
	else {
		rewind(inputfile);
		fgettr(inputfile, &tr);
	}

	ngather = 1;
	oldoffset = tr.offset;
	for(i=1; i<ntrc; i++)
	{
		if(ftype) readTraceSegy(inputfile, endian, nsp, format, nsegy, &tr);
		else fgettr(inputfile, &tr);

		if(oldoffset!=tr.offset)
		{
			oldoffset = tr.offset;
			ngather++;
		}
	}

//	printf("ngather=%i proc=%i \n", ngather, numprocs);
	nloop = (int) (ceil((double)ngather/ (double)(numprocs-1)));

	if(ftype)
		gotoTraceSegyPosition(inputfile, 0, nsegy);
	else
		rewind(inputfile);

	return(nloop);
}

bool getGahterGrouphByOffset(int ftype, FILE *inputfile, int endian, int nsp, int format, int nsegy,
		int numprocs, int recNtrc, float **data, char **headers, suktmig2dvar *hdrVar,
		int maxntrc, int curTr, int *endTr, int *nRecord)
{
	int i, j, idxbuff, idxtr;
	bool newrecord;
	int oldoffset=0, firstOffset=0;
	int cdpFirst=0, deltacdp=0, cdpEnd=0;

	i=0;
	j=0;
	idxtr = 0;
	while(i<numprocs-1)
	{
		newrecord = false;
		if(ftype) readTraceSegy(inputfile, endian, nsp, format, nsegy, &tr);
		else fgettr(inputfile, &tr);

		idxbuff = (i*recNtrc) + j;
		curTr++;
		if(j==0)
		{
			firstOffset = tr.offset;
			oldoffset = tr.offset;
			cdpFirst = tr.cdp;
			cdpEnd = cdpFirst;
			idxtr = 1;
			j++;

			memcpy(headers[idxbuff], (char*)&tr, HDRBYTES*sizeof(char));
			memcpy(data[idxbuff], &tr.data, nsp*sizeof(float));
			continue;
		}

		if(oldoffset==tr.offset)
		{
			deltacdp = tr.cdp-cdpEnd;
//			fprintf(stderr,"j=%i deltacdp=%i %i %i\n", j, deltacdp, tr.cdp, cdpEnd);
			cdpEnd = tr.cdp;

			if(j>=recNtrc)
			{
				fprintf(stderr, "trace in Record > ntrc. Change your ntrc parameter! \n\n");
				exit(0);
			}

			memcpy(headers[idxbuff], (char*)&tr, HDRBYTES*sizeof(char));
			memcpy(data[idxbuff], &tr.data, nsp*sizeof(float));
			idxtr++;
		}
		else
		{
			fseek(inputfile, -nsegy, SEEK_CUR); //go to the previous trace

			newrecord = true;
			curTr--;
		}

		if(newrecord || curTr>=maxntrc)
		{
			hdrVar[i].firstOffset = firstOffset;
			hdrVar[i].ntrc = idxtr;
			hdrVar[i].cdpFirst = cdpFirst;
			hdrVar[i].cdpEnd = cdpEnd;
			hdrVar[i].deltacdp = deltacdp;

			i++; //new record
			(*nRecord) = i;
			(*endTr) = curTr;
			j = -1;

			if(curTr>=maxntrc) //end of trace
				return(false);
		}
		j++;
	}
	return(true);
}

void  migkt2d ( float **data, int ntr, int nt, float dx, float dt, float tmax,
		int nfft, int fnyq, float h, float *fc, int nf, int nc, int nfc,
		int cdp_trace_first,float angmax, float **vel,float **mig, int dxcdp, int firstcdp)
{
	int k,imp,iip,it,ifc;	/* counters */

	float p=0.0;	/* horizontal slowness of the migration operator */
	float pmin=0.0;	/* maximum horizontal slowness for which there's */
	/* no aliasing of the operator */
	float x;	/* aperture distance */
	float xmax=0.0;	/* maximum aperture distance */

	float obliq;	/* obliquity factor */
	float geoms;	/* geometrical spreading factor */

	float mp,ip;	/* mid-point and image-point coordinates */
	float t;	/* time */
	float t0;	/* vertical traveltime */

	float ang;	/* aperture angle */
	float angtaper=0.0;	/* aperture-angle taper */
	float v;		/* velocity */

	float *filter=NULL;	/* array of low-pass filter values */

	float **lowpass=NULL;   /* low-pass filtered version of the trace */

	register float *rtin=NULL,*rtout=NULL;/* real traces */
	register complex *ct=NULL;   /* complex trace */

	float datalo[8], datahi[8];
	int itb, ite;
	float firstt, amplo, amphi;

	//fprintf(stderr, "TOSCO \n");
	/* Allocate space */
	lowpass=alloc2float(nt,nc+1);
	rtin= ealloc1float(nfft);
	rtout= ealloc1float(nfft);
	ct= ealloc1complex(nf);
	filter= alloc1float(nf);

	/* Zero all arrays */
	memset((void *) rtin, 0, nfft*FSIZE);
	memset((void *) rtout, 0, nfft*FSIZE);
	memset((void *) filter, 0, nf*FSIZE);
	memset((void *) lowpass[0], 0,nt*(nc+1)*FSIZE);

	/* Start the migration process */
	/* Loop over input mid-points first */
	for(imp=0; imp<ntr; ++imp){
		float perc;

		mp=imp*dx;
		perc=imp*100.0/(ntr-1);
		/*		if(fmod(imp*100,ntr-1)==0)
			warn("migrated %g\n ",perc);*/

		/* Calculate low-pass filtered versions  */
		/* of the data to be used for antialiasing */
		for(it=0; it<nt; ++it){
			rtin[it]=data[imp][it];
		}
		for(ifc=1; ifc<nc+1; ++ifc){
			memset((void *) rtout, 0, nfft*FSIZE);
			memset((void *) ct, 0, nf*FSIZE);
			lpfilt(nfc,nfft,dt,fc[ifc],filter);
			pfarc(1,nfft,rtin,ct);

			for(it=0; it<nf; ++it){
				ct[it] = crmul(ct[it],filter[it]);
			}
			pfacr(-1,nfft,ct,rtout);
			for(it=0; it<nt; ++it){
				lowpass[ifc][it]= rtout[it];
			}
		}


		/* Loop over vertical traveltimes */
		for(it=0; it<nt; ++it){
			int lx,ux;

			t0=it*dt;
			v=vel[imp*dxcdp-firstcdp+cdp_trace_first][it];
			/*			v=vel[imp][it];*/
			xmax=tan((angmax+10.0)*PI/180.0)*v*t0;
			lx=MAX(0,imp - ceil(xmax/dx));
			ux=MIN(ntr,imp + ceil(xmax/dx));


			/* loop over output image-points to the left of the midpoint */
			for(iip=imp; iip>lx; --iip){
				float ts,tr;
				int fplo=0, fphi=0;
				float ref,wlo,whi;

				ip=iip*dx;
				x=ip-mp;
				ts=sqrt( pow(t0/2,2) + pow((x+h)/v,2) );
				tr=sqrt( pow(t0/2,2) + pow((h-x)/v,2) );
				t= ts + tr;
				if(t>=tmax) break;
				geoms=sqrt(1/(t*v));
				obliq=sqrt(.5*(1 + (t0*t0/(4*ts*tr))
						- (1/(ts*tr))*sqrt(ts*ts - t0*t0/4)*sqrt(tr*tr - t0*t0/4)));
				ang=180.0*fabs(acos(t0/t))/PI;
				if(ang<=angmax) angtaper=1.0;
				if(ang>angmax) angtaper=cos((ang-angmax)*PI/20);
				/* Evaluate migration operator slowness p to determine */
				/* the low-pass filtered trace for antialiasing */
				pmin=1/(2*dx*fnyq);
				p=fabs((x+h)/(pow(v,2)*ts) + (x-h)/(pow(v,2)*tr));
				if(p>0){fplo=floor(nc*pmin/p);}
				if(p==0){fplo=nc;}
				ref=fmod(nc*pmin,p);
				wlo=1-ref;
				fphi=++fplo;
				whi=ref;
				itb=MAX(ceil(t/dt)-3,0);
				ite=MIN(itb+8,nt);
				firstt=(itb-1)*dt;
				/* Move energy from CMP to CIP */
				if(fplo>=nc){
					for(k=itb; k<ite; ++k){
						datalo[k-itb]=lowpass[nc][k];
					}
					ints8r(8,dt,firstt,datalo,0.0,0.0,1,&t,&amplo);
					mig[iip][it] +=geoms*obliq*angtaper*amplo;
				} else if(fplo<nc){
					for(k=itb; k<ite; ++k){
						datalo[k-itb]=lowpass[fplo][k];
						datahi[k-itb]=lowpass[fphi][k];
					}
					ints8r(8,dt,firstt,datalo,0.0,0.0,1,&t,&amplo);
					ints8r(8,dt,firstt,datahi,0.0,0.0,1,&t,&amphi);
					mig[iip][it] += geoms*obliq*angtaper*(wlo*amplo + whi*amphi);
				}
			}

			/* loop over output image-points to the right of the midpoint */
			for(iip=imp+1; iip<ux; ++iip){
				float ts,tr;
				int fplo=0, fphi;
				float ref,wlo,whi;

				ip=iip*dx;
				x=ip-mp;
				t0=it*dt;
				ts=sqrt( pow(t0/2,2) + pow((x+h)/v,2) );
				tr=sqrt( pow(t0/2,2) + pow((h-x)/v,2) );
				t= ts + tr;
				if(t>=tmax) break;
				geoms=sqrt(1/(t*v));
				obliq=sqrt(.5*(1 + (t0*t0/(4*ts*tr))
						- (1/(ts*tr))*sqrt(ts*ts
								- t0*t0/4)*sqrt(tr*tr
										- t0*t0/4)));
				ang=180.0*fabs(acos(t0/t))/PI;
				if(ang<=angmax) angtaper=1.0;
				if(ang>angmax) angtaper=cos((ang-angmax)*PI/20.0);

				/* Evaluate migration operator slowness p to determine the  */
				/* low-pass filtered trace for antialiasing */
				pmin=1/(2*dx*fnyq);
				p=fabs((x+h)/(pow(v,2)*ts) + (x-h)/(pow(v,2)*tr));
				if(p>0){
					fplo=floor(nc*pmin/p);
				}
				if(p==0){
					fplo=nc;
				}

				ref=fmod(nc*pmin,p);
				wlo=1-ref;
				fphi=fplo+1;
				whi=ref;
				itb=MAX(ceil(t/dt)-3,0);
				ite=MIN(itb+8,nt);
				firstt=(itb-1)*dt;

				/* Move energy from CMP to CIP */
				if(fplo>=nc){
					for(k=itb; k<ite; ++k){
						datalo[k-itb]=lowpass[nc][k];
					}
					ints8r(8,dt,firstt,datalo,0.0,0.0,1,&t,&amplo);
					mig[iip][it] +=geoms*obliq*angtaper*amplo;
				} else if(fplo<nc){
					for(k=itb; k<ite; ++k){
						datalo[k-itb]=lowpass[fplo][k];
						datahi[k-itb]=lowpass[fphi][k];
					}
					ints8r(8,dt,firstt,datalo,0.0,0.0,1,&t,&amplo);
					ints8r(8,dt,firstt,datahi,0.0,0.0,1,&t,&amphi);
					mig[iip][it] += geoms*obliq*angtaper*(wlo*amplo + whi*amphi);
				}
			}
		}

	}
}

void  suktmig2d ( float **data, int ntr, int nt, float dx, float dt, float tmax,
		int nfft, float fnyq, float h, float *fc, int nf, int nc, int nfc,
		int cdp_trace_first,float angmax, float **vel,float **mig, int dxcdp, int firstcdp)
{
	int imp, it, ifc, iip, k;
	float mp,ip;	/* mid-point and image-point coordinates */

	float t0;	/* vertical traveltime */
	float p=0.0;	/* horizontal slowness of the migration operator */
	float pmin=0.0;	/* maximum horizontal slowness for which there's */
	float ang;	/* aperture angle */
	float angtaper=0.0;	/* aperture-angle taper */
	float v;		/* velocity */
	float xmax=0.0;	/* maximum aperture distance */
	float t;	/* time */
	/* no aliasing of the operator */
	float x;	/* aperture distance */

	float obliq;	/* obliquity factor */
	float geoms;	/* geometrical spreading factor */

	float *filter=NULL;	/* array of low-pass filter values */
	float **lowpass=NULL;   /* low-pass filtered version of the trace */
	float *rtin=NULL,*rtout=NULL;/* real traces */
	complex *ct=NULL;   /* complex trace */

	float datalo[8], datahi[8];
	int itb, ite;
	float firstt, amplo, amphi;

//	fprintf(stderr, "nt=%i nc=%i nf=%i nfft=%i fnyq=%f ntr=%i dx=%f \n",
//			nt, nc, nf, nfft, fnyq, ntr, dx);
	/* Allocate space */
	lowpass = alloc2float(nt,nc+1);
	rtin = alloc1float(nfft);
	rtout = alloc1float(nfft);
	ct = alloc1complex(nf);
	filter = alloc1float(nf);

	/* Zero all arrays */
	memset((void *) mig[0], 0,nt*ntr*FSIZE);
	memset((void *) rtin, 0, nfft*FSIZE);
	memset((void *) filter, 0, nf*FSIZE);
	memset((void *) lowpass[0], 0,nt*(nc+1)*FSIZE);

	/* Start the migration process */
	/* Loop over input mid-points first */
//	if (verbose) warn("Starting migration process...\n");
	for(imp=0; imp<ntr; ++imp){
		float perc;

		mp = imp*dx;
		perc=imp*100.0/(ntr-1);
//		if(fmod(imp*100,ntr-1)==0 && verbose)
//			warn("migrated %g\n ",perc);

		/* Calculate low-pass filtered versions  */
		/* of the data to be used for antialiasing */
		for(it=0; it<nt; ++it){
			rtin[it]=data[imp][it];
		}
		for(ifc=1; ifc<nc+1; ++ifc){
			memset((void *) rtout, 0, nfft*FSIZE);
			memset((void *) ct, 0, nf*FSIZE);
			lpfilt(nfc,nfft,dt,fc[ifc],filter);
			pfarc(1,nfft,rtin,ct);

			for(it=0; it<nf; ++it){
				ct[it] = crmul(ct[it],filter[it]);
			}
			pfacr(-1,nfft,ct,rtout);
			for(it=0; it<nt; ++it){
				lowpass[ifc][it]= rtout[it];
			}
		}

		/* Loop over vertical traveltimes */
		for(it=0; it<nt; ++it){
			int lx,ux;

			t0=it*dt;
			v=vel[imp*dxcdp+cdp_trace_first-1][it];
			xmax=tan((angmax+10.0)*PI/180.0)*v*t0;
			lx=MAX(0,imp - ceil(xmax/dx));
			ux=MIN(ntr,imp + ceil(xmax/dx));

			/* loop over output image-points to the left of the midpoint */
			for(iip=imp; iip>lx; --iip){
				float ts,tr;
				int fplo=0, fphi=0;
				float ref,wlo,whi;

				ip=iip*dx;
				x=ip-mp;
				ts=sqrt( pow(t0/2,2) + pow((x+h)/v,2) );
				tr=sqrt( pow(t0/2,2) + pow((h-x)/v,2) );
				t= ts + tr;
				if(t>=tmax) break;
				geoms=sqrt(1/(t*v));
				obliq=sqrt(.5*(1 + (t0*t0/(4*ts*tr))
						- (1/(ts*tr))*sqrt(ts*ts - t0*t0/4)*sqrt(tr*tr - t0*t0/4)));
				ang=180.0*fabs(acos(t0/t))/PI;
				if(ang<=angmax) angtaper=1.0;
				if(ang>angmax) angtaper=cos((ang-angmax)*PI/20);
				/* Evaluate migration operator slowness p to determine */
				/* the low-pass filtered trace for antialiasing */
				pmin=1/(2*dx*fnyq);
				p=fabs((x+h)/(pow(v,2)*ts) + (x-h)/(pow(v,2)*tr));
				if(p>0){fplo=floor(nc*pmin/p);}
				if(p==0){fplo=nc;}
				ref=fmod(nc*pmin,p);
				wlo=1-ref;
				fphi=++fplo;
				whi=ref;
				itb=MAX(ceil(t/dt)-3,0);
				ite=MIN(itb+8,nt);
				firstt=(itb-1)*dt;
				/* Move energy from CMP to CIP */
				if(fplo>=nc){
					for(k=itb; k<ite; ++k){
						datalo[k-itb]=lowpass[nc][k];
					}
					ints8r(8,dt,firstt,datalo,0.0,0.0,1,&t,&amplo);
					mig[iip][it] +=geoms*obliq*angtaper*amplo;
				} else if(fplo<nc){
					for(k=itb; k<ite; ++k){
						datalo[k-itb]=lowpass[fplo][k];
						datahi[k-itb]=lowpass[fphi][k];
					}
					ints8r(8,dt,firstt,datalo,0.0,0.0,1,&t,&amplo);
					ints8r(8,dt,firstt,datahi,0.0,0.0,1,&t,&amphi);
					mig[iip][it] += geoms*obliq*angtaper*(wlo*amplo + whi*amphi);
				}
			}

			/* loop over output image-points to the right of the midpoint */
			for(iip=imp+1; iip<ux; ++iip){
				float ts,tr;
				int fplo=0, fphi;
				float ref,wlo,whi;

				ip=iip*dx;
				x=ip-mp;
				t0=it*dt;
				ts=sqrt( pow(t0/2,2) + pow((x+h)/v,2) );
				tr=sqrt( pow(t0/2,2) + pow((h-x)/v,2) );
				t= ts + tr;
				if(t>=tmax) break;
				geoms=sqrt(1/(t*v));
				obliq=sqrt(.5*(1 + (t0*t0/(4*ts*tr))
						- (1/(ts*tr))*sqrt(ts*ts
								- t0*t0/4)*sqrt(tr*tr
										- t0*t0/4)));
				ang=180.0*fabs(acos(t0/t))/PI;
				if(ang<=angmax) angtaper=1.0;
				if(ang>angmax) angtaper=cos((ang-angmax)*PI/20.0);

				/* Evaluate migration operator slowness p to determine the  */
				/* low-pass filtered trace for antialiasing */
				pmin=1/(2*dx*fnyq);
				p=fabs((x+h)/(pow(v,2)*ts) + (x-h)/(pow(v,2)*tr));
				if(p>0){
					fplo=floor(nc*pmin/p);
				}
				if(p==0){
					fplo=nc;
				}

				ref=fmod(nc*pmin,p);
				wlo=1-ref;
				fphi=fplo+1;
				whi=ref;
				itb=MAX(ceil(t/dt)-3,0);
				ite=MIN(itb+8,nt);
				firstt=(itb-1)*dt;

				/* Move energy from CMP to CIP */
				if(fplo>=nc){
					for(k=itb; k<ite; ++k){
						datalo[k-itb]=lowpass[nc][k];
					}
					ints8r(8,dt,firstt,datalo,0.0,0.0,1,&t,&amplo);
					mig[iip][it] +=geoms*obliq*angtaper*amplo;
				} else if(fplo<nc){
					for(k=itb; k<ite; ++k){
						datalo[k-itb]=lowpass[fplo][k];
						datahi[k-itb]=lowpass[fphi][k];
					}
					ints8r(8,dt,firstt,datalo,0.0,0.0,1,&t,&amplo);
					ints8r(8,dt,firstt,datahi,0.0,0.0,1,&t,&amphi);
					mig[iip][it] += geoms*obliq*angtaper*(wlo*amplo + whi*amphi);
				}
			}

		}
	}
	free2float(lowpass);
	free1float(rtin);
	free1float(rtout);
	free1complex(ct);
	free1float(filter);
}

void  suktmig2d_ver1 ( float **data, int ntr, int nt, float dx, float dt, float tmax,
		int nfft, float fnyq, float h, float *fc, int nf, int nc, int nfc,
		int cdp_trace_first,float angmax, float **vel,float **mig, int dxcdp, int firstcdp,
		float aprtmax)
{
	int imp, it, ifc, iip, k;
	float mp,ip;	/* mid-point and image-point coordinates */

	float t0;	/* vertical traveltime */
	float p=0.0;	/* horizontal slowness of the migration operator */
	float pmin=0.0;	/* maximum horizontal slowness for which there's */
	float ang;	/* aperture angle */
	float angtaper=0.0;	/* aperture-angle taper */
	float v;		/* velocity */
	float xmax=0.0;	/* maximum aperture distance */
	float t;	/* time */
	/* no aliasing of the operator */
	float x;	/* aperture distance */

	float obliq;	/* obliquity factor */
	float geoms;	/* geometrical spreading factor */

	float *filter=NULL;	/* array of low-pass filter values */
	float **lowpass=NULL;   /* low-pass filtered version of the trace */
	float *rtin=NULL,*rtout=NULL;/* real traces */
	complex *ct=NULL;   /* complex trace */

	float datalo[8], datahi[8];
	int itb, ite;
	float firstt, amplo, amphi;

//	fprintf(stderr, "nt=%i nc=%i nf=%i nfft=%i fnyq=%f ntr=%i dx=%f \n",
//			nt, nc, nf, nfft, fnyq, ntr, dx);
	/* Allocate space */
	lowpass = alloc2float(nt,nc+1);
	rtin = alloc1float(nfft);
	rtout = alloc1float(nfft);
	ct = alloc1complex(nf);
	filter = alloc1float(nf);

	/* Zero all arrays */
	memset((void *) mig[0], 0,nt*ntr*FSIZE);
	memset((void *) rtin, 0, nfft*FSIZE);
	memset((void *) filter, 0, nf*FSIZE);
	memset((void *) lowpass[0], 0,nt*(nc+1)*FSIZE);

	/* Start the migration process */
	/* Loop over input mid-points first */
//	if (verbose) warn("Starting migration process...\n");
	for(imp=0; imp<ntr; ++imp){
		float perc;

		mp = imp*dx;
		perc=imp*100.0/(ntr-1);
//		if(fmod(imp*100,ntr-1)==0 && verbose)
//			warn("migrated %g\n ",perc);

		/* Calculate low-pass filtered versions  */
		/* of the data to be used for antialiasing */
		for(it=0; it<nt; ++it){
			rtin[it]=data[imp][it];
		}
		for(ifc=1; ifc<nc+1; ++ifc){
			memset((void *) rtout, 0, nfft*FSIZE);
			memset((void *) ct, 0, nf*FSIZE);
			lpfilt(nfc,nfft,dt,fc[ifc],filter);
			pfarc(1,nfft,rtin,ct);

			for(it=0; it<nf; ++it){
				ct[it] = crmul(ct[it],filter[it]);
			}
			pfacr(-1,nfft,ct,rtout);
			for(it=0; it<nt; ++it){
				lowpass[ifc][it]= rtout[it];
			}
		}

		/* Loop over vertical traveltimes */
		for(it=0; it<nt; ++it){
			int lx,ux;

			t0=it*dt;
			v=vel[imp*dxcdp+cdp_trace_first-1][it];
			xmax=tan((angmax+10.0)*PI/180.0)*v*t0;

			/* add P&U */
			if ( fabs(xmax) > aprtmax ) {
				if ( xmax < 0 ) xmax = - aprtmax;
				else				 xmax = aprtmax;
			}

			lx=MAX(0,imp - ceil(xmax/dx));
			ux=MIN(ntr,imp + ceil(xmax/dx));

			/* loop over output image-points to the left of the midpoint */
			for(iip=imp; iip>lx; --iip){
				float ts,tr;
				int fplo=0, fphi=0;
				float ref,wlo,whi;

				ip=iip*dx;
				x=ip-mp;
				ts=sqrt( pow(t0/2,2) + pow((x+h)/v,2) );
				tr=sqrt( pow(t0/2,2) + pow((h-x)/v,2) );
				t= ts + tr;
				if(t>=tmax) break;
				geoms=sqrt(1/(t*v));
				obliq=sqrt(.5*(1 + (t0*t0/(4*ts*tr))
						- (1/(ts*tr))*sqrt(ts*ts - t0*t0/4)*sqrt(tr*tr - t0*t0/4)));
				ang=180.0*fabs(acos(t0/t))/PI;
				if(ang<=angmax) angtaper=1.0;
				if(ang>angmax) angtaper=cos((ang-angmax)*PI/20);
				/* Evaluate migration operator slowness p to determine */
				/* the low-pass filtered trace for antialiasing */
				pmin=1/(2*dx*fnyq);
				p=fabs((x+h)/(pow(v,2)*ts) + (x-h)/(pow(v,2)*tr));
				if(p>0){fplo=floor(nc*pmin/p);}
				if(p==0){fplo=nc;}
				ref=fmod(nc*pmin,p);
				wlo=1-ref;
				fphi=++fplo;
				whi=ref;
				itb=MAX(ceil(t/dt)-3,0);
				ite=MIN(itb+8,nt);
				firstt=(itb-1)*dt;
				/* Move energy from CMP to CIP */
				if(fplo>=nc){
					for(k=itb; k<ite; ++k){
						datalo[k-itb]=lowpass[nc][k];
					}
					ints8r(8,dt,firstt,datalo,0.0,0.0,1,&t,&amplo);
					mig[iip][it] +=geoms*obliq*angtaper*amplo;
				} else if(fplo<nc){
					for(k=itb; k<ite; ++k){
						datalo[k-itb]=lowpass[fplo][k];
						datahi[k-itb]=lowpass[fphi][k];
					}
					ints8r(8,dt,firstt,datalo,0.0,0.0,1,&t,&amplo);
					ints8r(8,dt,firstt,datahi,0.0,0.0,1,&t,&amphi);
					mig[iip][it] += geoms*obliq*angtaper*(wlo*amplo + whi*amphi);
				}
			}

			/* loop over output image-points to the right of the midpoint */
			for(iip=imp+1; iip<ux; ++iip){
				float ts,tr;
				int fplo=0, fphi;
				float ref,wlo,whi;

				ip=iip*dx;
				x=ip-mp;
				t0=it*dt;
				ts=sqrt( pow(t0/2,2) + pow((x+h)/v,2) );
				tr=sqrt( pow(t0/2,2) + pow((h-x)/v,2) );
				t= ts + tr;
				if(t>=tmax) break;
				geoms=sqrt(1/(t*v));
				obliq=sqrt(.5*(1 + (t0*t0/(4*ts*tr))
						- (1/(ts*tr))*sqrt(ts*ts
								- t0*t0/4)*sqrt(tr*tr
										- t0*t0/4)));
				ang=180.0*fabs(acos(t0/t))/PI;
				if(ang<=angmax) angtaper=1.0;
				if(ang>angmax) angtaper=cos((ang-angmax)*PI/20.0);

				/* Evaluate migration operator slowness p to determine the  */
				/* low-pass filtered trace for antialiasing */
				pmin=1/(2*dx*fnyq);
				p=fabs((x+h)/(pow(v,2)*ts) + (x-h)/(pow(v,2)*tr));
				if(p>0){
					fplo=floor(nc*pmin/p);
				}
				if(p==0){
					fplo=nc;
				}

				ref=fmod(nc*pmin,p);
				wlo=1-ref;
				fphi=fplo+1;
				whi=ref;
				itb=MAX(ceil(t/dt)-3,0);
				ite=MIN(itb+8,nt);
				firstt=(itb-1)*dt;

				/* Move energy from CMP to CIP */
				if(fplo>=nc){
					for(k=itb; k<ite; ++k){
						datalo[k-itb]=lowpass[nc][k];
					}
					ints8r(8,dt,firstt,datalo,0.0,0.0,1,&t,&amplo);
					mig[iip][it] +=geoms*obliq*angtaper*amplo;
				} else if(fplo<nc){
					for(k=itb; k<ite; ++k){
						datalo[k-itb]=lowpass[fplo][k];
						datahi[k-itb]=lowpass[fphi][k];
					}
					ints8r(8,dt,firstt,datalo,0.0,0.0,1,&t,&amplo);
					ints8r(8,dt,firstt,datahi,0.0,0.0,1,&t,&amphi);
					mig[iip][it] += geoms*obliq*angtaper*(wlo*amplo + whi*amphi);
				}
			}

		}
	}
	free2float(lowpass);
	free1float(rtin);
	free1float(rtout);
	free1complex(ct);
	free1float(filter);
}

void lpfilt(int nfc, int nfft, float dt, float fhi, float *filter)
/*******************************************************************************
lpfilt -- low-pass filter using Lanczos Smoothing
	(R.W. Hamming:"Digital Filtering",1977)
****************************************************************************
Input:
nfc	number of Fourier coefficients to approximate ideal filter
nfft	number of points in the fft
dt	time sampling interval
fhi	cut-frequency

Output:
filter  array[nf] of filter values
*****************************************************************************
Notes: Filter is to be applied in the frequency domain
*****************************************************************************
Author: CWP: Carlos Pacheco   2006
*****************************************************************************/
{
	int i,j;  /* counters */
	int nf;   /* Number of frequencies (including Nyquist) */
	float onfft;  /* reciprocal of nfft */
	float fn; /* Nyquist frequency */
	float df; /* frequency interval */
	float dw; /* frequency interval in radians */
	float whi;/* cut-frequency in radians */
	float w;  /* radian frequency */

	nf= nfft/2 + 1;
	onfft=1.0/nfft;
	fn=1.0/(2*dt);
	df=onfft/dt;
	whi=fhi*PI/fn;
	dw=df*PI/fn;

	for(i=0; i<nf; ++i){
		filter[i]= whi/PI;
		w=i*dw;

		for(j=1; j<nfc; ++j){
			float c= sin(whi*j)*sin(PI*j/nfc)*2*nfc/(PI*PI*j*j);
			filter[i] +=c*cos(j*w);
		}
	}
}
