/*
 * wpar_cadzow3d_v3.c
 *
 *  Created on: Apr 15, 2013
 *      Author: toto
 */

#include "../src_lib/segy_lib.h"
#include <time.h>
#include <stdbool.h>
#include "../src_lib/printLib.h"
#include "../src_lib/binaryLib.h"
#define SCANHEADER 5
#define SCANPRIMARY 3
#define WINDOW_LEN 4

char *sdoc[] = {
		"									",
		" WAVE_CADZOW3D.v3.0 = WAVE_CADZOW3D",
		" inp=                 (SEGY input)",
		" out=                 (SEGY output)",
		NULL};


int numprocs, rankmpi, namelen;
//char processor_name[MPI_MAX_PROCESSOR_NAME];

//global variable
char *ccinp=NULL, *ccout=NULL;
int endian, format;
int ntrc, nsp, nsegy;
float dt;

//temporary header
int **scanHeader;
int **windowPosition;
char *binposfile;

//data
int nLoop;
int nThirdHeader;
int *tracestag;
float **traces;
char **headers;

//temporary variable
int bsaveall;
int vblock;
int thirdmin, thirdmax, thirdspacing;
int spanfilter, spansize;
int maxPrimaryHeaderCount;
int nWindowCount;
int idx_primary, idx_secondary, idx_third;

//segy additional information
int nPrimary;
int nSecondaryInSegy, nSecondary;
int totalSecondary;
int sgyMinThird, sgyMaxThird;
int sgyMinSecondary, sgyMaxSecondary;
int sgyMinPrimary, sgyMaxPrimary;

void computeNumberTraceInEnsemble()
{
	if(thirdmin>=thirdmax)
		err("thirdmin>=thirdmax");

	nThirdHeader = ((thirdmax-thirdmin)/thirdspacing) + 1;
}

void fillWindowPost(int idx, int p1, int p2, int s1, int s2)
{
	windowPosition[idx][0] = p1;
	windowPosition[idx][1] = p2;
	windowPosition[idx][2] = s1;
	windowPosition[idx][3] = s2;
}

void creatingWindowingData(bool istest, int nlen, int *ntest)
{
	int i, j;
	int firstkey, lastkey;
	int jump, begdata, enddata;
	int ibeg, iend, ff, fl;
	int idx1, idx2;
	int max_primary;
	int itest;

	if(istest) max_primary = 1;
	else
	{
		max_primary = nPrimary;
		windowPosition = alloc2int(WINDOW_LEN, nlen);
	}

	firstkey = 0;
	itest = 0;
	for(i=0; i<max_primary; i++)	//loop over inline
	{
		lastkey = firstkey+nSecondary; //get the number of cdp in current line

		//read the first windowing data
		jump = (spanfilter*2) + 1;
		begdata = firstkey + spansize*2;
		enddata = lastkey - spansize;
		ff = begdata + spansize;
		fl = begdata - spanfilter - 1;

		if(ff>=lastkey)
			err("spansize[%i] is too big", spansize);
		idx1 = firstkey;
		idx2 = fl;
		if(!istest)
			fillWindowPost(itest, firstkey, ff, idx1, idx2);

		//printf("[%i - %i] -> [%i - %i] \n", firstkey, ff, idx1, idx2);
		itest++;

		//loop over cdp
		for ( j=begdata; j<enddata; j=j+jump )
		{
			ibeg = j-spansize;
			iend = j+spansize;
			ff = j-spanfilter;
			fl = j+spanfilter;
			idx1 = ff;
			idx2 = fl;
			if(!istest)
				fillWindowPost(itest, ibeg, iend, idx1, idx2);
			//printf("[%i - %i] -> [%i - %i] \n", ibeg, iend, idx1, idx2);
			itest++;
		}

		//read the last windowing data
		ff = j-spansize;
		fl = fl + 1;
		idx1 = fl;
		idx2 = lastkey-1;
		if(!istest)
			fillWindowPost(itest, ff, idx2, idx1, idx2);
		//printf("[%i - %i] -> [%i - %i] \n", ff, idx2, idx1, idx2);

		itest++;
		firstkey = lastkey;
	}

	(*ntest) = itest;
}

void printScanHeader()
{
	printf("\n\n");
	printf(" Pos File             : %s \n", binposfile);
	printf(" ntrc                 = %i \n", ntrc);
	printf(" dt                   = %f \n", dt);
	printf(" nsp                  = %i \n", nsp);
	printf(" nSecondaryInSegy     = %i \n", nSecondaryInSegy);
	printf(" nPrimary             = %i \n", nPrimary);
	printf(" nSecondary           = %i \n\n", nSecondary);

	printf(" Min Third Header     = %i \n", sgyMinThird);
	printf(" Max Third Header     = %i \n", sgyMaxThird);
	printf(" Min Primary Header   = %i \n", sgyMinPrimary);
	printf(" Max Primary Header   = %i \n", sgyMaxPrimary);
	printf(" Min Secondary Header = %i \n", sgyMinSecondary);
	printf(" Max Secondary Header = %i \n\n", sgyMaxSecondary);
}

void writeScanSegyHeader()
{
	FILE *fod=NULL;
	int i;

	fod = openFile(binposfile, "w");
	fwrite(&format, sizeof(int), 1, fod); //ntrc
	fwrite(&ntrc, sizeof(int), 1, fod); //ntrc
	fwrite(&dt, sizeof(float), 1, fod); //dt
	fwrite(&nsp, sizeof(int), 1, fod); //nsp
	fwrite(&nsegy, sizeof(int), 1, fod); //nsegy
	fwrite(&nSecondaryInSegy, sizeof(int), 1, fod); //nSecondary
	fwrite(&nPrimary, sizeof(int), 1, fod); //ninline
	fwrite(&nSecondary, sizeof(int), 1, fod); //nSecondary

	fwrite(&sgyMinThird, sizeof(int), 1, fod);
	fwrite(&sgyMaxThird, sizeof(int), 1, fod);
	fwrite(&sgyMinPrimary, sizeof(int), 1, fod);
	fwrite(&sgyMaxPrimary, sizeof(int), 1, fod);
	fwrite(&sgyMinSecondary, sizeof(int), 1, fod);
	fwrite(&sgyMaxSecondary, sizeof(int), 1, fod);

	//write scan header info data
	totalSecondary = nPrimary*nSecondary;
	for (i=0; i<totalSecondary; i++)
		fwrite(scanHeader[i], sizeof(int), SCANHEADER, fod);

	fclose(fod);

	printScanHeader();
}

void readScanSegyHeader(int bread)
{
	FILE *fid=NULL;
	int i;

	fid = openFile(binposfile, "r");
	fread(&format, sizeof(int), 1, fid); //ntrc
	fread(&ntrc, sizeof(int), 1, fid); //ntrc
	fread(&dt, sizeof(float), 1, fid); //dt
	fread(&nsp, sizeof(int), 1, fid); //nsp
	fread(&nsegy, sizeof(int), 1, fid); //nsegy
	fread(&nSecondaryInSegy, sizeof(int), 1, fid); //nSecondary
	fread(&nPrimary, sizeof(int), 1, fid); //ninline
	fread(&nSecondary, sizeof(int), 1, fid); //nSecondary

	fread(&sgyMinThird, sizeof(int), 1, fid);
	fread(&sgyMaxThird, sizeof(int), 1, fid);
	fread(&sgyMinPrimary, sizeof(int), 1, fid);
	fread(&sgyMaxPrimary, sizeof(int), 1, fid);
	fread(&sgyMinSecondary, sizeof(int), 1, fid);
	fread(&sgyMaxSecondary, sizeof(int), 1, fid);

	if(bread)
	{
		totalSecondary = nPrimary*nSecondary;
		scanHeader = alloc2int(SCANHEADER, totalSecondary);

		//write scan header info data
		for (i=0; i<totalSecondary; i++)
			fread(scanHeader[i], sizeof(int), SCANHEADER, fid);
	}
	fclose(fid);
}

int getSecondaryHeaderIndex(int primaryValue, int secondaryValue, int curr_index)
{
	int idx;
	int curPrimary;
	int curSecondary;

	idx = curr_index;
	while(true)
	{
		idx++;
		curPrimary = scanHeader[idx][0];
		curSecondary = scanHeader[idx][1];

		if( (curPrimary==primaryValue) && (curSecondary==secondaryValue) )
			break;

		if(idx>totalSecondary)
			err("Error detecting scanHeader Position");
	}
	return(idx);
}

void scanHeaderSegy(char *ccinp, int idxPrimary, int idxSecondary, int idxThird)
{
	int i, j, idx;
	FILE *fid=NULL;
	FILE *fod=NULL;
	char cebcid[3200];
	bhed bh;
	int result;
	int itr;
	unsigned char *buffer;
	char *tmpbuf;
	int *tmpPrimary;
	int itrPrim, itrSec;

	int oldPrimary, curPrimary;
	int oldSecondary, curSecondary, ithirdheader;
	int minPrimary, maxPrimary;
	int minSecondary, maxSecondary;
	int possecond1=0, possecond2=0;
	int tmpi[2];

	fid = openFile(ccinp, "r");
	readEbcdicHeader(fid, cebcid); /* read ebcdic header */
	readBinaryHeader(fid, endian, &bh, &nsegy); /*read binary header */
	ntrc = getNumberOfTraceSegyFile(fid, nsegy);
	dt = bh.hdt/1000000.0;
	nsp = bh.hns;
	format = bh.format;

	//allocate data
	buffer = (unsigned char*) calloc(nsegy, 1);
	tmpbuf = createAutoFileNameWithDir("/tmp"); 	//create temporary file

	//initialization data
	nPrimary = 0;
	nSecondaryInSegy = 0;
	oldPrimary = -1;
	oldSecondary = -1;
	minPrimary = -1;
	maxPrimary = -1;
	minSecondary = -1;
	maxSecondary = -1;

	//---------------- compute nSecondaryInSegy -------------------

	fod = openFile(tmpbuf, "w");

	//read first trace
	result = fread(buffer, sizeof(char), nsegy, fid);
	oldPrimary = uchar2int(buffer, idxPrimary, 4, endian);
	oldSecondary = uchar2int(buffer, idxSecondary, 4, endian);
	ithirdheader = uchar2int(buffer, idxThird, 4, endian);
	tmpi[0] = oldPrimary; tmpi[1] = oldSecondary;
	fwrite(tmpi, sizeof(int), 2, fod);

	//get minimum and maximum data
	sgyMinPrimary = oldPrimary;
	sgyMaxPrimary = oldPrimary;
	sgyMinSecondary = oldSecondary;
	sgyMaxSecondary = oldSecondary;
	sgyMinThird = ithirdheader;
	sgyMaxThird = ithirdheader;

	//allocate temporary inline data
	tmpPrimary = (int*) calloc(maxPrimaryHeaderCount, sizeof(int));
	tmpPrimary[nPrimary] = oldPrimary;
	for(i=1; i<ntrc; i++)
	{
		if(i%vblock==0)
			printf("Read Segy %i / %i \n", i, ntrc);

		result = fread(buffer, sizeof(char), nsegy, fid); //read segy

		//read primary, secondary and third header
		curPrimary = uchar2int(buffer, idxPrimary, 4, endian);
		curSecondary = uchar2int(buffer, idxSecondary, 4, endian);
		ithirdheader = uchar2int(buffer, idxThird, 4, endian);

		//set minimum and maximum segy info
		if(ithirdheader<sgyMinThird) sgyMinThird = ithirdheader;
		if(ithirdheader>sgyMaxThird) sgyMaxThird = ithirdheader;
		if(curPrimary<sgyMinPrimary) sgyMinPrimary = curPrimary;
		if(curPrimary>sgyMaxPrimary) sgyMaxPrimary = curPrimary;
		if(curSecondary<sgyMinSecondary) sgyMinSecondary = curSecondary;
		if(curSecondary>sgyMaxSecondary) sgyMaxSecondary = curSecondary;

		if(oldPrimary!=curPrimary)
		{
			nPrimary++;
			oldPrimary = curPrimary;
			tmpPrimary[nPrimary] = oldPrimary;
		}
		else
			if(oldSecondary!=curSecondary)
				nSecondaryInSegy++;

		//write inline and cdp to temporary file
		tmpi[0] = curPrimary;
		tmpi[1] = curSecondary;
		fwrite(tmpi, sizeof(int), 2, fod);
	}
	nPrimary++; //total number of primary header
	fclose(fod);
	fclose(fid);
	printf("\n");

	//----------- allocate scanHeader and scanPrimary data -----------
	nSecondary = sgyMaxSecondary-sgyMinSecondary+1;
	totalSecondary = nPrimary*nSecondary;

	//	scanPrimary = alloc2int(SCANPRIMARY, nPrimary);
	scanHeader = alloc2int(SCANHEADER, totalSecondary);
	//	memset(scanPrimary[0], 0, nPrimary*SCANPRIMARY*sizeof(int));
	memset(scanHeader[0], 0, totalSecondary*SCANHEADER*sizeof(int));

	//fill default value for scanHeader data
	idx = 0;
	for(i=0; i<nPrimary; i++)
	{
		for(j=0; j<nSecondary; j++)
		{
			scanHeader[idx][0] = tmpPrimary[i];
			scanHeader[idx][1] = sgyMinSecondary+j;
			idx++;
		}
	}

	//fill scanHeader data using temporary file
	fid = openFile(tmpbuf, "r");

	//read the first trace
	fread(tmpi, sizeof(int), 2, fid);
	oldPrimary = tmpi[0];
	oldSecondary = tmpi[1];
	itr = 1;
	itrPrim = 0;
	itrSec = -1;
	for(i=1; i<ntrc; i++)
	{
		if(i%vblock==0)
			printf("Compare Header %i / %i \n", i, ntrc);

		result = fread(tmpi, sizeof(int), 2, fid);

		//get current inline and cdp value
		curPrimary = tmpi[0];
		curSecondary = tmpi[1];

		if(oldPrimary!=curPrimary)
		{
			itrSec = getSecondaryHeaderIndex(oldPrimary, oldSecondary, itrSec);

			scanHeader[itrSec][2] = possecond1;
			scanHeader[itrSec][3] = possecond2;
			scanHeader[itrSec][4] = itr;

			itrPrim++;
			oldPrimary = curPrimary;
			oldSecondary = curSecondary;
			itr = 0;
			possecond1 = i;
			possecond2 = i;
		}
		else
		{
			//cdp sama
			if(oldSecondary!=curSecondary)
			{
				itrSec = getSecondaryHeaderIndex(oldPrimary, oldSecondary, itrSec);

				//fill scanHeader
				scanHeader[itrSec][2] = possecond1;
				scanHeader[itrSec][3] = possecond2;
				scanHeader[itrSec][4] = itr;

				oldSecondary = curSecondary;
				itr = 0;
				possecond1 = i;
				possecond2 = i;
			}

			//different secondary header
			else
			{
				possecond2 = i;
			}
		}

		itr++;
	}

	//set the last traces
	itrSec = getSecondaryHeaderIndex(oldPrimary, oldSecondary, itrSec);
	scanHeader[itrSec][2] = possecond1;
	scanHeader[itrSec][3] = possecond2;
	scanHeader[itrSec][4] = itr;
	fclose(fid);

	writeScanSegyHeader();

	//	print2int(scanPrimary, nPrimary, SCANPRIMARY, 0, 0);
	//	print2int(scanHeader, totalSecondary, SCANHEADER, 0, 0);

	//remove temporary file
	remove(tmpbuf);

	free(tmpPrimary);
	free(buffer);
	free(tmpbuf);
}

void readSegyInGrouph(FILE *fid, FILE *fod, int pos1, int pos2)
{
	int i, j;
	int firsttr;
	int curntrc;
	unsigned char *tmpdata;
	float *data;
	char *header;
	int ithirdheader;
	int idxtr, widxtr;
	int idxs1, idxs2;
	int tmpnsegy;

	tmpnsegy = nsp*4;
	tmpdata = (unsigned char*) calloc(tmpnsegy, sizeof(unsigned char));
	data = alloc1float(nsp);
	header = (char*) calloc(SEGY_HDRBYTES, 1);

	firsttr = scanHeader[windowPosition[pos1][0]][2];
	gotoTraceSegyPosition(fid, firsttr, nsegy);

	idxs1 = windowPosition[pos1][0];
	idxs2 = windowPosition[pos2][1];
	for(i=idxs1; i<=idxs2; i++) //scan data by secondary header
	{
		curntrc = scanHeader[i][4];
		for(j=0; j<curntrc; j++)
		{
			readSegy(fid, endian, nsp, format, nsegy, &tr, header, data);

			//get third header
			ithirdheader = char2int( header, idx_third, 4, !endian);
			idxtr = roundfunc((ithirdheader-sgyMinThird)/thirdspacing);

			//third header position more than offset input
			if(idxtr>=nThirdHeader)
				idxtr = nThirdHeader-1;

			//third header position less than offset input
			if(idxtr<0)
				idxtr = 0;

			widxtr = (i-idxs1)*nThirdHeader + idxtr;
			tracestag[widxtr] = 1;
			memcpy(headers[widxtr], header, SEGY_HDRBYTES*sizeof(char));
			memcpy(traces[widxtr], data, nsp*sizeof(float));
		}
	}

	free(tmpdata);
	free1float(data);
	free(header);
}

void writeSegyInGrouph(FILE *fod, int pos1, int pos2)
{
	int i, j;
	int idx;
	int idxs0, idxs1, idxs2;
//	int pos3, pos4;

	idxs0 = windowPosition[pos1][0];
	idxs1 = windowPosition[pos1][2];
	idxs2 = windowPosition[pos2][3];

	idx = (idxs1-idxs0)*nThirdHeader;
//	fprintf(stderr, "%i %i %i %i idx=%i \n", idxs0, idxs1, idxs2, nThirdHeader, idx);
	for(i=idxs1; i<=idxs2; i++)
	{
		for(j=0; j<nThirdHeader; j++)
		{
//			if(bsaveall==1) )
//			{
//
//			}
//			else
			if (tracestag[idx]==1)
			{
				writeSegy(fod, endian, &tr, nsegy, nsp, headers[idx], traces[idx]);
			}
			idx++;
		}
	}
}

void runParallel()
{
	int i;
	int result;
	int ntest = 0;
	int pos1, pos2;
	int inumprocs;
	int trInGrouph;
	FILE *fid, *fod;
	char sgyhdrbuf[3600];

	readScanSegyHeader(1);
	creatingWindowingData(true, 1, &ntest);
	nWindowCount = ntest*nPrimary;
	creatingWindowingData(false, nWindowCount, &ntest);
	computeNumberTraceInEnsemble();
	inumprocs = numprocs-1;

	//open segy file
	fid = openFile(ccinp, "r");
	fod = openFile(ccout, "w");
	result = fread(sgyhdrbuf, 1, 3600, fid);
	result = fwrite(sgyhdrbuf, 1, 3600, fod);

	nLoop = (int) (ceil( ((double)nWindowCount) / ((double)inumprocs)) );

	for(i=0; i<nLoop; i++)
	{
		pos1 = i*inumprocs;
		pos2 = pos1+inumprocs-1;
		if(pos2>=nWindowCount) pos2 = nWindowCount-1;

		trInGrouph = nThirdHeader*inumprocs*(windowPosition[pos2][1]+windowPosition[pos1][0]+1);

		//allocate data
		traces = alloc2float(nsp, trInGrouph);
		headers = alloc2char(SEGY_HDRBYTES, trInGrouph);
		tracestag = alloc1int(trInGrouph);
		memset(traces[0], 0, nsp*trInGrouph*sizeof(float));
		memset(headers[0], 0, SEGY_HDRBYTES*trInGrouph*sizeof(char));
		memset(tracestag, 0, trInGrouph*sizeof(int));
		fprintf(stderr, "Loop [%i / %i] --> Read [%i - %i] Save[%i - %i] \n",
				i+1, nLoop, windowPosition[pos1][0], windowPosition[pos2][1],
				windowPosition[pos1][2], windowPosition[pos2][3]);

		//read segy
		readSegyInGrouph(fid, fod, pos1, pos2);

		//process

		//save
		writeSegyInGrouph(fod, pos1, pos2);
		free2float(traces);
		free2char(headers);
		free1int(tracestag);
	}
	fclose(fid);
	fclose(fod);
}

int main(int argc, char **argv)
{
	int iproc;

	/* hook up getpar to handle the parameters */
	initargs(argc,argv);
	requestdoc(1);

	if (!getparint("numprocs",&numprocs)) numprocs=2;

	MUSTGETPARSTRING("inp",  &ccinp);
	MUSTGETPARSTRING("out",  &ccout);
	MUSTGETPARSTRING("posfile",  &binposfile);
	if (!getparint("endian",&endian)) endian=0;
	if (!getparint("vblock",&vblock)) vblock=10000;
	if (!getparint("bsaveall",&bsaveall)) bsaveall=0;

	if (!getparint("iprimary",&idx_primary)) idx_primary=9;
	if (!getparint("isecondary",&idx_secondary)) idx_secondary=13;
	if (!getparint("ithird",&idx_third)) idx_third=37;
	if (!getparint("proc",&iproc)) iproc=0;
	if (!getparint("totalprimary",&maxPrimaryHeaderCount)) maxPrimaryHeaderCount=10000;

	if (!getparint("spansize",&spansize)) spansize=5;
	if (!getparint("spanfilter",&spanfilter)) spanfilter=2;

	if (!getparint("thirdmin",&thirdmin)) thirdmin=0;
	if (!getparint("thirdmax",&thirdmax)) thirdmax=0;
	if (!getparint("thirdspacing",&thirdspacing)) thirdspacing=0;

	if(spanfilter>=spansize)
		err("spanfilter must be less than spansize");

	if(iproc==0)
	{
		scanHeaderSegy(ccinp, idx_primary, idx_secondary, idx_third);

		//		free2int(scanPrimary);
		free2int(scanHeader);
		return(0);
	}
	else if(iproc==1)
	{
		readScanSegyHeader(0);
		printScanHeader();
		return(0);
	}

	if(thirdmin<=0 || thirdmax<=0 || thirdspacing<=0)
		err("thirdmin<=0, thirdmax<=0, thirdspacing<=0");

	runParallel();

	free2int(windowPosition);
	free2int(scanHeader);
	return(0);
}
