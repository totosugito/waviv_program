/*
 * wpar_swt.c
 *
 *  Created on: Dec 14, 2012
 *      Author: toto
 */

#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <mpi.h>
#include "../src_lib/swt.h"

/*********************** self documentation *****************************/
//seleksi nilai koefisien details yang akan dilekage dengan perbandingan stgh nilai level dibawahnya
char *sdoc[] = {
		"									",
		" WPAR_SWT.V1.0",
		"",
		" Usage:",
		" mpirun -np [NUMBEROFPROCESS] -hostfile [HOSTFILE]",
		"	",
		" Required Parameters:	",
		" ftype=0           0=SU file, 1=SEGY file                               	",
		" endian=0          set =0 for little-endian machines(PC's,DEC,etc.)",
		" leakage1=3        leakage data until 'leakage1-1'",
		" leakage2=5        leakage data from 'leakage2'",
		" filterid=1        1=daubechies1",
		" treshold=0.5      ",
		" ntreshold=3       threshold until 'details level=nthreshold+1'",
		" level=7           number of decomposition sinyal",
		" invlevel=3        get inverse details from 'invlevel'",
		" vblock=100        show verbose every 'vblock'",
		"",
		" How to use :",
		" wpar_swt.v1.0 leakage1=3 leakage2=5 filterid=1 threshold=0.5 level=7 invlevel=3 <input >output",
		"",
		NULL};

int main(int argc, char **argv)
{
	int ftype;
	int endian;
	int nsegy;
	int ntrc=0;
	int format=0;
	int nsp;
	float dt;


	int numprocs, rankmpi, namelen;
	char processor_name[MPI_MAX_PROCESSOR_NAME];
	MPI_Status status;
	int nloop;
	int nread_traces, pos_tr1, pos_tr2;
	int itag;

	char *filenameinput=NULL;
	char *filenameoutput=NULL;
	FILE *inputfile = NULL;
	FILE *outfile = NULL;

	//swt variable
	int _leakage2, _leakage1;
	int _inv_level;
	int _ntreshold;
	float _treshold;
	int _level_scale;
	int _filter_id;

	int i, itr;
	char ebcdic[3200];
	bhed bh;

	float **traces=NULL;
	char **headers=NULL;
	float *trace, *tresult;
	int vblock;

	div_t divresult;
	time_t t1,t2;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
	MPI_Comm_rank(MPI_COMM_WORLD, &rankmpi);
	MPI_Get_processor_name(processor_name, &namelen);

	initargs(argc,argv);
	requestdoc(1);

	MUSTGETPARSTRING("inp", &filenameinput);
	MUSTGETPARSTRING("out", &filenameoutput);
	if (!getparint("ftype",&ftype)) ftype=0;
	if (!getparint("endian",&endian)) endian=0;
	if (!getparint("vblock",&vblock)) vblock=100;
	if (!getparint("leakage1",&_leakage1)) _leakage1=3;
	if (!getparint("leakage2",&_leakage2)) _leakage2=5;
	if((_leakage2<_leakage1) || (_leakage2<=0) || (_leakage1<=1))
		err("[leakage2 < leakage1] or [leakage2<=0] or [leakage1<=1]!!");

	if (!getparint("filterid",&_filter_id)) _filter_id=1;
	if (!getparfloat("treshold",&_treshold)) _treshold=0.5;
	if (!getparint("ntreshold",&_ntreshold)) _ntreshold=3;
	if (!getparint("level",&_level_scale)) _level_scale=7;
	if (!getparint("invlevel",&_inv_level)) _inv_level=3;
	if(_filter_id<0) _filter_id=1;


	t1 = time(NULL);
	/* -----------------------------------------------
	 * MPI INITIALIZATION
	 -------------------------------------------------*/
	MPI_Barrier(MPI_COMM_WORLD); //01
	if (rankmpi == 0)
	{
		//read segy header
		inputfile = fopen(filenameinput,"r");
		if(ftype)
		{
			readEbcdicHeader(inputfile, ebcdic);
			readBinaryHeader(inputfile, endian, &bh, &nsegy);

			ntrc = getNumberOfTraceSegyFile(inputfile, nsegy);
			nsp = bh.hns;
			format = bh.format;
			dt = (float) (bh.hdt/1000000.0);
		}
		else
		{
			fgettr(inputfile, &tr);
			nsp = tr.ns;
			dt = (float) (tr.dt/1000000.0);
			ntrc = getNumberOfTraceSuFile(inputfile);
			nsegy = nsp*4+240;
		}

		//write segy header
		outfile = fopen(filenameoutput, "w");
		if(ftype){
			writeEbcdicHeader(outfile, ebcdic);
			writeBinaryHeader(outfile, endian, &bh);
			gotoTraceSegyPosition(inputfile, 0, nsegy);
		}

		//get number of MPI loop
		nloop = (int) (ceil(ntrc/(numprocs-1)));

		//allocate data
		traces = alloc2float(nsp, (numprocs-1));
		headers = alloc2char(HDRBYTES, (numprocs-1));

		fprintf(stderr, "\n");
		fprintf(stderr, "Input File       : %s \n", filenameinput);
		fprintf(stderr, "Output File      : %s \n", filenameinput);
		fprintf(stderr, "nsp       : %i \n", nsp);
		fprintf(stderr, "ntrc      : %i \n", ntrc);
		fprintf(stderr, "nloop     : %i \n", nloop);
		fprintf(stderr, "\n");
	}
	MPI_Barrier(MPI_COMM_WORLD); //01

	/* -----------------------------------------------
	 * MPI BROADCAST DATA
	-------------------------------------------------*/
	MPI_Barrier(MPI_COMM_WORLD); //02
	MPI_Bcast(&nloop, 1, MPI_INT, 0, MPI_COMM_WORLD); //number of mpi loop
	MPI_Bcast(&nsp, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&_leakage1, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&_leakage2, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&_filter_id, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&_ntreshold, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&_treshold, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&_level_scale, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&_inv_level, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD); //02

	for(itr=0; itr<nloop; itr++)
	{
		MPI_Barrier(MPI_COMM_WORLD); //03

		if (rankmpi == 0)
		{
			//read traces
			nread_traces = 0;
			pos_tr1 = itr*(numprocs-1);
			pos_tr2 = pos_tr1;
			for (i=0; i<numprocs-1; i++)
			{
				if(ftype) readTraceSegy(inputfile, endian, nsp, format, nsegy, &tr);
				else fgettr(inputfile, &tr);

				memcpy(headers[i], (char*)&tr, HDRBYTES*sizeof(char));
				memcpy(traces[i], &tr.data, nsp*sizeof(float));

				nread_traces++;
				pos_tr2++;
				if(pos_tr2==ntrc)
					break;
			}

			// ------------- send data to every processor -------------
			fprintf(stderr, "RUN loop [%i / %i] --> traces [%i - %i] \n",
					itr+1, nloop, pos_tr1, pos_tr2);
			for (i = 1; i < numprocs; i++)
			{
				if (i <= nread_traces)
				{
					itag = 1;
					MPI_Send(&itag, 1, MPI_INT, i, 1, MPI_COMM_WORLD); //send tag identifier
					MPI_Send(traces[i-1], nsp, MPI_FLOAT, i, 1, MPI_COMM_WORLD);	//send trace data
				}
				else
				{
					itag = 0;
					MPI_Send(&itag, 1, MPI_INT, i, 1, MPI_COMM_WORLD); //send tag identifier
				}
			}

			//--------------- receive data from every processor -------------
			for(i = 1; i <= nread_traces; i++)
				MPI_Recv(traces[i-1], nsp, MPI_FLOAT, i, 1, MPI_COMM_WORLD, &status);

			//--------------- save data -------------
			for (i = 0; i < nread_traces; i++)
			{
				memcpy((char*)&tr, headers[i], HDRBYTES*sizeof(char));
				memcpy(&tr.data, traces[i], nsp*sizeof(float));

				if(ftype) writeTraceSegy(outfile, endian, &tr, nsegy, nsp);
				else fputtr(outfile, &tr);
			}
		}
		else
		{
			MPI_Recv(&itag, 1, MPI_INT, 0, 1, MPI_COMM_WORLD, &status);	//get tag

			if (itag == 1)
			{
				trace = alloc1float(nsp);

				//receive data from master
				MPI_Recv(trace, nsp, MPI_FLOAT, 0, 1, MPI_COMM_WORLD, &status);

				//process swt
				tresult = run_swt_v1(trace, nsp, _filter_id, _level_scale, _treshold, _ntreshold,
						_leakage1, _leakage2, _inv_level/*, &newNt*/);

				//send data to master
				MPI_Send(tresult, nsp, MPI_FLOAT, 0, 1, MPI_COMM_WORLD);

				free1float(trace);
				free1float(tresult);
			}
		}
		MPI_Barrier(MPI_COMM_WORLD); //03
	}

	MPI_Barrier(MPI_COMM_WORLD); //04
	if (rankmpi == 0)
	{
		free2float(traces);
		free2char(headers);

		fclose(outfile);
		fclose(inputfile);

		fprintf(stderr, "WPAR_SWT COMPLETE!\n");

		t2 = time(NULL);
		divresult = div (t2-t1, 60);
		fprintf (stderr, "Process time = %d min %d sec\n\n", divresult.quot, divresult.rem);
	}

	MPI_Barrier(MPI_COMM_WORLD);

	MPI_Finalize();

	return(0);
}



