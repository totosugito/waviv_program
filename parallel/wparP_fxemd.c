/*
 * wparP_ced.c
 *
 *  Created on: Dec 18, 2012
 *      Author: toto
 */
#include "../emd/emd.h"
#include "../src_lib/segy_lib.h"
#include "../src_lib/binaryLib.h"
#include <time.h>
#include <mpi.h>

const char *sdoc[] = {
		"",
		" WPARP_FXEMD.V.1 : PARALLEL PROMAX ADDONS FXEMD ",
		"",
		" Usage:",
		" mpirun -np [NUMBEROFPROCESS] -hostfile [HOSTFILE]",
		"",
		" grouph=        input binary grouph file",
		" traces=        input binary traces file",
		" nimf=1         extracted number of inf",
		" maxiter=10     maximum iteration",
		" nbsym=2        checking spline point",
		" splinetype=1   1=GSL, 2=MATLAB, 3=OSF",
		" splittime=0    0=process all, 1=split by 512 ms",
		" tapermethod=0  0=Without taper, 1=zeros taper, 2=cosinus function",
		" rescaling=0    0=without scaling, 1=using scaling",
		" taper=0.1      taper data (miliseconds)",
		" toptaper=0.1   top taper data (miliseconds)",
		" verbose=0      1=show verbose",
		"",
		" Sample Command :",
		" wparP_emd grouph=grouphData traces=tracesData nimf=1 maxiter=10",
		"",
		NULL};

// ----------------- segy variable ---------------------
int ntrc, nsp;
float dt;

// ----------------- mpi variable ----------------------
MPI_Status status;
int numprocs, rankmpi, namelen;
char processor_name[MPI_MAX_PROCESSOR_NAME];

// ----------------- temporary variable -----------------
int i;
int iidx, intrc;
float **traces=NULL;
int **grouphdata=NULL, ngrouph, ntrc_grouph;
char *cgrouph=NULL, *ctraces=NULL;
float **tmp_data;

// ----------------- program variable -------------------
int _nimf;
int _maxiter;
int _nbsym;
int _splinetype;
int _splittime;
int _tapermethod;
int _rescaling;
float _taper;
float _toptaper;
int _verbose;

void get_input_parameter()
{
	if (!getparint("nimf",&_nimf)) _nimf=1;
	if (!getparint("maxiter",&_maxiter)) _maxiter=10;
	if (!getparint("nbsym",&_nbsym)) _nbsym=2;
	if (!getparint("splinetype",&_splinetype)) _splinetype=1;
	if (!getparint("splittime",&_splittime)) _splittime=0;
	if (!getparint("tapermethod",&_tapermethod)) _tapermethod=0;
	if (!getparint("rescaling",&_rescaling)) _rescaling=0;
	if (!getparint("verbose",&_verbose)) _verbose=0;

	if (!getparfloat("taper",&_taper)) _taper=0.1;
	if (!getparfloat("toptaper",&_toptaper)) _toptaper=0.1;
	//printf("get input parameter success \n");
}
// -------------------------------------------------------

void broadcasting_data()
{
	MPI_Bcast(&nsp, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&dt, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&_nimf, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&_maxiter, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&_nbsym, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&_splinetype, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&_splittime, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&_tapermethod, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&_rescaling, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&_verbose, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&_taper, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&_toptaper, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);

}

void run_parallel()
{
	run_emd(tmp_data, ntrc, nsp, dt,
			_taper, _toptaper, _splittime, _splinetype,
			_nbsym, _nimf, _maxiter, _verbose, _tapermethod,
			_rescaling);
	//printf("ntrc=%i nsp=%i maxiter=%i\n",ntrc,nsp, _maxiter);
}

int main(int argc, char **argv)
{
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
	MPI_Comm_rank(MPI_COMM_WORLD, &rankmpi);
	MPI_Get_processor_name(processor_name, &namelen);

	/* hook up getpar to handle the parameters */
	initargs(argc,argv);
	requestdoc(1);

	MUSTGETPARSTRING("grouph",  &cgrouph);	/* input grouph file */
	MUSTGETPARSTRING("traces",  &ctraces);	/* input traces file */

	// --------------- get input parameter ---------------------
	get_input_parameter();

	// read grouph and trace data
	MPI_Barrier(MPI_COMM_WORLD); //01
	if (rankmpi == 0)
	{

		grouphdata = readBinaryGrouphFile(cgrouph, &nsp, &ntrc_grouph, &dt, &ngrouph);
		traces = readBinaryTracesFile(ctraces, nsp, ntrc_grouph);
		if(numprocs!=ngrouph+1){
			printf("\nERROR: Change number of processor from %i to %i \n\n\n", numprocs, ngrouph+1);
			exit(0);
		}
	}
	MPI_Barrier(MPI_COMM_WORLD); //01

	// broadcasting parameter
	broadcasting_data();
	//printf("ngrouph=%i\n", ngrouph);
	MPI_Barrier(MPI_COMM_WORLD); //03
	if (rankmpi == 0)
	{
		// ------------- send data to every processor -------------
		iidx = 0;
		for (i = 1; i <= ngrouph; i++)
		{
			intrc = grouphdata[i-1][3];
			if(_verbose)
				printf("running ntrc=%i traces \n", intrc);
			MPI_Send(&intrc, 1, MPI_INT, i, 1, MPI_COMM_WORLD); //number of traces
			MPI_Send(traces[iidx], nsp*intrc, MPI_FLOAT, i, 1, MPI_COMM_WORLD);	//send trace data
			iidx += intrc;
		}

		//--------------- receive data from every processor -------------
		iidx = 0;
		for(i = 1; i <= ngrouph; i++)
		{
			intrc = grouphdata[i-1][3];
			MPI_Recv(traces[iidx], nsp*intrc, MPI_FLOAT, i, 1, MPI_COMM_WORLD, &status);
			iidx += intrc;
		}
	}
	else
	{
		//receive data from master
		MPI_Recv(&ntrc, 1, MPI_INT, 0, 1, MPI_COMM_WORLD, &status);

		tmp_data = alloc2float(nsp, ntrc);
		MPI_Recv(tmp_data[0], nsp*ntrc, MPI_FLOAT, 0, 1, MPI_COMM_WORLD, &status);

		//running parallel process
		run_parallel();

		//send data to master
		MPI_Send(tmp_data[0], nsp*ntrc, MPI_FLOAT, 0, 1, MPI_COMM_WORLD);

		//free temporary data
		free2float(tmp_data);
	}
	MPI_Barrier(MPI_COMM_WORLD); //03

	//save data and free allocated memory
	MPI_Barrier(MPI_COMM_WORLD); //04
	if (rankmpi == 0)
	{
		//printf("write data inp=%s ntrc=%i nsp=%i\n",ctraces,ntrc_grouph,nsp);
		writeBinaryTracesFile(ctraces, traces, nsp, ntrc_grouph);
		free2float(traces);
		free2int(grouphdata);
	}
	MPI_Barrier(MPI_COMM_WORLD); //04

	MPI_Finalize();
	return(1);
}



