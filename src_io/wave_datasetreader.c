/*
 * wave_datasetreader.c
 *
 *  Created on: Jan 14, 2013
 *      Author: toto
 */


#include <stdio.h>
#include <stdlib.h>
#include "../promax_lib/segy_promax.h"
#include "../src_lib/segy_lib.h"
#define nbytes 200

char *sdoc[] =
{
		"",
		"WAVE_DATASETREADER	: CONVERT DATASET PROMAX TO SU FILE",
		"",
		" seis=             output seismic unix file",
		" dataset=          input dataset promax",
		" ",
		" wave_datasetreader seis=output.su dataset=/data/02938434 ",
		"",
		NULL
};

FILE *open_file(char *cname, char *cmode)
{
	FILE *fid=NULL;
	fid = fopen(cname, cmode);
	if(fid==NULL)
	{
		printf("error opening file %s \n", cname);
		exit(1);
	}

	return(fid);
}

int main(int argc, char **argv)
{
	char *cdataset_path;
	char *cseis;

	char *dtset_header;
	char *dtset_trace;
	FILE *fheader=NULL;
	FILE *ftrace=NULL;

	FILE *fseis=NULL;
	int i;
	int ns;
	int nwindows;
	int ntrc;

	PromaxTraceHeader pth;

	/* hook up getpar to handle the parameters */
	initargs(argc,argv);
	requestdoc(1);


	MUSTGETPARSTRING("seis", &cseis);
	MUSTGETPARSTRING("dataset", &cdataset_path);

	dtset_header = (char*) calloc(nbytes, 1);
	dtset_trace = (char*) calloc(nbytes, 1);
	sprintf(dtset_header, "%s/HDR1", cdataset_path);
	sprintf(dtset_trace, "%s/TRC1", cdataset_path);

	fheader = open_file(dtset_header, "r");
	ftrace = open_file(dtset_trace, "r");
	fseis = open_file(cseis, "w");

	//read trace header
	ReadFloat16Header(ftrace, &ns, &nwindows);
	rewind(ftrace);

	//get number of trace data
	ntrc = getNumberOfTraceFloat16File(ftrace, ns, nwindows);

	SeisCompress_Init(SCALAR16, ns, BIG_ENDIAN);

	//loop over trace
	for (i = 0; i < ntrc; i++)
	{
//		int result = fread(&pth, 1, HDRBYTES, fheader);
//		if(result!=HDRBYTES) err("error reading header trace at %i", i+1);
		ReadPromaxDatasetHeader(&pth, fheader);
		ReadPromaxHeader(&tr, pth, ns);
		getTrace16(ftrace, &tr);

		fputtr(fseis, &tr);
	}

	SeisCompress_Finalize();

	free(dtset_header);
	free(dtset_trace);
	fclose(fheader);
	fclose(ftrace);

	return EXIT_SUCCESS;
}
