/*
 * wave_datasetwriter.c
 *
 *  Created on: Jan 11, 2013
 *      Author: toto
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include "../promax_lib/segy_promax.h"
#include "../src_lib/segy_lib.h"
#define nbytes 200

char *sdoc[] =
{
		"",
		"WAVE_DATASETWRITER	: WRITE SEG-Y / SU to ProMAX dataset (alpha 1)",
		"",
		" seis=           input seismic file (SU or SEGY)",
		" path=           dataset path location",
		" sdataset=       input source dataset promax",
		" ddataset=       output destination dataset promax",
		" ftype=0         0=SU file, 1=SEGY file",
		" endian=0        set =0 for little-endian machines(PC's,DEC,etc.)",
		" dtype=0         0=gather, 1=stack seismic data",
		" key=21          only for gather. sort data by .. 21=cdp",
		" ",
		" How to use ;",
		" wave_datasetwriter seis= sdataset= ddataset= ftype=0 dtype=1 ",
		"",
		NULL
};
FILE *open_file(char *cname, char *cmode);
void create_requirement_dataset(char *pathname, char *dataset_id, char *dtset_header,
		char *dtset_trace, char *dtset_cmap, char *dtset_cind);
void create_CMAP_dataset(FILE *fcind, int ntrc);
void create_CIND_dataset(FILE *fecmap, FILE *fcmap, char *sdataset, char *ddataset);
void create_CIND_dataset_v1(FILE *fecind, FILE *fcind, char *pathname,
		char *sdataset, char *ddataset);
int main(int argc, char **argv)
{
	char *cseis;
	char *sdataset;
	char *src_dataset;
	char *ddataset;

	char *pathname;

	char *dtset_header;
	char *dtset_trace;
	char *dtset_cmap;
	char *dtset_cind;

	FILE *fseis=NULL;

	FILE *fsrc_cind=NULL;

	FILE *fheader=NULL;
	FILE *ftrace=NULL;
	FILE *fcmap=NULL;
	FILE *fcind=NULL;

	int key;
	int dtype;
	int ftype;
	int nsp;
	int dt;
	int ntrc;
	int nsegy;
	int format=0;
	int endian;
	char cebcdic[3200];

	int i;

	/* hook up getpar to handle the parameters */
	initargs(argc,argv);
	requestdoc(1);

	if (!getparint("ftype", &ftype)) ftype=0;
	if (!getparint("endian", &endian)) endian=0;
	if (!getparint("dtype", &dtype)) dtype=0; //0=gather, 1=stack
	if (!getparint("key", &key)) key=21;	//sorting data by cdp
	MUSTGETPARSTRING("path", &pathname);
	MUSTGETPARSTRING("seis", &cseis);
	MUSTGETPARSTRING("sdataset", &sdataset);
	MUSTGETPARSTRING("ddataset", &ddataset);

	src_dataset = (char*) calloc(nbytes, 1);
	sprintf(src_dataset, "%s/%sCIND", pathname, sdataset);

	dtset_header = (char*) calloc(nbytes, 1);
	dtset_trace = (char*) calloc(nbytes, 1);
	dtset_cmap = (char*) calloc(nbytes, 1);
	dtset_cind = (char*) calloc(nbytes, 1);
	create_requirement_dataset(pathname, ddataset, dtset_header, dtset_trace,
			dtset_cmap, dtset_cind);

//	fprintf(stderr, "dataset= %s \n", ddataset);
//	fprintf(stderr, "header= %s \n", dtset_header);
//	fprintf(stderr, "traces= %s \n", dtset_trace);
//	fprintf(stderr, "cind= %s \n", dtset_cind);
//	fprintf(stderr, "cmap= %s \n", dtset_cmap);

	//open seismic file
	fseis = open_file(cseis, "r");

	//open existing dataset
	fsrc_cind = open_file(src_dataset, "r");

	//create output dataset
	fheader = open_file(dtset_header, "w");
	ftrace = open_file(dtset_trace, "w");
	fcmap = open_file(dtset_cmap, "w");
	fcind = open_file(dtset_cind, "r+");

	/* Get info from first trace */
	if(ftype==0)
	{
		if (!fgettr(fseis, &tr)) err("can't get first trace");
		nsp = tr.ns;
		dt = tr.dt;
		ntrc = getNumberOfTraceSuFile(fseis);
		rewind(fseis);
	}
	else
	{
		readEbcdicHeader(fseis, cebcdic); /* read ebcdic header */
		readBinaryHeader(fseis, endian, &bh, &nsegy); /*read binary header */
		ntrc = getNumberOfTraceSegyFile(fseis, nsegy);
		nsp = bh.hns;
		dt = bh.hdt;
		format = bh.format;
	}

	SeisCompress_Init(SCALAR16, nsp, BIG_ENDIAN);
	for(i=0; i<ntrc; i++)
	{
		if(ftype==0)
		{
			fgettr(fseis, &tr);
		}
		else
		{
			readTraceSegy(fseis, endian, nsp, format, nsegy, &tr);
		}


		//write stack data to dataset
		if(dtype==1)
			writeTraceToDataset64(tr, i + 1, ntrc, 1, 1, nsp, dt, fheader, ftrace);

		//gather, create new algorithm for detecting end of ensemble
		else
		{
			writeTraceToDataset64(tr, i + 1, ntrc, 1, 1, nsp, dt, fheader, ftrace);
		}
	}
	SeisCompress_Finalize();

	//add cmap and ind dataset file
	create_CMAP_dataset(fcmap, ntrc);
	create_CIND_dataset_v1(fsrc_cind, fcind, pathname, sdataset, ddataset);
//	create_CIND_dataset(fsrc_cind, fcind, sdataset, ddataset);

	//free allocated memory
	free(dtset_header);
	free(dtset_trace);
	free(dtset_cmap);
	free(dtset_cind);
	free(src_dataset);

	//closed file
	fclose(fseis);
	fclose(fsrc_cind);
	fclose(fheader);
	fclose(ftrace);
	fclose(fcmap);
	fclose(fcind);

	return EXIT_SUCCESS;
}

void create_CMAP_dataset(FILE *fcind, int ntrc)
{
	unsigned char buff[4];
	int tmp1;

	tmp1 = (int)(ntrc/16777216);
	buff[0] = (unsigned char) (tmp1);

	ntrc = ntrc - (tmp1*16777216);
	tmp1 = (int)(ntrc/65536);
	buff[1] = (unsigned char) (tmp1);

	ntrc = ntrc - (tmp1*65536);
	tmp1 = (int)(ntrc/256);
	buff[2] = (unsigned char) (tmp1);

	ntrc = ntrc - (tmp1*256);
	tmp1 = (int)(ntrc);
	buff[3] = (unsigned char) (tmp1);
	fwrite(buff, 1, 4, fcind);
}

void create_CIND_dataset_v1(FILE *fecind, FILE *fcind, char *pathname,
		char *sdataset, char *ddataset)
{
	int result;
	int nbuffer;
	int size1, size2;
	char *buff_header=NULL, *buff_name=NULL, *buff_data=NULL;

	char *sdataset_enc=NULL;
	char *ddataset_enc=NULL;
	int n_dataset_path, ind_pos;

	//existing dataset variable
	int cind_top_header_size = 176;
	int cind_name_byte_size = 30;

	//current dataset variable
	int cind_empty_dataset_size = 216;
	int ccind_name_byte_start = 68;

	//get the size of previous cind file
	result = fseek(fecind, 0, SEEK_END);
	size1 = ftell(fecind);

	//get the size of current cind file
	result = fseek(fcind, 0, SEEK_END);
	size2 = ftell(fcind);

	//rewind data
	rewind(fecind);
	rewind(fcind);

	if(size1<=cind_empty_dataset_size) return;

	//encode source dataset
	sdataset_enc = EncodeToDataset(&n_dataset_path, pathname, sdataset);

	//encode destination dataset
	ddataset_enc = EncodeToDataset(&n_dataset_path, pathname, ddataset);

	//fill empty dataset
	if(size2==cind_empty_dataset_size)
	{
		// ------------- PREVIOUS DATASET IND FILE --------------
		//read top header existing CIND file
		buff_header = (char*) calloc(cind_top_header_size, 1);
		result = fread(buff_header, 1, cind_top_header_size, fecind);

		//skip dataset name
		result = fseek(fecind, cind_name_byte_size, SEEK_CUR);

		// ------------- CURRENT DATASET IND FILE --------------
		//skip header current CIND file
		result = fseek(fcind, ccind_name_byte_start, SEEK_SET);

		//read dataset name from CIND file
		buff_name = (char*) calloc(cind_name_byte_size, 1);
		result = fread(buff_name, 1, cind_name_byte_size, fcind);
		rewind(fcind);

		//read/write CIND data file
		result = fwrite(buff_header, 1, cind_top_header_size, fcind);
		result = fwrite(buff_name, 1, cind_name_byte_size, fcind);

		// ------------- save history CIND file -----------------
		nbuffer = size1 - (cind_top_header_size+cind_name_byte_size);
		buff_data = (char*) calloc(nbuffer, 1);
		//read cmap data
		result = fread(buff_data, 1, nbuffer, fecind);

		ind_pos = getIndexPathPosition(buff_data, nbuffer, sdataset_enc);
		if (ind_pos != 0)
		{
			changeIndexDatasetName(buff_data, ind_pos, n_dataset_path, n_dataset_path, ddataset_enc);
		}
		else
		{
			fprintf(stderr, "PATH = %s \n", pathname);
			fprintf(stderr, "Cannot find dataset name in index file %sCIND\n", sdataset);
			exit(0);
		}

		//write cmap data
		result = fwrite(buff_data, 1, nbuffer, fcind);
		free(buff_header);
		free(buff_name);
		free(buff_data);
	}
	else if(size2>cind_empty_dataset_size)
	{

	}
	else
	{
		fprintf(stderr, "Input CIND dataset file size is too small (<216 byte) \n");
		exit(0);
	}

	free(sdataset_enc);
	free(ddataset_enc);
}

void create_CIND_dataset(FILE *fecind, FILE *fcind, char *sdataset, char *ddataset)
{
	int result;
	int nbuffer;
	int size1, size2;
	char *buff_header=NULL, *buff_name=NULL, *buff_data=NULL;

	char *sdataset_path=NULL, *sdataset_enc=NULL;
	char *ddataset_enc=NULL;
	int n_dataset_path, ind_pos;

	//existing dataset variable
	int cind_top_header_size = 176;
	int cind_name_byte_size = 30;

	//current dataset variable
	int cind_empty_dataset_size = 216;
	int ccind_name_byte_start = 68;

	//get the size of previous cind file
	result = fseek(fecind, 0, SEEK_END);
	size1 = ftell(fecind);

	//get the size of current cind file
	result = fseek(fcind, 0, SEEK_END);
	size2 = ftell(fcind);

	//rewind data
	rewind(fecind);
	rewind(fcind);

	if(size1<=cind_empty_dataset_size) return;

	//encode source dataset
	sdataset_path = getWorkingDirectory();
	sdataset_enc = EncodeToDataset(&n_dataset_path, sdataset_path, sdataset);

	//encode destination dataset
	ddataset_enc = EncodeToDataset(&n_dataset_path, sdataset_path, ddataset);

	//fill empty dataset
	if(size2==cind_empty_dataset_size)
	{
		// ------------- PREVIOUS DATASET IND FILE --------------
		//read top header existing CIND file
		buff_header = (char*) calloc(cind_top_header_size, 1);
		result = fread(buff_header, 1, cind_top_header_size, fecind);

		//skip dataset name
		result = fseek(fecind, cind_name_byte_size, SEEK_CUR);

		// ------------- CURRENT DATASET IND FILE --------------
		//skip header current CIND file
		result = fseek(fcind, ccind_name_byte_start, SEEK_SET);

		//read dataset name from CIND file
		buff_name = (char*) calloc(cind_name_byte_size, 1);
		result = fread(buff_name, 1, cind_name_byte_size, fcind);
		rewind(fcind);

		//read/write CIND data file
		result = fwrite(buff_header, 1, cind_top_header_size, fcind);
		result = fwrite(buff_name, 1, cind_name_byte_size, fcind);

		// ------------- save history CIND file -----------------
		nbuffer = size1 - (cind_top_header_size+cind_name_byte_size);
		buff_data = (char*) calloc(nbuffer, 1);
		//read cmap data
		result = fread(buff_data, 1, nbuffer, fecind);

		ind_pos = getIndexPathPosition(buff_data, nbuffer, sdataset_enc);
		if (ind_pos != 0)
		{
			changeIndexDatasetName(buff_data, ind_pos, n_dataset_path, n_dataset_path, ddataset_enc);
		}
		else
		{
			fprintf(stderr, "PATH = %s \n", sdataset_path);
			fprintf(stderr, "Cannot find dataset name in index file %sCIND\n", sdataset);
			exit(0);
		}

		//write cmap data
		result = fwrite(buff_data, 1, nbuffer, fcind);
		free(buff_header);
		free(buff_name);
		free(buff_data);
	}
	else if(size2>cind_empty_dataset_size)
	{

	}
	else
	{
		fprintf(stderr, "Input CIND dataset file size is too small (<216 byte) \n");
		exit(0);
	}

	free(sdataset_path);
	free(sdataset_enc);
	free(ddataset_enc);
}

FILE *open_file(char *cname, char *cmode)
{
	FILE *fid=NULL;
	fid = fopen(cname, cmode);
	if(fid==NULL)
	{
		printf("error opening file %s \n", cname);
		exit(1);
	}

	return(fid);
}

void create_requirement_dataset(char *pathname, char *dataset_id, char *dtset_header,
		char *dtset_trace, char *dtset_cmap, char *dtset_cind)
{
	char *fullpath;
	fullpath = (char*) calloc(nbytes, 1);
	sprintf(fullpath, "%s/%s", pathname, dataset_id);

	if(mkdir(fullpath, 0764)==-1)
		printf("using existing directory \n"); //create dataset

	sprintf(dtset_header, "%s/%s/HDR1", pathname, dataset_id);
	sprintf(dtset_trace, "%s/%s/TRC1", pathname, dataset_id);
	sprintf(dtset_cmap, "%s/%sCMAP", pathname, dataset_id);
	sprintf(dtset_cind, "%s/%sCIND", pathname, dataset_id);

	free(fullpath);
}
