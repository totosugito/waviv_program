/*
 * wave_crsvelpicking.c
 *
 *  Created on: Jan 15, 2013
 *      Author: toto
 */


#include <stdio.h>
#include <stdlib.h>
#include "../src_lib/segy_lib.h"
#define LOG_COLUMN 3

char *sdoc[] =
{
		"",
		"WAVE_CRSVELPICKING	: CRS VELOCITY PICKING",
		"",
		" vel=            input velocity file",
		" coher=          input coherence file",
		" minpick=10      Minimum Picking in Time",
		" yspace=0.2      Spacing in Time (secon)",
		" xspace=0        Spacing in CDP",
		" threshold=0.5   Threshold for coherence",
		" ",
		" wave_crsvelpicking vel= coher= minpick=10 yspace=0.2 xspace=1 threshold=0.5 > output.txt",
		"",
		NULL
};

FILE *open_file(char *cname, char *cmode)
{
	FILE *fid=NULL;
	fid = fopen(cname, cmode);
	if(fid==NULL)
	{
		fprintf(stderr, "error opening file %s \n", cname);
		exit(1);
	}

	return(fid);
}

void process_data(float **vtraces, float **vcoher, int ntrc, int nsp, int dt,
		int *cdp, int minpick, int xspace, float yspace, float threshold)
{
	float **log_data=NULL;
	int i, j;
	int nypick;
	float fdt;
	int Yspace;

	fdt = (float) (dt/1000000.0);
	Yspace = (int) (yspace/fdt);
	if( (Yspace <= 0) || (Yspace >= nsp) ) Yspace = 1;

//	fprintf(stderr, "minpick=%i Yspace=%i xspace=%i threshold=%f \n",
//			minpick, Yspace, xspace, threshold);
	//create header
	printf("             CDP          Times (MS)          Velocity\n");

	log_data = alloc2float(LOG_COLUMN, nsp);
	i = 0;
	while(i<ntrc) //loop over trace
	{
//		fprintf(stderr, "X %i \n", i);
		j = 0;
		nypick = 0;
		while(j<nsp) //loop over time
		{
			if(vcoher[i][j] > threshold)
			{
				log_data[nypick][0] = cdp[i];
				log_data[nypick][1] = j*fdt*1000.0;
				log_data[nypick][2] = vtraces[i][j];
				nypick++;

				j = j+Yspace+1;
			}
			else
				j++;

			if(j>=nsp)
				break;
		}

//		fprintf(stderr, "X %i %i cdp=%i\n", i, nypick, cdp[i]);

		//create log file
		if(nypick>minpick)
		{
			for(j=0; j<nypick; j++)
				printf(" %15.0f %15.1f %20.5f \n",
						log_data[j][0], log_data[j][1], log_data[j][2]);
		}

		//remove existing data
		memset(log_data[0], 0, nsp*LOG_COLUMN*sizeof(float));

		if(nypick > minpick)
			i = i+xspace+1; //skip cdp with size xspace
		else
			i = i+1; //read next cdp

		if(i>=ntrc)
			break;
	}

	free2float(log_data);
}

int main(int argc, char **argv)
{
	int result;
	char header[SEGY_HDRBYTES];
	char *cvel, *ccoher;
	FILE *fvel=NULL, *fcoher=NULL;
	int minpick;
	float yspace;
	int xspace;
	float threshold;

	int ntrc, nsp, dt;
	int i;

	float **vtraces=NULL;
	float **vcoher=NULL;
	int *cdp=NULL;

	segy vtr;

	/* hook up getpar to handle the parameters */
	initargs(argc,argv);
	requestdoc(1);

	MUSTGETPARSTRING("vel", &cvel);
	MUSTGETPARSTRING("coher", &ccoher);
	if (!getparint("minpick", &minpick)) minpick=10;
	if (!getparfloat("yspace", &yspace)) yspace=0.2;
	if (!getparint("xspace", &xspace)) xspace=0;
	if (!getparfloat("threshold", &threshold)) threshold=0.5;

	fvel = open_file(cvel, "r");
	fcoher = open_file(ccoher, "r");

	//get the first trace
	fgettr(fvel, &vtr);
	nsp = vtr.ns;
	dt = vtr.dt;
	ntrc = getNumberOfTraceSuFile(fvel);
	rewind(fvel);

	//allocate data
	vtraces = alloc2float(nsp, ntrc);
	vcoher = alloc2float(nsp, ntrc);
	cdp = alloc1int(ntrc);

	//get data
	for(i=0; i<ntrc; i++)
	{
		fgettr(fvel, &vtr);
		cdp[i] = vtr.cdp;
		memcpy(vtraces[i], vtr.data, nsp*sizeof(float));

		result = fread(header, sizeof(char), SEGY_HDRBYTES, fcoher);
		result = fread(vcoher[i], sizeof(float), nsp, fcoher);
	}

	process_data(vtraces, vcoher, ntrc, nsp, dt,
			cdp, minpick, xspace, yspace, threshold);

	free2float(vtraces);
	free2float(vcoher);
	free1int(cdp);

	fclose(fvel);
	fclose(fcoher);
	return EXIT_SUCCESS;
}

