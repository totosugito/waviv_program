/*
 * wave_cadzow1d.c
 *
 *  Created on: Apr 25, 2012
 *      Author: toto
 */
#include "../src_lib/wvfxssa.h"
#include "../src_lib/segy_lib.h"
#include "../src_lib/binaryLib.h"
#include "../src_lib/fftLib.h"

char *sdoc[] = {
		"			",
		"WAVE_CADZOW1D_v3 ",
		"wave_cadzow1d <stdin rank=1> output",
		"			",
		"rank=             rank cadzow  ",
		"iswindowing=1     windowing data",
		"			",
		NULL};


int main(int argc, char ** argv) {
	char cebcdic[3200];
	int i, j;
	int ns, ntrc, format, endian, nsegy;
	float **dataOri;
	float dt;
	div_t divresult;
	time_t t1,t2;

	int nf, nfft;
	float d1;
	int rank;
	float ffmin;
	float ffmax;
	int spatspan;
	int tempspan;
	float fftpad;
	float epsilon;
	int pocs;
	int interp;
	int old;
	int maxiter;
	int tmin, tmax;
	int dealiased;

	/* hook up getpar to handle the parameters */
	initargs(argc,argv);
	requestdoc(1);

	if (!getparint("rank",&rank)) rank=1;
	if (!getparint("endian",&endian)) endian=0;
	if (!getparfloat("fmin",&ffmin)) ffmin=0.0;
	if (!getparfloat("fmax",&ffmax)) ffmax=60.0;

	if(!getparint("spatspan",&spatspan))spatspan=10;
	if(!getparint("tempspan",&tempspan))tempspan=120;
	if(!getparfloat("fftpad",&fftpad))fftpad=0.00;
	if(!getparfloat("epsilon",&epsilon))epsilon=0.45;

	if(!getparint("pocs",&pocs))pocs=0;
	if(!getparint("maxiter",&maxiter))maxiter=5;
	if(!getparint("interp",&interp))interp=0;
	if(!getparint("old",&old))old=0;
	if(!getparint("dealiased",&dealiased))dealiased=0;

	/* get variable fft */
	t1 = time(NULL);
	readEbcdicHeader(stdin, cebcdic); /* read ebcdic header */
	readBinaryHeader(stdin, endian, &bh, &nsegy); /*read binary header */
	ntrc = getNumberOfTraceSegyFile(stdin, nsegy);
	ns = bh.hns;
	format = bh.format;
	dt = (float) (bh.hdt / 1e+6);
	getNfft(ns, dt, &nf, &nfft, &d1);
	if(ffmin<0.0)	ffmin = 0.0;
	if(ffmax<=0 || ffmax>=nf*d1) ffmax = nf*d1 - d1;
	tmin = (int) (ffmin/d1);
	tmax = (int) (ffmax/d1);

	fprintf(stderr, "nf=%i nfft=%i fmin=%f fmax=%f tmin=%i tmax=%i \n",
			nf, nfft, fmin, fmax, tmin, tmax);
	bh.format = 1;
	writeEbcdicHeader(stdout, cebcdic);
	writeBinaryHeader(stdout, endian, &bh);

	//read data
	dataOri = alloc2float(ns, ntrc);
	for (i=0; i<ntrc; i++)
	{
		readTraceSegy(stdin, endian, ns, format, nsegy, &tr);
		for (j=0; j<ns; j++)
			dataOri[i][j] = tr.data[j];
	}

	if(ntrc > 2*(rank-1))
	{
		run_cadzow_v3(dataOri, ntrc, ns,
				rank, spatspan, tempspan, fftpad, epsilon,
				pocs, maxiter, interp, old, dealiased);
	}
	gotoTraceSegyPosition(stdin, 0, nsegy);
	for(i=0; i<ntrc; i++)
	{
		readTraceSegy(stdin, endian, ns, format, nsegy, &tr);
		for(j=0; j<ns; j++)
			tr.data[j] = dataOri[i][j];

		writeTraceSegy(stdout, endian, &tr, nsegy, ns);
	}

	free2float(dataOri);

	t2 = time(NULL);
	divresult = div (t2-t1, 60);
	fprintf (stderr, "Process time = %d min %d sec\n", divresult.quot, divresult.rem);

	return(1);
}
