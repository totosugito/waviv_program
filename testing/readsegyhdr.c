/*
 * readsegyhdr.c
 *
 *  Created on: Jun 7, 2012
 *      Author: toto
 */

#include "segy_lib.h"
#include <stdbool.h>
#include <time.h>

char *sdoc[] = {
		"									",
		" readsegyhdr",
		NULL};


int main (int argc, char **argv)
{
	/* hook up getpar to handle the parameters */
//	initargs(argc,argv);
//	requestdoc(1);

	float **data;
	char **header;

	int i;
	int endian;
	int ntrc;
	int nsp, format, nsegy;
	FILE *fid=NULL, *fod=NULL;
	char buffer[3600];


	ntrc = 240;
	nsp = 1751;
	endian=0;
	format = 1;
	nsegy = 240+nsp*4;

	fid = fopen("/data/1.sgy","r");
	if(fid==NULL)
		err("error reading file");

	fod = fopen("/data/1r.sgy","w");
	if(fod==NULL)
		err("error writing file");

	fread(buffer, 1, 3600, fid);
	fwrite(buffer,1,3600, fod);
	data = alloc2float(nsp, ntrc);
	header = alloc2char(240, ntrc);


	for(i=0; i<ntrc; i++)
	{
		readSegy(fid, endian, nsp, format, nsegy, &tr, header[i], data[i]);
//		memcpy(header[i], (char*)&tr, HDRBYTES);
//		memcpy(data[i], tr.data, nsp*sizeof(float));
	}

	for(i=0; i<ntrc; i++)
	{
//		memcpy((char*)&tr, header[i], HDRBYTES);
//		memcpy(tr.data, data[i], nsp*sizeof(float));
		writeSegy(fod, endian, &tr, nsegy, nsp, header[i], data[i]);
	}
	free2float(data);
	free2char(header);
	fclose(fid);
	fclose(fod);
	return(1);
}
