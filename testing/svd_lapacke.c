/*
 * svd_lapacke.c
 *
 *  Created on: May 3, 2012
 *      Author: toto
 */
#include "svd_lapacke.h"


void svdlapacke(double **data, int irow, int icol,
		double *S, double **U, double **VT)
{
	int m, n, lda, ldu, ldvt, info, scnt;
	int i;
	double *superb, *aloc, *u, *vt;

	m = irow;
	n = icol;
	lda = n;
	ldu = m;
	ldvt = n;

	superb = alloc1double(MIN(m,n)-1);
	scnt = MIN(m, n);
	aloc = alloc1double(lda*m);
	u = alloc1double(ldu*m);
	vt = alloc1double(ldvt*n);

	for(i=0; i<m; i++)
		memcpy(&aloc[i*lda],data[i],lda*sizeof(double));

	info = LAPACKE_dgesvd( LAPACK_ROW_MAJOR, 'A', 'A', m, n, aloc, lda,
			S, u, ldu, vt, ldvt, superb );

	if( info > 0 )
	{
		fprintf(stderr, "The algorithm computing SVD failed to converge.\n" );
		exit( 1 );
	}

	for(i=0; i<m; i++)
		memcpy(U[i], &u[i*m], m*sizeof(double));

	for(i=0; i<n; i++)
		memcpy(VT[i], &vt[i*n], n*sizeof(double));

	free1double(superb);
	free1double(aloc);
	free1double(u);
	free1double(vt);
}
