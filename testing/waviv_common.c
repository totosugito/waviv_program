#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <su.h>
#include <segy.h>
#include <math.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>

#include "waviv_common.h"

sugather* load(char *infilename){
	suheader traceheader;
	FILE* infileptr = efopen(infilename,"r");
	efread(&traceheader,1,sizeof(suheader),infileptr);
	struct stat buf;
	fstat(fileno(infileptr), &buf);
 	sugather* gather = (sugather*) malloc(sizeof(sugather));
	unsigned long size = buf.st_size;
	gather->ns = traceheader.ns;
	gather->ntraces = size / (240 + gather->ns * 4);
	gather->dt = (traceheader.dt/1000000.0);
	erewind(infileptr);
	gather->traces = (sutrace*) malloc(gather->ntraces*sizeof(sutrace));
	int i;
	for(i=0;i<gather->ntraces;i++){
		gather->traces[i].header = (suheader*) malloc(sizeof(suheader));
		gather->traces[i].data   = (float*) malloc(gather->ns*sizeof(float));
		efread(gather->traces[i].header,1,sizeof(suheader),infileptr);
		efread(gather->traces[i].data,1,(gather->ns*sizeof(float)),infileptr);
	}
	efclose(infileptr);
	return gather;
}

void save(FILE* outfileptr,sugather* gather){
	int tr;
	for(tr=0;tr<gather->ntraces;tr++){
		writetrace(outfileptr,&gather->traces[tr]);
	}
}

void writetrace(FILE* outfileptr,sutrace* trace){
	efwrite(trace->header,1,sizeof(suheader),outfileptr);
	efwrite(trace->data,1,trace->header->ns*sizeof(float),outfileptr);
}

void readtrace(FILE* fptr,sutrace* trace){
	efread(trace->header,1,sizeof(suheader),fptr);
	efread(trace->data,1,trace->header->ns*sizeof(float),fptr);
}

void freegather(sugather* gather){
	int i;
	for(i=0;i<gather->ntraces;i++){
		free(gather->traces[i].header);
		free(gather->traces[i].data);  
	}
	free(gather);
}

void getcol(float** input, int colselect, float* coldata, int nrow){
	int i;
	for(i=0;i<nrow;i++){
		coldata[i]=input[i][colselect];
	}
}

void setcol(float** input, int colselect, float* coldata, int nrow){
	int i;
	for(i=0;i<nrow;i++){
		input[i][colselect]=coldata[i];
	}
}

void getrow(float** input, int rowselect, float* rowdata, int ncol){
	memcpy(rowdata,input[rowselect],ncol*sizeof(float));
}

void setrow(float** input, int rowselect, float* rowdata, int ncol){
	memcpy(input[rowselect],rowdata,ncol*sizeof(float));
}

void getcold(double** input, int colselect, double* coldata, int nrow){
	int i;
	for(i=0;i<nrow;i++){
		coldata[i]=input[i][colselect];
	}
}

void setcold(double** input, int colselect, double* coldata, int nrow){
	int i;
	for(i=0;i<nrow;i++){
		input[i][colselect]=coldata[i];
	}
}

void getrowd(double** input, int rowselect, double* rowdata, int ncol){
	memcpy(rowdata,input[rowselect],ncol*sizeof(double));
}

void setrowd(double** input, int rowselect, double* rowdata, int ncol){
	memcpy(input[rowselect],rowdata,ncol*sizeof(double));
}

float** ftranspose(float** data, int row, int col){
	float** output = (float**) ealloc2float(row,col);

	int i,j;	
	for(i=0;i<row;i++){
		for(j=0;j<col;j++){
			output[j][i]=data[i][j];
		}
	}
	return output;
}

double** dtranspose(double** data, int row, int col){
	double** output = (double**) ealloc2double(row,col);

	int i,j;	
	for(i=0;i<row;i++){
		for(j=0;j<col;j++){
			output[j][i]=data[i][j];
		}
	}
	return output;
}

double* todouble1d(float* data, int n){
	double* out = (double*) calloc(n,sizeof(double));
	int i;	
	for(i=0;i<n;i++){
		out[i]=(double) data[i];
	}
	return out;
}

float* tofloat1d(double* data, int n){
	float* out = (float*) calloc(n,sizeof(float));
	int i;	
	for(i=0;i<n;i++){
		out[i]=(float) data[i];
	}
	return out;
}

double** todouble2d(float** data, int row, int col){
	double** output = (double**) ealloc2double(col,row);
	int i,j;
	for(i=0;i<row;i++){
		for(j=0;j<col;j++){
			output[i][j]=(double)data[i][j];
		}
	}
	return output;
}

float** tofloat2d(double** data, int row, int col){
	float** output = (float**) ealloc2float(col,row);
	int i,j;
	for(i=0;i<row;i++){
		for(j=0;j<col;j++){
			output[i][j] = (float)data[i][j];
		}
	}
	return output;
}

void zeros1d(float* data, int len){
	int i;
	for(i=0;i<len;i++){
			data[i]=0.0;		
	}
}

void ones1d(float* data, int len){
	int i;
	for(i=0;i<len;i++){
			data[i]=1.0;		
	}
}

void dzeros2d(double** data, int row, int col){
	int i,j;
	for(i=0;i<row;i++){
		for(j=0;j<col;j++){
			data[i][j]=0.0;		
		}
	}
}

void dones2d(double** data, int row, int col){
	int i,j;
	for(i=0;i<row;i++){
		for(j=0;j<col;j++){
			data[i][j]=1.0;		
		}
	}
}

void taper1dlin(float* data, int ns, int s){
	float m = 1.0/s;
	int i;
	float tap=0.0;
	for(i=0;i<s;i++){
		data[i]*=tap;
		tap+=m;
	}
	tap=1.0;
	for(i=ns-s-1;i<ns;i++){
		data[i]*=tap;
		tap-=m;
	}
}

hankel1d* createhankel1d(double* data, int n){
	hankel1d* hankel = malloc(sizeof(hankel1d)); 
	hankel->m = (n+1)/2;
	hankel->l = n-hankel->m+1;
	hankel->data = (double**) ealloc2double(hankel->l,hankel->m);
        
	int idx,row,col;
	for(row=0;row<hankel->m;row++){
		idx=row;
		for(col=0;col<hankel->l;col++){
			hankel->data[row][col]=data[idx];
			idx++;
		}
	}
	return hankel;
}

hankel2d* createhankel2d(double** in, int rowin, int colin){
	hankel2d* hankel = malloc(sizeof(hankel2d));
	hankel->s = colin;
    hankel->m = (hankel->s+1)/2;
    hankel->l = (hankel->s-hankel->m+1);
        
    hankel->ss = rowin;
    hankel->mm = (hankel->ss+1)/2;
    hankel->ll = hankel->ss-hankel->mm+1;
     
    hankel->hm = hankel->m*hankel->mm;
    hankel->hl = hankel->l*hankel->ll;
	hankel->data = ealloc2double(hankel->hl,hankel->hm);
	
	int beg=0;
	int row,col,r,c,i,j;
	
    for(row=0;row<hankel->mm;row++){
        for(col=0;col<hankel->ll;col++){
			hankel1d* sub = createhankel1d(in[beg+col],colin);
            r=(row*hankel->m);  
            for(i=0;i<hankel->m;i++){
                c=(col*hankel->l);
                for(j=0;j<hankel->l;j++){
                    hankel->data[r][c]=sub->data[i][j];
                    c++;
                }
                r++;
            }
			freehankel1d(sub);
        }
        beg++;
    }
	//printf("QC CODE\n");
	return hankel;
}

double* inverseHankel(hankel1d* hankel){
	int m = hankel->m;
	int l = hankel->l;
	double* val = (double*) calloc((l*m),sizeof(double));
	double* avg = (double*) calloc((l+m-1),sizeof(double)); 

	int dx=0;
	int i,j;
	for(i=0;i<m;i++){
		for(j=0;j<l;j++){
			val[dx]=hankel->data[i][j];
			dx++;
		}
	}

	for(i=0;i<m;i++){
		double cnt=0;
		for(j=0;j<i+1;j++){
			avg[i]+=val[i+(j*(l-1))];
			cnt++;
		}
		avg[i]/=cnt;
	}

	if(m==l){
		int cnt  = l-1;
		int mdex = 2;
		for(i=m;i<l+m-1;i++){
			int dex = (mdex*l)-1;
			int mnt = 0;
			for(j=0;j<cnt;j++){
				mnt++;
				avg[i]+=val[dex];
				dex+=(l-1);
			}
			avg[i]/=cnt;
			cnt--;
			mdex++;
		}

	}else{
		int cnt  = m;
		int mdex = 1;
		for(i=m;i<l+m-1;i++){
			int dex = (mdex*l)-1;
			int mnt = 0;
			for(j=0;j<cnt;j++){
				mnt++;
				avg[i]+=val[dex];
				dex+=(l-1);
			}
			avg[i]/=cnt;
			cnt--;
			mdex++;
		}
	}
	free(val);
    return avg;
}

double** inverseHankel2D(hankel2d* hankel){
    //Vector<double[]> list = new Vector<double[]>();
    int ssdex=0;
	int row,col,i,j;
	int mm=hankel->mm;
	int ll=hankel->ll;
	int m=hankel->m;
	int l=hankel->l;
	int s=hankel->s;
	int ss=hankel->ss;
	
	double** list = ealloc2double(l+m-1,mm*ll);
	
	int indexlist=0;
    for( row=0;row<mm;row++){
        for( col=0;col<ll;col++){
            double** sub = ealloc2double(l,m);
            int r=(row*m);
            for( i=0;i<m;i++){
                int c=(col*l);
                for( j=0;j<l;j++){
                    sub[i][j]=hankel->data[r][c];
                    c++;                        
                }
                r++;
            }
			double* av = average(sub,m,l);
			
			memcpy(list[indexlist],av,(m+l-1)*sizeof(double));
			//printarray1d(list[indexlist],m+l-1);
			indexlist++;
            ssdex++;
			free1double(av);
			free2double(sub);
        }
    }
	
    double** inv = ealloc2double(s,ss);
	
	for( i=0;i<mm;i++){
        double* a = calloc(s,sizeof(double));
        double cnt=0;
		
        for( j=0;j<i+1l;j++){
            stack(a,list[(i+(j*(ll-1)))],s);
            cnt++;
        }
		
        divarr(a,cnt,s);
		memcpy(inv[i],a,s*sizeof(double));
		free1double(a);
    }
        
    if(mm==ll){
        int cnt  = ll-1;
        int mdex = 2;
        for( i=mm;i<mm+ll-1;i++){
            int dex = (mdex*ll)-1;
            double* a = calloc(s,sizeof(double));
            for( j=0;j<cnt;j++){
                stack(a,list[dex],s);
                dex+=(ll-1);
            }
            divarr(a,cnt,s);
            memcpy(inv[i],a,s*sizeof(double));
            cnt--;
            mdex++;
			free1double(a);
        }
    }else{
        int cnt  = mm;
        int mdex = 1;
        for( i=mm;i<ll+mm-1;i++){
            int dex = (mdex*ll)-1;
            double* a = calloc(s,sizeof(double));
            for( j=0;j<cnt;j++){
                stack(a,list[dex],s);
                dex+=(ll-1);
            }
            divarr(a,cnt,s);
            memcpy(inv[i],a,s*sizeof(double));
            cnt--;
            mdex++;
			free1double(a);
        }
    }
    return inv;
}
	
double* average(double** data, int m, int l){
	if(l<m){
		printf("collum < row : this procedure are not intended for this condition\n");
	}
	double* val = (double*) calloc((l*m),sizeof(double));
	double* avg = (double*) calloc((l+m-1),sizeof(double)); 

	int dx=0;
	int i,j;
	for(i=0;i<m;i++){
		for(j=0;j<l;j++){
			val[dx]=data[i][j];
			dx++;
		}
	}

	double cnt=0;
	for(i=0;i<m;i++){
		cnt=0;
		for(j=0;j<i+1;j++){
			avg[i]+=val[i+(j*(l-1))];
			cnt++;
		}
		avg[i]/=cnt;
	}
	
	if(m==l){
		cnt  = l-1;
		int mdex = 2;
		for(i=m;i<l+m-1;i++){
			int dex = (mdex*l)-1;
			int mnt = 0;
			for(j=0;j<cnt;j++){
				mnt++;
				avg[i]+=val[dex];
				dex+=(l-1);
			}
			avg[i]/=cnt;
			cnt--;
			mdex++;
		}

	}else{
		cnt  = m;
		int mdex = 1;
		for(i=m;i<l+m-1;i++){
			int dex = (mdex*l)-1;
			int mnt = 0;
			for(j=0;j<cnt;j++){
				mnt++;
				avg[i]+=val[dex];
				dex+=(l-1);
			}
			avg[i]/=cnt;
			cnt--;
			mdex++;
		}
	}
    return avg;
}

void stack(double* d1,double* d2,int l){
	int i;
    for(i=0;i<l;i++){
        d1[i]+=d2[i];
    }
}

void divarr(double* d1,double div, int l){
    int i;
	for(i=0;i<l;i++){
        d1[i]/=div;
    }
} 

void freehankel1d(hankel1d* hankel){
	free2double(hankel->data);
	free(hankel);
}

void writearray(FILE* outfileptr,float** data, int n1, int n2){
	int row,col;	
	for(row=0;row<n1;row++){
		for(col=0;col<n2;col++){
			efwrite(&data[row][col],1,sizeof(float),outfileptr);	
		}   
	}
}

float** getdata(sugather* gather, int ntrace, int ns){
	float** data = (float**) ealloc2float(ns,ntrace);
	int row,col;

	for(row=0;row<ntrace;row++){
		for(col=0;col<ns;col++){
			data[row][col]=gather->traces[row].data[col];	
		}
	}
	return data;	
}

double** getdatad(sugather* gather, int ntrace, int ns){
	double** data = (double**) ealloc2double(ns,ntrace);
	int row,col;

	for(row=0;row<ntrace;row++){
		for(col=0;col<ns;col++){
			data[row][col]=(double)gather->traces[row].data[col];	
		}
	}
	return data;	
}

void convolve1d(float* function, int flength, float* kernel, int klength){
	int span=(klength-1)/2;
	int plength = (flength+(2*span));
	float* panel = (float*) calloc( plength , sizeof(float));
	memcpy(&panel[span],&function[0],sizeof(float)*flength);
	int i;
	for(i=0;i<span;i++){panel[i]=function[0];}
	for(i=plength-span;i<plength;i++){panel[i]=function[flength-1];}

	int indexConv;
	float ConvReslt;
	int cc;
	float* D = (float*) calloc(klength,sizeof(float));
	for(i=span; i<(plength-span);i++){
	    zeros1d(D,klength);
		indexConv=0;
		ConvReslt=0;
		for(cc=i-span; cc<=i+span; cc++){
                ConvReslt+=(panel[cc] * kernel[indexConv]);
				indexConv++;
		}
		function[i-span]=ConvReslt;
	}
	free(D);
	free(panel);
}

void convolven1(float** input, int n1, int n2, float* kernel, int klength){
	int row;
	for(row=0;row<n1;row++){
		convolve1d(input[row],n2,kernel,klength);
	}
}

void convolven2(float** input, int n1, int n2, float* kernel, int klength){
	float* panel = (float*) calloc(n1,sizeof(float));
	int col;
	for(col=0;col<n2;col++){
		getcol(input,col,panel,n1);
		convolve1d(panel,n1,kernel,klength);
		setcol(input,col,panel,n1);
	}
	free(panel);
}

float* gaussianderivative1kernel(int ksize,float sigma){
	if(ksize%2==0){printf("%i\n", ksize%2);exit(EXIT_FAILURE);}
	int span = (ksize-1) /2;
	float con = sqrt(2*3.14159)*sigma;
	float* kernel = (float*) malloc(ksize*sizeof(float));
	int x,index=0;
	for(x=span*-1; x<=span; x++){
		kernel[index]  = exp((-1.0*(float)x*(float)x)/(2.0*sigma*sigma))/con;
		kernel[index] *= ( (-1.0)* (float)x / (sigma*sigma));	
		index++;
	}
	return kernel;
}

float* morletkernel(int ksize,float sigma){
	if(ksize%2==0){printf("%i\n", ksize%2);exit(EXIT_FAILURE);}
	int span = (ksize-1) /2;
	float con = 2.0 / (sqrt(3*sigma)*1.331335083);
	float* kernel = (float*) malloc(ksize*sizeof(float));
	int x,index=0;

	for(x=span*-1; x<=span; x++){
		float xf = (float)x;
		kernel[index]  = exp((-1.0*xf*xf)/(2.0*sigma*sigma))*con;
		kernel[index] *= ( 1 - ( (xf*xf)/(sigma*sigma) ) );	
		index++;
	}
	return kernel;
}

float* gaussiankernel(int ksize,float sigma){
	if(ksize%2==0){printf("%i\n", ksize%2);exit(EXIT_FAILURE);}
	int span = (ksize-1) /2;
	float con = sqrt(2*3.14159)*sigma;
	float* kernel = (float*) malloc(ksize*sizeof(float));
	int x,index=0;
	for(x=span*-1; x<=span; x++){
		kernel[index]  = exp((-1.0*(float)x*(float)x)/(2.0*sigma*sigma))/con;	
		index++;
	}
	return kernel;
}

float* meankernel(int ksize){
	if(ksize%2==0){printf("%i\n", ksize%2);exit(EXIT_FAILURE);}
	int span = (ksize-1) /2;
	float* kernel = (float*) malloc(ksize*sizeof(float));
	int x;	
	float sum=0;
	for(x=0; x<ksize; x++){
		kernel[x]=1.0/(float)ksize;
		sum+=kernel[x];
	}
	return kernel;
}

void printarray1d(double* data,  int col){
	int j;
	for(j=0;j<col;j++){
		printf("%f ",data[j]);
	}
	printf("\n");
}

void printarray(double** data, int row, int col){
	int i,j;
	for(i=0;i<row;i++){
		for(j=0;j<col;j++){
			printf("%f ",data[i][j]);
		}
		printf("\n");
	}
}

void gettraceno(FILE* fptr,int tracedex, sutrace* trace){	
	unsigned int tlength = 240+(trace->header->ns*sizeof(float));
	unsigned long sel= tlength*tracedex;
	efseek(fptr,sel,SEEK_SET);
	efread(trace->header,1,sizeof(suheader),fptr);
	efread(trace->data,1,trace->header->ns*sizeof(float),fptr);
}

unsigned long fsize(FILE *fp){
    unsigned long prev=ftell(fp);
    fseek(fp, 0L, SEEK_END);
    unsigned long sz=ftell(fp);
    fseek(fp,prev,SEEK_SET); //go back to where we were
    return sz;
}
