/*
 * segy_replacevalue.c
 *
 *  Created on: May 22, 2012
 *      Author: toto
 */
#include "../src_lib/segy_lib.h"
/*********************** self documentation *****************************/
char *sdoc[] = {
		"									",
		" SEGY_REPLACEVALUE   : CHANGE SEGY DATA VALUE AT RANGE",
		"",
		" endian=0       (0=little endian)",
		" oldv=0.21      (change data if value >= oldv)",
		" newv=0.20      (replace oldv with newv)",
		"",
		NULL};

int main(int argc, char **argv)
{
	int endian;
	int format, nsegy;
	char ebcdic[3200];
	int ntrc, nsp;
	int i, j;
	float oldv, newv, dt;

	/* Initialize */
	initargs(argc, argv);
	requestdoc(1);

	if (!getparint("endian", &endian)) endian=0;
	if (!getparfloat("oldv", &oldv)) oldv=0.21;
	if (!getparfloat("newv", &newv)) newv=0.20;


	/*read header file */
	readEbcdicHeader(stdin, ebcdic);
	readBinaryHeader(stdin, endian, &bh, &nsegy);
	ntrc = getNumberOfTraceSegyFile(stdin, nsegy);
	nsp = bh.hns;
	dt = (float) (bh.hdt/1e+6);
	format = bh.format;
	bh.format = 1;

	writeEbcdicHeader(stdout, ebcdic);
	writeBinaryHeader(stdout, endian, &bh);
	for(i=0; i<ntrc; i++)
	{
		readTraceSegy(stdin, endian, nsp, format, nsegy, &tr);
		for(j=0; j<nsp; j++)
		{
			if(tr.data[j]>=oldv)
				tr.data[j] = newv;
		}
		writeTraceSegy(stdout, endian, &tr, nsegy, nsp);
	}

	return(1);
}
