/*
 * testcadzow1d.c
 *
 *  Created on: Jul 26, 2012
 *      Author: toto
 */

#include "../src_lib/cadzowLib.h"
#include "../src_lib/fftLib.h"

char *sdoc[] = {
		"			",
		"TESCADZOW1D ",
		"			",
		NULL};


int main(int argc, char ** argv) {

	/* hook up getpar to handle the parameters */
//	initargs(argc,argv);
//	requestdoc(1);
	int n, m, i, j;
	double *data;
	hankel1d *hank;

	n=10;
	data = alloc1double(n);
	for(i=0; i<n; i++)
		data[i] = (float)i;

	hank = createhankel1d_modif(data, n);
	inverseHankel1D(hank, data);


	m=3;
	n=3;
	double y[3][3] = { {1, 2, 3},
	                  {4, 5, 6},
	                  {7, 8, 9}   };
	printf("\n HANKEL \n");
	for (i=0; i<m; i++){
		for(j=0; j<n; j++){
			printf("%3.3f  ", y[i][j]);
		}
		printf("\n");
	}
	double **y1;
	y1= alloc2double(n,m);
	memcpy(y1[0], y[0], m*n*sizeof(double));
	rankApproximationd(y1,3,3,1);
	printf("\n HANKEL \n");
	for (i=0; i<m; i++){
		for(j=0; j<n; j++){
			printf("%3.3f  ", y1[i][j]);
		}
		printf("\n");
	}
//	//print data
//	printf("\n DATA : \n");
//	for (i=0; i<n; i++)
//		printf("%i. %f \n", i+1, data[i]);

	//print hankel


	return(1);
}
