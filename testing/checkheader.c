/*
 * checkheader.c
 *
 *  Created on: Jan 11, 2012
 *      Author: toto
 */

#include "segy_lib.h"
#include <sys/types.h>
#include <stdbool.h>
#include "wave_3dlib.h"
#include <time.h>

char *sdoc[] = {
		"									",
		" checkheader",
		NULL};
int main (int argc, char **argv)
{
	/* hook up getpar to handle the parameters */
//	initargs(argc,argv);
	requestdoc(1);
	float data[1];
	int endian;

	FILE *fid=NULL;
	fid = fopen("/data/Process/petrocina3d/BETARA3D_MERGE_PSTM_STK_INLINE270-750.segy","r");
	if(fid==NULL)
		err("error reading file");

	endian=0;
	fseek(fid, 3632, SEEK_SET);
	fread(data, sizeof(float), 1, fid);
	ibm_to_float((int *) data, (int *) data, 1, endian);
	printf("cdpx %f \n", data[0]);

	fread(data, sizeof(float), 1, fid);
	ibm_to_float((int *) data, (int *) data, 1, endian);
	printf("cdpy %f \n", data[0]);

	fclose(fid);
	return(1);
}
