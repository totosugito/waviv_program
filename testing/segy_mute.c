/*
 * segy_mute.c
 *
 *  Created on: Apr 27, 2012
 *      Author: toto
 */

#include "segy_lib.h"
#include <string.h>
#include <stdlib.h>

/*********************** self documentation *****************************/
char *sdoc[] = {
		"									",
		" SEGY_MUTE",
		"",
		" endian=0      (0=little endian)",
		"",
		NULL};

typedef struct muteData{
	float offset[SHRT_MAX];
	float time[SHRT_MAX];
	int n;
	float cdp;
}muteData;

muteData *readMuteFile(char *cinputfile, int *nmutedata);
void setMuteData(muteData *mutedata,  float foffset, float ftime, float fcdp, int imute);
void printMuteData(muteData *mutedata, int nmutedata);
int getRefCdpIndex(muteData *mutedata, int nmutedata, float cur_cdp, int cur_idx);
int getMutePoint(float offset1, float time1, float offset2, float time2,
		float cur_offset, float dt);
void getLiniearFormula(float offset1, float time1,
		float offset2, float time2,
		float *m, float *c);
int getMutePos(float m, float c, float offset, float dt);
int main(int argc, char **argv)
{
	int endian;
	muteData *mutedata=NULL;
	int nmutedata;
	int format, nsegy;
	char ebcdic[3200];
	char *cmutefile, *csegyinp;

	int ntrc, nsp, cur_idx, idx;
	int i;
	float cur_cdp, tmpcdp;

	FILE *fid;
	float dt;
	/* Initialize */
	initargs(argc, argv);
	requestdoc(1);

	MUSTGETPARSTRING("segyinp",  &csegyinp);
	MUSTGETPARSTRING("mutefile",  &cmutefile);
	if (!getparint("endian", &endian)) endian=0;


	/* open file */
	fid = fopen (csegyinp,"r");
	if (fid==NULL)
		err ("Error opening list file %s\n", csegyinp);
	/*read header file */
	readEbcdicHeader(fid, ebcdic);
	readBinaryHeader(fid, endian, &bh, &nsegy);
	ntrc = getNumberOfTraceSegyFile(fid, nsegy);
	nsp = bh.hns;
	dt = (float) (bh.hdt/1e+6);
	format = bh.format;

	mutedata = readMuteFile(cmutefile, &nmutedata);
	//printMuteData(mutedata, nmutedata);

	cur_idx = 0;
	cur_cdp = 0;
	for(i=0; i<ntrc; i++)
	{
		readTraceSegy(fid, endian, nsp, format, nsegy, &tr);
		tmpcdp = tr.cdp;

//		cur_cdp = tmpcdp;
//		idx = getRefCdpIndex(mutedata, nmutedata, cur_cdp, cur_idx);
//		//get T mute
//		cur_idx = idx;

		//------------- PRINTF---------------------------------------------
		if(i==0)
		{
			cur_cdp = tmpcdp;
			idx = getRefCdpIndex(mutedata, nmutedata, cur_cdp, cur_idx);

			//get T mute

			printf("[SEGY_CDP MUTE_CDP] = [ %5.1f   %5.1f ] \n", cur_cdp, mutedata[idx].cdp);
			cur_idx = idx;
		}

		if(cur_cdp != tmpcdp)
		{
			cur_cdp = tmpcdp;
			idx = getRefCdpIndex(mutedata, nmutedata, cur_cdp, cur_idx);
			printf("[SEGY_CDP MUTE_CDP] = [ %5.1f   %5.1f ] \n", cur_cdp, mutedata[idx].cdp);
			cur_idx = idx;
		}
		//-------------END PRINTF---------------------------------------------

	}
	fclose(fid);

	free(mutedata);
	return(1);
}

muteData *readMuteFile(char *cinputfile, int *nmutedata)
{
	char *buffer, *sizec=NULL;
	FILE *fid=NULL;
	int nbyte;
	int i, ncdp, nmute_tr;
	float last_cdp;
	float cur_gx, cur_gy, cur_cdp, cur_offset, cur_time;
	muteData *mutedata;

	nbyte = 200;
	buffer = (char*) calloc (nbyte, sizeof(char));

	fid = fopen(cinputfile, "r");
	if(!fid) err("error opening file %s", cinputfile);

	for(i=0; i<4; i++)	//ignore header mute file
		sizec = fgets (buffer, nbyte, fid);

	//----------------------------- GET FILE INFO -----------------------------------
	sizec = fgets (buffer, nbyte, fid); //get first mute data
	sscanf(buffer, "%f %f %f %f %f", &cur_gx, &cur_gy, &cur_cdp, &cur_offset, &cur_time);
	last_cdp = cur_cdp;
	ncdp = 1;
	nmute_tr = 1;

	while (fgets (buffer, nbyte, fid)!=NULL) //read mute data
	{
		sscanf(buffer, "%f %f %f %f %f", &cur_gx, &cur_gy, &cur_cdp, &cur_offset, &cur_time);
		if(last_cdp==cur_cdp)
			nmute_tr = nmute_tr+1;
		else
		{
			nmute_tr = 1;
			last_cdp = cur_cdp;
			ncdp = ncdp+1;
		}
	}

	//----------------------------- SAVE MUTE DATA -----------------------------------
	rewind(fid);
	mutedata = (muteData*) calloc((ncdp+1), sizeof(muteData));

	for(i=0; i<4; i++)	//ignore header mute file
		sizec = fgets (buffer, nbyte, fid);

	sizec = fgets (buffer, nbyte, fid); //get first mute data
	sscanf(buffer, "%f %f %f %f %f", &cur_gx, &cur_gy, &cur_cdp, &cur_offset, &cur_time);
	last_cdp = cur_cdp;
	ncdp = 1;
	nmute_tr = 1;
	setMuteData(&mutedata[ncdp-1], cur_offset, cur_time, cur_cdp, nmute_tr);

	while (fgets (buffer, nbyte, fid)!=NULL) //read mute data
	{
		sscanf(buffer, "%f %f %f %f %f", &cur_gx, &cur_gy, &cur_cdp, &cur_offset, &cur_time);
		if(last_cdp==cur_cdp)
			nmute_tr = nmute_tr+1;
		else
		{
			nmute_tr = 1;
			last_cdp = cur_cdp;
			ncdp = ncdp+1;
		}
		setMuteData(&mutedata[ncdp-1], cur_offset, cur_time, cur_cdp, nmute_tr);
	}

	(*nmutedata) = ncdp;
	fclose(fid);
	free(buffer);

	return(mutedata);
}

void setMuteData(muteData *mutedata, float foffset, float ftime, float fcdp, int imute)
{
	mutedata->n = imute;
	mutedata->offset[imute-1] = foffset;
	mutedata->time[imute-1] = ftime;
	mutedata->cdp = fcdp;
}

void printMuteData(muteData *mutedata, int nmutedata)
{
	int i, j;

	for(i=0; i<nmutedata; i++) //loop over cdp mute info
	{
		printf("%i. CDP [ %5.1f ] \n", i+1, mutedata[i].cdp);
		printf("[Offset, Time] \n");
		for(j=0; j<mutedata[i].n; j++)
			printf("[%5.3f, %5.3f], ", mutedata[i].offset[j], mutedata[i].time[j]);

		printf("\n\n");
	}
}

int getRefCdpIndex(muteData *mutedata, int nmutedata, float cur_cdp, int cur_idx)
{
	int idx;

	idx = cur_idx;

	if(cur_idx == nmutedata-1)
		return(nmutedata-1);

	if(cur_idx == 0)
	{
		if(cur_cdp <= mutedata[cur_idx].cdp)
			return(idx);
	}

	while(true)
	{
		//printf("%5.2f   %5.2f   %5.2f \n", cur_cdp, mutedata[cur_idx].cdp, mutedata[cur_idx+1].cdp);
		if((cur_cdp==mutedata[cur_idx+1].cdp) && (cur_idx+1<nmutedata))
		{
			idx = cur_idx+1;
			if(idx==nmutedata) idx = idx-1;
			break;
		}

		if((cur_cdp >=mutedata[cur_idx].cdp) && (cur_cdp <mutedata[cur_idx+1].cdp))
		{
			idx = cur_idx;
			break;
		}
		else
			cur_idx = cur_idx+1;

		if(cur_idx==nmutedata)
			break;
	}

	return(idx);
}


int getMutePoint(float offset1, float time1, float offset2, float time2,
		float cur_offset, float dt)
{
	float m,c;
	int pos;

	getLiniearFormula(offset1, time1,
			offset2, time2, &m, &c);
	pos = getMutePos(m, c, cur_offset, dt);

	return(pos);
}

void getLiniearFormula(float offset1, float time1,
		float offset2, float time2,
		float *m, float *c)
{
	float m1, c1;

	m1 = (offset2 - offset1) / (time2 - time1);
	c1 = offset1 - m1*time1;

	(*m) = m1;
	(*c) = c1;
}

int getMutePos(float m, float c, float offset, float dt)
{
	int mpoint, tpoint;

	mpoint = m*offset + c;
	tpoint = (int) (dt*mpoint);
	return(tpoint);
}
