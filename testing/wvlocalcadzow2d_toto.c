#include <stdio.h>
#include <fftw3.h>
#include <stdlib.h>

#include "waviv_common.h"
#include "../src_lib/fftLib.h"
#include <su.h>

#define CFL 0.7
#define LOOKFAC	2	/* Look ahead factor for npfaro	  */
#define PFA_MAX	720720	/* Largest allowed nfft	          */
#define SF_PI (3.14159265358979323846264338328)


/*********************** self documentation **********************/
char *sdoc[] = {
		"	Eigen Filter PROTOTYPE			",
		"						",
		"						",
};
/********************* end self documentation ********************/
/* DGESVD prototype */
extern void dgesvd_( char* jobu, char* jobvt, int* m, int* n, double* a,
		int* lda, double* s, double* u, int* ldu, double* vt, int* ldvt,
		double* work, int* lwork, int* info );
#define min(a,b) ((a)>(b)?(b):(a))
void print_matrix( char* desc, int m, int n, double* a, int lda );
void svd(double** data, int row, int col, double** S, double** U, double** VT);
void svd_inv(double** data, int m, int n, double** S, double** U, double** VT);
double** rankApproximation(double** data,int row,int col, int rank);
double** getrank(double** u,double** s,double** v, int row, int col, int rank);
void getsubmatrix(double** data, double** window, int cx, int cy, int span,int spantrace);
void setsubmatrix(double** data, double** window, int cx, int cy, int span,int spantrace);
void fftProcessxx(double** input, double** outputreal, double** outputimag, int ntraces, int ns);
void ifftProcessxx(double** output, double** inputreal, double** inputimag, int ntraces, int ns);
void getNfft(int nz, float dt, int *nf, int *nfft, float *d1);
void cadzow2d(double** data,double** outdata, int row, int col, int nbeg, int nend,int rank, int verbose);
void setsubmatrixuse(double** data, double** window, int cx, int cy, int span,int use,int spantrace);
void addsubmatrix(double** data, double** window, int cx, int cy, int span,int spantrace);
void gaussian2dmask(double** data, int nx, int ny,float sigmax, float sigmay);
void mask(double** input, double** mask, int nrow, int ncol);
void svdlapackd(double **data, int irow, int icol,
		double **U, double *S, double **VT);
int main(int argc, char* argv[]){
	initargs(argc,argv);
	char *infilename;
	char *outfilename;
	FILE* outfileptr;
	int iswindowing;
	MUSTGETPARSTRING("infile",&infilename);
	MUSTGETPARSTRING("outfile",&outfilename);

	int rank,span,use,spantrace,usetrace;
	float sigmax,sigmay;
	if(!getparint("rank",&rank))rank=3;

	if(!getparint("span",&span))span=30;
	if(!getparint("use",&use))use=5;
	if(!getparint("iswindowing",&iswindowing))iswindowing=0;

	if(!getparint("spantrace",&spantrace))spantrace=5;
	if(!getparint("usetrace",&usetrace))usetrace=1;

	if(!getparfloat("sigmax",&sigmax))sigmax=10;
	if(!getparfloat("sigmay",&sigmay))sigmay=10;

	sugather* gather = (sugather*) load(infilename);
	int ns = gather->ns;
	int ntraces = gather->ntraces;
	float dt = gather->dt;

	int nf, nfft;
	float d1;
	getNfft(ns, dt, &nf,&nfft, &d1);
	double** data = (double**) getdatad(gather,ntraces,ns);
	double** panel = ealloc2double(ns,(ntraces));
	double** panel_out = ealloc2double(ns,(ntraces));

	int x,y;
	for(x=0;x<ns;x++){
		for(y=0;y<ntraces;y++){
			panel[y][x]=data[y][x];
			panel_out[y][x]=0.0;
		}
	}
	if(iswindowing==1)
	{	
		int jump=(use*2)+1;
		int ww = (span*2)+1;

		int jumptrace=(usetrace*2)+1;
		int wwtrace = (spantrace*2)+1;

		double** window = ealloc2double(ww,wwtrace);
		double** filtered = ealloc2double(ww,wwtrace);
		double** windowreal = ealloc2double(ww,wwtrace);
		double** windowimag = ealloc2double(ww,wwtrace);
		double** windowrealcadzowed = ealloc2double(ww,wwtrace);
		double** windowimagcadzowed = ealloc2double(ww,wwtrace);
		double** gaussmask = ealloc2double(ww,wwtrace);
		gaussian2dmask(gaussmask, wwtrace,ww, sigmax, sigmay);


		for(x=span;x<ns-span;x+=jump){
			for(y=spantrace;y<ntraces-spantrace;y+=jumptrace){
				getsubmatrix(panel,window,x,y,span,spantrace);
				mask(window,gaussmask,wwtrace,ww);
				fftProcessxx(window, windowreal, windowimag, wwtrace, ww);

				cadzow2d(windowreal,windowrealcadzowed,wwtrace,ww,0,ww,rank,0);
				cadzow2d(windowimag,windowimagcadzowed,wwtrace,ww,0,ww,rank,0);

				ifftProcessxx(filtered, windowrealcadzowed, windowimagcadzowed, wwtrace, ww);

				mask(filtered,gaussmask,wwtrace,ww);
				addsubmatrix(panel_out,filtered,x,y,span,spantrace);

			}
		}
		free2double(windowreal);
		free2double(windowimag);
		free2double(windowrealcadzowed);
		free2double(windowimagcadzowed);
	}
	else
	{
		double** windowreal = ealloc2double(nf, ntraces);
		double** windowimag = ealloc2double(nf, ntraces);
		double** windowrealcadzowed = ealloc2double(nf,ntraces);
		double** windowimagcadzowed = ealloc2double(nf,ntraces);
		float **data = alloc2float(ns, ntraces);

		for(x=0;x<ns;x++)
			for(y=0;y<ntraces;y++)
				data[y][x] = (float) (panel[y][x]);
//		fftProcess(panel, windowreal, windowimag, ntraces, ns);
		fftProcessd(data, ns, ntraces, nf, nfft, windowreal, windowimag);
		cadzow2d(windowreal, windowrealcadzowed, ntraces, nf, 0, nf, rank,0);
		cadzow2d(windowimag, windowimagcadzowed, ntraces, nf, 0, nf, rank,0);
		ifftProcessd(data, ns, ntraces, nf, nfft, windowrealcadzowed, windowimagcadzowed);
//		ifftProcess(panel_out, windowrealcadzowed, windowimagcadzowed, ntraces, ns);

		for(x=0;x<ns;x++)
			for(y=0;y<ntraces;y++)
				panel_out[y][x] = (double) (data[y][x]);

		free2double(windowreal);
		free2double(windowimag);
		free2double(windowrealcadzowed);
		free2double(windowimagcadzowed);
	}
	printf("FINISHED \n");
	//printf("Last trace center: %i %i \n",y,(y+spantrace));
	//printf("Number of Traces: %i\n",ntraces);

	float** outdata = (float**) tofloat2d(panel_out, ntraces, ns);

	outfileptr = efopen(outfilename,"w");
	int tr;
	for(tr=0;tr<ntraces;tr++){
		efwrite(gather->traces[tr].header,1,sizeof(suheader),outfileptr);
		efwrite(outdata[tr],1,ns*sizeof(float),outfileptr);
	}
	efclose(outfileptr);
	free(gather);
	free2double(data);
	free2double(panel);
	free2double(panel_out);

	/*free2double(window);
	free2double(filtered);*/

	free2float(outdata);
	return(0);
}


void getsubmatrix(double** data, double** window, int cx, int cy, int span,int spantrace){
	int i,j;
	int ndex=0,tdex=0;
	for(i=cx-span;i<=cx+span;i++){
		tdex=0;
		for(j=cy-spantrace;j<=cy+spantrace;j++){
			window[tdex][ndex]=data[j][i];
			tdex++;
		}
		ndex++;
	}
}

void setsubmatrix(double** data, double** window, int cx, int cy, int span,int spantrace){
	int i,j;
	int ndex=0,tdex=0;
	for(i=cx-span;i<=cx+span;i++){
		tdex=0;
		for(j=cy-span;j<=cy+span;j++){
			data[j][i]=window[tdex][ndex];
			tdex++;
		}
		ndex++;
	}
}

void addsubmatrix(double** data, double** window, int cx, int cy, int span,int spantrace){
	int i,j;
	int ndex=0,tdex=0;
	for(i=cx-span;i<=cx+span;i++){
		tdex=0;
		for(j=cy-spantrace;j<=cy+spantrace;j++){
			data[j][i]+=window[tdex][ndex];
			tdex++;
		}
		ndex++;
	}
}

void setsubmatrixuse(double** data, double** window, int cx, int cy, int span,int use,int spantrace){
	int i,j;
	int tdex=(span-use);
	int ndex=(span-use);
	for(i=cx-use;i<=cx+use;i++){
		tdex=(span-use);
		for(j=cy-use;j<=cy+use;j++){
			data[j][i]=window[tdex][ndex];
			tdex++;
		}
		ndex++;
	}
}

void svd_inv(double** data, int m, int n, double** S, double** U, double** VT){
	int i,j,r;
	double** temp = (double**) ealloc2double(n,m);
	dzeros2d(temp,m,n);
	dzeros2d(data,m,n);

	for(i=0;i<m;i++){
		for(j=0;j<n;j++){
			for(r=0;r<n;r++){
				temp[i][j]+=(S[i][r]*VT[r][j]);
			}
		}
	}

	for(i=0;i<m;i++){
		for(j=0;j<n;j++){
			for(r=0;r<m;r++){
				data[i][j]+=(U[i][r]*temp[r][j]);
			}
		}
	}

	free2double(temp);
}

void svd(double** data, int row, int col, double** S, double** U, double** VT){
//	int m=row,n=col,lda=n,ldu=m,ldvt=n,info;
//	double superb[min(m,n)-1];
//	int scnt = min(m,n);
//	double* aloc = (double*) malloc(lda*m*sizeof(double));
//	double* s = (double*) malloc(n*sizeof(double));
//	double* u = (double*) malloc(ldu*m*sizeof(double));
//	double* vt = (double*) malloc(ldvt*n*sizeof(double));
//
//
//	int i;
//	for(i=0;i<m;i++){
//		memcpy(&aloc[i*lda],data[i],lda*sizeof(double));
//	}
//
////	info = LAPACKE_dgesvd( LAPACK_ROW_MAJOR, 'A', 'A', m, n, aloc, lda,
////			s, u, ldu, vt, ldvt, superb );
//
//
//	if( info > 0 ) {
//		printf( "The algorithm computing SVD failed to converge.\n" );
//		exit( 1 );
//	}
//
//	for(i=0;i<m;i++){
//		memcpy(U[i],&u[i*m],m*sizeof(double));
//	}
//
//	for(i=0;i<n;i++){
//		memcpy(VT[i],&vt[i*n],n*sizeof(double));
//	}
//
//	for(i=0;i<scnt;i++){
//		S[i][i]=s[i];
//	}
//
//	free(aloc);
//	free(s);
//	free(u);
//	free(vt);
}

void svdlapackd(double **data, int irow, int icol,
		double **U, double *S, double **VT)
{
	/* Locals  */
	int info, lwork;

	double *tmpa, *tmpu, *tmpv;
	double wkopt;
	double* work;
	int i, j, ioff;

	tmpa = alloc1double(irow*icol);
	tmpu = alloc1double(irow*irow);
	tmpv = alloc1double(icol*icol);

	ioff = 0;
	for(i=0; i<icol; i++)
	{
		for(j=0; j<irow; j++)
		{
			tmpa[ioff] = data[j][i];
			ioff = ioff+1;
		}
	}

	/* Query and allocate the optimal workspace */
	lwork = -1;
	dgesvd_( "All", "All", &irow, &icol, tmpa, &irow, S, tmpu, &irow, tmpv, &icol, &wkopt, &lwork,
			&info );

	lwork = (int)wkopt;
	work = (double*)malloc( lwork*sizeof(double) );

	/* Compute SVD */
	dgesvd_( "All", "All", &irow, &icol, tmpa, &irow, S, tmpu, &irow, tmpv, &icol, work, &lwork,
			&info );

	/* Check for convergence */
	if( info > 0 ) {
		printf( "The algorithm computing SVD failed to converge.\n" );
		exit( 1 );
	}

	/* copy from tmpu to u */
	ioff = 0;
	for(i=0; i<irow; i++)
	{
		for(j=0; j<irow; j++)
		{
			U[j][i] = tmpu[ioff];
			ioff = ioff+1;
		}
	}

	ioff = 0;
	for(i=0; i<icol; i++)
	{
		for(j=0; j<icol; j++)
		{
			VT[j][i] = tmpv[ioff];		//V transpose
			//VT[i][j] = tmpv[ioff];	//V
			ioff = ioff+1;
		}
	}

	free( (void*)work );
	free1double(tmpa);
	free1double(tmpu);
	free1double(tmpv);
}


double** rankApproximation(double** data,int row,int col, int rank){
	double** u = (double**)ealloc2double(row,row);
	double** s = (double**)ealloc2double(col,row);
	double** v = (double**)ealloc2double(col,col);
	double *s1 = alloc1double(row);
	int i;
	//svd(data, row, col, s, u, v);
	svdlapackd(data, row, col, u, s1, v);

	int scnt = min(col,row);
	for(i=0; i<scnt; i++)
		s[i][i] = s1[i];
	free1double(s1);

	double** out = (double**)ealloc2double(col,row);
	int iter,j;
	for(i=0;i<row;i++){
		for(j=0;j<col;j++){
			out[i][j]=0.0;
		}
	}
	for(iter=0;iter<rank;iter++){
		double** eigenimage = getrank(u,s,v,row,col,iter);
		for(i=0;i<row;i++){
			for(j=0;j<col;j++){
				out[i][j]+=eigenimage[i][j];
			}
		}
		free2double(eigenimage);
	}

	free2double(u);
	free2double(s);
	free2double(v);

	return out;
}

double** getrank(double** u,double** s,double** v, int row, int col, int rank){
	double** su = (double**)ealloc2double(col,row);
	int i,j;
	for(i=0;i<row;i++){
		for(j=0;j<col;j++){
			su[i][j]=(v[rank][j]*u[i][rank]*s[rank][rank]);
		}
	}
	return su;
}

void fftProcessxx(double** input, double** outputreal, double** outputimag, int ntraces, int ns){
	int SIZE=ns;
	fftw_complex    *data, *fft_result;
	fftw_plan       plan_forward;

	data        = ( fftw_complex* ) fftw_malloc( sizeof( fftw_complex ) * SIZE );
	fft_result  = ( fftw_complex* ) fftw_malloc( sizeof( fftw_complex ) * SIZE );
	plan_forward  = fftw_plan_dft_1d( SIZE, data, fft_result, FFTW_FORWARD, FFTW_ESTIMATE );
	int i, tracedex;

	for(tracedex=0;tracedex<ntraces;tracedex++){
		for( i = 0 ; i < SIZE ; i++ ) {
			data[i][0] = input[tracedex][i];
			data[i][1] = 0.0;
		}
		fftw_execute( plan_forward );
		for( i = 0 ; i < SIZE ; i++ ) {
			outputreal[tracedex][i] = fft_result[i][0];
			outputimag[tracedex][i] = fft_result[i][1];
		}
	}

	/* free memory */
	fftw_destroy_plan( plan_forward );
	fftw_free( data );
	fftw_free( fft_result );

}

void ifftProcessxx(double** output, double** inputreal, double** inputimag, int ntraces, int ns){
	int SIZE=ns;
	fftw_complex    *ifft_result,*fft_result;
	fftw_plan       plan_backward;

	fft_result  = ( fftw_complex* ) fftw_malloc( sizeof( fftw_complex ) * SIZE );
	ifft_result = ( fftw_complex* ) fftw_malloc( sizeof( fftw_complex ) * SIZE );
	plan_backward = fftw_plan_dft_1d( SIZE, fft_result, ifft_result, FFTW_BACKWARD, FFTW_ESTIMATE );
	int i, tracedex;

	for(tracedex=0;tracedex<ntraces;tracedex++){
		for( i = 0 ; i < SIZE ; i++ ) {
			fft_result[i][0]=inputreal[tracedex][i];
			fft_result[i][1]=inputimag[tracedex][i];
		}
		fftw_execute( plan_backward );

		for( i = 0 ; i < SIZE ; i++ ) {
			output[tracedex][i] = ifft_result[i][0]/SIZE;
		}
	}
	fftw_destroy_plan( plan_backward );
	fftw_free( ifft_result );
	fftw_free( fft_result );
}

void cadzow2d(double** data,double** outdata, int row, int col, int nbeg, int nend,int rank, int verbose){
	int f;	
	for(f=nbeg;f<nend;f++){
		double* fsliced = (double*) malloc(row*sizeof(double));
		getcold(data,f,fsliced,row);
		hankel1d* hankel = (hankel1d*) createhankel1d(fsliced,row);
		double** filter = (double**) rankApproximation(hankel->data,hankel->m,hankel->l,rank);
		memcpy(hankel->data[0],filter[0],hankel->m*hankel->l*sizeof(double));	
		double* invhankel = (double*) inverseHankel(hankel);
		setcold(outdata,f,invhankel,row);
		free(fsliced);
		free(invhankel);
		free2double(filter);
		freehankel1d(hankel);
	}	
}

void gaussian2dmask(double** data, int nx, int ny,float sigmax, float sigmay){
	if(nx%2==0){printf("%i\n", nx%2);exit(EXIT_FAILURE);}
	if(ny%2==0){printf("%i\n", ny%2);exit(EXIT_FAILURE);}

	int cx = ((nx-1)/2)+1;
	int cy = ((ny-1)/2)+1;
	int x,y;
	for(x=0;x<nx;x++){
		for(y=0;y<ny;y++){
			double xx = ((x-cx)*(x-cx))/(2*sigmax*sigmax);
			double yy = ((y-cy)*(y-cy))/(2*sigmay*sigmay);
			data[x][y]=exp(-1*(xx+yy));
		}
	}
}

void mask(double** input, double** mask, int nrow, int ncol){
	int x,y;
	for(y=0;y<ncol;y++){
		for(x=0;x<nrow;x++){
			input[x][y]=input[x][y]*mask[x][y];
		}
	}
}
