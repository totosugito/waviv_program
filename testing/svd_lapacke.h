/*
 * svd_lapacke.h
 *
 *  Created on: May 3, 2012
 *      Author: toto
 */

#ifndef SVD_LAPACKE_H_
#define SVD_LAPACKE_H_
#include "../lapacke/include/lapacke.h"
#include "../src_lib/segy_lib.h"

void svdlapacke(double **data, int irow, int icol,
		double *S, double **U, double **VT);
#endif /* SVD_LAPACKE_H_ */
