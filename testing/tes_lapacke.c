/*
 * tes_lapacke.c
 *
 *  Created on: Apr 26, 2012
 *      Author: toto
 */

#include "../src_lib/svdlapack.h"
#include "segy_lib.h"
/*********************** self documentation *****************************/
char *sdoc[] = {
		"									",
		" TES_LAPACKE",
		"",
		"",
		NULL};

void printdata(char *info, double **data, int irow, int icol);
void printdata1(char *info, double *data, int irow);
int main(int argc, char **argv)
{
	int i, j, idx;
	double **data;
	int irow, icol;
	double *S1, **U1, **VT1;

	initargs(argc, argv);

	if (!getparint("irow", &irow)) irow=7;
	if (!getparint("icol", &icol)) icol=4;

	data = alloc2double(icol, irow);
	idx=1;
	for(i=0; i<irow; i++)
	{
		for(j=0; j<icol; j++)
		{
			data[i][j] = (double)(idx);
			idx++;
		}
	}

	printdata("data ASLI", data, irow, icol); //print data asli
	U1 = alloc2double(irow, irow);
	VT1 = alloc2double(icol, icol);
	S1 = alloc1double(icol);
	svdlapacked(data, irow, icol, U1, S1, VT1);
	printdata("U", U1, irow, irow); //print data asli
	printdata("VT", VT1, icol, icol); //print data asli
	printdata1("S", S1, icol); //print data asli

	svdlapackd(data, irow, icol, U1, S1, VT1);
	printdata("U", U1, irow, irow); //print data asli
	printdata("VT", VT1, icol, icol); //print data asli
	printdata1("S", S1, icol); //print data asli


	free2double(data);
	free2double(U1);
	free2double(VT1);
	free1double(S1);
	return(1);
}

void printdata(char *info, double **data, int irow, int icol)
{
	int i,j;

	printf("%s\n", info);
	for(i=0; i<irow; i++)
	{
		for(j=0; j<icol; j++)
		{
			printf("%5.3f  ", data[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

void printdata1(char *info, double *data, int irow)
{
	int i;

	printf("%s\n", info);
	for(i=0; i<irow; i++)
	{
		printf("%5.3f  ", data[i]);
	}
	printf("\n");
}

